package org.apache.xml.security.algorithms;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Map;
import org.apache.xml.security.signature.XMLSignatureException;
import org.w3c.dom.Document;

public class MessageDigestAlgorithm
  extends Algorithm
{
  static ThreadLocal b;
  MessageDigest a = null;
  
  static
  {
    MessageDigestAlgorithm.1 local1 = new org/apache/xml/security/algorithms/MessageDigestAlgorithm$1;
    local1.<init>();
    b = local1;
  }
  
  private MessageDigestAlgorithm(Document paramDocument, MessageDigest paramMessageDigest, String paramString)
  {
    super(paramDocument, paramString);
    a = paramMessageDigest;
  }
  
  public static MessageDigestAlgorithm a(Document paramDocument, String paramString)
  {
    MessageDigest localMessageDigest = b(paramString);
    MessageDigestAlgorithm localMessageDigestAlgorithm = new org/apache/xml/security/algorithms/MessageDigestAlgorithm;
    localMessageDigestAlgorithm.<init>(paramDocument, localMessageDigest, paramString);
    return localMessageDigestAlgorithm;
  }
  
  public static boolean a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    return MessageDigest.isEqual(paramArrayOfByte1, paramArrayOfByte2);
  }
  
  private static MessageDigest b(String paramString)
  {
    i = 2;
    j = 1;
    Object localObject1 = (MessageDigest)((Map)b.get()).get(paramString);
    if (localObject1 != null) {
      return (MessageDigest)localObject1;
    }
    localObject4 = JCEMapper.a(paramString);
    if (localObject4 == null)
    {
      localObject1 = new Object[j];
      localObject1[0] = paramString;
      localObject4 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject4).<init>("algorithms.NoSuchMap", (Object[])localObject1);
      throw ((Throwable)localObject4);
    }
    localObject1 = JCEMapper.a();
    if (localObject1 == null) {}
    for (;;)
    {
      try
      {
        localObject1 = MessageDigest.getInstance((String)localObject4);
        localObject4 = localObject1;
        ((Map)b.get()).put(paramString, localObject4);
        localObject1 = localObject4;
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
      {
        arrayOfObject = new Object[i];
        arrayOfObject[0] = localObject4;
        Object localObject2 = localNoSuchAlgorithmException.getLocalizedMessage();
        arrayOfObject[j] = localObject2;
        localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject2).<init>("algorithms.NoSuchAlgorithm", arrayOfObject);
        throw ((Throwable)localObject2);
      }
      catch (NoSuchProviderException localNoSuchProviderException)
      {
        Object[] arrayOfObject = new Object[i];
        arrayOfObject[0] = localObject4;
        Object localObject3 = localNoSuchProviderException.getLocalizedMessage();
        arrayOfObject[j] = localObject3;
        localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject3).<init>("algorithms.NoSuchAlgorithm", arrayOfObject);
        throw ((Throwable)localObject3);
      }
      localObject1 = MessageDigest.getInstance((String)localObject4, (String)localObject1);
      localObject4 = localObject1;
    }
  }
  
  public void a(byte paramByte)
  {
    a.update(paramByte);
  }
  
  public void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    a.update(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public byte[] b()
  {
    return a.digest();
  }
  
  public void c()
  {
    a.reset();
  }
  
  public String d()
  {
    return "http://www.w3.org/2000/09/xmldsig#";
  }
  
  public String e()
  {
    return "DigestMethod";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/MessageDigestAlgorithm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */