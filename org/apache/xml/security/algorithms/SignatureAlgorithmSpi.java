package org.apache.xml.security.algorithms;

import java.security.Key;
import org.w3c.dom.Element;

public abstract class SignatureAlgorithmSpi
{
  protected abstract String a();
  
  protected abstract void a(byte paramByte);
  
  protected abstract void a(Key paramKey);
  
  protected void a(Element paramElement) {}
  
  protected abstract void a(byte[] paramArrayOfByte);
  
  protected abstract void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2);
  
  protected abstract String b();
  
  protected abstract boolean b(byte[] paramArrayOfByte);
  
  public void c() {}
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/SignatureAlgorithmSpi.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */