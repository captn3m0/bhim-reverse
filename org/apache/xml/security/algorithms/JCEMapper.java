package org.apache.xml.security.algorithms;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JCEMapper
{
  static Log a;
  static Class b;
  private static Map c;
  private static Map d;
  private static String e;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = b("org.apache.xml.security.algorithms.JCEMapper");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      e = null;
      return;
      localClass = b;
    }
  }
  
  public static String a()
  {
    return e;
  }
  
  public static String a(String paramString)
  {
    Log localLog = a;
    boolean bool = localLog.isDebugEnabled();
    if (bool)
    {
      localLog = a;
      Object localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>();
      String str = "Request for URI ";
      localObject = str + paramString;
      localLog.debug(localObject);
    }
    return (String)c.get(paramString);
  }
  
  public static void a(Element paramElement)
  {
    b((Element)paramElement.getElementsByTagName("Algorithms").item(0));
  }
  
  static Class b(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  static void b(Element paramElement)
  {
    Object localObject1 = paramElement.getFirstChild();
    String str1 = "Algorithm";
    Element[] arrayOfElement = XMLUtils.a((Node)localObject1, "http://www.xmlsecurity.org/NS/#configuration", str1);
    localObject1 = new java/util/HashMap;
    int i = arrayOfElement.length * 2;
    ((HashMap)localObject1).<init>(i);
    c = (Map)localObject1;
    localObject1 = new java/util/HashMap;
    i = arrayOfElement.length * 2;
    ((HashMap)localObject1).<init>(i);
    d = (Map)localObject1;
    int j = 0;
    localObject1 = null;
    for (;;)
    {
      i = arrayOfElement.length;
      if (j >= i) {
        break;
      }
      str1 = arrayOfElement[j];
      String str2 = str1.getAttribute("URI");
      Object localObject2 = str1.getAttribute("JCEName");
      c.put(str2, localObject2);
      localObject2 = d;
      JCEMapper.Algorithm localAlgorithm = new org/apache/xml/security/algorithms/JCEMapper$Algorithm;
      localAlgorithm.<init>(str1);
      ((Map)localObject2).put(str2, localAlgorithm);
      j += 1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/JCEMapper.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */