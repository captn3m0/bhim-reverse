package org.apache.xml.security.algorithms.implementations;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.JCEMapper;
import org.apache.xml.security.algorithms.MessageDigestAlgorithm;
import org.apache.xml.security.algorithms.SignatureAlgorithmSpi;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public abstract class IntegrityHmac
  extends SignatureAlgorithmSpi
{
  static Log a;
  static Class c;
  static Class d;
  int b = 0;
  private Mac e = null;
  private boolean f = false;
  
  static
  {
    Class localClass = c;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.algorithms.implementations.IntegrityHmac$IntegrityHmacSHA1");
      c = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = c;
    }
  }
  
  public IntegrityHmac()
  {
    String str1 = JCEMapper.a(d());
    Object localObject1 = a;
    boolean bool = ((Log)localObject1).isDebugEnabled();
    Object localObject3;
    if (bool)
    {
      localObject1 = a;
      localObject3 = new java/lang/StringBuffer;
      ((StringBuffer)localObject3).<init>();
      String str2 = "Created IntegrityHmacSHA1 using ";
      localObject3 = str2 + str1;
      ((Log)localObject1).debug(localObject3);
    }
    try
    {
      localObject1 = Mac.getInstance(str1);
      e = ((Mac)localObject1);
      return;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      localObject3 = new Object[2];
      localObject3[0] = str1;
      Object localObject2 = localNoSuchAlgorithmException.getLocalizedMessage();
      localObject3[1] = localObject2;
      localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject2).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject3);
      throw ((Throwable)localObject2);
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  protected String a()
  {
    a.debug("engineGetJCEAlgorithmString()");
    return e.getAlgorithm();
  }
  
  protected void a(byte paramByte)
  {
    try
    {
      Mac localMac = e;
      localMac.update(paramByte);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localIllegalStateException);
      throw localXMLSignatureException;
    }
  }
  
  protected void a(Key paramKey)
  {
    boolean bool1 = paramKey instanceof SecretKey;
    Object localObject2;
    if (!bool1)
    {
      localObject1 = paramKey.getClass().getName();
      localObject2 = d;
      if (localObject2 == null)
      {
        localObject2 = a("javax.crypto.SecretKey");
        d = (Class)localObject2;
      }
      for (;;)
      {
        localObject2 = ((Class)localObject2).getName();
        localObject3 = new Object[2];
        localObject3[0] = localObject1;
        localObject3[1] = localObject2;
        localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject2).<init>("algorithms.WrongKeyForThisOperation", (Object[])localObject3);
        throw ((Throwable)localObject2);
        localObject2 = d;
      }
    }
    try
    {
      localObject2 = e;
      ((Mac)localObject2).init((Key)paramKey);
      return;
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      localObject3 = e;
    }
    try
    {
      localObject1 = e;
      localObject1 = ((Mac)localObject1).getAlgorithm();
      localObject1 = Mac.getInstance((String)localObject1);
      e = ((Mac)localObject1);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log localLog = a;
        boolean bool2 = localLog.isDebugEnabled();
        if (bool2)
        {
          localLog = a;
          StringBuffer localStringBuffer = new java/lang/StringBuffer;
          localStringBuffer.<init>();
          String str2 = "Exception when reinstantiating Mac:";
          localStringBuffer = localStringBuffer.append(str2);
          String str1 = localException;
          localLog.debug(str1);
        }
        e = ((Mac)localObject3);
      }
    }
    Object localObject1 = new org/apache/xml/security/signature/XMLSignatureException;
    ((XMLSignatureException)localObject1).<init>("empty", localInvalidKeyException);
    throw ((Throwable)localObject1);
  }
  
  protected void a(Element paramElement)
  {
    super.a(paramElement);
    if (paramElement == null)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("element null");
      throw ((Throwable)localObject);
    }
    Object localObject = paramElement.getFirstChild();
    String str = "HMACOutputLength";
    localObject = XMLUtils.b((Node)localObject, str, 0);
    if (localObject != null)
    {
      localObject = ((Text)localObject).getData();
      int i = Integer.parseInt((String)localObject);
      b = i;
      i = 1;
      f = i;
    }
  }
  
  protected void a(byte[] paramArrayOfByte)
  {
    try
    {
      Mac localMac = e;
      localMac.update(paramArrayOfByte);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localIllegalStateException);
      throw localXMLSignatureException;
    }
  }
  
  protected void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      Mac localMac = e;
      localMac.update(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (IllegalStateException localIllegalStateException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localIllegalStateException);
      throw localXMLSignatureException;
    }
  }
  
  protected String b()
  {
    return e.getProvider().getName();
  }
  
  protected boolean b(byte[] paramArrayOfByte)
  {
    try
    {
      boolean bool1 = f;
      if (bool1)
      {
        int i = b;
        int j = e();
        if (i < j)
        {
          Object localObject1 = a;
          boolean bool2 = ((Log)localObject1).isDebugEnabled();
          if (bool2)
          {
            localObject1 = a;
            localObject3 = new java/lang/StringBuffer;
            ((StringBuffer)localObject3).<init>();
            str = "HMACOutputLength must not be less than ";
            localObject3 = ((StringBuffer)localObject3).append(str);
            k = e();
            localObject3 = ((StringBuffer)localObject3).append(k);
            localObject3 = ((StringBuffer)localObject3).toString();
            ((Log)localObject1).debug(localObject3);
          }
          bool2 = true;
          localObject1 = new Object[bool2];
          j = 0;
          localObject3 = null;
          int k = e();
          String str = String.valueOf(k);
          localObject1[0] = str;
          localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
          str = "algorithms.HMACOutputLengthMin";
          ((XMLSignatureException)localObject3).<init>(str, (Object[])localObject1);
          throw ((Throwable)localObject3);
        }
      }
    }
    catch (IllegalStateException localIllegalStateException)
    {
      Object localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject3).<init>("empty", localIllegalStateException);
      throw ((Throwable)localObject3);
    }
    Object localObject2 = e;
    localObject2 = ((Mac)localObject2).doFinal();
    return MessageDigestAlgorithm.a((byte[])localObject2, paramArrayOfByte);
  }
  
  public void c()
  {
    b = 0;
    f = false;
    e.reset();
  }
  
  public abstract String d();
  
  abstract int e();
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/implementations/IntegrityHmac.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */