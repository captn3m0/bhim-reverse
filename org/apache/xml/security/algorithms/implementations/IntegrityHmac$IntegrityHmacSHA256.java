package org.apache.xml.security.algorithms.implementations;

public class IntegrityHmac$IntegrityHmacSHA256
  extends IntegrityHmac
{
  public String d()
  {
    return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256";
  }
  
  int e()
  {
    return 256;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/implementations/IntegrityHmac$IntegrityHmacSHA256.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */