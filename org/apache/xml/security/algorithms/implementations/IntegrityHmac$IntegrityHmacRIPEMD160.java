package org.apache.xml.security.algorithms.implementations;

public class IntegrityHmac$IntegrityHmacRIPEMD160
  extends IntegrityHmac
{
  public String d()
  {
    return "http://www.w3.org/2001/04/xmldsig-more#hmac-ripemd160";
  }
  
  int e()
  {
    return 160;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/implementations/IntegrityHmac$IntegrityHmacRIPEMD160.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */