package org.apache.xml.security.algorithms.implementations;

public class IntegrityHmac$IntegrityHmacSHA512
  extends IntegrityHmac
{
  public String d()
  {
    return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha512";
  }
  
  int e()
  {
    return 512;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/implementations/IntegrityHmac$IntegrityHmacSHA512.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */