package org.apache.xml.security.algorithms.implementations;

public class IntegrityHmac$IntegrityHmacSHA384
  extends IntegrityHmac
{
  public String d()
  {
    return "http://www.w3.org/2001/04/xmldsig-more#hmac-sha384";
  }
  
  int e()
  {
    return 384;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/implementations/IntegrityHmac$IntegrityHmacSHA384.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */