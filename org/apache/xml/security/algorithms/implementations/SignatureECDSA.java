package org.apache.xml.security.algorithms.implementations;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.JCEMapper;
import org.apache.xml.security.algorithms.SignatureAlgorithmSpi;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.utils.Base64;

public abstract class SignatureECDSA
  extends SignatureAlgorithmSpi
{
  static Log a;
  static Class b;
  static Class c;
  private Signature d = null;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.algorithms.implementations.SignatureECDSA");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = b;
    }
  }
  
  public SignatureECDSA()
  {
    str1 = JCEMapper.a(d());
    Object localObject1 = a;
    boolean bool = ((Log)localObject1).isDebugEnabled();
    if (bool)
    {
      localObject1 = a;
      localObject4 = new java/lang/StringBuffer;
      ((StringBuffer)localObject4).<init>();
      String str2 = "Created SignatureECDSA using ";
      localObject4 = str2 + str1;
      ((Log)localObject1).debug(localObject4);
    }
    localObject1 = JCEMapper.a();
    if (localObject1 == null) {}
    for (;;)
    {
      try
      {
        localObject1 = Signature.getInstance(str1);
        d = ((Signature)localObject1);
        return;
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
      {
        localObject4 = new Object[i];
        localObject4[0] = str1;
        Object localObject2 = localNoSuchAlgorithmException.getLocalizedMessage();
        localObject4[j] = localObject2;
        localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject2).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject4);
        throw ((Throwable)localObject2);
      }
      catch (NoSuchProviderException localNoSuchProviderException)
      {
        localObject4 = new Object[i];
        localObject4[0] = str1;
        Object localObject3 = localNoSuchProviderException.getLocalizedMessage();
        localObject4[j] = localObject3;
        localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject3).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject4);
        throw ((Throwable)localObject3);
      }
      localObject1 = Signature.getInstance(str1, (String)localObject1);
      d = ((Signature)localObject1);
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  private static byte[] c(byte[] paramArrayOfByte)
  {
    int i = 48;
    int j = 2;
    int k = paramArrayOfByte.length;
    if (k < i)
    {
      IOException localIOException = new java/io/IOException;
      localIOException.<init>("Invalid XMLDSIG format of ECDSA signature");
      throw localIOException;
    }
    k = paramArrayOfByte.length;
    int m = k / 2;
    int n = m;
    while (n > 0)
    {
      k = m - n;
      k = paramArrayOfByte[k];
      if (k != 0) {
        break;
      }
      n += -1;
    }
    k = m - n;
    k = paramArrayOfByte[k];
    if (k < 0) {}
    for (k = n + 1;; k = n)
    {
      int i1 = m;
      while (i1 > 0)
      {
        i2 = m * 2 - i1;
        i2 = paramArrayOfByte[i2];
        if (i2 != 0) {
          break;
        }
        i1 += -1;
      }
      int i2 = m * 2 - i1;
      i2 = paramArrayOfByte[i2];
      if (i2 < 0) {}
      for (i2 = i1 + 1;; i2 = i1)
      {
        byte[] arrayOfByte = new byte[k + 6 + i2];
        arrayOfByte[0] = i;
        i = (byte)(k + 4 + i2);
        arrayOfByte[1] = i;
        arrayOfByte[j] = j;
        i = (byte)k;
        arrayOfByte[3] = i;
        int i3 = m - n;
        i = k + 4 - n;
        System.arraycopy(paramArrayOfByte, i3, arrayOfByte, i, n);
        n = k + 4;
        arrayOfByte[n] = j;
        n = k + 5;
        i3 = (byte)i2;
        arrayOfByte[n] = i3;
        n = m * 2 - i1;
        k = k + 6 + i2 - i1;
        System.arraycopy(paramArrayOfByte, n, arrayOfByte, k, i1);
        return arrayOfByte;
      }
    }
  }
  
  protected String a()
  {
    return d.getAlgorithm();
  }
  
  protected void a(byte paramByte)
  {
    try
    {
      Signature localSignature = d;
      localSignature.update(paramByte);
      return;
    }
    catch (SignatureException localSignatureException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localSignatureException);
      throw localXMLSignatureException;
    }
  }
  
  protected void a(Key paramKey)
  {
    boolean bool1 = paramKey instanceof PublicKey;
    Object localObject2;
    if (!bool1)
    {
      localObject1 = paramKey.getClass().getName();
      localObject2 = c;
      if (localObject2 == null)
      {
        localObject2 = a("java.security.PublicKey");
        c = (Class)localObject2;
      }
      for (;;)
      {
        localObject2 = ((Class)localObject2).getName();
        localObject3 = new Object[2];
        localObject3[0] = localObject1;
        localObject3[1] = localObject2;
        localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject2).<init>("algorithms.WrongKeyForThisOperation", (Object[])localObject3);
        throw ((Throwable)localObject2);
        localObject2 = c;
      }
    }
    try
    {
      localObject2 = d;
      paramKey = (PublicKey)paramKey;
      ((Signature)localObject2).initVerify(paramKey);
      return;
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      localObject3 = d;
    }
    try
    {
      localObject1 = d;
      localObject1 = ((Signature)localObject1).getAlgorithm();
      localObject1 = Signature.getInstance((String)localObject1);
      d = ((Signature)localObject1);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log localLog = a;
        boolean bool2 = localLog.isDebugEnabled();
        if (bool2)
        {
          localLog = a;
          StringBuffer localStringBuffer = new java/lang/StringBuffer;
          localStringBuffer.<init>();
          String str2 = "Exception when reinstantiating Signature:";
          localStringBuffer = localStringBuffer.append(str2);
          String str1 = localException;
          localLog.debug(str1);
        }
        d = ((Signature)localObject3);
      }
    }
    Object localObject1 = new org/apache/xml/security/signature/XMLSignatureException;
    ((XMLSignatureException)localObject1).<init>("empty", localInvalidKeyException);
    throw ((Throwable)localObject1);
  }
  
  protected void a(byte[] paramArrayOfByte)
  {
    try
    {
      Signature localSignature = d;
      localSignature.update(paramArrayOfByte);
      return;
    }
    catch (SignatureException localSignatureException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localSignatureException);
      throw localXMLSignatureException;
    }
  }
  
  protected void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      Signature localSignature = d;
      localSignature.update(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (SignatureException localSignatureException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localSignatureException);
      throw localXMLSignatureException;
    }
  }
  
  protected String b()
  {
    return d.getProvider().getName();
  }
  
  protected boolean b(byte[] paramArrayOfByte)
  {
    try
    {
      byte[] arrayOfByte = c(paramArrayOfByte);
      localObject1 = a;
      boolean bool = ((Log)localObject1).isDebugEnabled();
      if (bool)
      {
        localObject1 = a;
        Object localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        String str = "Called ECDSA.verify() on ";
        localObject2 = ((StringBuffer)localObject2).append(str);
        str = Base64.b(paramArrayOfByte);
        localObject2 = ((StringBuffer)localObject2).append(str);
        localObject2 = ((StringBuffer)localObject2).toString();
        ((Log)localObject1).debug(localObject2);
      }
      localObject1 = d;
      return ((Signature)localObject1).verify(arrayOfByte);
    }
    catch (SignatureException localSignatureException)
    {
      localObject1 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject1).<init>("empty", localSignatureException);
      throw ((Throwable)localObject1);
    }
    catch (IOException localIOException)
    {
      Object localObject1 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject1).<init>("empty", localIOException);
      throw ((Throwable)localObject1);
    }
  }
  
  public abstract String d();
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/implementations/SignatureECDSA.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */