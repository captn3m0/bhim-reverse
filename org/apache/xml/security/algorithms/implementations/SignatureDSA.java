package org.apache.xml.security.algorithms.implementations;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.JCEMapper;
import org.apache.xml.security.algorithms.SignatureAlgorithmSpi;
import org.apache.xml.security.signature.XMLSignatureException;
import org.apache.xml.security.utils.Base64;

public class SignatureDSA
  extends SignatureAlgorithmSpi
{
  static Log a;
  static Class b;
  static Class c;
  private Signature d = null;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.algorithms.implementations.SignatureDSA");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = b;
    }
  }
  
  public SignatureDSA()
  {
    str1 = JCEMapper.a("http://www.w3.org/2000/09/xmldsig#dsa-sha1");
    Object localObject1 = a;
    boolean bool = ((Log)localObject1).isDebugEnabled();
    if (bool)
    {
      localObject1 = a;
      localObject4 = new java/lang/StringBuffer;
      ((StringBuffer)localObject4).<init>();
      String str2 = "Created SignatureDSA using ";
      localObject4 = str2 + str1;
      ((Log)localObject1).debug(localObject4);
    }
    localObject1 = JCEMapper.a();
    if (localObject1 == null) {}
    for (;;)
    {
      try
      {
        localObject1 = Signature.getInstance(str1);
        d = ((Signature)localObject1);
        return;
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
      {
        localObject4 = new Object[i];
        localObject4[0] = str1;
        Object localObject2 = localNoSuchAlgorithmException.getLocalizedMessage();
        localObject4[j] = localObject2;
        localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject2).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject4);
        throw ((Throwable)localObject2);
      }
      catch (NoSuchProviderException localNoSuchProviderException)
      {
        localObject4 = new Object[i];
        localObject4[0] = str1;
        Object localObject3 = localNoSuchProviderException.getLocalizedMessage();
        localObject4[j] = localObject3;
        localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject3).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject4);
        throw ((Throwable)localObject3);
      }
      localObject1 = Signature.getInstance(str1, (String)localObject1);
      d = ((Signature)localObject1);
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  private static byte[] c(byte[] paramArrayOfByte)
  {
    int i = 20;
    int j = 2;
    int k = paramArrayOfByte.length;
    int m = 40;
    if (k != m)
    {
      IOException localIOException = new java/io/IOException;
      localIOException.<init>("Invalid XMLDSIG format of DSA signature");
      throw localIOException;
    }
    m = i;
    while (m > 0)
    {
      k = 20 - m;
      k = paramArrayOfByte[k];
      if (k != 0) {
        break;
      }
      m += -1;
    }
    k = 20 - m;
    k = paramArrayOfByte[k];
    if (k < 0) {
      k = m + 1;
    }
    for (;;)
    {
      if (i > 0)
      {
        n = 40 - i;
        n = paramArrayOfByte[n];
        if (n == 0)
        {
          i += -1;
          continue;
        }
      }
      int n = 40 - i;
      n = paramArrayOfByte[n];
      if (n < 0) {}
      for (n = i + 1;; n = i)
      {
        byte[] arrayOfByte = new byte[k + 6 + n];
        arrayOfByte[0] = 48;
        int i1 = (byte)(k + 4 + n);
        arrayOfByte[1] = i1;
        arrayOfByte[j] = j;
        i1 = (byte)k;
        arrayOfByte[3] = i1;
        int i2 = 20 - m;
        i1 = k + 4 - m;
        System.arraycopy(paramArrayOfByte, i2, arrayOfByte, i1, m);
        m = k + 4;
        arrayOfByte[m] = j;
        m = k + 5;
        i2 = (byte)n;
        arrayOfByte[m] = i2;
        m = 40 - i;
        k = k + 6 + n - i;
        System.arraycopy(paramArrayOfByte, m, arrayOfByte, k, i);
        return arrayOfByte;
      }
      k = m;
    }
  }
  
  protected String a()
  {
    return d.getAlgorithm();
  }
  
  protected void a(byte paramByte)
  {
    try
    {
      Signature localSignature = d;
      localSignature.update(paramByte);
      return;
    }
    catch (SignatureException localSignatureException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localSignatureException);
      throw localXMLSignatureException;
    }
  }
  
  protected void a(Key paramKey)
  {
    boolean bool1 = paramKey instanceof PublicKey;
    Object localObject2;
    if (!bool1)
    {
      localObject1 = paramKey.getClass().getName();
      localObject2 = c;
      if (localObject2 == null)
      {
        localObject2 = a("java.security.PublicKey");
        c = (Class)localObject2;
      }
      for (;;)
      {
        localObject2 = ((Class)localObject2).getName();
        localObject3 = new Object[2];
        localObject3[0] = localObject1;
        localObject3[1] = localObject2;
        localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
        ((XMLSignatureException)localObject2).<init>("algorithms.WrongKeyForThisOperation", (Object[])localObject3);
        throw ((Throwable)localObject2);
        localObject2 = c;
      }
    }
    try
    {
      localObject2 = d;
      paramKey = (PublicKey)paramKey;
      ((Signature)localObject2).initVerify(paramKey);
      return;
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      localObject3 = d;
    }
    try
    {
      localObject1 = d;
      localObject1 = ((Signature)localObject1).getAlgorithm();
      localObject1 = Signature.getInstance((String)localObject1);
      d = ((Signature)localObject1);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Log localLog = a;
        boolean bool2 = localLog.isDebugEnabled();
        if (bool2)
        {
          localLog = a;
          StringBuffer localStringBuffer = new java/lang/StringBuffer;
          localStringBuffer.<init>();
          String str2 = "Exception when reinstantiating Signature:";
          localStringBuffer = localStringBuffer.append(str2);
          String str1 = localException;
          localLog.debug(str1);
        }
        d = ((Signature)localObject3);
      }
    }
    Object localObject1 = new org/apache/xml/security/signature/XMLSignatureException;
    ((XMLSignatureException)localObject1).<init>("empty", localInvalidKeyException);
    throw ((Throwable)localObject1);
  }
  
  protected void a(byte[] paramArrayOfByte)
  {
    try
    {
      Signature localSignature = d;
      localSignature.update(paramArrayOfByte);
      return;
    }
    catch (SignatureException localSignatureException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localSignatureException);
      throw localXMLSignatureException;
    }
  }
  
  protected void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    try
    {
      Signature localSignature = d;
      localSignature.update(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (SignatureException localSignatureException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localSignatureException);
      throw localXMLSignatureException;
    }
  }
  
  protected String b()
  {
    return d.getProvider().getName();
  }
  
  protected boolean b(byte[] paramArrayOfByte)
  {
    try
    {
      Object localObject1 = a;
      boolean bool = ((Log)localObject1).isDebugEnabled();
      if (bool)
      {
        localObject1 = a;
        localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        String str = "Called DSA.verify() on ";
        localObject2 = ((StringBuffer)localObject2).append(str);
        str = Base64.b(paramArrayOfByte);
        localObject2 = ((StringBuffer)localObject2).append(str);
        localObject2 = ((StringBuffer)localObject2).toString();
        ((Log)localObject1).debug(localObject2);
      }
      localObject1 = c(paramArrayOfByte);
      localObject2 = d;
      return ((Signature)localObject2).verify((byte[])localObject1);
    }
    catch (SignatureException localSignatureException)
    {
      localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject2).<init>("empty", localSignatureException);
      throw ((Throwable)localObject2);
    }
    catch (IOException localIOException)
    {
      Object localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject2).<init>("empty", localIOException);
      throw ((Throwable)localObject2);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/implementations/SignatureDSA.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */