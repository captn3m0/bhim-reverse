package org.apache.xml.security.algorithms;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.AlgorithmAlreadyRegisteredException;
import org.apache.xml.security.signature.XMLSignatureException;
import org.w3c.dom.Element;

public class SignatureAlgorithm
  extends Algorithm
{
  static Log a;
  static boolean b;
  static HashMap c;
  static ThreadLocal d;
  static ThreadLocal e;
  static ThreadLocal f;
  static ThreadLocal g;
  static Class i;
  protected SignatureAlgorithmSpi h = null;
  private String q;
  
  static
  {
    Object localObject = i;
    if (localObject == null)
    {
      localObject = b("org.apache.xml.security.algorithms.SignatureAlgorithm");
      i = (Class)localObject;
    }
    for (;;)
    {
      a = LogFactory.getLog(((Class)localObject).getName());
      b = false;
      c = null;
      localObject = new org/apache/xml/security/algorithms/SignatureAlgorithm$1;
      ((SignatureAlgorithm.1)localObject).<init>();
      d = (ThreadLocal)localObject;
      localObject = new org/apache/xml/security/algorithms/SignatureAlgorithm$2;
      ((SignatureAlgorithm.2)localObject).<init>();
      e = (ThreadLocal)localObject;
      localObject = new org/apache/xml/security/algorithms/SignatureAlgorithm$3;
      ((SignatureAlgorithm.3)localObject).<init>();
      f = (ThreadLocal)localObject;
      localObject = new org/apache/xml/security/algorithms/SignatureAlgorithm$4;
      ((SignatureAlgorithm.4)localObject).<init>();
      g = (ThreadLocal)localObject;
      return;
      localObject = i;
    }
  }
  
  public SignatureAlgorithm(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
    String str = g();
    q = str;
  }
  
  public static void a(String paramString1, String paramString2)
  {
    int j = 2;
    int k = 1;
    Object localObject1 = a;
    boolean bool = ((Log)localObject1).isDebugEnabled();
    Object localObject2;
    Object localObject3;
    if (bool)
    {
      localObject1 = a;
      localObject2 = new java/lang/StringBuffer;
      ((StringBuffer)localObject2).<init>();
      localObject2 = ((StringBuffer)localObject2).append("Try to register ").append(paramString1);
      localObject3 = " ";
      localObject2 = (String)localObject3 + paramString2;
      ((Log)localObject1).debug(localObject2);
    }
    localObject1 = g(paramString1);
    if (localObject1 != null)
    {
      localObject1 = ((Class)localObject1).getName();
      if (localObject1 != null)
      {
        int m = ((String)localObject1).length();
        if (m != 0)
        {
          localObject2 = new Object[j];
          localObject2[0] = paramString1;
          localObject2[k] = localObject1;
          localObject1 = new org/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;
          ((AlgorithmAlreadyRegisteredException)localObject1).<init>("algorithm.alreadyRegistered", (Object[])localObject2);
          throw ((Throwable)localObject1);
        }
      }
    }
    try
    {
      localObject1 = c;
      localObject2 = Class.forName(paramString2);
      ((HashMap)localObject1).put(paramString1, localObject2);
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      localObject2 = new Object[j];
      localObject2[0] = paramString1;
      localObject3 = localClassNotFoundException.getMessage();
      localObject2[k] = localObject3;
      localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject3).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject2, localClassNotFoundException);
      throw ((Throwable)localObject3);
    }
    catch (NullPointerException localNullPointerException)
    {
      localObject2 = new Object[j];
      localObject2[0] = paramString1;
      localObject3 = localNullPointerException.getMessage();
      localObject2[k] = localObject3;
      localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject3).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject2, localNullPointerException);
      throw ((Throwable)localObject3);
    }
  }
  
  private void a(boolean paramBoolean)
  {
    if (paramBoolean) {}
    for (SignatureAlgorithmSpi localSignatureAlgorithmSpi = d(q);; localSignatureAlgorithmSpi = e(q))
    {
      h = localSignatureAlgorithmSpi;
      localSignatureAlgorithmSpi = h;
      Element localElement = k;
      localSignatureAlgorithmSpi.a(localElement);
      return;
    }
  }
  
  static Class b(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  private static SignatureAlgorithmSpi d(String paramString)
  {
    Object localObject = (SignatureAlgorithmSpi)((Map)d.get()).get(paramString);
    if (localObject != null) {
      ((SignatureAlgorithmSpi)localObject).c();
    }
    for (;;)
    {
      return (SignatureAlgorithmSpi)localObject;
      SignatureAlgorithmSpi localSignatureAlgorithmSpi = f(paramString);
      ((Map)d.get()).put(paramString, localSignatureAlgorithmSpi);
      localObject = localSignatureAlgorithmSpi;
    }
  }
  
  private static SignatureAlgorithmSpi e(String paramString)
  {
    Object localObject = (SignatureAlgorithmSpi)((Map)e.get()).get(paramString);
    if (localObject != null) {
      ((SignatureAlgorithmSpi)localObject).c();
    }
    for (;;)
    {
      return (SignatureAlgorithmSpi)localObject;
      SignatureAlgorithmSpi localSignatureAlgorithmSpi = f(paramString);
      ((Map)e.get()).put(paramString, localSignatureAlgorithmSpi);
      localObject = localSignatureAlgorithmSpi;
    }
  }
  
  private static SignatureAlgorithmSpi f(String paramString)
  {
    int j = 2;
    int k = 1;
    try
    {
      Object localObject1 = g(paramString);
      localObject2 = a;
      boolean bool = ((Log)localObject2).isDebugEnabled();
      if (bool)
      {
        localObject2 = a;
        localObject3 = new java/lang/StringBuffer;
        ((StringBuffer)localObject3).<init>();
        String str = "Create URI \"";
        localObject3 = ((StringBuffer)localObject3).append(str);
        localObject3 = ((StringBuffer)localObject3).append(paramString);
        str = "\" class \"";
        localObject3 = ((StringBuffer)localObject3).append(str);
        localObject3 = ((StringBuffer)localObject3).append(localObject1);
        str = "\"";
        localObject3 = ((StringBuffer)localObject3).append(str);
        localObject3 = ((StringBuffer)localObject3).toString();
        ((Log)localObject2).debug(localObject3);
      }
      localObject1 = ((Class)localObject1).newInstance();
      return (SignatureAlgorithmSpi)localObject1;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      localObject2 = new Object[j];
      localObject2[0] = paramString;
      localObject3 = localIllegalAccessException.getMessage();
      localObject2[k] = localObject3;
      localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject3).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject2, localIllegalAccessException);
      throw ((Throwable)localObject3);
    }
    catch (InstantiationException localInstantiationException)
    {
      localObject2 = new Object[j];
      localObject2[0] = paramString;
      localObject3 = localInstantiationException.getMessage();
      localObject2[k] = localObject3;
      localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject3).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject2, localInstantiationException);
      throw ((Throwable)localObject3);
    }
    catch (NullPointerException localNullPointerException)
    {
      Object localObject2 = new Object[j];
      localObject2[0] = paramString;
      Object localObject3 = localNullPointerException.getMessage();
      localObject2[k] = localObject3;
      localObject3 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject3).<init>("algorithms.NoSuchAlgorithm", (Object[])localObject2, localNullPointerException);
      throw ((Throwable)localObject3);
    }
  }
  
  private static Class g(String paramString)
  {
    Object localObject = c;
    if (localObject == null) {}
    for (localObject = null;; localObject = (Class)c.get(paramString)) {
      return (Class)localObject;
    }
  }
  
  public static void h()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = i;
      if (localObject != null) {
        break label83;
      }
      localObject = b("org.apache.xml.security.algorithms.SignatureAlgorithm");
      i = (Class)localObject;
    }
    for (;;)
    {
      localObject = LogFactory.getLog(((Class)localObject).getName());
      a = (Log)localObject;
      localObject = a;
      String str = "Init() called";
      ((Log)localObject).debug(str);
      boolean bool = b;
      if (!bool)
      {
        localObject = new java/util/HashMap;
        int j = 10;
        ((HashMap)localObject).<init>(j);
        c = (HashMap)localObject;
        bool = true;
        b = bool;
      }
      return;
      label83:
      localObject = i;
    }
  }
  
  public void a(byte paramByte)
  {
    h.a(paramByte);
  }
  
  public void a(Key paramKey)
  {
    a(false);
    Object localObject1 = (Map)g.get();
    Object localObject2 = q;
    localObject2 = ((Map)localObject1).get(localObject2);
    if (localObject2 == paramKey) {}
    for (;;)
    {
      return;
      localObject2 = q;
      ((Map)localObject1).put(localObject2, paramKey);
      localObject1 = h;
      ((SignatureAlgorithmSpi)localObject1).a(paramKey);
    }
  }
  
  public void a(byte[] paramArrayOfByte)
  {
    h.a(paramArrayOfByte);
  }
  
  public void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    h.a(paramArrayOfByte, paramInt1, paramInt2);
  }
  
  public String b()
  {
    try
    {
      localObject1 = q;
      localObject1 = e((String)localObject1);
      localObject1 = ((SignatureAlgorithmSpi)localObject1).a();
    }
    catch (XMLSignatureException localXMLSignatureException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  public boolean b(byte[] paramArrayOfByte)
  {
    return h.b(paramArrayOfByte);
  }
  
  public String c()
  {
    try
    {
      localObject1 = q;
      localObject1 = e((String)localObject1);
      localObject1 = ((SignatureAlgorithmSpi)localObject1).b();
    }
    catch (XMLSignatureException localXMLSignatureException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  public String d()
  {
    return "http://www.w3.org/2000/09/xmldsig#";
  }
  
  public String e()
  {
    return "SignatureMethod";
  }
  
  public void f()
  {
    ((Map)g.get()).clear();
    ((Map)e.get()).clear();
  }
  
  public final String g()
  {
    return k.getAttributeNS(null, "Algorithm");
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/SignatureAlgorithm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */