package org.apache.xml.security.algorithms;

import org.apache.xml.security.utils.SignatureElementProxy;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class Algorithm
  extends SignatureElementProxy
{
  public Algorithm(Document paramDocument, String paramString)
  {
    super(paramDocument);
    a(paramString);
  }
  
  public Algorithm(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
  }
  
  public String a()
  {
    return k.getAttributeNS(null, "Algorithm");
  }
  
  protected void a(String paramString)
  {
    if (paramString != null)
    {
      Element localElement = k;
      String str = "Algorithm";
      localElement.setAttributeNS(null, str, paramString);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/algorithms/Algorithm.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */