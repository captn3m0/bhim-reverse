package org.apache.xml.security.c14n;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

public abstract class CanonicalizerSpi
{
  protected boolean a = false;
  
  public abstract void a(OutputStream paramOutputStream);
  
  public abstract byte[] a(Node paramNode);
  
  public abstract byte[] a(Node paramNode, String paramString);
  
  public byte[] a(byte[] paramArrayOfByte)
  {
    Object localObject = new java/io/ByteArrayInputStream;
    ((ByteArrayInputStream)localObject).<init>(paramArrayOfByte);
    InputSource localInputSource = new org/xml/sax/InputSource;
    localInputSource.<init>((InputStream)localObject);
    localObject = DocumentBuilderFactory.newInstance();
    ((DocumentBuilderFactory)localObject).setNamespaceAware(true);
    localObject = ((DocumentBuilderFactory)localObject).newDocumentBuilder().parse(localInputSource);
    return a((Node)localObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/CanonicalizerSpi.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */