package org.apache.xml.security.c14n;

import org.apache.xml.security.exceptions.XMLSecurityException;

public class InvalidCanonicalizerException
  extends XMLSecurityException
{
  public InvalidCanonicalizerException() {}
  
  public InvalidCanonicalizerException(String paramString, Object[] paramArrayOfObject)
  {
    super(paramString, paramArrayOfObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/InvalidCanonicalizerException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */