package org.apache.xml.security.c14n;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import org.apache.xml.security.exceptions.AlgorithmAlreadyRegisteredException;
import org.w3c.dom.Node;

public class Canonicalizer
{
  static boolean a = false;
  static Map b = null;
  protected CanonicalizerSpi c;
  
  private Canonicalizer(String paramString)
  {
    Object localObject = null;
    c = null;
    try
    {
      localObject = b(paramString);
      localObject = ((Class)localObject).newInstance();
      localObject = (CanonicalizerSpi)localObject;
      c = ((CanonicalizerSpi)localObject);
      localObject = c;
      boolean bool = true;
      a = bool;
      return;
    }
    catch (Exception localException)
    {
      Object[] arrayOfObject = new Object[i];
      arrayOfObject[0] = paramString;
      InvalidCanonicalizerException localInvalidCanonicalizerException = new org/apache/xml/security/c14n/InvalidCanonicalizerException;
      localInvalidCanonicalizerException.<init>("signature.Canonicalizer.UnknownCanonicalizer", arrayOfObject);
      throw localInvalidCanonicalizerException;
    }
  }
  
  public static final Canonicalizer a(String paramString)
  {
    Canonicalizer localCanonicalizer = new org/apache/xml/security/c14n/Canonicalizer;
    localCanonicalizer.<init>(paramString);
    return localCanonicalizer;
  }
  
  public static void a()
  {
    boolean bool = a;
    if (!bool)
    {
      HashMap localHashMap = new java/util/HashMap;
      int i = 10;
      localHashMap.<init>(i);
      b = localHashMap;
      bool = true;
      a = bool;
    }
  }
  
  public static void a(String paramString1, String paramString2)
  {
    Object localObject1 = b(paramString1);
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = new Object[2];
      localObject2[0] = paramString1;
      localObject2[1] = localObject1;
      localObject1 = new org/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException;
      ((AlgorithmAlreadyRegisteredException)localObject1).<init>("algorithm.alreadyRegistered", (Object[])localObject2);
      throw ((Throwable)localObject1);
    }
    try
    {
      localObject1 = b;
      localObject2 = Class.forName(paramString2);
      ((Map)localObject1).put(paramString1, localObject2);
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      RuntimeException localRuntimeException = new java/lang/RuntimeException;
      localRuntimeException.<init>("c14n class not found");
      throw localRuntimeException;
    }
  }
  
  private static Class b(String paramString)
  {
    return (Class)b.get(paramString);
  }
  
  public void a(OutputStream paramOutputStream)
  {
    c.a(paramOutputStream);
  }
  
  public byte[] a(Node paramNode)
  {
    return c.a(paramNode);
  }
  
  public byte[] a(Node paramNode, String paramString)
  {
    return c.a(paramNode, paramString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/Canonicalizer.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */