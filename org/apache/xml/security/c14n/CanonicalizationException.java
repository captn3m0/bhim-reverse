package org.apache.xml.security.c14n;

import org.apache.xml.security.exceptions.XMLSecurityException;

public class CanonicalizationException
  extends XMLSecurityException
{
  public CanonicalizationException() {}
  
  public CanonicalizationException(String paramString)
  {
    super(paramString);
  }
  
  public CanonicalizationException(String paramString, Exception paramException)
  {
    super(paramString, paramException);
  }
  
  public CanonicalizationException(String paramString, Object[] paramArrayOfObject)
  {
    super(paramString, paramArrayOfObject);
  }
  
  public CanonicalizationException(String paramString, Object[] paramArrayOfObject, Exception paramException)
  {
    super(paramString, paramArrayOfObject, paramException);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/CanonicalizationException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */