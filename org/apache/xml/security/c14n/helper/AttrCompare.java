package org.apache.xml.security.c14n.helper;

import java.io.Serializable;
import java.util.Comparator;
import org.w3c.dom.Attr;

public class AttrCompare
  implements Serializable, Comparator
{
  public int compare(Object paramObject1, Object paramObject2)
  {
    int i = 1;
    int j = -1;
    paramObject1 = (Attr)paramObject1;
    paramObject2 = (Attr)paramObject2;
    String str1 = ((Attr)paramObject1).getNamespaceURI();
    String str2 = ((Attr)paramObject2).getNamespaceURI();
    String str3 = "http://www.w3.org/2000/xmlns/";
    boolean bool1 = str3.equals(str1);
    String str4 = "http://www.w3.org/2000/xmlns/";
    boolean bool2 = str4.equals(str2);
    String str5;
    String str6;
    if (bool1) {
      if (bool2)
      {
        str5 = ((Attr)paramObject1).getLocalName();
        str6 = ((Attr)paramObject2).getLocalName();
        str1 = "xmlns";
        boolean bool3 = str1.equals(str5);
        if (bool3) {
          str5 = "";
        }
        str1 = "xmlns";
        bool3 = str1.equals(str6);
        if (bool3) {
          str6 = "";
        }
        j = str5.compareTo(str6);
      }
    }
    for (;;)
    {
      return j;
      if (bool2)
      {
        j = i;
      }
      else if (str1 == null)
      {
        if (str2 == null)
        {
          str5 = ((Attr)paramObject1).getName();
          str6 = ((Attr)paramObject2).getName();
          j = str5.compareTo(str6);
        }
      }
      else if (str2 == null)
      {
        j = i;
      }
      else
      {
        j = str1.compareTo(str2);
        if (j == 0)
        {
          str5 = ((Attr)paramObject1).getLocalName();
          str6 = ((Attr)paramObject2).getLocalName();
          j = str5.compareTo(str6);
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/helper/AttrCompare.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */