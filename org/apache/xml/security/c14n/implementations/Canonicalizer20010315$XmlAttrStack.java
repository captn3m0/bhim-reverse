package org.apache.xml.security.c14n.implementations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Attr;

class Canonicalizer20010315$XmlAttrStack
{
  int a = 0;
  int b = 0;
  Canonicalizer20010315.XmlAttrStack.XmlsStackElement c;
  List d;
  
  Canonicalizer20010315$XmlAttrStack()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    d = localArrayList;
  }
  
  void a(int paramInt)
  {
    a = paramInt;
    int i = a;
    int j = -1;
    if (i == j) {
      return;
    }
    i = 0;
    Object localObject = null;
    c = null;
    for (;;)
    {
      i = b;
      j = a;
      if (i < j) {
        break;
      }
      localObject = d;
      List localList = d;
      j = localList.size() + -1;
      ((List)localObject).remove(j);
      localObject = d;
      i = ((List)localObject).size();
      if (i == 0)
      {
        i = 0;
        localObject = null;
        b = 0;
        break;
      }
      localObject = d;
      localList = d;
      j = localList.size() + -1;
      localObject = (Canonicalizer20010315.XmlAttrStack.XmlsStackElement)((List)localObject).get(j);
      i = a;
      b = i;
    }
  }
  
  void a(Collection paramCollection)
  {
    int i = 1;
    int j = d.size();
    int m = j + -1;
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = new org/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack$XmlsStackElement;
      ((Canonicalizer20010315.XmlAttrStack.XmlsStackElement)localObject1).<init>();
      c = ((Canonicalizer20010315.XmlAttrStack.XmlsStackElement)localObject1);
      localObject1 = c;
      n = a;
      a = n;
      j = a;
      b = j;
      localObject1 = d;
      localObject2 = c;
      ((List)localObject1).add(localObject2);
    }
    int n = 0;
    Object localObject2 = null;
    j = -1;
    if (m == j) {
      j = i;
    }
    for (;;)
    {
      if (j != 0)
      {
        localObject1 = c.c;
        paramCollection.addAll((Collection)localObject1);
        localObject1 = c;
        b = i;
      }
      for (;;)
      {
        return;
        localObject1 = (Canonicalizer20010315.XmlAttrStack.XmlsStackElement)d.get(m);
        boolean bool2 = b;
        if (!bool2) {
          break label355;
        }
        j = a + 1;
        int i1 = a;
        if (j != i1) {
          break label355;
        }
        j = i;
        break;
        localObject2 = new java/util/HashMap;
        ((HashMap)localObject2).<init>();
        while (m >= 0)
        {
          localObject1 = d.get(m)).c;
          Iterator localIterator = ((List)localObject1).iterator();
          for (;;)
          {
            boolean bool1 = localIterator.hasNext();
            if (!bool1) {
              break;
            }
            localObject1 = (Attr)localIterator.next();
            String str = ((Attr)localObject1).getName();
            boolean bool3 = ((Map)localObject2).containsKey(str);
            if (!bool3)
            {
              str = ((Attr)localObject1).getName();
              ((Map)localObject2).put(str, localObject1);
            }
          }
          k = m + -1;
          m = k;
        }
        c.b = i;
        localObject1 = ((Map)localObject2).values();
        paramCollection.addAll((Collection)localObject1);
      }
      label355:
      int k = 0;
      localObject1 = null;
    }
  }
  
  void a(Attr paramAttr)
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = new org/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack$XmlsStackElement;
      ((Canonicalizer20010315.XmlAttrStack.XmlsStackElement)localObject).<init>();
      c = ((Canonicalizer20010315.XmlAttrStack.XmlsStackElement)localObject);
      localObject = c;
      int i = a;
      a = i;
      localObject = d;
      Canonicalizer20010315.XmlAttrStack.XmlsStackElement localXmlsStackElement = c;
      ((List)localObject).add(localXmlsStackElement);
      int j = a;
      b = j;
    }
    c.c.add(paramAttr);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */