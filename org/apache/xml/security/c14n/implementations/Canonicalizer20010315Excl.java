package org.apache.xml.security.c14n.implementations;

import java.util.Iterator;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.helper.AttrCompare;
import org.apache.xml.security.c14n.helper.C14nHelper;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.params.InclusiveNamespaces;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public abstract class Canonicalizer20010315Excl
  extends CanonicalizerBase
{
  TreeSet b;
  final SortedSet c;
  
  public Canonicalizer20010315Excl(boolean paramBoolean)
  {
    super(paramBoolean);
    TreeSet localTreeSet = new java/util/TreeSet;
    localTreeSet.<init>();
    b = localTreeSet;
    localTreeSet = new java/util/TreeSet;
    AttrCompare localAttrCompare = g;
    localTreeSet.<init>(localAttrCompare);
    c = localTreeSet;
  }
  
  Iterator a(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    SortedSet localSortedSet = c;
    localSortedSet.clear();
    int i = 0;
    Object localObject1 = null;
    boolean bool2 = paramElement.hasAttributes();
    Object localObject2;
    int k;
    if (bool2)
    {
      localObject2 = paramElement.getAttributes();
      i = ((NamedNodeMap)localObject2).getLength();
      k = i;
    }
    for (Object localObject3 = localObject2;; localObject3 = null)
    {
      localObject1 = (SortedSet)b.clone();
      int m = 0;
      if (m < k)
      {
        localObject2 = (Attr)((NamedNodeMap)localObject3).item(m);
        String str1 = "http://www.w3.org/2000/xmlns/";
        String str2 = ((Attr)localObject2).getNamespaceURI();
        boolean bool3 = str1.equals(str2);
        boolean bool4;
        if (!bool3)
        {
          str1 = ((Attr)localObject2).getPrefix();
          if (str1 != null)
          {
            str2 = "xml";
            bool4 = str1.equals(str2);
            if (!bool4)
            {
              str2 = "xmlns";
              bool4 = str1.equals(str2);
              if (!bool4) {
                ((SortedSet)localObject1).add(str1);
              }
            }
          }
          localSortedSet.add(localObject2);
        }
        label254:
        do
        {
          boolean bool5;
          do
          {
            do
            {
              int j = m + 1;
              m = j;
              break;
              str1 = ((Attr)localObject2).getLocalName();
              str2 = ((Attr)localObject2).getNodeValue();
              String str3 = "xml";
              bool5 = str3.equals(str1);
              if (!bool5) {
                break label254;
              }
              str3 = "http://www.w3.org/XML/1998/namespace";
              bool5 = str3.equals(str2);
            } while (bool5);
            bool5 = paramNameSpaceSymbTable.a(str1, str2, (Attr)localObject2);
          } while (!bool5);
          bool4 = C14nHelper.a(str2);
        } while (!bool4);
        localObject1 = new Object[3];
        str4 = paramElement.getTagName();
        localObject1[0] = str4;
        localObject1[1] = str1;
        localObject2 = ((Attr)localObject2).getNodeValue();
        localObject1[2] = localObject2;
        localObject2 = new org/apache/xml/security/c14n/CanonicalizationException;
        ((CanonicalizationException)localObject2).<init>("c14n.Canonicalizer.RelativeNamespace", (Object[])localObject1);
        throw ((Throwable)localObject2);
      }
      localObject2 = paramElement.getNamespaceURI();
      if (localObject2 != null)
      {
        localObject2 = paramElement.getPrefix();
        if (localObject2 != null)
        {
          k = ((String)localObject2).length();
          if (k != 0) {
            break label383;
          }
        }
      }
      for (localObject2 = "xmlns";; localObject2 = "xmlns")
      {
        label383:
        ((SortedSet)localObject1).add(localObject2);
        localObject2 = ((SortedSet)localObject1).iterator();
        for (;;)
        {
          boolean bool1 = ((Iterator)localObject2).hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = (String)((Iterator)localObject2).next();
          localObject1 = paramNameSpaceSymbTable.a((String)localObject1);
          if (localObject1 != null) {
            localSortedSet.add(localObject1);
          }
        }
      }
      return localSortedSet.iterator();
      k = 0;
      String str4 = null;
    }
  }
  
  void a(XMLSignatureInput paramXMLSignatureInput)
  {
    boolean bool = paramXMLSignatureInput.a();
    if (bool)
    {
      localObject = b;
      bool = ((TreeSet)localObject).isEmpty();
      if (!bool) {}
    }
    else
    {
      return;
    }
    Object localObject = paramXMLSignatureInput.m();
    if (localObject != null) {}
    for (localObject = XMLUtils.b(paramXMLSignatureInput.m());; localObject = XMLUtils.a(paramXMLSignatureInput.b()))
    {
      XMLUtils.a((Document)localObject);
      break;
    }
  }
  
  public byte[] a(XMLSignatureInput paramXMLSignatureInput, String paramString)
  {
    TreeSet localTreeSet = (TreeSet)InclusiveNamespaces.a(paramString);
    b = localTreeSet;
    return super.b(paramXMLSignatureInput);
  }
  
  public byte[] a(Node paramNode)
  {
    return a(paramNode, "", null);
  }
  
  public byte[] a(Node paramNode, String paramString)
  {
    return a(paramNode, paramString, null);
  }
  
  public byte[] a(Node paramNode1, String paramString, Node paramNode2)
  {
    TreeSet localTreeSet = (TreeSet)InclusiveNamespaces.a(paramString);
    b = localTreeSet;
    return super.a(paramNode1, paramNode2);
  }
  
  final Iterator b(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    SortedSet localSortedSet = c;
    localSortedSet.clear();
    int i = 0;
    Object localObject1 = null;
    int j = 0;
    Object localObject2 = null;
    boolean bool2 = paramElement.hasAttributes();
    Object localObject3;
    if (bool2)
    {
      localObject1 = paramElement.getAttributes();
      j = ((NamedNodeMap)localObject1).getLength();
      localObject3 = localObject1;
      i = j;
    }
    for (;;)
    {
      j = 0;
      localObject2 = null;
      int k = paramNameSpaceSymbTable.f();
      k = a(paramElement, k);
      int m = 1;
      int n;
      if (k == m)
      {
        k = 1;
        n = k;
        if (n == 0) {
          break label801;
        }
        localObject2 = (Set)b.clone();
      }
      for (Object localObject4 = localObject2;; localObject4 = null)
      {
        j = 0;
        localObject2 = null;
        m = 0;
        label129:
        if (m < i)
        {
          localObject2 = (Attr)((NamedNodeMap)localObject3).item(m);
          String str1 = "http://www.w3.org/2000/xmlns/";
          String str2 = ((Attr)localObject2).getNamespaceURI();
          boolean bool3 = str1.equals(str2);
          if (!bool3)
          {
            bool3 = c((Node)localObject2);
            if (bool3) {}
          }
          boolean bool4;
          label346:
          do
          {
            boolean bool5;
            do
            {
              for (;;)
              {
                j = m + 1;
                m = j;
                break label129;
                k = 0;
                localObject4 = null;
                n = 0;
                break;
                if (n != 0)
                {
                  str1 = ((Attr)localObject2).getPrefix();
                  if (str1 != null)
                  {
                    str2 = "xml";
                    bool4 = str1.equals(str2);
                    if (!bool4)
                    {
                      str2 = "xmlns";
                      bool4 = str1.equals(str2);
                      if (!bool4) {
                        ((Set)localObject4).add(str1);
                      }
                    }
                  }
                  localSortedSet.add(localObject2);
                  continue;
                  str1 = ((Attr)localObject2).getLocalName();
                  if (n == 0) {
                    break label346;
                  }
                  bool4 = c((Node)localObject2);
                  if (bool4) {
                    break label346;
                  }
                  str2 = "xmlns";
                  bool4 = str2.equals(str1);
                  if (bool4) {
                    break label346;
                  }
                  paramNameSpaceSymbTable.d(str1);
                }
              }
              str2 = ((Attr)localObject2).getNodeValue();
              if (n == 0)
              {
                bool5 = c((Node)localObject2);
                if (bool5)
                {
                  Object localObject5 = b;
                  bool5 = ((TreeSet)localObject5).contains(str1);
                  if (bool5)
                  {
                    bool5 = paramNameSpaceSymbTable.e(str1);
                    if (!bool5)
                    {
                      localObject5 = paramNameSpaceSymbTable.b(str1, str2, (Attr)localObject2);
                      if (localObject5 != null)
                      {
                        localSortedSet.add(localObject5);
                        bool5 = C14nHelper.a((Attr)localObject2);
                        if (bool5)
                        {
                          localObject1 = new Object[3];
                          localObject4 = paramElement.getTagName();
                          localObject1[0] = localObject4;
                          localObject1[1] = str1;
                          localObject2 = ((Attr)localObject2).getNodeValue();
                          localObject1[2] = localObject2;
                          localObject2 = new org/apache/xml/security/c14n/CanonicalizationException;
                          ((CanonicalizationException)localObject2).<init>("c14n.Canonicalizer.RelativeNamespace", (Object[])localObject1);
                          throw ((Throwable)localObject2);
                        }
                      }
                    }
                  }
                }
              }
              bool5 = paramNameSpaceSymbTable.a(str1, str2, (Attr)localObject2);
            } while (!bool5);
            bool4 = C14nHelper.a(str2);
          } while (!bool4);
          localObject1 = new Object[3];
          localObject4 = paramElement.getTagName();
          localObject1[0] = localObject4;
          localObject1[1] = str1;
          localObject2 = ((Attr)localObject2).getNodeValue();
          localObject1[2] = localObject2;
          localObject2 = new org/apache/xml/security/c14n/CanonicalizationException;
          ((CanonicalizationException)localObject2).<init>("c14n.Canonicalizer.RelativeNamespace", (Object[])localObject1);
          throw ((Throwable)localObject2);
        }
        if (n != 0)
        {
          localObject1 = "xmlns";
          localObject2 = paramElement.getAttributeNodeNS("http://www.w3.org/2000/xmlns/", (String)localObject1);
          boolean bool1;
          if (localObject2 != null)
          {
            bool1 = c((Node)localObject2);
            if (!bool1)
            {
              localObject2 = "xmlns";
              localObject1 = "";
              localObject3 = i;
              paramNameSpaceSymbTable.a((String)localObject2, (String)localObject1, (Attr)localObject3);
            }
          }
          localObject2 = paramElement.getNamespaceURI();
          if (localObject2 != null)
          {
            localObject2 = paramElement.getPrefix();
            if (localObject2 != null)
            {
              i = ((String)localObject2).length();
              if (i != 0) {}
            }
            else
            {
              localObject2 = "xmlns";
              ((Set)localObject4).add(localObject2);
            }
          }
          for (;;)
          {
            localObject1 = ((Set)localObject4).iterator();
            for (;;)
            {
              bool1 = ((Iterator)localObject1).hasNext();
              if (!bool1) {
                break;
              }
              localObject2 = (String)((Iterator)localObject1).next();
              localObject2 = paramNameSpaceSymbTable.a((String)localObject2);
              if (localObject2 != null) {
                localSortedSet.add(localObject2);
              }
            }
            ((Set)localObject4).add(localObject2);
            continue;
            localObject2 = "xmlns";
            ((Set)localObject4).add(localObject2);
          }
        }
        return localSortedSet.iterator();
        label801:
        k = 0;
      }
      bool2 = false;
      localObject3 = null;
      i = 0;
      localObject1 = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/Canonicalizer20010315Excl.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */