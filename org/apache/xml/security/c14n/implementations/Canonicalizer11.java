package org.apache.xml.security.c14n.implementations;

import java.net.URI;
import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.helper.AttrCompare;
import org.apache.xml.security.c14n.helper.C14nHelper;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public abstract class Canonicalizer11
  extends CanonicalizerBase
{
  static Log d;
  static Class f;
  boolean b = true;
  final SortedSet c;
  Canonicalizer11.XmlAttrStack e;
  
  static
  {
    Class localClass = f;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.c14n.implementations.Canonicalizer11");
      f = localClass;
    }
    for (;;)
    {
      d = LogFactory.getLog(localClass.getName());
      return;
      localClass = f;
    }
  }
  
  public Canonicalizer11(boolean paramBoolean)
  {
    super(paramBoolean);
    Object localObject = new java/util/TreeSet;
    AttrCompare localAttrCompare = g;
    ((TreeSet)localObject).<init>(localAttrCompare);
    c = ((SortedSet)localObject);
    localObject = new org/apache/xml/security/c14n/implementations/Canonicalizer11$XmlAttrStack;
    ((Canonicalizer11.XmlAttrStack)localObject).<init>();
    e = ((Canonicalizer11.XmlAttrStack)localObject);
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  static String a(String paramString1, String paramString2)
  {
    return b(paramString1, paramString2);
  }
  
  private static void a(String paramString1, String paramString2, String paramString3)
  {
    Log localLog = d;
    boolean bool = localLog.isDebugEnabled();
    Object localObject;
    String str;
    if (bool)
    {
      localLog = d;
      localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>();
      localObject = ((StringBuffer)localObject).append(" ").append(paramString1);
      str = ":   ";
      localObject = str + paramString2;
      localLog.debug(localObject);
      int i = paramString2.length();
      if (i != 0) {
        break label126;
      }
      localLog = d;
      localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>();
      str = "\t\t\t\t";
      localObject = str + paramString3;
      localLog.debug(localObject);
    }
    for (;;)
    {
      return;
      label126:
      localLog = d;
      localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>();
      str = "\t\t\t";
      localObject = str + paramString3;
      localLog.debug(localObject);
    }
  }
  
  private static String b(String paramString)
  {
    int i = 2;
    int j = 1;
    String str1 = null;
    int k = -1;
    int m = 47;
    Object localObject = d;
    String str2 = "STEP   OUTPUT BUFFER\t\tINPUT BUFFER";
    ((Log)localObject).debug(str2);
    for (;;)
    {
      localObject = "//";
      int n = paramString.indexOf((String)localObject);
      if (n <= k) {
        break;
      }
      localObject = "//";
      str2 = "/";
      paramString = paramString.replaceAll((String)localObject, str2);
    }
    localObject = new java/lang/StringBuffer;
    ((StringBuffer)localObject).<init>();
    int i1 = paramString.charAt(0);
    if (i1 == m)
    {
      str2 = "/";
      ((StringBuffer)localObject).append(str2);
      paramString = paramString.substring(j);
    }
    str2 = "1 ";
    String str3 = ((StringBuffer)localObject).toString();
    a(str2, str3, paramString);
    int i7;
    for (;;)
    {
      i1 = paramString.length();
      if (i1 == 0) {
        break label1043;
      }
      str2 = "./";
      boolean bool1 = paramString.startsWith(str2);
      if (bool1)
      {
        paramString = paramString.substring(i);
        str2 = "2A";
        str3 = ((StringBuffer)localObject).toString();
        a(str2, str3, paramString);
      }
      else
      {
        str2 = "../";
        bool1 = paramString.startsWith(str2);
        if (bool1)
        {
          paramString = paramString.substring(3);
          str2 = ((StringBuffer)localObject).toString();
          str3 = "/";
          bool1 = str2.equals(str3);
          if (!bool1)
          {
            str2 = "../";
            ((StringBuffer)localObject).append(str2);
          }
          str2 = "2A";
          str3 = ((StringBuffer)localObject).toString();
          a(str2, str3, paramString);
        }
        else
        {
          str2 = "/./";
          bool1 = paramString.startsWith(str2);
          if (bool1)
          {
            paramString = paramString.substring(i);
            str2 = "2B";
            str3 = ((StringBuffer)localObject).toString();
            a(str2, str3, paramString);
          }
          else
          {
            str2 = "/.";
            bool1 = paramString.equals(str2);
            if (bool1)
            {
              paramString = paramString.replaceFirst("/.", "/");
              str2 = "2B";
              str3 = ((StringBuffer)localObject).toString();
              a(str2, str3, paramString);
            }
            else
            {
              str2 = "/../";
              bool1 = paramString.startsWith(str2);
              if (bool1)
              {
                paramString = paramString.substring(3);
                int i2 = ((StringBuffer)localObject).length();
                if (i2 == 0)
                {
                  str2 = "/";
                  ((StringBuffer)localObject).append(str2);
                }
                for (;;)
                {
                  str2 = "2C";
                  str3 = ((StringBuffer)localObject).toString();
                  a(str2, str3, paramString);
                  break;
                  str2 = ((StringBuffer)localObject).toString();
                  str3 = "../";
                  boolean bool2 = str2.endsWith(str3);
                  if (bool2)
                  {
                    str2 = "..";
                    ((StringBuffer)localObject).append(str2);
                  }
                  else
                  {
                    str2 = ((StringBuffer)localObject).toString();
                    str3 = "..";
                    bool2 = str2.endsWith(str3);
                    if (bool2)
                    {
                      str2 = "/..";
                      ((StringBuffer)localObject).append(str2);
                    }
                    else
                    {
                      str2 = "/";
                      int i3 = ((StringBuffer)localObject).lastIndexOf(str2);
                      if (i3 == k)
                      {
                        localObject = new java/lang/StringBuffer;
                        ((StringBuffer)localObject).<init>();
                        i3 = paramString.charAt(0);
                        if (i3 == m) {
                          paramString = paramString.substring(j);
                        }
                      }
                      else
                      {
                        i7 = ((StringBuffer)localObject).length();
                        localObject = ((StringBuffer)localObject).delete(i3, i7);
                      }
                    }
                  }
                }
              }
              str2 = "/..";
              boolean bool3 = paramString.equals(str2);
              if (bool3)
              {
                str2 = "/..";
                str3 = "/";
                paramString = paramString.replaceFirst(str2, str3);
                int i4 = ((StringBuffer)localObject).length();
                if (i4 == 0)
                {
                  str2 = "/";
                  ((StringBuffer)localObject).append(str2);
                }
                for (;;)
                {
                  str2 = "2C";
                  str3 = ((StringBuffer)localObject).toString();
                  a(str2, str3, paramString);
                  break;
                  str2 = ((StringBuffer)localObject).toString();
                  str3 = "../";
                  boolean bool4 = str2.endsWith(str3);
                  if (bool4)
                  {
                    str2 = "..";
                    ((StringBuffer)localObject).append(str2);
                  }
                  else
                  {
                    str2 = ((StringBuffer)localObject).toString();
                    str3 = "..";
                    bool4 = str2.endsWith(str3);
                    if (bool4)
                    {
                      str2 = "/..";
                      ((StringBuffer)localObject).append(str2);
                    }
                    else
                    {
                      str2 = "/";
                      int i5 = ((StringBuffer)localObject).lastIndexOf(str2);
                      if (i5 == k)
                      {
                        localObject = new java/lang/StringBuffer;
                        ((StringBuffer)localObject).<init>();
                        i5 = paramString.charAt(0);
                        if (i5 == m) {
                          paramString = paramString.substring(j);
                        }
                      }
                      else
                      {
                        i7 = ((StringBuffer)localObject).length();
                        localObject = ((StringBuffer)localObject).delete(i5, i7);
                      }
                    }
                  }
                }
              }
              str2 = ".";
              boolean bool5 = paramString.equals(str2);
              if (bool5)
              {
                paramString = "";
                str2 = "2D";
                str3 = ((StringBuffer)localObject).toString();
                a(str2, str3, paramString);
              }
              else
              {
                str2 = "..";
                bool5 = paramString.equals(str2);
                if (!bool5) {
                  break;
                }
                str2 = ((StringBuffer)localObject).toString();
                str3 = "/";
                bool5 = str2.equals(str3);
                if (!bool5)
                {
                  str2 = "..";
                  ((StringBuffer)localObject).append(str2);
                }
                paramString = "";
                str2 = "2D";
                str3 = ((StringBuffer)localObject).toString();
                a(str2, str3, paramString);
              }
            }
          }
        }
      }
    }
    int i6 = paramString.indexOf(m);
    if (i6 == 0)
    {
      i7 = paramString.indexOf(m, j);
      label962:
      if (i7 != k) {
        break label1023;
      }
      str2 = paramString.substring(i6);
    }
    for (paramString = "";; paramString = paramString.substring(i7))
    {
      ((StringBuffer)localObject).append(str2);
      str2 = "2E";
      str3 = ((StringBuffer)localObject).toString();
      a(str2, str3, paramString);
      break;
      i7 = i6;
      i6 = 0;
      str2 = null;
      break label962;
      label1023:
      str2 = paramString.substring(i6, i7);
    }
    label1043:
    str2 = ((StringBuffer)localObject).toString();
    str1 = "..";
    boolean bool6 = str2.endsWith(str1);
    if (bool6)
    {
      ((StringBuffer)localObject).append("/");
      str2 = "3 ";
      str1 = ((StringBuffer)localObject).toString();
      a(str2, str1, paramString);
    }
    return ((StringBuffer)localObject).toString();
  }
  
  private static String b(String paramString1, String paramString2)
  {
    Object localObject1 = "";
    boolean bool1;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    Object localObject5;
    if (paramString1 != null)
    {
      localObject1 = "..";
      bool1 = paramString1.endsWith((String)localObject1);
      if (bool1)
      {
        localObject1 = new java/lang/StringBuffer;
        ((StringBuffer)localObject1).<init>();
        localObject1 = ((StringBuffer)localObject1).append(paramString1);
        localObject2 = "/";
        localObject1 = ((StringBuffer)localObject1).append((String)localObject2);
        paramString1 = ((StringBuffer)localObject1).toString();
      }
      localObject3 = new java/net/URI;
      ((URI)localObject3).<init>(paramString1);
      localObject4 = ((URI)localObject3).getScheme();
      localObject5 = ((URI)localObject3).getAuthority();
      localObject2 = ((URI)localObject3).getPath();
      localObject1 = ((URI)localObject3).getQuery();
      ((URI)localObject3).getFragment();
    }
    for (;;)
    {
      Object localObject6 = new java/net/URI;
      ((URI)localObject6).<init>(paramString2);
      String str = ((URI)localObject6).getScheme();
      Object localObject7 = ((URI)localObject6).getAuthority();
      localObject3 = ((URI)localObject6).getPath();
      localObject6 = ((URI)localObject6).getQuery();
      if (str != null)
      {
        boolean bool2 = str.equals(localObject4);
        if (bool2) {
          str = null;
        }
      }
      if (str != null)
      {
        localObject4 = b((String)localObject3);
        localObject3 = localObject6;
        localObject5 = localObject7;
        localObject2 = str;
      }
      for (;;)
      {
        localObject1 = new java/net/URI;
        ((URI)localObject1).<init>((String)localObject2, (String)localObject5, (String)localObject4, (String)localObject3, null);
        return ((URI)localObject1).toString();
        if (localObject7 == null) {
          break;
        }
        localObject2 = b((String)localObject3);
        localObject5 = localObject7;
        localObject3 = localObject6;
        Object localObject8 = localObject2;
        localObject2 = localObject4;
        localObject4 = localObject8;
      }
      int j = ((String)localObject3).length();
      if (j == 0) {
        if (localObject6 == null) {}
      }
      for (localObject1 = localObject6;; localObject1 = localObject6)
      {
        localObject6 = localObject1;
        break;
        localObject1 = "/";
        bool1 = ((String)localObject3).startsWith((String)localObject1);
        if (!bool1) {
          break label291;
        }
        localObject1 = b((String)localObject3);
        localObject2 = localObject1;
      }
      label291:
      if (localObject5 != null)
      {
        i = ((String)localObject2).length();
        if (i == 0)
        {
          localObject1 = new java/lang/StringBuffer;
          ((StringBuffer)localObject1).<init>();
          localObject2 = "/";
          localObject1 = (String)localObject2 + (String)localObject3;
        }
      }
      for (;;)
      {
        localObject1 = b((String)localObject1);
        break;
        i = ((String)localObject2).lastIndexOf('/');
        j = -1;
        if (i == j)
        {
          localObject1 = localObject3;
        }
        else
        {
          localObject7 = new java/lang/StringBuffer;
          ((StringBuffer)localObject7).<init>();
          str = null;
          i += 1;
          localObject1 = ((String)localObject2).substring(0, i);
          localObject1 = (String)localObject1 + (String)localObject3;
        }
      }
      localObject2 = localObject1;
      localObject5 = null;
      localObject4 = null;
      int i = 0;
      localObject1 = null;
    }
  }
  
  Iterator a(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    boolean bool1 = paramElement.hasAttributes();
    if (!bool1)
    {
      bool1 = b;
      if (!bool1) {
        bool1 = false;
      }
    }
    Object localObject2;
    for (Object localObject1 = null;; localObject1 = ((SortedSet)localObject2).iterator())
    {
      return (Iterator)localObject1;
      localObject2 = c;
      ((SortedSet)localObject2).clear();
      NamedNodeMap localNamedNodeMap = paramElement.getAttributes();
      int j = localNamedNodeMap.getLength();
      int k = 0;
      Object[] arrayOfObject = null;
      if (k < j)
      {
        localObject1 = (Attr)localNamedNodeMap.item(k);
        String str1 = ((Attr)localObject1).getNamespaceURI();
        Object localObject3 = "http://www.w3.org/2000/xmlns/";
        boolean bool3 = ((String)localObject3).equals(str1);
        if (!bool3) {
          ((SortedSet)localObject2).add(localObject1);
        }
        label190:
        boolean bool5;
        do
        {
          do
          {
            boolean bool4;
            do
            {
              int i = k + 1;
              k = i;
              break;
              str1 = ((Attr)localObject1).getLocalName();
              localObject3 = ((Attr)localObject1).getValue();
              String str2 = "xml";
              bool4 = str2.equals(str1);
              if (!bool4) {
                break label190;
              }
              str2 = "http://www.w3.org/XML/1998/namespace";
              bool4 = str2.equals(localObject3);
            } while (bool4);
            localObject3 = paramNameSpaceSymbTable.b(str1, (String)localObject3, (Attr)localObject1);
          } while (localObject3 == null);
          ((SortedSet)localObject2).add(localObject3);
          bool5 = C14nHelper.a((Attr)localObject1);
        } while (!bool5);
        arrayOfObject = new Object[3];
        localObject2 = paramElement.getTagName();
        arrayOfObject[0] = localObject2;
        arrayOfObject[1] = str1;
        localObject1 = ((Attr)localObject1).getNodeValue();
        arrayOfObject[2] = localObject1;
        localObject1 = new org/apache/xml/security/c14n/CanonicalizationException;
        ((CanonicalizationException)localObject1).<init>("c14n.Canonicalizer.RelativeNamespace", arrayOfObject);
        throw ((Throwable)localObject1);
      }
      boolean bool2 = b;
      if (bool2)
      {
        paramNameSpaceSymbTable.a((Collection)localObject2);
        localObject1 = e;
        ((Canonicalizer11.XmlAttrStack)localObject1).a((Collection)localObject2);
        b = false;
      }
    }
  }
  
  void a(XMLSignatureInput paramXMLSignatureInput)
  {
    boolean bool = paramXMLSignatureInput.a();
    if (!bool) {
      return;
    }
    Object localObject = paramXMLSignatureInput.m();
    if (localObject != null) {}
    for (localObject = XMLUtils.b(paramXMLSignatureInput.m());; localObject = XMLUtils.a(paramXMLSignatureInput.b()))
    {
      XMLUtils.a((Document)localObject);
      break;
    }
  }
  
  public byte[] a(Node paramNode, String paramString)
  {
    CanonicalizationException localCanonicalizationException = new org/apache/xml/security/c14n/CanonicalizationException;
    localCanonicalizationException.<init>("c14n.Canonicalizer.UnsupportedOperation");
    throw localCanonicalizationException;
  }
  
  Iterator b(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    Object localObject1 = null;
    int i = 1;
    Object localObject2 = e;
    int j = paramNameSpaceSymbTable.f();
    ((Canonicalizer11.XmlAttrStack)localObject2).a(j);
    int k = paramNameSpaceSymbTable.f();
    k = a(paramElement, k);
    Object localObject3;
    int m;
    Object localObject4;
    int n;
    if (k == i)
    {
      j = i;
      boolean bool1 = paramElement.hasAttributes();
      if (!bool1) {
        break label625;
      }
      localObject3 = paramElement.getAttributes();
      m = ((NamedNodeMap)localObject3).getLength();
      localObject4 = localObject3;
      n = m;
    }
    for (;;)
    {
      SortedSet localSortedSet = c;
      localSortedSet.clear();
      int i1 = 0;
      label104:
      Object localObject7;
      if (i1 < n)
      {
        localObject2 = (Attr)((NamedNodeMap)localObject4).item(i1);
        Object localObject5 = ((Attr)localObject2).getNamespaceURI();
        Object localObject6 = "http://www.w3.org/2000/xmlns/";
        boolean bool3 = ((String)localObject6).equals(localObject5);
        if (!bool3)
        {
          localObject6 = "http://www.w3.org/XML/1998/namespace";
          boolean bool4 = ((String)localObject6).equals(localObject5);
          if (bool4)
          {
            localObject5 = ((Attr)localObject2).getLocalName();
            localObject6 = "id";
            bool4 = ((String)localObject5).equals(localObject6);
            if (bool4) {
              if (j != 0) {
                localSortedSet.add(localObject2);
              }
            }
          }
        }
        for (;;)
        {
          m = i1 + 1;
          i1 = m;
          break label104;
          j = 0;
          localObject7 = null;
          break;
          localObject5 = e;
          ((Canonicalizer11.XmlAttrStack)localObject5).a((Attr)localObject2);
          continue;
          if (j != 0)
          {
            localSortedSet.add(localObject2);
            continue;
            localObject5 = ((Attr)localObject2).getLocalName();
            localObject6 = ((Attr)localObject2).getValue();
            String str = "xml";
            boolean bool5 = str.equals(localObject5);
            if (bool5)
            {
              str = "http://www.w3.org/XML/1998/namespace";
              bool5 = str.equals(localObject6);
              if (bool5) {}
            }
            else
            {
              bool5 = c((Node)localObject2);
              if (bool5)
              {
                if (j == 0)
                {
                  bool5 = paramNameSpaceSymbTable.e((String)localObject5);
                  if (bool5) {}
                }
                else
                {
                  localObject6 = paramNameSpaceSymbTable.b((String)localObject5, (String)localObject6, (Attr)localObject2);
                  if (localObject6 != null)
                  {
                    localSortedSet.add(localObject6);
                    bool3 = C14nHelper.a((Attr)localObject2);
                    if (bool3)
                    {
                      localObject7 = new Object[3];
                      localObject3 = paramElement.getTagName();
                      localObject7[0] = localObject3;
                      localObject7[i] = localObject5;
                      localObject2 = ((Attr)localObject2).getNodeValue();
                      localObject7[2] = localObject2;
                      localObject2 = new org/apache/xml/security/c14n/CanonicalizationException;
                      ((CanonicalizationException)localObject2).<init>("c14n.Canonicalizer.RelativeNamespace", (Object[])localObject7);
                      throw ((Throwable)localObject2);
                    }
                  }
                }
              }
              else
              {
                if (j != 0)
                {
                  str = "xmlns";
                  bool5 = str.equals(localObject5);
                  if (!bool5)
                  {
                    paramNameSpaceSymbTable.c((String)localObject5);
                    continue;
                  }
                }
                paramNameSpaceSymbTable.a((String)localObject5, (String)localObject6, (Attr)localObject2);
              }
            }
          }
        }
      }
      if (j != 0)
      {
        localObject7 = "xmlns";
        localObject2 = paramElement.getAttributeNodeNS("http://www.w3.org/2000/xmlns/", (String)localObject7);
        if (localObject2 != null) {
          break label584;
        }
        localObject2 = "xmlns";
        localObject1 = paramNameSpaceSymbTable.a((String)localObject2);
      }
      for (;;)
      {
        if (localObject1 != null) {
          localSortedSet.add(localObject1);
        }
        localObject2 = e;
        ((Canonicalizer11.XmlAttrStack)localObject2).a(localSortedSet);
        paramNameSpaceSymbTable.a(localSortedSet);
        return localSortedSet.iterator();
        label584:
        boolean bool2 = c((Node)localObject2);
        if (!bool2)
        {
          localObject2 = "xmlns";
          localObject7 = "";
          Attr localAttr = i;
          localObject1 = paramNameSpaceSymbTable.b((String)localObject2, (String)localObject7, localAttr);
        }
      }
      label625:
      n = 0;
      localObject3 = null;
      localObject4 = null;
    }
  }
  
  void c(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    boolean bool1 = paramElement.hasAttributes();
    if (!bool1) {
      return;
    }
    e.a(-1);
    NamedNodeMap localNamedNodeMap = paramElement.getAttributes();
    int j = localNamedNodeMap.getLength();
    bool1 = false;
    Attr localAttr = null;
    int k = 0;
    label45:
    Object localObject;
    String str1;
    if (k < j)
    {
      localAttr = (Attr)localNamedNodeMap.item(k);
      localObject = "http://www.w3.org/2000/xmlns/";
      str1 = localAttr.getNamespaceURI();
      boolean bool2 = ((String)localObject).equals(str1);
      if (bool2) {
        break label146;
      }
      localObject = "http://www.w3.org/XML/1998/namespace";
      str1 = localAttr.getNamespaceURI();
      bool2 = ((String)localObject).equals(str1);
      if (!bool2)
      {
        localObject = e;
        ((Canonicalizer11.XmlAttrStack)localObject).a(localAttr);
      }
    }
    for (;;)
    {
      int i = k + 1;
      k = i;
      break label45;
      break;
      label146:
      localObject = localAttr.getLocalName();
      str1 = localAttr.getNodeValue();
      String str2 = "xml";
      boolean bool3 = str2.equals(localObject);
      if (bool3)
      {
        str2 = "http://www.w3.org/XML/1998/namespace";
        bool3 = str2.equals(str1);
        if (bool3) {}
      }
      else
      {
        paramNameSpaceSymbTable.a((String)localObject, str1, localAttr);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/Canonicalizer11.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */