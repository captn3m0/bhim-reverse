package org.apache.xml.security.c14n.implementations;

import java.util.ArrayList;
import java.util.List;

class SymbMap
  implements Cloneable
{
  int a = 23;
  NameSpaceSymbEntry[] b;
  String[] c;
  
  SymbMap()
  {
    Object localObject = new NameSpaceSymbEntry[a];
    b = ((NameSpaceSymbEntry[])localObject);
    localObject = new String[a];
    c = ((String[])localObject);
  }
  
  protected int a(Object paramObject)
  {
    String[] arrayOfString = c;
    int i = arrayOfString.length;
    int j = paramObject.hashCode();
    int k = -1 >>> 1;
    j = (j & k) % i;
    String str = arrayOfString[j];
    boolean bool;
    if (str != null)
    {
      bool = str.equals(paramObject);
      if (!bool) {}
    }
    else
    {
      return j;
    }
    i += -1;
    label59:
    if (j == i) {
      j = 0;
    }
    for (;;)
    {
      str = arrayOfString[j];
      if (str == null) {
        break;
      }
      bool = str.equals(paramObject);
      if (!bool) {
        break label59;
      }
      break;
      j += 1;
    }
  }
  
  List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int i = 0;
    for (;;)
    {
      Object localObject = b;
      int j = localObject.length;
      if (i >= j) {
        break;
      }
      localObject = b[i];
      if (localObject != null)
      {
        localObject = "";
        String str = b[i].c;
        boolean bool = ((String)localObject).equals(str);
        if (!bool)
        {
          localObject = b[i];
          localArrayList.add(localObject);
        }
      }
      i += 1;
    }
    return localArrayList;
  }
  
  NameSpaceSymbEntry a(String paramString)
  {
    NameSpaceSymbEntry[] arrayOfNameSpaceSymbEntry = b;
    int i = a(paramString);
    return arrayOfNameSpaceSymbEntry[i];
  }
  
  protected void a(int paramInt)
  {
    Object localObject1 = c;
    int i = localObject1.length;
    String[] arrayOfString = c;
    NameSpaceSymbEntry[] arrayOfNameSpaceSymbEntry = b;
    Object localObject2 = new String[paramInt];
    c = ((String[])localObject2);
    localObject2 = new NameSpaceSymbEntry[paramInt];
    b = ((NameSpaceSymbEntry[])localObject2);
    for (;;)
    {
      int j = i + -1;
      if (i > 0)
      {
        localObject1 = arrayOfString[j];
        if (localObject1 != null)
        {
          localObject1 = arrayOfString[j];
          int k = a(localObject1);
          c[k] = localObject1;
          localObject1 = b;
          NameSpaceSymbEntry localNameSpaceSymbEntry = arrayOfNameSpaceSymbEntry[j];
          localObject1[k] = localNameSpaceSymbEntry;
          i = j;
        }
      }
      else
      {
        return;
      }
      i = j;
    }
  }
  
  void a(String paramString, NameSpaceSymbEntry paramNameSpaceSymbEntry)
  {
    int i = a(paramString);
    String str = c[i];
    c[i] = paramString;
    NameSpaceSymbEntry[] arrayOfNameSpaceSymbEntry1 = b;
    arrayOfNameSpaceSymbEntry1[i] = paramNameSpaceSymbEntry;
    if (str != null)
    {
      boolean bool = str.equals(paramString);
      if (bool) {}
    }
    else
    {
      int j = a + -1;
      a = j;
      if (j == 0)
      {
        NameSpaceSymbEntry[] arrayOfNameSpaceSymbEntry2 = b;
        j = arrayOfNameSpaceSymbEntry2.length;
        a = j;
        j = a << 2;
        a(j);
      }
    }
  }
  
  protected Object clone()
  {
    try
    {
      localObject1 = super.clone();
      localObject1 = (SymbMap)localObject1;
      Object localObject2 = b;
      int i = localObject2.length;
      localObject2 = new NameSpaceSymbEntry[i];
      b = ((NameSpaceSymbEntry[])localObject2);
      localObject2 = b;
      Object localObject3 = b;
      Object localObject4 = b;
      int j = localObject4.length;
      System.arraycopy(localObject2, 0, localObject3, 0, j);
      localObject2 = c;
      i = localObject2.length;
      localObject2 = new String[i];
      c = ((String[])localObject2);
      localObject2 = c;
      localObject3 = c;
      localObject4 = c;
      j = localObject4.length;
      System.arraycopy(localObject2, 0, localObject3, 0, j);
    }
    catch (CloneNotSupportedException localCloneNotSupportedException)
    {
      for (;;)
      {
        localCloneNotSupportedException.printStackTrace();
        Object localObject1 = null;
      }
    }
    return localObject1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/SymbMap.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */