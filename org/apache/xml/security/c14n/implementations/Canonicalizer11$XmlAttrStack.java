package org.apache.xml.security.c14n.implementations;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.w3c.dom.Attr;

class Canonicalizer11$XmlAttrStack
{
  int a = 0;
  int b = 0;
  Canonicalizer11.XmlAttrStack.XmlsStackElement c;
  List d;
  
  Canonicalizer11$XmlAttrStack()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    d = localArrayList;
  }
  
  void a(int paramInt)
  {
    a = paramInt;
    int i = a;
    int j = -1;
    if (i == j) {
      return;
    }
    i = 0;
    Object localObject = null;
    c = null;
    for (;;)
    {
      i = b;
      j = a;
      if (i < j) {
        break;
      }
      localObject = d;
      List localList = d;
      j = localList.size() + -1;
      ((List)localObject).remove(j);
      localObject = d;
      i = ((List)localObject).size();
      if (i == 0)
      {
        i = 0;
        localObject = null;
        b = 0;
        break;
      }
      localObject = d;
      localList = d;
      j = localList.size() + -1;
      localObject = (Canonicalizer11.XmlAttrStack.XmlsStackElement)((List)localObject).get(j);
      i = a;
      b = i;
    }
  }
  
  void a(Collection paramCollection)
  {
    String str1 = null;
    String str2 = null;
    int i = 1;
    float f1 = Float.MIN_VALUE;
    Object localObject1 = c;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = new org/apache/xml/security/c14n/implementations/Canonicalizer11$XmlAttrStack$XmlsStackElement;
      ((Canonicalizer11.XmlAttrStack.XmlsStackElement)localObject1).<init>();
      c = ((Canonicalizer11.XmlAttrStack.XmlsStackElement)localObject1);
      localObject1 = c;
      j = a;
      a = j;
      k = a;
      b = k;
      localObject1 = d;
      localObject2 = c;
      ((List)localObject1).add(localObject2);
    }
    localObject1 = d;
    int j = ((List)localObject1).size() + -2;
    int k = -1;
    if (j == k) {
      k = i;
    }
    for (;;)
    {
      if (k != 0)
      {
        localObject1 = c.c;
        paramCollection.addAll((Collection)localObject1);
        localObject1 = c;
        b = i;
        return;
        localObject1 = (Canonicalizer11.XmlAttrStack.XmlsStackElement)d.get(j);
        boolean bool3 = b;
        if (bool3)
        {
          k = a + 1;
          int n = a;
          if (k == n) {
            k = i;
          }
        }
      }
      else
      {
        HashMap localHashMap = new java/util/HashMap;
        localHashMap.<init>();
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        int i1 = j;
        j = i;
        float f2 = f1;
        label249:
        boolean bool4;
        Object localObject3;
        if (i1 >= 0)
        {
          localObject1 = (Canonicalizer11.XmlAttrStack.XmlsStackElement)d.get(i1);
          bool4 = b;
          if (!bool4) {
            break label709;
          }
          bool4 = false;
          localObject3 = null;
        }
        for (float f3 = 0.0F;; f3 = f2)
        {
          localObject2 = c;
          Iterator localIterator = ((List)localObject2).iterator();
          for (;;)
          {
            bool1 = localIterator.hasNext();
            if ((!bool1) || (!bool4)) {
              break;
            }
            localObject2 = (Attr)localIterator.next();
            String str3 = ((Attr)localObject2).getLocalName();
            String str4 = "base";
            boolean bool5 = str3.equals(str4);
            if (bool5)
            {
              bool5 = b;
              if (!bool5) {
                localArrayList.add(localObject2);
              }
            }
            else
            {
              str3 = ((Attr)localObject2).getName();
              bool5 = localHashMap.containsKey(str3);
              if (!bool5)
              {
                str3 = ((Attr)localObject2).getName();
                localHashMap.put(str3, localObject2);
              }
            }
          }
          k = i1 + -1;
          boolean bool1 = bool4;
          f2 = f3;
          i1 = k;
          break label249;
          boolean bool2 = localArrayList.isEmpty();
          if (!bool2)
          {
            localObject1 = c.c;
            localObject2 = ((List)localObject1).iterator();
            do
            {
              bool2 = ((Iterator)localObject2).hasNext();
              if (!bool2) {
                break;
              }
              localObject1 = (Attr)((Iterator)localObject2).next();
              localObject3 = ((Attr)localObject1).getLocalName();
              str2 = "base";
              bool4 = ((String)localObject3).equals(str2);
            } while (!bool4);
            str1 = ((Attr)localObject1).getValue();
          }
          for (;;)
          {
            localObject3 = localArrayList.iterator();
            localObject2 = localObject1;
            for (;;)
            {
              bool2 = ((Iterator)localObject3).hasNext();
              if (!bool2) {
                break;
              }
              localObject1 = (Attr)((Iterator)localObject3).next();
              if (str1 == null)
              {
                str1 = ((Attr)localObject1).getValue();
                localObject2 = localObject1;
              }
              else
              {
                try
                {
                  localObject1 = ((Attr)localObject1).getValue();
                  str1 = Canonicalizer11.a((String)localObject1, str1);
                }
                catch (URISyntaxException localURISyntaxException)
                {
                  localURISyntaxException.printStackTrace();
                }
              }
            }
            if (str1 != null)
            {
              m = str1.length();
              if (m != 0)
              {
                ((Attr)localObject2).setValue(str1);
                paramCollection.add(localObject2);
              }
            }
            c.b = i;
            localCollection = localHashMap.values();
            paramCollection.addAll(localCollection);
            break;
            m = 0;
            localCollection = null;
          }
          label709:
          bool4 = bool1;
        }
      }
      int m = 0;
      Collection localCollection = null;
    }
  }
  
  void a(Attr paramAttr)
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = new org/apache/xml/security/c14n/implementations/Canonicalizer11$XmlAttrStack$XmlsStackElement;
      ((Canonicalizer11.XmlAttrStack.XmlsStackElement)localObject).<init>();
      c = ((Canonicalizer11.XmlAttrStack.XmlsStackElement)localObject);
      localObject = c;
      int i = a;
      a = i;
      localObject = d;
      Canonicalizer11.XmlAttrStack.XmlsStackElement localXmlsStackElement = c;
      ((List)localObject).add(localXmlsStackElement);
      int j = a;
      b = j;
    }
    c.c.add(paramAttr);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/Canonicalizer11$XmlAttrStack.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */