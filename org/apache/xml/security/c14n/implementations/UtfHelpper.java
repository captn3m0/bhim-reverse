package org.apache.xml.security.c14n.implementations;

import java.io.OutputStream;
import java.util.Map;

public class UtfHelpper
{
  static final void a(char paramChar, OutputStream paramOutputStream)
  {
    char c1 = '';
    char c2 = '?';
    float f = 8.8E-44F;
    if (paramChar < c1) {
      paramOutputStream.write(paramChar);
    }
    for (;;)
    {
      return;
      c3 = 55296;
      if (paramChar >= c3)
      {
        c3 = 56319;
        if (paramChar <= c3) {}
      }
      else
      {
        c3 = 56320;
        if (paramChar < c3) {
          break;
        }
        c3 = 57343;
        if (paramChar > c3) {
          break;
        }
      }
      paramOutputStream.write(c2);
    }
    char c3 = '߿';
    if (paramChar > c3)
    {
      char c4 = (char)(paramChar >>> '\f');
      c3 = 'à';
      if (c4 > 0)
      {
        c4 &= 0xF;
        c3 |= c4;
      }
      paramOutputStream.write(c3);
      c3 = c1;
    }
    for (c1 = c2;; c1 = '\037')
    {
      c2 = (char)(paramChar >>> '\006');
      if (c2 > 0)
      {
        c1 &= c2;
        c3 |= c1;
      }
      paramOutputStream.write(c3);
      c3 = paramChar & 0x3F | 0x80;
      paramOutputStream.write(c3);
      break;
      c3 = 'À';
    }
  }
  
  static final void a(String paramString, OutputStream paramOutputStream)
  {
    int i = 128;
    int j = 63;
    int k = paramString.length();
    int m = 0;
    while (m < k)
    {
      int n = m + 1;
      int i1 = paramString.charAt(m);
      if (i1 < i)
      {
        paramOutputStream.write(i1);
        m = n;
      }
      else
      {
        m = 55296;
        if (i1 >= m)
        {
          m = 56319;
          if (i1 <= m) {}
        }
        else
        {
          m = 56320;
          if (i1 < m) {
            break label112;
          }
          m = 57343;
          if (i1 > m) {
            break label112;
          }
        }
        paramOutputStream.write(j);
        m = n;
        continue;
        label112:
        m = 2047;
        if (i1 > m)
        {
          i2 = (char)(i1 >>> 12);
          m = 224;
          if (i2 > 0)
          {
            i2 &= 0xF;
            m |= i2;
          }
          paramOutputStream.write(m);
          m = i;
        }
        for (int i2 = j;; i2 = 31)
        {
          int i3 = (char)(i1 >>> 6);
          if (i3 > 0)
          {
            i2 &= i3;
            m |= i2;
          }
          paramOutputStream.write(m);
          m = i1 & 0x3F | 0x80;
          paramOutputStream.write(m);
          m = n;
          break;
          m = 192;
        }
      }
    }
  }
  
  static final void a(String paramString, OutputStream paramOutputStream, Map paramMap)
  {
    byte[] arrayOfByte = (byte[])paramMap.get(paramString);
    if (arrayOfByte == null)
    {
      arrayOfByte = a(paramString);
      paramMap.put(paramString, arrayOfByte);
    }
    paramOutputStream.write(arrayOfByte);
  }
  
  public static final byte[] a(String paramString)
  {
    int i = 63;
    float f1 = 8.8E-44F;
    int j = paramString.length();
    Object localObject1 = new byte[j];
    int k = 0;
    int m = 0;
    Object localObject2 = null;
    int n = 0;
    Object localObject3 = null;
    float f2 = 0.0F;
    int i1;
    int i2;
    int i3;
    label162:
    int i4;
    float f3;
    while (m < j)
    {
      i1 = m + 1;
      i2 = paramString.charAt(m);
      m = 128;
      if (i2 < m)
      {
        m = k + 1;
        i3 = (byte)i2;
        localObject1[k] = i3;
        k = m;
        m = i1;
      }
      else
      {
        m = 55296;
        if (i2 >= m)
        {
          m = 56319;
          if (i2 <= m) {}
        }
        else
        {
          m = 56320;
          if (i2 < m) {
            break label162;
          }
          m = 57343;
          if (i2 > m) {
            break label162;
          }
        }
        m = k + 1;
        localObject1[k] = i;
        k = m;
        m = i1;
        continue;
        if (n != 0) {
          break label430;
        }
        n = j * 3;
        localObject3 = new byte[n];
        System.arraycopy(localObject1, 0, localObject3, 0, k);
        i4 = 1;
        f3 = Float.MIN_VALUE;
        localObject2 = localObject3;
        i3 = i4;
      }
    }
    for (float f4 = f3;; f4 = f2)
    {
      n = 2047;
      f2 = 2.868E-42F;
      if (i2 > n)
      {
        i4 = (char)(i2 >>> 12);
        n = -32;
        f2 = 0.0F / 0.0F;
        if (i4 > 0)
        {
          i4 &= 0xF;
          n = (byte)(n | i4);
        }
        i4 = k + 1;
        localObject2[k] = n;
        n = -128;
        f2 = 0.0F / 0.0F;
        k = i4;
        i4 = i;
      }
      for (f3 = f1;; f3 = 4.3E-44F)
      {
        int i5 = (char)(i2 >>> 6);
        if (i5 > 0)
        {
          i4 &= i5;
          n = (byte)(n | i4);
        }
        i4 = k + 1;
        localObject2[k] = n;
        k = i4 + 1;
        n = (byte)(i2 & 0x3F | 0x80);
        localObject2[i4] = n;
        localObject1 = localObject2;
        n = i3;
        f2 = f4;
        m = i1;
        break;
        n = -64;
        f2 = 0.0F / 0.0F;
        i4 = 31;
      }
      if (n != 0)
      {
        localObject3 = new byte[k];
        System.arraycopy(localObject1, 0, localObject3, 0, k);
      }
      for (;;)
      {
        return (byte[])localObject3;
        localObject3 = localObject1;
      }
      label430:
      localObject2 = localObject1;
      i3 = n;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/UtfHelpper.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */