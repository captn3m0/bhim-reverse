package org.apache.xml.security.c14n.implementations;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.CanonicalizerSpi;
import org.apache.xml.security.c14n.helper.AttrCompare;
import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.UnsyncByteArrayOutputStream;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.ProcessingInstruction;

public abstract class CanonicalizerBase
  extends CanonicalizerSpi
{
  private static final byte[] b;
  private static final byte[] c;
  private static final byte[] d;
  private static final byte[] e;
  private static final byte[] f;
  static final AttrCompare g;
  static final byte[] h;
  protected static final Attr i;
  private static final byte[] o;
  private static final byte[] p;
  private static final byte[] q;
  private static final byte[] r;
  private static final byte[] s;
  private static final byte[] t;
  private static final byte[] u;
  List j;
  boolean k;
  Set l = null;
  Node m = null;
  OutputStream n;
  
  static
  {
    int i1 = 4;
    int i2 = 5;
    int i3 = 2;
    Object localObject1 = new byte[i3];
    Object tmp11_10 = localObject1;
    tmp11_10[0] = 63;
    tmp11_10[1] = 62;
    b = (byte[])localObject1;
    localObject1 = new byte[i3];
    Object tmp29_28 = localObject1;
    tmp29_28[0] = 60;
    tmp29_28[1] = 63;
    c = (byte[])localObject1;
    localObject1 = new byte[3];
    Object tmp47_46 = localObject1;
    tmp47_46[0] = 45;
    Object tmp52_47 = tmp47_46;
    tmp52_47[1] = 45;
    tmp52_47[2] = 62;
    d = (byte[])localObject1;
    localObject1 = new byte[i1];
    Object tmp70_69 = localObject1;
    Object tmp71_70 = tmp70_69;
    Object tmp71_70 = tmp70_69;
    tmp71_70[0] = 60;
    tmp71_70[1] = 33;
    tmp71_70[2] = 45;
    tmp71_70[3] = 45;
    e = (byte[])localObject1;
    localObject1 = new byte[i2];
    Object tmp97_96 = localObject1;
    Object tmp98_97 = tmp97_96;
    Object tmp98_97 = tmp97_96;
    tmp98_97[0] = 38;
    tmp98_97[1] = 35;
    tmp98_97[2] = 120;
    Object tmp111_98 = tmp98_97;
    tmp111_98[3] = 65;
    tmp111_98[4] = 59;
    f = (byte[])localObject1;
    localObject1 = new byte[i2];
    Object tmp129_128 = localObject1;
    Object tmp130_129 = tmp129_128;
    Object tmp130_129 = tmp129_128;
    tmp130_129[0] = 38;
    tmp130_129[1] = 35;
    tmp130_129[2] = 120;
    Object tmp143_130 = tmp130_129;
    tmp143_130[3] = 57;
    tmp143_130[4] = 59;
    o = (byte[])localObject1;
    int i4 = 6;
    localObject1 = new byte[i4];
    Object tmp166_165 = localObject1;
    Object tmp167_166 = tmp166_165;
    Object tmp167_166 = tmp166_165;
    tmp167_166[0] = 38;
    tmp167_166[1] = 113;
    Object tmp176_167 = tmp167_166;
    Object tmp176_167 = tmp167_166;
    tmp176_167[2] = 117;
    tmp176_167[3] = 111;
    tmp176_167[4] = 116;
    tmp176_167[5] = 59;
    p = (byte[])localObject1;
    localObject1 = new byte[i2];
    Object tmp202_201 = localObject1;
    Object tmp203_202 = tmp202_201;
    Object tmp203_202 = tmp202_201;
    tmp203_202[0] = 38;
    tmp203_202[1] = 35;
    tmp203_202[2] = 120;
    Object tmp216_203 = tmp203_202;
    tmp216_203[3] = 68;
    tmp216_203[4] = 59;
    q = (byte[])localObject1;
    localObject1 = new byte[i1];
    Object tmp234_233 = localObject1;
    Object tmp235_234 = tmp234_233;
    Object tmp235_234 = tmp234_233;
    tmp235_234[0] = 38;
    tmp235_234[1] = 103;
    tmp235_234[2] = 116;
    tmp235_234[3] = 59;
    r = (byte[])localObject1;
    localObject1 = new byte[i1];
    Object tmp261_260 = localObject1;
    Object tmp262_261 = tmp261_260;
    Object tmp262_261 = tmp261_260;
    tmp262_261[0] = 38;
    tmp262_261[1] = 108;
    tmp262_261[2] = 116;
    tmp262_261[3] = 59;
    s = (byte[])localObject1;
    localObject1 = new byte[i3];
    Object tmp288_287 = localObject1;
    tmp288_287[0] = 60;
    tmp288_287[1] = 47;
    t = (byte[])localObject1;
    localObject1 = new byte[i2];
    Object tmp306_305 = localObject1;
    Object tmp307_306 = tmp306_305;
    Object tmp307_306 = tmp306_305;
    tmp307_306[0] = 38;
    tmp307_306[1] = 97;
    tmp307_306[2] = 109;
    Object tmp320_307 = tmp307_306;
    tmp320_307[3] = 112;
    tmp320_307[4] = 59;
    u = (byte[])localObject1;
    localObject1 = new org/apache/xml/security/c14n/helper/AttrCompare;
    ((AttrCompare)localObject1).<init>();
    g = (AttrCompare)localObject1;
    localObject1 = new byte[i3];
    Object tmp350_349 = localObject1;
    tmp350_349[0] = 61;
    tmp350_349[1] = 34;
    h = (byte[])localObject1;
    try
    {
      localObject1 = DocumentBuilderFactory.newInstance();
      localObject1 = ((DocumentBuilderFactory)localObject1).newDocumentBuilder();
      localObject1 = ((DocumentBuilder)localObject1).newDocument();
      localObject2 = "http://www.w3.org/2000/xmlns/";
      localObject3 = "xmlns";
      localObject1 = ((Document)localObject1).createAttributeNS((String)localObject2, (String)localObject3);
      i = (Attr)localObject1;
      localObject1 = i;
      localObject2 = "";
      ((Attr)localObject1).setValue((String)localObject2);
      return;
    }
    catch (Exception localException)
    {
      Object localObject2 = new java/lang/RuntimeException;
      Object localObject3 = new java/lang/StringBuffer;
      ((StringBuffer)localObject3).<init>();
      String str = "Unable to create nullNode" + localException;
      ((RuntimeException)localObject2).<init>(str);
      throw ((Throwable)localObject2);
    }
  }
  
  public CanonicalizerBase(boolean paramBoolean)
  {
    UnsyncByteArrayOutputStream localUnsyncByteArrayOutputStream = new org/apache/xml/security/utils/UnsyncByteArrayOutputStream;
    localUnsyncByteArrayOutputStream.<init>();
    n = localUnsyncByteArrayOutputStream;
    k = paramBoolean;
  }
  
  static final void a(String paramString, OutputStream paramOutputStream)
  {
    int i1 = paramString.length();
    int i2 = 0;
    byte[] arrayOfByte = null;
    int i3 = 0;
    if (i3 < i1)
    {
      i2 = paramString.charAt(i3);
      switch (i2)
      {
      default: 
        int i4 = 128;
        if (i2 < i4) {
          paramOutputStream.write(i2);
        }
        break;
      }
      for (;;)
      {
        i2 = i3 + 1;
        i3 = i2;
        break;
        arrayOfByte = u;
        for (;;)
        {
          paramOutputStream.write(arrayOfByte);
          break;
          arrayOfByte = s;
          continue;
          arrayOfByte = r;
          continue;
          arrayOfByte = q;
        }
        UtfHelpper.a(i2, paramOutputStream);
      }
    }
  }
  
  static final void a(String paramString1, String paramString2, OutputStream paramOutputStream, Map paramMap)
  {
    paramOutputStream.write(32);
    UtfHelpper.a(paramString1, paramOutputStream, paramMap);
    byte[] arrayOfByte = h;
    paramOutputStream.write(arrayOfByte);
    int i1 = paramString2.length();
    int i2 = 0;
    arrayOfByte = null;
    while (i2 < i1)
    {
      int i3 = i2 + 1;
      i2 = paramString2.charAt(i2);
      switch (i2)
      {
      default: 
        int i4 = 128;
        if (i2 < i4)
        {
          paramOutputStream.write(i2);
          i2 = i3;
        }
        break;
      case 38: 
        arrayOfByte = u;
      case 60: 
      case 34: 
      case 9: 
      case 10: 
      case 13: 
        for (;;)
        {
          paramOutputStream.write(arrayOfByte);
          i2 = i3;
          break;
          arrayOfByte = s;
          continue;
          arrayOfByte = p;
          continue;
          arrayOfByte = o;
          continue;
          arrayOfByte = f;
          continue;
          arrayOfByte = q;
        }
        UtfHelpper.a(i2, paramOutputStream);
        i2 = i3;
      }
    }
    paramOutputStream.write(34);
  }
  
  static final void a(Comment paramComment, OutputStream paramOutputStream, int paramInt)
  {
    int i1 = 10;
    int i2 = 1;
    if (paramInt == i2) {
      paramOutputStream.write(i1);
    }
    byte[] arrayOfByte1 = e;
    paramOutputStream.write(arrayOfByte1);
    String str = paramComment.getData();
    int i3 = str.length();
    i2 = 0;
    arrayOfByte1 = null;
    if (i2 < i3)
    {
      char c1 = str.charAt(i2);
      char c2 = '\r';
      if (c1 == c2)
      {
        byte[] arrayOfByte2 = q;
        paramOutputStream.write(arrayOfByte2);
      }
      for (;;)
      {
        i2 += 1;
        break;
        c2 = '';
        if (c1 < c2) {
          paramOutputStream.write(c1);
        } else {
          UtfHelpper.a(c1, paramOutputStream);
        }
      }
    }
    arrayOfByte1 = d;
    paramOutputStream.write(arrayOfByte1);
    i2 = -1;
    if (paramInt == i2) {
      paramOutputStream.write(i1);
    }
  }
  
  static final void a(ProcessingInstruction paramProcessingInstruction, OutputStream paramOutputStream, int paramInt)
  {
    int i1 = 13;
    int i2 = 10;
    int i3 = 0;
    byte[] arrayOfByte1 = null;
    int i4 = 1;
    if (paramInt == i4) {
      paramOutputStream.write(i2);
    }
    Object localObject = c;
    paramOutputStream.write((byte[])localObject);
    String str = paramProcessingInstruction.getTarget();
    int i5 = str.length();
    i4 = 0;
    localObject = null;
    if (i4 < i5)
    {
      int i6 = str.charAt(i4);
      if (i6 == i1)
      {
        byte[] arrayOfByte2 = q;
        paramOutputStream.write(arrayOfByte2);
      }
      for (;;)
      {
        i4 += 1;
        break;
        int i7 = 128;
        if (i6 < i7) {
          paramOutputStream.write(i6);
        } else {
          UtfHelpper.a(i6, paramOutputStream);
        }
      }
    }
    localObject = paramProcessingInstruction.getData();
    int i8 = ((String)localObject).length();
    if (i8 > 0)
    {
      i5 = 32;
      paramOutputStream.write(i5);
      if (i3 < i8)
      {
        i5 = ((String)localObject).charAt(i3);
        if (i5 == i1)
        {
          byte[] arrayOfByte3 = q;
          paramOutputStream.write(arrayOfByte3);
        }
        for (;;)
        {
          i3 += 1;
          break;
          UtfHelpper.a(i5, paramOutputStream);
        }
      }
    }
    arrayOfByte1 = b;
    paramOutputStream.write(arrayOfByte1);
    i3 = -1;
    if (paramInt == i3) {
      paramOutputStream.write(i2);
    }
  }
  
  /* Error */
  private byte[] d(Node paramNode)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_1
    //   3: invokevirtual 189	org/apache/xml/security/c14n/implementations/CanonicalizerBase:b	(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)V
    //   6: aload_0
    //   7: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   10: astore_2
    //   11: aload_2
    //   12: invokevirtual 192	java/io/OutputStream:close	()V
    //   15: aload_0
    //   16: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   19: astore_2
    //   20: aload_2
    //   21: instanceof 194
    //   24: istore_3
    //   25: iload_3
    //   26: ifeq +47 -> 73
    //   29: aload_0
    //   30: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   33: astore_2
    //   34: aload_2
    //   35: checkcast 194	java/io/ByteArrayOutputStream
    //   38: astore_2
    //   39: aload_2
    //   40: invokevirtual 198	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   43: astore 4
    //   45: aload_0
    //   46: getfield 200	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	Z
    //   49: istore_3
    //   50: iload_3
    //   51: ifeq +17 -> 68
    //   54: aload_0
    //   55: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   58: astore_2
    //   59: aload_2
    //   60: checkcast 194	java/io/ByteArrayOutputStream
    //   63: astore_2
    //   64: aload_2
    //   65: invokevirtual 203	java/io/ByteArrayOutputStream:reset	()V
    //   68: aload 4
    //   70: astore_2
    //   71: aload_2
    //   72: areturn
    //   73: aload_0
    //   74: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   77: astore_2
    //   78: aload_2
    //   79: instanceof 136
    //   82: istore_3
    //   83: iload_3
    //   84: ifeq +48 -> 132
    //   87: aload_0
    //   88: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   91: astore_2
    //   92: aload_2
    //   93: checkcast 136	org/apache/xml/security/utils/UnsyncByteArrayOutputStream
    //   96: astore_2
    //   97: aload_2
    //   98: invokevirtual 205	org/apache/xml/security/utils/UnsyncByteArrayOutputStream:a	()[B
    //   101: astore 4
    //   103: aload_0
    //   104: getfield 200	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	Z
    //   107: istore_3
    //   108: iload_3
    //   109: ifeq +17 -> 126
    //   112: aload_0
    //   113: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   116: astore_2
    //   117: aload_2
    //   118: checkcast 136	org/apache/xml/security/utils/UnsyncByteArrayOutputStream
    //   121: astore_2
    //   122: aload_2
    //   123: invokevirtual 207	org/apache/xml/security/utils/UnsyncByteArrayOutputStream:b	()V
    //   126: aload 4
    //   128: astore_2
    //   129: goto -58 -> 71
    //   132: iconst_0
    //   133: istore_3
    //   134: aconst_null
    //   135: astore_2
    //   136: goto -65 -> 71
    //   139: astore_2
    //   140: new 209	org/apache/xml/security/c14n/CanonicalizationException
    //   143: astore 4
    //   145: aload 4
    //   147: ldc -45
    //   149: aload_2
    //   150: invokespecial 214	org/apache/xml/security/c14n/CanonicalizationException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   153: aload 4
    //   155: athrow
    //   156: astore_2
    //   157: new 209	org/apache/xml/security/c14n/CanonicalizationException
    //   160: astore 4
    //   162: aload 4
    //   164: ldc -45
    //   166: aload_2
    //   167: invokespecial 214	org/apache/xml/security/c14n/CanonicalizationException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   170: aload 4
    //   172: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	173	0	this	CanonicalizerBase
    //   0	173	1	paramNode	Node
    //   10	126	2	localObject1	Object
    //   139	11	2	localUnsupportedEncodingException	java.io.UnsupportedEncodingException
    //   156	11	2	localIOException	java.io.IOException
    //   24	110	3	bool	boolean
    //   43	128	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   2	6	139	java/io/UnsupportedEncodingException
    //   6	10	139	java/io/UnsupportedEncodingException
    //   11	15	139	java/io/UnsupportedEncodingException
    //   15	19	139	java/io/UnsupportedEncodingException
    //   29	33	139	java/io/UnsupportedEncodingException
    //   34	38	139	java/io/UnsupportedEncodingException
    //   39	43	139	java/io/UnsupportedEncodingException
    //   45	49	139	java/io/UnsupportedEncodingException
    //   54	58	139	java/io/UnsupportedEncodingException
    //   59	63	139	java/io/UnsupportedEncodingException
    //   64	68	139	java/io/UnsupportedEncodingException
    //   73	77	139	java/io/UnsupportedEncodingException
    //   87	91	139	java/io/UnsupportedEncodingException
    //   92	96	139	java/io/UnsupportedEncodingException
    //   97	101	139	java/io/UnsupportedEncodingException
    //   103	107	139	java/io/UnsupportedEncodingException
    //   112	116	139	java/io/UnsupportedEncodingException
    //   117	121	139	java/io/UnsupportedEncodingException
    //   122	126	139	java/io/UnsupportedEncodingException
    //   2	6	156	java/io/IOException
    //   6	10	156	java/io/IOException
    //   11	15	156	java/io/IOException
    //   15	19	156	java/io/IOException
    //   29	33	156	java/io/IOException
    //   34	38	156	java/io/IOException
    //   39	43	156	java/io/IOException
    //   45	49	156	java/io/IOException
    //   54	58	156	java/io/IOException
    //   59	63	156	java/io/IOException
    //   64	68	156	java/io/IOException
    //   73	77	156	java/io/IOException
    //   87	91	156	java/io/IOException
    //   92	96	156	java/io/IOException
    //   97	101	156	java/io/IOException
    //   103	107	156	java/io/IOException
    //   112	116	156	java/io/IOException
    //   117	121	156	java/io/IOException
    //   122	126	156	java/io/IOException
  }
  
  int a(Node paramNode, int paramInt)
  {
    int i1 = 1;
    Object localObject = j;
    int i2;
    if (localObject != null)
    {
      localObject = j;
      Iterator localIterator = ((List)localObject).iterator();
      do
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject = (NodeFilter)localIterator.next();
        i2 = ((NodeFilter)localObject).a(paramNode, paramInt);
      } while (i2 == i1);
    }
    for (;;)
    {
      return i2;
      localObject = l;
      if (localObject != null)
      {
        localObject = l;
        bool2 = ((Set)localObject).contains(paramNode);
        if (!bool2)
        {
          bool2 = false;
          localObject = null;
          continue;
        }
      }
      boolean bool2 = i1;
    }
  }
  
  abstract Iterator a(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable);
  
  public void a(OutputStream paramOutputStream)
  {
    n = paramOutputStream;
  }
  
  abstract void a(XMLSignatureInput paramXMLSignatureInput);
  
  final void a(Node paramNode1, NameSpaceSymbTable paramNameSpaceSymbTable, Node paramNode2, int paramInt)
  {
    int i1 = b(paramNode1);
    int i2 = -1;
    if (i1 == i2) {
      return;
    }
    i1 = 0;
    OutputStream localOutputStream = n;
    Node localNode = m;
    boolean bool1 = k;
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject1 = null;
    i2 = 0;
    Object localObject2 = null;
    Object localObject3 = paramNode1;
    label61:
    int i3 = ((Node)localObject3).getNodeType();
    switch (i3)
    {
    case 5: 
    case 10: 
    default: 
      paramNode1 = (Node)localObject2;
      localObject2 = localObject1;
    }
    for (;;)
    {
      if ((paramNode1 == null) && (localObject2 != null))
      {
        localObject3 = t;
        localOutputStream.write((byte[])localObject3);
        localObject3 = localObject2;
        localObject3 = ((Element)localObject2).getTagName();
        UtfHelpper.a((String)localObject3, localOutputStream, localHashMap);
        i1 = 62;
        localOutputStream.write(i1);
        paramNameSpaceSymbTable.b();
        if (localObject2 == paramNode2) {
          break;
        }
        paramNode1 = ((Node)localObject2).getNextSibling();
        localObject1 = ((Node)localObject2).getParentNode();
        if (localObject1 != null)
        {
          i1 = 1;
          i2 = ((Node)localObject1).getNodeType();
          if (i1 == i2) {
            break label639;
          }
        }
        paramInt = 1;
        localObject1 = null;
        i2 = 0;
        localObject2 = null;
        continue;
        localObject3 = new org/apache/xml/security/c14n/CanonicalizationException;
        ((CanonicalizationException)localObject3).<init>("empty");
        throw ((Throwable)localObject3);
        paramNameSpaceSymbTable.a();
        paramNode1 = ((Node)localObject3).getFirstChild();
        localObject2 = localObject1;
        continue;
        if (!bool1) {
          break label629;
        }
        localObject3 = (Comment)localObject3;
        a((Comment)localObject3, localOutputStream, paramInt);
        paramNode1 = (Node)localObject2;
        localObject2 = localObject1;
        continue;
        localObject3 = (ProcessingInstruction)localObject3;
        a((ProcessingInstruction)localObject3, localOutputStream, paramInt);
        paramNode1 = (Node)localObject2;
        localObject2 = localObject1;
        continue;
        localObject3 = ((Node)localObject3).getNodeValue();
        a((String)localObject3, localOutputStream);
        paramNode1 = (Node)localObject2;
        localObject2 = localObject1;
        continue;
        paramInt = 0;
        if (localObject3 == localNode)
        {
          paramNode1 = (Node)localObject2;
          localObject2 = localObject1;
          continue;
        }
        localObject2 = localObject3;
        localObject2 = (Element)localObject3;
        paramNameSpaceSymbTable.a();
        i3 = 60;
        localOutputStream.write(i3);
        String str1 = ((Element)localObject2).getTagName();
        UtfHelpper.a(str1, localOutputStream, localHashMap);
        Iterator localIterator = a((Element)localObject2, paramNameSpaceSymbTable);
        if (localIterator != null) {
          for (;;)
          {
            boolean bool2 = localIterator.hasNext();
            if (!bool2) {
              break;
            }
            Object localObject4 = (Attr)localIterator.next();
            String str2 = ((Attr)localObject4).getNodeName();
            localObject4 = ((Attr)localObject4).getNodeValue();
            a(str2, (String)localObject4, localOutputStream, localHashMap);
          }
        }
        int i4 = 62;
        localOutputStream.write(i4);
        paramNode1 = ((Node)localObject3).getFirstChild();
        if (paramNode1 != null) {
          continue;
        }
        localObject2 = t;
        localOutputStream.write((byte[])localObject2);
        UtfHelpper.a(str1, localOutputStream);
        i2 = 62;
        localOutputStream.write(i2);
        paramNameSpaceSymbTable.b();
        if (localObject1 == null) {
          break label622;
        }
        paramNode1 = ((Node)localObject3).getNextSibling();
        localObject2 = localObject1;
        continue;
      }
      if (paramNode1 == null) {
        break;
      }
      localObject3 = paramNode1.getNextSibling();
      localObject1 = localObject2;
      localObject2 = localObject3;
      localObject3 = paramNode1;
      break label61;
      label622:
      localObject2 = localObject1;
      continue;
      label629:
      paramNode1 = (Node)localObject2;
      localObject2 = localObject1;
      continue;
      label639:
      localObject2 = localObject1;
    }
  }
  
  public byte[] a(Set paramSet)
  {
    l = paramSet;
    Document localDocument = XMLUtils.a(l);
    return d(localDocument);
  }
  
  public byte[] a(Node paramNode)
  {
    ((Node)null);
    return a(paramNode, null);
  }
  
  /* Error */
  byte[] a(Node paramNode1, Node paramNode2)
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_2
    //   2: putfield 134	org/apache/xml/security/c14n/implementations/CanonicalizerBase:m	Lorg/w3c/dom/Node;
    //   5: new 267	org/apache/xml/security/c14n/implementations/NameSpaceSymbTable
    //   8: astore_3
    //   9: aload_3
    //   10: invokespecial 317	org/apache/xml/security/c14n/implementations/NameSpaceSymbTable:<init>	()V
    //   13: iconst_m1
    //   14: istore 4
    //   16: aload_1
    //   17: ifnull +47 -> 64
    //   20: iconst_1
    //   21: istore 5
    //   23: aload_1
    //   24: invokeinterface 259 1 0
    //   29: istore 6
    //   31: iload 5
    //   33: iload 6
    //   35: if_icmpne +29 -> 64
    //   38: aload_1
    //   39: astore 7
    //   41: aload_1
    //   42: checkcast 261	org/w3c/dom/Element
    //   45: astore 7
    //   47: aload 7
    //   49: astore 8
    //   51: aload_0
    //   52: aload 7
    //   54: aload_3
    //   55: invokevirtual 320	org/apache/xml/security/c14n/implementations/CanonicalizerBase:d	(Lorg/w3c/dom/Element;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;)V
    //   58: iconst_0
    //   59: istore 4
    //   61: aconst_null
    //   62: astore 8
    //   64: aload_0
    //   65: aload_1
    //   66: aload_3
    //   67: aload_1
    //   68: iload 4
    //   70: invokevirtual 323	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	(Lorg/w3c/dom/Node;Lorg/apache/xml/security/c14n/implementations/NameSpaceSymbTable;Lorg/w3c/dom/Node;I)V
    //   73: aload_0
    //   74: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   77: astore 8
    //   79: aload 8
    //   81: invokevirtual 192	java/io/OutputStream:close	()V
    //   84: aload_0
    //   85: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   88: astore 8
    //   90: aload 8
    //   92: instanceof 194
    //   95: istore 4
    //   97: iload 4
    //   99: ifeq +57 -> 156
    //   102: aload_0
    //   103: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   106: astore 8
    //   108: aload 8
    //   110: checkcast 194	java/io/ByteArrayOutputStream
    //   113: astore 8
    //   115: aload 8
    //   117: invokevirtual 198	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   120: astore_3
    //   121: aload_0
    //   122: getfield 200	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	Z
    //   125: istore 4
    //   127: iload 4
    //   129: ifeq +21 -> 150
    //   132: aload_0
    //   133: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   136: astore 8
    //   138: aload 8
    //   140: checkcast 194	java/io/ByteArrayOutputStream
    //   143: astore 8
    //   145: aload 8
    //   147: invokevirtual 203	java/io/ByteArrayOutputStream:reset	()V
    //   150: aload_3
    //   151: astore 8
    //   153: aload 8
    //   155: areturn
    //   156: aload_0
    //   157: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   160: astore 8
    //   162: aload 8
    //   164: instanceof 136
    //   167: istore 4
    //   169: iload 4
    //   171: ifeq +57 -> 228
    //   174: aload_0
    //   175: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   178: astore 8
    //   180: aload 8
    //   182: checkcast 136	org/apache/xml/security/utils/UnsyncByteArrayOutputStream
    //   185: astore 8
    //   187: aload 8
    //   189: invokevirtual 205	org/apache/xml/security/utils/UnsyncByteArrayOutputStream:a	()[B
    //   192: astore_3
    //   193: aload_0
    //   194: getfield 200	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	Z
    //   197: istore 4
    //   199: iload 4
    //   201: ifeq +21 -> 222
    //   204: aload_0
    //   205: getfield 139	org/apache/xml/security/c14n/implementations/CanonicalizerBase:n	Ljava/io/OutputStream;
    //   208: astore 8
    //   210: aload 8
    //   212: checkcast 136	org/apache/xml/security/utils/UnsyncByteArrayOutputStream
    //   215: astore 8
    //   217: aload 8
    //   219: invokevirtual 207	org/apache/xml/security/utils/UnsyncByteArrayOutputStream:b	()V
    //   222: aload_3
    //   223: astore 8
    //   225: goto -72 -> 153
    //   228: iconst_0
    //   229: istore 4
    //   231: aconst_null
    //   232: astore 8
    //   234: goto -81 -> 153
    //   237: astore 8
    //   239: new 209	org/apache/xml/security/c14n/CanonicalizationException
    //   242: astore_3
    //   243: aload_3
    //   244: ldc -45
    //   246: aload 8
    //   248: invokespecial 214	org/apache/xml/security/c14n/CanonicalizationException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   251: aload_3
    //   252: athrow
    //   253: astore 8
    //   255: new 209	org/apache/xml/security/c14n/CanonicalizationException
    //   258: astore_3
    //   259: aload_3
    //   260: ldc -45
    //   262: aload 8
    //   264: invokespecial 214	org/apache/xml/security/c14n/CanonicalizationException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   267: aload_3
    //   268: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	269	0	this	CanonicalizerBase
    //   0	269	1	paramNode1	Node
    //   0	269	2	paramNode2	Node
    //   8	260	3	localObject1	Object
    //   14	55	4	i1	int
    //   95	135	4	bool	boolean
    //   21	15	5	i2	int
    //   29	7	6	i3	int
    //   39	14	7	localObject2	Object
    //   49	184	8	localObject3	Object
    //   237	10	8	localUnsupportedEncodingException	java.io.UnsupportedEncodingException
    //   253	10	8	localIOException	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   5	8	237	java/io/UnsupportedEncodingException
    //   9	13	237	java/io/UnsupportedEncodingException
    //   23	29	237	java/io/UnsupportedEncodingException
    //   41	45	237	java/io/UnsupportedEncodingException
    //   54	58	237	java/io/UnsupportedEncodingException
    //   68	73	237	java/io/UnsupportedEncodingException
    //   73	77	237	java/io/UnsupportedEncodingException
    //   79	84	237	java/io/UnsupportedEncodingException
    //   84	88	237	java/io/UnsupportedEncodingException
    //   102	106	237	java/io/UnsupportedEncodingException
    //   108	113	237	java/io/UnsupportedEncodingException
    //   115	120	237	java/io/UnsupportedEncodingException
    //   121	125	237	java/io/UnsupportedEncodingException
    //   132	136	237	java/io/UnsupportedEncodingException
    //   138	143	237	java/io/UnsupportedEncodingException
    //   145	150	237	java/io/UnsupportedEncodingException
    //   156	160	237	java/io/UnsupportedEncodingException
    //   174	178	237	java/io/UnsupportedEncodingException
    //   180	185	237	java/io/UnsupportedEncodingException
    //   187	192	237	java/io/UnsupportedEncodingException
    //   193	197	237	java/io/UnsupportedEncodingException
    //   204	208	237	java/io/UnsupportedEncodingException
    //   210	215	237	java/io/UnsupportedEncodingException
    //   217	222	237	java/io/UnsupportedEncodingException
    //   5	8	253	java/io/IOException
    //   9	13	253	java/io/IOException
    //   23	29	253	java/io/IOException
    //   41	45	253	java/io/IOException
    //   54	58	253	java/io/IOException
    //   68	73	253	java/io/IOException
    //   73	77	253	java/io/IOException
    //   79	84	253	java/io/IOException
    //   84	88	253	java/io/IOException
    //   102	106	253	java/io/IOException
    //   108	113	253	java/io/IOException
    //   115	120	253	java/io/IOException
    //   121	125	253	java/io/IOException
    //   132	136	253	java/io/IOException
    //   138	143	253	java/io/IOException
    //   145	150	253	java/io/IOException
    //   156	160	253	java/io/IOException
    //   174	178	253	java/io/IOException
    //   180	185	253	java/io/IOException
    //   187	192	253	java/io/IOException
    //   193	197	253	java/io/IOException
    //   204	208	253	java/io/IOException
    //   210	215	253	java/io/IOException
    //   217	222	253	java/io/IOException
  }
  
  int b(Node paramNode)
  {
    int i1 = 1;
    Object localObject = j;
    int i2;
    if (localObject != null)
    {
      localObject = j;
      Iterator localIterator = ((List)localObject).iterator();
      do
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject = (NodeFilter)localIterator.next();
        i2 = ((NodeFilter)localObject).a(paramNode);
      } while (i2 == i1);
    }
    for (;;)
    {
      return i2;
      localObject = l;
      if (localObject != null)
      {
        localObject = l;
        bool2 = ((Set)localObject).contains(paramNode);
        if (!bool2)
        {
          bool2 = false;
          localObject = null;
          continue;
        }
      }
      boolean bool2 = i1;
    }
  }
  
  abstract Iterator b(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable);
  
  final void b(Node paramNode1, Node paramNode2)
  {
    int i1 = b(paramNode1);
    int i3 = -1;
    float f1 = 0.0F / 0.0F;
    if (i1 == i3) {
      return;
    }
    NameSpaceSymbTable localNameSpaceSymbTable = new org/apache/xml/security/c14n/implementations/NameSpaceSymbTable;
    localNameSpaceSymbTable.<init>();
    if (paramNode1 != null)
    {
      i1 = 1;
      f2 = Float.MIN_VALUE;
      i3 = paramNode1.getNodeType();
      if (i1 == i3)
      {
        localObject1 = paramNode1;
        localObject1 = (Element)paramNode1;
        d((Element)localObject1, localNameSpaceSymbTable);
      }
    }
    int i4 = 0;
    float f3 = 0.0F;
    Object localObject2 = null;
    Object localObject3 = null;
    OutputStream localOutputStream = n;
    i1 = -1;
    float f2 = 0.0F / 0.0F;
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject4 = null;
    i3 = i1;
    f1 = f2;
    Object localObject1 = paramNode1;
    label120:
    int i6 = ((Node)localObject1).getNodeType();
    switch (i6)
    {
    case 5: 
    case 10: 
    default: 
      paramNode1 = (Node)localObject2;
      i4 = i3;
      f3 = f1;
      localObject3 = localObject4;
    }
    for (;;)
    {
      label207:
      if ((paramNode1 == null) && (localObject3 != null))
      {
        boolean bool1 = c((Node)localObject3);
        int i2;
        if (bool1)
        {
          localObject1 = t;
          localOutputStream.write((byte[])localObject1);
          localObject1 = localObject3;
          localObject1 = ((Element)localObject3).getTagName();
          UtfHelpper.a((String)localObject1, localOutputStream, localHashMap);
          i2 = 62;
          f2 = 8.7E-44F;
          localOutputStream.write(i2);
          localNameSpaceSymbTable.b();
        }
        while (localObject3 != paramNode2)
        {
          paramNode1 = ((Node)localObject3).getNextSibling();
          localObject4 = ((Node)localObject3).getParentNode();
          if (localObject4 != null)
          {
            i2 = 1;
            f2 = Float.MIN_VALUE;
            i3 = ((Node)localObject4).getNodeType();
            if (i2 == i3) {
              break label1090;
            }
          }
          localObject4 = null;
          i4 = 1;
          f3 = Float.MIN_VALUE;
          i3 = 0;
          localObject3 = null;
          f1 = 0.0F;
          break label207;
          localObject1 = new org/apache/xml/security/c14n/CanonicalizationException;
          ((CanonicalizationException)localObject1).<init>("empty");
          throw ((Throwable)localObject1);
          localNameSpaceSymbTable.a();
          localObject2 = ((Node)localObject1).getFirstChild();
          paramNode1 = (Node)localObject2;
          i4 = i3;
          f3 = f1;
          localObject3 = localObject4;
          break label207;
          boolean bool3 = k;
          if (!bool3) {
            break label1072;
          }
          int i7 = localNameSpaceSymbTable.f();
          i7 = a((Node)localObject1, i7);
          int i9 = 1;
          if (i7 != i9) {
            break label1072;
          }
          localObject1 = (Comment)localObject1;
          a((Comment)localObject1, localOutputStream, i3);
          paramNode1 = (Node)localObject2;
          i4 = i3;
          f3 = f1;
          localObject3 = localObject4;
          break label207;
          boolean bool4 = c((Node)localObject1);
          if (!bool4) {
            break label1072;
          }
          localObject1 = (ProcessingInstruction)localObject1;
          a((ProcessingInstruction)localObject1, localOutputStream, i3);
          paramNode1 = (Node)localObject2;
          i4 = i3;
          f3 = f1;
          localObject3 = localObject4;
          break label207;
          bool4 = c((Node)localObject1);
          if (!bool4) {
            break label1072;
          }
          Object localObject5 = ((Node)localObject1).getNodeValue();
          a((String)localObject5, localOutputStream);
          for (localObject1 = ((Node)localObject1).getNextSibling(); localObject1 != null; localObject1 = ((Node)localObject1).getNextSibling())
          {
            i8 = ((Node)localObject1).getNodeType();
            i9 = 3;
            if (i8 != i9)
            {
              i8 = ((Node)localObject1).getNodeType();
              i9 = 4;
              if (i8 != i9) {
                break;
              }
            }
            a(((Node)localObject1).getNodeValue(), localOutputStream);
            localObject2 = ((Node)localObject1).getNextSibling();
          }
          localObject3 = localObject1;
          localObject3 = (Element)localObject1;
          i4 = 0;
          f3 = 0.0F;
          localObject2 = null;
          int i8 = localNameSpaceSymbTable.f();
          i8 = a((Node)localObject1, i8);
          i9 = -1;
          if (i8 == i9)
          {
            localObject2 = ((Node)localObject1).getNextSibling();
            localObject3 = localObject4;
            paramNode1 = (Node)localObject2;
            i4 = 0;
            localObject2 = null;
            f3 = 0.0F;
            break label207;
          }
          i9 = 1;
          if (i8 == i9)
          {
            i8 = 1;
            i9 = i8;
            if (i9 == 0) {
              break label859;
            }
            localNameSpaceSymbTable.a();
            i4 = 60;
            f3 = 8.4E-44F;
            localOutputStream.write(i4);
            localObject2 = ((Element)localObject3).getTagName();
            UtfHelpper.a((String)localObject2, localOutputStream, localHashMap);
          }
          for (localObject5 = localObject2;; localObject5 = null)
          {
            Iterator localIterator = b((Element)localObject3, localNameSpaceSymbTable);
            if (localIterator == null) {
              break label873;
            }
            for (;;)
            {
              boolean bool2 = localIterator.hasNext();
              if (!bool2) {
                break;
              }
              localObject2 = (Attr)localIterator.next();
              String str = ((Attr)localObject2).getNodeName();
              localObject2 = ((Attr)localObject2).getNodeValue();
              a(str, (String)localObject2, localOutputStream, localHashMap);
            }
            i8 = 0;
            localObject5 = null;
            i9 = 0;
            break;
            label859:
            localNameSpaceSymbTable.c();
            i8 = 0;
          }
          label873:
          if (i9 != 0)
          {
            i5 = 62;
            f3 = 8.7E-44F;
            localOutputStream.write(i5);
          }
          localObject2 = ((Node)localObject1).getFirstChild();
          if (localObject2 == null)
          {
            if (i9 != 0)
            {
              localObject3 = t;
              localOutputStream.write((byte[])localObject3);
              UtfHelpper.a((String)localObject5, localOutputStream, localHashMap);
              i3 = 62;
              f1 = 8.7E-44F;
              localOutputStream.write(i3);
              localNameSpaceSymbTable.b();
            }
            for (;;)
            {
              if (localObject4 == null) {
                break label1053;
              }
              localObject2 = ((Node)localObject1).getNextSibling();
              localObject3 = localObject4;
              paramNode1 = (Node)localObject2;
              i5 = 0;
              localObject2 = null;
              f3 = 0.0F;
              break;
              localNameSpaceSymbTable.d();
            }
          }
          paramNode1 = (Node)localObject2;
          i5 = 0;
          localObject2 = null;
          f3 = 0.0F;
          break label207;
          localNameSpaceSymbTable.d();
        }
      }
      if (paramNode1 == null) {
        break;
      }
      localObject1 = paramNode1.getNextSibling();
      localObject4 = localObject3;
      i3 = i5;
      f1 = f3;
      localObject2 = localObject1;
      localObject1 = paramNode1;
      break label120;
      label1053:
      localObject3 = localObject4;
      paramNode1 = (Node)localObject2;
      int i5 = 0;
      localObject2 = null;
      f3 = 0.0F;
      continue;
      label1072:
      paramNode1 = (Node)localObject2;
      i5 = i3;
      f3 = f1;
      localObject3 = localObject4;
      continue;
      label1090:
      localObject3 = localObject4;
    }
  }
  
  /* Error */
  public byte[] b(XMLSignatureInput paramXMLSignatureInput)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 341	org/apache/xml/security/signature/XMLSignatureInput:n	()Z
    //   4: istore_2
    //   5: iload_2
    //   6: ifeq +12 -> 18
    //   9: iconst_0
    //   10: istore_2
    //   11: aconst_null
    //   12: astore_3
    //   13: aload_0
    //   14: iconst_0
    //   15: putfield 141	org/apache/xml/security/c14n/implementations/CanonicalizerBase:k	Z
    //   18: aload_1
    //   19: invokevirtual 343	org/apache/xml/security/signature/XMLSignatureInput:h	()Z
    //   22: istore_2
    //   23: iload_2
    //   24: ifeq +16 -> 40
    //   27: aload_1
    //   28: invokevirtual 345	org/apache/xml/security/signature/XMLSignatureInput:e	()[B
    //   31: astore_3
    //   32: aload_0
    //   33: aload_3
    //   34: invokevirtual 348	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	([B)[B
    //   37: astore_3
    //   38: aload_3
    //   39: areturn
    //   40: aload_1
    //   41: invokevirtual 350	org/apache/xml/security/signature/XMLSignatureInput:g	()Z
    //   44: istore_2
    //   45: iload_2
    //   46: ifeq +25 -> 71
    //   49: aload_1
    //   50: invokevirtual 352	org/apache/xml/security/signature/XMLSignatureInput:m	()Lorg/w3c/dom/Node;
    //   53: astore_3
    //   54: aload_1
    //   55: invokevirtual 354	org/apache/xml/security/signature/XMLSignatureInput:l	()Lorg/w3c/dom/Node;
    //   58: astore 4
    //   60: aload_0
    //   61: aload_3
    //   62: aload 4
    //   64: invokevirtual 316	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)[B
    //   67: astore_3
    //   68: goto -30 -> 38
    //   71: aload_1
    //   72: invokevirtual 356	org/apache/xml/security/signature/XMLSignatureInput:f	()Z
    //   75: istore_2
    //   76: iload_2
    //   77: ifeq +55 -> 132
    //   80: aload_1
    //   81: invokevirtual 359	org/apache/xml/security/signature/XMLSignatureInput:p	()Ljava/util/List;
    //   84: astore_3
    //   85: aload_0
    //   86: aload_3
    //   87: putfield 220	org/apache/xml/security/c14n/implementations/CanonicalizerBase:j	Ljava/util/List;
    //   90: aload_0
    //   91: aload_1
    //   92: invokevirtual 362	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	(Lorg/apache/xml/security/signature/XMLSignatureInput;)V
    //   95: aload_1
    //   96: invokevirtual 352	org/apache/xml/security/signature/XMLSignatureInput:m	()Lorg/w3c/dom/Node;
    //   99: astore_3
    //   100: aload_3
    //   101: ifnull +17 -> 118
    //   104: aload_1
    //   105: invokevirtual 352	org/apache/xml/security/signature/XMLSignatureInput:m	()Lorg/w3c/dom/Node;
    //   108: astore_3
    //   109: aload_0
    //   110: aload_3
    //   111: invokespecial 313	org/apache/xml/security/c14n/implementations/CanonicalizerBase:d	(Lorg/w3c/dom/Node;)[B
    //   114: astore_3
    //   115: goto -77 -> 38
    //   118: aload_1
    //   119: invokevirtual 365	org/apache/xml/security/signature/XMLSignatureInput:b	()Ljava/util/Set;
    //   122: astore_3
    //   123: aload_0
    //   124: aload_3
    //   125: invokevirtual 368	org/apache/xml/security/c14n/implementations/CanonicalizerBase:a	(Ljava/util/Set;)[B
    //   128: astore_3
    //   129: goto -91 -> 38
    //   132: iconst_0
    //   133: istore_2
    //   134: aconst_null
    //   135: astore_3
    //   136: goto -98 -> 38
    //   139: astore_3
    //   140: new 209	org/apache/xml/security/c14n/CanonicalizationException
    //   143: astore 4
    //   145: aload 4
    //   147: ldc -45
    //   149: aload_3
    //   150: invokespecial 214	org/apache/xml/security/c14n/CanonicalizationException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   153: aload 4
    //   155: athrow
    //   156: astore_3
    //   157: new 209	org/apache/xml/security/c14n/CanonicalizationException
    //   160: astore 4
    //   162: aload 4
    //   164: ldc -45
    //   166: aload_3
    //   167: invokespecial 214	org/apache/xml/security/c14n/CanonicalizationException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   170: aload 4
    //   172: athrow
    //   173: astore_3
    //   174: new 209	org/apache/xml/security/c14n/CanonicalizationException
    //   177: astore 4
    //   179: aload 4
    //   181: ldc -45
    //   183: aload_3
    //   184: invokespecial 214	org/apache/xml/security/c14n/CanonicalizationException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   187: aload 4
    //   189: athrow
    //   190: astore_3
    //   191: new 209	org/apache/xml/security/c14n/CanonicalizationException
    //   194: astore 4
    //   196: aload 4
    //   198: ldc -45
    //   200: aload_3
    //   201: invokespecial 214	org/apache/xml/security/c14n/CanonicalizationException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   204: aload 4
    //   206: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	207	0	this	CanonicalizerBase
    //   0	207	1	paramXMLSignatureInput	XMLSignatureInput
    //   4	130	2	bool	boolean
    //   12	124	3	localObject1	Object
    //   139	11	3	localCanonicalizationException	CanonicalizationException
    //   156	11	3	localParserConfigurationException	javax.xml.parsers.ParserConfigurationException
    //   173	11	3	localIOException	java.io.IOException
    //   190	11	3	localSAXException	org.xml.sax.SAXException
    //   58	147	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   0	4	139	org/apache/xml/security/c14n/CanonicalizationException
    //   14	18	139	org/apache/xml/security/c14n/CanonicalizationException
    //   18	22	139	org/apache/xml/security/c14n/CanonicalizationException
    //   27	31	139	org/apache/xml/security/c14n/CanonicalizationException
    //   33	37	139	org/apache/xml/security/c14n/CanonicalizationException
    //   40	44	139	org/apache/xml/security/c14n/CanonicalizationException
    //   49	53	139	org/apache/xml/security/c14n/CanonicalizationException
    //   54	58	139	org/apache/xml/security/c14n/CanonicalizationException
    //   62	67	139	org/apache/xml/security/c14n/CanonicalizationException
    //   71	75	139	org/apache/xml/security/c14n/CanonicalizationException
    //   80	84	139	org/apache/xml/security/c14n/CanonicalizationException
    //   86	90	139	org/apache/xml/security/c14n/CanonicalizationException
    //   91	95	139	org/apache/xml/security/c14n/CanonicalizationException
    //   95	99	139	org/apache/xml/security/c14n/CanonicalizationException
    //   104	108	139	org/apache/xml/security/c14n/CanonicalizationException
    //   110	114	139	org/apache/xml/security/c14n/CanonicalizationException
    //   118	122	139	org/apache/xml/security/c14n/CanonicalizationException
    //   124	128	139	org/apache/xml/security/c14n/CanonicalizationException
    //   0	4	156	javax/xml/parsers/ParserConfigurationException
    //   14	18	156	javax/xml/parsers/ParserConfigurationException
    //   18	22	156	javax/xml/parsers/ParserConfigurationException
    //   27	31	156	javax/xml/parsers/ParserConfigurationException
    //   33	37	156	javax/xml/parsers/ParserConfigurationException
    //   40	44	156	javax/xml/parsers/ParserConfigurationException
    //   49	53	156	javax/xml/parsers/ParserConfigurationException
    //   54	58	156	javax/xml/parsers/ParserConfigurationException
    //   62	67	156	javax/xml/parsers/ParserConfigurationException
    //   71	75	156	javax/xml/parsers/ParserConfigurationException
    //   80	84	156	javax/xml/parsers/ParserConfigurationException
    //   86	90	156	javax/xml/parsers/ParserConfigurationException
    //   91	95	156	javax/xml/parsers/ParserConfigurationException
    //   95	99	156	javax/xml/parsers/ParserConfigurationException
    //   104	108	156	javax/xml/parsers/ParserConfigurationException
    //   110	114	156	javax/xml/parsers/ParserConfigurationException
    //   118	122	156	javax/xml/parsers/ParserConfigurationException
    //   124	128	156	javax/xml/parsers/ParserConfigurationException
    //   0	4	173	java/io/IOException
    //   14	18	173	java/io/IOException
    //   18	22	173	java/io/IOException
    //   27	31	173	java/io/IOException
    //   33	37	173	java/io/IOException
    //   40	44	173	java/io/IOException
    //   49	53	173	java/io/IOException
    //   54	58	173	java/io/IOException
    //   62	67	173	java/io/IOException
    //   71	75	173	java/io/IOException
    //   80	84	173	java/io/IOException
    //   86	90	173	java/io/IOException
    //   91	95	173	java/io/IOException
    //   95	99	173	java/io/IOException
    //   104	108	173	java/io/IOException
    //   110	114	173	java/io/IOException
    //   118	122	173	java/io/IOException
    //   124	128	173	java/io/IOException
    //   0	4	190	org/xml/sax/SAXException
    //   14	18	190	org/xml/sax/SAXException
    //   18	22	190	org/xml/sax/SAXException
    //   27	31	190	org/xml/sax/SAXException
    //   33	37	190	org/xml/sax/SAXException
    //   40	44	190	org/xml/sax/SAXException
    //   49	53	190	org/xml/sax/SAXException
    //   54	58	190	org/xml/sax/SAXException
    //   62	67	190	org/xml/sax/SAXException
    //   71	75	190	org/xml/sax/SAXException
    //   80	84	190	org/xml/sax/SAXException
    //   86	90	190	org/xml/sax/SAXException
    //   91	95	190	org/xml/sax/SAXException
    //   95	99	190	org/xml/sax/SAXException
    //   104	108	190	org/xml/sax/SAXException
    //   110	114	190	org/xml/sax/SAXException
    //   118	122	190	org/xml/sax/SAXException
    //   124	128	190	org/xml/sax/SAXException
  }
  
  void c(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    boolean bool1 = paramElement.hasAttributes();
    if (!bool1) {
      return;
    }
    NamedNodeMap localNamedNodeMap = paramElement.getAttributes();
    int i2 = localNamedNodeMap.getLength();
    bool1 = false;
    Attr localAttr = null;
    int i3 = 0;
    label37:
    String str1;
    String str2;
    if (i3 < i2)
    {
      localAttr = (Attr)localNamedNodeMap.item(i3);
      str1 = "http://www.w3.org/2000/xmlns/";
      str2 = localAttr.getNamespaceURI();
      boolean bool2 = str1.equals(str2);
      if (bool2) {
        break label96;
      }
    }
    for (;;)
    {
      int i1 = i3 + 1;
      i3 = i1;
      break label37;
      break;
      label96:
      str1 = localAttr.getLocalName();
      str2 = localAttr.getNodeValue();
      String str3 = "xml";
      boolean bool3 = str3.equals(str1);
      if (bool3)
      {
        str3 = "http://www.w3.org/XML/1998/namespace";
        bool3 = str3.equals(str2);
        if (bool3) {}
      }
      else
      {
        paramNameSpaceSymbTable.a(str1, str2, localAttr);
      }
    }
  }
  
  boolean c(Node paramNode)
  {
    int i1 = 1;
    Object localObject = j;
    int i2;
    if (localObject != null)
    {
      localObject = j;
      Iterator localIterator = ((List)localObject).iterator();
      do
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject = (NodeFilter)localIterator.next();
        i2 = ((NodeFilter)localObject).a(paramNode);
      } while (i2 == i1);
      i2 = 0;
      localObject = null;
    }
    for (;;)
    {
      return i2;
      localObject = l;
      if (localObject != null)
      {
        localObject = l;
        bool2 = ((Set)localObject).contains(paramNode);
        if (!bool2)
        {
          bool2 = false;
          localObject = null;
          continue;
        }
      }
      boolean bool2 = i1;
    }
  }
  
  final void d(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    int i1 = 1;
    Object localObject1 = new java/util/ArrayList;
    int i2 = 10;
    ((ArrayList)localObject1).<init>(i2);
    Object localObject2 = paramElement.getParentNode();
    int i3;
    if (localObject2 != null)
    {
      i3 = ((Node)localObject2).getNodeType();
      if (i1 == i3) {
        break label47;
      }
    }
    for (;;)
    {
      return;
      label47:
      boolean bool;
      for (localObject2 = (Element)localObject2;; localObject2 = (Element)localObject2)
      {
        if (localObject2 != null)
        {
          ((List)localObject1).add(localObject2);
          localObject2 = ((Element)localObject2).getParentNode();
          if (localObject2 != null)
          {
            i3 = ((Node)localObject2).getNodeType();
            if (i1 == i3) {
              continue;
            }
          }
        }
        i2 = ((List)localObject1).size();
        localObject1 = ((List)localObject1).listIterator(i2);
        for (;;)
        {
          bool = ((ListIterator)localObject1).hasPrevious();
          if (!bool) {
            break;
          }
          localObject2 = (Element)((ListIterator)localObject1).previous();
          c((Element)localObject2, paramNameSpaceSymbTable);
        }
      }
      localObject2 = paramNameSpaceSymbTable.b("xmlns");
      if (localObject2 != null)
      {
        localObject1 = "";
        localObject2 = ((Attr)localObject2).getValue();
        bool = ((String)localObject1).equals(localObject2);
        if (bool)
        {
          localObject2 = "xmlns";
          localObject1 = "";
          Attr localAttr = i;
          paramNameSpaceSymbTable.b((String)localObject2, (String)localObject1, localAttr);
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/CanonicalizerBase.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */