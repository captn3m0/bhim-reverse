package org.apache.xml.security.c14n.implementations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.w3c.dom.Attr;
import org.w3c.dom.Node;

public class NameSpaceSymbTable
{
  static final SymbMap e;
  SymbMap a;
  int b = 0;
  List c;
  boolean d = true;
  
  static
  {
    Object localObject = new org/apache/xml/security/c14n/implementations/SymbMap;
    ((SymbMap)localObject).<init>();
    e = (SymbMap)localObject;
    localObject = new org/apache/xml/security/c14n/implementations/NameSpaceSymbEntry;
    ((NameSpaceSymbEntry)localObject).<init>("", null, true, "xmlns");
    d = "";
    e.a("xmlns", (NameSpaceSymbEntry)localObject);
  }
  
  public NameSpaceSymbTable()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(10);
    c = ((List)localObject);
    localObject = (SymbMap)e.clone();
    a = ((SymbMap)localObject);
  }
  
  public Attr a(String paramString)
  {
    Object localObject1 = null;
    Object localObject2 = a.a(paramString);
    if (localObject2 == null) {}
    for (;;)
    {
      return (Attr)localObject1;
      boolean bool = e;
      if (!bool)
      {
        localObject1 = (NameSpaceSymbEntry)((NameSpaceSymbEntry)localObject2).clone();
        e();
        a.a(paramString, (NameSpaceSymbEntry)localObject1);
        e = true;
        int i = b;
        a = i;
        localObject2 = c;
        d = ((String)localObject2);
        localObject1 = f;
      }
    }
  }
  
  public void a()
  {
    int i = b + 1;
    b = i;
    c();
  }
  
  public void a(Collection paramCollection)
  {
    Object localObject1 = a.a();
    Iterator localIterator = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (NameSpaceSymbEntry)localIterator.next();
      boolean bool2 = e;
      if (!bool2)
      {
        Object localObject2 = f;
        if (localObject2 != null)
        {
          localObject1 = (NameSpaceSymbEntry)((NameSpaceSymbEntry)localObject1).clone();
          e();
          localObject2 = a;
          String str = b;
          ((SymbMap)localObject2).a(str, (NameSpaceSymbEntry)localObject1);
          localObject2 = c;
          d = ((String)localObject2);
          bool2 = true;
          e = bool2;
          localObject1 = f;
          paramCollection.add(localObject1);
        }
      }
    }
  }
  
  public boolean a(String paramString1, String paramString2, Attr paramAttr)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    Object localObject1 = null;
    NameSpaceSymbEntry localNameSpaceSymbEntry = a.a(paramString1);
    Object localObject2;
    if (localNameSpaceSymbEntry != null)
    {
      localObject2 = c;
      boolean bool3 = paramString2.equals(localObject2);
      if (!bool3) {}
    }
    for (;;)
    {
      return bool2;
      localObject2 = new org/apache/xml/security/c14n/implementations/NameSpaceSymbEntry;
      ((NameSpaceSymbEntry)localObject2).<init>(paramString2, paramAttr, false, paramString1);
      e();
      localObject1 = a;
      ((SymbMap)localObject1).a(paramString1, (NameSpaceSymbEntry)localObject2);
      if (localNameSpaceSymbEntry != null)
      {
        localObject1 = d;
        d = ((String)localObject1);
        localObject1 = d;
        if (localObject1 != null)
        {
          localObject1 = d;
          bool2 = ((String)localObject1).equals(paramString2);
          if (bool2) {
            e = bool1;
          }
        }
      }
      bool2 = bool1;
    }
  }
  
  public Attr b(String paramString)
  {
    Attr localAttr = null;
    NameSpaceSymbEntry localNameSpaceSymbEntry = a.a(paramString);
    if (localNameSpaceSymbEntry == null) {}
    for (;;)
    {
      return localAttr;
      boolean bool = e;
      if (!bool) {
        localAttr = f;
      }
    }
  }
  
  public Node b(String paramString1, String paramString2, Attr paramAttr)
  {
    Object localObject1 = null;
    boolean bool1 = true;
    Object localObject2 = a.a(paramString1);
    Object localObject3;
    if (localObject2 != null)
    {
      localObject3 = c;
      boolean bool2 = paramString2.equals(localObject3);
      if (bool2)
      {
        bool2 = e;
        if (!bool2)
        {
          localObject1 = (NameSpaceSymbEntry)((NameSpaceSymbEntry)localObject2).clone();
          e();
          localObject2 = a;
          ((SymbMap)localObject2).a(paramString1, (NameSpaceSymbEntry)localObject1);
          d = paramString2;
          e = bool1;
          localObject1 = f;
        }
      }
    }
    for (;;)
    {
      return (Node)localObject1;
      localObject3 = new org/apache/xml/security/c14n/implementations/NameSpaceSymbEntry;
      ((NameSpaceSymbEntry)localObject3).<init>(paramString2, paramAttr, bool1, paramString1);
      d = paramString2;
      e();
      Object localObject4 = a;
      ((SymbMap)localObject4).a(paramString1, (NameSpaceSymbEntry)localObject3);
      if (localObject2 != null)
      {
        localObject4 = d;
        if (localObject4 != null)
        {
          localObject2 = d;
          boolean bool3 = ((String)localObject2).equals(paramString2);
          if (bool3)
          {
            e = bool1;
            continue;
          }
        }
      }
      localObject1 = f;
    }
  }
  
  public void b()
  {
    int i = b + -1;
    b = i;
    d();
  }
  
  public void c()
  {
    c.add(null);
    d = false;
  }
  
  public void c(String paramString)
  {
    Object localObject = a.a(paramString);
    if (localObject != null)
    {
      e();
      localObject = a;
      ((SymbMap)localObject).a(paramString, null);
    }
  }
  
  public void d()
  {
    int i = c.size();
    int j = i + -1;
    Object localObject = c.remove(j);
    if (localObject != null)
    {
      localObject = (SymbMap)localObject;
      a = ((SymbMap)localObject);
      if (j == 0) {
        d = false;
      }
    }
    for (;;)
    {
      return;
      localObject = c;
      j += -1;
      localObject = ((List)localObject).get(j);
      SymbMap localSymbMap = a;
      if (localObject != localSymbMap) {
        i = 1;
      }
      for (;;)
      {
        d = i;
        break;
        i = 0;
        localObject = null;
      }
      d = false;
    }
  }
  
  public void d(String paramString)
  {
    Object localObject = a.a(paramString);
    if (localObject != null)
    {
      boolean bool = e;
      if (!bool)
      {
        e();
        localObject = a;
        ((SymbMap)localObject).a(paramString, null);
      }
    }
  }
  
  final void e()
  {
    boolean bool = d;
    if (!bool)
    {
      Object localObject = c;
      List localList = c;
      int i = localList.size() + -1;
      SymbMap localSymbMap = a;
      ((List)localObject).set(i, localSymbMap);
      localObject = (SymbMap)a.clone();
      a = ((SymbMap)localObject);
      bool = true;
      d = bool;
    }
  }
  
  public boolean e(String paramString)
  {
    Object localObject = a.a(paramString);
    if (localObject != null)
    {
      boolean bool = e;
      if (bool)
      {
        e();
        localObject = a;
        ((SymbMap)localObject).a(paramString, null);
      }
    }
    return false;
  }
  
  public int f()
  {
    return c.size();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/NameSpaceSymbTable.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */