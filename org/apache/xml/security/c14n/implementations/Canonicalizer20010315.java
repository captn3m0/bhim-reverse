package org.apache.xml.security.c14n.implementations;

import java.util.Collection;
import java.util.Iterator;
import java.util.SortedSet;
import java.util.TreeSet;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.helper.AttrCompare;
import org.apache.xml.security.c14n.helper.C14nHelper;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public abstract class Canonicalizer20010315
  extends CanonicalizerBase
{
  boolean b = true;
  final SortedSet c;
  Canonicalizer20010315.XmlAttrStack d;
  
  public Canonicalizer20010315(boolean paramBoolean)
  {
    super(paramBoolean);
    Object localObject = new java/util/TreeSet;
    AttrCompare localAttrCompare = g;
    ((TreeSet)localObject).<init>(localAttrCompare);
    c = ((SortedSet)localObject);
    localObject = new org/apache/xml/security/c14n/implementations/Canonicalizer20010315$XmlAttrStack;
    ((Canonicalizer20010315.XmlAttrStack)localObject).<init>();
    d = ((Canonicalizer20010315.XmlAttrStack)localObject);
  }
  
  Iterator a(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    boolean bool1 = paramElement.hasAttributes();
    if (!bool1)
    {
      bool1 = b;
      if (!bool1) {
        bool1 = false;
      }
    }
    Object localObject2;
    for (Object localObject1 = null;; localObject1 = ((SortedSet)localObject2).iterator())
    {
      return (Iterator)localObject1;
      localObject2 = c;
      ((SortedSet)localObject2).clear();
      NamedNodeMap localNamedNodeMap = paramElement.getAttributes();
      int j = localNamedNodeMap.getLength();
      int k = 0;
      Object[] arrayOfObject = null;
      if (k < j)
      {
        localObject1 = (Attr)localNamedNodeMap.item(k);
        String str1 = ((Attr)localObject1).getNamespaceURI();
        Object localObject3 = "http://www.w3.org/2000/xmlns/";
        boolean bool3 = ((String)localObject3).equals(str1);
        if (!bool3) {
          ((SortedSet)localObject2).add(localObject1);
        }
        label187:
        boolean bool5;
        do
        {
          do
          {
            boolean bool4;
            do
            {
              int i = k + 1;
              k = i;
              break;
              str1 = ((Attr)localObject1).getLocalName();
              localObject3 = ((Attr)localObject1).getValue();
              String str2 = "xml";
              bool4 = str2.equals(str1);
              if (!bool4) {
                break label187;
              }
              str2 = "http://www.w3.org/XML/1998/namespace";
              bool4 = str2.equals(localObject3);
            } while (bool4);
            localObject3 = paramNameSpaceSymbTable.b(str1, (String)localObject3, (Attr)localObject1);
          } while (localObject3 == null);
          ((SortedSet)localObject2).add(localObject3);
          bool5 = C14nHelper.a((Attr)localObject1);
        } while (!bool5);
        arrayOfObject = new Object[3];
        localObject2 = paramElement.getTagName();
        arrayOfObject[0] = localObject2;
        arrayOfObject[1] = str1;
        localObject1 = ((Attr)localObject1).getNodeValue();
        arrayOfObject[2] = localObject1;
        localObject1 = new org/apache/xml/security/c14n/CanonicalizationException;
        ((CanonicalizationException)localObject1).<init>("c14n.Canonicalizer.RelativeNamespace", arrayOfObject);
        throw ((Throwable)localObject1);
      }
      boolean bool2 = b;
      if (bool2)
      {
        paramNameSpaceSymbTable.a((Collection)localObject2);
        localObject1 = d;
        ((Canonicalizer20010315.XmlAttrStack)localObject1).a((Collection)localObject2);
        b = false;
      }
    }
  }
  
  void a(XMLSignatureInput paramXMLSignatureInput)
  {
    boolean bool = paramXMLSignatureInput.a();
    if (!bool) {
      return;
    }
    Object localObject = paramXMLSignatureInput.m();
    if (localObject != null) {}
    for (localObject = XMLUtils.b(paramXMLSignatureInput.m());; localObject = XMLUtils.a(paramXMLSignatureInput.b()))
    {
      XMLUtils.a((Document)localObject);
      break;
    }
  }
  
  public byte[] a(Node paramNode, String paramString)
  {
    CanonicalizationException localCanonicalizationException = new org/apache/xml/security/c14n/CanonicalizationException;
    localCanonicalizationException.<init>("c14n.Canonicalizer.UnsupportedOperation");
    throw localCanonicalizationException;
  }
  
  Iterator b(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    Object localObject1 = null;
    int i = 1;
    Object localObject2 = d;
    int j = paramNameSpaceSymbTable.f();
    ((Canonicalizer20010315.XmlAttrStack)localObject2).a(j);
    int k = paramNameSpaceSymbTable.f();
    k = a(paramElement, k);
    Object localObject3;
    int m;
    Object localObject4;
    int n;
    if (k == i)
    {
      j = i;
      boolean bool1 = paramElement.hasAttributes();
      if (!bool1) {
        break label569;
      }
      localObject3 = paramElement.getAttributes();
      m = ((NamedNodeMap)localObject3).getLength();
      localObject4 = localObject3;
      n = m;
    }
    for (;;)
    {
      SortedSet localSortedSet = c;
      localSortedSet.clear();
      int i1 = 0;
      label104:
      Object localObject7;
      if (i1 < n)
      {
        localObject2 = (Attr)((NamedNodeMap)localObject4).item(i1);
        Object localObject5 = ((Attr)localObject2).getNamespaceURI();
        Object localObject6 = "http://www.w3.org/2000/xmlns/";
        boolean bool3 = ((String)localObject6).equals(localObject5);
        if (!bool3)
        {
          localObject6 = "http://www.w3.org/XML/1998/namespace";
          boolean bool4 = ((String)localObject6).equals(localObject5);
          if (bool4)
          {
            localObject5 = d;
            ((Canonicalizer20010315.XmlAttrStack)localObject5).a((Attr)localObject2);
          }
        }
        for (;;)
        {
          m = i1 + 1;
          i1 = m;
          break label104;
          j = 0;
          localObject7 = null;
          break;
          if (j != 0)
          {
            localSortedSet.add(localObject2);
            continue;
            localObject5 = ((Attr)localObject2).getLocalName();
            localObject6 = ((Attr)localObject2).getValue();
            String str = "xml";
            boolean bool5 = str.equals(localObject5);
            if (bool5)
            {
              str = "http://www.w3.org/XML/1998/namespace";
              bool5 = str.equals(localObject6);
              if (bool5) {}
            }
            else
            {
              bool5 = c((Node)localObject2);
              if (bool5)
              {
                if (j == 0)
                {
                  bool5 = paramNameSpaceSymbTable.e((String)localObject5);
                  if (bool5) {}
                }
                else
                {
                  localObject6 = paramNameSpaceSymbTable.b((String)localObject5, (String)localObject6, (Attr)localObject2);
                  if (localObject6 != null)
                  {
                    localSortedSet.add(localObject6);
                    bool3 = C14nHelper.a((Attr)localObject2);
                    if (bool3)
                    {
                      localObject7 = new Object[3];
                      localObject3 = paramElement.getTagName();
                      localObject7[0] = localObject3;
                      localObject7[i] = localObject5;
                      localObject2 = ((Attr)localObject2).getNodeValue();
                      localObject7[2] = localObject2;
                      localObject2 = new org/apache/xml/security/c14n/CanonicalizationException;
                      ((CanonicalizationException)localObject2).<init>("c14n.Canonicalizer.RelativeNamespace", (Object[])localObject7);
                      throw ((Throwable)localObject2);
                    }
                  }
                }
              }
              else
              {
                if (j != 0)
                {
                  str = "xmlns";
                  bool5 = str.equals(localObject5);
                  if (!bool5)
                  {
                    paramNameSpaceSymbTable.c((String)localObject5);
                    continue;
                  }
                }
                paramNameSpaceSymbTable.a((String)localObject5, (String)localObject6, (Attr)localObject2);
              }
            }
          }
        }
      }
      if (j != 0)
      {
        localObject7 = "xmlns";
        localObject2 = paramElement.getAttributeNodeNS("http://www.w3.org/2000/xmlns/", (String)localObject7);
        if (localObject2 != null) {
          break label529;
        }
        localObject2 = "xmlns";
        localObject1 = paramNameSpaceSymbTable.a((String)localObject2);
      }
      for (;;)
      {
        if (localObject1 != null) {
          localSortedSet.add(localObject1);
        }
        localObject2 = d;
        ((Canonicalizer20010315.XmlAttrStack)localObject2).a(localSortedSet);
        paramNameSpaceSymbTable.a(localSortedSet);
        return localSortedSet.iterator();
        label529:
        boolean bool2 = c((Node)localObject2);
        if (!bool2)
        {
          localObject2 = "xmlns";
          localObject7 = "";
          Attr localAttr = i;
          localObject1 = paramNameSpaceSymbTable.b((String)localObject2, (String)localObject7, localAttr);
        }
      }
      label569:
      n = 0;
      localObject3 = null;
      localObject4 = null;
    }
  }
  
  void c(Element paramElement, NameSpaceSymbTable paramNameSpaceSymbTable)
  {
    boolean bool1 = paramElement.hasAttributes();
    if (!bool1) {
      return;
    }
    d.a(-1);
    NamedNodeMap localNamedNodeMap = paramElement.getAttributes();
    int j = localNamedNodeMap.getLength();
    bool1 = false;
    Attr localAttr = null;
    int k = 0;
    label45:
    Object localObject;
    String str1;
    if (k < j)
    {
      localAttr = (Attr)localNamedNodeMap.item(k);
      localObject = "http://www.w3.org/2000/xmlns/";
      str1 = localAttr.getNamespaceURI();
      boolean bool2 = ((String)localObject).equals(str1);
      if (bool2) {
        break label144;
      }
      localObject = "http://www.w3.org/XML/1998/namespace";
      str1 = localAttr.getNamespaceURI();
      bool2 = ((String)localObject).equals(str1);
      if (bool2)
      {
        localObject = d;
        ((Canonicalizer20010315.XmlAttrStack)localObject).a(localAttr);
      }
    }
    for (;;)
    {
      int i = k + 1;
      k = i;
      break label45;
      break;
      label144:
      localObject = localAttr.getLocalName();
      str1 = localAttr.getNodeValue();
      String str2 = "xml";
      boolean bool3 = str2.equals(localObject);
      if (bool3)
      {
        str2 = "http://www.w3.org/XML/1998/namespace";
        bool3 = str2.equals(str1);
        if (bool3) {}
      }
      else
      {
        paramNameSpaceSymbTable.a((String)localObject, str1, localAttr);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/c14n/implementations/Canonicalizer20010315.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */