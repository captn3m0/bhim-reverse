package org.apache.xml.security.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;

public class XMLSecurityRuntimeException
  extends RuntimeException
{
  protected Exception a = null;
  protected String b;
  
  public XMLSecurityRuntimeException()
  {
    super("Missing message string");
    b = null;
    a = null;
  }
  
  public XMLSecurityRuntimeException(String paramString, Exception paramException)
  {
    super(str);
    b = paramString;
    a = paramException;
  }
  
  public XMLSecurityRuntimeException(String paramString, Object[] paramArrayOfObject, Exception paramException)
  {
    super(str);
    b = paramString;
    a = paramException;
  }
  
  public void printStackTrace()
  {
    synchronized (System.err)
    {
      Object localObject1 = System.err;
      super.printStackTrace((PrintStream)localObject1);
      localObject1 = a;
      if (localObject1 != null)
      {
        localObject1 = a;
        PrintStream localPrintStream2 = System.err;
        ((Exception)localObject1).printStackTrace(localPrintStream2);
      }
      return;
    }
  }
  
  public void printStackTrace(PrintStream paramPrintStream)
  {
    super.printStackTrace(paramPrintStream);
    Exception localException = a;
    if (localException != null)
    {
      localException = a;
      localException.printStackTrace(paramPrintStream);
    }
  }
  
  public void printStackTrace(PrintWriter paramPrintWriter)
  {
    super.printStackTrace(paramPrintWriter);
    Exception localException = a;
    if (localException != null)
    {
      localException = a;
      localException.printStackTrace(paramPrintWriter);
    }
  }
  
  public String toString()
  {
    Object localObject1 = getClass().getName();
    Object localObject2 = super.getLocalizedMessage();
    if (localObject2 != null)
    {
      Object localObject3 = new java/lang/StringBuffer;
      ((StringBuffer)localObject3).<init>();
      localObject1 = ((StringBuffer)localObject3).append((String)localObject1);
      localObject3 = ": ";
      localObject1 = (String)localObject3 + (String)localObject2;
    }
    localObject2 = a;
    if (localObject2 != null)
    {
      localObject2 = new java/lang/StringBuffer;
      ((StringBuffer)localObject2).<init>();
      localObject1 = ((StringBuffer)localObject2).append((String)localObject1).append("\nOriginal Exception was ");
      localObject2 = a.toString();
      localObject1 = (String)localObject2;
    }
    return (String)localObject1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/exceptions/XMLSecurityRuntimeException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */