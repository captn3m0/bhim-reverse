package org.apache.xml.security.exceptions;

public class AlgorithmAlreadyRegisteredException
  extends XMLSecurityException
{
  public AlgorithmAlreadyRegisteredException() {}
  
  public AlgorithmAlreadyRegisteredException(String paramString, Object[] paramArrayOfObject)
  {
    super(paramString, paramArrayOfObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */