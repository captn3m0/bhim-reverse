package org.apache.xml.security.exceptions;

public class Base64DecodingException
  extends XMLSecurityException
{
  public Base64DecodingException() {}
  
  public Base64DecodingException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/exceptions/Base64DecodingException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */