package org.apache.xml.security.keys.content;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.SignatureElementProxy;

public class X509Data
  extends SignatureElementProxy
  implements KeyInfoContent
{
  static Log a;
  static Class b;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.keys.content.X509Data");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = b;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public String e()
  {
    return "X509Data";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/content/X509Data.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */