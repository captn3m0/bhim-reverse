package org.apache.xml.security.keys.content;

import org.apache.xml.security.utils.SignatureElementProxy;

public class PGPData
  extends SignatureElementProxy
  implements KeyInfoContent
{
  public String e()
  {
    return "PGPData";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/content/PGPData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */