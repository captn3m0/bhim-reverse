package org.apache.xml.security.keys.content.x509;

import java.math.BigInteger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.RFC2253Parser;
import org.apache.xml.security.utils.SignatureElementProxy;

public class XMLX509IssuerSerial
  extends SignatureElementProxy
  implements XMLX509DataContent
{
  static Log a;
  static Class b;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.keys.content.x509.XMLX509IssuerSerial");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = b;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public BigInteger a()
  {
    String str1 = b("X509SerialNumber", "http://www.w3.org/2000/09/xmldsig#");
    Object localObject1 = a;
    boolean bool = ((Log)localObject1).isDebugEnabled();
    if (bool)
    {
      localObject1 = a;
      Object localObject2 = new java/lang/StringBuffer;
      ((StringBuffer)localObject2).<init>();
      String str2 = "X509SerialNumber text: ";
      localObject2 = str2 + str1;
      ((Log)localObject1).debug(localObject2);
    }
    localObject1 = new java/math/BigInteger;
    ((BigInteger)localObject1).<init>(str1);
    return (BigInteger)localObject1;
  }
  
  public String b()
  {
    return RFC2253Parser.a(b("X509IssuerName", "http://www.w3.org/2000/09/xmldsig#"));
  }
  
  public String e()
  {
    return "X509IssuerSerial";
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    boolean bool2 = paramObject instanceof XMLX509IssuerSerial;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      paramObject = (XMLX509IssuerSerial)paramObject;
      Object localObject1 = a();
      Object localObject2 = ((XMLX509IssuerSerial)paramObject).a();
      bool2 = ((BigInteger)localObject1).equals(localObject2);
      if (bool2)
      {
        localObject1 = b();
        localObject2 = ((XMLX509IssuerSerial)paramObject).b();
        bool2 = ((String)localObject1).equals(localObject2);
        if (bool2) {
          bool1 = true;
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = (a().hashCode() + 527) * 31;
    int j = b().hashCode();
    return i + j;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/content/x509/XMLX509IssuerSerial.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */