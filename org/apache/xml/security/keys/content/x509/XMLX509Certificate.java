package org.apache.xml.security.keys.content.x509;

import java.util.Arrays;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.utils.SignatureElementProxy;

public class XMLX509Certificate
  extends SignatureElementProxy
  implements XMLX509DataContent
{
  public byte[] a()
  {
    return n();
  }
  
  public String e()
  {
    return "X509Certificate";
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    boolean bool2 = paramObject instanceof XMLX509Certificate;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      paramObject = (XMLX509Certificate)paramObject;
      try
      {
        byte[] arrayOfByte1 = ((XMLX509Certificate)paramObject).a();
        byte[] arrayOfByte2 = a();
        bool1 = Arrays.equals(arrayOfByte1, arrayOfByte2);
      }
      catch (XMLSecurityException localXMLSecurityException) {}
    }
  }
  
  public int hashCode()
  {
    i = 17;
    try
    {
      byte[] arrayOfByte = a();
      int j = 0;
      for (;;)
      {
        int k = arrayOfByte.length;
        if (j >= k) {
          break;
        }
        k = i * 31;
        i = arrayOfByte[j];
        k += i;
        j += 1;
        i = k;
      }
      return i;
    }
    catch (XMLSecurityException localXMLSecurityException) {}
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/content/x509/XMLX509Certificate.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */