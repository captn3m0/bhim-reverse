package org.apache.xml.security.keys.content.x509;

import java.util.Arrays;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.utils.SignatureElementProxy;

public class XMLX509SKI
  extends SignatureElementProxy
  implements XMLX509DataContent
{
  static Log a;
  static Class b;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.keys.content.x509.XMLX509SKI");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = b;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public byte[] a()
  {
    return n();
  }
  
  public String e()
  {
    return "X509SKI";
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    boolean bool2 = paramObject instanceof XMLX509SKI;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      paramObject = (XMLX509SKI)paramObject;
      try
      {
        byte[] arrayOfByte1 = ((XMLX509SKI)paramObject).a();
        byte[] arrayOfByte2 = a();
        bool1 = Arrays.equals(arrayOfByte1, arrayOfByte2);
      }
      catch (XMLSecurityException localXMLSecurityException) {}
    }
  }
  
  public int hashCode()
  {
    i = 17;
    try
    {
      byte[] arrayOfByte = a();
      int j = 0;
      for (;;)
      {
        int k = arrayOfByte.length;
        if (j >= k) {
          break;
        }
        k = i * 31;
        i = arrayOfByte[j];
        k += i;
        j += 1;
        i = k;
      }
      return i;
    }
    catch (XMLSecurityException localXMLSecurityException) {}
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/content/x509/XMLX509SKI.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */