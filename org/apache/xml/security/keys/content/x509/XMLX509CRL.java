package org.apache.xml.security.keys.content.x509;

import org.apache.xml.security.utils.SignatureElementProxy;

public class XMLX509CRL
  extends SignatureElementProxy
  implements XMLX509DataContent
{
  public String e()
  {
    return "X509CRL";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/content/x509/XMLX509CRL.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */