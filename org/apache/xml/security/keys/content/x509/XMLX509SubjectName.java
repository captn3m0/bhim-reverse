package org.apache.xml.security.keys.content.x509;

import org.apache.xml.security.utils.RFC2253Parser;
import org.apache.xml.security.utils.SignatureElementProxy;

public class XMLX509SubjectName
  extends SignatureElementProxy
  implements XMLX509DataContent
{
  public String a()
  {
    return RFC2253Parser.a(o());
  }
  
  public String e()
  {
    return "X509SubjectName";
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool = paramObject instanceof XMLX509SubjectName;
    String str1;
    if (!bool)
    {
      bool = false;
      str1 = null;
    }
    for (;;)
    {
      return bool;
      paramObject = (XMLX509SubjectName)paramObject;
      str1 = ((XMLX509SubjectName)paramObject).a();
      String str2 = a();
      bool = str2.equals(str1);
    }
  }
  
  public int hashCode()
  {
    return a().hashCode() + 527;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/content/x509/XMLX509SubjectName.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */