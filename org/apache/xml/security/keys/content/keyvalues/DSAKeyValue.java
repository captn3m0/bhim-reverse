package org.apache.xml.security.keys.content.keyvalues;

import org.apache.xml.security.utils.SignatureElementProxy;

public class DSAKeyValue
  extends SignatureElementProxy
  implements KeyValueContent
{
  public String e()
  {
    return "DSAKeyValue";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/content/keyvalues/DSAKeyValue.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */