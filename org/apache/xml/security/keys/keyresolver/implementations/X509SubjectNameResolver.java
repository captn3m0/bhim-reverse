package org.apache.xml.security.keys.keyresolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.keyresolver.KeyResolverSpi;

public class X509SubjectNameResolver
  extends KeyResolverSpi
{
  static Log c;
  static Class d;
  
  static
  {
    Class localClass = d;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.keys.keyresolver.implementations.X509SubjectNameResolver");
      d = localClass;
    }
    for (;;)
    {
      c = LogFactory.getLog(localClass.getName());
      return;
      localClass = d;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/keyresolver/implementations/X509SubjectNameResolver.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */