package org.apache.xml.security.keys.keyresolver;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.storage.StorageResolver;

public class KeyResolver
{
  static Log a;
  static boolean b;
  static List c;
  static Class f;
  protected KeyResolverSpi d = null;
  protected StorageResolver e = null;
  
  static
  {
    Class localClass = f;
    if (localClass == null)
    {
      localClass = b("org.apache.xml.security.keys.keyresolver.KeyResolver");
      f = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      b = false;
      c = null;
      return;
      localClass = f;
    }
  }
  
  private KeyResolver(String paramString)
  {
    KeyResolverSpi localKeyResolverSpi = (KeyResolverSpi)Class.forName(paramString).newInstance();
    d = localKeyResolverSpi;
    d.a(true);
  }
  
  public static void a()
  {
    boolean bool = b;
    if (!bool)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      int i = 10;
      localArrayList.<init>(i);
      c = localArrayList;
      bool = true;
      b = bool;
    }
  }
  
  public static void a(String paramString)
  {
    List localList = c;
    KeyResolver localKeyResolver = new org/apache/xml/security/keys/keyresolver/KeyResolver;
    localKeyResolver.<init>(paramString);
    localList.add(localKeyResolver);
  }
  
  static Class b(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/keyresolver/KeyResolver.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */