package org.apache.xml.security.keys.keyresolver;

import java.util.Iterator;

class KeyResolver$ResolverIterator
  implements Iterator
{
  Iterator a;
  int b;
  
  public boolean hasNext()
  {
    return a.hasNext();
  }
  
  public Object next()
  {
    int i = b + 1;
    b = i;
    Object localObject = (KeyResolver)a.next();
    if (localObject == null)
    {
      localObject = new java/lang/RuntimeException;
      ((RuntimeException)localObject).<init>("utils.resolver.noClass");
      throw ((Throwable)localObject);
    }
    return d;
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Can't remove resolvers using the iterator");
    throw localUnsupportedOperationException;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/keyresolver/KeyResolver$ResolverIterator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */