package org.apache.xml.security.keys.storage.implementations;

import java.util.Iterator;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.keys.storage.StorageResolverSpi;

public class CertsInFilesystemDirectoryResolver
  extends StorageResolverSpi
{
  static Log a;
  static Class b;
  private List c;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.keys.storage.implementations.CertsInFilesystemDirectoryResolver");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = b;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public Iterator a()
  {
    CertsInFilesystemDirectoryResolver.FilesystemIterator localFilesystemIterator = new org/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver$FilesystemIterator;
    List localList = c;
    localFilesystemIterator.<init>(localList);
    return localFilesystemIterator;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */