package org.apache.xml.security.keys.storage.implementations;

import java.security.cert.X509Certificate;
import java.util.Iterator;
import java.util.NoSuchElementException;

class SingleCertificateResolver$InternalIterator
  implements Iterator
{
  boolean a = false;
  X509Certificate b = null;
  
  public SingleCertificateResolver$InternalIterator(X509Certificate paramX509Certificate)
  {
    b = paramX509Certificate;
  }
  
  public boolean hasNext()
  {
    boolean bool = a;
    if (!bool) {}
    for (bool = true;; bool = false) {
      return bool;
    }
  }
  
  public Object next()
  {
    boolean bool = a;
    if (bool)
    {
      NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
      localNoSuchElementException.<init>();
      throw localNoSuchElementException;
    }
    a = true;
    return b;
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Can't remove keys from KeyStore");
    throw localUnsupportedOperationException;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/storage/implementations/SingleCertificateResolver$InternalIterator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */