package org.apache.xml.security.keys.storage.implementations;

import java.util.Iterator;
import java.util.List;

class CertsInFilesystemDirectoryResolver$FilesystemIterator
  implements Iterator
{
  List a = null;
  int b;
  
  public CertsInFilesystemDirectoryResolver$FilesystemIterator(List paramList)
  {
    a = paramList;
    b = 0;
  }
  
  public boolean hasNext()
  {
    int i = b;
    List localList = a;
    int k = localList.size();
    if (i < k) {}
    int j;
    for (i = 1;; j = 0) {
      return i;
    }
  }
  
  public Object next()
  {
    List localList = a;
    int i = b;
    int j = i + 1;
    b = j;
    return localList.get(i);
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Can't remove keys from KeyStore");
    throw localUnsupportedOperationException;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/storage/implementations/CertsInFilesystemDirectoryResolver$FilesystemIterator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */