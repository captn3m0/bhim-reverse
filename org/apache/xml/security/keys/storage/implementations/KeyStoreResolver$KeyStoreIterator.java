package org.apache.xml.security.keys.storage.implementations;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.cert.Certificate;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.NoSuchElementException;

class KeyStoreResolver$KeyStoreIterator
  implements Iterator
{
  KeyStore a = null;
  Enumeration b = null;
  Certificate c = null;
  
  public KeyStoreResolver$KeyStoreIterator(KeyStore paramKeyStore)
  {
    try
    {
      a = paramKeyStore;
      localObject = a;
      localObject = ((KeyStore)localObject).aliases();
      b = ((Enumeration)localObject);
      return;
    }
    catch (KeyStoreException localKeyStoreException)
    {
      for (;;)
      {
        KeyStoreResolver.1 local1 = new org/apache/xml/security/keys/storage/implementations/KeyStoreResolver$1;
        local1.<init>(this);
        b = local1;
      }
    }
  }
  
  private Certificate a()
  {
    Object localObject1 = b;
    boolean bool = ((Enumeration)localObject1).hasMoreElements();
    if (bool) {}
    Object localObject2;
    for (localObject1 = (String)b.nextElement();; localObject2 = null)
    {
      try
      {
        KeyStore localKeyStore = a;
        localObject1 = localKeyStore.getCertificate((String)localObject1);
        if (localObject1 == null) {
          break;
        }
      }
      catch (KeyStoreException localKeyStoreException)
      {
        for (;;)
        {
          bool = false;
          localObject2 = null;
        }
      }
      return (Certificate)localObject1;
      bool = false;
    }
  }
  
  public boolean hasNext()
  {
    Certificate localCertificate = c;
    if (localCertificate == null)
    {
      localCertificate = a();
      c = localCertificate;
    }
    localCertificate = c;
    boolean bool;
    if (localCertificate != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localCertificate = null;
    }
  }
  
  public Object next()
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = a();
      c = ((Certificate)localObject);
      localObject = c;
      if (localObject == null)
      {
        localObject = new java/util/NoSuchElementException;
        ((NoSuchElementException)localObject).<init>();
        throw ((Throwable)localObject);
      }
    }
    localObject = c;
    c = null;
    return localObject;
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Can't remove keys from KeyStore");
    throw localUnsupportedOperationException;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/storage/implementations/KeyStoreResolver$KeyStoreIterator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */