package org.apache.xml.security.keys.storage;

import java.util.Iterator;
import java.util.NoSuchElementException;

class StorageResolver$StorageResolverIterator
  implements Iterator
{
  Iterator a;
  Iterator b;
  
  private Iterator a()
  {
    Iterator localIterator;
    boolean bool1;
    boolean bool2;
    do
    {
      localIterator = a;
      bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localIterator = ((StorageResolverSpi)a.next()).a();
      bool2 = localIterator.hasNext();
    } while (!bool2);
    for (;;)
    {
      return localIterator;
      bool1 = false;
      localIterator = null;
    }
  }
  
  public boolean hasNext()
  {
    boolean bool1 = true;
    Iterator localIterator = b;
    if (localIterator == null) {
      bool1 = false;
    }
    for (;;)
    {
      return bool1;
      localIterator = b;
      boolean bool2 = localIterator.hasNext();
      if (!bool2)
      {
        localIterator = a();
        b = localIterator;
        localIterator = b;
        if (localIterator == null) {
          bool1 = false;
        }
      }
    }
  }
  
  public Object next()
  {
    boolean bool = hasNext();
    if (bool) {
      return b.next();
    }
    NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
    localNoSuchElementException.<init>();
    throw localNoSuchElementException;
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Can't remove keys from KeyStore");
    throw localUnsupportedOperationException;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/storage/StorageResolver$StorageResolverIterator.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */