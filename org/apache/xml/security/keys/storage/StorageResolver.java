package org.apache.xml.security.keys.storage;

import java.util.Iterator;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class StorageResolver
{
  static Log a;
  static Class d;
  List b = null;
  Iterator c = null;
  
  static
  {
    Class localClass = d;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.keys.storage.StorageResolver");
      d = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = d;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/storage/StorageResolver.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */