package org.apache.xml.security.keys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.w3c.dom.Element;

public class KeyInfo
  extends SignatureElementProxy
{
  static Log a;
  static final List d;
  static boolean g;
  static Class h;
  List b = null;
  List c = null;
  List e = null;
  List f;
  
  static
  {
    Object localObject = h;
    if (localObject == null)
    {
      localObject = a("org.apache.xml.security.keys.KeyInfo");
      h = (Class)localObject;
    }
    for (;;)
    {
      a = LogFactory.getLog(((Class)localObject).getName());
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      ((List)localObject).add(null);
      d = Collections.unmodifiableList((List)localObject);
      g = false;
      return;
      localObject = h;
    }
  }
  
  public KeyInfo(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
    List localList = d;
    f = localList;
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public static void a()
  {
    boolean bool = g;
    Object localObject;
    if (!bool)
    {
      localObject = a;
      if (localObject == null)
      {
        localObject = h;
        if (localObject != null) {
          break label65;
        }
        localObject = a("org.apache.xml.security.keys.KeyInfo");
        h = (Class)localObject;
      }
    }
    for (;;)
    {
      a = LogFactory.getLog(((Class)localObject).getName());
      localObject = a;
      String str = "Had to assign log in the init() function";
      ((Log)localObject).error(str);
      bool = true;
      g = bool;
      return;
      label65:
      localObject = h;
    }
  }
  
  public String e()
  {
    return "KeyInfo";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/keys/KeyInfo.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */