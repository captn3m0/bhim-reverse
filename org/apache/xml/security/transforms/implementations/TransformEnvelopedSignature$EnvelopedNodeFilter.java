package org.apache.xml.security.transforms.implementations;

import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Node;

class TransformEnvelopedSignature$EnvelopedNodeFilter
  implements NodeFilter
{
  Node a;
  
  TransformEnvelopedSignature$EnvelopedNodeFilter(Node paramNode)
  {
    a = paramNode;
  }
  
  public int a(Node paramNode)
  {
    Node localNode = a;
    if (paramNode != localNode)
    {
      localNode = a;
      boolean bool = XMLUtils.a(localNode, paramNode);
      if (!bool) {
        break label29;
      }
    }
    label29:
    for (int i = -1;; i = 1) {
      return i;
    }
  }
  
  public int a(Node paramNode, int paramInt)
  {
    Node localNode = a;
    if (paramNode == localNode) {}
    for (int i = -1;; i = 1) {
      return i;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformEnvelopedSignature$EnvelopedNodeFilter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */