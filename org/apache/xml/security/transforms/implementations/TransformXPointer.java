package org.apache.xml.security.transforms.implementations;

import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;

public class TransformXPointer
  extends TransformSpi
{
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = "http://www.w3.org/TR/2001/WD-xptr-20010108";
    TransformationException localTransformationException = new org/apache/xml/security/transforms/TransformationException;
    localTransformationException.<init>("signature.Transform.NotYetImplemented", arrayOfObject);
    throw localTransformationException;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformXPointer.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */