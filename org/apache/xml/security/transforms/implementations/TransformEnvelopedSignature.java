package org.apache.xml.security.transforms.implementations;

import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TransformEnvelopedSignature
  extends TransformSpi
{
  private static Node a(Node paramNode)
  {
    Node localNode = paramNode;
    int i;
    Object localObject;
    if (localNode != null)
    {
      i = localNode.getNodeType();
      int j = 9;
      if (i == j)
      {
        i = 0;
        localObject = null;
      }
    }
    for (;;)
    {
      if (i == 0)
      {
        localObject = new org/apache/xml/security/transforms/TransformationException;
        ((TransformationException)localObject).<init>("transform.envelopedSignatureTransformNotInSignatureElement");
        throw ((Throwable)localObject);
        localObject = localNode;
        localObject = (Element)localNode;
        String str1 = ((Element)localObject).getNamespaceURI();
        String str2 = "http://www.w3.org/2000/09/xmldsig#";
        boolean bool2 = str1.equals(str2);
        if (bool2)
        {
          localObject = ((Element)localObject).getLocalName();
          str1 = "Signature";
          bool1 = ((String)localObject).equals(str1);
          if (bool1)
          {
            bool1 = true;
            continue;
          }
        }
        localNode = localNode.getParentNode();
        break;
      }
      return localNode;
      boolean bool1 = false;
      localObject = null;
    }
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    Node localNode = a(paramTransform.k());
    paramXMLSignatureInput.a(localNode);
    TransformEnvelopedSignature.EnvelopedNodeFilter localEnvelopedNodeFilter = new org/apache/xml/security/transforms/implementations/TransformEnvelopedSignature$EnvelopedNodeFilter;
    localEnvelopedNodeFilter.<init>(localNode);
    paramXMLSignatureInput.a(localEnvelopedNodeFilter);
    return paramXMLSignatureInput;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformEnvelopedSignature.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */