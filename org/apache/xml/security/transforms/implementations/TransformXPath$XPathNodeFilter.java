package org.apache.xml.security.transforms.implementations;

import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.utils.CachedXPathAPIHolder;
import org.apache.xml.security.utils.CachedXPathFuncHereAPI;
import org.apache.xml.utils.PrefixResolverDefault;
import org.apache.xpath.CachedXPathAPI;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

class TransformXPath$XPathNodeFilter
  implements NodeFilter
{
  PrefixResolverDefault a;
  CachedXPathFuncHereAPI b;
  Node c;
  String d;
  
  TransformXPath$XPathNodeFilter(Element paramElement, Node paramNode, String paramString)
  {
    Object localObject = new org/apache/xml/security/utils/CachedXPathFuncHereAPI;
    CachedXPathAPI localCachedXPathAPI = CachedXPathAPIHolder.a();
    ((CachedXPathFuncHereAPI)localObject).<init>(localCachedXPathAPI);
    b = ((CachedXPathFuncHereAPI)localObject);
    c = paramNode;
    d = paramString;
    localObject = new org/apache/xml/utils/PrefixResolverDefault;
    ((PrefixResolverDefault)localObject).<init>(paramElement);
    a = ((PrefixResolverDefault)localObject);
  }
  
  /* Error */
  public int a(Node paramNode)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: aload_0
    //   5: getfield 30	org/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter:b	Lorg/apache/xml/security/utils/CachedXPathFuncHereAPI;
    //   8: astore 4
    //   10: aload_0
    //   11: getfield 32	org/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter:c	Lorg/w3c/dom/Node;
    //   14: astore 5
    //   16: aload_0
    //   17: getfield 34	org/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter:d	Ljava/lang/String;
    //   20: astore 6
    //   22: aload_0
    //   23: getfield 41	org/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter:a	Lorg/apache/xml/utils/PrefixResolverDefault;
    //   26: astore 7
    //   28: aload 4
    //   30: aload_1
    //   31: aload 5
    //   33: aload 6
    //   35: aload 7
    //   37: invokevirtual 45	org/apache/xml/security/utils/CachedXPathFuncHereAPI:a	(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;Ljava/lang/String;Lorg/apache/xml/utils/PrefixResolver;)Lorg/apache/xpath/objects/XObject;
    //   40: astore 4
    //   42: aload 4
    //   44: invokevirtual 51	org/apache/xpath/objects/XObject:bool	()Z
    //   47: istore 8
    //   49: iload 8
    //   51: ifeq +5 -> 56
    //   54: iload_2
    //   55: ireturn
    //   56: iconst_0
    //   57: istore_2
    //   58: aconst_null
    //   59: astore 9
    //   61: goto -7 -> 54
    //   64: astore 4
    //   66: iload_2
    //   67: anewarray 4	java/lang/Object
    //   70: astore 9
    //   72: aload 9
    //   74: iconst_0
    //   75: aload_1
    //   76: aastore
    //   77: new 53	org/apache/xml/security/exceptions/XMLSecurityRuntimeException
    //   80: astore_3
    //   81: aload_3
    //   82: ldc 55
    //   84: aload 9
    //   86: aload 4
    //   88: invokespecial 58	org/apache/xml/security/exceptions/XMLSecurityRuntimeException:<init>	(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
    //   91: aload_3
    //   92: athrow
    //   93: astore 4
    //   95: iconst_2
    //   96: anewarray 4	java/lang/Object
    //   99: astore 5
    //   101: aload 5
    //   103: iconst_0
    //   104: aload_1
    //   105: aastore
    //   106: new 61	java/lang/Short
    //   109: astore_3
    //   110: aload_1
    //   111: invokeinterface 67 1 0
    //   116: istore 10
    //   118: aload_3
    //   119: iload 10
    //   121: invokespecial 70	java/lang/Short:<init>	(S)V
    //   124: aload 5
    //   126: iload_2
    //   127: aload_3
    //   128: aastore
    //   129: new 53	org/apache/xml/security/exceptions/XMLSecurityRuntimeException
    //   132: astore 9
    //   134: aload 9
    //   136: ldc 72
    //   138: aload 5
    //   140: aload 4
    //   142: invokespecial 58	org/apache/xml/security/exceptions/XMLSecurityRuntimeException:<init>	(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;)V
    //   145: aload 9
    //   147: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	148	0	this	XPathNodeFilter
    //   0	148	1	paramNode	Node
    //   1	126	2	i	int
    //   3	125	3	localObject1	Object
    //   8	35	4	localObject2	Object
    //   64	23	4	localTransformerException	javax.xml.transform.TransformerException
    //   93	48	4	localException	Exception
    //   14	125	5	localObject3	Object
    //   20	14	6	str	String
    //   26	10	7	localPrefixResolverDefault	PrefixResolverDefault
    //   47	3	8	bool	boolean
    //   59	87	9	localObject4	Object
    //   116	4	10	s	short
    // Exception table:
    //   from	to	target	type
    //   4	8	64	javax/xml/transform/TransformerException
    //   10	14	64	javax/xml/transform/TransformerException
    //   16	20	64	javax/xml/transform/TransformerException
    //   22	26	64	javax/xml/transform/TransformerException
    //   35	40	64	javax/xml/transform/TransformerException
    //   42	47	64	javax/xml/transform/TransformerException
    //   4	8	93	java/lang/Exception
    //   10	14	93	java/lang/Exception
    //   16	20	93	java/lang/Exception
    //   22	26	93	java/lang/Exception
    //   35	40	93	java/lang/Exception
    //   42	47	93	java/lang/Exception
  }
  
  public int a(Node paramNode, int paramInt)
  {
    return a(paramNode);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */