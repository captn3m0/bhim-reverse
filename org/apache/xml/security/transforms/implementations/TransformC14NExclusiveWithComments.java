package org.apache.xml.security.transforms.implementations;

import java.io.OutputStream;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.implementations.Canonicalizer20010315ExclWithComments;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.params.InclusiveNamespaces;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TransformC14NExclusiveWithComments
  extends TransformSpi
{
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, OutputStream paramOutputStream, Transform paramTransform)
  {
    Object localObject1 = null;
    Object localObject2 = "http://www.w3.org/2001/10/xml-exc-c14n#";
    String str = "InclusiveNamespaces";
    try
    {
      int i = paramTransform.c((String)localObject2, str);
      int j = 1;
      if (i == j)
      {
        localObject1 = paramTransform.k();
        localObject1 = ((Element)localObject1).getFirstChild();
        localObject2 = "http://www.w3.org/2001/10/xml-exc-c14n#";
        str = "InclusiveNamespaces";
        localObject1 = XMLUtils.a((Node)localObject1, (String)localObject2, str, 0);
        localObject2 = new org/apache/xml/security/transforms/params/InclusiveNamespaces;
        str = paramTransform.l();
        ((InclusiveNamespaces)localObject2).<init>((Element)localObject1, str);
        localObject1 = ((InclusiveNamespaces)localObject2).a();
      }
      localObject2 = new org/apache/xml/security/c14n/implementations/Canonicalizer20010315ExclWithComments;
      ((Canonicalizer20010315ExclWithComments)localObject2).<init>();
      if (paramOutputStream != null) {
        ((Canonicalizer20010315ExclWithComments)localObject2).a(paramOutputStream);
      }
      localObject1 = ((Canonicalizer20010315ExclWithComments)localObject2).a(paramXMLSignatureInput, (String)localObject1);
      localObject2 = new org/apache/xml/security/signature/XMLSignatureInput;
      ((XMLSignatureInput)localObject2).<init>((byte[])localObject1);
      return (XMLSignatureInput)localObject2;
    }
    catch (XMLSecurityException localXMLSecurityException)
    {
      localObject2 = new org/apache/xml/security/c14n/CanonicalizationException;
      ((CanonicalizationException)localObject2).<init>("empty", localXMLSecurityException);
      throw ((Throwable)localObject2);
    }
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    return a(paramXMLSignatureInput, null, paramTransform);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformC14NExclusiveWithComments.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */