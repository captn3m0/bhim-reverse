package org.apache.xml.security.transforms.implementations;

import java.util.Iterator;
import java.util.Set;
import org.apache.xml.security.signature.NodeFilter;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Node;

class XPath2NodeFilter
  implements NodeFilter
{
  boolean a;
  boolean b;
  boolean c;
  Set d;
  Set e;
  Set f;
  int g;
  int h;
  int i;
  
  XPath2NodeFilter(Set paramSet1, Set paramSet2, Set paramSet3)
  {
    g = j;
    h = j;
    i = j;
    d = paramSet1;
    boolean bool2 = paramSet1.isEmpty();
    if (!bool2)
    {
      bool2 = bool1;
      a = bool2;
      e = paramSet2;
      bool2 = paramSet2.isEmpty();
      if (bool2) {
        break label115;
      }
      bool2 = bool1;
      label78:
      b = bool2;
      f = paramSet3;
      bool2 = paramSet3.isEmpty();
      if (bool2) {
        break label121;
      }
    }
    for (;;)
    {
      c = bool1;
      return;
      bool2 = false;
      break;
      label115:
      bool2 = false;
      break label78;
      label121:
      bool1 = false;
    }
  }
  
  static boolean a(Node paramNode, Set paramSet)
  {
    boolean bool1 = true;
    boolean bool2 = paramSet.contains(paramNode);
    if (bool2) {
      bool2 = bool1;
    }
    for (;;)
    {
      return bool2;
      Iterator localIterator = paramSet.iterator();
      do
      {
        bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        localNode = (Node)localIterator.next();
        bool2 = XMLUtils.a(localNode, paramNode);
      } while (!bool2);
      bool2 = bool1;
      continue;
      bool2 = false;
      Node localNode = null;
    }
  }
  
  static boolean b(Node paramNode, Set paramSet)
  {
    return paramSet.contains(paramNode);
  }
  
  public int a(Node paramNode)
  {
    int j = 0;
    int k = 1;
    float f1 = Float.MIN_VALUE;
    boolean bool1 = b;
    Set localSet;
    int m;
    if (bool1)
    {
      localSet = e;
      bool1 = a(paramNode, localSet);
      if (bool1) {
        m = -1;
      }
    }
    for (;;)
    {
      if (m == k) {
        j = k;
      }
      for (;;)
      {
        return j;
        n = c;
        if (n == 0) {
          break label131;
        }
        localSet = f;
        n = a(paramNode, localSet);
        if (n != 0) {
          break label131;
        }
        n = 0;
        localSet = null;
        break;
        boolean bool2 = a;
        if (bool2)
        {
          localSet = d;
          n = a(paramNode, localSet);
          if (n != 0) {
            j = k;
          }
        }
        else
        {
          j = n;
        }
      }
      label131:
      int n = k;
    }
  }
  
  public int a(Node paramNode, int paramInt)
  {
    int j = 0;
    int k = 1;
    float f1 = Float.MIN_VALUE;
    int m = -1;
    float f2 = 0.0F / 0.0F;
    boolean bool1 = b;
    Set localSet1;
    if (bool1)
    {
      int n = g;
      if (n != m)
      {
        n = g;
        if (paramInt > n) {}
      }
      else
      {
        localSet1 = e;
        boolean bool2 = b(paramNode, localSet1);
        if (!bool2) {
          break label198;
        }
        g = paramInt;
      }
      i1 = g;
      if (i1 == m) {}
    }
    label168:
    label198:
    label207:
    int i2;
    for (int i1 = m;; i2 = k)
    {
      if (i1 != m)
      {
        boolean bool4 = c;
        if (bool4)
        {
          int i3 = h;
          if (i3 != m)
          {
            i3 = h;
            if (paramInt > i3) {}
          }
          else
          {
            Set localSet2 = f;
            boolean bool5 = b(paramNode, localSet2);
            if (bool5) {
              break label207;
            }
            h = m;
            i1 = 0;
            localSet1 = null;
          }
        }
      }
      int i4 = i;
      if (paramInt <= i4) {
        i = m;
      }
      if (i1 == k) {
        j = k;
      }
      for (;;)
      {
        return j;
        g = m;
        break;
        h = paramInt;
        break label168;
        boolean bool6 = a;
        if (bool6)
        {
          i1 = i;
          if (i1 == m)
          {
            localSet1 = d;
            boolean bool3 = b(paramNode, localSet1);
            if (bool3) {
              i = paramInt;
            }
          }
          i2 = i;
          if (i2 != m) {
            j = k;
          }
        }
        else
        {
          j = i2;
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/XPath2NodeFilter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */