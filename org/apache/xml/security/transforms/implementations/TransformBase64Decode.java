package org.apache.xml.security.transforms.implementations;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.utils.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class TransformBase64Decode
  extends TransformSpi
{
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, OutputStream paramOutputStream, Transform paramTransform)
  {
    for (;;)
    {
      boolean bool;
      Object localObject3;
      try
      {
        bool = paramXMLSignatureInput.g();
        Object localObject1;
        if (bool)
        {
          localObject1 = paramXMLSignatureInput.m();
          localObject3 = paramXMLSignatureInput.m();
          int i = ((Node)localObject3).getNodeType();
          int j = 3;
          if (i == j) {
            localObject1 = ((Node)localObject1).getParentNode();
          }
          localObject3 = new java/lang/StringBuffer;
          ((StringBuffer)localObject3).<init>();
          localObject1 = (Element)localObject1;
          a((Element)localObject1, (StringBuffer)localObject3);
          if (paramOutputStream == null)
          {
            localObject1 = ((StringBuffer)localObject3).toString();
            localObject3 = Base64.a((String)localObject1);
            localObject1 = new org/apache/xml/security/signature/XMLSignatureInput;
            ((XMLSignatureInput)localObject1).<init>((byte[])localObject3);
            return (XMLSignatureInput)localObject1;
          }
          localObject1 = ((StringBuffer)localObject3).toString();
          Base64.a((String)localObject1, paramOutputStream);
          localObject3 = new org/apache/xml/security/signature/XMLSignatureInput;
          bool = false;
          localObject1 = null;
          localObject1 = (byte[])null;
          ((XMLSignatureInput)localObject3).<init>(null);
          ((XMLSignatureInput)localObject3).b(paramOutputStream);
          localObject1 = localObject3;
          continue;
        }
        bool = paramXMLSignatureInput.h();
        if (!bool)
        {
          bool = paramXMLSignatureInput.f();
          if (!bool) {
            break label328;
          }
        }
        if (paramOutputStream == null)
        {
          localObject1 = paramXMLSignatureInput.e();
          localObject3 = Base64.a((byte[])localObject1);
          localObject1 = new org/apache/xml/security/signature/XMLSignatureInput;
          ((XMLSignatureInput)localObject1).<init>((byte[])localObject3);
          continue;
        }
        bool = paramXMLSignatureInput.j();
      }
      catch (Base64DecodingException localBase64DecodingException)
      {
        localObject3 = new org/apache/xml/security/transforms/TransformationException;
        ((TransformationException)localObject3).<init>("Base64Decoding", localBase64DecodingException);
        throw ((Throwable)localObject3);
      }
      Object localObject2;
      if (!bool)
      {
        bool = paramXMLSignatureInput.f();
        if (!bool) {}
      }
      else
      {
        localObject2 = paramXMLSignatureInput.e();
        Base64.a((byte[])localObject2, paramOutputStream);
      }
      for (;;)
      {
        localObject3 = new org/apache/xml/security/signature/XMLSignatureInput;
        bool = false;
        localObject2 = null;
        localObject2 = (byte[])null;
        ((XMLSignatureInput)localObject3).<init>(null);
        ((XMLSignatureInput)localObject3).b(paramOutputStream);
        localObject2 = localObject3;
        break;
        localObject2 = new java/io/BufferedInputStream;
        localObject3 = paramXMLSignatureInput.d();
        ((BufferedInputStream)localObject2).<init>((InputStream)localObject3);
        Base64.a((InputStream)localObject2, paramOutputStream);
      }
      try
      {
        label328:
        localObject2 = DocumentBuilderFactory.newInstance();
        localObject2 = ((DocumentBuilderFactory)localObject2).newDocumentBuilder();
        localObject3 = paramXMLSignatureInput.c();
        localObject2 = ((DocumentBuilder)localObject2).parse((InputStream)localObject3);
        localObject2 = ((Document)localObject2).getDocumentElement();
        localObject3 = new java/lang/StringBuffer;
        ((StringBuffer)localObject3).<init>();
        a((Element)localObject2, (StringBuffer)localObject3);
        localObject2 = ((StringBuffer)localObject3).toString();
        localObject3 = Base64.a((String)localObject2);
        localObject2 = new org/apache/xml/security/signature/XMLSignatureInput;
        ((XMLSignatureInput)localObject2).<init>((byte[])localObject3);
      }
      catch (ParserConfigurationException localParserConfigurationException)
      {
        localObject3 = new org/apache/xml/security/transforms/TransformationException;
        str = "c14n.Canonicalizer.Exception";
        ((TransformationException)localObject3).<init>(str, localParserConfigurationException);
        throw ((Throwable)localObject3);
      }
      catch (SAXException localSAXException)
      {
        localObject3 = new org/apache/xml/security/transforms/TransformationException;
        String str = "SAX exception";
        ((TransformationException)localObject3).<init>(str, localSAXException);
        throw ((Throwable)localObject3);
      }
    }
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    return a(paramXMLSignatureInput, null, paramTransform);
  }
  
  void a(Element paramElement, StringBuffer paramStringBuffer)
  {
    Node localNode = paramElement.getFirstChild();
    if (localNode != null)
    {
      int i = localNode.getNodeType();
      switch (i)
      {
      }
      for (;;)
      {
        localNode = localNode.getNextSibling();
        break;
        Object localObject = localNode;
        localObject = (Element)localNode;
        a((Element)localObject, paramStringBuffer);
        continue;
        localObject = localNode;
        localObject = ((Text)localNode).getData();
        paramStringBuffer.append((String)localObject);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformBase64Decode.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */