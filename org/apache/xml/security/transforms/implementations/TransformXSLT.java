package org.apache.xml.security.transforms.implementations;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class TransformXSLT
  extends TransformSpi
{
  static Log b;
  static Class c;
  static Class d;
  private static Class e = null;
  
  static
  {
    Object localObject = "javax.xml.XMLConstants";
    try
    {
      localObject = Class.forName((String)localObject);
      e = (Class)localObject;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    localObject = c;
    if (localObject == null)
    {
      localObject = a("org.apache.xml.security.transforms.implementations.TransformXSLT");
      c = (Class)localObject;
    }
    for (;;)
    {
      b = LogFactory.getLog(((Class)localObject).getName());
      return;
      localObject = c;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, OutputStream paramOutputStream, Transform paramTransform)
  {
    int i = 1;
    Object localObject1 = e;
    Object localObject5;
    if (localObject1 == null)
    {
      localObject1 = new Object[i];
      localObject1[0] = "SECURE_PROCESSING_FEATURE not supported";
      localObject5 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject5).<init>("generic.EmptyMessage", (Object[])localObject1);
      throw ((Throwable)localObject5);
    }
    try
    {
      localObject1 = paramTransform.k();
      localObject1 = ((Element)localObject1).getFirstChild();
      localObject5 = "http://www.w3.org/1999/XSL/Transform";
      localObject6 = "stylesheet";
      j = 0;
      localObject7 = null;
      localObject5 = XMLUtils.a((Node)localObject1, (String)localObject5, (String)localObject6, 0);
      if (localObject5 == null)
      {
        k = 2;
        localObject1 = new Object[k];
        int m = 0;
        localObject5 = null;
        localObject6 = "xslt:stylesheet";
        localObject1[0] = localObject6;
        m = 1;
        localObject6 = "Transform";
        localObject1[m] = localObject6;
        localObject5 = new org/apache/xml/security/transforms/TransformationException;
        localObject6 = "xml.WrongContent";
        ((TransformationException)localObject5).<init>((String)localObject6, (Object[])localObject1);
        throw ((Throwable)localObject5);
      }
    }
    catch (XMLSecurityException localXMLSecurityException)
    {
      int j;
      localObject5 = new Object[i];
      localObject6 = localXMLSecurityException.getMessage();
      localObject5[0] = localObject6;
      localObject6 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject6).<init>("generic.EmptyMessage", (Object[])localObject5, localXMLSecurityException);
      throw ((Throwable)localObject6);
      localObject6 = TransformerFactory.newInstance();
      localObject7 = localObject6.getClass();
      Object localObject8 = "setFeature";
      k = 2;
      Object localObject9 = new Class[k];
      Class localClass = null;
      Object localObject2 = d;
      if (localObject2 == null)
      {
        localObject2 = "java.lang.String";
        localObject2 = a((String)localObject2);
        d = (Class)localObject2;
      }
      for (;;)
      {
        localObject9[0] = localObject2;
        k = 1;
        localClass = Boolean.TYPE;
        localObject9[k] = localClass;
        localObject2 = ((Class)localObject7).getMethod((String)localObject8, (Class[])localObject9);
        j = 2;
        localObject7 = new Object[j];
        int n = 0;
        localObject8 = null;
        localObject9 = "http://javax.xml.XMLConstants/feature/secure-processing";
        localObject7[0] = localObject9;
        n = 1;
        localObject9 = Boolean.TRUE;
        localObject7[n] = localObject9;
        ((Method)localObject2).invoke(localObject6, (Object[])localObject7);
        localObject7 = new javax/xml/transform/stream/StreamSource;
        localObject2 = new java/io/ByteArrayInputStream;
        localObject8 = paramXMLSignatureInput.e();
        ((ByteArrayInputStream)localObject2).<init>((byte[])localObject8);
        ((StreamSource)localObject7).<init>((InputStream)localObject2);
        localObject2 = new java/io/ByteArrayOutputStream;
        ((ByteArrayOutputStream)localObject2).<init>();
        localObject8 = ((TransformerFactory)localObject6).newTransformer();
        localObject9 = new javax/xml/transform/dom/DOMSource;
        ((DOMSource)localObject9).<init>((Node)localObject5);
        localObject5 = new javax/xml/transform/stream/StreamResult;
        ((StreamResult)localObject5).<init>((OutputStream)localObject2);
        ((Transformer)localObject8).transform((Source)localObject9, (Result)localObject5);
        localObject5 = new javax/xml/transform/stream/StreamSource;
        localObject8 = new java/io/ByteArrayInputStream;
        localObject2 = ((ByteArrayOutputStream)localObject2).toByteArray();
        ((ByteArrayInputStream)localObject8).<init>((byte[])localObject2);
        ((StreamSource)localObject5).<init>((InputStream)localObject8);
        localObject5 = ((TransformerFactory)localObject6).newTransformer((Source)localObject5);
        localObject2 = "{http://xml.apache.org/xalan}line-separator";
        localObject6 = "\n";
        try
        {
          ((Transformer)localObject5).setOutputProperty((String)localObject2, (String)localObject6);
          if (paramOutputStream == null)
          {
            localObject6 = new java/io/ByteArrayOutputStream;
            ((ByteArrayOutputStream)localObject6).<init>();
            localObject2 = new javax/xml/transform/stream/StreamResult;
            ((StreamResult)localObject2).<init>((OutputStream)localObject6);
            ((Transformer)localObject5).transform((Source)localObject7, (Result)localObject2);
            localObject2 = new org/apache/xml/security/signature/XMLSignatureInput;
            localObject5 = ((ByteArrayOutputStream)localObject6).toByteArray();
            ((XMLSignatureInput)localObject2).<init>((byte[])localObject5);
            return (XMLSignatureInput)localObject2;
            localObject2 = d;
          }
        }
        catch (Exception localException)
        {
          for (;;)
          {
            localObject6 = b;
            localObject8 = new java/lang/StringBuffer;
            ((StringBuffer)localObject8).<init>();
            localObject9 = "Unable to set Xalan line-separator property: ";
            localObject8 = ((StringBuffer)localObject8).append((String)localObject9);
            Object localObject3 = localException.getMessage();
            localObject3 = ((StringBuffer)localObject8).append((String)localObject3);
            localObject3 = ((StringBuffer)localObject3).toString();
            ((Log)localObject6).warn(localObject3);
          }
        }
      }
    }
    catch (TransformerConfigurationException localTransformerConfigurationException)
    {
      for (;;)
      {
        Object localObject7;
        localObject5 = new Object[i];
        localObject6 = localTransformerConfigurationException.getMessage();
        localObject5[0] = localObject6;
        localObject6 = new org/apache/xml/security/transforms/TransformationException;
        ((TransformationException)localObject6).<init>("generic.EmptyMessage", (Object[])localObject5, localTransformerConfigurationException);
        throw ((Throwable)localObject6);
        Object localObject4 = new javax/xml/transform/stream/StreamResult;
        ((StreamResult)localObject4).<init>(paramOutputStream);
        ((Transformer)localObject5).transform((Source)localObject7, (Result)localObject4);
        localObject5 = new org/apache/xml/security/signature/XMLSignatureInput;
        int k = 0;
        localObject4 = null;
        localObject4 = (byte[])null;
        ((XMLSignatureInput)localObject5).<init>(null);
        ((XMLSignatureInput)localObject5).b(paramOutputStream);
        localObject4 = localObject5;
      }
    }
    catch (TransformerException localTransformerException)
    {
      localObject5 = new Object[i];
      localObject6 = localTransformerException.getMessage();
      localObject5[0] = localObject6;
      localObject6 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject6).<init>("generic.EmptyMessage", (Object[])localObject5, localTransformerException);
      throw ((Throwable)localObject6);
    }
    catch (NoSuchMethodException localNoSuchMethodException)
    {
      localObject5 = new Object[i];
      localObject6 = localNoSuchMethodException.getMessage();
      localObject5[0] = localObject6;
      localObject6 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject6).<init>("generic.EmptyMessage", (Object[])localObject5, localNoSuchMethodException);
      throw ((Throwable)localObject6);
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      localObject5 = new Object[i];
      localObject6 = localIllegalAccessException.getMessage();
      localObject5[0] = localObject6;
      localObject6 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject6).<init>("generic.EmptyMessage", (Object[])localObject5, localIllegalAccessException);
      throw ((Throwable)localObject6);
    }
    catch (InvocationTargetException localInvocationTargetException)
    {
      localObject5 = new Object[i];
      Object localObject6 = localInvocationTargetException.getMessage();
      localObject5[0] = localObject6;
      localObject6 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject6).<init>("generic.EmptyMessage", (Object[])localObject5, localInvocationTargetException);
      throw ((Throwable)localObject6);
    }
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    return a(paramXMLSignatureInput, null, paramTransform);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformXSLT.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */