package org.apache.xml.security.transforms.implementations;

import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.utils.CachedXPathAPIHolder;
import org.apache.xml.security.utils.CachedXPathFuncHereAPI;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class TransformXPath
  extends TransformSpi
{
  private boolean a(String paramString)
  {
    int i = -1;
    String str = "namespace";
    int j = paramString.indexOf(str);
    if (j == i)
    {
      str = "name()";
      j = paramString.indexOf(str);
      if (j == i) {}
    }
    else
    {
      j = 1;
    }
    for (;;)
    {
      return j;
      int k = 0;
      str = null;
    }
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    short s;
    try
    {
      Object localObject1 = paramTransform.k();
      localObject1 = ((Element)localObject1).getOwnerDocument();
      CachedXPathAPIHolder.a((Document)localObject1);
      localObject1 = paramTransform.k();
      localObject1 = ((Element)localObject1).getFirstChild();
      localObject2 = "XPath";
      str = null;
      localObject1 = XMLUtils.a((Node)localObject1, (String)localObject2, 0);
      if (localObject1 == null)
      {
        i = 2;
        localObject1 = new Object[i];
        s = 0;
        localObject2 = null;
        str = "ds:XPath";
        localObject1[0] = str;
        s = 1;
        str = "Transform";
        localObject1[s] = str;
        localObject2 = new org/apache/xml/security/transforms/TransformationException;
        str = "xml.WrongContent";
        ((TransformationException)localObject2).<init>(str, (Object[])localObject1);
        throw ((Throwable)localObject2);
      }
    }
    catch (DOMException localDOMException1)
    {
      localObject2 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject2).<init>("empty", localDOMException1);
      throw ((Throwable)localObject2);
    }
    Object localObject2 = localDOMException1.getChildNodes();
    String str = null;
    localObject2 = ((NodeList)localObject2).item(0);
    str = CachedXPathFuncHereAPI.a((Node)localObject2);
    boolean bool = a(str);
    paramXMLSignatureInput.a(bool);
    DOMException localDOMException2;
    if (localObject2 == null)
    {
      localDOMException2 = new org/w3c/dom/DOMException;
      s = 3;
      str = "Text must be in ds:Xpath";
      localDOMException2.<init>(s, str);
      throw localDOMException2;
    }
    TransformXPath.XPathNodeFilter localXPathNodeFilter = new org/apache/xml/security/transforms/implementations/TransformXPath$XPathNodeFilter;
    localXPathNodeFilter.<init>(localDOMException2, (Node)localObject2, str);
    paramXMLSignatureInput.a(localXPathNodeFilter);
    int i = 1;
    paramXMLSignatureInput.d(i);
    return paramXMLSignatureInput;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformXPath.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */