package org.apache.xml.security.transforms.implementations;

import java.io.OutputStream;
import org.apache.xml.security.c14n.implementations.Canonicalizer11_OmitComments;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;

public class TransformC14N11
  extends TransformSpi
{
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, OutputStream paramOutputStream, Transform paramTransform)
  {
    Object localObject = new org/apache/xml/security/c14n/implementations/Canonicalizer11_OmitComments;
    ((Canonicalizer11_OmitComments)localObject).<init>();
    if (paramOutputStream != null) {
      ((Canonicalizer11_OmitComments)localObject).a(paramOutputStream);
    }
    localObject = ((Canonicalizer11_OmitComments)localObject).b(paramXMLSignatureInput);
    XMLSignatureInput localXMLSignatureInput = new org/apache/xml/security/signature/XMLSignatureInput;
    localXMLSignatureInput.<init>((byte[])localObject);
    if (paramOutputStream != null) {
      localXMLSignatureInput.b(paramOutputStream);
    }
    return localXMLSignatureInput;
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    return a(paramXMLSignatureInput, null, paramTransform);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformC14N11.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */