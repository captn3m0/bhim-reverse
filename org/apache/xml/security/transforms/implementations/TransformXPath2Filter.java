package org.apache.xml.security.transforms.implementations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.transforms.Transform;
import org.apache.xml.security.transforms.TransformSpi;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.transforms.params.XPath2FilterContainer;
import org.apache.xml.security.utils.CachedXPathAPIHolder;
import org.apache.xml.security.utils.CachedXPathFuncHereAPI;
import org.apache.xml.security.utils.XMLUtils;
import org.apache.xpath.CachedXPathAPI;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TransformXPath2Filter
  extends TransformSpi
{
  static Set a(List paramList)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    int j;
    for (int i = 0;; i = j)
    {
      j = paramList.size();
      if (i >= j) {
        break;
      }
      NodeList localNodeList = (NodeList)paramList.get(i);
      int k = localNodeList.getLength();
      int m = 0;
      while (m < k)
      {
        Node localNode = localNodeList.item(m);
        localHashSet.add(localNode);
        m += 1;
      }
      j = i + 1;
    }
    return localHashSet;
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    int i = 0;
    Object localObject1 = null;
    Object localObject2 = paramTransform.k().getOwnerDocument();
    CachedXPathAPIHolder.a((Document)localObject2);
    try
    {
      localObject4 = new java/util/ArrayList;
      ((ArrayList)localObject4).<init>();
      localObject5 = new java/util/ArrayList;
      ((ArrayList)localObject5).<init>();
      localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      localCachedXPathFuncHereAPI = new org/apache/xml/security/utils/CachedXPathFuncHereAPI;
      localObject2 = CachedXPathAPIHolder.a();
      localCachedXPathFuncHereAPI.<init>((CachedXPathAPI)localObject2);
      localObject2 = paramTransform.k();
      localObject2 = ((Element)localObject2).getFirstChild();
      String str = "http://www.w3.org/2002/06/xmldsig-filter2";
      localObject6 = "XPath";
      localObject2 = XMLUtils.a((Node)localObject2, str, (String)localObject6);
      j = localObject2.length;
      if (j == 0)
      {
        k = 2;
        localObject2 = new Object[k];
        i = 0;
        localObject1 = null;
        localObject4 = "http://www.w3.org/2002/06/xmldsig-filter2";
        localObject2[0] = localObject4;
        i = 1;
        localObject4 = "XPath";
        localObject2[i] = localObject4;
        localObject1 = new org/apache/xml/security/transforms/TransformationException;
        localObject4 = "xml.WrongContent";
        ((TransformationException)localObject1).<init>((String)localObject4, (Object[])localObject2);
        throw ((Throwable)localObject1);
      }
    }
    catch (TransformerException localTransformerException)
    {
      CachedXPathFuncHereAPI localCachedXPathFuncHereAPI;
      int j;
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localTransformerException);
      throw ((Throwable)localObject1);
      Object localObject3 = paramXMLSignatureInput.m();
      boolean bool1;
      if (localObject3 != null)
      {
        localObject3 = paramXMLSignatureInput.m();
        localObject3 = XMLUtils.b((Node)localObject3);
        if (i >= j) {
          break label445;
        }
        localObject6 = paramTransform.k();
        localObject6 = ((Element)localObject6).getFirstChild();
        localObject7 = "http://www.w3.org/2002/06/xmldsig-filter2";
        Object localObject8 = "XPath";
        localObject6 = XMLUtils.a((Node)localObject6, (String)localObject7, (String)localObject8, i);
        localObject7 = paramXMLSignatureInput.k();
        localObject6 = XPath2FilterContainer.a((Element)localObject6, (String)localObject7);
        localObject7 = ((XPath2FilterContainer)localObject6).f();
        localObject8 = ((XPath2FilterContainer)localObject6).f();
        localObject8 = CachedXPathFuncHereAPI.a((Node)localObject8);
        Element localElement = ((XPath2FilterContainer)localObject6).k();
        localObject7 = localCachedXPathFuncHereAPI.a((Node)localObject3, (Node)localObject7, (String)localObject8, localElement);
        bool1 = ((XPath2FilterContainer)localObject6).a();
        if (!bool1) {
          break label357;
        }
        localArrayList.add(localObject7);
      }
      for (;;)
      {
        i += 1;
        break;
        localObject3 = paramXMLSignatureInput.b();
        localObject3 = XMLUtils.a((Set)localObject3);
        break;
        bool1 = ((XPath2FilterContainer)localObject6).b();
        if (!bool1) {
          break label401;
        }
        ((List)localObject5).add(localObject7);
      }
    }
    catch (DOMException localDOMException)
    {
      for (;;)
      {
        Object localObject6;
        Object localObject7;
        localObject1 = new org/apache/xml/security/transforms/TransformationException;
        ((TransformationException)localObject1).<init>("empty", localDOMException);
        throw ((Throwable)localObject1);
        boolean bool2 = ((XPath2FilterContainer)localObject6).c();
        if (bool2) {
          ((List)localObject4).add(localObject7);
        }
      }
    }
    catch (CanonicalizationException localCanonicalizationException)
    {
      ArrayList localArrayList;
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localCanonicalizationException);
      throw ((Throwable)localObject1);
      XPath2NodeFilter localXPath2NodeFilter = new org/apache/xml/security/transforms/implementations/XPath2NodeFilter;
      localObject1 = a((List)localObject4);
      Object localObject4 = a((List)localObject5);
      Object localObject5 = a(localArrayList);
      localXPath2NodeFilter.<init>((Set)localObject1, (Set)localObject4, (Set)localObject5);
      paramXMLSignatureInput.a(localXPath2NodeFilter);
      int k = 1;
      paramXMLSignatureInput.d(k);
      return paramXMLSignatureInput;
    }
    catch (InvalidCanonicalizerException localInvalidCanonicalizerException)
    {
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localInvalidCanonicalizerException);
      throw ((Throwable)localObject1);
    }
    catch (XMLSecurityException localXMLSecurityException)
    {
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localXMLSecurityException);
      throw ((Throwable)localObject1);
    }
    catch (SAXException localSAXException)
    {
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localSAXException);
      throw ((Throwable)localObject1);
    }
    catch (IOException localIOException)
    {
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localIOException);
      throw ((Throwable)localObject1);
    }
    catch (ParserConfigurationException localParserConfigurationException)
    {
      label357:
      label401:
      label445:
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localParserConfigurationException);
      throw ((Throwable)localObject1);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/TransformXPath2Filter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */