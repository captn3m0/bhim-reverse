package org.apache.xml.security.transforms.implementations;

import org.apache.xml.dtm.DTMManager;
import org.apache.xml.security.utils.I18n;
import org.apache.xpath.XPathContext;
import org.w3c.dom.Node;

public class FuncHereContext
  extends XPathContext
{
  private FuncHereContext() {}
  
  public FuncHereContext(Node paramNode, DTMManager paramDTMManager)
  {
    super(paramNode);
    try
    {
      m_dtmManager = paramDTMManager;
      return;
    }
    catch (IllegalAccessError localIllegalAccessError1)
    {
      IllegalAccessError localIllegalAccessError2 = new java/lang/IllegalAccessError;
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      String str2 = I18n.a("endorsed.jdk1.4.0");
      localStringBuffer = localStringBuffer.append(str2).append(" Original message was \"");
      String str1 = localIllegalAccessError1.getMessage();
      str1 = str1 + "\"";
      localIllegalAccessError2.<init>(str1);
      throw localIllegalAccessError2;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/implementations/FuncHereContext.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */