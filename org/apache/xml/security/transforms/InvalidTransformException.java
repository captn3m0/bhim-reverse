package org.apache.xml.security.transforms;

import org.apache.xml.security.exceptions.XMLSecurityException;

public class InvalidTransformException
  extends XMLSecurityException
{
  public InvalidTransformException() {}
  
  public InvalidTransformException(String paramString, Object[] paramArrayOfObject)
  {
    super(paramString, paramArrayOfObject);
  }
  
  public InvalidTransformException(String paramString, Object[] paramArrayOfObject, Exception paramException)
  {
    super(paramString, paramArrayOfObject, paramException);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/InvalidTransformException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */