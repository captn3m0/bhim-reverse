package org.apache.xml.security.transforms;

import java.io.OutputStream;
import java.util.HashMap;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public final class Transform
  extends SignatureElementProxy
{
  static Class a;
  private static Log b;
  private static boolean c;
  private static HashMap d;
  private static HashMap e;
  private TransformSpi f = null;
  
  static
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = a("org.apache.xml.security.transforms.Transform");
      a = (Class)localObject;
    }
    for (;;)
    {
      b = LogFactory.getLog(((Class)localObject).getName());
      c = false;
      d = null;
      localObject = new java/util/HashMap;
      ((HashMap)localObject).<init>();
      e = (HashMap)localObject;
      return;
      localObject = a;
    }
  }
  
  public Transform(Document paramDocument, String paramString, NodeList paramNodeList)
  {
    super(paramDocument);
    Object localObject2 = k;
    Object localObject3 = "Algorithm";
    ((Element)localObject2).setAttributeNS(null, (String)localObject3, paramString);
    localObject2 = d(paramString);
    f = ((TransformSpi)localObject2);
    localObject2 = f;
    if (localObject2 == null)
    {
      localObject2 = new Object[bool1];
      localObject2[0] = paramString;
      localInvalidTransformException = new org/apache/xml/security/transforms/InvalidTransformException;
      localInvalidTransformException.<init>("signature.Transform.UnknownTransform", (Object[])localObject2);
      throw localInvalidTransformException;
    }
    localObject2 = b;
    boolean bool2 = ((Log)localObject2).isDebugEnabled();
    if (bool2)
    {
      localObject2 = b;
      localObject3 = new java/lang/StringBuffer;
      ((StringBuffer)localObject3).<init>();
      localObject3 = ((StringBuffer)localObject3).append("Create URI \"").append(paramString).append("\" class \"");
      localObject1 = f.getClass();
      localObject3 = localObject1 + "\"";
      ((Log)localObject2).debug(localObject3);
      localObject2 = b;
      localObject3 = new java/lang/StringBuffer;
      ((StringBuffer)localObject3).<init>();
      localObject1 = "The NodeList is ";
      localObject3 = (String)localObject1 + paramNodeList;
      ((Log)localObject2).debug(localObject3);
    }
    if (paramNodeList != null) {
      for (;;)
      {
        int j = paramNodeList.getLength();
        if (i >= j) {
          break;
        }
        localObject2 = k;
        localObject3 = paramNodeList.item(i).cloneNode(bool1);
        ((Element)localObject2).appendChild((Node)localObject3);
        i += 1;
      }
    }
  }
  
  public Transform(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
    Object localObject2 = paramElement.getAttributeNS(null, "Algorithm");
    if (localObject2 != null)
    {
      i = ((String)localObject2).length();
      if (i != 0) {}
    }
    else
    {
      localObject2 = new Object[2];
      localObject2[0] = "Algorithm";
      localObject2[j] = "Transform";
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("xml.WrongContent", (Object[])localObject2);
      throw ((Throwable)localObject1);
    }
    localObject1 = d((String)localObject2);
    f = ((TransformSpi)localObject1);
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new Object[j];
      localObject1[0] = localObject2;
      localObject2 = new org/apache/xml/security/transforms/InvalidTransformException;
      ((InvalidTransformException)localObject2).<init>("signature.Transform.UnknownTransform", (Object[])localObject1);
      throw ((Throwable)localObject2);
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public static Transform a(Document paramDocument, String paramString)
  {
    ((NodeList)null);
    return a(paramDocument, paramString, null);
  }
  
  public static Transform a(Document paramDocument, String paramString, NodeList paramNodeList)
  {
    Transform localTransform = new org/apache/xml/security/transforms/Transform;
    localTransform.<init>(paramDocument, paramString, paramNodeList);
    return localTransform;
  }
  
  public static void a()
  {
    boolean bool = c;
    if (!bool)
    {
      HashMap localHashMap = new java/util/HashMap;
      int i = 10;
      localHashMap.<init>(i);
      d = localHashMap;
      bool = true;
      c = bool;
    }
  }
  
  /* Error */
  public static void a(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic 177	org/apache/xml/security/transforms/Transform:b	(Ljava/lang/String;)Ljava/lang/Class;
    //   4: astore_2
    //   5: aload_2
    //   6: ifnull +29 -> 35
    //   9: iconst_2
    //   10: anewarray 71	java/lang/Object
    //   13: astore_3
    //   14: aload_3
    //   15: iconst_0
    //   16: aload_0
    //   17: aastore
    //   18: aload_3
    //   19: iconst_1
    //   20: aload_2
    //   21: aastore
    //   22: new 179	org/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException
    //   25: astore_2
    //   26: aload_2
    //   27: ldc -75
    //   29: aload_3
    //   30: invokespecial 182	org/apache/xml/security/exceptions/AlgorithmAlreadyRegisteredException:<init>	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   33: aload_2
    //   34: athrow
    //   35: getstatic 40	org/apache/xml/security/transforms/Transform:d	Ljava/util/HashMap;
    //   38: astore_3
    //   39: getstatic 17	org/apache/xml/security/transforms/Transform:a	Ljava/lang/Class;
    //   42: astore_2
    //   43: aload_2
    //   44: ifnonnull +29 -> 73
    //   47: ldc 19
    //   49: astore_2
    //   50: aload_2
    //   51: invokestatic 22	org/apache/xml/security/transforms/Transform:a	(Ljava/lang/String;)Ljava/lang/Class;
    //   54: astore_2
    //   55: aload_2
    //   56: putstatic 17	org/apache/xml/security/transforms/Transform:a	Ljava/lang/Class;
    //   59: aload_1
    //   60: aload_2
    //   61: invokestatic 187	org/apache/xml/security/utils/ClassLoaderUtils:a	(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Class;
    //   64: astore_2
    //   65: aload_3
    //   66: aload_0
    //   67: aload_2
    //   68: invokevirtual 191	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   71: pop
    //   72: return
    //   73: getstatic 17	org/apache/xml/security/transforms/Transform:a	Ljava/lang/Class;
    //   76: astore_2
    //   77: goto -18 -> 59
    //   80: astore_2
    //   81: new 193	java/lang/RuntimeException
    //   84: astore_3
    //   85: aload_3
    //   86: aload_2
    //   87: invokespecial 196	java/lang/RuntimeException:<init>	(Ljava/lang/Throwable;)V
    //   90: aload_3
    //   91: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	92	0	paramString1	String
    //   0	92	1	paramString2	String
    //   4	73	2	localObject1	Object
    //   80	7	2	localClassNotFoundException	ClassNotFoundException
    //   13	78	3	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   35	38	80	java/lang/ClassNotFoundException
    //   39	42	80	java/lang/ClassNotFoundException
    //   50	54	80	java/lang/ClassNotFoundException
    //   55	59	80	java/lang/ClassNotFoundException
    //   60	64	80	java/lang/ClassNotFoundException
    //   67	72	80	java/lang/ClassNotFoundException
    //   73	76	80	java/lang/ClassNotFoundException
  }
  
  private static Class b(String paramString)
  {
    return (Class)d.get(paramString);
  }
  
  private static TransformSpi d(String paramString)
  {
    int i = 1;
    InvalidTransformException localInvalidTransformException = null;
    for (;;)
    {
      try
      {
        Object localObject1 = e;
        localObject1 = ((HashMap)localObject1).get(paramString);
        if (localObject1 != null)
        {
          localObject1 = (TransformSpi)localObject1;
          return (TransformSpi)localObject1;
        }
        localObject1 = d;
        localObject1 = ((HashMap)localObject1).get(paramString);
        localObject1 = (Class)localObject1;
        Object localObject3;
        if (localObject1 != null)
        {
          localObject1 = ((Class)localObject1).newInstance();
          localObject1 = (TransformSpi)localObject1;
          localObject3 = e;
          ((HashMap)localObject3).put(paramString, localObject1);
        }
        else
        {
          Object localObject2 = null;
        }
      }
      catch (InstantiationException localInstantiationException)
      {
        localObject3 = new Object[i];
        localObject3[0] = paramString;
        localInvalidTransformException = new org/apache/xml/security/transforms/InvalidTransformException;
        localInvalidTransformException.<init>("signature.Transform.UnknownTransform", (Object[])localObject3, localInstantiationException);
        throw localInvalidTransformException;
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        localObject3 = new Object[i];
        localObject3[0] = paramString;
        localInvalidTransformException = new org/apache/xml/security/transforms/InvalidTransformException;
        localInvalidTransformException.<init>("signature.Transform.UnknownTransform", (Object[])localObject3, localIllegalAccessException);
        throw localInvalidTransformException;
      }
    }
  }
  
  public XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput)
  {
    int i = 2;
    int j = 1;
    try
    {
      TransformSpi localTransformSpi = f;
      return localTransformSpi.a(paramXMLSignatureInput, this);
    }
    catch (ParserConfigurationException localParserConfigurationException)
    {
      arrayOfObject = new Object[i];
      localObject = b();
      arrayOfObject[0] = localObject;
      arrayOfObject[j] = "ParserConfigurationException";
      localObject = new org/apache/xml/security/c14n/CanonicalizationException;
      ((CanonicalizationException)localObject).<init>("signature.Transform.ErrorDuringTransform", arrayOfObject, localParserConfigurationException);
      throw ((Throwable)localObject);
    }
    catch (SAXException localSAXException)
    {
      Object[] arrayOfObject = new Object[i];
      Object localObject = b();
      arrayOfObject[0] = localObject;
      arrayOfObject[j] = "SAXException";
      localObject = new org/apache/xml/security/c14n/CanonicalizationException;
      ((CanonicalizationException)localObject).<init>("signature.Transform.ErrorDuringTransform", arrayOfObject, localSAXException);
      throw ((Throwable)localObject);
    }
  }
  
  public XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, OutputStream paramOutputStream)
  {
    int i = 2;
    int j = 1;
    try
    {
      TransformSpi localTransformSpi = f;
      return localTransformSpi.a(paramXMLSignatureInput, paramOutputStream, this);
    }
    catch (ParserConfigurationException localParserConfigurationException)
    {
      arrayOfObject = new Object[i];
      localObject = b();
      arrayOfObject[0] = localObject;
      arrayOfObject[j] = "ParserConfigurationException";
      localObject = new org/apache/xml/security/c14n/CanonicalizationException;
      ((CanonicalizationException)localObject).<init>("signature.Transform.ErrorDuringTransform", arrayOfObject, localParserConfigurationException);
      throw ((Throwable)localObject);
    }
    catch (SAXException localSAXException)
    {
      Object[] arrayOfObject = new Object[i];
      Object localObject = b();
      arrayOfObject[0] = localObject;
      arrayOfObject[j] = "SAXException";
      localObject = new org/apache/xml/security/c14n/CanonicalizationException;
      ((CanonicalizationException)localObject).<init>("signature.Transform.ErrorDuringTransform", arrayOfObject, localSAXException);
      throw ((Throwable)localObject);
    }
  }
  
  public String b()
  {
    return k.getAttributeNS(null, "Algorithm");
  }
  
  public String e()
  {
    return "Transform";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/Transform.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */