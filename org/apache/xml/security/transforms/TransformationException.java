package org.apache.xml.security.transforms;

import org.apache.xml.security.exceptions.XMLSecurityException;

public class TransformationException
  extends XMLSecurityException
{
  public TransformationException() {}
  
  public TransformationException(String paramString)
  {
    super(paramString);
  }
  
  public TransformationException(String paramString, Exception paramException)
  {
    super(paramString, paramException);
  }
  
  public TransformationException(String paramString, Object[] paramArrayOfObject)
  {
    super(paramString, paramArrayOfObject);
  }
  
  public TransformationException(String paramString, Object[] paramArrayOfObject, Exception paramException)
  {
    super(paramString, paramArrayOfObject, paramException);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/TransformationException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */