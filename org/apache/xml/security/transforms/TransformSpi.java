package org.apache.xml.security.transforms;

import java.io.OutputStream;
import org.apache.xml.security.signature.XMLSignatureInput;

public abstract class TransformSpi
{
  protected Transform a = null;
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput)
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>();
    throw localUnsupportedOperationException;
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, OutputStream paramOutputStream, Transform paramTransform)
  {
    return a(paramXMLSignatureInput, paramTransform);
  }
  
  protected XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, Transform paramTransform)
  {
    try
    {
      Object localObject = getClass();
      localObject = ((Class)localObject).newInstance();
      localObject = (TransformSpi)localObject;
      ((TransformSpi)localObject).a(paramTransform);
      return ((TransformSpi)localObject).a(paramXMLSignatureInput);
    }
    catch (InstantiationException localInstantiationException)
    {
      localTransformationException = new org/apache/xml/security/transforms/TransformationException;
      localTransformationException.<init>("", localInstantiationException);
      throw localTransformationException;
    }
    catch (IllegalAccessException localIllegalAccessException)
    {
      TransformationException localTransformationException = new org/apache/xml/security/transforms/TransformationException;
      localTransformationException.<init>("", localIllegalAccessException);
      throw localTransformationException;
    }
  }
  
  protected void a(Transform paramTransform)
  {
    a = paramTransform;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/TransformSpi.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */