package org.apache.xml.security.transforms;

import java.io.IOException;
import java.io.OutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Transforms
  extends SignatureElementProxy
{
  static Log a;
  static Class c;
  Element[] b;
  
  static
  {
    Class localClass = c;
    if (localClass == null)
    {
      localClass = b("org.apache.xml.security.transforms.Transforms");
      c = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = c;
    }
  }
  
  protected Transforms() {}
  
  public Transforms(Document paramDocument)
  {
    super(paramDocument);
    XMLUtils.b(k);
  }
  
  public Transforms(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
    int i = a();
    if (i == 0)
    {
      Object[] arrayOfObject = new Object[2];
      arrayOfObject[0] = "Transform";
      arrayOfObject[1] = "Transforms";
      TransformationException localTransformationException = new org/apache/xml/security/transforms/TransformationException;
      localTransformationException.<init>("xml.WrongContent", arrayOfObject);
      throw localTransformationException;
    }
  }
  
  private void a(Transform paramTransform)
  {
    Object localObject1 = a;
    boolean bool = ((Log)localObject1).isDebugEnabled();
    if (bool)
    {
      localObject1 = a;
      Object localObject2 = new java/lang/StringBuffer;
      ((StringBuffer)localObject2).<init>();
      localObject2 = ((StringBuffer)localObject2).append("Transforms.addTransform(");
      String str = paramTransform.b();
      localObject2 = ((StringBuffer)localObject2).append(str);
      str = ")";
      localObject2 = str;
      ((Log)localObject1).debug(localObject2);
    }
    localObject1 = paramTransform.k();
    k.appendChild((Node)localObject1);
    XMLUtils.b(k);
  }
  
  static Class b(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public int a()
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = k.getFirstChild();
      String str = "Transform";
      localObject = XMLUtils.a((Node)localObject, str);
      b = ((Element[])localObject);
    }
    return b.length;
  }
  
  public XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, OutputStream paramOutputStream)
  {
    try
    {
      int i = a() + -1;
      int j = 0;
      int k = 0;
      localObject1 = null;
      for (XMLSignatureInput localXMLSignatureInput = paramXMLSignatureInput; k < i; localXMLSignatureInput = paramXMLSignatureInput)
      {
        Transform localTransform = a(k);
        Log localLog = a;
        boolean bool = localLog.isDebugEnabled();
        if (bool)
        {
          localLog = a;
          Object localObject2 = new java/lang/StringBuffer;
          ((StringBuffer)localObject2).<init>();
          String str = "Perform the (";
          localObject2 = ((StringBuffer)localObject2).append(str);
          localObject2 = ((StringBuffer)localObject2).append(k);
          str = ")th ";
          localObject2 = ((StringBuffer)localObject2).append(str);
          str = localTransform.b();
          localObject2 = ((StringBuffer)localObject2).append(str);
          str = " transform";
          localObject2 = ((StringBuffer)localObject2).append(str);
          localObject2 = ((StringBuffer)localObject2).toString();
          localLog.debug(localObject2);
        }
        paramXMLSignatureInput = localTransform.a(localXMLSignatureInput);
        j = k + 1;
        k = j;
      }
      if (i >= 0)
      {
        localObject1 = a(i);
        localXMLSignatureInput = ((Transform)localObject1).a(localXMLSignatureInput, paramOutputStream);
      }
      return localXMLSignatureInput;
    }
    catch (IOException localIOException)
    {
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localIOException);
      throw ((Throwable)localObject1);
    }
    catch (CanonicalizationException localCanonicalizationException)
    {
      localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localCanonicalizationException);
      throw ((Throwable)localObject1);
    }
    catch (InvalidCanonicalizerException localInvalidCanonicalizerException)
    {
      Object localObject1 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject1).<init>("empty", localInvalidCanonicalizerException);
      throw ((Throwable)localObject1);
    }
  }
  
  public Transform a(int paramInt)
  {
    try
    {
      Object localObject1 = b;
      if (localObject1 == null)
      {
        localObject1 = k;
        localObject1 = ((Element)localObject1).getFirstChild();
        localObject2 = "Transform";
        localObject1 = XMLUtils.a((Node)localObject1, (String)localObject2);
        b = ((Element[])localObject1);
      }
      localObject1 = new org/apache/xml/security/transforms/Transform;
      localObject2 = b;
      localObject2 = localObject2[paramInt];
      String str = l;
      ((Transform)localObject1).<init>((Element)localObject2, str);
      return (Transform)localObject1;
    }
    catch (XMLSecurityException localXMLSecurityException)
    {
      Object localObject2 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject2).<init>("empty", localXMLSecurityException);
      throw ((Throwable)localObject2);
    }
  }
  
  public void a(String paramString)
  {
    try
    {
      Object localObject1 = a;
      boolean bool = ((Log)localObject1).isDebugEnabled();
      if (bool)
      {
        localObject1 = a;
        localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        String str = "Transforms.addTransform(";
        localObject2 = ((StringBuffer)localObject2).append(str);
        localObject2 = ((StringBuffer)localObject2).append(paramString);
        str = ")";
        localObject2 = ((StringBuffer)localObject2).append(str);
        localObject2 = ((StringBuffer)localObject2).toString();
        ((Log)localObject1).debug(localObject2);
      }
      localObject1 = m;
      localObject1 = Transform.a((Document)localObject1, paramString);
      a((Transform)localObject1);
      return;
    }
    catch (InvalidTransformException localInvalidTransformException)
    {
      Object localObject2 = new org/apache/xml/security/transforms/TransformationException;
      ((TransformationException)localObject2).<init>("empty", localInvalidTransformException);
      throw ((Throwable)localObject2);
    }
  }
  
  public String e()
  {
    return "Transforms";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/Transforms.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */