package org.apache.xml.security.transforms.params;

import org.apache.xml.security.transforms.TransformParam;
import org.apache.xml.security.utils.ElementProxy;

public class XPath2FilterContainer04
  extends ElementProxy
  implements TransformParam
{
  public final String d()
  {
    return "http://www.w3.org/2002/04/xmldsig-filter2";
  }
  
  public final String e()
  {
    return "XPath";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/params/XPath2FilterContainer04.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */