package org.apache.xml.security.transforms.params;

import java.util.SortedSet;
import java.util.StringTokenizer;
import java.util.TreeSet;
import org.apache.xml.security.transforms.TransformParam;
import org.apache.xml.security.utils.ElementProxy;
import org.w3c.dom.Element;

public class InclusiveNamespaces
  extends ElementProxy
  implements TransformParam
{
  public InclusiveNamespaces(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
  }
  
  public static SortedSet a(String paramString)
  {
    TreeSet localTreeSet = new java/util/TreeSet;
    localTreeSet.<init>();
    if (paramString != null)
    {
      int i = paramString.length();
      if (i != 0) {}
    }
    else
    {
      return localTreeSet;
    }
    StringTokenizer localStringTokenizer = new java/util/StringTokenizer;
    String str1 = " \t\r\n";
    localStringTokenizer.<init>(paramString, str1);
    for (;;)
    {
      boolean bool1 = localStringTokenizer.hasMoreTokens();
      if (!bool1) {
        break;
      }
      str1 = localStringTokenizer.nextToken();
      String str2 = "#default";
      boolean bool2 = str1.equals(str2);
      if (bool2)
      {
        str1 = "xmlns";
        localTreeSet.add(str1);
      }
      else
      {
        localTreeSet.add(str1);
      }
    }
  }
  
  public String a()
  {
    return k.getAttributeNS(null, "PrefixList");
  }
  
  public String d()
  {
    return "http://www.w3.org/2001/10/xml-exc-c14n#";
  }
  
  public String e()
  {
    return "InclusiveNamespaces";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/params/InclusiveNamespaces.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */