package org.apache.xml.security.transforms.params;

import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.transforms.TransformParam;
import org.apache.xml.security.utils.ElementProxy;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XPath2FilterContainer
  extends ElementProxy
  implements TransformParam
{
  private XPath2FilterContainer() {}
  
  private XPath2FilterContainer(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
    Object localObject1 = k;
    String str = "Filter";
    localObject1 = ((Element)localObject1).getAttributeNS(null, str);
    Object localObject2 = "intersect";
    boolean bool = ((String)localObject1).equals(localObject2);
    if (!bool)
    {
      localObject2 = "subtract";
      bool = ((String)localObject1).equals(localObject2);
      if (!bool)
      {
        localObject2 = "union";
        bool = ((String)localObject1).equals(localObject2);
        if (!bool)
        {
          localObject2 = new Object[3];
          localObject2[0] = "Filter";
          localObject2[1] = localObject1;
          localObject2[2] = "intersect, subtract or union";
          localObject1 = new org/apache/xml/security/exceptions/XMLSecurityException;
          ((XMLSecurityException)localObject1).<init>("attributeValueIllegal", (Object[])localObject2);
          throw ((Throwable)localObject1);
        }
      }
    }
  }
  
  public static XPath2FilterContainer a(Element paramElement, String paramString)
  {
    XPath2FilterContainer localXPath2FilterContainer = new org/apache/xml/security/transforms/params/XPath2FilterContainer;
    localXPath2FilterContainer.<init>(paramElement, paramString);
    return localXPath2FilterContainer;
  }
  
  public boolean a()
  {
    return k.getAttributeNS(null, "Filter").equals("intersect");
  }
  
  public boolean b()
  {
    return k.getAttributeNS(null, "Filter").equals("subtract");
  }
  
  public boolean c()
  {
    return k.getAttributeNS(null, "Filter").equals("union");
  }
  
  public final String d()
  {
    return "http://www.w3.org/2002/06/xmldsig-filter2";
  }
  
  public final String e()
  {
    return "XPath";
  }
  
  public Node f()
  {
    NodeList localNodeList = this.k.getChildNodes();
    int i = localNodeList.getLength();
    int j = 0;
    Node localNode1 = null;
    if (j < i)
    {
      Node localNode2 = localNodeList.item(j);
      int k = localNode2.getNodeType();
      int m = 3;
      if (k != m) {}
    }
    for (localNode1 = localNodeList.item(j);; localNode1 = null)
    {
      return localNode1;
      j += 1;
      break;
      j = 0;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/params/XPath2FilterContainer.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */