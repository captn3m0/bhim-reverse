package org.apache.xml.security.transforms.params;

import org.apache.xml.security.transforms.TransformParam;
import org.apache.xml.security.utils.SignatureElementProxy;

public class XPathContainer
  extends SignatureElementProxy
  implements TransformParam
{
  public String e()
  {
    return "XPath";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/transforms/params/XPathContainer.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */