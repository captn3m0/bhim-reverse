package org.apache.xml.security.signature;

public class ReferenceNotInitializedException
  extends XMLSignatureException
{
  public ReferenceNotInitializedException() {}
  
  public ReferenceNotInitializedException(String paramString, Exception paramException)
  {
    super(paramString, paramException);
  }
  
  public ReferenceNotInitializedException(String paramString, Object[] paramArrayOfObject)
  {
    super(paramString, paramArrayOfObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/signature/ReferenceNotInitializedException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */