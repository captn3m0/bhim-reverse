package org.apache.xml.security.signature;

import java.io.OutputStream;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.MessageDigestAlgorithm;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.InvalidCanonicalizerException;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.transforms.TransformationException;
import org.apache.xml.security.transforms.Transforms;
import org.apache.xml.security.utils.Base64;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.apache.xml.security.utils.XMLUtils;
import org.apache.xml.security.utils.resolver.ResourceResolver;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;

public class Reference
  extends SignatureElementProxy
{
  static Log a;
  static Class d;
  private static boolean e;
  Manifest b = null;
  XMLSignatureInput c;
  private Transforms f;
  private Element g;
  private Element h;
  
  static
  {
    Object localObject = new org/apache/xml/security/signature/Reference$1;
    ((Reference.1)localObject).<init>();
    boolean bool = ((Boolean)AccessController.doPrivileged((PrivilegedAction)localObject)).booleanValue();
    e = bool;
    localObject = d;
    if (localObject == null)
    {
      localObject = a("org.apache.xml.security.signature.Reference");
      d = (Class)localObject;
    }
    for (;;)
    {
      a = LogFactory.getLog(((Class)localObject).getName());
      return;
      localObject = d;
    }
  }
  
  protected Reference(Element paramElement, String paramString, Manifest paramManifest)
  {
    super(paramElement, paramString);
    l = paramString;
    Element localElement = XMLUtils.a(paramElement.getFirstChild());
    Object localObject = "Transforms";
    String str = localElement.getLocalName();
    boolean bool = ((String)localObject).equals(str);
    if (bool)
    {
      localObject = "http://www.w3.org/2000/09/xmldsig#";
      str = localElement.getNamespaceURI();
      bool = ((String)localObject).equals(str);
      if (bool)
      {
        localObject = new org/apache/xml/security/transforms/Transforms;
        str = l;
        ((Transforms)localObject).<init>(localElement, str);
        f = ((Transforms)localObject);
        localElement = XMLUtils.a(localElement.getNextSibling());
      }
    }
    g = localElement;
    localElement = XMLUtils.a(g.getNextSibling());
    h = localElement;
    b = paramManifest;
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  private XMLSignatureInput a(XMLSignatureInput paramXMLSignatureInput, OutputStream paramOutputStream)
  {
    try
    {
      Transforms localTransforms = h();
      if (localTransforms != null)
      {
        paramXMLSignatureInput = localTransforms.a(paramXMLSignatureInput, paramOutputStream);
        c = paramXMLSignatureInput;
      }
      return paramXMLSignatureInput;
    }
    catch (ResourceResolverException localResourceResolverException)
    {
      localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localResourceResolverException);
      throw localXMLSignatureException;
    }
    catch (CanonicalizationException localCanonicalizationException)
    {
      localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localCanonicalizationException);
      throw localXMLSignatureException;
    }
    catch (InvalidCanonicalizerException localInvalidCanonicalizerException)
    {
      localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localInvalidCanonicalizerException);
      throw localXMLSignatureException;
    }
    catch (TransformationException localTransformationException)
    {
      localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localTransformationException);
      throw localXMLSignatureException;
    }
    catch (XMLSecurityException localXMLSecurityException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localXMLSecurityException);
      throw localXMLSignatureException;
    }
  }
  
  /* Error */
  private byte[] a(boolean paramBoolean)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 148	org/apache/xml/security/signature/Reference:a	()Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 152	org/apache/xml/security/algorithms/MessageDigestAlgorithm:c	()V
    //   9: new 154	org/apache/xml/security/utils/DigesterOutputStream
    //   12: astore_3
    //   13: aload_3
    //   14: aload_2
    //   15: invokespecial 157	org/apache/xml/security/utils/DigesterOutputStream:<init>	(Lorg/apache/xml/security/algorithms/MessageDigestAlgorithm;)V
    //   18: new 159	org/apache/xml/security/utils/UnsyncBufferedOutputStream
    //   21: astore_2
    //   22: aload_2
    //   23: aload_3
    //   24: invokespecial 162	org/apache/xml/security/utils/UnsyncBufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   27: aload_0
    //   28: aload_2
    //   29: invokevirtual 165	org/apache/xml/security/signature/Reference:a	(Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    //   32: astore 4
    //   34: getstatic 39	org/apache/xml/security/signature/Reference:e	Z
    //   37: istore 5
    //   39: iload 5
    //   41: ifeq +140 -> 181
    //   44: iload_1
    //   45: ifne +136 -> 181
    //   48: aload 4
    //   50: invokevirtual 170	org/apache/xml/security/signature/XMLSignatureInput:i	()Z
    //   53: istore 5
    //   55: iload 5
    //   57: ifne +124 -> 181
    //   60: aload 4
    //   62: invokevirtual 172	org/apache/xml/security/signature/XMLSignatureInput:h	()Z
    //   65: istore 5
    //   67: iload 5
    //   69: ifne +112 -> 181
    //   72: aload_0
    //   73: getfield 101	org/apache/xml/security/signature/Reference:f	Lorg/apache/xml/security/transforms/Transforms;
    //   76: astore 6
    //   78: aload 6
    //   80: ifnonnull +64 -> 144
    //   83: new 98	org/apache/xml/security/transforms/Transforms
    //   86: astore 6
    //   88: aload_0
    //   89: getfield 176	org/apache/xml/security/signature/Reference:m	Lorg/w3c/dom/Document;
    //   92: astore 7
    //   94: aload 6
    //   96: aload 7
    //   98: invokespecial 179	org/apache/xml/security/transforms/Transforms:<init>	(Lorg/w3c/dom/Document;)V
    //   101: aload_0
    //   102: aload 6
    //   104: putfield 101	org/apache/xml/security/signature/Reference:f	Lorg/apache/xml/security/transforms/Transforms;
    //   107: aload_0
    //   108: getfield 182	org/apache/xml/security/signature/Reference:k	Lorg/w3c/dom/Element;
    //   111: astore 6
    //   113: aload_0
    //   114: getfield 101	org/apache/xml/security/signature/Reference:f	Lorg/apache/xml/security/transforms/Transforms;
    //   117: astore 7
    //   119: aload 7
    //   121: invokevirtual 185	org/apache/xml/security/transforms/Transforms:k	()Lorg/w3c/dom/Element;
    //   124: astore 7
    //   126: aload_0
    //   127: getfield 106	org/apache/xml/security/signature/Reference:g	Lorg/w3c/dom/Element;
    //   130: astore 8
    //   132: aload 6
    //   134: aload 7
    //   136: aload 8
    //   138: invokeinterface 189 3 0
    //   143: pop
    //   144: aload_0
    //   145: getfield 101	org/apache/xml/security/signature/Reference:f	Lorg/apache/xml/security/transforms/Transforms;
    //   148: astore 6
    //   150: ldc -65
    //   152: astore 7
    //   154: aload 6
    //   156: aload 7
    //   158: invokevirtual 194	org/apache/xml/security/transforms/Transforms:a	(Ljava/lang/String;)V
    //   161: iconst_1
    //   162: istore 5
    //   164: aload 4
    //   166: aload_2
    //   167: iload 5
    //   169: invokevirtual 198	org/apache/xml/security/signature/XMLSignatureInput:a	(Ljava/io/OutputStream;Z)V
    //   172: aload_2
    //   173: invokevirtual 203	java/io/OutputStream:flush	()V
    //   176: aload_3
    //   177: invokevirtual 206	org/apache/xml/security/utils/DigesterOutputStream:a	()[B
    //   180: areturn
    //   181: aload 4
    //   183: aload_2
    //   184: invokevirtual 208	org/apache/xml/security/signature/XMLSignatureInput:a	(Ljava/io/OutputStream;)V
    //   187: goto -15 -> 172
    //   190: astore_2
    //   191: new 210	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   194: astore_3
    //   195: aload_3
    //   196: ldc -124
    //   198: aload_2
    //   199: invokespecial 211	org/apache/xml/security/signature/ReferenceNotInitializedException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   202: aload_3
    //   203: athrow
    //   204: astore_2
    //   205: new 210	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   208: astore_3
    //   209: aload_3
    //   210: ldc -124
    //   212: aload_2
    //   213: invokespecial 211	org/apache/xml/security/signature/ReferenceNotInitializedException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   216: aload_3
    //   217: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	218	0	this	Reference
    //   0	218	1	paramBoolean	boolean
    //   4	180	2	localObject1	Object
    //   190	9	2	localXMLSecurityException	XMLSecurityException
    //   204	9	2	localIOException	java.io.IOException
    //   12	205	3	localObject2	Object
    //   32	150	4	localXMLSignatureInput	XMLSignatureInput
    //   37	131	5	bool	boolean
    //   76	79	6	localObject3	Object
    //   92	65	7	localObject4	Object
    //   130	7	8	localElement	Element
    // Exception table:
    //   from	to	target	type
    //   0	4	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   5	9	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   9	12	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   14	18	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   18	21	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   23	27	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   28	32	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   34	37	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   48	53	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   60	65	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   72	76	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   83	86	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   88	92	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   96	101	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   102	107	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   107	111	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   113	117	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   119	124	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   126	130	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   136	144	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   144	148	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   156	161	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   167	172	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   172	176	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   176	180	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   183	187	190	org/apache/xml/security/exceptions/XMLSecurityException
    //   0	4	204	java/io/IOException
    //   5	9	204	java/io/IOException
    //   9	12	204	java/io/IOException
    //   14	18	204	java/io/IOException
    //   18	21	204	java/io/IOException
    //   23	27	204	java/io/IOException
    //   28	32	204	java/io/IOException
    //   34	37	204	java/io/IOException
    //   48	53	204	java/io/IOException
    //   60	65	204	java/io/IOException
    //   72	76	204	java/io/IOException
    //   83	86	204	java/io/IOException
    //   88	92	204	java/io/IOException
    //   96	101	204	java/io/IOException
    //   102	107	204	java/io/IOException
    //   107	111	204	java/io/IOException
    //   113	117	204	java/io/IOException
    //   119	124	204	java/io/IOException
    //   126	130	204	java/io/IOException
    //   136	144	204	java/io/IOException
    //   144	148	204	java/io/IOException
    //   156	161	204	java/io/IOException
    //   167	172	204	java/io/IOException
    //   172	176	204	java/io/IOException
    //   176	180	204	java/io/IOException
    //   183	187	204	java/io/IOException
  }
  
  public MessageDigestAlgorithm a()
  {
    MessageDigestAlgorithm localMessageDigestAlgorithm = null;
    Object localObject = g;
    if (localObject == null) {}
    for (;;)
    {
      return localMessageDigestAlgorithm;
      localObject = g;
      String str = "Algorithm";
      localObject = ((Element)localObject).getAttributeNS(null, str);
      if (localObject != null) {
        localMessageDigestAlgorithm = MessageDigestAlgorithm.a(m, (String)localObject);
      }
    }
  }
  
  protected XMLSignatureInput a(OutputStream paramOutputStream)
  {
    try
    {
      XMLSignatureInput localXMLSignatureInput = g();
      localXMLSignatureInput = a(localXMLSignatureInput, paramOutputStream);
      c = localXMLSignatureInput;
      return localXMLSignatureInput;
    }
    catch (XMLSecurityException localXMLSecurityException)
    {
      ReferenceNotInitializedException localReferenceNotInitializedException = new org/apache/xml/security/signature/ReferenceNotInitializedException;
      localReferenceNotInitializedException.<init>("empty", localXMLSecurityException);
      throw localReferenceNotInitializedException;
    }
  }
  
  public String b()
  {
    return k.getAttributeNS(null, "URI");
  }
  
  public String c()
  {
    return k.getAttributeNS(null, "Type");
  }
  
  public String e()
  {
    return "Reference";
  }
  
  public boolean f()
  {
    String str1 = "http://www.w3.org/2000/09/xmldsig#Manifest";
    String str2 = c();
    boolean bool = str1.equals(str2);
    if (bool) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      str1 = null;
    }
  }
  
  public XMLSignatureInput g()
  {
    ReferenceNotInitializedException localReferenceNotInitializedException = null;
    try
    {
      localObject2 = k;
      localObject3 = null;
      localObject4 = "URI";
      localObject2 = ((Element)localObject2).getAttributeNodeNS(null, (String)localObject4);
      if (localObject2 != null) {
        break label101;
      }
    }
    catch (ResourceResolverException localResourceResolverException)
    {
      Object localObject3;
      for (;;)
      {
        Object localObject4;
        int i;
        localObject2 = new org/apache/xml/security/signature/ReferenceNotInitializedException;
        ((ReferenceNotInitializedException)localObject2).<init>("empty", localResourceResolverException);
        throw ((Throwable)localObject2);
        localObject1 = ((Attr)localObject2).getNodeValue();
      }
      Object localObject1 = b;
      localObject1 = d;
      ((ResourceResolver)localObject3).a((Map)localObject1);
      localObject1 = l;
      return ((ResourceResolver)localObject3).b((Attr)localObject2, (String)localObject1);
    }
    catch (XMLSecurityException localXMLSecurityException)
    {
      Object localObject2 = new org/apache/xml/security/signature/ReferenceNotInitializedException;
      ((ReferenceNotInitializedException)localObject2).<init>("empty", localXMLSecurityException);
      throw ((Throwable)localObject2);
    }
    localObject3 = l;
    localObject4 = b;
    localObject4 = e;
    localObject3 = ResourceResolver.a((Attr)localObject2, (String)localObject3, (List)localObject4);
    if (localObject3 == null)
    {
      i = 1;
      localObject2 = new Object[i];
      localObject3 = null;
      localObject2[0] = localReferenceNotInitializedException;
      localReferenceNotInitializedException = new org/apache/xml/security/signature/ReferenceNotInitializedException;
      localObject3 = "signature.Verification.Reference.NoInput";
      localReferenceNotInitializedException.<init>((String)localObject3, (Object[])localObject2);
      throw localReferenceNotInitializedException;
    }
  }
  
  public Transforms h()
  {
    return f;
  }
  
  public byte[] i()
  {
    Object localObject = h;
    if (localObject == null)
    {
      localObject = new Object[2];
      localObject[0] = "DigestValue";
      localObject[1] = "http://www.w3.org/2000/09/xmldsig#";
      XMLSecurityException localXMLSecurityException = new org/apache/xml/security/exceptions/XMLSecurityException;
      localXMLSecurityException.<init>("signature.Verification.NoSignatureElement", (Object[])localObject);
      throw localXMLSecurityException;
    }
    return Base64.a(h);
  }
  
  public boolean j()
  {
    Object localObject1 = i();
    boolean bool1 = true;
    Object localObject2 = a(bool1);
    boolean bool2 = MessageDigestAlgorithm.a((byte[])localObject1, (byte[])localObject2);
    Object localObject3;
    if (!bool2)
    {
      localObject3 = a;
      Object localObject4 = new java/lang/StringBuffer;
      ((StringBuffer)localObject4).<init>();
      localObject4 = ((StringBuffer)localObject4).append("Verification failed for URI \"");
      String str = b();
      localObject4 = str + "\"";
      ((Log)localObject3).warn(localObject4);
      localObject3 = a;
      localObject4 = new java/lang/StringBuffer;
      ((StringBuffer)localObject4).<init>();
      str = "Expected Digest: ";
      localObject4 = ((StringBuffer)localObject4).append(str);
      localObject1 = Base64.b((byte[])localObject1);
      localObject1 = (String)localObject1;
      ((Log)localObject3).warn(localObject1);
      localObject1 = a;
      localObject3 = new java/lang/StringBuffer;
      ((StringBuffer)localObject3).<init>();
      localObject4 = "Actual Digest: ";
      localObject3 = ((StringBuffer)localObject3).append((String)localObject4);
      localObject2 = Base64.b((byte[])localObject2);
      localObject2 = (String)localObject2;
      ((Log)localObject1).warn(localObject2);
    }
    for (;;)
    {
      return bool2;
      localObject1 = a;
      localObject2 = new java/lang/StringBuffer;
      ((StringBuffer)localObject2).<init>();
      localObject2 = ((StringBuffer)localObject2).append("Verification successful for URI \"");
      localObject3 = b();
      localObject2 = ((StringBuffer)localObject2).append((String)localObject3);
      localObject3 = "\"";
      localObject2 = (String)localObject3;
      ((Log)localObject1).debug(localObject2);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/signature/Reference.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */