package org.apache.xml.security.signature;

public class MissingResourceFailureException
  extends XMLSignatureException
{
  Reference c = null;
  
  public MissingResourceFailureException(String paramString, Reference paramReference)
  {
    super(paramString);
    c = paramReference;
  }
  
  public MissingResourceFailureException(String paramString, Object[] paramArrayOfObject, Exception paramException, Reference paramReference)
  {
    super(paramString, paramArrayOfObject, paramException);
    c = paramReference;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/signature/MissingResourceFailureException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */