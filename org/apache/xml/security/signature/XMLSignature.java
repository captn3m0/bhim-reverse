package org.apache.xml.security.signature;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.apache.xml.security.keys.KeyInfo;
import org.apache.xml.security.utils.Base64;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Element;

public final class XMLSignature
  extends SignatureElementProxy
{
  static Log a;
  static Class b;
  private SignedInfo c = null;
  private KeyInfo d = null;
  private boolean e = false;
  private Element f;
  private int g = 0;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.signature.XMLSignature");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = b;
    }
  }
  
  public XMLSignature(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
    Object localObject1 = XMLUtils.a(paramElement.getFirstChild());
    if (localObject1 == null)
    {
      localObject1 = new Object[i];
      localObject1[0] = "SignedInfo";
      localObject1[j] = "Signature";
      localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject2).<init>("xml.WrongContent", (Object[])localObject1);
      throw ((Throwable)localObject2);
    }
    Object localObject2 = new org/apache/xml/security/signature/SignedInfo;
    ((SignedInfo)localObject2).<init>((Element)localObject1, paramString);
    c = ((SignedInfo)localObject2);
    localObject1 = XMLUtils.a(XMLUtils.a(paramElement.getFirstChild()).getNextSibling());
    f = ((Element)localObject1);
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = new Object[i];
      localObject1[0] = "SignatureValue";
      localObject1[j] = "Signature";
      localObject2 = new org/apache/xml/security/signature/XMLSignatureException;
      ((XMLSignatureException)localObject2).<init>("xml.WrongContent", (Object[])localObject1);
      throw ((Throwable)localObject2);
    }
    localObject1 = XMLUtils.a(f.getNextSibling());
    if (localObject1 != null)
    {
      localObject2 = ((Element)localObject1).getNamespaceURI();
      str = "http://www.w3.org/2000/09/xmldsig#";
      boolean bool = ((String)localObject2).equals(str);
      if (bool)
      {
        localObject2 = ((Element)localObject1).getLocalName();
        str = "KeyInfo";
        bool = ((String)localObject2).equals(str);
        if (bool)
        {
          localObject2 = new org/apache/xml/security/keys/KeyInfo;
          ((KeyInfo)localObject2).<init>((Element)localObject1, paramString);
          d = ((KeyInfo)localObject2);
        }
      }
    }
    g = j;
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public SignedInfo a()
  {
    return c;
  }
  
  /* Error */
  public boolean a(java.security.Key paramKey)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: aload_1
    //   5: ifnonnull +29 -> 34
    //   8: iconst_1
    //   9: anewarray 66	java/lang/Object
    //   12: astore 4
    //   14: aload 4
    //   16: iconst_0
    //   17: ldc 122
    //   19: aastore
    //   20: new 72	org/apache/xml/security/signature/XMLSignatureException
    //   23: astore_3
    //   24: aload_3
    //   25: ldc 124
    //   27: aload 4
    //   29: invokespecial 77	org/apache/xml/security/signature/XMLSignatureException:<init>	(Ljava/lang/String;[Ljava/lang/Object;)V
    //   32: aload_3
    //   33: athrow
    //   34: aload_0
    //   35: invokevirtual 127	org/apache/xml/security/signature/XMLSignature:a	()Lorg/apache/xml/security/signature/SignedInfo;
    //   38: astore 5
    //   40: aload 5
    //   42: invokevirtual 130	org/apache/xml/security/signature/SignedInfo:c	()Lorg/apache/xml/security/algorithms/SignatureAlgorithm;
    //   45: astore 6
    //   47: getstatic 39	org/apache/xml/security/signature/XMLSignature:a	Lorg/apache/commons/logging/Log;
    //   50: astore 4
    //   52: aload 4
    //   54: invokeinterface 136 1 0
    //   59: istore 7
    //   61: iload 7
    //   63: ifeq +235 -> 298
    //   66: getstatic 39	org/apache/xml/security/signature/XMLSignature:a	Lorg/apache/commons/logging/Log;
    //   69: astore 4
    //   71: new 138	java/lang/StringBuffer
    //   74: astore 8
    //   76: aload 8
    //   78: invokespecial 139	java/lang/StringBuffer:<init>	()V
    //   81: ldc -115
    //   83: astore 9
    //   85: aload 8
    //   87: aload 9
    //   89: invokevirtual 145	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   92: astore 8
    //   94: aload 6
    //   96: invokevirtual 149	org/apache/xml/security/algorithms/SignatureAlgorithm:a	()Ljava/lang/String;
    //   99: astore 9
    //   101: aload 8
    //   103: aload 9
    //   105: invokevirtual 145	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   108: astore 8
    //   110: aload 8
    //   112: invokevirtual 152	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   115: astore 8
    //   117: aload 4
    //   119: aload 8
    //   121: invokeinterface 156 2 0
    //   126: getstatic 39	org/apache/xml/security/signature/XMLSignature:a	Lorg/apache/commons/logging/Log;
    //   129: astore 4
    //   131: new 138	java/lang/StringBuffer
    //   134: astore 8
    //   136: aload 8
    //   138: invokespecial 139	java/lang/StringBuffer:<init>	()V
    //   141: ldc -98
    //   143: astore 9
    //   145: aload 8
    //   147: aload 9
    //   149: invokevirtual 145	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   152: astore 8
    //   154: aload 6
    //   156: invokevirtual 160	org/apache/xml/security/algorithms/SignatureAlgorithm:b	()Ljava/lang/String;
    //   159: astore 9
    //   161: aload 8
    //   163: aload 9
    //   165: invokevirtual 145	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   168: astore 8
    //   170: aload 8
    //   172: invokevirtual 152	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   175: astore 8
    //   177: aload 4
    //   179: aload 8
    //   181: invokeinterface 156 2 0
    //   186: getstatic 39	org/apache/xml/security/signature/XMLSignature:a	Lorg/apache/commons/logging/Log;
    //   189: astore 4
    //   191: new 138	java/lang/StringBuffer
    //   194: astore 8
    //   196: aload 8
    //   198: invokespecial 139	java/lang/StringBuffer:<init>	()V
    //   201: ldc -94
    //   203: astore 9
    //   205: aload 8
    //   207: aload 9
    //   209: invokevirtual 145	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   212: astore 8
    //   214: aload 6
    //   216: invokevirtual 164	org/apache/xml/security/algorithms/SignatureAlgorithm:c	()Ljava/lang/String;
    //   219: astore 9
    //   221: aload 8
    //   223: aload 9
    //   225: invokevirtual 145	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   228: astore 8
    //   230: aload 8
    //   232: invokevirtual 152	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   235: astore 8
    //   237: aload 4
    //   239: aload 8
    //   241: invokeinterface 156 2 0
    //   246: getstatic 39	org/apache/xml/security/signature/XMLSignature:a	Lorg/apache/commons/logging/Log;
    //   249: astore 4
    //   251: new 138	java/lang/StringBuffer
    //   254: astore 8
    //   256: aload 8
    //   258: invokespecial 139	java/lang/StringBuffer:<init>	()V
    //   261: ldc -90
    //   263: astore 9
    //   265: aload 8
    //   267: aload 9
    //   269: invokevirtual 145	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   272: astore 8
    //   274: aload 8
    //   276: aload_1
    //   277: invokevirtual 169	java/lang/StringBuffer:append	(Ljava/lang/Object;)Ljava/lang/StringBuffer;
    //   280: astore 8
    //   282: aload 8
    //   284: invokevirtual 152	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   287: astore 8
    //   289: aload 4
    //   291: aload 8
    //   293: invokeinterface 156 2 0
    //   298: iconst_0
    //   299: istore 7
    //   301: aconst_null
    //   302: astore 4
    //   304: aload 6
    //   306: aload_1
    //   307: invokevirtual 172	org/apache/xml/security/algorithms/SignatureAlgorithm:a	(Ljava/security/Key;)V
    //   310: new 174	org/apache/xml/security/utils/SignerOutputStream
    //   313: astore 8
    //   315: aload 8
    //   317: aload 6
    //   319: invokespecial 177	org/apache/xml/security/utils/SignerOutputStream:<init>	(Lorg/apache/xml/security/algorithms/SignatureAlgorithm;)V
    //   322: new 179	org/apache/xml/security/utils/UnsyncBufferedOutputStream
    //   325: astore 9
    //   327: aload 9
    //   329: aload 8
    //   331: invokespecial 182	org/apache/xml/security/utils/UnsyncBufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   334: aload 5
    //   336: aload 9
    //   338: invokevirtual 184	org/apache/xml/security/signature/SignedInfo:a	(Ljava/io/OutputStream;)V
    //   341: aload 9
    //   343: invokevirtual 189	java/io/OutputStream:close	()V
    //   346: aload_0
    //   347: invokevirtual 192	org/apache/xml/security/signature/XMLSignature:b	()[B
    //   350: astore 4
    //   352: aload 6
    //   354: aload 4
    //   356: invokevirtual 195	org/apache/xml/security/algorithms/SignatureAlgorithm:b	([B)Z
    //   359: istore 7
    //   361: iload 7
    //   363: ifne +59 -> 422
    //   366: getstatic 39	org/apache/xml/security/signature/XMLSignature:a	Lorg/apache/commons/logging/Log;
    //   369: astore 4
    //   371: ldc -59
    //   373: astore 5
    //   375: aload 4
    //   377: aload 5
    //   379: invokeinterface 200 2 0
    //   384: iload_2
    //   385: ireturn
    //   386: astore 8
    //   388: aload 6
    //   390: invokevirtual 202	org/apache/xml/security/algorithms/SignatureAlgorithm:f	()V
    //   393: goto -41 -> 352
    //   396: athrow
    //   397: astore_3
    //   398: aload 6
    //   400: invokevirtual 202	org/apache/xml/security/algorithms/SignatureAlgorithm:f	()V
    //   403: aload_3
    //   404: athrow
    //   405: astore_3
    //   406: new 72	org/apache/xml/security/signature/XMLSignatureException
    //   409: astore 4
    //   411: aload 4
    //   413: ldc 124
    //   415: aload_3
    //   416: invokespecial 205	org/apache/xml/security/signature/XMLSignatureException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   419: aload 4
    //   421: athrow
    //   422: aload_0
    //   423: getfield 51	org/apache/xml/security/signature/XMLSignature:e	Z
    //   426: istore_2
    //   427: aload 5
    //   429: iload_2
    //   430: invokevirtual 208	org/apache/xml/security/signature/SignedInfo:b	(Z)Z
    //   433: istore_2
    //   434: goto -50 -> 384
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	437	0	this	XMLSignature
    //   0	437	1	paramKey	java.security.Key
    //   1	433	2	bool1	boolean
    //   3	30	3	localXMLSignatureException1	XMLSignatureException
    //   397	7	3	localXMLSecurityException1	org.apache.xml.security.exceptions.XMLSecurityException
    //   405	11	3	localXMLSecurityException2	org.apache.xml.security.exceptions.XMLSecurityException
    //   12	408	4	localObject1	Object
    //   38	390	5	localObject2	Object
    //   45	354	6	localSignatureAlgorithm	org.apache.xml.security.algorithms.SignatureAlgorithm
    //   59	303	7	bool2	boolean
    //   74	256	8	localObject3	Object
    //   386	1	8	localIOException	java.io.IOException
    //   83	259	9	localObject4	Object
    //   396	1	13	localXMLSignatureException2	XMLSignatureException
    // Exception table:
    //   from	to	target	type
    //   306	310	386	java/io/IOException
    //   310	313	386	java/io/IOException
    //   317	322	386	java/io/IOException
    //   322	325	386	java/io/IOException
    //   329	334	386	java/io/IOException
    //   336	341	386	java/io/IOException
    //   341	346	386	java/io/IOException
    //   346	350	386	java/io/IOException
    //   34	38	396	org/apache/xml/security/signature/XMLSignatureException
    //   40	45	396	org/apache/xml/security/signature/XMLSignatureException
    //   47	50	396	org/apache/xml/security/signature/XMLSignatureException
    //   52	59	396	org/apache/xml/security/signature/XMLSignatureException
    //   66	69	396	org/apache/xml/security/signature/XMLSignatureException
    //   71	74	396	org/apache/xml/security/signature/XMLSignatureException
    //   76	81	396	org/apache/xml/security/signature/XMLSignatureException
    //   87	92	396	org/apache/xml/security/signature/XMLSignatureException
    //   94	99	396	org/apache/xml/security/signature/XMLSignatureException
    //   103	108	396	org/apache/xml/security/signature/XMLSignatureException
    //   110	115	396	org/apache/xml/security/signature/XMLSignatureException
    //   119	126	396	org/apache/xml/security/signature/XMLSignatureException
    //   126	129	396	org/apache/xml/security/signature/XMLSignatureException
    //   131	134	396	org/apache/xml/security/signature/XMLSignatureException
    //   136	141	396	org/apache/xml/security/signature/XMLSignatureException
    //   147	152	396	org/apache/xml/security/signature/XMLSignatureException
    //   154	159	396	org/apache/xml/security/signature/XMLSignatureException
    //   163	168	396	org/apache/xml/security/signature/XMLSignatureException
    //   170	175	396	org/apache/xml/security/signature/XMLSignatureException
    //   179	186	396	org/apache/xml/security/signature/XMLSignatureException
    //   186	189	396	org/apache/xml/security/signature/XMLSignatureException
    //   191	194	396	org/apache/xml/security/signature/XMLSignatureException
    //   196	201	396	org/apache/xml/security/signature/XMLSignatureException
    //   207	212	396	org/apache/xml/security/signature/XMLSignatureException
    //   214	219	396	org/apache/xml/security/signature/XMLSignatureException
    //   223	228	396	org/apache/xml/security/signature/XMLSignatureException
    //   230	235	396	org/apache/xml/security/signature/XMLSignatureException
    //   239	246	396	org/apache/xml/security/signature/XMLSignatureException
    //   246	249	396	org/apache/xml/security/signature/XMLSignatureException
    //   251	254	396	org/apache/xml/security/signature/XMLSignatureException
    //   256	261	396	org/apache/xml/security/signature/XMLSignatureException
    //   267	272	396	org/apache/xml/security/signature/XMLSignatureException
    //   276	280	396	org/apache/xml/security/signature/XMLSignatureException
    //   282	287	396	org/apache/xml/security/signature/XMLSignatureException
    //   291	298	396	org/apache/xml/security/signature/XMLSignatureException
    //   306	310	396	org/apache/xml/security/signature/XMLSignatureException
    //   310	313	396	org/apache/xml/security/signature/XMLSignatureException
    //   317	322	396	org/apache/xml/security/signature/XMLSignatureException
    //   322	325	396	org/apache/xml/security/signature/XMLSignatureException
    //   329	334	396	org/apache/xml/security/signature/XMLSignatureException
    //   336	341	396	org/apache/xml/security/signature/XMLSignatureException
    //   341	346	396	org/apache/xml/security/signature/XMLSignatureException
    //   346	350	396	org/apache/xml/security/signature/XMLSignatureException
    //   354	359	396	org/apache/xml/security/signature/XMLSignatureException
    //   366	369	396	org/apache/xml/security/signature/XMLSignatureException
    //   377	384	396	org/apache/xml/security/signature/XMLSignatureException
    //   388	393	396	org/apache/xml/security/signature/XMLSignatureException
    //   398	403	396	org/apache/xml/security/signature/XMLSignatureException
    //   403	405	396	org/apache/xml/security/signature/XMLSignatureException
    //   422	426	396	org/apache/xml/security/signature/XMLSignatureException
    //   429	433	396	org/apache/xml/security/signature/XMLSignatureException
    //   306	310	397	org/apache/xml/security/exceptions/XMLSecurityException
    //   310	313	397	org/apache/xml/security/exceptions/XMLSecurityException
    //   317	322	397	org/apache/xml/security/exceptions/XMLSecurityException
    //   322	325	397	org/apache/xml/security/exceptions/XMLSecurityException
    //   329	334	397	org/apache/xml/security/exceptions/XMLSecurityException
    //   336	341	397	org/apache/xml/security/exceptions/XMLSecurityException
    //   341	346	397	org/apache/xml/security/exceptions/XMLSecurityException
    //   346	350	397	org/apache/xml/security/exceptions/XMLSecurityException
    //   34	38	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   40	45	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   47	50	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   52	59	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   66	69	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   71	74	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   76	81	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   87	92	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   94	99	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   103	108	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   110	115	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   119	126	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   126	129	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   131	134	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   136	141	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   147	152	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   154	159	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   163	168	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   170	175	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   179	186	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   186	189	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   191	194	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   196	201	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   207	212	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   214	219	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   223	228	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   230	235	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   239	246	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   246	249	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   251	254	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   256	261	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   267	272	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   276	280	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   282	287	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   291	298	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   354	359	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   366	369	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   377	384	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   388	393	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   398	403	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   403	405	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   422	426	405	org/apache/xml/security/exceptions/XMLSecurityException
    //   429	433	405	org/apache/xml/security/exceptions/XMLSecurityException
  }
  
  public byte[] b()
  {
    try
    {
      Element localElement = f;
      return Base64.a(localElement);
    }
    catch (Base64DecodingException localBase64DecodingException)
    {
      XMLSignatureException localXMLSignatureException = new org/apache/xml/security/signature/XMLSignatureException;
      localXMLSignatureException.<init>("empty", localBase64DecodingException);
      throw localXMLSignatureException;
    }
  }
  
  public String e()
  {
    return "Signature";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/signature/XMLSignature.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */