package org.apache.xml.security.signature;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.c14n.CanonicalizationException;
import org.apache.xml.security.c14n.implementations.Canonicalizer11_OmitComments;
import org.apache.xml.security.c14n.implementations.Canonicalizer20010315OmitComments;
import org.apache.xml.security.c14n.implementations.CanonicalizerBase;
import org.apache.xml.security.exceptions.XMLSecurityRuntimeException;
import org.apache.xml.security.utils.IgnoreAllErrorHandler;
import org.apache.xml.security.utils.JavaUtils;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;

public class XMLSignatureInput
  implements Cloneable
{
  static Log a;
  static Class l;
  InputStream b = null;
  Set c = null;
  Node d = null;
  Node e = null;
  boolean f = false;
  boolean g = false;
  byte[] h = null;
  List i;
  boolean j;
  OutputStream k;
  private String m = null;
  private String n = null;
  
  static
  {
    Class localClass = l;
    if (localClass == null)
    {
      localClass = c("org.apache.xml.security.signature.XMLSignatureInput");
      l = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = l;
    }
  }
  
  public XMLSignatureInput(InputStream paramInputStream)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    i = localArrayList;
    j = false;
    k = null;
    b = paramInputStream;
  }
  
  public XMLSignatureInput(Node paramNode)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    i = localArrayList;
    j = false;
    k = null;
    d = paramNode;
  }
  
  public XMLSignatureInput(byte[] paramArrayOfByte)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    i = localArrayList;
    j = false;
    k = null;
    h = paramArrayOfByte;
  }
  
  static Class c(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public void a(OutputStream paramOutputStream)
  {
    a(paramOutputStream, false);
  }
  
  public void a(OutputStream paramOutputStream, boolean paramBoolean)
  {
    Object localObject1 = k;
    if (paramOutputStream == localObject1) {}
    int i3;
    for (;;)
    {
      return;
      localObject1 = h;
      if (localObject1 != null)
      {
        localObject1 = h;
        paramOutputStream.write((byte[])localObject1);
      }
      else
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          if (paramBoolean)
          {
            localObject1 = new org/apache/xml/security/c14n/implementations/Canonicalizer11_OmitComments;
            ((Canonicalizer11_OmitComments)localObject1).<init>();
          }
          for (;;)
          {
            ((CanonicalizerBase)localObject1).a(paramOutputStream);
            ((CanonicalizerBase)localObject1).b(this);
            break;
            localObject1 = new org/apache/xml/security/c14n/implementations/Canonicalizer20010315OmitComments;
            ((Canonicalizer20010315OmitComments)localObject1).<init>();
          }
        }
        localObject1 = b;
        boolean bool = localObject1 instanceof FileInputStream;
        if (bool)
        {
          int i1 = 4096;
          localObject1 = new byte[i1];
          for (;;)
          {
            localObject2 = b;
            i2 = ((InputStream)localObject2).read((byte[])localObject1);
            i3 = -1;
            if (i2 == i3) {
              break;
            }
            paramOutputStream.write((byte[])localObject1, 0, i2);
          }
        }
        localObject1 = o();
        localObject2 = h;
        if (localObject2 == null) {
          break;
        }
        localObject1 = h;
        localObject2 = h;
        i2 = localObject2.length;
        paramOutputStream.write((byte[])localObject1, 0, i2);
      }
    }
    ((InputStream)localObject1).reset();
    int i2 = 1024;
    Object localObject2 = new byte[i2];
    for (;;)
    {
      i3 = ((InputStream)localObject1).read((byte[])localObject2);
      if (i3 <= 0) {
        break;
      }
      paramOutputStream.write((byte[])localObject2, 0, i3);
    }
  }
  
  public void a(String paramString)
  {
    m = paramString;
  }
  
  public void a(NodeFilter paramNodeFilter)
  {
    boolean bool = h();
    if (bool) {}
    try
    {
      q();
      i.add(paramNodeFilter);
      return;
    }
    catch (Exception localException)
    {
      XMLSecurityRuntimeException localXMLSecurityRuntimeException = new org/apache/xml/security/exceptions/XMLSecurityRuntimeException;
      localXMLSecurityRuntimeException.<init>("signature.XMLSignatureInput.nodesetReference", localException);
      throw localXMLSecurityRuntimeException;
    }
  }
  
  public void a(Node paramNode)
  {
    e = paramNode;
  }
  
  public void a(boolean paramBoolean)
  {
    j = paramBoolean;
  }
  
  public boolean a()
  {
    return j;
  }
  
  public Set b()
  {
    return b(false);
  }
  
  public Set b(boolean paramBoolean)
  {
    Object localObject1 = c;
    if (localObject1 != null) {
      localObject1 = c;
    }
    for (;;)
    {
      return (Set)localObject1;
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject1 = d;
        if (localObject1 != null)
        {
          if (paramBoolean)
          {
            localObject1 = XMLUtils.b(d);
            XMLUtils.a((Document)localObject1);
          }
          localObject1 = new java/util/HashSet;
          ((HashSet)localObject1).<init>();
          c = ((Set)localObject1);
          localObject1 = d;
          localObject2 = c;
          localNode = e;
          bool1 = f;
          XMLUtils.a((Node)localObject1, (Set)localObject2, localNode, bool1);
          localObject1 = c;
          continue;
        }
      }
      boolean bool2 = h();
      if (!bool2) {
        break;
      }
      q();
      localObject1 = new java/util/HashSet;
      ((HashSet)localObject1).<init>();
      Object localObject2 = d;
      Node localNode = null;
      boolean bool1 = false;
      XMLUtils.a((Node)localObject2, (Set)localObject1, null, false);
    }
    localObject1 = new java/lang/RuntimeException;
    ((RuntimeException)localObject1).<init>("getNodeSet() called but no input data present");
    throw ((Throwable)localObject1);
  }
  
  public void b(OutputStream paramOutputStream)
  {
    k = paramOutputStream;
  }
  
  public void b(String paramString)
  {
    n = paramString;
  }
  
  public InputStream c()
  {
    InputStream localInputStream = b;
    boolean bool = localInputStream instanceof FileInputStream;
    if (bool) {}
    for (localInputStream = b;; localInputStream = o()) {
      return localInputStream;
    }
  }
  
  public void c(boolean paramBoolean)
  {
    f = paramBoolean;
  }
  
  public InputStream d()
  {
    return b;
  }
  
  public void d(boolean paramBoolean)
  {
    g = paramBoolean;
  }
  
  public byte[] e()
  {
    Object localObject = h;
    if (localObject != null) {
      localObject = h;
    }
    for (;;)
    {
      return (byte[])localObject;
      localObject = o();
      if (localObject != null)
      {
        byte[] arrayOfByte = h;
        if (arrayOfByte == null)
        {
          ((InputStream)localObject).reset();
          localObject = JavaUtils.a((InputStream)localObject);
          h = ((byte[])localObject);
        }
        localObject = h;
      }
      else
      {
        localObject = new org/apache/xml/security/c14n/implementations/Canonicalizer20010315OmitComments;
        ((Canonicalizer20010315OmitComments)localObject).<init>();
        localObject = ((Canonicalizer20010315OmitComments)localObject).b(this);
        h = ((byte[])localObject);
        localObject = h;
      }
    }
  }
  
  public boolean f()
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = c;
      if (localObject != null) {}
    }
    else
    {
      bool = g;
      if (!bool) {
        break label31;
      }
    }
    boolean bool = true;
    for (;;)
    {
      return bool;
      label31:
      bool = false;
      localObject = null;
    }
  }
  
  public boolean g()
  {
    Object localObject = b;
    boolean bool;
    if (localObject == null)
    {
      localObject = d;
      if (localObject != null)
      {
        localObject = c;
        if (localObject == null)
        {
          bool = g;
          if (!bool) {
            bool = true;
          }
        }
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  public boolean h()
  {
    Object localObject = b;
    boolean bool;
    if (localObject == null)
    {
      localObject = h;
      if (localObject == null) {}
    }
    else
    {
      localObject = c;
      if (localObject == null)
      {
        localObject = d;
        if (localObject == null) {
          bool = true;
        }
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  public boolean i()
  {
    OutputStream localOutputStream = k;
    boolean bool;
    if (localOutputStream != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localOutputStream = null;
    }
  }
  
  public boolean j()
  {
    Object localObject = h;
    boolean bool;
    if (localObject != null)
    {
      localObject = c;
      if (localObject == null)
      {
        localObject = d;
        if (localObject == null) {
          bool = true;
        }
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  public String k()
  {
    return n;
  }
  
  public Node l()
  {
    return e;
  }
  
  public Node m()
  {
    return d;
  }
  
  public boolean n()
  {
    return f;
  }
  
  protected InputStream o()
  {
    Object localObject1 = b;
    boolean bool = localObject1 instanceof ByteArrayInputStream;
    Object localObject2;
    if (bool)
    {
      localObject1 = b;
      bool = ((InputStream)localObject1).markSupported();
      if (!bool)
      {
        localObject1 = new java/lang/RuntimeException;
        localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        localObject2 = ((StringBuffer)localObject2).append("Accepted as Markable but not truly been");
        InputStream localInputStream = b;
        localObject2 = localInputStream;
        ((RuntimeException)localObject1).<init>((String)localObject2);
        throw ((Throwable)localObject1);
      }
      localObject1 = b;
    }
    for (;;)
    {
      return (InputStream)localObject1;
      localObject1 = h;
      if (localObject1 != null)
      {
        localObject1 = new java/io/ByteArrayInputStream;
        localObject2 = h;
        ((ByteArrayInputStream)localObject1).<init>((byte[])localObject2);
        b = ((InputStream)localObject1);
        localObject1 = b;
      }
      else
      {
        localObject1 = b;
        if (localObject1 == null)
        {
          bool = false;
          localObject1 = null;
        }
        else
        {
          localObject1 = b;
          bool = ((InputStream)localObject1).markSupported();
          if (bool)
          {
            localObject1 = a;
            localObject2 = "Mark Suported but not used as reset";
            ((Log)localObject1).info(localObject2);
          }
          localObject1 = JavaUtils.a(b);
          h = ((byte[])localObject1);
          b.close();
          localObject1 = new java/io/ByteArrayInputStream;
          localObject2 = h;
          ((ByteArrayInputStream)localObject1).<init>((byte[])localObject2);
          b = ((InputStream)localObject1);
          localObject1 = b;
        }
      }
    }
  }
  
  public List p()
  {
    return i;
  }
  
  void q()
  {
    localObject1 = DocumentBuilderFactory.newInstance();
    Object localObject2 = null;
    ((DocumentBuilderFactory)localObject1).setValidating(false);
    boolean bool = true;
    ((DocumentBuilderFactory)localObject1).setNamespaceAware(bool);
    localObject1 = ((DocumentBuilderFactory)localObject1).newDocumentBuilder();
    try
    {
      localObject2 = new org/apache/xml/security/utils/IgnoreAllErrorHandler;
      ((IgnoreAllErrorHandler)localObject2).<init>();
      ((DocumentBuilder)localObject1).setErrorHandler((ErrorHandler)localObject2);
      localObject2 = c();
      localObject2 = ((DocumentBuilder)localObject1).parse((InputStream)localObject2);
      d = ((Node)localObject2);
    }
    catch (SAXException localSAXException)
    {
      for (;;)
      {
        Object localObject3 = new java/io/ByteArrayOutputStream;
        ((ByteArrayOutputStream)localObject3).<init>();
        Object localObject4 = "<container>".getBytes();
        ((ByteArrayOutputStream)localObject3).write((byte[])localObject4);
        localObject4 = e();
        ((ByteArrayOutputStream)localObject3).write((byte[])localObject4);
        localObject4 = "</container>".getBytes();
        ((ByteArrayOutputStream)localObject3).write((byte[])localObject4);
        localObject3 = ((ByteArrayOutputStream)localObject3).toByteArray();
        localObject4 = new java/io/ByteArrayInputStream;
        ((ByteArrayInputStream)localObject4).<init>((byte[])localObject3);
        localObject1 = ((DocumentBuilder)localObject1).parse((InputStream)localObject4).getDocumentElement().getFirstChild().getFirstChild();
        d = ((Node)localObject1);
      }
    }
    b = null;
    h = null;
  }
  
  public String toString()
  {
    boolean bool1 = f();
    Object localObject1;
    Object localObject4;
    if (bool1)
    {
      localObject1 = new java/lang/StringBuffer;
      ((StringBuffer)localObject1).<init>();
      localObject1 = ((StringBuffer)localObject1).append("XMLSignatureInput/NodeSet/");
      int i1 = c.size();
      localObject1 = ((StringBuffer)localObject1).append(i1).append(" nodes/");
      localObject4 = k();
      localObject1 = (String)localObject4;
    }
    for (;;)
    {
      return (String)localObject1;
      bool1 = g();
      if (bool1)
      {
        localObject1 = new java/lang/StringBuffer;
        ((StringBuffer)localObject1).<init>();
        localObject1 = ((StringBuffer)localObject1).append("XMLSignatureInput/Element/");
        localObject4 = d;
        localObject1 = ((StringBuffer)localObject1).append(localObject4).append(" exclude ");
        localObject4 = e;
        localObject1 = ((StringBuffer)localObject1).append(localObject4).append(" comments:");
        boolean bool2 = f;
        localObject1 = ((StringBuffer)localObject1).append(bool2).append("/");
        localObject4 = k();
        localObject1 = (String)localObject4;
      }
      else
      {
        try
        {
          localObject1 = new java/lang/StringBuffer;
          ((StringBuffer)localObject1).<init>();
          localObject4 = "XMLSignatureInput/OctetStream/";
          localObject1 = ((StringBuffer)localObject1).append((String)localObject4);
          localObject4 = e();
          int i2 = localObject4.length;
          localObject1 = ((StringBuffer)localObject1).append(i2);
          localObject4 = " octets/";
          localObject1 = ((StringBuffer)localObject1).append((String)localObject4);
          localObject4 = k();
          localObject1 = ((StringBuffer)localObject1).append((String)localObject4);
          localObject1 = ((StringBuffer)localObject1).toString();
        }
        catch (IOException localIOException)
        {
          Object localObject2 = new java/lang/StringBuffer;
          ((StringBuffer)localObject2).<init>();
          localObject2 = ((StringBuffer)localObject2).append("XMLSignatureInput/OctetStream//");
          localObject4 = k();
          localObject2 = (String)localObject4;
        }
        catch (CanonicalizationException localCanonicalizationException)
        {
          Object localObject3 = new java/lang/StringBuffer;
          ((StringBuffer)localObject3).<init>();
          localObject3 = ((StringBuffer)localObject3).append("XMLSignatureInput/OctetStream//");
          localObject4 = k();
          localObject3 = (String)localObject4;
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/signature/XMLSignatureInput.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */