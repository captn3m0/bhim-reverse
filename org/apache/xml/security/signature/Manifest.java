package org.apache.xml.security.signature;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.utils.I18n;
import org.apache.xml.security.utils.SignatureElementProxy;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.DOMException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Manifest
  extends SignatureElementProxy
{
  static Log a;
  static Class f;
  List b;
  Element[] c;
  HashMap d = null;
  List e = null;
  private boolean[] g = null;
  
  static
  {
    Class localClass = f;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.signature.Manifest");
      f = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = f;
    }
  }
  
  public Manifest(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
    Object localObject1 = k.getFirstChild();
    Object localObject2 = "Reference";
    localObject1 = XMLUtils.a((Node)localObject1, (String)localObject2);
    c = ((Element[])localObject1);
    localObject1 = c;
    int j = localObject1.length;
    if (j == 0)
    {
      localObject1 = new Object[2];
      localObject1[0] = "Reference";
      localObject1[1] = "Manifest";
      localDOMException = new org/w3c/dom/DOMException;
      localObject1 = I18n.a("xml.WrongContent", (Object[])localObject1);
      localDOMException.<init>((short)4, (String)localObject1);
      throw localDOMException;
    }
    localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>(j);
    b = ((List)localObject2);
    while (i < j)
    {
      localObject2 = b;
      ((List)localObject2).add(null);
      i += 1;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  private void a(int paramInt, boolean paramBoolean)
  {
    boolean[] arrayOfBoolean = g;
    if (arrayOfBoolean == null)
    {
      int i = a();
      arrayOfBoolean = new boolean[i];
      g = arrayOfBoolean;
    }
    g[paramInt] = paramBoolean;
  }
  
  public int a()
  {
    return b.size();
  }
  
  /* Error */
  public boolean a(boolean paramBoolean)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: ldc 71
    //   4: fstore_3
    //   5: aload_0
    //   6: getfield 67	org/apache/xml/security/signature/Manifest:c	[Lorg/w3c/dom/Element;
    //   9: astore 4
    //   11: aload 4
    //   13: ifnonnull +33 -> 46
    //   16: aload_0
    //   17: getfield 52	org/apache/xml/security/signature/Manifest:k	Lorg/w3c/dom/Element;
    //   20: invokeinterface 58 1 0
    //   25: astore 4
    //   27: ldc 60
    //   29: astore 5
    //   31: aload 4
    //   33: aload 5
    //   35: invokestatic 65	org/apache/xml/security/utils/XMLUtils:a	(Lorg/w3c/dom/Node;Ljava/lang/String;)[Lorg/w3c/dom/Element;
    //   38: astore 4
    //   40: aload_0
    //   41: aload 4
    //   43: putfield 67	org/apache/xml/security/signature/Manifest:c	[Lorg/w3c/dom/Element;
    //   46: getstatic 38	org/apache/xml/security/signature/Manifest:a	Lorg/apache/commons/logging/Log;
    //   49: astore 4
    //   51: aload 4
    //   53: invokeinterface 125 1 0
    //   58: istore 6
    //   60: iload 6
    //   62: ifeq +126 -> 188
    //   65: getstatic 38	org/apache/xml/security/signature/Manifest:a	Lorg/apache/commons/logging/Log;
    //   68: astore 4
    //   70: new 127	java/lang/StringBuffer
    //   73: astore 5
    //   75: aload 5
    //   77: invokespecial 128	java/lang/StringBuffer:<init>	()V
    //   80: aload 5
    //   82: ldc -126
    //   84: invokevirtual 134	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   87: astore 5
    //   89: aload_0
    //   90: getfield 67	org/apache/xml/security/signature/Manifest:c	[Lorg/w3c/dom/Element;
    //   93: arraylength
    //   94: istore 7
    //   96: aload 5
    //   98: iload 7
    //   100: invokevirtual 137	java/lang/StringBuffer:append	(I)Ljava/lang/StringBuffer;
    //   103: ldc -117
    //   105: invokevirtual 134	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   108: invokevirtual 142	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   111: astore 5
    //   113: aload 4
    //   115: aload 5
    //   117: invokeinterface 146 2 0
    //   122: getstatic 38	org/apache/xml/security/signature/Manifest:a	Lorg/apache/commons/logging/Log;
    //   125: astore 5
    //   127: new 127	java/lang/StringBuffer
    //   130: astore 4
    //   132: aload 4
    //   134: invokespecial 128	java/lang/StringBuffer:<init>	()V
    //   137: aload 4
    //   139: ldc -108
    //   141: invokevirtual 134	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   144: astore 8
    //   146: iload_1
    //   147: ifeq +72 -> 219
    //   150: ldc -106
    //   152: astore 4
    //   154: aload 8
    //   156: aload 4
    //   158: invokevirtual 134	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   161: astore 4
    //   163: ldc -104
    //   165: astore 8
    //   167: aload 4
    //   169: aload 8
    //   171: invokevirtual 134	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   174: invokevirtual 142	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   177: astore 4
    //   179: aload 5
    //   181: aload 4
    //   183: invokeinterface 146 2 0
    //   188: aload_0
    //   189: getfield 67	org/apache/xml/security/signature/Manifest:c	[Lorg/w3c/dom/Element;
    //   192: astore 4
    //   194: aload 4
    //   196: arraylength
    //   197: istore 6
    //   199: iload 6
    //   201: ifne +25 -> 226
    //   204: new 154	org/apache/xml/security/exceptions/XMLSecurityException
    //   207: astore 4
    //   209: aload 4
    //   211: ldc -100
    //   213: invokespecial 159	org/apache/xml/security/exceptions/XMLSecurityException:<init>	(Ljava/lang/String;)V
    //   216: aload 4
    //   218: athrow
    //   219: ldc -95
    //   221: astore 4
    //   223: goto -69 -> 154
    //   226: aload_0
    //   227: getfield 67	org/apache/xml/security/signature/Manifest:c	[Lorg/w3c/dom/Element;
    //   230: arraylength
    //   231: newarray <illegal type>
    //   233: astore 4
    //   235: aload_0
    //   236: aload 4
    //   238: putfield 44	org/apache/xml/security/signature/Manifest:g	[Z
    //   241: iconst_0
    //   242: istore 7
    //   244: aconst_null
    //   245: astore 8
    //   247: iload_2
    //   248: istore 6
    //   250: fload_3
    //   251: fstore 9
    //   253: aload_0
    //   254: getfield 67	org/apache/xml/security/signature/Manifest:c	[Lorg/w3c/dom/Element;
    //   257: astore 5
    //   259: aload 5
    //   261: arraylength
    //   262: istore 10
    //   264: iload 7
    //   266: iload 10
    //   268: if_icmpge +650 -> 918
    //   271: new 163	org/apache/xml/security/signature/Reference
    //   274: astore 11
    //   276: aload_0
    //   277: getfield 67	org/apache/xml/security/signature/Manifest:c	[Lorg/w3c/dom/Element;
    //   280: iload 7
    //   282: aaload
    //   283: astore 5
    //   285: aload_0
    //   286: getfield 167	org/apache/xml/security/signature/Manifest:l	Ljava/lang/String;
    //   289: astore 12
    //   291: aload 11
    //   293: aload 5
    //   295: aload 12
    //   297: aload_0
    //   298: invokespecial 170	org/apache/xml/security/signature/Reference:<init>	(Lorg/w3c/dom/Element;Ljava/lang/String;Lorg/apache/xml/security/signature/Manifest;)V
    //   301: aload_0
    //   302: getfield 93	org/apache/xml/security/signature/Manifest:b	Ljava/util/List;
    //   305: astore 5
    //   307: aload 5
    //   309: iload 7
    //   311: aload 11
    //   313: invokeinterface 174 3 0
    //   318: pop
    //   319: aload 11
    //   321: invokevirtual 177	org/apache/xml/security/signature/Reference:j	()Z
    //   324: istore 10
    //   326: aload_0
    //   327: iload 7
    //   329: iload 10
    //   331: invokespecial 180	org/apache/xml/security/signature/Manifest:a	(IZ)V
    //   334: iload 10
    //   336: ifne +613 -> 949
    //   339: iconst_0
    //   340: istore 13
    //   342: aconst_null
    //   343: astore 12
    //   345: fconst_0
    //   346: fstore 14
    //   348: getstatic 38	org/apache/xml/security/signature/Manifest:a	Lorg/apache/commons/logging/Log;
    //   351: astore 4
    //   353: aload 4
    //   355: invokeinterface 125 1 0
    //   360: istore 6
    //   362: iload 6
    //   364: ifeq +63 -> 427
    //   367: getstatic 38	org/apache/xml/security/signature/Manifest:a	Lorg/apache/commons/logging/Log;
    //   370: astore 4
    //   372: new 127	java/lang/StringBuffer
    //   375: astore 5
    //   377: aload 5
    //   379: invokespecial 128	java/lang/StringBuffer:<init>	()V
    //   382: ldc -74
    //   384: astore 15
    //   386: aload 5
    //   388: aload 15
    //   390: invokevirtual 134	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   393: astore 5
    //   395: aload 11
    //   397: invokevirtual 184	org/apache/xml/security/signature/Reference:c	()Ljava/lang/String;
    //   400: astore 15
    //   402: aload 5
    //   404: aload 15
    //   406: invokevirtual 134	java/lang/StringBuffer:append	(Ljava/lang/String;)Ljava/lang/StringBuffer;
    //   409: astore 5
    //   411: aload 5
    //   413: invokevirtual 142	java/lang/StringBuffer:toString	()Ljava/lang/String;
    //   416: astore 5
    //   418: aload 4
    //   420: aload 5
    //   422: invokeinterface 146 2 0
    //   427: iload 13
    //   429: ifeq +509 -> 938
    //   432: iload_1
    //   433: ifeq +505 -> 938
    //   436: aload 11
    //   438: invokevirtual 186	org/apache/xml/security/signature/Reference:f	()Z
    //   441: istore 6
    //   443: iload 6
    //   445: ifeq +493 -> 938
    //   448: getstatic 38	org/apache/xml/security/signature/Manifest:a	Lorg/apache/commons/logging/Log;
    //   451: astore 4
    //   453: ldc -68
    //   455: astore 5
    //   457: aload 4
    //   459: aload 5
    //   461: invokeinterface 146 2 0
    //   466: iconst_0
    //   467: istore 6
    //   469: fconst_0
    //   470: fstore 9
    //   472: aconst_null
    //   473: astore 4
    //   475: aload 11
    //   477: aconst_null
    //   478: invokevirtual 191	org/apache/xml/security/signature/Reference:a	(Ljava/io/OutputStream;)Lorg/apache/xml/security/signature/XMLSignatureInput;
    //   481: astore 15
    //   483: aload 15
    //   485: invokevirtual 196	org/apache/xml/security/signature/XMLSignatureInput:b	()Ljava/util/Set;
    //   488: astore 4
    //   490: aload 4
    //   492: invokeinterface 202 1 0
    //   497: astore 16
    //   499: aload 16
    //   501: invokeinterface 207 1 0
    //   506: istore 6
    //   508: iload 6
    //   510: ifeq +416 -> 926
    //   513: aload 16
    //   515: invokeinterface 211 1 0
    //   520: astore 4
    //   522: aload 4
    //   524: checkcast 213	org/w3c/dom/Node
    //   527: astore 4
    //   529: aload 4
    //   531: invokeinterface 217 1 0
    //   536: istore 10
    //   538: iload 10
    //   540: iload_2
    //   541: if_icmpne -42 -> 499
    //   544: aload 4
    //   546: astore 17
    //   548: aload 4
    //   550: checkcast 54	org/w3c/dom/Element
    //   553: astore 17
    //   555: aload 17
    //   557: astore 5
    //   559: aload 17
    //   561: invokeinterface 220 1 0
    //   566: astore 5
    //   568: ldc -34
    //   570: astore 18
    //   572: aload 5
    //   574: aload 18
    //   576: invokevirtual 227	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   579: istore 10
    //   581: iload 10
    //   583: ifeq -84 -> 499
    //   586: aload 4
    //   588: astore 17
    //   590: aload 4
    //   592: checkcast 54	org/w3c/dom/Element
    //   595: astore 17
    //   597: aload 17
    //   599: astore 5
    //   601: aload 17
    //   603: invokeinterface 230 1 0
    //   608: astore 5
    //   610: ldc 73
    //   612: astore 18
    //   614: aload 5
    //   616: aload 18
    //   618: invokevirtual 227	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   621: istore 10
    //   623: iload 10
    //   625: ifeq -126 -> 499
    //   628: new 2	org/apache/xml/security/signature/Manifest
    //   631: astore 5
    //   633: aload 4
    //   635: checkcast 54	org/w3c/dom/Element
    //   638: astore 4
    //   640: aload 15
    //   642: invokevirtual 232	org/apache/xml/security/signature/XMLSignatureInput:k	()Ljava/lang/String;
    //   645: astore 18
    //   647: aload 5
    //   649: aload 4
    //   651: aload 18
    //   653: invokespecial 233	org/apache/xml/security/signature/Manifest:<init>	(Lorg/w3c/dom/Element;Ljava/lang/String;)V
    //   656: aload 5
    //   658: astore 4
    //   660: aload 4
    //   662: ifnonnull +89 -> 751
    //   665: new 235	org/apache/xml/security/signature/MissingResourceFailureException
    //   668: astore 4
    //   670: ldc -100
    //   672: astore 5
    //   674: aload 4
    //   676: aload 5
    //   678: aload 11
    //   680: invokespecial 238	org/apache/xml/security/signature/MissingResourceFailureException:<init>	(Ljava/lang/String;Lorg/apache/xml/security/signature/Reference;)V
    //   683: aload 4
    //   685: athrow
    //   686: astore 4
    //   688: new 240	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   691: astore 5
    //   693: ldc -100
    //   695: astore 8
    //   697: aload 5
    //   699: aload 8
    //   701: aload 4
    //   703: invokespecial 243	org/apache/xml/security/signature/ReferenceNotInitializedException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   706: aload 5
    //   708: athrow
    //   709: astore 4
    //   711: iload_2
    //   712: anewarray 70	java/lang/Object
    //   715: astore 5
    //   717: aload 11
    //   719: invokevirtual 245	org/apache/xml/security/signature/Reference:b	()Ljava/lang/String;
    //   722: astore 8
    //   724: aload 5
    //   726: iconst_0
    //   727: aload 8
    //   729: aastore
    //   730: new 235	org/apache/xml/security/signature/MissingResourceFailureException
    //   733: astore 8
    //   735: aload 8
    //   737: ldc -9
    //   739: aload 5
    //   741: aload 4
    //   743: aload 11
    //   745: invokespecial 250	org/apache/xml/security/signature/MissingResourceFailureException:<init>	(Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Exception;Lorg/apache/xml/security/signature/Reference;)V
    //   748: aload 8
    //   750: athrow
    //   751: aload_0
    //   752: getfield 48	org/apache/xml/security/signature/Manifest:e	Ljava/util/List;
    //   755: astore 5
    //   757: aload 4
    //   759: aload 5
    //   761: putfield 48	org/apache/xml/security/signature/Manifest:e	Ljava/util/List;
    //   764: aload_0
    //   765: getfield 46	org/apache/xml/security/signature/Manifest:d	Ljava/util/HashMap;
    //   768: astore 5
    //   770: aload 4
    //   772: aload 5
    //   774: putfield 46	org/apache/xml/security/signature/Manifest:d	Ljava/util/HashMap;
    //   777: aload 4
    //   779: iload_1
    //   780: invokevirtual 253	org/apache/xml/security/signature/Manifest:a	(Z)Z
    //   783: istore 6
    //   785: iload 6
    //   787: ifne +55 -> 842
    //   790: getstatic 38	org/apache/xml/security/signature/Manifest:a	Lorg/apache/commons/logging/Log;
    //   793: astore 4
    //   795: ldc -1
    //   797: astore 5
    //   799: aload 4
    //   801: aload 5
    //   803: invokeinterface 258 2 0
    //   808: iconst_0
    //   809: istore 6
    //   811: aconst_null
    //   812: astore 4
    //   814: fconst_0
    //   815: fstore 9
    //   817: iload 6
    //   819: istore 10
    //   821: fload 9
    //   823: fstore 19
    //   825: iload 7
    //   827: iconst_1
    //   828: iadd
    //   829: istore 7
    //   831: iload 10
    //   833: istore 6
    //   835: fload 19
    //   837: fstore 9
    //   839: goto -586 -> 253
    //   842: getstatic 38	org/apache/xml/security/signature/Manifest:a	Lorg/apache/commons/logging/Log;
    //   845: astore 4
    //   847: ldc_w 260
    //   850: astore 5
    //   852: aload 4
    //   854: aload 5
    //   856: invokeinterface 146 2 0
    //   861: iload 13
    //   863: istore 6
    //   865: fload 14
    //   867: fstore 9
    //   869: goto -52 -> 817
    //   872: astore 4
    //   874: new 240	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   877: astore 5
    //   879: ldc -100
    //   881: astore 8
    //   883: aload 5
    //   885: aload 8
    //   887: aload 4
    //   889: invokespecial 243	org/apache/xml/security/signature/ReferenceNotInitializedException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   892: aload 5
    //   894: athrow
    //   895: astore 4
    //   897: new 240	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   900: astore 5
    //   902: ldc -100
    //   904: astore 8
    //   906: aload 5
    //   908: aload 8
    //   910: aload 4
    //   912: invokespecial 243	org/apache/xml/security/signature/ReferenceNotInitializedException:<init>	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   915: aload 5
    //   917: athrow
    //   918: iload 6
    //   920: ireturn
    //   921: astore 4
    //   923: goto -424 -> 499
    //   926: iconst_0
    //   927: istore 6
    //   929: aconst_null
    //   930: astore 4
    //   932: fconst_0
    //   933: fstore 9
    //   935: goto -275 -> 660
    //   938: iload 13
    //   940: istore 10
    //   942: fload 14
    //   944: fstore 19
    //   946: goto -121 -> 825
    //   949: iload 6
    //   951: istore 13
    //   953: fload 9
    //   955: fstore 14
    //   957: goto -609 -> 348
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	960	0	this	Manifest
    //   0	960	1	paramBoolean	boolean
    //   1	711	2	i	int
    //   4	247	3	f1	float
    //   9	675	4	localObject1	Object
    //   686	16	4	localIOException	java.io.IOException
    //   709	69	4	localReferenceNotInitializedException	ReferenceNotInitializedException
    //   793	60	4	localLog	Log
    //   872	16	4	localParserConfigurationException	javax.xml.parsers.ParserConfigurationException
    //   895	16	4	localSAXException	org.xml.sax.SAXException
    //   921	1	4	localXMLSecurityException	org.apache.xml.security.exceptions.XMLSecurityException
    //   930	1	4	localObject2	Object
    //   29	887	5	localObject3	Object
    //   58	3	6	bool1	boolean
    //   197	52	6	j	int
    //   360	590	6	bool2	boolean
    //   94	736	7	k	int
    //   144	765	8	localObject4	Object
    //   251	703	9	f2	float
    //   262	7	10	m	int
    //   324	11	10	bool3	boolean
    //   536	6	10	n	int
    //   579	362	10	bool4	boolean
    //   274	470	11	localReference	Reference
    //   289	55	12	str1	String
    //   340	612	13	bool5	boolean
    //   346	610	14	f3	float
    //   384	257	15	localObject5	Object
    //   497	17	16	localIterator	java.util.Iterator
    //   546	56	17	localObject6	Object
    //   570	82	18	str2	String
    //   823	122	19	f4	float
    // Exception table:
    //   from	to	target	type
    //   477	481	686	java/io/IOException
    //   483	488	686	java/io/IOException
    //   490	497	686	java/io/IOException
    //   499	506	686	java/io/IOException
    //   513	520	686	java/io/IOException
    //   522	527	686	java/io/IOException
    //   529	536	686	java/io/IOException
    //   548	553	686	java/io/IOException
    //   559	566	686	java/io/IOException
    //   574	579	686	java/io/IOException
    //   590	595	686	java/io/IOException
    //   601	608	686	java/io/IOException
    //   616	621	686	java/io/IOException
    //   628	631	686	java/io/IOException
    //   633	638	686	java/io/IOException
    //   640	645	686	java/io/IOException
    //   651	656	686	java/io/IOException
    //   665	668	686	java/io/IOException
    //   678	683	686	java/io/IOException
    //   683	686	686	java/io/IOException
    //   751	755	686	java/io/IOException
    //   759	764	686	java/io/IOException
    //   764	768	686	java/io/IOException
    //   772	777	686	java/io/IOException
    //   779	783	686	java/io/IOException
    //   790	793	686	java/io/IOException
    //   801	808	686	java/io/IOException
    //   842	845	686	java/io/IOException
    //   854	861	686	java/io/IOException
    //   319	324	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   329	334	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   348	351	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   353	360	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   367	370	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   372	375	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   377	382	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   388	393	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   395	400	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   404	409	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   411	416	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   420	427	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   436	441	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   448	451	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   459	466	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   477	481	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   483	488	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   490	497	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   499	506	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   513	520	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   522	527	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   529	536	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   548	553	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   559	566	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   574	579	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   590	595	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   601	608	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   616	621	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   628	631	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   633	638	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   640	645	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   651	656	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   665	668	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   678	683	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   683	686	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   688	691	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   701	706	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   706	709	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   751	755	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   759	764	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   764	768	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   772	777	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   779	783	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   790	793	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   801	808	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   842	845	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   854	861	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   874	877	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   887	892	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   892	895	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   897	900	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   910	915	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   915	918	709	org/apache/xml/security/signature/ReferenceNotInitializedException
    //   477	481	872	javax/xml/parsers/ParserConfigurationException
    //   483	488	872	javax/xml/parsers/ParserConfigurationException
    //   490	497	872	javax/xml/parsers/ParserConfigurationException
    //   499	506	872	javax/xml/parsers/ParserConfigurationException
    //   513	520	872	javax/xml/parsers/ParserConfigurationException
    //   522	527	872	javax/xml/parsers/ParserConfigurationException
    //   529	536	872	javax/xml/parsers/ParserConfigurationException
    //   548	553	872	javax/xml/parsers/ParserConfigurationException
    //   559	566	872	javax/xml/parsers/ParserConfigurationException
    //   574	579	872	javax/xml/parsers/ParserConfigurationException
    //   590	595	872	javax/xml/parsers/ParserConfigurationException
    //   601	608	872	javax/xml/parsers/ParserConfigurationException
    //   616	621	872	javax/xml/parsers/ParserConfigurationException
    //   628	631	872	javax/xml/parsers/ParserConfigurationException
    //   633	638	872	javax/xml/parsers/ParserConfigurationException
    //   640	645	872	javax/xml/parsers/ParserConfigurationException
    //   651	656	872	javax/xml/parsers/ParserConfigurationException
    //   665	668	872	javax/xml/parsers/ParserConfigurationException
    //   678	683	872	javax/xml/parsers/ParserConfigurationException
    //   683	686	872	javax/xml/parsers/ParserConfigurationException
    //   751	755	872	javax/xml/parsers/ParserConfigurationException
    //   759	764	872	javax/xml/parsers/ParserConfigurationException
    //   764	768	872	javax/xml/parsers/ParserConfigurationException
    //   772	777	872	javax/xml/parsers/ParserConfigurationException
    //   779	783	872	javax/xml/parsers/ParserConfigurationException
    //   790	793	872	javax/xml/parsers/ParserConfigurationException
    //   801	808	872	javax/xml/parsers/ParserConfigurationException
    //   842	845	872	javax/xml/parsers/ParserConfigurationException
    //   854	861	872	javax/xml/parsers/ParserConfigurationException
    //   477	481	895	org/xml/sax/SAXException
    //   483	488	895	org/xml/sax/SAXException
    //   490	497	895	org/xml/sax/SAXException
    //   499	506	895	org/xml/sax/SAXException
    //   513	520	895	org/xml/sax/SAXException
    //   522	527	895	org/xml/sax/SAXException
    //   529	536	895	org/xml/sax/SAXException
    //   548	553	895	org/xml/sax/SAXException
    //   559	566	895	org/xml/sax/SAXException
    //   574	579	895	org/xml/sax/SAXException
    //   590	595	895	org/xml/sax/SAXException
    //   601	608	895	org/xml/sax/SAXException
    //   616	621	895	org/xml/sax/SAXException
    //   628	631	895	org/xml/sax/SAXException
    //   633	638	895	org/xml/sax/SAXException
    //   640	645	895	org/xml/sax/SAXException
    //   651	656	895	org/xml/sax/SAXException
    //   665	668	895	org/xml/sax/SAXException
    //   678	683	895	org/xml/sax/SAXException
    //   683	686	895	org/xml/sax/SAXException
    //   751	755	895	org/xml/sax/SAXException
    //   759	764	895	org/xml/sax/SAXException
    //   764	768	895	org/xml/sax/SAXException
    //   772	777	895	org/xml/sax/SAXException
    //   779	783	895	org/xml/sax/SAXException
    //   790	793	895	org/xml/sax/SAXException
    //   801	808	895	org/xml/sax/SAXException
    //   842	845	895	org/xml/sax/SAXException
    //   854	861	895	org/xml/sax/SAXException
    //   628	631	921	org/apache/xml/security/exceptions/XMLSecurityException
    //   633	638	921	org/apache/xml/security/exceptions/XMLSecurityException
    //   640	645	921	org/apache/xml/security/exceptions/XMLSecurityException
    //   651	656	921	org/apache/xml/security/exceptions/XMLSecurityException
  }
  
  public String e()
  {
    return "Manifest";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/signature/Manifest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */