package org.apache.xml.security.signature;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.xml.security.algorithms.SignatureAlgorithm;
import org.apache.xml.security.c14n.Canonicalizer;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.apache.xml.security.transforms.params.InclusiveNamespaces;
import org.apache.xml.security.utils.XMLUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class SignedInfo
  extends Manifest
{
  private SignatureAlgorithm g = null;
  private byte[] h = null;
  private Element i;
  private Element q;
  
  public SignedInfo(Element paramElement, String paramString)
  {
    super((Element)localObject, paramString);
    localObject = XMLUtils.a(paramElement.getFirstChild());
    i = ((Element)localObject);
    localObject = XMLUtils.a(i.getNextSibling());
    q = ((Element)localObject);
    localObject = new org/apache/xml/security/algorithms/SignatureAlgorithm;
    Element localElement = q;
    String str = l();
    ((SignatureAlgorithm)localObject).<init>(localElement, str);
    g = ((SignatureAlgorithm)localObject);
  }
  
  private static Element a(Element paramElement)
  {
    Object localObject1 = XMLUtils.a(paramElement.getFirstChild());
    Object localObject2 = "Algorithm";
    localObject1 = ((Element)localObject1).getAttributeNS(null, (String)localObject2);
    Object localObject3 = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
    boolean bool1 = ((String)localObject1).equals(localObject3);
    if (!bool1)
    {
      localObject3 = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments";
      bool1 = ((String)localObject1).equals(localObject3);
      if (!bool1)
      {
        localObject3 = "http://www.w3.org/2001/10/xml-exc-c14n#";
        bool1 = ((String)localObject1).equals(localObject3);
        if (!bool1)
        {
          localObject3 = "http://www.w3.org/2001/10/xml-exc-c14n#WithComments";
          bool1 = ((String)localObject1).equals(localObject3);
          if (!bool1)
          {
            localObject3 = "http://www.w3.org/2006/12/xml-c14n11";
            bool1 = ((String)localObject1).equals(localObject3);
            if (!bool1)
            {
              localObject3 = "http://www.w3.org/2006/12/xml-c14n11#WithComments";
              bool1 = ((String)localObject1).equals(localObject3);
              if (bool1) {}
            }
          }
        }
      }
    }
    for (;;)
    {
      try
      {
        localObject1 = Canonicalizer.a((String)localObject1);
        localObject1 = ((Canonicalizer)localObject1).a(paramElement);
        localObject3 = DocumentBuilderFactory.newInstance();
        boolean bool2 = true;
        ((DocumentBuilderFactory)localObject3).setNamespaceAware(bool2);
        localObject3 = ((DocumentBuilderFactory)localObject3).newDocumentBuilder();
        localObject2 = new java/io/ByteArrayInputStream;
        ((ByteArrayInputStream)localObject2).<init>((byte[])localObject1);
        localObject1 = ((DocumentBuilder)localObject3).parse((InputStream)localObject2);
        localObject3 = paramElement.getOwnerDocument();
        localObject1 = ((Document)localObject1).getDocumentElement();
        bool2 = true;
        localObject1 = ((Document)localObject3).importNode((Node)localObject1, bool2);
        localObject3 = paramElement.getParentNode();
        ((Node)localObject3).replaceChild((Node)localObject1, paramElement);
        localObject1 = (Element)localObject1;
        return (Element)localObject1;
      }
      catch (ParserConfigurationException localParserConfigurationException)
      {
        localObject3 = new org/apache/xml/security/exceptions/XMLSecurityException;
        ((XMLSecurityException)localObject3).<init>("empty", localParserConfigurationException);
        throw ((Throwable)localObject3);
      }
      catch (IOException localIOException)
      {
        localObject3 = new org/apache/xml/security/exceptions/XMLSecurityException;
        ((XMLSecurityException)localObject3).<init>("empty", localIOException);
        throw ((Throwable)localObject3);
      }
      catch (SAXException localSAXException)
      {
        localObject3 = new org/apache/xml/security/exceptions/XMLSecurityException;
        ((XMLSecurityException)localObject3).<init>("empty", localSAXException);
        throw ((Throwable)localObject3);
      }
      Element localElement = paramElement;
    }
  }
  
  public void a(OutputStream paramOutputStream)
  {
    Object localObject1 = h;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = Canonicalizer.a(b());
      ((Canonicalizer)localObject1).a(paramOutputStream);
      localObject2 = f();
      if (localObject2 == null)
      {
        localObject2 = k;
        ((Canonicalizer)localObject1).a((Node)localObject2);
      }
    }
    for (;;)
    {
      return;
      Element localElement = k;
      ((Canonicalizer)localObject1).a(localElement, (String)localObject2);
      continue;
      try
      {
        localObject1 = h;
        paramOutputStream.write((byte[])localObject1);
      }
      catch (IOException localIOException)
      {
        localObject2 = new java/lang/RuntimeException;
        ((RuntimeException)localObject2).<init>(localIOException);
        throw ((Throwable)localObject2);
      }
    }
  }
  
  public String b()
  {
    return i.getAttributeNS(null, "Algorithm");
  }
  
  public boolean b(boolean paramBoolean)
  {
    return super.a(paramBoolean);
  }
  
  protected SignatureAlgorithm c()
  {
    return g;
  }
  
  public String e()
  {
    return "SignedInfo";
  }
  
  public String f()
  {
    String str1 = null;
    Object localObject1 = i.getAttributeNS(null, "Algorithm");
    Object localObject2 = "http://www.w3.org/2001/10/xml-exc-c14n#";
    boolean bool1 = ((String)localObject1).equals(localObject2);
    if (!bool1)
    {
      localObject2 = "http://www.w3.org/2001/10/xml-exc-c14n#WithComments";
      boolean bool2 = ((String)localObject1).equals(localObject2);
      if (bool2) {}
    }
    for (;;)
    {
      return str1;
      localObject1 = XMLUtils.a(i.getFirstChild());
      if (localObject1 != null) {
        try
        {
          localObject2 = new org/apache/xml/security/transforms/params/InclusiveNamespaces;
          String str2 = "http://www.w3.org/2001/10/xml-exc-c14n#";
          ((InclusiveNamespaces)localObject2).<init>((Element)localObject1, str2);
          str1 = ((InclusiveNamespaces)localObject2).a();
        }
        catch (XMLSecurityException localXMLSecurityException) {}
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/signature/SignedInfo.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */