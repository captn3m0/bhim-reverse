package org.apache.xml.security.signature;

import org.apache.xml.security.exceptions.XMLSecurityException;

public class XMLSignatureException
  extends XMLSecurityException
{
  public XMLSignatureException() {}
  
  public XMLSignatureException(String paramString)
  {
    super(paramString);
  }
  
  public XMLSignatureException(String paramString, Exception paramException)
  {
    super(paramString, paramException);
  }
  
  public XMLSignatureException(String paramString, Object[] paramArrayOfObject)
  {
    super(paramString, paramArrayOfObject);
  }
  
  public XMLSignatureException(String paramString, Object[] paramArrayOfObject, Exception paramException)
  {
    super(paramString, paramArrayOfObject, paramException);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/signature/XMLSignatureException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */