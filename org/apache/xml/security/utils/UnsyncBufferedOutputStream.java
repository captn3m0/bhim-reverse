package org.apache.xml.security.utils;

import java.io.OutputStream;

public class UnsyncBufferedOutputStream
  extends OutputStream
{
  private static ThreadLocal d;
  final OutputStream a;
  final byte[] b;
  int c = 0;
  
  static
  {
    UnsyncBufferedOutputStream.1 local1 = new org/apache/xml/security/utils/UnsyncBufferedOutputStream$1;
    local1.<init>();
    d = local1;
  }
  
  public UnsyncBufferedOutputStream(OutputStream paramOutputStream)
  {
    byte[] arrayOfByte = (byte[])d.get();
    b = arrayOfByte;
    a = paramOutputStream;
  }
  
  private final void a()
  {
    int i = c;
    if (i > 0)
    {
      OutputStream localOutputStream = a;
      byte[] arrayOfByte = b;
      int j = c;
      localOutputStream.write(arrayOfByte, 0, j);
    }
    c = 0;
  }
  
  public void close()
  {
    flush();
  }
  
  public void flush()
  {
    a();
    a.flush();
  }
  
  public void write(int paramInt)
  {
    int i = c;
    int j = 8192;
    if (i >= j) {
      a();
    }
    byte[] arrayOfByte = b;
    j = c;
    int k = j + 1;
    c = k;
    k = (byte)paramInt;
    arrayOfByte[j] = k;
  }
  
  public void write(byte[] paramArrayOfByte)
  {
    int i = paramArrayOfByte.length;
    write(paramArrayOfByte, 0, i);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = 8192;
    int j = c + paramInt2;
    if (j > i)
    {
      a();
      if (paramInt2 > i)
      {
        OutputStream localOutputStream = a;
        localOutputStream.write(paramArrayOfByte, paramInt1, paramInt2);
      }
    }
    for (;;)
    {
      return;
      j = paramInt2;
      byte[] arrayOfByte = b;
      int k = c;
      System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, k, paramInt2);
      c = j;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/UnsyncBufferedOutputStream.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */