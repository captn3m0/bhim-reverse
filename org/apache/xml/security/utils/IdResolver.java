package org.apache.xml.security.utils;

import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.List;
import java.util.WeakHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

public class IdResolver
{
  static Class a;
  private static Log b;
  private static WeakHashMap c;
  private static List d;
  private static int e;
  
  static
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = a("org.apache.xml.security.utils.IdResolver");
      a = (Class)localObject;
    }
    for (;;)
    {
      b = LogFactory.getLog(((Class)localObject).getName());
      localObject = new java/util/WeakHashMap;
      ((WeakHashMap)localObject).<init>();
      c = (WeakHashMap)localObject;
      localObject = new String[6];
      localObject[0] = "http://www.w3.org/2000/09/xmldsig#";
      localObject[1] = "http://www.w3.org/2001/04/xmlenc#";
      localObject[2] = "http://schemas.xmlsoap.org/soap/security/2000-12";
      localObject[3] = "http://www.w3.org/2002/03/xkms#";
      localObject[4] = "urn:oasis:names:tc:SAML:1.0:assertion";
      localObject[5] = "urn:oasis:names:tc:SAML:1.0:protocol";
      d = Arrays.asList((Object[])localObject);
      e = d.size();
      return;
      localObject = a;
    }
  }
  
  public static int a(Element paramElement, String paramString, Element[] paramArrayOfElement)
  {
    int i = paramElement.hasAttributes();
    if (i == 0)
    {
      i = 0;
      localObject1 = null;
      return i;
    }
    NamedNodeMap localNamedNodeMap = paramElement.getAttributes();
    Object localObject1 = d;
    String str1 = paramElement.getNamespaceURI();
    int j = ((List)localObject1).indexOf(str1);
    int i3;
    label60:
    int i5;
    label77:
    Object localObject2;
    int i6;
    label116:
    String str2;
    int i7;
    int i9;
    if (j < 0)
    {
      j = e;
      i3 = j;
      int i4 = localNamedNodeMap.getLength();
      j = 0;
      localObject1 = null;
      i5 = 0;
      if (i5 >= i4) {
        break label382;
      }
      localObject1 = (Attr)localNamedNodeMap.item(i5);
      localObject2 = ((Attr)localObject1).getNamespaceURI();
      if (localObject2 != null) {
        break label183;
      }
      i6 = i3;
      if (i6 < 0) {
        i6 = e;
      }
      str2 = ((Attr)localObject1).getLocalName();
      if (str2 == null) {
        str2 = ((Attr)localObject1).getName();
      }
      i7 = str2.length();
      i9 = 2;
      if (i7 <= i9) {
        break label211;
      }
    }
    for (;;)
    {
      j = i5 + 1;
      i5 = j;
      break label77;
      i3 = j;
      break label60;
      label183:
      localObject2 = d;
      str2 = ((Attr)localObject1).getNamespaceURI();
      i6 = ((List)localObject2).indexOf(str2);
      break label116;
      label211:
      localObject1 = ((Attr)localObject1).getNodeValue();
      String str3 = null;
      i7 = str2.charAt(0);
      i9 = 73;
      if (i7 == i9)
      {
        int i10 = str2.charAt(1);
        i7 = 100;
        if (i10 == i7)
        {
          boolean bool6 = ((String)localObject1).equals(paramString);
          if (bool6)
          {
            paramArrayOfElement[i6] = paramElement;
            if (i6 != 0) {
              continue;
            }
            j = 1;
            break;
          }
        }
        int i8 = 68;
        if (i10 != i8) {
          continue;
        }
        boolean bool1 = ((String)localObject1).endsWith(paramString);
        if (!bool1) {
          continue;
        }
        int k = 3;
        if (i6 != k) {
          i6 = e;
        }
        paramArrayOfElement[i6] = paramElement;
        continue;
      }
      str3 = "id";
      boolean bool7 = str3.equals(str2);
      if (bool7)
      {
        boolean bool2 = ((String)localObject1).equals(paramString);
        if (bool2)
        {
          m = 2;
          if (i6 != m) {
            i6 = e;
          }
          paramArrayOfElement[i6] = paramElement;
        }
      }
    }
    label382:
    int m = 3;
    int n;
    if (i3 == m)
    {
      localObject1 = paramElement.getAttribute("OriginalRequestID");
      boolean bool3 = ((String)localObject1).equals(paramString);
      if (!bool3)
      {
        localObject1 = paramElement.getAttribute("RequestID");
        bool3 = ((String)localObject1).equals(paramString);
        if (!bool3)
        {
          localObject1 = paramElement.getAttribute("ResponseID");
          bool3 = ((String)localObject1).equals(paramString);
          if (!bool3) {
            break label467;
          }
        }
      }
      n = 3;
      paramArrayOfElement[n] = paramElement;
    }
    for (;;)
    {
      n = 0;
      localObject1 = null;
      break;
      label467:
      n = 4;
      if (i3 == n)
      {
        localObject1 = paramElement.getAttribute("AssertionID");
        boolean bool4 = ((String)localObject1).equals(paramString);
        if (bool4)
        {
          i1 = 4;
          paramArrayOfElement[i1] = paramElement;
          continue;
        }
      }
      int i1 = 5;
      if (i3 == i1)
      {
        localObject1 = paramElement.getAttribute("RequestID");
        boolean bool5 = ((String)localObject1).equals(paramString);
        if (!bool5)
        {
          localObject1 = paramElement.getAttribute("ResponseID");
          bool5 = ((String)localObject1).equals(paramString);
          if (!bool5) {}
        }
        else
        {
          int i2 = 5;
          paramArrayOfElement[i2] = paramElement;
        }
      }
    }
  }
  
  private static int a(Node paramNode, String paramString, Element[] paramArrayOfElement)
  {
    int i = 1;
    Object localObject1 = null;
    int j = 0;
    Object localObject2 = null;
    Node localNode = paramNode;
    int k = localNode.getNodeType();
    switch (k)
    {
    default: 
      paramNode = (Node)localObject2;
      localObject2 = localObject1;
    }
    for (;;)
    {
      if ((paramNode == null) && (localObject2 != null))
      {
        paramNode = ((Node)localObject2).getNextSibling();
        localObject1 = ((Node)localObject2).getParentNode();
        if (localObject1 == null) {
          break label230;
        }
        j = ((Node)localObject1).getNodeType();
        if (i == j) {
          break label230;
        }
        j = 0;
        localObject2 = null;
        continue;
        paramNode = localNode.getFirstChild();
        localObject2 = localObject1;
        continue;
        localObject2 = localNode;
        localObject2 = (Element)localNode;
        k = a((Element)localObject2, paramString, paramArrayOfElement);
        if (k != i) {}
      }
      while (paramNode == null)
      {
        return i;
        paramNode = localNode.getFirstChild();
        if (paramNode != null) {
          break;
        }
        if (localObject1 == null) {
          break label223;
        }
        paramNode = localNode.getNextSibling();
        localObject2 = localObject1;
        break;
      }
      localNode = paramNode.getNextSibling();
      localObject1 = localObject2;
      localObject2 = localNode;
      localNode = paramNode;
      break;
      label223:
      localObject2 = localObject1;
      continue;
      label230:
      localObject2 = localObject1;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public static Element a(Document paramDocument, String paramString)
  {
    Element localElement = c(paramDocument, paramString);
    Log localLog;
    Object localObject;
    String str;
    if (localElement != null)
    {
      localLog = b;
      localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>();
      localObject = ((StringBuffer)localObject).append("I could find an Element using the simple getElementByIdType method: ");
      str = localElement.getTagName();
      localObject = str;
      localLog.debug(localObject);
    }
    for (;;)
    {
      return localElement;
      localElement = b(paramDocument, paramString);
      if (localElement != null)
      {
        localLog = b;
        localObject = new java/lang/StringBuffer;
        ((StringBuffer)localObject).<init>();
        localObject = ((StringBuffer)localObject).append("I could find an Element using the simple getElementByIdUsingDOM method: ");
        str = localElement.getTagName();
        localObject = str;
        localLog.debug(localObject);
      }
      else
      {
        localElement = a(paramDocument, paramString);
        if (localElement != null) {
          a(localElement, paramString);
        } else {
          localElement = null;
        }
      }
    }
  }
  
  private static Element a(Node paramNode, String paramString)
  {
    Element[] arrayOfElement = new Element[e + 1];
    a(paramNode, paramString, arrayOfElement);
    int i = 0;
    Element localElement1 = null;
    int j = arrayOfElement.length;
    if (i < j)
    {
      Element localElement2 = arrayOfElement[i];
      if (localElement2 == null) {}
    }
    for (localElement1 = arrayOfElement[i];; localElement1 = null)
    {
      return localElement1;
      i += 1;
      break;
      i = 0;
    }
  }
  
  public static void a(Element paramElement, String paramString)
  {
    Object localObject1 = paramElement.getOwnerDocument();
    synchronized (c)
    {
      Object localObject2 = c;
      localObject2 = ((WeakHashMap)localObject2).get(localObject1);
      localObject2 = (WeakHashMap)localObject2;
      if (localObject2 == null)
      {
        localObject2 = new java/util/WeakHashMap;
        ((WeakHashMap)localObject2).<init>();
        WeakHashMap localWeakHashMap2 = c;
        localWeakHashMap2.put(localObject1, localObject2);
      }
      localObject1 = new java/lang/ref/WeakReference;
      ((WeakReference)localObject1).<init>(paramElement);
      ((WeakHashMap)localObject2).put(paramString, localObject1);
      return;
    }
  }
  
  private static Element b(Document paramDocument, String paramString)
  {
    Log localLog = b;
    boolean bool = localLog.isDebugEnabled();
    if (bool)
    {
      localLog = b;
      Object localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>();
      String str = "getElementByIdUsingDOM() Search for ID ";
      localObject = str + paramString;
      localLog.debug(localObject);
    }
    return paramDocument.getElementById(paramString);
  }
  
  private static Element c(Document paramDocument, String paramString)
  {
    Object localObject1 = b;
    boolean bool = ((Log)localObject1).isDebugEnabled();
    if (bool)
    {
      localObject1 = b;
      ??? = new java/lang/StringBuffer;
      ((StringBuffer)???).<init>();
      String str = "getElementByIdType() Search for ID ";
      ??? = str + paramString;
      ((Log)localObject1).debug(???);
    }
    for (;;)
    {
      synchronized (c)
      {
        localObject1 = c;
        localObject1 = ((WeakHashMap)localObject1).get(paramDocument);
        localObject1 = (WeakHashMap)localObject1;
        if (localObject1 != null)
        {
          localObject1 = (WeakReference)((WeakHashMap)localObject1).get(paramString);
          if (localObject1 != null)
          {
            localObject1 = (Element)((WeakReference)localObject1).get();
            return (Element)localObject1;
          }
        }
      }
      bool = false;
      Object localObject3 = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/IdResolver.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */