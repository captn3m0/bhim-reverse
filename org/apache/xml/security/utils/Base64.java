package org.apache.xml.security.utils;

import java.io.InputStream;
import java.io.OutputStream;
import org.apache.xml.security.exceptions.Base64DecodingException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public class Base64
{
  private static final byte[] a;
  private static final char[] b;
  
  static
  {
    int i = 63;
    int j = 62;
    int k = 47;
    int m = 43;
    int n = 0;
    a = new byte['ÿ'];
    b = new char[64];
    int i1 = 0;
    Object localObject;
    int i3;
    for (;;)
    {
      i2 = 255;
      if (i1 >= i2) {
        break;
      }
      localObject = a;
      i3 = -1;
      localObject[i1] = i3;
      i1 += 1;
    }
    i1 = 90;
    for (;;)
    {
      i2 = 65;
      if (i1 < i2) {
        break;
      }
      localObject = a;
      i3 = (byte)(i1 + -65);
      localObject[i1] = i3;
      i1 += -1;
    }
    i1 = 122;
    for (;;)
    {
      i2 = 97;
      if (i1 < i2) {
        break;
      }
      localObject = a;
      i3 = (byte)(i1 + -97 + 26);
      localObject[i1] = i3;
      i1 += -1;
    }
    i1 = 57;
    for (;;)
    {
      i2 = 48;
      if (i1 < i2) {
        break;
      }
      localObject = a;
      i3 = (byte)(i1 + -48 + 52);
      localObject[i1] = i3;
      i1 += -1;
    }
    a[m] = j;
    a[k] = i;
    i1 = 0;
    for (;;)
    {
      i2 = 25;
      if (i1 > i2) {
        break;
      }
      localObject = b;
      i3 = (char)(i1 + 65);
      localObject[i1] = i3;
      i1 += 1;
    }
    int i2 = 26;
    i1 = 0;
    for (;;)
    {
      i3 = 51;
      if (i2 > i3) {
        break;
      }
      char[] arrayOfChar = b;
      int i4 = (char)(i1 + 97);
      arrayOfChar[i2] = i4;
      i2 += 1;
      i1 += 1;
    }
    i1 = 52;
    for (;;)
    {
      i2 = 61;
      if (i1 > i2) {
        break;
      }
      localObject = b;
      i3 = (char)(n + 48);
      localObject[i1] = i3;
      i1 += 1;
      n += 1;
    }
    b[j] = m;
    b[i] = k;
  }
  
  protected static final int a(String paramString, byte[] paramArrayOfByte)
  {
    boolean bool = false;
    int j = paramString.length();
    int k = 0;
    int m = 0;
    int i;
    if (k < j)
    {
      byte b1 = (byte)paramString.charAt(k);
      bool = a(b1);
      if (bool) {
        break label64;
      }
      i = m + 1;
      paramArrayOfByte[m] = b1;
    }
    for (;;)
    {
      k += 1;
      m = i;
      break;
      return m;
      label64:
      i = m;
    }
  }
  
  public static final String a(byte[] paramArrayOfByte, int paramInt)
  {
    int i = 4;
    int j = paramInt;
    if (paramInt < i) {
      paramInt = -1 >>> 1;
    }
    if (paramArrayOfByte == null) {
      i = 0;
    }
    for (Object localObject = null;; localObject = "")
    {
      return (String)localObject;
      i = paramArrayOfByte.length * 8;
      if (i != 0) {
        break;
      }
    }
    int k = i % 24;
    int m = i / 24;
    int n;
    int i1;
    char[] arrayOfChar1;
    int i2;
    char[] arrayOfChar2;
    char[] arrayOfChar3;
    int i3;
    char[] arrayOfChar4;
    int i4;
    if (k != 0)
    {
      i = m + 1;
      n = paramInt / 4;
      i1 = (i + -1) / n;
      arrayOfChar1 = new char[i * 4 + i1];
      i2 = 0;
      arrayOfChar2 = null;
      n = 0;
      arrayOfChar3 = null;
      i3 = 0;
      arrayOfChar4 = null;
      i4 = 0;
      i = 0;
      localObject = null;
    }
    int i5;
    int i6;
    label130:
    int i7;
    int i8;
    for (;;)
    {
      if (i4 >= i1) {
        break label1119;
      }
      i5 = 0;
      i3 = i;
      i6 = i2;
      i = 19;
      if (i5 < i)
      {
        i = n + 1;
        n = paramArrayOfByte[n];
        i2 = i + 1;
        i7 = paramArrayOfByte[i];
        i8 = i2 + 1;
        int i9 = paramArrayOfByte[i2];
        int i10 = (byte)(i7 & 0xF);
        j = (byte)(n & 0x3);
        int i11 = j;
        i = n & 0xFFFFFF80;
        if (i == 0)
        {
          i = (byte)(n >> 2);
          i2 = i;
          label208:
          i = i7 & 0xFFFFFF80;
          if (i != 0) {
            break label421;
          }
          i = (byte)(i7 >> 4);
          n = i;
          label227:
          i = i9 & 0xFFFFFF80;
          if (i != 0) {
            break label437;
          }
        }
        label421:
        label437:
        for (i = (byte)(i9 >> 6);; i = (byte)(i9 >> 6 ^ 0xFC))
        {
          i7 = i6 + 1;
          char[] arrayOfChar5 = b;
          i2 = arrayOfChar5[i2];
          arrayOfChar1[i6] = i2;
          i2 = i7 + 1;
          char[] arrayOfChar6 = b;
          i11 <<= 4;
          n |= i11;
          n = arrayOfChar6[n];
          arrayOfChar1[i7] = n;
          n = i2 + 1;
          arrayOfChar6 = b;
          i7 = i10 << 2;
          i |= i7;
          i = arrayOfChar6[i];
          arrayOfChar1[i2] = i;
          i2 = n + 1;
          localObject = b;
          i6 = i9 & 0x3F;
          i = localObject[i6];
          arrayOfChar1[n] = i;
          n = i3 + 1;
          i = i5 + 1;
          i5 = i;
          i3 = n;
          i6 = i2;
          n = i8;
          break label130;
          i = m;
          break;
          i = (byte)(n >> 2 ^ 0xC0);
          i2 = i;
          break label208;
          i = (byte)(i7 >> 4 ^ 0xF0);
          n = i;
          break label227;
        }
      }
      i2 = i6 + 1;
      arrayOfChar1[i6] = '\n';
      i4 += 1;
      i = i3;
    }
    for (;;)
    {
      if (i5 < m)
      {
        n = i + 1;
        i = paramArrayOfByte[i];
        i2 = n + 1;
        n = paramArrayOfByte[n];
        i3 = i2 + 1;
        i6 = paramArrayOfByte[i2];
        i4 = (byte)(n & 0xF);
        i1 = (byte)(i & 0x3);
        i2 = i & 0xFFFFFF80;
        if (i2 == 0)
        {
          i = (byte)(i >> 2);
          i2 = i;
          label549:
          i = n & 0xFFFFFF80;
          if (i != 0) {
            break label742;
          }
          i = (byte)(n >> 4);
          n = i;
          label568:
          i = i6 & 0xFFFFFF80;
          if (i != 0) {
            break label758;
          }
        }
        label742:
        label758:
        for (i = (byte)(i6 >> 6);; i = (byte)(i6 >> 6 ^ 0xFC))
        {
          i7 = i8 + 1;
          char[] arrayOfChar7 = b;
          i2 = arrayOfChar7[i2];
          arrayOfChar1[i8] = i2;
          i2 = i7 + 1;
          char[] arrayOfChar8 = b;
          i1 <<= 4;
          n |= i1;
          n = arrayOfChar8[n];
          arrayOfChar1[i7] = n;
          i8 = i2 + 1;
          arrayOfChar3 = b;
          i4 <<= 2;
          i |= i4;
          i = arrayOfChar3[i];
          arrayOfChar1[i2] = i;
          n = i8 + 1;
          localObject = b;
          i2 = i6 & 0x3F;
          i = localObject[i2];
          arrayOfChar1[i8] = i;
          i5 += 1;
          i8 = n;
          i = i3;
          break;
          i = (byte)(i >> 2 ^ 0xC0);
          i2 = i;
          break label549;
          i = (byte)(n >> 4 ^ 0xF0);
          n = i;
          break label568;
        }
      }
      m = 8;
      if (k == m)
      {
        i = paramArrayOfByte[i];
        m = (byte)(i & 0x3);
        n = i & 0xFFFFFF80;
        if (n == 0)
        {
          i = (byte)(i >> 2);
          label809:
          n = i8 + 1;
          i = b[i];
          arrayOfChar1[i8] = i;
          i = n + 1;
          arrayOfChar2 = b;
          m <<= 4;
          m = arrayOfChar2[m];
          arrayOfChar1[n] = m;
          m = i + 1;
          n = 61;
          arrayOfChar1[i] = n;
          i = m + 1;
          i = 61;
          arrayOfChar1[m] = i;
        }
      }
      do
      {
        localObject = new java/lang/String;
        ((String)localObject).<init>(arrayOfChar1);
        break;
        i = (byte)(i >> 2 ^ 0xC0);
        break label809;
        m = 16;
      } while (k != m);
      m = paramArrayOfByte[i];
      i += 1;
      n = paramArrayOfByte[i];
      i2 = (byte)(n & 0xF);
      i5 = (byte)(m & 0x3);
      i = m & 0xFFFFFF80;
      if (i == 0)
      {
        i = (byte)(m >> 2);
        m = i;
        label972:
        i = n & 0xFFFFFF80;
        if (i != 0) {
          break label1106;
        }
      }
      label1106:
      for (i = (byte)(n >> 4);; i = (byte)(n >> 4 ^ 0xF0))
      {
        n = i8 + 1;
        m = b[m];
        arrayOfChar1[i8] = m;
        m = n + 1;
        arrayOfChar4 = b;
        i5 <<= 4;
        i |= i5;
        i = arrayOfChar4[i];
        arrayOfChar1[n] = i;
        i = m + 1;
        arrayOfChar3 = b;
        i2 <<= 2;
        n = arrayOfChar3[i2];
        arrayOfChar1[m] = n;
        m = i + 1;
        m = 61;
        arrayOfChar1[i] = m;
        break;
        i = (byte)(m >> 2 ^ 0xC0);
        m = i;
        break label972;
      }
      label1119:
      i5 = i;
      i8 = i2;
      i = n;
    }
  }
  
  public static final void a(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    int i = 2;
    int j = 1;
    int k = 3;
    int m = -1;
    int n = 0;
    byte[] arrayOfByte1 = new byte[4];
    int i1 = 0;
    Object localObject = null;
    byte b2;
    int i4;
    label234:
    int i10;
    for (;;)
    {
      int i2 = paramInputStream.read();
      if (i2 > 0)
      {
        b1 = (byte)i2;
        boolean bool1 = a(b1);
        if (bool1) {
          continue;
        }
        bool1 = b(b1);
        if (!bool1) {
          break label234;
        }
        i3 = i1 + 1;
        arrayOfByte1[i1] = b1;
        if (i3 == k)
        {
          i1 = i3 + 1;
          i1 = (byte)paramInputStream.read();
          arrayOfByte1[i3] = i1;
        }
      }
      i1 = arrayOfByte1[0];
      n = arrayOfByte1[j];
      int i3 = arrayOfByte1[i];
      b2 = arrayOfByte1[k];
      i1 = a[i1];
      n = a[n];
      byte[] arrayOfByte2 = a;
      byte b1 = arrayOfByte2[i3];
      byte[] arrayOfByte3 = a;
      int i9 = arrayOfByte3[b2];
      if ((b1 != m) && (i9 != m)) {
        break label608;
      }
      int i6 = b(i3);
      if (i6 == 0) {
        break label470;
      }
      i6 = b(b2);
      if (i6 == 0) {
        break label470;
      }
      i4 = n & 0xF;
      if (i4 == 0) {
        break;
      }
      localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
      ((Base64DecodingException)localObject).<init>("decoding.general");
      throw ((Throwable)localObject);
      i4 = i1 + 1;
      arrayOfByte1[i1] = i6;
      if (i6 == m)
      {
        localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
        ((Base64DecodingException)localObject).<init>("decoding.general");
        throw ((Throwable)localObject);
      }
      i1 = 4;
      if (i4 != i1)
      {
        i1 = i4;
      }
      else
      {
        localObject = a;
        i4 = arrayOfByte1[0];
        i1 = localObject[i4];
        byte[] arrayOfByte4 = a;
        int i7 = arrayOfByte1[j];
        i4 = arrayOfByte4[i7];
        arrayOfByte2 = a;
        i10 = arrayOfByte1[i];
        i7 = arrayOfByte2[i10];
        arrayOfByte3 = a;
        int i11 = arrayOfByte1[k];
        i10 = arrayOfByte3[i11];
        i1 <<= 2;
        i11 = i4 >> 4;
        i1 = (byte)(i1 | i11);
        paramOutputStream.write(i1);
        i1 = (i4 & 0xF) << 4;
        i4 = i7 >> 2 & 0xF;
        i1 = (byte)(i1 | i4);
        paramOutputStream.write(i1);
        i1 = (byte)(i7 << 6 | i10);
        paramOutputStream.write(i1);
        i1 = 0;
        localObject = null;
      }
    }
    i1 <<= 2;
    n >>= 4;
    i1 = (byte)(i1 | n);
    paramOutputStream.write(i1);
    for (;;)
    {
      return;
      label470:
      boolean bool2 = b(i4);
      if (!bool2)
      {
        boolean bool3 = b(b2);
        if (bool3)
        {
          arrayOfByte1 = a;
          i5 = arrayOfByte1[i4];
          int i8 = i5 & 0x3;
          if (i8 != 0)
          {
            localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
            ((Base64DecodingException)localObject).<init>("decoding.general");
            throw ((Throwable)localObject);
          }
          i1 <<= 2;
          i8 = n >> 4;
          i1 = (byte)(i1 | i8);
          paramOutputStream.write(i1);
          i1 = (n & 0xF) << 4;
          n = i5 >> 2 & 0xF;
          i1 = (byte)(i1 | n);
          paramOutputStream.write(i1);
          continue;
        }
      }
      localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
      ((Base64DecodingException)localObject).<init>("decoding.general");
      throw ((Throwable)localObject);
      label608:
      i1 <<= 2;
      int i5 = n >> 4;
      i1 = (byte)(i1 | i5);
      paramOutputStream.write(i1);
      i1 = (n & 0xF) << 4;
      n = bool2 >> true & 0xF;
      i1 = (byte)(i1 | n);
      paramOutputStream.write(i1);
      i1 = (byte)(bool2 << true | i10);
      paramOutputStream.write(i1);
    }
  }
  
  public static final void a(String paramString, OutputStream paramOutputStream)
  {
    byte[] arrayOfByte = new byte[paramString.length()];
    int i = a(paramString, arrayOfByte);
    a(arrayOfByte, paramOutputStream, i);
  }
  
  public static final void a(byte[] paramArrayOfByte, OutputStream paramOutputStream)
  {
    a(paramArrayOfByte, paramOutputStream, -1);
  }
  
  protected static final void a(byte[] paramArrayOfByte, OutputStream paramOutputStream, int paramInt)
  {
    int i = -1;
    if (paramInt == i) {
      paramInt = c(paramArrayOfByte);
    }
    int j = paramInt % 4;
    Object localObject;
    if (j != 0)
    {
      localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
      ((Base64DecodingException)localObject).<init>("decoding.divisible.four");
      throw ((Throwable)localObject);
    }
    int k = paramInt / 4;
    if (k == 0) {}
    for (;;)
    {
      return;
      j = 0;
      localObject = null;
      k += -1;
      while (k > 0)
      {
        arrayOfByte1 = a;
        m = j + 1;
        j = paramArrayOfByte[j];
        i1 = arrayOfByte1[j];
        localObject = a;
        i2 = m + 1;
        m = paramArrayOfByte[m];
        m = localObject[m];
        localObject = a;
        i3 = i2 + 1;
        i2 = paramArrayOfByte[i2];
        i2 = localObject[i2];
        byte[] arrayOfByte2 = a;
        j = i3 + 1;
        i3 = paramArrayOfByte[i3];
        i3 = arrayOfByte2[i3];
        if ((i1 == i) || (m == i) || (i2 == i) || (i3 == i))
        {
          localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
          ((Base64DecodingException)localObject).<init>("decoding.general");
          throw ((Throwable)localObject);
        }
        i1 <<= 2;
        i5 = m >> 4;
        i1 = (byte)(i1 | i5);
        paramOutputStream.write(i1);
        i1 = (m & 0xF) << 4;
        m = i2 >> 2 & 0xF;
        i1 = (byte)(i1 | m);
        paramOutputStream.write(i1);
        i1 = (byte)(i2 << 6 | i3);
        paramOutputStream.write(i1);
        k += -1;
      }
      byte[] arrayOfByte3 = a;
      int i1 = j + 1;
      j = paramArrayOfByte[j];
      j = arrayOfByte3[j];
      arrayOfByte3 = a;
      int m = i1 + 1;
      i1 = paramArrayOfByte[i1];
      k = arrayOfByte3[i1];
      if ((j == i) || (k == i))
      {
        localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
        ((Base64DecodingException)localObject).<init>("decoding.general");
        throw ((Throwable)localObject);
      }
      byte[] arrayOfByte1 = a;
      int i2 = m + 1;
      m = paramArrayOfByte[m];
      i1 = arrayOfByte1[m];
      byte[] arrayOfByte4 = a;
      int i5 = i2 + 1;
      i2 = paramArrayOfByte[i2];
      int i3 = arrayOfByte4[i2];
      int i4;
      int n;
      if ((i1 == i) || (i3 == i))
      {
        i4 = b(m);
        if (i4 != 0)
        {
          i4 = b(i2);
          if (i4 != 0)
          {
            i1 = k & 0xF;
            if (i1 != 0)
            {
              localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
              ((Base64DecodingException)localObject).<init>("decoding.general");
              throw ((Throwable)localObject);
            }
            j <<= 2;
            k >>= 4;
            j = (byte)(j | k);
            paramOutputStream.write(j);
            continue;
          }
        }
        boolean bool = b(m);
        if (!bool)
        {
          bool = b(i2);
          if (bool)
          {
            n = i1 & 0x3;
            if (n != 0)
            {
              localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
              ((Base64DecodingException)localObject).<init>("decoding.general");
              throw ((Throwable)localObject);
            }
            j <<= 2;
            n = k >> 4;
            j = (byte)(j | n);
            paramOutputStream.write(j);
            j = (k & 0xF) << 4;
            k = i1 >> 2 & 0xF;
            j = (byte)(j | k);
            paramOutputStream.write(j);
            continue;
          }
        }
        localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
        ((Base64DecodingException)localObject).<init>("decoding.general");
        throw ((Throwable)localObject);
      }
      else
      {
        j <<= 2;
        n = k >> 4;
        j = (byte)(j | n);
        paramOutputStream.write(j);
        j = (k & 0xF) << 4;
        k = i1 >> 2 & 0xF;
        j = (byte)(j | k);
        paramOutputStream.write(j);
        j = (byte)(i1 << 6 | i4);
        paramOutputStream.write(j);
      }
    }
  }
  
  protected static final boolean a(byte paramByte)
  {
    byte b1 = 32;
    if (paramByte != b1)
    {
      b1 = 13;
      if (paramByte != b1)
      {
        b1 = 10;
        if (paramByte != b1)
        {
          b1 = 9;
          if (paramByte != b1) {
            break label36;
          }
        }
      }
    }
    label36:
    for (b1 = 1;; b1 = 0) {
      return b1;
    }
  }
  
  public static final byte[] a(String paramString)
  {
    int i;
    if (paramString == null) {
      i = 0;
    }
    int j;
    for (byte[] arrayOfByte = null;; arrayOfByte = b(arrayOfByte, j))
    {
      return arrayOfByte;
      i = paramString.length();
      arrayOfByte = new byte[i];
      j = a(paramString, arrayOfByte);
    }
  }
  
  public static final byte[] a(Element paramElement)
  {
    Node localNode = paramElement.getFirstChild();
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    localStringBuffer.<init>();
    while (localNode != null)
    {
      int i = localNode.getNodeType();
      int j = 3;
      if (i == j)
      {
        Object localObject = localNode;
        localObject = ((Text)localNode).getData();
        localStringBuffer.append((String)localObject);
      }
      localNode = localNode.getNextSibling();
    }
    return a(localStringBuffer.toString());
  }
  
  public static final byte[] a(byte[] paramArrayOfByte)
  {
    return b(paramArrayOfByte, -1);
  }
  
  public static final String b(byte[] paramArrayOfByte)
  {
    boolean bool = XMLUtils.a();
    int i;
    if (bool) {
      i = -1 >>> 1;
    }
    for (String str = a(paramArrayOfByte, i);; str = a(paramArrayOfByte, i))
    {
      return str;
      i = 76;
    }
  }
  
  protected static final boolean b(byte paramByte)
  {
    byte b1 = 61;
    if (paramByte == b1) {}
    for (b1 = 1;; b1 = 0) {
      return b1;
    }
  }
  
  protected static final byte[] b(byte[] paramArrayOfByte, int paramInt)
  {
    int i = 0;
    byte[] arrayOfByte1 = null;
    int j = -1;
    if (paramInt == j) {
      paramInt = c(paramArrayOfByte);
    }
    int k = paramInt % 4;
    Object localObject;
    if (k != 0)
    {
      localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
      ((Base64DecodingException)localObject).<init>("decoding.divisible.four");
      throw ((Throwable)localObject);
    }
    int i1 = paramInt / 4;
    if (i1 == 0) {
      localObject = new byte[0];
    }
    label810:
    for (;;)
    {
      return (byte[])localObject;
      k = (i1 + -1) * 4;
      int i2 = (i1 + -1) * 3;
      byte[] arrayOfByte2 = a;
      int i3 = k + 1;
      k = paramArrayOfByte[k];
      int i4 = arrayOfByte2[k];
      localObject = a;
      int i5 = i3 + 1;
      i3 = paramArrayOfByte[i3];
      i3 = localObject[i3];
      if ((i4 == j) || (i3 == j))
      {
        localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
        ((Base64DecodingException)localObject).<init>("decoding.general");
        throw ((Throwable)localObject);
      }
      localObject = a;
      int i7 = i5 + 1;
      i5 = paramArrayOfByte[i5];
      int i9 = localObject[i5];
      localObject = a;
      int i10 = i7 + 1;
      i7 = paramArrayOfByte[i7];
      i10 = localObject[i7];
      if ((i9 == j) || (i10 == j))
      {
        boolean bool1 = b(i5);
        if (bool1)
        {
          bool1 = b(i7);
          if (bool1)
          {
            int m = i3 & 0xF;
            if (m != 0)
            {
              localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
              ((Base64DecodingException)localObject).<init>("decoding.general");
              throw ((Throwable)localObject);
            }
            m = i2 + 1;
            localObject = new byte[m];
            i4 <<= 2;
            i3 >>= 4;
            i4 = (byte)(i4 | i3);
            localObject[i2] = i4;
            i2 = i1 + -1;
            i1 = 0;
          }
        }
      }
      for (;;)
      {
        if (i2 <= 0) {
          break label810;
        }
        arrayOfByte2 = a;
        i3 = i + 1;
        i = paramArrayOfByte[i];
        i4 = arrayOfByte2[i];
        arrayOfByte1 = a;
        int i6 = i3 + 1;
        i3 = paramArrayOfByte[i3];
        i3 = arrayOfByte1[i3];
        arrayOfByte1 = a;
        int i8 = i6 + 1;
        i6 = paramArrayOfByte[i6];
        i6 = arrayOfByte1[i6];
        byte[] arrayOfByte3 = a;
        i = i8 + 1;
        i8 = paramArrayOfByte[i8];
        i8 = arrayOfByte3[i8];
        if ((i4 == j) || (i3 == j) || (i6 == j) || (i8 == j))
        {
          localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
          ((Base64DecodingException)localObject).<init>("decoding.general");
          throw ((Throwable)localObject);
          boolean bool2 = b(i6);
          if (!bool2)
          {
            bool2 = b(i8);
            if (bool2)
            {
              n = i9 & 0x3;
              if (n != 0)
              {
                localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
                ((Base64DecodingException)localObject).<init>("decoding.general");
                throw ((Throwable)localObject);
              }
              n = i2 + 2;
              localObject = new byte[n];
              i6 = i2 + 1;
              i4 <<= 2;
              i8 = i3 >> 4;
              i4 = (byte)(i4 | i8);
              localObject[i2] = i4;
              i2 = (i3 & 0xF) << 4;
              i4 = i9 >> 2 & 0xF;
              i2 = (byte)(i2 | i4);
              localObject[i6] = i2;
              break;
            }
          }
          localObject = new org/apache/xml/security/exceptions/Base64DecodingException;
          ((Base64DecodingException)localObject).<init>("decoding.general");
          throw ((Throwable)localObject);
          int n = i2 + 3;
          localObject = new byte[n];
          i6 = i2 + 1;
          i4 <<= 2;
          i8 = i3 >> 4;
          i4 = (byte)(i4 | i8);
          localObject[i2] = i4;
          i2 = i6 + 1;
          i4 = (i3 & 0xF) << 4;
          i3 = i9 >> 2 & 0xF;
          i4 = (byte)(i4 | i3);
          localObject[i6] = i4;
          i4 = i2 + 1;
          i4 = (byte)(i9 << 6 | i10);
          localObject[i2] = i4;
          break;
        }
        i9 = i1 + 1;
        i4 <<= 2;
        i10 = i3 >> 4;
        i4 = (byte)(i4 | i10);
        localObject[i1] = i4;
        i4 = i9 + 1;
        i1 = (i3 & 0xF) << 4;
        i3 = i6 >> 2 & 0xF;
        i1 = (byte)(i1 | i3);
        localObject[i9] = i1;
        i1 = i4 + 1;
        i3 = (byte)(i6 << 6 | i8);
        localObject[i4] = i3;
        i2 += -1;
      }
    }
  }
  
  protected static final int c(byte[] paramArrayOfByte)
  {
    int i = 0;
    if (paramArrayOfByte == null) {
      return i;
    }
    int j = paramArrayOfByte.length;
    int k = 0;
    label13:
    int m;
    if (k < j)
    {
      byte b1 = paramArrayOfByte[k];
      boolean bool = a(b1);
      if (bool) {
        break label55;
      }
      m = i + 1;
      paramArrayOfByte[i] = b1;
    }
    for (;;)
    {
      k += 1;
      i = m;
      break label13;
      break;
      label55:
      m = i;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/Base64.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */