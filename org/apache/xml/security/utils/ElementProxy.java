package org.apache.xml.security.utils;

import java.util.HashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.exceptions.XMLSecurityException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

public abstract class ElementProxy
{
  static Log j;
  static HashMap n;
  static HashMap o;
  static Class p;
  protected Element k = null;
  protected String l = null;
  protected Document m = null;
  
  static
  {
    Object localObject = p;
    if (localObject == null)
    {
      localObject = c("org.apache.xml.security.utils.ElementProxy");
      p = (Class)localObject;
    }
    for (;;)
    {
      j = LogFactory.getLog(((Class)localObject).getName());
      localObject = new java/util/HashMap;
      ((HashMap)localObject).<init>();
      n = (HashMap)localObject;
      localObject = new java/util/HashMap;
      ((HashMap)localObject).<init>();
      o = (HashMap)localObject;
      return;
      localObject = p;
    }
  }
  
  public ElementProxy() {}
  
  public ElementProxy(Element paramElement, String paramString)
  {
    if (paramElement == null)
    {
      localObject1 = new org/apache/xml/security/exceptions/XMLSecurityException;
      ((XMLSecurityException)localObject1).<init>("ElementProxy.nullElement");
      throw ((Throwable)localObject1);
    }
    localObject1 = j;
    bool = ((Log)localObject1).isDebugEnabled();
    if (bool)
    {
      localObject1 = j;
      Object localObject2 = new java/lang/StringBuffer;
      ((StringBuffer)localObject2).<init>();
      localObject2 = ((StringBuffer)localObject2).append("setElement(\"");
      String str = paramElement.getTagName();
      localObject2 = ((StringBuffer)localObject2).append(str).append("\", \"").append(paramString);
      str = "\")";
      localObject2 = str;
      ((Log)localObject1).debug(localObject2);
    }
    localObject1 = paramElement.getOwnerDocument();
    m = ((Document)localObject1);
    k = paramElement;
    l = paramString;
    m();
  }
  
  static Class c(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public static void d(String paramString1, String paramString2)
  {
    Object localObject1 = n;
    boolean bool1 = ((HashMap)localObject1).containsValue(paramString2);
    if (bool1)
    {
      localObject1 = n.get(paramString1);
      boolean bool2 = localObject1.equals(paramString2);
      if (!bool2)
      {
        localObject2 = new Object[3];
        localObject2[0] = paramString2;
        localObject2[1] = paramString1;
        localObject2[2] = localObject1;
        localObject1 = new org/apache/xml/security/exceptions/XMLSecurityException;
        ((XMLSecurityException)localObject1).<init>("prefix.AlreadyAssigned", (Object[])localObject2);
        throw ((Throwable)localObject1);
      }
    }
    localObject1 = "http://www.w3.org/2000/09/xmldsig#";
    bool1 = ((String)localObject1).equals(paramString1);
    if (bool1) {
      XMLUtils.a = paramString2;
    }
    localObject1 = "http://www.w3.org/2001/04/xmlenc#";
    bool1 = ((String)localObject1).equals(paramString1);
    if (bool1) {
      XMLUtils.b = paramString2;
    }
    localObject1 = n;
    Object localObject2 = paramString2.intern();
    ((HashMap)localObject1).put(paramString1, localObject2);
    int i = paramString2.length();
    if (i == 0)
    {
      localObject1 = o;
      localObject2 = "xmlns";
      ((HashMap)localObject1).put(paramString1, localObject2);
    }
    for (;;)
    {
      return;
      localObject1 = o;
      localObject2 = new java/lang/StringBuffer;
      ((StringBuffer)localObject2).<init>();
      String str = "xmlns:";
      localObject2 = (str + paramString2).intern();
      ((HashMap)localObject1).put(paramString1, localObject2);
    }
  }
  
  public String b(String paramString1, String paramString2)
  {
    return ((Text)XMLUtils.a(k.getFirstChild(), paramString2, paramString1, 0).getFirstChild()).getData();
  }
  
  public int c(String paramString1, String paramString2)
  {
    Node localNode1 = k.getFirstChild();
    int i = 0;
    for (Node localNode2 = localNode1; localNode2 != null; localNode2 = localNode2.getNextSibling())
    {
      String str = localNode2.getLocalName();
      boolean bool = paramString2.equals(str);
      if (bool)
      {
        str = localNode2.getNamespaceURI();
        bool = paramString1.equals(str);
        if (bool) {
          i += 1;
        }
      }
    }
    return i;
  }
  
  public abstract String d();
  
  public abstract String e();
  
  public final Element k()
  {
    return k;
  }
  
  public String l()
  {
    return l;
  }
  
  void m()
  {
    Object localObject1 = e();
    String str1 = d();
    String str2 = k.getLocalName();
    Object localObject2 = k.getNamespaceURI();
    boolean bool = str1.equals(localObject2);
    if (!bool)
    {
      bool = ((String)localObject1).equals(str2);
      if (!bool)
      {
        Object[] arrayOfObject = new Object[2];
        StringBuffer localStringBuffer = new java/lang/StringBuffer;
        localStringBuffer.<init>();
        str2 = (String)localObject2 + ":" + str2;
        arrayOfObject[0] = str2;
        localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        localObject1 = str1 + ":" + (String)localObject1;
        arrayOfObject[1] = localObject1;
        localObject1 = new org/apache/xml/security/exceptions/XMLSecurityException;
        ((XMLSecurityException)localObject1).<init>("xml.WrongElement", arrayOfObject);
        throw ((Throwable)localObject1);
      }
    }
  }
  
  public byte[] n()
  {
    return Base64.a(XMLUtils.a(k));
  }
  
  public String o()
  {
    return XMLUtils.a(k);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/ElementProxy.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */