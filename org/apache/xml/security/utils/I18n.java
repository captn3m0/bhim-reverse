package org.apache.xml.security.utils;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;
import org.apache.xml.security.Init;

public class I18n
{
  static String a;
  static String b;
  static ResourceBundle c;
  static boolean d = false;
  static String e = null;
  static String f = null;
  
  public static String a(String paramString)
  {
    return b(paramString);
  }
  
  public static String a(String paramString, Exception paramException)
  {
    bool = true;
    try
    {
      localObject1 = new Object[bool];
      localObject4 = null;
      String str = paramException.getMessage();
      localObject1[0] = str;
      localObject4 = c;
      localObject4 = ((ResourceBundle)localObject4).getString(paramString);
      localObject1 = MessageFormat.format((String)localObject4, (Object[])localObject1);
    }
    finally
    {
      for (;;)
      {
        Object localObject1;
        Object localObject4;
        bool = Init.a();
        Object localObject3;
        if (bool)
        {
          localObject3 = new java/lang/StringBuffer;
          ((StringBuffer)localObject3).<init>();
          localObject3 = ((StringBuffer)localObject3).append("No message with ID \"").append(paramString).append("\" found in resource bundle \"").append("org/apache/xml/security/resource/xmlsecurity").append("\". Original Exception was a ");
          localObject4 = paramException.getClass().getName();
          localObject3 = ((StringBuffer)localObject3).append((String)localObject4).append(" and message ");
          localObject4 = paramException.getMessage();
          localObject3 = (String)localObject4;
        }
        else
        {
          localObject3 = "You must initialize the xml-security library correctly before you use it. Call the static method \"org.apache.xml.security.Init.init();\" to do that before you use any functionality from that library.";
        }
      }
    }
    return (String)localObject1;
  }
  
  public static String a(String paramString, Object[] paramArrayOfObject)
  {
    return b(paramString, paramArrayOfObject);
  }
  
  public static void a(String paramString1, String paramString2)
  {
    a = paramString1;
    String str1 = a;
    if (str1 == null)
    {
      str1 = Locale.getDefault().getLanguage();
      a = str1;
    }
    b = paramString2;
    str1 = b;
    if (str1 == null)
    {
      str1 = Locale.getDefault().getCountry();
      b = str1;
    }
    str1 = a;
    String str2 = b;
    b(str1, str2);
  }
  
  public static String b(String paramString)
  {
    try
    {
      localObject1 = c;
      localObject1 = ((ResourceBundle)localObject1).getString(paramString);
    }
    finally
    {
      for (;;)
      {
        Object localObject1;
        boolean bool = Init.a();
        Object localObject3;
        if (bool)
        {
          localObject3 = new java/lang/StringBuffer;
          ((StringBuffer)localObject3).<init>();
          localObject3 = ((StringBuffer)localObject3).append("No message with ID \"").append(paramString).append("\" found in resource bundle \"").append("org/apache/xml/security/resource/xmlsecurity");
          String str = "\"";
          localObject3 = str;
        }
        else
        {
          localObject3 = "You must initialize the xml-security library correctly before you use it. Call the static method \"org.apache.xml.security.Init.init();\" to do that before you use any functionality from that library.";
        }
      }
    }
    return (String)localObject1;
  }
  
  public static String b(String paramString, Object[] paramArrayOfObject)
  {
    try
    {
      localObject1 = c;
      localObject1 = ((ResourceBundle)localObject1).getString(paramString);
      localObject1 = MessageFormat.format((String)localObject1, paramArrayOfObject);
    }
    finally
    {
      for (;;)
      {
        Object localObject1;
        boolean bool = Init.a();
        Object localObject3;
        if (bool)
        {
          localObject3 = new java/lang/StringBuffer;
          ((StringBuffer)localObject3).<init>();
          localObject3 = ((StringBuffer)localObject3).append("No message with ID \"").append(paramString).append("\" found in resource bundle \"").append("org/apache/xml/security/resource/xmlsecurity");
          String str = "\"";
          localObject3 = str;
        }
        else
        {
          localObject3 = "You must initialize the xml-security library correctly before you use it. Call the static method \"org.apache.xml.security.Init.init();\" to do that before you use any functionality from that library.";
        }
      }
    }
    return (String)localObject1;
  }
  
  public static void b(String paramString1, String paramString2)
  {
    boolean bool = d;
    Object localObject;
    if (bool)
    {
      localObject = e;
      bool = paramString1.equals(localObject);
      if (bool)
      {
        localObject = f;
        bool = paramString2.equals(localObject);
        if (bool) {
          return;
        }
      }
    }
    if ((paramString1 != null) && (paramString2 != null))
    {
      int i = paramString1.length();
      if (i > 0)
      {
        i = paramString2.length();
        if (i > 0)
        {
          e = paramString1;
          f = paramString2;
        }
      }
    }
    for (;;)
    {
      Locale localLocale = new java/util/Locale;
      String str1 = e;
      String str2 = f;
      localLocale.<init>(str1, str2);
      localObject = ResourceBundle.getBundle("org/apache/xml/security/resource/xmlsecurity", localLocale);
      c = (ResourceBundle)localObject;
      break;
      f = b;
      localObject = a;
      e = (String)localObject;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/I18n.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */