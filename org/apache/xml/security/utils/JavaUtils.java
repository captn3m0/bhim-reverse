package org.apache.xml.security.utils;

import java.io.InputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class JavaUtils
{
  static Log a;
  static Class b;
  
  static
  {
    Class localClass = b;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.utils.JavaUtils");
      b = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = b;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public static byte[] a(InputStream paramInputStream)
  {
    UnsyncByteArrayOutputStream localUnsyncByteArrayOutputStream = new org/apache/xml/security/utils/UnsyncByteArrayOutputStream;
    localUnsyncByteArrayOutputStream.<init>();
    int i = 1024;
    byte[] arrayOfByte = new byte[i];
    for (;;)
    {
      int j = paramInputStream.read(arrayOfByte);
      if (j <= 0) {
        break;
      }
      localUnsyncByteArrayOutputStream.write(arrayOfByte, 0, j);
    }
    return localUnsyncByteArrayOutputStream.a();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/JavaUtils.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */