package org.apache.xml.security.utils.resolver;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.w3c.dom.Attr;

public class ResourceResolver
{
  static Log a;
  static boolean b;
  static List c;
  static boolean d;
  static Class f;
  protected ResourceResolverSpi e = null;
  
  static
  {
    Class localClass = f;
    if (localClass == null)
    {
      localClass = b("org.apache.xml.security.utils.resolver.ResourceResolver");
      f = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      b = false;
      c = null;
      d = true;
      return;
      localClass = f;
    }
  }
  
  private ResourceResolver(String paramString)
  {
    ResourceResolverSpi localResourceResolverSpi = (ResourceResolverSpi)Class.forName(paramString).newInstance();
    e = localResourceResolverSpi;
  }
  
  public ResourceResolver(ResourceResolverSpi paramResourceResolverSpi)
  {
    e = paramResourceResolverSpi;
  }
  
  public static final ResourceResolver a(Attr paramAttr, String paramString)
  {
    Object localObject1 = c;
    int i = ((List)localObject1).size();
    for (int j = 0; j < i; j = k)
    {
      localObject1 = (ResourceResolver)c.get(j);
      try
      {
        boolean bool = d;
        Object localObject4;
        if (!bool)
        {
          localObject3 = e;
          bool = ((ResourceResolverSpi)localObject3).a();
          if (!bool) {}
        }
        else
        {
          localObject4 = localObject1;
        }
        for (;;)
        {
          localObject3 = a;
          bool = ((Log)localObject3).isDebugEnabled();
          if (bool)
          {
            localObject3 = a;
            Object localObject5 = new java/lang/StringBuffer;
            ((StringBuffer)localObject5).<init>();
            localObject5 = ((StringBuffer)localObject5).append("check resolvability by class ");
            String str = e.getClass().getName();
            localObject5 = str;
            ((Log)localObject3).debug(localObject5);
          }
          if (localObject1 == null) {
            break;
          }
          bool = ((ResourceResolver)localObject4).c(paramAttr, paramString);
          if (!bool) {
            break;
          }
          if (j != 0)
          {
            localObject3 = (List)((ArrayList)c).clone();
            ((List)localObject3).remove(j);
            ((List)localObject3).add(0, localObject1);
            c = (List)localObject3;
          }
          return (ResourceResolver)localObject4;
          localObject4 = new org/apache/xml/security/utils/resolver/ResourceResolver;
          localObject3 = e;
          localObject3 = localObject3.getClass();
          localObject3 = ((Class)localObject3).newInstance();
          localObject3 = (ResourceResolverSpi)localObject3;
          ((ResourceResolver)localObject4).<init>((ResourceResolverSpi)localObject3);
        }
        k = j + 1;
      }
      catch (InstantiationException localInstantiationException)
      {
        localObject3 = new org/apache/xml/security/utils/resolver/ResourceResolverException;
        ((ResourceResolverException)localObject3).<init>("", localInstantiationException, paramAttr, paramString);
        throw ((Throwable)localObject3);
      }
      catch (IllegalAccessException localIllegalAccessException)
      {
        localObject3 = new org/apache/xml/security/utils/resolver/ResourceResolverException;
        ((ResourceResolverException)localObject3).<init>("", localIllegalAccessException, paramAttr, paramString);
        throw ((Throwable)localObject3);
      }
    }
    int k = 2;
    Object localObject3 = new Object[k];
    if (paramAttr != null) {}
    for (Object localObject2 = paramAttr.getNodeValue();; localObject2 = "null")
    {
      localObject3[0] = localObject2;
      localObject3[1] = paramString;
      localObject2 = new org/apache/xml/security/utils/resolver/ResourceResolverException;
      ((ResourceResolverException)localObject2).<init>("utils.resolver.noClass", (Object[])localObject3, paramAttr, paramString);
      throw ((Throwable)localObject2);
    }
  }
  
  public static final ResourceResolver a(Attr paramAttr, String paramString, List paramList)
  {
    int i = 0;
    Object localObject1 = a;
    int j = ((Log)localObject1).isDebugEnabled();
    Object localObject3;
    if (j != 0)
    {
      Object localObject2 = a;
      localObject1 = new java/lang/StringBuffer;
      ((StringBuffer)localObject1).<init>();
      localObject3 = ((StringBuffer)localObject1).append("I was asked to create a ResourceResolver and got ");
      if (paramList == null)
      {
        j = 0;
        localObject1 = null;
        localObject1 = j;
        ((Log)localObject2).debug(localObject1);
        localObject1 = a;
        localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        localObject2 = ((StringBuffer)localObject2).append(" extra resolvers to my existing ");
        int m = c.size();
        localObject2 = ((StringBuffer)localObject2).append(m);
        localObject3 = " system-wide resolvers";
        localObject2 = (String)localObject3;
        ((Log)localObject1).debug(localObject2);
      }
    }
    else
    {
      if (paramList == null) {
        break label300;
      }
      int n = paramList.size();
      if (n <= 0) {
        break label300;
      }
      label161:
      if (i >= n) {
        break label300;
      }
      localObject1 = (ResourceResolver)paramList.get(i);
      if (localObject1 == null) {
        break label293;
      }
      localObject3 = e.getClass().getName();
      Log localLog = a;
      boolean bool2 = localLog.isDebugEnabled();
      if (bool2)
      {
        localLog = a;
        StringBuffer localStringBuffer = new java/lang/StringBuffer;
        localStringBuffer.<init>();
        String str = "check resolvability by class ";
        localStringBuffer = localStringBuffer.append(str);
        localObject3 = (String)localObject3;
        localLog.debug(localObject3);
      }
      boolean bool1 = ((ResourceResolver)localObject1).c(paramAttr, paramString);
      if (!bool1) {
        break label293;
      }
    }
    for (;;)
    {
      return (ResourceResolver)localObject1;
      int k = paramList.size();
      break;
      label293:
      i += 1;
      break label161;
      label300:
      localObject1 = a(paramAttr, paramString);
    }
  }
  
  public static void a()
  {
    boolean bool = b;
    if (!bool)
    {
      ArrayList localArrayList = new java/util/ArrayList;
      int i = 10;
      localArrayList.<init>(i);
      c = localArrayList;
      bool = true;
      b = bool;
    }
  }
  
  public static void a(String paramString)
  {
    a(paramString, false);
  }
  
  private static void a(String paramString, boolean paramBoolean)
  {
    for (;;)
    {
      try
      {
        localObject1 = new org/apache/xml/security/utils/resolver/ResourceResolver;
        ((ResourceResolver)localObject1).<init>(paramString);
        if (paramBoolean)
        {
          localObject2 = c;
          str = null;
          ((List)localObject2).add(0, localObject1);
          localObject2 = a;
          str = "registered resolver";
          ((Log)localObject2).debug(str);
          localObject1 = e;
          boolean bool = ((ResourceResolverSpi)localObject1).a();
          if (!bool)
          {
            bool = false;
            localObject1 = null;
            d = false;
          }
          return;
        }
      }
      catch (Exception localException)
      {
        Object localObject1;
        Log localLog1 = a;
        localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        localObject2 = ((StringBuffer)localObject2).append("Error loading resolver ").append(paramString);
        str = " disabling it";
        localObject2 = str;
        localLog1.warn(localObject2);
        continue;
      }
      catch (NoClassDefFoundError localNoClassDefFoundError)
      {
        Log localLog2 = a;
        Object localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        localObject2 = ((StringBuffer)localObject2).append("Error loading resolver ").append(paramString);
        String str = " disabling it";
        localObject2 = str;
        localLog2.warn(localObject2);
        continue;
      }
      localObject2 = c;
      ((List)localObject2).add(localObject1);
    }
  }
  
  static Class b(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  private boolean c(Attr paramAttr, String paramString)
  {
    return e.b(paramAttr, paramString);
  }
  
  public void a(Map paramMap)
  {
    e.a(paramMap);
  }
  
  public XMLSignatureInput b(Attr paramAttr, String paramString)
  {
    return e.a(paramAttr, paramString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/resolver/ResourceResolver.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */