package org.apache.xml.security.utils.resolver;

import org.apache.xml.security.exceptions.XMLSecurityException;
import org.w3c.dom.Attr;

public class ResourceResolverException
  extends XMLSecurityException
{
  Attr c = null;
  String d;
  
  public ResourceResolverException(String paramString1, Exception paramException, Attr paramAttr, String paramString2)
  {
    super(paramString1, paramException);
    c = paramAttr;
    d = paramString2;
  }
  
  public ResourceResolverException(String paramString1, Object[] paramArrayOfObject, Attr paramAttr, String paramString2)
  {
    super(paramString1, paramArrayOfObject);
    c = paramAttr;
    d = paramString2;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/resolver/ResourceResolverException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */