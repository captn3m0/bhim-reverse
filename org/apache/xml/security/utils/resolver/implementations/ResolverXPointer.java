package org.apache.xml.security.utils.resolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.IdResolver;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class ResolverXPointer
  extends ResourceResolverSpi
{
  static Log d;
  static Class e;
  private static final int f;
  
  static
  {
    Class localClass = e;
    if (localClass == null)
    {
      localClass = c("org.apache.xml.security.utils.resolver.implementations.ResolverXPointer");
      e = localClass;
    }
    for (;;)
    {
      d = LogFactory.getLog(localClass.getName());
      f = "#xpointer(id(".length();
      return;
      localClass = e;
    }
  }
  
  static Class c(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  private static boolean d(String paramString)
  {
    String str = "#xpointer(/)";
    boolean bool = paramString.equals(str);
    if (bool) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  private static boolean e(String paramString)
  {
    int i = 39;
    int j = 34;
    int k = 1;
    boolean bool1 = false;
    Log localLog = null;
    String str1 = "#xpointer(id(";
    boolean bool2 = paramString.startsWith(str1);
    if (bool2)
    {
      str1 = "))";
      bool2 = paramString.endsWith(str1);
      if (bool2)
      {
        int n = f;
        int i1 = paramString.length() + -2;
        str1 = paramString.substring(n, i1);
        i1 = str1.length() + -1;
        int i2 = str1.charAt(0);
        if (i2 == j)
        {
          i2 = str1.charAt(i1);
          if (i2 == j) {}
        }
        else
        {
          i2 = str1.charAt(0);
          if (i2 != i) {
            break label219;
          }
          i2 = str1.charAt(i1);
          if (i2 != i) {
            break label219;
          }
        }
        localLog = d;
        bool1 = localLog.isDebugEnabled();
        if (bool1)
        {
          localLog = d;
          StringBuffer localStringBuffer = new java/lang/StringBuffer;
          localStringBuffer.<init>();
          String str2 = "Id=";
          localStringBuffer = localStringBuffer.append(str2);
          str1 = str1.substring(k, i1);
          str1 = str1;
          localLog.debug(str1);
        }
      }
    }
    for (;;)
    {
      return k;
      label219:
      int m = 0;
    }
  }
  
  private static String f(String paramString)
  {
    int i = 39;
    int j = 34;
    String str = "#xpointer(id(";
    boolean bool = paramString.startsWith(str);
    int k;
    int m;
    int n;
    if (bool)
    {
      str = "))";
      bool = paramString.endsWith(str);
      if (bool)
      {
        k = f;
        m = paramString.length() + -2;
        str = paramString.substring(k, m);
        m = str.length() + -1;
        n = str.charAt(0);
        if (n == j)
        {
          n = str.charAt(m);
          if (n == j) {}
        }
        else
        {
          n = str.charAt(0);
          if (n != i) {
            break label135;
          }
          n = str.charAt(m);
          if (n != i) {
            break label135;
          }
        }
        n = 1;
      }
    }
    for (str = str.substring(n, m);; str = null)
    {
      return str;
      label135:
      k = 0;
    }
  }
  
  public XMLSignatureInput a(Attr paramAttr, String paramString)
  {
    Object localObject1 = null;
    Object localObject2 = paramAttr.getOwnerElement().getOwnerDocument();
    String str = paramAttr.getNodeValue();
    boolean bool = d(str);
    if (bool) {}
    for (;;)
    {
      localObject1 = new org/apache/xml/security/signature/XMLSignatureInput;
      ((XMLSignatureInput)localObject1).<init>((Node)localObject2);
      localObject2 = "text/xml";
      ((XMLSignatureInput)localObject1).a((String)localObject2);
      if (paramString != null)
      {
        i = paramString.length();
        if (i > 0)
        {
          localObject2 = paramAttr.getNodeValue();
          localObject2 = paramString.concat((String)localObject2);
          ((XMLSignatureInput)localObject1).b((String)localObject2);
        }
      }
      for (;;)
      {
        return (XMLSignatureInput)localObject1;
        bool = e(str);
        if (!bool) {
          break label169;
        }
        localObject1 = f(str);
        localObject2 = IdResolver.a((Document)localObject2, (String)localObject1);
        if (localObject2 != null) {
          break;
        }
        localObject2 = new Object[1];
        localObject2[0] = localObject1;
        localObject1 = new org/apache/xml/security/utils/resolver/ResourceResolverException;
        ((ResourceResolverException)localObject1).<init>("signature.Verification.MissingID", (Object[])localObject2, paramAttr, paramString);
        throw ((Throwable)localObject1);
        localObject2 = paramAttr.getNodeValue();
        ((XMLSignatureInput)localObject1).b((String)localObject2);
      }
      label169:
      int i = 0;
      localObject2 = null;
    }
  }
  
  public boolean a()
  {
    return true;
  }
  
  public boolean b(Attr paramAttr, String paramString)
  {
    boolean bool1 = false;
    if (paramAttr == null) {}
    for (;;)
    {
      return bool1;
      String str = paramAttr.getNodeValue();
      boolean bool2 = d(str);
      if (!bool2)
      {
        boolean bool3 = e(str);
        if (!bool3) {}
      }
      else
      {
        bool1 = true;
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/resolver/implementations/ResolverXPointer.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */