package org.apache.xml.security.utils.resolver.implementations;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.Base64;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.apache.xml.utils.URI;
import org.w3c.dom.Attr;

public class ResolverDirectHTTP
  extends ResourceResolverSpi
{
  static Log d;
  static Class e;
  private static final String[] f;
  
  static
  {
    Object localObject = e;
    if (localObject == null)
    {
      localObject = c("org.apache.xml.security.utils.resolver.implementations.ResolverDirectHTTP");
      e = (Class)localObject;
    }
    for (;;)
    {
      d = LogFactory.getLog(((Class)localObject).getName());
      localObject = new String[6];
      localObject[0] = "http.proxy.host";
      localObject[1] = "http.proxy.port";
      localObject[2] = "http.proxy.username";
      localObject[3] = "http.proxy.password";
      localObject[4] = "http.basic.username";
      localObject[5] = "http.basic.password";
      f = (String[])localObject;
      return;
      localObject = e;
    }
  }
  
  private URI a(String paramString1, String paramString2)
  {
    Object localObject;
    if (paramString2 != null)
    {
      localObject = "";
      boolean bool = ((String)localObject).equals(paramString2);
      if (!bool) {}
    }
    else
    {
      localObject = new org/apache/xml/utils/URI;
      ((URI)localObject).<init>(paramString1);
    }
    for (;;)
    {
      return (URI)localObject;
      localObject = new org/apache/xml/utils/URI;
      URI localURI = new org/apache/xml/utils/URI;
      localURI.<init>(paramString2);
      ((URI)localObject).<init>(localURI, paramString1);
    }
  }
  
  static Class c(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public XMLSignatureInput a(Attr paramAttr, String paramString)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    Object localObject1 = null;
    int i = 0;
    Object localObject2 = null;
    for (;;)
    {
      try
      {
        localObject3 = f;
        j = 0;
        localObject4 = null;
        localObject3 = localObject3[0];
        localObject5 = a((String)localObject3);
        localObject3 = f;
        j = 1;
        localObject3 = localObject3[j];
        Object localObject6 = a((String)localObject3);
        if ((localObject5 == null) || (localObject6 == null)) {
          break label997;
        }
        k = bool1;
        if (k != 0)
        {
          localObject1 = d;
          bool2 = ((Log)localObject1).isDebugEnabled();
          if (bool2)
          {
            localObject1 = d;
            localObject3 = new java/lang/StringBuffer;
            ((StringBuffer)localObject3).<init>();
            localObject4 = "Use of HTTP proxy enabled: ";
            localObject3 = ((StringBuffer)localObject3).append((String)localObject4);
            localObject3 = ((StringBuffer)localObject3).append((String)localObject5);
            localObject4 = ":";
            localObject3 = ((StringBuffer)localObject3).append((String)localObject4);
            localObject3 = ((StringBuffer)localObject3).append((String)localObject6);
            localObject3 = ((StringBuffer)localObject3).toString();
            ((Log)localObject1).debug(localObject3);
          }
          localObject1 = "http.proxySet";
          localObject4 = System.getProperty((String)localObject1);
          localObject1 = "http.proxyHost";
          localObject3 = System.getProperty((String)localObject1);
          localObject1 = "http.proxyPort";
          localObject1 = System.getProperty((String)localObject1);
          Object localObject7 = "http.proxySet";
          Object localObject8 = "true";
          System.setProperty((String)localObject7, (String)localObject8);
          localObject7 = "http.proxyHost";
          System.setProperty((String)localObject7, (String)localObject5);
          localObject5 = "http.proxyPort";
          System.setProperty((String)localObject5, (String)localObject6);
          localObject5 = localObject4;
          localObject4 = localObject3;
          localObject3 = localObject1;
          if ((localObject5 != null) && (localObject4 != null) && (localObject3 != null))
          {
            bool2 = bool1;
            localObject9 = paramAttr.getNodeValue();
            localObject6 = a((String)localObject9, paramString);
            localObject9 = new org/apache/xml/utils/URI;
            ((URI)localObject9).<init>((URI)localObject6);
            localObject7 = null;
            ((URI)localObject9).setFragment(null);
            localObject7 = new java/net/URL;
            localObject9 = ((URI)localObject9).toString();
            ((URL)localObject7).<init>((String)localObject9);
            localObject9 = ((URL)localObject7).openConnection();
            localObject8 = f;
            int m = 2;
            localObject8 = localObject8[m];
            localObject8 = a((String)localObject8);
            localObject10 = f;
            int n = 3;
            localObject10 = localObject10[n];
            localObject10 = a((String)localObject10);
            if ((localObject8 != null) && (localObject10 != null))
            {
              localObject11 = new java/lang/StringBuffer;
              ((StringBuffer)localObject11).<init>();
              localObject8 = ((StringBuffer)localObject11).append((String)localObject8);
              localObject11 = ":";
              localObject8 = ((StringBuffer)localObject8).append((String)localObject11);
              localObject8 = ((StringBuffer)localObject8).append((String)localObject10);
              localObject8 = ((StringBuffer)localObject8).toString();
              localObject8 = ((String)localObject8).getBytes();
              localObject8 = Base64.b((byte[])localObject8);
              localObject10 = "Proxy-Authorization";
              ((URLConnection)localObject9).setRequestProperty((String)localObject10, (String)localObject8);
            }
            localObject8 = "WWW-Authenticate";
            localObject8 = ((URLConnection)localObject9).getHeaderField((String)localObject8);
            if (localObject8 != null)
            {
              localObject10 = "Basic";
              boolean bool3 = ((String)localObject8).startsWith((String)localObject10);
              if (bool3)
              {
                localObject8 = f;
                m = 4;
                localObject8 = localObject8[m];
                localObject8 = a((String)localObject8);
                localObject10 = f;
                n = 5;
                localObject10 = localObject10[n];
                localObject10 = a((String)localObject10);
                if ((localObject8 != null) && (localObject10 != null))
                {
                  localObject9 = ((URL)localObject7).openConnection();
                  localObject7 = new java/lang/StringBuffer;
                  ((StringBuffer)localObject7).<init>();
                  localObject7 = ((StringBuffer)localObject7).append((String)localObject8);
                  localObject8 = ":";
                  localObject7 = ((StringBuffer)localObject7).append((String)localObject8);
                  localObject7 = ((StringBuffer)localObject7).append((String)localObject10);
                  localObject7 = ((StringBuffer)localObject7).toString();
                  localObject7 = ((String)localObject7).getBytes();
                  localObject7 = Base64.b((byte[])localObject7);
                  localObject8 = "Authorization";
                  localObject10 = new java/lang/StringBuffer;
                  ((StringBuffer)localObject10).<init>();
                  localObject11 = "Basic ";
                  localObject10 = ((StringBuffer)localObject10).append((String)localObject11);
                  localObject7 = ((StringBuffer)localObject10).append((String)localObject7);
                  localObject7 = ((StringBuffer)localObject7).toString();
                  ((URLConnection)localObject9).setRequestProperty((String)localObject8, (String)localObject7);
                }
              }
            }
            localObject7 = "Content-Type";
            localObject7 = ((URLConnection)localObject9).getHeaderField((String)localObject7);
            localObject9 = ((URLConnection)localObject9).getInputStream();
            localObject8 = new java/io/ByteArrayOutputStream;
            ((ByteArrayOutputStream)localObject8).<init>();
            m = 4096;
            localObject10 = new byte[m];
            n = ((InputStream)localObject9).read((byte[])localObject10);
            if (n >= 0)
            {
              ((ByteArrayOutputStream)localObject8).write((byte[])localObject10, 0, n);
              i += n;
              continue;
            }
          }
          else
          {
            bool2 = false;
            localObject1 = null;
            continue;
          }
          localObject9 = d;
          Object localObject10 = new java/lang/StringBuffer;
          ((StringBuffer)localObject10).<init>();
          Object localObject11 = "Fetched ";
          localObject10 = ((StringBuffer)localObject10).append((String)localObject11);
          localObject2 = ((StringBuffer)localObject10).append(i);
          localObject10 = " bytes from URI ";
          localObject2 = ((StringBuffer)localObject2).append((String)localObject10);
          localObject10 = ((URI)localObject6).toString();
          localObject2 = ((StringBuffer)localObject2).append((String)localObject10);
          localObject2 = ((StringBuffer)localObject2).toString();
          ((Log)localObject9).debug(localObject2);
          localObject2 = new org/apache/xml/security/signature/XMLSignatureInput;
          localObject9 = ((ByteArrayOutputStream)localObject8).toByteArray();
          ((XMLSignatureInput)localObject2).<init>((byte[])localObject9);
          localObject9 = ((URI)localObject6).toString();
          ((XMLSignatureInput)localObject2).b((String)localObject9);
          ((XMLSignatureInput)localObject2).a((String)localObject7);
          if ((k != 0) && (bool2))
          {
            localObject9 = "http.proxySet";
            System.setProperty((String)localObject9, (String)localObject5);
            localObject9 = "http.proxyHost";
            System.setProperty((String)localObject9, (String)localObject4);
            localObject9 = "http.proxyPort";
            System.setProperty((String)localObject9, (String)localObject3);
          }
          return (XMLSignatureInput)localObject2;
        }
      }
      catch (MalformedURLException localMalformedURLException)
      {
        localObject9 = new org/apache/xml/security/utils/resolver/ResourceResolverException;
        ((ResourceResolverException)localObject9).<init>("generic.EmptyMessage", localMalformedURLException, paramAttr, paramString);
        throw ((Throwable)localObject9);
      }
      catch (IOException localIOException)
      {
        Object localObject9 = new org/apache/xml/security/utils/resolver/ResourceResolverException;
        ((ResourceResolverException)localObject9).<init>("generic.EmptyMessage", localIOException, paramAttr, paramString);
        throw ((Throwable)localObject9);
      }
      Object localObject3 = null;
      int j = 0;
      Object localObject4 = null;
      Object localObject5 = null;
      continue;
      label997:
      int k = 0;
    }
  }
  
  public boolean a()
  {
    return true;
  }
  
  public boolean b(Attr paramAttr, String paramString)
  {
    boolean bool1 = false;
    Log localLog = null;
    Object localObject1;
    Object localObject2;
    if (paramAttr == null)
    {
      localObject1 = d;
      localObject2 = "quick fail, uri == null";
      ((Log)localObject1).debug(localObject2);
    }
    for (;;)
    {
      return bool1;
      localObject1 = paramAttr.getNodeValue();
      localObject2 = "";
      boolean bool2 = ((String)localObject1).equals(localObject2);
      if (!bool2)
      {
        int i = ((String)localObject1).charAt(0);
        int j = 35;
        if (i != j) {}
      }
      else
      {
        localObject1 = d;
        localObject2 = "quick fail for empty URIs and local ones";
        ((Log)localObject1).debug(localObject2);
        continue;
      }
      localObject2 = d;
      boolean bool3 = ((Log)localObject2).isDebugEnabled();
      Object localObject3;
      String str;
      if (bool3)
      {
        localObject2 = d;
        localObject3 = new java/lang/StringBuffer;
        ((StringBuffer)localObject3).<init>();
        str = "I was asked whether I can resolve ";
        localObject3 = str + (String)localObject1;
        ((Log)localObject2).debug(localObject3);
      }
      localObject2 = "http:";
      bool3 = ((String)localObject1).startsWith((String)localObject2);
      if (!bool3)
      {
        if (paramString != null)
        {
          localObject2 = "http:";
          bool3 = paramString.startsWith((String)localObject2);
          if (!bool3) {}
        }
      }
      else
      {
        localLog = d;
        bool1 = localLog.isDebugEnabled();
        if (bool1)
        {
          localLog = d;
          localObject2 = new java/lang/StringBuffer;
          ((StringBuffer)localObject2).<init>();
          localObject3 = "I state that I can resolve ";
          localObject2 = ((StringBuffer)localObject2).append((String)localObject3);
          localObject1 = (String)localObject1;
          localLog.debug(localObject1);
        }
        bool1 = true;
        continue;
      }
      localObject2 = d;
      bool3 = ((Log)localObject2).isDebugEnabled();
      if (bool3)
      {
        localObject2 = d;
        localObject3 = new java/lang/StringBuffer;
        ((StringBuffer)localObject3).<init>();
        str = "I state that I can't resolve ";
        localObject3 = ((StringBuffer)localObject3).append(str);
        localObject1 = (String)localObject1;
        ((Log)localObject2).debug(localObject1);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/resolver/implementations/ResolverDirectHTTP.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */