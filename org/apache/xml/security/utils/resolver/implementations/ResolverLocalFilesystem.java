package org.apache.xml.security.utils.resolver.implementations;

import java.io.FileInputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.apache.xml.utils.URI;
import org.w3c.dom.Attr;

public class ResolverLocalFilesystem
  extends ResourceResolverSpi
{
  static Log d;
  static Class e;
  private static int f;
  
  static
  {
    Class localClass = e;
    if (localClass == null)
    {
      localClass = c("org.apache.xml.security.utils.resolver.implementations.ResolverLocalFilesystem");
      e = localClass;
    }
    for (;;)
    {
      d = LogFactory.getLog(localClass.getName());
      f = "file:/".length();
      return;
      localClass = e;
    }
  }
  
  private static URI a(String paramString1, String paramString2)
  {
    Object localObject;
    if (paramString2 != null)
    {
      localObject = "";
      boolean bool = ((String)localObject).equals(paramString2);
      if (!bool) {}
    }
    else
    {
      localObject = new org/apache/xml/utils/URI;
      ((URI)localObject).<init>(paramString1);
    }
    for (;;)
    {
      return (URI)localObject;
      localObject = new org/apache/xml/utils/URI;
      URI localURI = new org/apache/xml/utils/URI;
      localURI.<init>(paramString2);
      ((URI)localObject).<init>(localURI, paramString1);
    }
  }
  
  static Class c(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  private static String d(String paramString)
  {
    int i = -1;
    int j = f;
    Object localObject1 = paramString.substring(j);
    Object localObject2 = "%20";
    j = ((String)localObject1).indexOf((String)localObject2);
    Object localObject3;
    int k;
    if (j > i)
    {
      j = 0;
      localObject2 = null;
      localObject3 = new java/lang/StringBuffer;
      k = ((String)localObject1).length();
      ((StringBuffer)localObject3).<init>(k);
      String str1 = "%20";
      k = ((String)localObject1).indexOf(str1, j);
      if (k == i)
      {
        String str2 = ((String)localObject1).substring(j);
        ((StringBuffer)localObject3).append(str2);
        label85:
        if (k != i) {
          break label151;
        }
      }
    }
    for (localObject2 = ((StringBuffer)localObject3).toString();; localObject2 = localObject1)
    {
      int m = ((String)localObject2).charAt(1);
      int n = 58;
      if (m == n) {}
      for (;;)
      {
        return (String)localObject2;
        localObject2 = ((String)localObject1).substring(j, k);
        ((StringBuffer)localObject3).append((String)localObject2);
        ((StringBuffer)localObject3).append(' ');
        j = k + 3;
        break label85;
        label151:
        break;
        localObject1 = new java/lang/StringBuffer;
        ((StringBuffer)localObject1).<init>();
        localObject3 = "/";
        localObject1 = ((StringBuffer)localObject1).append((String)localObject3);
        localObject2 = (String)localObject2;
      }
    }
  }
  
  public XMLSignatureInput a(Attr paramAttr, String paramString)
  {
    try
    {
      Object localObject1 = paramAttr.getNodeValue();
      localObject1 = a((String)localObject1, paramString);
      localObject2 = new org/apache/xml/utils/URI;
      ((URI)localObject2).<init>((URI)localObject1);
      FileInputStream localFileInputStream = null;
      ((URI)localObject2).setFragment(null);
      localObject2 = ((URI)localObject2).toString();
      localObject2 = d((String)localObject2);
      localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>((String)localObject2);
      localObject2 = new org/apache/xml/security/signature/XMLSignatureInput;
      ((XMLSignatureInput)localObject2).<init>(localFileInputStream);
      localObject1 = ((URI)localObject1).toString();
      ((XMLSignatureInput)localObject2).b((String)localObject1);
      return (XMLSignatureInput)localObject2;
    }
    catch (Exception localException)
    {
      Object localObject2 = new org/apache/xml/security/utils/resolver/ResourceResolverException;
      ((ResourceResolverException)localObject2).<init>("generic.EmptyMessage", localException, paramAttr, paramString);
      throw ((Throwable)localObject2);
    }
  }
  
  public boolean a()
  {
    return true;
  }
  
  public boolean b(Attr paramAttr, String paramString)
  {
    boolean bool1 = false;
    if (paramAttr == null) {}
    for (;;)
    {
      return bool1;
      Object localObject1 = paramAttr.getNodeValue();
      Object localObject2 = "";
      boolean bool2 = ((String)localObject1).equals(localObject2);
      if (!bool2)
      {
        int i = ((String)localObject1).charAt(0);
        int j = 35;
        if (i != j)
        {
          localObject2 = "http:";
          boolean bool3 = ((String)localObject1).startsWith((String)localObject2);
          if (!bool3) {
            try
            {
              localObject2 = d;
              bool3 = ((Log)localObject2).isDebugEnabled();
              Object localObject3;
              String str;
              if (bool3)
              {
                localObject2 = d;
                localObject3 = new java/lang/StringBuffer;
                ((StringBuffer)localObject3).<init>();
                str = "I was asked whether I can resolve ";
                localObject3 = ((StringBuffer)localObject3).append(str);
                localObject3 = ((StringBuffer)localObject3).append((String)localObject1);
                localObject3 = ((StringBuffer)localObject3).toString();
                ((Log)localObject2).debug(localObject3);
              }
              localObject2 = "file:";
              bool3 = ((String)localObject1).startsWith((String)localObject2);
              if (!bool3)
              {
                localObject2 = "file:";
                bool3 = paramString.startsWith((String)localObject2);
                if (!bool3) {}
              }
              else
              {
                localObject2 = d;
                bool3 = ((Log)localObject2).isDebugEnabled();
                if (bool3)
                {
                  localObject2 = d;
                  localObject3 = new java/lang/StringBuffer;
                  ((StringBuffer)localObject3).<init>();
                  str = "I state that I can resolve ";
                  localObject3 = ((StringBuffer)localObject3).append(str);
                  localObject1 = ((StringBuffer)localObject3).append((String)localObject1);
                  localObject1 = ((StringBuffer)localObject1).toString();
                  ((Log)localObject2).debug(localObject1);
                }
                bool1 = true;
              }
            }
            catch (Exception localException)
            {
              Log localLog = d;
              localObject2 = "But I can't";
              localLog.debug(localObject2);
            }
          }
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/resolver/implementations/ResolverLocalFilesystem.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */