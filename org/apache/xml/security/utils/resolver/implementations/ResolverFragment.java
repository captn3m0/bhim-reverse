package org.apache.xml.security.utils.resolver.implementations;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.apache.xml.security.utils.IdResolver;
import org.apache.xml.security.utils.resolver.ResourceResolverException;
import org.apache.xml.security.utils.resolver.ResourceResolverSpi;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class ResolverFragment
  extends ResourceResolverSpi
{
  static Log d;
  static Class e;
  
  static
  {
    Class localClass = e;
    if (localClass == null)
    {
      localClass = c("org.apache.xml.security.utils.resolver.implementations.ResolverFragment");
      e = localClass;
    }
    for (;;)
    {
      d = LogFactory.getLog(localClass.getName());
      return;
      localClass = e;
    }
  }
  
  static Class c(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public XMLSignatureInput a(Attr paramAttr, String paramString)
  {
    int i = 1;
    Object localObject1 = paramAttr.getNodeValue();
    Object localObject2 = paramAttr.getOwnerElement().getOwnerDocument();
    Object localObject3 = "";
    boolean bool = ((String)localObject1).equals(localObject3);
    if (bool)
    {
      localObject1 = d;
      localObject3 = "ResolverFragment with empty URI (means complete document)";
      ((Log)localObject1).debug(localObject3);
      localObject1 = new org/apache/xml/security/signature/XMLSignatureInput;
      ((XMLSignatureInput)localObject1).<init>((Node)localObject2);
      ((XMLSignatureInput)localObject1).c(i);
      localObject2 = "text/xml";
      ((XMLSignatureInput)localObject1).a((String)localObject2);
      if (paramString == null) {
        break label256;
      }
      localObject2 = paramAttr.getNodeValue();
    }
    label256:
    for (localObject2 = paramString.concat((String)localObject2);; localObject2 = paramAttr.getNodeValue())
    {
      ((XMLSignatureInput)localObject1).b((String)localObject2);
      return (XMLSignatureInput)localObject1;
      localObject1 = ((String)localObject1).substring(i);
      localObject2 = IdResolver.a((Document)localObject2, (String)localObject1);
      if (localObject2 == null)
      {
        localObject2 = new Object[i];
        localObject2[0] = localObject1;
        localObject1 = new org/apache/xml/security/utils/resolver/ResourceResolverException;
        ((ResourceResolverException)localObject1).<init>("signature.Verification.MissingID", (Object[])localObject2, paramAttr, paramString);
        throw ((Throwable)localObject1);
      }
      localObject3 = d;
      bool = ((Log)localObject3).isDebugEnabled();
      if (!bool) {
        break;
      }
      localObject3 = d;
      Object localObject4 = new java/lang/StringBuffer;
      ((StringBuffer)localObject4).<init>();
      String str = "Try to catch an Element with ID ";
      localObject1 = ((StringBuffer)localObject4).append(str).append((String)localObject1);
      localObject4 = " and Element was ";
      localObject1 = (String)localObject4 + localObject2;
      ((Log)localObject3).debug(localObject1);
      break;
    }
  }
  
  public boolean a()
  {
    return true;
  }
  
  public boolean b(Attr paramAttr, String paramString)
  {
    int i = 1;
    boolean bool2 = false;
    Log localLog1 = null;
    Log localLog2;
    Object localObject1;
    if (paramAttr == null)
    {
      localLog2 = d;
      localObject1 = "Quick fail for null uri";
      localLog2.debug(localObject1);
    }
    for (;;)
    {
      return bool2;
      localObject1 = paramAttr.getNodeValue();
      Object localObject2 = "";
      boolean bool3 = ((String)localObject1).equals(localObject2);
      if (!bool3)
      {
        int j = ((String)localObject1).charAt(0);
        int k = 35;
        if (j != k) {
          break label198;
        }
        j = ((String)localObject1).charAt(i);
        k = 120;
        if (j == k)
        {
          localObject2 = "#xpointer(";
          boolean bool4 = ((String)localObject1).startsWith((String)localObject2);
          if (bool4) {
            break label198;
          }
        }
      }
      localLog1 = d;
      bool2 = localLog1.isDebugEnabled();
      String str;
      if (bool2)
      {
        localLog1 = d;
        localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        str = "State I can resolve reference: \"";
        localObject1 = ((StringBuffer)localObject2).append(str).append((String)localObject1);
        localObject2 = "\"";
        localObject1 = (String)localObject2;
        localLog1.debug(localObject1);
      }
      bool2 = i;
      continue;
      label198:
      localLog2 = d;
      boolean bool1 = localLog2.isDebugEnabled();
      if (bool1)
      {
        localLog2 = d;
        localObject2 = new java/lang/StringBuffer;
        ((StringBuffer)localObject2).<init>();
        str = "Do not seem to be able to resolve reference: \"";
        localObject1 = ((StringBuffer)localObject2).append(str).append((String)localObject1);
        localObject2 = "\"";
        localObject1 = (String)localObject2;
        localLog2.debug(localObject1);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/resolver/implementations/ResolverFragment.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */