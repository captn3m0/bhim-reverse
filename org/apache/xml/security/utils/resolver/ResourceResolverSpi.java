package org.apache.xml.security.utils.resolver;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.signature.XMLSignatureInput;
import org.w3c.dom.Attr;

public abstract class ResourceResolverSpi
{
  static Log a;
  static Class c;
  protected Map b = null;
  
  static
  {
    Class localClass = c;
    if (localClass == null)
    {
      localClass = b("org.apache.xml.security.utils.resolver.ResourceResolverSpi");
      c = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      return;
      localClass = c;
    }
  }
  
  static Class b(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public String a(String paramString)
  {
    Object localObject = b;
    if (localObject == null) {}
    for (localObject = null;; localObject = (String)b.get(paramString)) {
      return (String)localObject;
    }
  }
  
  public abstract XMLSignatureInput a(Attr paramAttr, String paramString);
  
  public void a(Map paramMap)
  {
    if (paramMap != null)
    {
      Object localObject = b;
      if (localObject == null)
      {
        localObject = new java/util/HashMap;
        ((HashMap)localObject).<init>();
        b = ((Map)localObject);
      }
      localObject = b;
      ((Map)localObject).putAll(paramMap);
    }
  }
  
  public boolean a()
  {
    return false;
  }
  
  public abstract boolean b(Attr paramAttr, String paramString);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/resolver/ResourceResolverSpi.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */