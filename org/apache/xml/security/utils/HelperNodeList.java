package org.apache.xml.security.utils;

import java.util.ArrayList;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class HelperNodeList
  implements NodeList
{
  ArrayList a;
  boolean b;
  
  public HelperNodeList()
  {
    this(false);
  }
  
  public HelperNodeList(boolean paramBoolean)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(20);
    a = localArrayList;
    b = false;
    b = paramBoolean;
  }
  
  public int getLength()
  {
    return a.size();
  }
  
  public Node item(int paramInt)
  {
    return (Node)a.get(paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/HelperNodeList.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */