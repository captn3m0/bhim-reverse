package org.apache.xml.security.utils;

import java.io.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.SignatureAlgorithm;
import org.apache.xml.security.signature.XMLSignatureException;

public class SignerOutputStream
  extends ByteArrayOutputStream
{
  static Log b;
  static Class c;
  final SignatureAlgorithm a;
  
  static
  {
    Class localClass = c;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.utils.SignerOutputStream");
      c = localClass;
    }
    for (;;)
    {
      b = LogFactory.getLog(localClass.getName());
      return;
      localClass = c;
    }
  }
  
  public SignerOutputStream(SignatureAlgorithm paramSignatureAlgorithm)
  {
    a = paramSignatureAlgorithm;
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public void write(int paramInt)
  {
    try
    {
      SignatureAlgorithm localSignatureAlgorithm = a;
      byte b1 = (byte)paramInt;
      localSignatureAlgorithm.a(b1);
      return;
    }
    catch (XMLSignatureException localXMLSignatureException)
    {
      RuntimeException localRuntimeException = new java/lang/RuntimeException;
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      String str = "" + localXMLSignatureException;
      localRuntimeException.<init>(str);
      throw localRuntimeException;
    }
  }
  
  public void write(byte[] paramArrayOfByte)
  {
    try
    {
      SignatureAlgorithm localSignatureAlgorithm = a;
      localSignatureAlgorithm.a(paramArrayOfByte);
      return;
    }
    catch (XMLSignatureException localXMLSignatureException)
    {
      RuntimeException localRuntimeException = new java/lang/RuntimeException;
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      String str = "" + localXMLSignatureException;
      localRuntimeException.<init>(str);
      throw localRuntimeException;
    }
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    Object localObject1 = b;
    boolean bool = ((Log)localObject1).isDebugEnabled();
    Object localObject2;
    if (bool)
    {
      localObject1 = b;
      ((Log)localObject1).debug("Canonicalized SignedInfo:");
      localObject2 = new java/lang/StringBuffer;
      ((StringBuffer)localObject2).<init>(paramInt2);
      int i = paramInt1;
      for (;;)
      {
        int j = paramInt1 + paramInt2;
        if (i >= j) {
          break;
        }
        j = (char)paramArrayOfByte[i];
        ((StringBuffer)localObject2).append(j);
        i += 1;
      }
      localObject1 = b;
      localObject2 = ((StringBuffer)localObject2).toString();
      ((Log)localObject1).debug(localObject2);
    }
    try
    {
      localObject1 = a;
      ((SignatureAlgorithm)localObject1).a(paramArrayOfByte, paramInt1, paramInt2);
      return;
    }
    catch (XMLSignatureException localXMLSignatureException)
    {
      localObject2 = new java/lang/RuntimeException;
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      String str = "" + localXMLSignatureException;
      ((RuntimeException)localObject2).<init>(str);
      throw ((Throwable)localObject2);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/SignerOutputStream.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */