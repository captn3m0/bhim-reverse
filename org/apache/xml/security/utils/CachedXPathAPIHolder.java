package org.apache.xml.security.utils;

import org.apache.xpath.CachedXPathAPI;
import org.apache.xpath.XPathContext;
import org.w3c.dom.Document;

public class CachedXPathAPIHolder
{
  static ThreadLocal a;
  static ThreadLocal b;
  
  static
  {
    ThreadLocal localThreadLocal = new java/lang/ThreadLocal;
    localThreadLocal.<init>();
    a = localThreadLocal;
    localThreadLocal = new java/lang/ThreadLocal;
    localThreadLocal.<init>();
    b = localThreadLocal;
  }
  
  public static CachedXPathAPI a()
  {
    CachedXPathAPI localCachedXPathAPI = (CachedXPathAPI)a.get();
    if (localCachedXPathAPI == null)
    {
      localCachedXPathAPI = new org/apache/xpath/CachedXPathAPI;
      localCachedXPathAPI.<init>();
      a.set(localCachedXPathAPI);
      ThreadLocal localThreadLocal = b;
      localThreadLocal.set(null);
    }
    return localCachedXPathAPI;
  }
  
  public static void a(Document paramDocument)
  {
    Object localObject = b.get();
    if (localObject != paramDocument)
    {
      localObject = (CachedXPathAPI)a.get();
      if (localObject != null) {
        break label53;
      }
      localObject = new org/apache/xpath/CachedXPathAPI;
      ((CachedXPathAPI)localObject).<init>();
      ThreadLocal localThreadLocal = a;
      localThreadLocal.set(localObject);
      localObject = b;
      ((ThreadLocal)localObject).set(paramDocument);
    }
    for (;;)
    {
      return;
      label53:
      ((CachedXPathAPI)localObject).getXPathContext().reset();
      localObject = b;
      ((ThreadLocal)localObject).set(paramDocument);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/CachedXPathAPIHolder.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */