package org.apache.xml.security.utils;

public final class ClassLoaderUtils
{
  static Class a;
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public static Class a(String paramString, Class paramClass)
  {
    try
    {
      localObject = Thread.currentThread();
      localObject = ((Thread)localObject).getContextClassLoader();
      if (localObject == null) {
        break label22;
      }
      localObject = ((ClassLoader)localObject).loadClass(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      for (;;)
      {
        Object localObject;
        label22:
        Class localClass = b(paramString, paramClass);
      }
    }
    return (Class)localObject;
  }
  
  private static Class b(String paramString, Class paramClass)
  {
    try
    {
      localObject1 = Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException2)
    {
      try
      {
        Object localObject1 = a;
        if (localObject1 != null) {
          break label71;
        }
        localObject1 = "org.apache.xml.security.utils.ClassLoaderUtils";
        localObject1 = a((String)localObject1);
        a = (Class)localObject1;
        label28:
        localObject1 = ((Class)localObject1).getClassLoader();
        if (localObject1 == null) {
          break label111;
        }
        localObject1 = a;
        if (localObject1 != null) {
          break label78;
        }
        localObject1 = "org.apache.xml.security.utils.ClassLoaderUtils";
        localObject1 = a((String)localObject1);
        a = (Class)localObject1;
        for (;;)
        {
          localObject1 = ((Class)localObject1).getClassLoader();
          localObject1 = ((ClassLoader)localObject1).loadClass(paramString);
          break;
          label71:
          localObject1 = a;
          break label28;
          label78:
          localObject1 = a;
        }
      }
      catch (ClassNotFoundException localClassNotFoundException1)
      {
        while (paramClass != null)
        {
          Object localObject2 = paramClass.getClassLoader();
          if (localObject2 == null) {
            break;
          }
          localObject2 = paramClass.getClassLoader().loadClass(paramString);
        }
        label111:
        throw localClassNotFoundException2;
      }
    }
    return (Class)localObject1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/ClassLoaderUtils.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */