package org.apache.xml.security.utils;

import java.io.OutputStream;

public class UnsyncByteArrayOutputStream
  extends OutputStream
{
  private static ThreadLocal a;
  private byte[] b;
  private int c = 8192;
  private int d = 0;
  
  static
  {
    UnsyncByteArrayOutputStream.1 local1 = new org/apache/xml/security/utils/UnsyncByteArrayOutputStream$1;
    local1.<init>();
    a = local1;
  }
  
  public UnsyncByteArrayOutputStream()
  {
    byte[] arrayOfByte = (byte[])a.get();
    b = arrayOfByte;
  }
  
  private void a(int paramInt)
  {
    int i = c;
    while (paramInt > i) {
      i <<= 2;
    }
    byte[] arrayOfByte1 = new byte[i];
    byte[] arrayOfByte2 = b;
    int j = d;
    System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, j);
    b = arrayOfByte1;
    c = i;
  }
  
  public byte[] a()
  {
    byte[] arrayOfByte1 = new byte[d];
    byte[] arrayOfByte2 = b;
    int i = d;
    System.arraycopy(arrayOfByte2, 0, arrayOfByte1, 0, i);
    return arrayOfByte1;
  }
  
  public void b()
  {
    d = 0;
  }
  
  public void write(int paramInt)
  {
    int i = d + 1;
    int j = c;
    if (i > j) {
      a(i);
    }
    byte[] arrayOfByte = b;
    j = d;
    int k = j + 1;
    d = k;
    k = (byte)paramInt;
    arrayOfByte[j] = k;
  }
  
  public void write(byte[] paramArrayOfByte)
  {
    int i = d;
    int j = paramArrayOfByte.length;
    i += j;
    j = c;
    if (i > j) {
      a(i);
    }
    byte[] arrayOfByte = b;
    int k = d;
    int m = paramArrayOfByte.length;
    System.arraycopy(paramArrayOfByte, 0, arrayOfByte, k, m);
    d = i;
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = d + paramInt2;
    int j = c;
    if (i > j) {
      a(i);
    }
    byte[] arrayOfByte = b;
    int k = d;
    System.arraycopy(paramArrayOfByte, paramInt1, arrayOfByte, k, paramInt2);
    d = i;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/UnsyncByteArrayOutputStream.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */