package org.apache.xml.security.utils;

import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

public class XMLUtils
{
  static String a;
  static String b = null;
  private static boolean c;
  private static Map d;
  
  static
  {
    Object localObject = new org/apache/xml/security/utils/XMLUtils$1;
    ((XMLUtils.1)localObject).<init>();
    c = ((Boolean)AccessController.doPrivileged((PrivilegedAction)localObject)).booleanValue();
    a = null;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    d = Collections.synchronizedMap((Map)localObject);
  }
  
  public static String a(Element paramElement)
  {
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    localStringBuffer.<init>();
    NodeList localNodeList = paramElement.getChildNodes();
    int i = localNodeList.getLength();
    int j = 0;
    Object localObject = null;
    for (int k = 0; k < i; k = j)
    {
      localObject = localNodeList.item(k);
      int m = ((Node)localObject).getNodeType();
      int n = 3;
      if (m == n)
      {
        localObject = ((Text)localObject).getData();
        localStringBuffer.append((String)localObject);
      }
      j = k + 1;
    }
    return localStringBuffer.toString();
  }
  
  public static Document a(Set paramSet)
  {
    Object localObject1 = null;
    Object localObject2 = paramSet.iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject2).hasNext();
      if (bool)
      {
        localObject1 = (Node)((Iterator)localObject2).next();
        int i = ((Node)localObject1).getNodeType();
        int j = 9;
        if (i == j) {}
        for (localObject1 = (Document)localObject1;; localObject1 = ((Node)localObject1).getOwnerDocument()) {
          for (;;)
          {
            return (Document)localObject1;
            j = 2;
            if (i == j) {}
            StringBuffer localStringBuffer;
            String str;
            try
            {
              localObject1 = (Attr)localObject1;
              localObject1 = ((Attr)localObject1).getOwnerElement();
              localObject1 = ((Element)localObject1).getOwnerDocument();
            }
            catch (NullPointerException localNullPointerException) {}
          }
        }
      }
      localObject2 = new java/lang/NullPointerException;
      localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      str = I18n.a("endorsed.jdk1.4.0");
      localStringBuffer = localStringBuffer.append(str);
      str = " Original message was \"";
      localStringBuffer = localStringBuffer.append(str);
      if (localObject1 == null) {}
      for (localObject1 = "";; localObject1 = ((NullPointerException)localObject1).getMessage())
      {
        localObject1 = (String)localObject1 + "\"";
        ((NullPointerException)localObject2).<init>((String)localObject1);
        throw ((Throwable)localObject2);
      }
    }
  }
  
  public static Element a(Document paramDocument, String paramString)
  {
    if (paramDocument == null)
    {
      localObject1 = new java/lang/RuntimeException;
      ((RuntimeException)localObject1).<init>("Document is null");
      throw ((Throwable)localObject1);
    }
    Object localObject1 = a;
    if (localObject1 != null)
    {
      localObject1 = a;
      int i = ((String)localObject1).length();
      if (i != 0) {
        break label49;
      }
    }
    label49:
    Object localObject2;
    for (localObject1 = paramDocument.createElementNS("http://www.w3.org/2000/09/xmldsig#", paramString);; localObject1 = paramDocument.createElementNS((String)localObject2, (String)localObject1))
    {
      return (Element)localObject1;
      localObject1 = (String)d.get(paramString);
      if (localObject1 == null)
      {
        localObject1 = new java/lang/StringBuffer;
        localObject2 = a;
        ((StringBuffer)localObject1).<init>((String)localObject2);
        char c1 = ':';
        ((StringBuffer)localObject1).append(c1);
        ((StringBuffer)localObject1).append(paramString);
        localObject1 = ((StringBuffer)localObject1).toString();
        localObject2 = d;
        ((Map)localObject2).put(paramString, localObject1);
      }
      localObject2 = "http://www.w3.org/2000/09/xmldsig#";
    }
  }
  
  public static Element a(Node paramNode)
  {
    for (Node localNode = paramNode; localNode != null; localNode = localNode.getNextSibling())
    {
      int i = localNode.getNodeType();
      int j = 1;
      if (i == j) {
        break;
      }
    }
    return (Element)localNode;
  }
  
  public static Element a(Node paramNode, String paramString, int paramInt)
  {
    int i = paramInt;
    Object localObject = paramNode;
    if (localObject != null)
    {
      String str1 = "http://www.w3.org/2000/09/xmldsig#";
      String str2 = ((Node)localObject).getNamespaceURI();
      boolean bool = str1.equals(str2);
      if (bool)
      {
        str1 = ((Node)localObject).getLocalName();
        bool = str1.equals(paramString);
        if (bool) {
          if (i != 0) {}
        }
      }
    }
    for (localObject = (Element)localObject;; localObject = null)
    {
      return (Element)localObject;
      i += -1;
      localObject = ((Node)localObject).getNextSibling();
      break;
    }
  }
  
  public static Element a(Node paramNode, String paramString1, String paramString2, int paramInt)
  {
    int i = paramInt;
    Object localObject = paramNode;
    if (localObject != null)
    {
      String str = ((Node)localObject).getNamespaceURI();
      if (str != null)
      {
        str = ((Node)localObject).getNamespaceURI();
        boolean bool = str.equals(paramString1);
        if (bool)
        {
          str = ((Node)localObject).getLocalName();
          bool = str.equals(paramString2);
          if (bool) {
            if (i != 0) {}
          }
        }
      }
    }
    for (localObject = (Element)localObject;; localObject = null)
    {
      return (Element)localObject;
      i += -1;
      localObject = ((Node)localObject).getNextSibling();
      break;
    }
  }
  
  public static void a(Document paramDocument)
  {
    Element localElement = paramDocument.getDocumentElement();
    String str1 = "xmlns";
    Object localObject = localElement.getAttributeNodeNS("http://www.w3.org/2000/xmlns/", str1);
    if (localObject == null)
    {
      localObject = "http://www.w3.org/2000/xmlns/";
      str1 = "xmlns";
      String str2 = "";
      localElement.setAttributeNS((String)localObject, str1, str2);
    }
    c(paramDocument);
  }
  
  public static void a(Node paramNode1, Set paramSet, Node paramNode2, boolean paramBoolean)
  {
    if (paramNode2 != null)
    {
      boolean bool = a(paramNode2, paramNode1);
      if (!bool) {}
    }
    for (;;)
    {
      return;
      b(paramNode1, paramSet, paramNode2, paramBoolean);
    }
  }
  
  public static boolean a()
  {
    return c;
  }
  
  public static boolean a(Node paramNode1, Node paramNode2)
  {
    boolean bool1 = true;
    boolean bool2;
    if (paramNode1 == paramNode2)
    {
      bool2 = bool1;
      return bool2;
    }
    Object localObject = paramNode2;
    for (;;)
    {
      if (localObject == null)
      {
        bool2 = false;
        localObject = null;
        break;
      }
      if (localObject == paramNode1)
      {
        bool2 = bool1;
        break;
      }
      int i = ((Node)localObject).getNodeType();
      int j = 2;
      if (i == j) {
        localObject = ((Attr)localObject).getOwnerElement();
      } else {
        localObject = ((Node)localObject).getParentNode();
      }
    }
  }
  
  public static Element[] a(Node paramNode, String paramString)
  {
    return a(paramNode, "http://www.w3.org/2000/09/xmldsig#", paramString);
  }
  
  public static Element[] a(Node paramNode, String paramString1, String paramString2)
  {
    int i = 20;
    Object localObject1 = new Element[i];
    int j = 0;
    Node localNode = paramNode;
    Object localObject2;
    int k;
    int m;
    Object localObject3;
    if (localNode != null)
    {
      localObject2 = localNode.getNamespaceURI();
      if (localObject2 == null) {
        break label196;
      }
      localObject2 = localNode.getNamespaceURI();
      k = ((String)localObject2).equals(paramString1);
      if (k == 0) {
        break label196;
      }
      localObject2 = localNode.getLocalName();
      k = ((String)localObject2).equals(paramString2);
      if (k == 0) {
        break label196;
      }
      m = j + 1;
      localObject2 = localNode;
      localObject2 = (Element)localNode;
      localObject1[j] = localObject2;
      if (i > m) {
        break label182;
      }
      j = i << 2;
      localObject2 = new Element[j];
      System.arraycopy(localObject1, 0, localObject2, 0, i);
      localObject3 = localObject2;
      k = m;
    }
    for (;;)
    {
      localNode = localNode.getNextSibling();
      localObject1 = localObject3;
      i = j;
      j = k;
      break;
      localObject2 = new Element[j];
      System.arraycopy(localObject1, 0, localObject2, 0, j);
      return (Element[])localObject2;
      label182:
      k = m;
      j = i;
      localObject3 = localObject1;
      continue;
      label196:
      k = j;
      localObject3 = localObject1;
      j = i;
    }
  }
  
  public static Document b(Node paramNode)
  {
    int i = paramNode.getNodeType();
    int j = 9;
    if (i == j) {
      paramNode = (Document)paramNode;
    }
    for (;;)
    {
      return paramNode;
      try
      {
        paramNode = paramNode.getOwnerDocument();
      }
      catch (NullPointerException localNullPointerException1)
      {
        NullPointerException localNullPointerException2 = new java/lang/NullPointerException;
        StringBuffer localStringBuffer = new java/lang/StringBuffer;
        localStringBuffer.<init>();
        String str2 = I18n.a("endorsed.jdk1.4.0");
        localStringBuffer = localStringBuffer.append(str2).append(" Original message was \"");
        String str1 = localNullPointerException1.getMessage();
        str1 = str1 + "\"";
        localNullPointerException2.<init>(str1);
        throw localNullPointerException2;
      }
    }
  }
  
  public static Text b(Node paramNode, String paramString, int paramInt)
  {
    Object localObject = a(paramNode, paramString, paramInt);
    if (localObject == null) {}
    for (localObject = null;; localObject = (Text)localObject)
    {
      return (Text)localObject;
      for (localObject = ((Node)localObject).getFirstChild(); localObject != null; localObject = ((Node)localObject).getNextSibling())
      {
        int i = ((Node)localObject).getNodeType();
        int j = 3;
        if (i == j) {
          break;
        }
      }
    }
  }
  
  public static void b(Element paramElement)
  {
    boolean bool = c;
    if (!bool)
    {
      Object localObject = paramElement.getOwnerDocument();
      String str = "\n";
      localObject = ((Document)localObject).createTextNode(str);
      paramElement.appendChild((Node)localObject);
    }
  }
  
  static final void b(Node paramNode1, Set paramSet, Node paramNode2, boolean paramBoolean)
  {
    int i = 3;
    if (paramNode1 == paramNode2) {}
    for (;;)
    {
      return;
      int j = paramNode1.getNodeType();
      Object localObject;
      switch (j)
      {
      case 10: 
      default: 
        paramSet.add(paramNode1);
        break;
      case 1: 
        paramSet.add(paramNode1);
        localObject = paramNode1;
        localObject = (Element)paramNode1;
        int k = ((Element)localObject).hasAttributes();
        if (k != 0)
        {
          localObject = paramNode1;
          NamedNodeMap localNamedNodeMap = ((Element)paramNode1).getAttributes();
          k = 0;
          localObject = null;
          for (;;)
          {
            int n = localNamedNodeMap.getLength();
            if (k >= n) {
              break;
            }
            Node localNode = localNamedNodeMap.item(k);
            paramSet.add(localNode);
            int m;
            k += 1;
          }
        }
      case 9: 
        for (localObject = paramNode1.getFirstChild(); localObject != null; localObject = ((Node)localObject).getNextSibling())
        {
          int i1 = ((Node)localObject).getNodeType();
          if (i1 == i)
          {
            paramSet.add(localObject);
            while (localObject != null)
            {
              i1 = ((Node)localObject).getNodeType();
              if (i1 != i) {
                break;
              }
              localObject = ((Node)localObject).getNextSibling();
            }
            if (localObject == null) {
              break;
            }
          }
          b((Node)localObject, paramSet, paramNode2, paramBoolean);
        }
      case 8: 
        if (paramBoolean) {
          paramSet.add(paramNode1);
        }
        break;
      }
    }
  }
  
  private static void c(Node paramNode)
  {
    int i = 0;
    Object localObject1 = null;
    boolean bool1 = false;
    Object localObject2 = null;
    int j = 0;
    Object localObject3 = null;
    for (Object localObject4 = paramNode;; localObject4 = paramNode)
    {
      i = ((Node)localObject4).getNodeType();
      switch (i)
      {
      default: 
        paramNode = (Node)localObject2;
        localObject4 = localObject3;
      }
      while ((paramNode == null) && (localObject4 != null))
      {
        localObject2 = ((Node)localObject4).getNextSibling();
        localObject4 = ((Node)localObject4).getParentNode();
        paramNode = (Node)localObject2;
        continue;
        localObject1 = localObject4;
        localObject1 = (Element)localObject4;
        int k = ((Element)localObject1).hasChildNodes();
        if (k == 0)
        {
          paramNode = (Node)localObject2;
          localObject4 = localObject3;
        }
        else
        {
          bool1 = ((Element)localObject1).hasAttributes();
          if (bool1)
          {
            NamedNodeMap localNamedNodeMap = ((Element)localObject1).getAttributes();
            int m = localNamedNodeMap.getLength();
            localObject2 = ((Element)localObject1).getFirstChild();
            while (localObject2 != null)
            {
              i = ((Node)localObject2).getNodeType();
              j = 1;
              if (i != j)
              {
                localObject2 = ((Node)localObject2).getNextSibling();
              }
              else
              {
                localObject1 = localObject2;
                localObject1 = (Element)localObject2;
                j = 0;
                localObject3 = null;
                k = 0;
                label220:
                String str1;
                String str2;
                boolean bool2;
                if (k < m)
                {
                  localObject3 = (Attr)localNamedNodeMap.item(k);
                  str1 = "http://www.w3.org/2000/xmlns/";
                  str2 = ((Attr)localObject3).getNamespaceURI();
                  bool2 = str1.equals(str2);
                  if (bool2) {
                    break label281;
                  }
                }
                for (;;)
                {
                  j = k + 1;
                  k = j;
                  break label220;
                  break;
                  label281:
                  str1 = "http://www.w3.org/2000/xmlns/";
                  str2 = ((Attr)localObject3).getLocalName();
                  bool2 = ((Element)localObject1).hasAttributeNS(str1, str2);
                  if (!bool2)
                  {
                    str1 = "http://www.w3.org/2000/xmlns/";
                    str2 = ((Attr)localObject3).getName();
                    localObject3 = ((Attr)localObject3).getNodeValue();
                    ((Element)localObject1).setAttributeNS(str1, str2, (String)localObject3);
                  }
                }
              }
            }
          }
          localObject2 = ((Node)localObject4).getFirstChild();
          paramNode = (Node)localObject2;
        }
      }
      if (paramNode == null) {
        return;
      }
      localObject1 = paramNode.getNextSibling();
      localObject2 = localObject1;
      localObject3 = localObject4;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/XMLUtils.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */