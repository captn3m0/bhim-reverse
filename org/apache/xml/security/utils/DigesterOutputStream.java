package org.apache.xml.security.utils;

import java.io.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.security.algorithms.MessageDigestAlgorithm;

public class DigesterOutputStream
  extends ByteArrayOutputStream
{
  static Log b;
  static Class c;
  final MessageDigestAlgorithm a;
  
  static
  {
    Class localClass = c;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.utils.DigesterOutputStream");
      c = localClass;
    }
    for (;;)
    {
      b = LogFactory.getLog(localClass.getName());
      return;
      localClass = c;
    }
  }
  
  public DigesterOutputStream(MessageDigestAlgorithm paramMessageDigestAlgorithm)
  {
    a = paramMessageDigestAlgorithm;
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public byte[] a()
  {
    return a.b();
  }
  
  public void write(int paramInt)
  {
    MessageDigestAlgorithm localMessageDigestAlgorithm = a;
    byte b1 = (byte)paramInt;
    localMessageDigestAlgorithm.a(b1);
  }
  
  public void write(byte[] paramArrayOfByte)
  {
    int i = paramArrayOfByte.length;
    write(paramArrayOfByte, 0, i);
  }
  
  public void write(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    Log localLog = b;
    boolean bool = localLog.isDebugEnabled();
    if (bool)
    {
      localLog = b;
      localLog.debug("Pre-digested input:");
      Object localObject = new java/lang/StringBuffer;
      ((StringBuffer)localObject).<init>(paramInt2);
      int i = paramInt1;
      for (;;)
      {
        int j = paramInt1 + paramInt2;
        if (i >= j) {
          break;
        }
        j = (char)paramArrayOfByte[i];
        ((StringBuffer)localObject).append(j);
        i += 1;
      }
      localLog = b;
      localObject = ((StringBuffer)localObject).toString();
      localLog.debug(localObject);
    }
    a.a(paramArrayOfByte, paramInt1, paramInt2);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/DigesterOutputStream.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */