package org.apache.xml.security.utils;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import javax.xml.transform.TransformerException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xml.dtm.DTMManager;
import org.apache.xml.security.transforms.implementations.FuncHere;
import org.apache.xml.security.transforms.implementations.FuncHereContext;
import org.apache.xml.utils.PrefixResolver;
import org.apache.xml.utils.PrefixResolverDefault;
import org.apache.xpath.CachedXPathAPI;
import org.apache.xpath.XPath;
import org.apache.xpath.XPathContext;
import org.apache.xpath.compiler.FunctionTable;
import org.apache.xpath.objects.XObject;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;

public class CachedXPathFuncHereAPI
{
  static Log a;
  static FunctionTable g;
  static Class h;
  static Class i;
  static Class j;
  static Class k;
  static Class l;
  static Class m;
  static Class n;
  static Class o;
  static Class p;
  static Class q;
  FuncHereContext b = null;
  DTMManager c = null;
  XPathContext d = null;
  String e = null;
  XPath f = null;
  
  static
  {
    Class localClass = h;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.utils.CachedXPathFuncHereAPI");
      h = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      g = null;
      a();
      return;
      localClass = h;
    }
  }
  
  private CachedXPathFuncHereAPI() {}
  
  public CachedXPathFuncHereAPI(CachedXPathAPI paramCachedXPathAPI)
  {
    Object localObject = paramCachedXPathAPI.getXPathContext().getDTMManager();
    c = ((DTMManager)localObject);
    localObject = paramCachedXPathAPI.getXPathContext();
    d = ((XPathContext)localObject);
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public static String a(Node paramNode)
  {
    int i1 = 3;
    int i2 = paramNode.getNodeType();
    Object localObject;
    if (i2 == i1)
    {
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      localObject = paramNode.getParentNode();
      for (Node localNode = ((Node)localObject).getFirstChild(); localNode != null; localNode = localNode.getNextSibling())
      {
        i2 = localNode.getNodeType();
        if (i2 == i1)
        {
          localObject = localNode;
          localObject = ((Text)localNode).getData();
          localStringBuffer.append((String)localObject);
        }
      }
      localObject = localStringBuffer.toString();
    }
    for (;;)
    {
      return (String)localObject;
      i2 = paramNode.getNodeType();
      int i3 = 2;
      if (i2 == i3)
      {
        paramNode = (Attr)paramNode;
        localObject = paramNode.getNodeValue();
      }
      else
      {
        i2 = paramNode.getNodeType();
        i3 = 7;
        if (i2 == i3)
        {
          paramNode = (ProcessingInstruction)paramNode;
          localObject = paramNode.getNodeValue();
        }
        else
        {
          i2 = 0;
          localObject = null;
        }
      }
    }
  }
  
  private XPath a(String paramString, PrefixResolver paramPrefixResolver)
  {
    int i1 = 3;
    int i2 = 2;
    int i3 = 1;
    i4 = 6;
    Object localObject1 = new Class[i4];
    Object localObject2 = i;
    label69:
    label98:
    label141:
    label173:
    Object localObject5;
    FunctionTable localFunctionTable;
    if (localObject2 == null)
    {
      localObject2 = a("java.lang.String");
      i = (Class)localObject2;
      localObject1[0] = localObject2;
      localObject2 = j;
      if (localObject2 != null) {
        break label335;
      }
      localObject2 = a("javax.xml.transform.SourceLocator");
      j = (Class)localObject2;
      localObject1[i3] = localObject2;
      localObject2 = k;
      if (localObject2 != null) {
        break label343;
      }
      localObject2 = a("org.apache.xml.utils.PrefixResolver");
      k = (Class)localObject2;
      localObject1[i2] = localObject2;
      localObject2 = Integer.TYPE;
      localObject1[i1] = localObject2;
      int i5 = 4;
      localObject2 = l;
      if (localObject2 != null) {
        break label351;
      }
      localObject2 = a("javax.xml.transform.ErrorListener");
      l = (Class)localObject2;
      localObject1[i5] = localObject2;
      i5 = 5;
      localObject2 = m;
      if (localObject2 != null) {
        break label359;
      }
      localObject2 = a("org.apache.xpath.compiler.FunctionTable");
      m = (Class)localObject2;
      localObject1[i5] = localObject2;
      localObject5 = new Object[6];
      localObject5[0] = paramString;
      localObject5[i3] = null;
      localObject5[i2] = paramPrefixResolver;
      localObject2 = new java/lang/Integer;
      ((Integer)localObject2).<init>(0);
      localObject5[i1] = localObject2;
      localObject5[4] = null;
      i4 = 5;
      localFunctionTable = g;
      localObject5[i4] = localFunctionTable;
    }
    for (;;)
    {
      try
      {
        localObject2 = n;
        if (localObject2 != null) {
          continue;
        }
        localObject2 = "org.apache.xpath.XPath";
        localObject2 = a((String)localObject2);
        n = (Class)localObject2;
        localObject2 = ((Class)localObject2).getConstructor((Class[])localObject1);
        localObject2 = ((Constructor)localObject2).newInstance((Object[])localObject5);
        localObject2 = (XPath)localObject2;
      }
      finally
      {
        label335:
        label343:
        label351:
        label359:
        i4 = 0;
        Object localObject4 = null;
        continue;
      }
      if (localObject2 == null)
      {
        localObject2 = new org/apache/xpath/XPath;
        localObject1 = paramString;
        localObject5 = paramPrefixResolver;
        i3 = 0;
        localFunctionTable = null;
        ((XPath)localObject2).<init>(paramString, null, paramPrefixResolver, 0, null);
      }
      return (XPath)localObject2;
      localObject2 = i;
      break;
      localObject2 = j;
      break label69;
      localObject2 = k;
      break label98;
      localObject2 = l;
      break label141;
      localObject2 = m;
      break label173;
      localObject2 = n;
    }
  }
  
  private static void a()
  {
    boolean bool1 = false;
    float f1 = 0.0F;
    Object localObject1 = null;
    boolean bool2 = true;
    float f2 = Float.MIN_VALUE;
    Object localObject2 = a;
    Object localObject4 = "Registering Here function";
    ((Log)localObject2).info(localObject4);
    int i1 = 2;
    try
    {
      localObject4 = new Class[i1];
      i2 = 0;
      localObject5 = null;
      localObject2 = i;
      if (localObject2 != null) {
        break label550;
      }
      localObject2 = "java.lang.String";
      localObject2 = a((String)localObject2);
      i = (Class)localObject2;
    }
    finally
    {
      for (;;)
      {
        try
        {
          int i3;
          Object localObject6;
          localObject2 = new org/apache/xpath/compiler/FunctionTable;
          ((FunctionTable)localObject2).<init>();
          g = (FunctionTable)localObject2;
          i1 = 2;
          localObject4 = new Class[i1];
          int i2 = 0;
          Object localObject5 = null;
          localObject2 = i;
          if (localObject2 == null)
          {
            localObject2 = "java.lang.String";
            localObject2 = a((String)localObject2);
            i = (Class)localObject2;
            localObject4[0] = localObject2;
            i2 = 1;
            localObject2 = p;
            if (localObject2 == null)
            {
              localObject2 = "java.lang.Class";
              localObject2 = a((String)localObject2);
              p = (Class)localObject2;
              localObject4[i2] = localObject2;
              localObject2 = m;
              if (localObject2 != null) {
                continue;
              }
              localObject2 = "org.apache.xpath.compiler.FunctionTable";
              localObject2 = a((String)localObject2);
              m = (Class)localObject2;
              localObject5 = "installFunction";
              localObject4 = ((Class)localObject2).getMethod((String)localObject5, (Class[])localObject4);
              i1 = 2;
              localObject5 = new Object[i1];
              i1 = 0;
              localObject2 = null;
              localObject6 = "here";
              localObject5[0] = localObject6;
              int i4 = 1;
              localObject2 = q;
              if (localObject2 != null) {
                continue;
              }
              localObject2 = "org.apache.xml.security.transforms.implementations.FuncHere";
              localObject2 = a((String)localObject2);
              q = (Class)localObject2;
              localObject5[i4] = localObject2;
              localObject2 = g;
              ((Method)localObject4).invoke(localObject2, (Object[])localObject5);
              localObject1 = a;
              bool1 = ((Log)localObject1).isDebugEnabled();
              if (bool1)
              {
                if (!bool2) {
                  break;
                }
                Log localLog1 = a;
                localObject1 = new java/lang/StringBuffer;
                ((StringBuffer)localObject1).<init>();
                localObject2 = ((StringBuffer)localObject1).append("Registered class ");
                localObject1 = q;
                if (localObject1 != null) {
                  break label661;
                }
                localObject1 = a("org.apache.xml.security.transforms.implementations.FuncHere");
                q = (Class)localObject1;
                localObject1 = ((Class)localObject1).getName();
                localObject1 = ((StringBuffer)localObject2).append((String)localObject1);
                localObject2 = " for XPath function 'here()' function in internal table";
                localObject1 = (String)localObject2;
                localLog1.debug(localObject1);
              }
              return;
              label550:
              localObject2 = i;
              continue;
              localObject2 = o;
              continue;
              label566:
              localObject2 = m;
              continue;
              localThrowable1 = finally;
              localObject4 = a;
              localObject5 = "Error installing function using the static installFunction method";
              ((Log)localObject4).debug(localObject5, localThrowable1);
              continue;
            }
          }
          else
          {
            localObject3 = i;
            continue;
          }
          localObject3 = p;
          continue;
          localObject3 = m;
          continue;
          localObject3 = q;
          continue;
          bool2 = bool1;
        }
        finally
        {
          localObject3 = a;
          localObject4 = "Error installing function using the static installFunction method";
          ((Log)localObject3).debug(localObject4, localThrowable2);
        }
        label653:
        f2 = f1;
        continue;
        label661:
        localObject1 = q;
      }
      localLog2 = a;
      localObject1 = new java/lang/StringBuffer;
      ((StringBuffer)localObject1).<init>();
      localObject3 = ((StringBuffer)localObject1).append("Unable to register class ");
      localObject1 = q;
      if (localObject1 != null) {
        break label744;
      }
    }
    localObject4[0] = localObject2;
    i2 = 1;
    localObject2 = o;
    if (localObject2 == null)
    {
      localObject2 = "org.apache.xpath.Expression";
      localObject2 = a((String)localObject2);
      o = (Class)localObject2;
      localObject4[i2] = localObject2;
      localObject2 = m;
      if (localObject2 != null) {
        break label566;
      }
      localObject2 = "org.apache.xpath.compiler.FunctionTable";
      localObject2 = a((String)localObject2);
      m = (Class)localObject2;
      localObject5 = "installFunction";
      localObject2 = ((Class)localObject2).getMethod((String)localObject5, (Class[])localObject4);
      i3 = ((Method)localObject2).getModifiers() & 0x8;
      if (i3 != 0)
      {
        i3 = 2;
        localObject4 = new Object[i3];
        i2 = 0;
        localObject5 = null;
        localObject6 = "here";
        localObject4[0] = localObject6;
        i2 = 1;
        localObject6 = new org/apache/xml/security/transforms/implementations/FuncHere;
        ((FuncHere)localObject6).<init>();
        localObject4[i2] = localObject6;
        i2 = 0;
        localObject5 = null;
        ((Method)localObject2).invoke(null, (Object[])localObject4);
        bool1 = bool2;
        f1 = f2;
      }
      if (bool1) {
        break label653;
      }
    }
    Object localObject3;
    Log localLog2;
    localObject1 = a("org.apache.xml.security.transforms.implementations.FuncHere");
    q = (Class)localObject1;
    for (;;)
    {
      localObject1 = ((Class)localObject1).getName();
      localObject1 = ((StringBuffer)localObject3).append((String)localObject1);
      localObject3 = " for XPath function 'here()' function in internal table";
      localObject1 = (String)localObject3;
      localLog2.debug(localObject1);
      break;
      label744:
      localObject1 = q;
    }
  }
  
  public XObject a(Node paramNode1, Node paramNode2, String paramString, PrefixResolver paramPrefixResolver)
  {
    Object localObject1 = e;
    int i1;
    if (paramString != localObject1)
    {
      localObject1 = "here()";
      i1 = paramString.indexOf((String)localObject1);
      if (i1 > 0)
      {
        d.reset();
        localObject1 = d.getDTMManager();
        c = ((DTMManager)localObject1);
      }
    }
    try
    {
      localObject1 = a(paramString, paramPrefixResolver);
      f = ((XPath)localObject1);
      e = paramString;
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject1 = new org/apache/xml/security/transforms/implementations/FuncHereContext;
        localObject2 = c;
        ((FuncHereContext)localObject1).<init>(paramNode2, (DTMManager)localObject2);
        b = ((FuncHereContext)localObject1);
      }
      i1 = b.getDTMHandleFromNode(paramNode1);
      localObject2 = f;
      localObject3 = b;
      return ((XPath)localObject2).execute((XPathContext)localObject3, i1, paramPrefixResolver);
    }
    catch (TransformerException localTransformerException)
    {
      Object localObject3;
      Object localObject2 = localTransformerException.getCause();
      boolean bool = localObject2 instanceof ClassNotFoundException;
      String str1;
      if (bool)
      {
        localObject2 = ((Throwable)localObject2).getMessage();
        localObject3 = "FuncHere";
        int i2 = ((String)localObject2).indexOf((String)localObject3);
        if (i2 > 0)
        {
          localObject2 = new java/lang/RuntimeException;
          localObject3 = new java/lang/StringBuffer;
          ((StringBuffer)localObject3).<init>();
          String str2 = I18n.a("endorsed.jdk1.4.0");
          str1 = str2 + localTransformerException;
          ((RuntimeException)localObject2).<init>(str1);
          throw ((Throwable)localObject2);
        }
      }
      throw str1;
    }
  }
  
  public NodeList a(Node paramNode1, Node paramNode2, String paramString, Node paramNode3)
  {
    return b(paramNode1, paramNode2, paramString, paramNode3).nodelist();
  }
  
  public XObject b(Node paramNode1, Node paramNode2, String paramString, Node paramNode3)
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new org/apache/xml/security/transforms/implementations/FuncHereContext;
      localObject2 = c;
      ((FuncHereContext)localObject1).<init>(paramNode2, (DTMManager)localObject2);
      b = ((FuncHereContext)localObject1);
    }
    localObject1 = new org/apache/xml/utils/PrefixResolverDefault;
    int i1 = paramNode3.getNodeType();
    int i2 = 9;
    if (i1 == i2) {
      paramNode3 = ((Document)paramNode3).getDocumentElement();
    }
    ((PrefixResolverDefault)localObject1).<init>(paramNode3);
    Object localObject2 = e;
    if (paramString != localObject2)
    {
      localObject2 = "here()";
      i1 = paramString.indexOf((String)localObject2);
      if (i1 > 0)
      {
        d.reset();
        localObject2 = d.getDTMManager();
        c = ((DTMManager)localObject2);
      }
      localObject2 = a(paramString, (PrefixResolver)localObject1);
      f = ((XPath)localObject2);
      e = paramString;
    }
    i1 = b.getDTMHandleFromNode(paramNode1);
    XPath localXPath = f;
    FuncHereContext localFuncHereContext = b;
    return localXPath.execute(localFuncHereContext, i1, (PrefixResolver)localObject1);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/CachedXPathFuncHereAPI.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */