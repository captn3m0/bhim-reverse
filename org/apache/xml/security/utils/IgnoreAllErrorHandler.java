package org.apache.xml.security.utils;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

public class IgnoreAllErrorHandler
  implements ErrorHandler
{
  static Log a;
  static final boolean b;
  static final boolean c;
  static Class d;
  
  static
  {
    Class localClass = d;
    if (localClass == null)
    {
      localClass = a("org.apache.xml.security.utils.IgnoreAllErrorHandler");
      d = localClass;
    }
    for (;;)
    {
      a = LogFactory.getLog(localClass.getName());
      b = System.getProperty("org.apache.xml.security.test.warn.on.exceptions", "false").equals("true");
      c = System.getProperty("org.apache.xml.security.test.throw.exceptions", "false").equals("true");
      return;
      localClass = d;
    }
  }
  
  static Class a(String paramString)
  {
    try
    {
      return Class.forName(paramString);
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      NoClassDefFoundError localNoClassDefFoundError = new java/lang/NoClassDefFoundError;
      localNoClassDefFoundError.<init>();
      throw localNoClassDefFoundError.initCause(localClassNotFoundException);
    }
  }
  
  public void error(SAXParseException paramSAXParseException)
  {
    boolean bool = b;
    if (bool)
    {
      Log localLog = a;
      String str = "";
      localLog.error(str, paramSAXParseException);
    }
    bool = c;
    if (bool) {
      throw paramSAXParseException;
    }
  }
  
  public void fatalError(SAXParseException paramSAXParseException)
  {
    boolean bool = b;
    if (bool)
    {
      Log localLog = a;
      String str = "";
      localLog.warn(str, paramSAXParseException);
    }
    bool = c;
    if (bool) {
      throw paramSAXParseException;
    }
  }
  
  public void warning(SAXParseException paramSAXParseException)
  {
    boolean bool = b;
    if (bool)
    {
      Log localLog = a;
      String str = "";
      localLog.warn(str, paramSAXParseException);
    }
    bool = c;
    if (bool) {
      throw paramSAXParseException;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/IgnoreAllErrorHandler.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */