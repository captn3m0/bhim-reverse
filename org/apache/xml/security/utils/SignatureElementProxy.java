package org.apache.xml.security.utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public abstract class SignatureElementProxy
  extends ElementProxy
{
  protected SignatureElementProxy() {}
  
  public SignatureElementProxy(Document paramDocument)
  {
    if (paramDocument == null)
    {
      localObject = new java/lang/RuntimeException;
      ((RuntimeException)localObject).<init>("Document is null");
      throw ((Throwable)localObject);
    }
    m = paramDocument;
    Object localObject = m;
    String str = e();
    localObject = XMLUtils.a((Document)localObject, str);
    k = ((Element)localObject);
  }
  
  public SignatureElementProxy(Element paramElement, String paramString)
  {
    super(paramElement, paramString);
  }
  
  public String d()
  {
    return "http://www.w3.org/2000/09/xmldsig#";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/apache/xml/security/utils/SignatureElementProxy.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */