package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.widget.c;
import org.npci.upi.security.pinactivitycomponent.widget.o;

public class b
  extends h
  implements o
{
  private static final String ap = b.class.getSimpleName();
  private HashMap aq;
  private int ar;
  private boolean as;
  private ViewSwitcher at;
  
  public b()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    aq = localHashMap;
    ar = 0;
    as = false;
    at = null;
  }
  
  private void M()
  {
    int i = 1;
    int j = 0;
    HashMap localHashMap = null;
    int k = g;
    int i2 = -1;
    Object localObject1;
    Object localObject2;
    int i3;
    if (k != i2)
    {
      localObject1 = f;
      i2 = g;
      localObject1 = ((ArrayList)localObject1).get(i2);
      boolean bool1 = localObject1 instanceof org.npci.upi.security.pinactivitycomponent.widget.b;
      if (bool1)
      {
        localObject1 = f;
        i2 = g;
        localObject1 = (org.npci.upi.security.pinactivitycomponent.widget.b)((ArrayList)localObject1).get(i2);
        localObject2 = ((org.npci.upi.security.pinactivitycomponent.widget.b)localObject1).getInputValue();
        if (localObject2 != null)
        {
          i2 = ((String)localObject2).length();
          i3 = ((org.npci.upi.security.pinactivitycomponent.widget.b)localObject1).getInputLength();
          if (i2 == i3) {}
        }
        else
        {
          i2 = a.f.invalid_otp;
          localObject2 = a(i2);
          b((View)localObject1, (String)localObject2);
        }
      }
    }
    for (;;)
    {
      return;
      i2 = 0;
      localObject2 = null;
      String str;
      for (;;)
      {
        localObject1 = f;
        int m = ((ArrayList)localObject1).size();
        if (i2 >= m) {
          break label262;
        }
        localObject1 = f.get(i2);
        boolean bool2 = localObject1 instanceof org.npci.upi.security.pinactivitycomponent.widget.b;
        if (bool2)
        {
          localObject1 = (org.npci.upi.security.pinactivitycomponent.widget.b)f.get(i2);
          str = ((org.npci.upi.security.pinactivitycomponent.widget.b)localObject1).getInputValue();
          i3 = str.length();
          int i4 = ((org.npci.upi.security.pinactivitycomponent.widget.b)localObject1).getInputLength();
          if (i3 != i4)
          {
            i2 = a.f.componentMessage;
            localObject2 = a(i2);
            b((View)localObject1, (String)localObject2);
            break;
          }
        }
        int n = i2 + 1;
        i2 = n;
      }
      label262:
      boolean bool3 = as;
      if (!bool3)
      {
        as = i;
        for (;;)
        {
          localObject1 = f;
          int i1 = ((ArrayList)localObject1).size();
          if (j >= i1) {
            break;
          }
          try
          {
            localObject1 = f;
            localObject1 = ((ArrayList)localObject1).get(j);
            localObject1 = (c)localObject1;
            localObject1 = ((c)localObject1).getFormDataTag();
            localObject1 = (JSONObject)localObject1;
            localObject2 = "type";
            localObject2 = ((JSONObject)localObject1).getString((String)localObject2);
            str = "subtype";
            str = ((JSONObject)localObject1).getString(str);
            Object localObject3 = b;
            Object localObject4 = "credential";
            localObject1 = f;
            localObject1 = ((ArrayList)localObject1).get(j);
            localObject1 = (c)localObject1;
            localObject1 = ((c)localObject1).getInputValue();
            ((JSONObject)localObject3).put((String)localObject4, localObject1);
            localObject1 = ao;
            localObject1 = (GetCredential)localObject1;
            localObject1 = ((GetCredential)localObject1).o();
            localObject1 = ((w)localObject1).a();
            localObject3 = b;
            localObject3 = ((t)localObject1).a((JSONObject)localObject3);
            localObject1 = ao;
            localObject1 = (GetCredential)localObject1;
            localObject1 = ((GetCredential)localObject1).o();
            localObject1 = ((w)localObject1).b();
            localObject4 = b;
            localObject1 = ((ac)localObject1).a((String)localObject3, (String)localObject2, str, (JSONObject)localObject4);
            if (localObject1 != null)
            {
              localObject2 = aq;
              localObject1 = ao.a(localObject1);
              ((HashMap)localObject2).put(str, localObject1);
            }
          }
          catch (Exception localException)
          {
            for (;;)
            {
              localObject2 = ap;
              g.a((String)localObject2, localException);
            }
          }
          j += 1;
        }
        localObject2 = new android/os/Bundle;
        ((Bundle)localObject2).<init>();
        localHashMap = aq;
        ((Bundle)localObject2).putSerializable("credBlocks", localHashMap);
        ((GetCredential)ao).o().d().send(i, (Bundle)localObject2);
        Activity localActivity = ((GetCredential)ao).o().c();
        localActivity.finish();
      }
    }
  }
  
  private void N()
  {
    int i = g;
    int k = -1;
    if (i != k)
    {
      localObject1 = f;
      k = g;
      localObject1 = ((ArrayList)localObject1).get(k);
      bool1 = localObject1 instanceof org.npci.upi.security.pinactivitycomponent.widget.b;
      if (bool1)
      {
        localObject1 = f;
        k = g;
        localObject1 = (org.npci.upi.security.pinactivitycomponent.widget.b)((ArrayList)localObject1).get(k);
        a((org.npci.upi.security.pinactivitycomponent.widget.b)localObject1);
        ((org.npci.upi.security.pinactivitycomponent.widget.b)localObject1).a();
      }
    }
    boolean bool1 = false;
    Object localObject1 = null;
    Object localObject2 = f;
    int m = ((ArrayList)localObject2).size();
    int j;
    for (int n = 0; n < m; n = j)
    {
      j = g;
      if (n != j)
      {
        c localc = (c)f.get(n);
        localObject1 = h();
        k = a.c.ic_visibility_on;
        Drawable localDrawable1 = android.support.v4.a.a.a((Context)localObject1, k);
        localObject1 = h();
        k = a.c.ic_visibility_off;
        Drawable localDrawable2 = android.support.v4.a.a.a((Context)localObject1, k);
        j = a.f.action_hide;
        String str1 = a(j);
        j = a.f.action_show;
        String str2 = a(j);
        localObject1 = new org/npci/upi/security/pinactivitycomponent/v;
        localObject2 = this;
        ((v)localObject1).<init>(this, localc, str1, str2, localDrawable2, localDrawable1);
        boolean bool2 = true;
        boolean bool3 = true;
        localc.a(str2, localDrawable1, (View.OnClickListener)localObject1, 0, bool2, bool3);
      }
      j = n + 1;
    }
  }
  
  private void a(View paramView)
  {
    int i = a.d.switcherLayout1;
    LinearLayout localLinearLayout1 = (LinearLayout)paramView.findViewById(i);
    int j = a.d.switcherLayout2;
    LinearLayout localLinearLayout2 = (LinearLayout)paramView.findViewById(j);
    int k = a.d.view_switcher;
    Object localObject1 = (ViewSwitcher)paramView.findViewById(k);
    at = ((ViewSwitcher)localObject1);
    localObject1 = c;
    int n;
    if (localObject1 != null)
    {
      k = 0;
      localObject1 = null;
      n = 0;
      localObject1 = c;
      k = ((JSONArray)localObject1).length();
      if (n < k) {
        for (;;)
        {
          try
          {
            localObject1 = c;
            localJSONObject = ((JSONArray)localObject1).getJSONObject(n);
            localObject1 = "subtype";
            localObject3 = localJSONObject.getString((String)localObject1);
            localObject1 = "dLength";
            k = localJSONObject.optInt((String)localObject1);
            if (k != 0) {
              continue;
            }
            k = 6;
            i1 = k;
            localObject1 = "MPIN";
            boolean bool1 = ((String)localObject3).equals(localObject1);
            if (!bool1) {
              continue;
            }
            m = a.f.npci_set_mpin_title;
            localObject1 = a(m);
            localObject1 = a((String)localObject1, n, i1);
            int i2 = a.f.npci_confirm_mpin_title;
            localObject3 = a(i2);
            localObject3 = a((String)localObject3, n, i1);
            localObject4 = new java/util/ArrayList;
            ((ArrayList)localObject4).<init>();
            ((ArrayList)localObject4).add(localObject1);
            ((ArrayList)localObject4).add(localObject3);
            localObject1 = new org/npci/upi/security/pinactivitycomponent/widget/a;
            localObject3 = h();
            ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject1).<init>((Context)localObject3);
            ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject1).a((ArrayList)localObject4, this);
            ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject1).setFormDataTag(localJSONObject);
            localObject3 = f;
            ((ArrayList)localObject3).add(localObject1);
            localLinearLayout2.addView((View)localObject1);
          }
          catch (Exception localException)
          {
            JSONObject localJSONObject;
            int i1;
            Object localObject3 = ap;
            Object localObject4 = "Error while inflating Layout";
            Log.e((String)localObject3, (String)localObject4, localException);
            continue;
            String str = "OTP";
            boolean bool4 = str.equals(localObject3);
            if (bool4) {
              continue;
            }
            str = "SMS";
            bool4 = str.equals(localObject3);
            if (bool4) {
              continue;
            }
            str = "EMAIL";
            bool4 = str.equals(localObject3);
            if (bool4) {
              continue;
            }
            str = "HOTP";
            bool4 = str.equals(localObject3);
            if (bool4) {
              continue;
            }
            str = "TOTP";
            boolean bool3 = str.equals(localObject3);
            if (!bool3) {
              continue;
            }
            int m = a.f.npci_otp_title;
            localObject3 = a(m);
            g = n;
            Object localObject2 = h();
            boolean bool2 = localObject2 instanceof GetCredential;
            if (!bool2) {
              continue;
            }
            localObject2 = h();
            localObject2 = (GetCredential)localObject2;
            bool2 = ((GetCredential)localObject2).n();
            if (!bool2) {
              continue;
            }
            c(i1);
            localObject2 = localObject3;
            continue;
          }
          m = n + 1;
          n = m;
          break;
          localObject1 = "dLength";
          m = localJSONObject.optInt((String)localObject1);
          i1 = m;
          continue;
          localObject1 = "";
          str = "ATMPIN";
          bool4 = ((String)localObject3).equals(str);
          if (!bool4) {
            continue;
          }
          m = a.f.npci_atm_title;
          localObject1 = a(m);
          localObject1 = a((String)localObject1, n, i1);
          ((org.npci.upi.security.pinactivitycomponent.widget.b)localObject1).setFormDataTag(localJSONObject);
          localObject3 = f;
          ((ArrayList)localObject3).add(localObject1);
          localLinearLayout1.addView((View)localObject1);
        }
      }
    }
  }
  
  public View a(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = a.e.fragment_atmpin;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public void a()
  {
    int i = 2;
    int j = 1;
    int k = ar;
    Object localObject;
    int m;
    if (k == 0)
    {
      localObject = f;
      m = ar + 1;
      localObject = (c)((ArrayList)localObject).get(m);
      ((c)localObject).d();
      k = ar + 1;
      ar = k;
    }
    for (;;)
    {
      return;
      k = ar;
      if (k == j)
      {
        m = ((org.npci.upi.security.pinactivitycomponent.widget.b)f.get(0)).getInputLength();
        localObject = ((c)f.get(0)).getInputValue();
        k = ((String)localObject).length();
        String str;
        if (m != k)
        {
          localObject = (View)f.get(0);
          m = a.f.npci_otp_title;
          str = a(m);
          b((View)localObject, str);
          continue;
        }
        m = ((org.npci.upi.security.pinactivitycomponent.widget.b)f.get(j)).getInputLength();
        localObject = ((c)f.get(j)).getInputValue();
        k = ((String)localObject).length();
        if (m != k)
        {
          localObject = (View)f.get(j);
          m = a.f.npci_atm_title;
          str = a(m);
          b((View)localObject, str);
          continue;
        }
        localObject = at;
        if (localObject != null)
        {
          localObject = at;
          ((ViewSwitcher)localObject).showNext();
          ar = i;
          continue;
        }
      }
      k = ar;
      if (k == i)
      {
        localObject = f;
        m = ar;
        localObject = (c)((ArrayList)localObject).get(m);
        boolean bool = ((c)localObject).d();
        if (bool) {
          M();
        }
      }
      else
      {
        M();
      }
    }
  }
  
  public void a(int paramInt, String paramString)
  {
    int i = g;
    int j = -1;
    if (i != j)
    {
      i = g;
      if (i == paramInt)
      {
        Object localObject1 = f;
        j = g;
        localObject1 = ((ArrayList)localObject1).get(j);
        boolean bool = localObject1 instanceof org.npci.upi.security.pinactivitycomponent.widget.b;
        if (bool)
        {
          localObject1 = this.i;
          a((Timer)localObject1);
          localObject1 = f;
          j = g;
          ((org.npci.upi.security.pinactivitycomponent.widget.b)((ArrayList)localObject1).get(j)).a(false);
          localObject1 = f;
          j = g;
          ((org.npci.upi.security.pinactivitycomponent.widget.b)((ArrayList)localObject1).get(j)).a("", null, false, false);
          localObject1 = f;
          j = g;
          localObject1 = (org.npci.upi.security.pinactivitycomponent.widget.b)((ArrayList)localObject1).get(j);
          Object localObject2 = h();
          int k = a.c.ic_tick_ok;
          localObject2 = android.support.v4.a.a.a((Context)localObject2, k);
          k = 1;
          ((org.npci.upi.security.pinactivitycomponent.widget.b)localObject1).a((Drawable)localObject2, k);
        }
      }
    }
  }
  
  public void a(View paramView, Bundle paramBundle)
  {
    super.a(paramView, paramBundle);
    K();
    a(paramView);
    N();
  }
  
  public void a(View paramView, String paramString)
  {
    b(paramView, paramString);
  }
  
  public void b(int paramInt)
  {
    Object localObject = f.get(paramInt);
    boolean bool = localObject instanceof org.npci.upi.security.pinactivitycomponent.widget.a;
    if (!bool) {
      ar = paramInt;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */