package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.util.Base64;
import in.org.npci.commonlibrary.l;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class z
{
  private Context a;
  private String b = "";
  private ab c = null;
  private aa d;
  private String e;
  
  public z(Context paramContext)
  {
    Object localObject = "";
    e = ((String)localObject);
    try
    {
      a = paramContext;
      localObject = new org/npci/upi/security/pinactivitycomponent/ab;
      ((ab)localObject).<init>(paramContext);
      c = ((ab)localObject);
      localObject = new org/npci/upi/security/pinactivitycomponent/aa;
      ((aa)localObject).<init>();
      d = ((aa)localObject);
      return;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      throw localException;
    }
  }
  
  private boolean b(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    boolean bool1 = false;
    try
    {
      Object localObject1 = c;
      localObject1 = ((ab)localObject1).b();
      Object localObject2 = "Token in CL";
      g.b((String)localObject2, (String)localObject1);
      localObject2 = d;
      localObject1 = ((aa)localObject2).b((String)localObject1);
      localObject2 = null;
      localObject2 = Base64.decode(paramString1, 0);
      Object localObject3 = d;
      localObject1 = ((aa)localObject3).b((byte[])localObject2, (byte[])localObject1);
      localObject2 = null;
      localObject1 = Base64.encodeToString((byte[])localObject1, 0);
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append(paramString2);
      localObject3 = "|";
      localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).append(paramString3);
      localObject3 = "|";
      localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).append(paramString4);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject3 = "CL Hmac Msg";
      g.b((String)localObject3, (String)localObject2);
      localObject3 = d;
      localObject2 = ((aa)localObject3).a((String)localObject2);
      localObject3 = null;
      localObject2 = Base64.encodeToString((byte[])localObject2, 0);
      localObject3 = "CL Hash";
      g.b((String)localObject3, (String)localObject2);
      boolean bool2 = ((String)localObject2).equalsIgnoreCase((String)localObject1);
      if (bool2) {
        bool1 = true;
      }
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      for (;;)
      {
        localNoSuchAlgorithmException.printStackTrace();
      }
    }
    catch (IllegalBlockSizeException localIllegalBlockSizeException)
    {
      for (;;)
      {
        localIllegalBlockSizeException.printStackTrace();
      }
    }
    catch (BadPaddingException localBadPaddingException)
    {
      for (;;)
      {
        localBadPaddingException.printStackTrace();
      }
    }
    catch (InvalidAlgorithmParameterException localInvalidAlgorithmParameterException)
    {
      for (;;)
      {
        localInvalidAlgorithmParameterException.printStackTrace();
      }
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      for (;;)
      {
        localInvalidKeyException.printStackTrace();
      }
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;)
      {
        localUnsupportedEncodingException.printStackTrace();
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
    return bool1;
  }
  
  public String a()
  {
    localObject = "AES";
    try
    {
      localObject = KeyGenerator.getInstance((String)localObject);
      int i = 256;
      ((KeyGenerator)localObject).init(i);
      localObject = ((KeyGenerator)localObject).generateKey();
      localObject = ((SecretKey)localObject).getEncoded();
      aa localaa = d;
      localObject = localaa.a((byte[])localObject);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        localObject = null;
      }
    }
    return (String)localObject;
  }
  
  public String a(String paramString1, String paramString2)
  {
    Object localObject1 = "";
    Object localObject2 = "";
    Object localObject3 = new java/text/SimpleDateFormat;
    ((SimpleDateFormat)localObject3).<init>("dd/MM/yyyy");
    Object localObject5 = new java/sql/Date;
    long l = System.currentTimeMillis();
    ((java.sql.Date)localObject5).<init>(l);
    localObject5 = ((SimpleDateFormat)localObject3).format((java.util.Date)localObject5);
    for (;;)
    {
      try
      {
        localObject3 = b();
        b = ((String)localObject3);
        localObject3 = a();
        localObject1 = localObject3;
        boolean bool;
        int i;
        ((Exception)localObject2).printStackTrace();
      }
      catch (Exception localException2)
      {
        try
        {
          localObject6 = new org/npci/upi/security/pinactivitycomponent/aa;
          ((aa)localObject6).<init>();
          localObject3 = "initial";
          bool = paramString1.equalsIgnoreCase((String)localObject3);
          if (bool)
          {
            localObject3 = c;
            ((ab)localObject3).c();
            localObject3 = c;
            localObject3 = ((ab)localObject3).a();
            i = ((List)localObject3).size();
            if (i > 0)
            {
              localObject3 = c;
              localObject7 = new org/npci/upi/security/pinactivitycomponent/u;
              str = b;
              ((u)localObject7).<init>((String)localObject1, str, (String)localObject5);
              ((ab)localObject3).b((u)localObject7);
              localObject3 = new java/lang/StringBuilder;
              ((StringBuilder)localObject3).<init>();
              localObject5 = b;
              localObject3 = ((StringBuilder)localObject3).append((String)localObject5);
              localObject5 = "|";
              localObject3 = ((StringBuilder)localObject3).append((String)localObject5);
              localObject3 = ((StringBuilder)localObject3).append((String)localObject1);
              localObject5 = "|";
              localObject3 = ((StringBuilder)localObject3).append((String)localObject5);
              localObject3 = ((StringBuilder)localObject3).append(paramString2);
              localObject2 = ((StringBuilder)localObject3).toString();
              localObject3 = l.a();
              localObject3 = ((aa)localObject6).a((String)localObject2, (PublicKey)localObject3);
              localObject2 = "K0 in Challenge";
            }
          }
        }
        catch (Exception localException3)
        {
          localObject4 = localObject2;
          localObject2 = localException3;
        }
        try
        {
          g.b((String)localObject2, (String)localObject1);
          localObject2 = "token in Challenge";
          localObject1 = b;
          g.b((String)localObject2, (String)localObject1);
          return (String)localObject3;
        }
        catch (Exception localException1)
        {
          for (;;) {}
        }
        localException2 = localException2;
        localException2.printStackTrace();
        continue;
        localObject4 = c;
        localObject7 = new org/npci/upi/security/pinactivitycomponent/u;
        str = b;
        ((u)localObject7).<init>((String)localObject1, str, (String)localObject5);
        ((ab)localObject4).a((u)localObject7);
        continue;
      }
      continue;
      Object localObject4 = c;
      localObject4 = ((ab)localObject4).a();
      Object localObject7 = null;
      localObject4 = ((List)localObject4).get(0);
      localObject4 = (u)localObject4;
      localObject4 = ((u)localObject4).a();
      localObject7 = new java/lang/StringBuilder;
      ((StringBuilder)localObject7).<init>();
      String str = b;
      localObject7 = ((StringBuilder)localObject7).append(str);
      str = "|";
      localObject7 = ((StringBuilder)localObject7).append(str);
      localObject7 = ((StringBuilder)localObject7).append((String)localObject1);
      str = "|";
      localObject7 = ((StringBuilder)localObject7).append(str);
      localObject7 = ((StringBuilder)localObject7).append(paramString2);
      localObject2 = ((StringBuilder)localObject7).toString();
      localObject7 = "K0 in Challenge";
      g.b((String)localObject7, (String)localObject1);
      localObject7 = "token in Challenge";
      str = b;
      g.b((String)localObject7, str);
      localObject4 = ((aa)localObject6).b((String)localObject4);
      localObject7 = ((String)localObject2).getBytes();
      localObject4 = ((aa)localObject6).a((byte[])localObject7, (byte[])localObject4);
      Object localObject6 = null;
      localObject4 = Base64.encodeToString((byte[])localObject4, 0);
      localObject2 = c;
      ((ab)localObject2).c();
      localObject2 = c;
      localObject6 = new org/npci/upi/security/pinactivitycomponent/u;
      localObject7 = b;
      ((u)localObject6).<init>((String)localObject1, (String)localObject7, (String)localObject5);
      ((ab)localObject2).a((u)localObject6);
    }
  }
  
  public boolean a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    String str = "hmac";
    g.b(str, paramString4);
    boolean bool = b(paramString4, paramString1, paramString2, paramString3);
    if (bool) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  public String b()
  {
    localObject = "AES";
    try
    {
      localObject = KeyGenerator.getInstance((String)localObject);
      int i = 256;
      ((KeyGenerator)localObject).init(i);
      localObject = ((KeyGenerator)localObject).generateKey();
      localObject = ((SecretKey)localObject).getEncoded();
      aa localaa = d;
      localObject = localaa.a((byte[])localObject);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
        localObject = null;
      }
    }
    return (String)localObject;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/z.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */