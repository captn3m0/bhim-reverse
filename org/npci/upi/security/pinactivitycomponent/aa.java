package org.npci.upi.security.pinactivitycomponent;

import android.util.Base64;
import java.security.MessageDigest;
import java.security.PublicKey;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class aa
{
  Cipher a;
  byte[] b;
  byte[] c;
  
  public aa()
  {
    Object localObject = Cipher.getInstance("AES/CBC/PKCS7Padding");
    a = ((Cipher)localObject);
    localObject = new byte[32];
    b = ((byte[])localObject);
    localObject = new byte[16];
    c = ((byte[])localObject);
  }
  
  public String a(String paramString, PublicKey paramPublicKey)
  {
    byte[] arrayOfByte1 = paramString.getBytes();
    byte[] arrayOfByte2 = null;
    Object localObject = "RSA/ECB/PKCS1Padding";
    try
    {
      localObject = Cipher.getInstance((String)localObject);
      int i = 1;
      ((Cipher)localObject).init(i, paramPublicKey);
      arrayOfByte2 = ((Cipher)localObject).doFinal(arrayOfByte1);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
    return Base64.encodeToString(arrayOfByte2, 2);
  }
  
  public String a(byte[] paramArrayOfByte)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = paramArrayOfByte.length * 2;
    localStringBuilder.<init>(i);
    int j = paramArrayOfByte.length;
    i = 0;
    while (i < j)
    {
      int k = paramArrayOfByte[i];
      String str = "%02x";
      int m = 1;
      Object[] arrayOfObject = new Object[m];
      k &= 0xFF;
      Object localObject = Integer.valueOf(k);
      arrayOfObject[0] = localObject;
      localObject = String.format(str, arrayOfObject);
      localStringBuilder.append((String)localObject);
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public byte[] a(String paramString)
  {
    MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-256");
    byte[] arrayOfByte = paramString.getBytes("UTF-8");
    localMessageDigest.update(arrayOfByte);
    return localMessageDigest.digest();
  }
  
  public byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    Object localObject = new byte[16];
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    localIvParameterSpec.<init>((byte[])localObject);
    localObject = Cipher.getInstance("AES/CBC/PKCS7Padding");
    ((Cipher)localObject).init(1, localSecretKeySpec, localIvParameterSpec);
    return ((Cipher)localObject).doFinal(paramArrayOfByte1);
  }
  
  public byte[] b(String paramString)
  {
    byte[] arrayOfByte = new byte[paramString.length() / 2];
    int i = 0;
    for (;;)
    {
      int j = arrayOfByte.length;
      if (i >= j) {
        break;
      }
      j = i * 2;
      int k = j + 2;
      String str = paramString.substring(j, k);
      k = 16;
      j = (byte)Integer.parseInt(str, k);
      arrayOfByte[i] = j;
      i += 1;
    }
    return arrayOfByte;
  }
  
  public byte[] b(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    Object localObject = new byte[16];
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    localIvParameterSpec.<init>((byte[])localObject);
    localObject = Cipher.getInstance("AES/CBC/PKCS7Padding");
    ((Cipher)localObject).init(2, localSecretKeySpec, localIvParameterSpec);
    return ((Cipher)localObject).doFinal(paramArrayOfByte1);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/aa.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */