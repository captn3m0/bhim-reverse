package org.npci.upi.security.pinactivitycomponent;

import android.animation.Animator.AnimatorListener;
import android.animation.TimeInterpolator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.p;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewPropertyAnimator;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONObject;

public class GetCredential
  extends android.support.v7.a.b
{
  private static final String m = GetCredential.class.getSimpleName();
  private TransitionDrawable A;
  private ImageView B;
  private int C;
  private boolean D;
  private int E;
  private Thread.UncaughtExceptionHandler F;
  private JSONObject n = null;
  private JSONObject o = null;
  private JSONObject p = null;
  private JSONArray q = null;
  private JSONArray r;
  private String s;
  private an t;
  private w u;
  private h v;
  private final Context w;
  private am x;
  private View y;
  private View z;
  
  public GetCredential()
  {
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    r = localJSONArray;
    s = "en_US";
    v = null;
    w = this;
    D = false;
    E = 0;
    F = null;
  }
  
  private int a(float paramFloat)
  {
    return (int)(getResourcesgetDisplayMetricsdensityDpi / 160 * paramFloat);
  }
  
  private void b(boolean paramBoolean)
  {
    int i = 300;
    int j = 1127481344;
    float f1 = 180.0F;
    float f2 = 0.0F;
    Object localObject1 = null;
    Object localObject2;
    int k;
    ViewPropertyAnimator localViewPropertyAnimator;
    if (paramBoolean)
    {
      localObject2 = B;
      a(0.0F, f1, i, (View)localObject2);
      k = Build.VERSION.SDK_INT;
      j = 14;
      f1 = 2.0E-44F;
      if (k <= j) {
        break label230;
      }
      localObject2 = y;
      k = ((View)localObject2).getHeight();
      if (k == 0) {
        k = C;
      }
      y.clearAnimation();
      localObject3 = y;
      localViewPropertyAnimator = ((View)localObject3).animate();
      if (!paramBoolean) {
        break label212;
      }
      j = 0;
      localObject3 = null;
    }
    label212:
    float f3;
    for (f1 = 0.0F;; f1 = -1.0F * f3)
    {
      localObject3 = localViewPropertyAnimator.y(f1);
      if (paramBoolean) {
        f2 = 1.0F;
      }
      localObject3 = ((ViewPropertyAnimator)localObject3).alpha(f2);
      long l = 300L;
      localObject3 = ((ViewPropertyAnimator)localObject3).setDuration(l);
      localObject1 = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject1).<init>();
      localObject3 = ((ViewPropertyAnimator)localObject3).setInterpolator((TimeInterpolator)localObject1);
      localObject1 = new org/npci/upi/security/pinactivitycomponent/al;
      ((al)localObject1).<init>(this, paramBoolean, k);
      ((ViewPropertyAnimator)localObject3).setListener((Animator.AnimatorListener)localObject1);
      return;
      localObject2 = B;
      a(f1, 0.0F, i, (View)localObject2);
      break;
      j = -1082130432;
      f3 = k;
    }
    label230:
    Object localObject3 = y;
    if (paramBoolean)
    {
      k = 0;
      localObject2 = null;
    }
    for (;;)
    {
      ((View)localObject3).setVisibility(k);
      break;
      k = 8;
    }
  }
  
  private boolean q()
  {
    int i = 2;
    int j = 3;
    int k = 1;
    String[] arrayOfString1 = new String[j];
    arrayOfString1[0] = "ATMPIN";
    arrayOfString1[k] = "SMS|EMAIL|HOTP|TOTP";
    arrayOfString1[i] = "MPIN";
    String[] arrayOfString2 = new String[j];
    Object localObject = q;
    if (localObject != null)
    {
      localObject = q;
      int i1 = ((JSONArray)localObject).length();
      if (i1 == j)
      {
        j = 0;
        i = 0;
        int i3 = 0;
        int i4 = 0;
        for (;;)
        {
          localObject = q;
          i1 = ((JSONArray)localObject).length();
          if (j >= i1) {
            break;
          }
          try
          {
            localObject = q;
            localObject = ((JSONArray)localObject).get(j);
            localObject = (JSONObject)localObject;
            str1 = "subtype";
            String str2 = "";
            localObject = ((JSONObject)localObject).optString(str1, str2);
            arrayOfString2[j] = localObject;
            localObject = arrayOfString2[j];
            int i5 = 0;
            str1 = null;
            str1 = arrayOfString1[0];
            boolean bool = ((String)localObject).matches(str1);
            if (bool) {
              i4 = k;
            }
            localObject = arrayOfString2[j];
            i5 = 1;
            str1 = arrayOfString1[i5];
            bool = ((String)localObject).matches(str1);
            if (bool) {
              i3 = k;
            }
            localObject = arrayOfString2[j];
            i5 = 2;
            str1 = arrayOfString1[i5];
            bool = ((String)localObject).matches(str1);
            if (bool) {
              i = k;
            }
          }
          catch (Exception localException)
          {
            for (;;)
            {
              int i2;
              String str1 = m;
              g.a(str1, localException);
            }
          }
          i2 = j + 1;
          j = i2;
        }
        if ((i4 == 0) || (i3 == 0) || (i == 0)) {}
      }
    }
    for (;;)
    {
      return k;
      k = 0;
    }
  }
  
  private void r()
  {
    Object localObject1 = getIntent().getExtras();
    Object localObject2;
    if (localObject1 != null) {
      localObject2 = "configuration";
    }
    try
    {
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      Object localObject3;
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        n = ((JSONObject)localObject3);
      }
      localObject2 = "controls";
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        o = ((JSONObject)localObject3);
        localObject2 = o;
        if (localObject2 != null)
        {
          localObject2 = o;
          localObject3 = "CredAllowed";
          localObject2 = ((JSONObject)localObject2).getString((String)localObject3);
          if (localObject2 != null)
          {
            localObject3 = new org/json/JSONArray;
            ((JSONArray)localObject3).<init>((String)localObject2);
            q = ((JSONArray)localObject3);
          }
        }
      }
      localObject2 = "salt";
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        p = ((JSONObject)localObject3);
      }
      localObject2 = "payInfo";
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONArray;
        ((JSONArray)localObject3).<init>((String)localObject2);
        r = ((JSONArray)localObject3);
      }
      localObject2 = "languagePref";
      localObject1 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject1 != null)
      {
        s = ((String)localObject1);
        localObject1 = s;
        localObject2 = "_";
        localObject2 = ((String)localObject1).split((String)localObject2);
        localObject1 = new java/util/Locale;
        localObject3 = s;
        ((Locale)localObject1).<init>((String)localObject3);
        int i = localObject2.length;
        int j = 2;
        if (i == j)
        {
          localObject1 = new java/util/Locale;
          i = 0;
          localObject3 = null;
          localObject3 = localObject2[0];
          j = 1;
          localObject2 = localObject2[j];
          ((Locale)localObject1).<init>((String)localObject3, (String)localObject2);
        }
        Locale.setDefault((Locale)localObject1);
        localObject2 = new android/content/res/Configuration;
        ((Configuration)localObject2).<init>();
        locale = ((Locale)localObject1);
        localObject1 = getBaseContext();
        localObject1 = ((Context)localObject1).getResources();
        localObject3 = getBaseContext();
        localObject3 = ((Context)localObject3).getResources();
        localObject3 = ((Resources)localObject3).getDisplayMetrics();
        ((Resources)localObject1).updateConfiguration((Configuration)localObject2, (DisplayMetrics)localObject3);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localObject2 = m;
        g.a((String)localObject2, localException);
      }
    }
  }
  
  private void s()
  {
    int i = a.d.fragmentTelKeyboard;
    Keypad localKeypad = (Keypad)findViewById(i);
    if (localKeypad != null)
    {
      ah localah = new org/npci/upi/security/pinactivitycomponent/ah;
      localah.<init>(this);
      localKeypad.setOnKeyPressCallback(localah);
    }
  }
  
  private boolean t()
  {
    View localView = y;
    int i = localView.getVisibility();
    if (i == 0) {
      i = 1;
    }
    for (;;)
    {
      return i;
      int j = 0;
      localView = null;
    }
  }
  
  private void u()
  {
    Object localObject1 = new org/npci/upi/security/pinactivitycomponent/an;
    ((an)localObject1).<init>();
    t = ((an)localObject1);
    try
    {
      localObject1 = new org/npci/upi/security/pinactivitycomponent/w;
      localObject2 = getApplicationContext();
      localObject3 = t;
      ((w)localObject1).<init>((Context)localObject2, (an)localObject3, this);
      u = ((w)localObject1);
      localObject1 = t;
      localObject2 = getIntent();
      localObject2 = ((Intent)localObject2).getExtras();
      ((an)localObject1).a((Bundle)localObject2, this);
      return;
    }
    catch (d locald)
    {
      for (;;)
      {
        localObject2 = m;
        localObject3 = locald.a();
        Log.e((String)localObject2, (String)localObject3, locald);
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = m;
        Object localObject3 = "Error parsing and validating arguments to CL";
        Log.e((String)localObject2, (String)localObject3, localException);
      }
    }
  }
  
  private void v()
  {
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>();
    Object localObject2 = "android.provider.Telephony.SMS_RECEIVED";
    try
    {
      localIntentFilter.addAction((String)localObject2);
      int i = 999;
      localIntentFilter.setPriority(i);
      localObject2 = x;
      registerReceiver((BroadcastReceiver)localObject2, localIntentFilter);
      return;
    }
    finally
    {
      for (;;)
      {
        String str = m;
        localObject2 = "Failed to register SMS broadcast receiver (Ignoring)";
        g.a(str, (String)localObject2);
      }
    }
  }
  
  private void w()
  {
    try
    {
      am localam = x;
      if (localam != null)
      {
        localam = x;
        unregisterReceiver(localam);
        localam = null;
        x = null;
      }
      return;
    }
    finally
    {
      for (;;)
      {
        String str1 = m;
        String str2 = "Failed to unregister SMS receiver (Ignoring)";
        g.a(str1, str2);
      }
    }
  }
  
  private void x()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("error", "USER_ABORTED");
    o().d().send(0, localBundle);
    finish();
  }
  
  public void a(float paramFloat1, float paramFloat2, int paramInt, View paramView)
  {
    float f = 0.5F;
    int i = 1;
    RotateAnimation localRotateAnimation = new android/view/animation/RotateAnimation;
    localRotateAnimation.<init>(paramFloat1, paramFloat2, i, f, i, f);
    LinearInterpolator localLinearInterpolator = new android/view/animation/LinearInterpolator;
    localLinearInterpolator.<init>();
    localRotateAnimation.setInterpolator(localLinearInterpolator);
    long l = paramInt;
    localRotateAnimation.setDuration(l);
    localRotateAnimation.setFillEnabled(i);
    localRotateAnimation.setFillAfter(i);
    paramView.startAnimation(localRotateAnimation);
  }
  
  public void a(h paramh)
  {
    v = paramh;
  }
  
  public void a(h paramh, Bundle paramBundle, boolean paramBoolean)
  {
    try
    {
      Object localObject1 = f();
      if (paramBundle != null) {
        paramh.g(paramBundle);
      }
      localObject1 = ((p)localObject1).a();
      int i = a.d.main_inner_layout;
      ((android.support.v4.app.r)localObject1).a(i, paramh);
      if (paramBoolean)
      {
        Object localObject2 = paramh.getClass();
        localObject2 = ((Class)localObject2).getSimpleName();
        ((android.support.v4.app.r)localObject1).a((String)localObject2);
      }
      ((android.support.v4.app.r)localObject1).b();
      a(paramh);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
  }
  
  public void b(int paramInt)
  {
    E = paramInt;
  }
  
  void k()
  {
    Object localObject1 = "";
    Object localObject2 = n;
    if (localObject2 != null)
    {
      localObject1 = n;
      localObject2 = "payerBankName";
      localObject1 = ((JSONObject)localObject1).optString((String)localObject2);
    }
    for (Object localObject3 = localObject1;; localObject3 = localException)
    {
      localObject1 = p;
      if (localObject1 == null)
      {
        localObject1 = new org/npci/upi/security/pinactivitycomponent/d;
        localObject2 = "l12";
        localObject4 = "l12.message";
        ((d)localObject1).<init>(this, (String)localObject2, (String)localObject4);
        return;
      }
      localObject1 = p;
      localObject2 = "txnAmount";
      String str = ((JSONObject)localObject1).optString((String)localObject2);
      Object localObject4 = "";
      localObject1 = "";
      boolean bool1 = ((String)localObject4).equals(localObject1);
      int i1;
      if (bool1)
      {
        bool1 = false;
        localObject1 = null;
        i1 = 0;
        localObject2 = null;
        localObject1 = r;
        int i = ((JSONArray)localObject1).length();
        if (i1 >= i) {}
      }
      for (;;)
      {
        try
        {
          localObject1 = r;
          localObject1 = ((JSONArray)localObject1).get(i1);
          localObject1 = (JSONObject)localObject1;
          localObject5 = "name";
          Object localObject6 = "";
          localObject1 = ((JSONObject)localObject1).optString((String)localObject5, (String)localObject6);
          localObject5 = "payeeName";
          boolean bool2 = ((String)localObject1).equals(localObject5);
          if (bool2)
          {
            localObject1 = r;
            localObject1 = ((JSONArray)localObject1).get(i1);
            localObject1 = (JSONObject)localObject1;
            localObject5 = "value";
            localObject6 = "";
            localObject1 = ((JSONObject)localObject1).optString((String)localObject5, (String)localObject6);
            localObject7 = localObject1;
            int j = a.d.transaction_bar_root;
            localObject1 = (LinearLayout)findViewById(j);
            i1 = a.d.tv_acc_or_payee;
            localObject2 = (TextView)findViewById(i1);
            int i4 = a.d.transaction_bar_title;
            localObject4 = (TextView)findViewById(i4);
            int i5 = a.d.transaction_bar_info;
            localObject5 = (TextView)findViewById(i5);
            int i6 = a.d.transaction_bar_arrow;
            localObject6 = (ImageView)findViewById(i6);
            B = ((ImageView)localObject6);
            ((TextView)localObject4).setText((CharSequence)localObject7);
            localObject4 = "";
            boolean bool5 = ((String)localObject3).equals(localObject4);
            if (!bool5) {
              ((TextView)localObject2).setText((CharSequence)localObject3);
            }
            localObject2 = "";
            boolean bool4 = str.equals(localObject2);
            if (!bool4)
            {
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              localObject4 = "₹ ";
              localObject2 = (String)localObject4 + str;
              ((TextView)localObject5).setText((CharSequence)localObject2);
            }
            localObject2 = getWindowManager().getDefaultDisplay();
            localObject4 = new android/graphics/Point;
            ((Point)localObject4).<init>();
            i5 = Build.VERSION.SDK_INT;
            i6 = 13;
            if (i5 < i6) {
              break label781;
            }
            ((Display)localObject2).getSize((Point)localObject4);
            i2 = y;
            C = i2;
            localObject2 = new org/npci/upi/security/pinactivitycomponent/ai;
            ((ai)localObject2).<init>(this);
            ((LinearLayout)localObject1).setOnClickListener((View.OnClickListener)localObject2);
            j = a.d.transaction_details_scroller;
            localObject1 = findViewById(j);
            y = ((View)localObject1);
            j = a.d.transaction_details_expanded_space;
            localObject1 = findViewById(j);
            z = ((View)localObject1);
            localObject1 = y;
            localObject2 = new org/npci/upi/security/pinactivitycomponent/aj;
            ((aj)localObject2).<init>(this);
            ((View)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
            localObject1 = z;
            if (localObject1 != null)
            {
              localObject1 = z;
              localObject2 = new org/npci/upi/security/pinactivitycomponent/ak;
              ((ak)localObject2).<init>(this);
              ((View)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
            }
            j = a.d.transaction_info_root;
            localObject1 = (TransitionDrawable)findViewById(j).getBackground();
            A = ((TransitionDrawable)localObject1);
            localObject1 = A;
            i2 = 1;
            ((TransitionDrawable)localObject1).setCrossFadeEnabled(i2);
            break;
          }
          localObject1 = r;
          localObject1 = ((JSONArray)localObject1).get(i2);
          localObject1 = (JSONObject)localObject1;
          localObject5 = "name";
          localObject6 = "";
          localObject1 = ((JSONObject)localObject1).optString((String)localObject5, (String)localObject6);
          localObject5 = "account";
          boolean bool3 = ((String)localObject1).equals(localObject5);
          if (bool3)
          {
            localObject1 = r;
            localObject1 = ((JSONArray)localObject1).get(i2);
            localObject1 = (JSONObject)localObject1;
            localObject5 = "value";
            localObject6 = "";
            localObject1 = ((JSONObject)localObject1).optString((String)localObject5, (String)localObject6);
            localObject7 = localObject1;
            continue;
          }
          localObject1 = r;
          localObject1 = ((JSONArray)localObject1).get(i2);
          localObject1 = (JSONObject)localObject1;
          localObject5 = "name";
          localObject6 = "";
          localObject1 = ((JSONObject)localObject1).optString((String)localObject5, (String)localObject6);
          localObject5 = "mobileNumber";
          bool3 = ((String)localObject1).equals(localObject5);
          if (bool3)
          {
            localObject1 = r;
            localObject1 = ((JSONArray)localObject1).get(i2);
            localObject1 = (JSONObject)localObject1;
            localObject5 = "payinfo.mobilenumber.label";
            localObject6 = "";
            localObject1 = ((JSONObject)localObject1).optString((String)localObject5, (String)localObject6);
            localObject7 = localObject1;
            continue;
          }
        }
        catch (Exception localException)
        {
          Object localObject5 = m;
          g.a((String)localObject5, localException);
          int k = i2 + 1;
          int i2 = k;
        }
        label781:
        int i3 = ((Display)localObject2).getHeight();
        C = i3;
        continue;
        Object localObject7 = localObject4;
      }
    }
  }
  
  void l()
  {
    int i = a.d.transaction_details_root;
    LinearLayout localLinearLayout = (LinearLayout)findViewById(i);
    int k;
    for (int j = 0;; j = k)
    {
      localObject1 = r;
      k = ((JSONArray)localObject1).length();
      if (j >= k) {
        break;
      }
      localObject1 = LayoutInflater.from(this);
      int i1 = a.e.layout_transaction_details_item;
      localObject1 = (ViewGroup)((LayoutInflater)localObject1).inflate(i1, localLinearLayout, false);
      i1 = a.d.transaction_details_item_name;
      localObject2 = (TextView)((ViewGroup)localObject1).findViewById(i1);
      int i2 = a.d.transaction_details_item_value;
      TextView localTextView = (TextView)((ViewGroup)localObject1).findViewById(i2);
      JSONObject localJSONObject = r.optJSONObject(j);
      String str = localJSONObject.optString("name").toUpperCase();
      ((TextView)localObject2).setText(str);
      localObject2 = localJSONObject.optString("value");
      localTextView.setText((CharSequence)localObject2);
      localLinearLayout.addView((View)localObject1);
      k = j + 1;
    }
    Object localObject1 = new android/view/View;
    ((View)localObject1).<init>(this);
    Object localObject2 = new android/widget/LinearLayout$LayoutParams;
    j = a(3.0F);
    ((LinearLayout.LayoutParams)localObject2).<init>(-1, j);
    ((View)localObject1).setLayoutParams((ViewGroup.LayoutParams)localObject2);
    ((View)localObject1).setBackgroundColor(-16777216);
    android.support.v4.f.af.b((View)localObject1, 0.33F);
    localLinearLayout.addView((View)localObject1);
  }
  
  public boolean m()
  {
    String str = "android.permission.RECEIVE_SMS";
    int i = checkCallingOrSelfPermission(str);
    if (i == 0) {
      i = 1;
    }
    for (;;)
    {
      return i;
      int j = 0;
      str = null;
    }
  }
  
  public boolean n()
  {
    String str = "android.permission.READ_SMS";
    int i = checkCallingOrSelfPermission(str);
    if (i == 0) {
      i = 1;
    }
    for (;;)
    {
      return i;
      int j = 0;
      str = null;
    }
  }
  
  public w o()
  {
    return u;
  }
  
  public void onBackPressed()
  {
    boolean bool = D;
    Object localObject1;
    Object localObject2;
    if (bool)
    {
      localObject1 = new android/os/Bundle;
      ((Bundle)localObject1).<init>();
      String str = "USER_ABORTED";
      ((Bundle)localObject1).putString("error", str);
      localObject2 = o().d();
      ((ResultReceiver)localObject2).send(0, (Bundle)localObject1);
      super.onBackPressed();
    }
    for (;;)
    {
      return;
      D = true;
      int i = a.f.back_button_exit_message;
      localObject1 = getString(i);
      Toast.makeText(this, (CharSequence)localObject1, 0).show();
      localObject1 = new android/os/Handler;
      ((Handler)localObject1).<init>();
      localObject2 = new org/npci/upi/security/pinactivitycomponent/ag;
      ((ag)localObject2).<init>(this);
      long l = 2000L;
      ((Handler)localObject1).postDelayed((Runnable)localObject2, l);
    }
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject1 = Thread.currentThread().getUncaughtExceptionHandler();
    F = ((Thread.UncaughtExceptionHandler)localObject1);
    localObject1 = Thread.currentThread();
    Object localObject2 = new org/npci/upi/security/pinactivitycomponent/ad;
    ((ad)localObject2).<init>();
    ((Thread)localObject1).setUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)localObject2);
    r();
    int i = a.e.activity_pin_activity_component;
    setContentView(i);
    s();
    u();
    k();
    l();
    boolean bool = q();
    if (bool)
    {
      localObject1 = new org/npci/upi/security/pinactivitycomponent/b;
      ((b)localObject1).<init>();
      localObject2 = getIntent().getExtras();
      a((h)localObject1, (Bundle)localObject2, false);
    }
    for (;;)
    {
      int j = a.d.go_back;
      localObject1 = (TextView)findViewById(j);
      if (localObject1 != null)
      {
        localObject2 = new org/npci/upi/security/pinactivitycomponent/af;
        ((af)localObject2).<init>(this);
        ((TextView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
      }
      return;
      localObject1 = new org/npci/upi/security/pinactivitycomponent/r;
      ((r)localObject1).<init>();
      localObject2 = getIntent().getExtras();
      a((h)localObject1, (Bundle)localObject2, false);
    }
  }
  
  protected void onDestroy()
  {
    super.onDestroy();
    Thread localThread = Thread.currentThread();
    Thread.UncaughtExceptionHandler localUncaughtExceptionHandler = F;
    localThread.setUncaughtExceptionHandler(localUncaughtExceptionHandler);
  }
  
  protected void onPause()
  {
    super.onPause();
    w();
  }
  
  protected void onResume()
  {
    super.onResume();
    boolean bool = m();
    Object localObject;
    String str;
    if (bool)
    {
      localObject = new org/npci/upi/security/pinactivitycomponent/am;
      str = null;
      ((am)localObject).<init>(this, null);
      x = ((am)localObject);
      v();
    }
    for (;;)
    {
      return;
      localObject = m;
      str = "RECEIVE_SMS permission not provided by the App. This will affect Auto OTP detection feature of Common Library";
      Log.e((String)localObject, str);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/GetCredential.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */