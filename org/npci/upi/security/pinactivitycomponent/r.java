package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.widget.b;
import org.npci.upi.security.pinactivitycomponent.widget.c;
import org.npci.upi.security.pinactivitycomponent.widget.o;

public class r
  extends h
  implements o
{
  private static final String ar = h.class.getSimpleName();
  LinearLayout ap;
  LinearLayout aq;
  private int as = 0;
  private Timer at = null;
  private Boolean au = null;
  private HashMap av;
  private boolean aw;
  
  public r()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    av = localHashMap;
    aw = false;
  }
  
  private void M()
  {
    int i = 1;
    int j = 0;
    HashMap localHashMap = null;
    int k = g;
    int i2 = -1;
    Object localObject1;
    Object localObject2;
    int i3;
    if (k != i2)
    {
      localObject1 = f;
      i2 = g;
      localObject1 = ((ArrayList)localObject1).get(i2);
      boolean bool1 = localObject1 instanceof b;
      if (bool1)
      {
        localObject1 = f;
        i2 = g;
        localObject1 = (b)((ArrayList)localObject1).get(i2);
        localObject2 = ((b)localObject1).getInputValue();
        if (localObject2 != null)
        {
          i2 = ((String)localObject2).length();
          i3 = ((b)localObject1).getInputLength();
          if (i2 == i3) {}
        }
        else
        {
          i2 = a.f.invalid_otp;
          localObject2 = a(i2);
          b((View)localObject1, (String)localObject2);
        }
      }
    }
    for (;;)
    {
      return;
      i2 = 0;
      localObject2 = null;
      String str;
      for (;;)
      {
        localObject1 = f;
        int m = ((ArrayList)localObject1).size();
        if (i2 >= m) {
          break label262;
        }
        localObject1 = f.get(i2);
        boolean bool2 = localObject1 instanceof b;
        if (bool2)
        {
          localObject1 = (b)f.get(i2);
          str = ((b)localObject1).getInputValue();
          i3 = str.length();
          int i4 = ((b)localObject1).getInputLength();
          if (i3 != i4)
          {
            i2 = a.f.componentMessage;
            localObject2 = a(i2);
            b((View)localObject1, (String)localObject2);
            break;
          }
        }
        int n = i2 + 1;
        i2 = n;
      }
      label262:
      boolean bool3 = aw;
      if (!bool3)
      {
        aw = i;
        for (;;)
        {
          localObject1 = f;
          int i1 = ((ArrayList)localObject1).size();
          if (j >= i1) {
            break;
          }
          try
          {
            localObject1 = f;
            localObject1 = ((ArrayList)localObject1).get(j);
            localObject1 = (c)localObject1;
            localObject1 = ((c)localObject1).getFormDataTag();
            localObject1 = (JSONObject)localObject1;
            localObject2 = "type";
            localObject2 = ((JSONObject)localObject1).getString((String)localObject2);
            str = "subtype";
            str = ((JSONObject)localObject1).getString(str);
            Object localObject3 = b;
            Object localObject4 = "credential";
            localObject1 = f;
            localObject1 = ((ArrayList)localObject1).get(j);
            localObject1 = (c)localObject1;
            localObject1 = ((c)localObject1).getInputValue();
            ((JSONObject)localObject3).put((String)localObject4, localObject1);
            localObject1 = ao;
            localObject1 = (GetCredential)localObject1;
            localObject1 = ((GetCredential)localObject1).o();
            localObject1 = ((w)localObject1).a();
            localObject3 = b;
            localObject3 = ((t)localObject1).a((JSONObject)localObject3);
            localObject1 = ao;
            localObject1 = (GetCredential)localObject1;
            localObject1 = ((GetCredential)localObject1).o();
            localObject1 = ((w)localObject1).b();
            localObject4 = b;
            localObject1 = ((ac)localObject1).a((String)localObject3, (String)localObject2, str, (JSONObject)localObject4);
            if (localObject1 != null)
            {
              localObject2 = av;
              localObject1 = ao.a(localObject1);
              ((HashMap)localObject2).put(str, localObject1);
            }
          }
          catch (Exception localException)
          {
            for (;;)
            {
              localObject2 = ar;
              g.a((String)localObject2, localException);
            }
          }
          j += 1;
        }
        localObject2 = new android/os/Bundle;
        ((Bundle)localObject2).<init>();
        localHashMap = av;
        ((Bundle)localObject2).putSerializable("credBlocks", localHashMap);
        ((GetCredential)ao).o().d().send(i, (Bundle)localObject2);
        Activity localActivity = ((GetCredential)ao).o().c();
        localActivity.finish();
      }
    }
  }
  
  private boolean N()
  {
    Object localObject1 = au;
    int i;
    if (localObject1 != null)
    {
      localObject1 = au;
      i = ((Boolean)localObject1).booleanValue();
    }
    for (;;)
    {
      return i;
      localObject1 = c;
      if (localObject1 != null)
      {
        ArrayList localArrayList = new java/util/ArrayList;
        localArrayList.<init>();
        i = 0;
        localObject1 = null;
        for (;;)
        {
          Object localObject2 = c;
          int k = ((JSONArray)localObject2).length();
          if (i >= k) {
            break;
          }
          try
          {
            localObject2 = c;
            localObject2 = ((JSONArray)localObject2).getJSONObject(i);
            str = "subtype";
            localObject2 = ((JSONObject)localObject2).getString(str);
            if (localObject2 != null) {
              localArrayList.add(localObject2);
            }
          }
          catch (Exception localException)
          {
            for (;;)
            {
              int j;
              String str = ar;
              g.a(str, localException);
            }
          }
          i += 1;
        }
        localObject1 = "OTP";
        bool = localArrayList.contains(localObject1);
        if (!bool)
        {
          localObject1 = "SMS";
          bool = localArrayList.contains(localObject1);
          if (!bool)
          {
            localObject1 = "EMAIL";
            bool = localArrayList.contains(localObject1);
            if (!bool)
            {
              localObject1 = "HOTP";
              bool = localArrayList.contains(localObject1);
              if (!bool)
              {
                localObject1 = "TOTP";
                bool = localArrayList.contains(localObject1);
                if (!bool) {
                  break label239;
                }
                localObject1 = "MPIN";
                bool = localArrayList.contains(localObject1);
                if (!bool) {
                  break label239;
                }
              }
            }
          }
        }
        localObject1 = Boolean.valueOf(true);
        au = ((Boolean)localObject1);
        localObject1 = au;
        bool = ((Boolean)localObject1).booleanValue();
        continue;
      }
      label239:
      localObject1 = Boolean.valueOf(false);
      au = ((Boolean)localObject1);
      localObject1 = au;
      boolean bool = ((Boolean)localObject1).booleanValue();
    }
  }
  
  private void O()
  {
    int i = g;
    int k = -1;
    if (i != k)
    {
      localObject1 = f;
      k = g;
      localObject1 = ((ArrayList)localObject1).get(k);
      bool1 = localObject1 instanceof b;
      if (bool1)
      {
        localObject1 = f;
        k = g;
        localObject1 = (b)((ArrayList)localObject1).get(k);
        a((b)localObject1);
        ((b)localObject1).a();
      }
    }
    boolean bool1 = false;
    Object localObject1 = null;
    Object localObject2 = f;
    int m = ((ArrayList)localObject2).size();
    int j;
    for (int n = 0; n < m; n = j)
    {
      j = g;
      if (n != j)
      {
        c localc = (c)f.get(n);
        localObject1 = h();
        k = a.c.ic_visibility_on;
        Drawable localDrawable1 = android.support.v4.a.a.a((Context)localObject1, k);
        localObject1 = h();
        k = a.c.ic_visibility_off;
        Drawable localDrawable2 = android.support.v4.a.a.a((Context)localObject1, k);
        j = a.f.action_hide;
        String str1 = a(j);
        j = a.f.action_show;
        String str2 = a(j);
        localObject1 = new org/npci/upi/security/pinactivitycomponent/s;
        localObject2 = this;
        ((s)localObject1).<init>(this, localc, str1, str2, localDrawable2, localDrawable1);
        boolean bool2 = true;
        boolean bool3 = true;
        localc.a(str2, localDrawable1, (View.OnClickListener)localObject1, 0, bool2, bool3);
      }
      j = n + 1;
    }
  }
  
  private void a(View paramView)
  {
    int i = a.d.main_inner_layout;
    LinearLayout localLinearLayout = (LinearLayout)paramView.findViewById(i);
    Object localObject1 = c;
    int n;
    Object localObject3;
    int i1;
    String str;
    if (localObject1 != null)
    {
      int j = 0;
      localObject1 = null;
      n = 0;
      localObject1 = c;
      j = ((JSONArray)localObject1).length();
      if (n < j)
      {
        for (;;)
        {
          try
          {
            localObject1 = c;
            localJSONObject = ((JSONArray)localObject1).getJSONObject(n);
            localObject1 = "subtype";
            localObject3 = localJSONObject.getString((String)localObject1);
            localObject1 = "dLength";
            j = localJSONObject.optInt((String)localObject1);
            if (j != 0) {
              continue;
            }
            j = 6;
            i1 = j;
            localObject1 = "MPIN";
            boolean bool1 = ((String)localObject3).equals(localObject1);
            if (!bool1)
            {
              localObject1 = "NMPIN";
              bool1 = ((String)localObject3).equals(localObject1);
              if (!bool1)
              {
                localObject1 = "ATMPIN";
                bool1 = ((String)localObject1).equals(localObject3);
                if (!bool1)
                {
                  localObject1 = "OTP";
                  bool1 = ((String)localObject1).equals(localObject3);
                  if (!bool1)
                  {
                    localObject1 = "SMS";
                    bool1 = ((String)localObject1).equals(localObject3);
                    if (!bool1)
                    {
                      localObject1 = "EMAIL";
                      bool1 = ((String)localObject1).equals(localObject3);
                      if (!bool1)
                      {
                        localObject1 = "HOTP";
                        bool1 = ((String)localObject1).equals(localObject3);
                        if (!bool1)
                        {
                          localObject1 = "TOTP";
                          bool1 = ((String)localObject1).equals(localObject3);
                          if (!bool1) {
                            continue;
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
            localObject1 = "NMPIN";
            bool1 = ((String)localObject3).equals(localObject1);
            if (!bool1)
            {
              localObject1 = "MPIN";
              bool1 = ((String)localObject3).equals(localObject1);
              if (!bool1) {
                continue;
              }
              bool1 = N();
              if (!bool1) {
                continue;
              }
            }
            k = a.f.npci_set_mpin_title;
            localObject1 = a(k);
            localObject1 = a((String)localObject1, n, i1);
            int i2 = a.f.npci_confirm_mpin_title;
            localObject3 = a(i2);
            localObject3 = a((String)localObject3, n, i1);
            localObject4 = new java/util/ArrayList;
            ((ArrayList)localObject4).<init>();
            ((ArrayList)localObject4).add(localObject1);
            ((ArrayList)localObject4).add(localObject3);
            localObject1 = new org/npci/upi/security/pinactivitycomponent/widget/a;
            localObject3 = h();
            ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject1).<init>((Context)localObject3);
            ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject1).a((ArrayList)localObject4, this);
            ((org.npci.upi.security.pinactivitycomponent.widget.a)localObject1).setFormDataTag(localJSONObject);
            localObject3 = f;
            ((ArrayList)localObject3).add(localObject1);
            localLinearLayout.addView((View)localObject1);
          }
          catch (Exception localException)
          {
            JSONObject localJSONObject;
            localObject3 = ar;
            Object localObject4 = "Error while inflating Layout";
            Log.e((String)localObject3, (String)localObject4, localException);
            continue;
            str = "OTP";
            boolean bool4 = str.equals(localObject3);
            if (bool4) {
              continue;
            }
            str = "SMS";
            bool4 = str.equals(localObject3);
            if (bool4) {
              continue;
            }
            str = "EMAIL";
            bool4 = str.equals(localObject3);
            if (bool4) {
              continue;
            }
            str = "HOTP";
            bool4 = str.equals(localObject3);
            if (bool4) {
              continue;
            }
            str = "TOTP";
            bool4 = str.equals(localObject3);
            if (!bool4) {
              break label733;
            }
            int k = a.f.npci_otp_title;
            localObject3 = a(k);
            g = n;
            localObject2 = h();
            bool2 = localObject2 instanceof GetCredential;
            if (!bool2) {
              break label769;
            }
          }
          k = n + 1;
          n = k;
          break;
          localObject1 = "dLength";
          k = localJSONObject.optInt((String)localObject1);
          i1 = k;
        }
        localObject1 = "";
        str = "MPIN";
        bool4 = ((String)localObject3).equals(str);
        if (bool4)
        {
          k = a.f.npci_mpin_title;
          localObject1 = a(k);
        }
      }
    }
    for (;;)
    {
      localObject1 = a((String)localObject1, n, i1);
      ((b)localObject1).setFormDataTag(localJSONObject);
      localObject3 = f;
      ((ArrayList)localObject3).add(localObject1);
      localLinearLayout.addView((View)localObject1);
      break;
      Object localObject2 = h();
      localObject2 = (GetCredential)localObject2;
      boolean bool2 = ((GetCredential)localObject2).n();
      if (bool2)
      {
        c(i1);
        localObject2 = localObject3;
        continue;
        label733:
        str = "ATMPIN";
        boolean bool3 = str.equals(localObject3);
        if (bool3)
        {
          int m = a.f.npci_atm_title;
          localObject2 = a(m);
        }
      }
      else
      {
        label769:
        localObject2 = localObject3;
      }
    }
  }
  
  public View a(LayoutInflater paramLayoutInflater, ViewGroup paramViewGroup, Bundle paramBundle)
  {
    int i = a.e.fragment_pin;
    return paramLayoutInflater.inflate(i, paramViewGroup, false);
  }
  
  public void a()
  {
    int i = as;
    ArrayList localArrayList = f;
    int k = localArrayList.size() + -1;
    if (i < k)
    {
      Object localObject = f;
      k = as + 1;
      localObject = (c)((ArrayList)localObject).get(k);
      boolean bool = ((c)localObject).d();
      if (bool)
      {
        int j = as + 1;
        as = j;
        j = as;
        localArrayList = f;
        k = localArrayList.size() + -1;
        if (j >= k) {
          M();
        }
      }
    }
    for (;;)
    {
      return;
      M();
    }
  }
  
  public void a(int paramInt, String paramString)
  {
    int i = g;
    int j = -1;
    if (i != j)
    {
      i = g;
      if (i == paramInt)
      {
        Object localObject1 = f;
        j = g;
        localObject1 = ((ArrayList)localObject1).get(j);
        boolean bool = localObject1 instanceof b;
        if (bool)
        {
          localObject1 = at;
          a((Timer)localObject1);
          localObject1 = f;
          j = g;
          ((b)((ArrayList)localObject1).get(j)).a(false);
          localObject1 = f;
          j = g;
          ((b)((ArrayList)localObject1).get(j)).a("", null, false, false);
          localObject1 = f;
          j = g;
          localObject1 = (b)((ArrayList)localObject1).get(j);
          Object localObject2 = h();
          int k = a.c.ic_tick_ok;
          localObject2 = android.support.v4.a.a.a((Context)localObject2, k);
          k = 1;
          ((b)localObject1).a((Drawable)localObject2, k);
        }
      }
    }
  }
  
  public void a(View paramView, Bundle paramBundle)
  {
    super.a(paramView, paramBundle);
    int i = a.d.main_inner_layout;
    LinearLayout localLinearLayout = (LinearLayout)paramView.findViewById(i);
    aq = localLinearLayout;
    i = a.d.main_layout;
    localLinearLayout = (LinearLayout)paramView.findViewById(i);
    ap = localLinearLayout;
    K();
    a(paramView);
    O();
  }
  
  public void a(View paramView, String paramString)
  {
    b(paramView, paramString);
  }
  
  public void b(int paramInt)
  {
    Object localObject = f.get(paramInt);
    boolean bool = localObject instanceof org.npci.upi.security.pinactivitycomponent.widget.a;
    if (!bool) {
      as = paramInt;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/r.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */