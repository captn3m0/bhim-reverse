package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.a.a;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Timer;
import org.json.JSONArray;
import org.json.JSONObject;
import org.npci.upi.security.pinactivitycomponent.widget.FormItemEditText;
import org.npci.upi.security.pinactivitycomponent.widget.b;
import org.npci.upi.security.pinactivitycomponent.widget.c;

public abstract class h
  extends Fragment
  implements org.npci.upi.security.pinactivitycomponent.widget.o
{
  protected JSONObject a = null;
  protected Timer ai;
  protected Handler aj;
  protected Runnable ak;
  protected JSONObject al;
  protected JSONArray am;
  protected long an;
  protected Context ao;
  private boolean ap;
  protected JSONObject b = null;
  protected JSONArray c = null;
  protected Timer d = null;
  protected long e = 45000L;
  protected ArrayList f;
  protected int g;
  protected PopupWindow h;
  protected Timer i;
  
  public h()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    f = ((ArrayList)localObject);
    g = -1;
    i = null;
    al = null;
    localObject = new org/json/JSONArray;
    ((JSONArray)localObject).<init>();
    am = ((JSONArray)localObject);
    an = 3000L;
    ap = false;
  }
  
  private void M()
  {
    int j = 2;
    Object localObject1 = null;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int k = 0;
    Object localObject2 = null;
    int n = 0;
    Object localObject3 = null;
    Object localObject4 = null;
    int i1 = 0;
    Object localObject5 = null;
    localObject2 = c;
    k = ((JSONArray)localObject2).length();
    if (n < k) {}
    for (;;)
    {
      try
      {
        localObject2 = c;
        localObject2 = ((JSONArray)localObject2).get(n);
        localObject2 = (JSONObject)localObject2;
        localObject6 = "subtype";
        String str = "";
        localObject2 = ((JSONObject)localObject2).optString((String)localObject6, str);
        localObject6 = "ATMPIN";
        boolean bool2 = ((String)localObject2).equals(localObject6);
        if (bool2)
        {
          localObject6 = c;
          localObject5 = ((JSONArray)localObject6).getJSONObject(n);
        }
        localObject6 = "OTP|SMS|HOTP|TOTP";
        bool2 = ((String)localObject2).matches((String)localObject6);
        if (bool2)
        {
          localObject6 = c;
          localObject4 = ((JSONArray)localObject6).getJSONObject(n);
        }
        localObject6 = "MPIN";
        bool2 = ((String)localObject2).equals(localObject6);
        if (bool2)
        {
          localObject6 = c;
          localObject3 = ((JSONArray)localObject6).getJSONObject(n);
        }
        localObject6 = "NMPIN";
        boolean bool1 = ((String)localObject2).equals(localObject6);
        if (!bool1) {
          break label478;
        }
        localObject2 = c;
        localObject2 = ((JSONArray)localObject2).getJSONObject(n);
        localObject1 = localObject3;
        localObject3 = localObject4;
        localObject4 = localObject5;
      }
      catch (Exception localException)
      {
        localObject2 = localObject3;
        localObject3 = localObject4;
        localObject4 = localObject5;
        localObject5 = localException;
        Object localObject6 = "NPCIFragment";
        g.a((String)localObject6, localException);
        Object localObject7 = localObject1;
        localObject1 = localObject2;
        localObject2 = localObject7;
        continue;
      }
      i1 = n + 1;
      n = i1;
      localObject5 = localObject4;
      localObject4 = localObject3;
      localObject3 = localObject1;
      localObject1 = localObject2;
      break;
      localObject2 = c;
      int m = ((JSONArray)localObject2).length();
      n = 3;
      if ((m == n) && (localObject5 != null) && (localObject4 != null) && (localObject3 != null))
      {
        localArrayList.add(localObject4);
        localArrayList.add(localObject5);
        localArrayList.add(localObject3);
      }
      localObject2 = c;
      m = ((JSONArray)localObject2).length();
      if ((m == j) && (localObject4 != null) && (localObject3 != null))
      {
        localArrayList.add(localObject4);
        localArrayList.add(localObject3);
      }
      localObject2 = c;
      m = ((JSONArray)localObject2).length();
      if ((m == j) && (localObject3 != null) && (localObject1 != null))
      {
        localArrayList.add(localObject3);
        localArrayList.add(localObject1);
      }
      m = localArrayList.size();
      if (m > 0)
      {
        localObject2 = new org/json/JSONArray;
        ((JSONArray)localObject2).<init>(localArrayList);
        c = ((JSONArray)localObject2);
      }
      return;
      label478:
      localObject2 = localObject1;
    }
  }
  
  protected void K()
  {
    Object localObject1 = g();
    Object localObject2;
    if (localObject1 != null) {
      localObject2 = "configuration";
    }
    try
    {
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        a = ((JSONObject)localObject3);
      }
      localObject2 = "controls";
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        al = ((JSONObject)localObject3);
        localObject2 = al;
        localObject3 = "CredAllowed";
        localObject2 = ((JSONObject)localObject2).getString((String)localObject3);
        if (localObject2 != null)
        {
          localObject3 = new org/json/JSONArray;
          ((JSONArray)localObject3).<init>((String)localObject2);
          c = ((JSONArray)localObject3);
          M();
        }
      }
      localObject2 = "salt";
      localObject2 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject2 != null)
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>((String)localObject2);
        b = ((JSONObject)localObject3);
      }
      localObject2 = "payInfo";
      localObject1 = ((Bundle)localObject1).getString((String)localObject2);
      if (localObject1 != null)
      {
        localObject2 = new org/json/JSONArray;
        ((JSONArray)localObject2).<init>((String)localObject1);
        am = ((JSONArray)localObject2);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localObject2 = "NPCIFragment";
        Object localObject3 = "Error while reading Arguments";
        Log.e((String)localObject2, (String)localObject3, localException);
      }
    }
  }
  
  protected void L()
  {
    Object localObject1 = a;
    String str = "false";
    localObject1 = ((JSONObject)localObject1).optString("resendOTPFeature", str);
    Object localObject2 = "false";
    boolean bool = ((String)localObject1).equals(localObject2);
    if (!bool)
    {
      bool = ap;
      if (!bool) {
        break label43;
      }
    }
    for (;;)
    {
      return;
      label43:
      localObject1 = h();
      localObject2 = new org/npci/upi/security/pinactivitycomponent/m;
      ((m)localObject2).<init>(this);
      ((android.support.v4.app.l)localObject1).runOnUiThread((Runnable)localObject2);
    }
  }
  
  int a(float paramFloat)
  {
    return (int)(igetDisplayMetricsdensityDpi / 160 * paramFloat);
  }
  
  protected b a(String paramString, int paramInt1, int paramInt2)
  {
    int j = 1107296256;
    float f1 = 32.0F;
    int k = 1;
    LinearLayout.LayoutParams localLayoutParams = new android/widget/LinearLayout$LayoutParams;
    int m = -1;
    float f2 = 0.0F / 0.0F;
    localLayoutParams.<init>(m, -2);
    b localb = new org/npci/upi/security/pinactivitycomponent/widget/b;
    Object localObject1 = h();
    localb.<init>((Context)localObject1);
    localObject1 = c;
    int n = ((JSONArray)localObject1).length();
    if (n == k)
    {
      localb.setActionBarPositionTop(k);
      int i1 = a(240.0F);
      width = i1;
      f2 = 40.0F;
      i1 = a(f2);
      topMargin = i1;
      localb.getFormInputView().setCharSize(0.0F);
      localObject1 = localb.getFormInputView();
      float f3 = a(15.0F);
      ((FormItemEditText)localObject1).setSpace(f3);
      localObject1 = localb.getFormInputView();
      f3 = a(f1);
      ((FormItemEditText)localObject1).setFontSize(f3);
      localObject1 = localb.getFormInputView();
      int i2 = a(f1);
      ((FormItemEditText)localObject1).setPadding(0, i2, 0, 0);
      localObject1 = localb.getFormInputView();
      Object localObject2 = new int[4];
      localObject2[0] = 0;
      j = a(f1);
      localObject2[k] = j;
      localObject2[2] = 0;
      f1 = 4.2E-45F;
      localObject2[3] = 0;
      ((FormItemEditText)localObject1).setMargin((int[])localObject2);
      localb.getFormInputView().setLineStrokeCentered(k);
      localObject1 = localb.getFormInputView();
      i2 = a(2.0F);
      f3 = i2;
      ((FormItemEditText)localObject1).setLineStrokeSelected(f3);
      localObject1 = localb.getFormInputView();
      localObject2 = h();
      j = a.b.form_item_input_colors_transparent;
      localObject2 = a.b((Context)localObject2, j);
      ((FormItemEditText)localObject1).setColorStates((ColorStateList)localObject2);
    }
    localb.setLayoutParams(localLayoutParams);
    localb.setInputLength(paramInt2);
    localb.setFormItemListener(this);
    localb.setTitle(paramString);
    localb.setFormItemTag(paramInt1);
    return localb;
  }
  
  public abstract void a();
  
  public void a(Context paramContext)
  {
    super.a(paramContext);
    ao = paramContext;
  }
  
  public void a(View paramView, Bundle paramBundle)
  {
    super.a(paramView, paramBundle);
    Object localObject = h();
    boolean bool = localObject instanceof GetCredential;
    if (bool)
    {
      localObject = (GetCredential)h();
      ((GetCredential)localObject).a(this);
    }
  }
  
  protected void a(Timer paramTimer)
  {
    if (paramTimer != null) {}
    try
    {
      paramTimer.cancel();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str = "NPCIFragment";
        g.a(str, localException);
      }
    }
  }
  
  protected void a(p paramp)
  {
    try
    {
      int j = g;
      int k = -1;
      if (j != k)
      {
        j = 1;
        ap = j;
        Object localObject = f;
        k = g;
        localObject = ((ArrayList)localObject).get(k);
        localObject = (c)localObject;
        String str = paramp.b();
        ((c)localObject).setText(str);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  protected void a(b paramb)
  {
    boolean bool = true;
    Object localObject = new java/util/Timer;
    ((Timer)localObject).<init>();
    i = ((Timer)localObject);
    localObject = i;
    o localo = new org/npci/upi/security/pinactivitycomponent/o;
    localo.<init>(this);
    long l = e;
    ((Timer)localObject).schedule(localo, l);
    localObject = paramb;
    paramb.a("", null, null, 0, false, false);
    paramb.a(null, false);
    int j = a.f.detecting_otp;
    localObject = a(j);
    paramb.a((String)localObject, null, bool, false);
    paramb.a(bool);
  }
  
  protected void b(View paramView, String paramString)
  {
    Object localObject1 = h;
    if (localObject1 != null)
    {
      localObject1 = h;
      ((PopupWindow)localObject1).dismiss();
    }
    localObject1 = h().getLayoutInflater();
    int j = a.e.layout_popup_view;
    Object localObject2 = ((LayoutInflater)localObject1).inflate(j, null);
    int k = a.d.popup_text;
    ((TextView)((View)localObject2).findViewById(k)).setText(paramString);
    localObject1 = new android/widget/PopupWindow;
    int m = a(60.0F);
    ((PopupWindow)localObject1).<init>((View)localObject2, -2, m);
    h = ((PopupWindow)localObject1);
    localObject1 = h;
    int n = a.g.PopupAnimation;
    ((PopupWindow)localObject1).setAnimationStyle(n);
    h.showAtLocation(paramView, 17, 0, 0);
    k = a.d.popup_button;
    localObject1 = ((View)localObject2).findViewById(k);
    localObject2 = new org/npci/upi/security/pinactivitycomponent/k;
    ((k)localObject2).<init>(this);
    ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject1 = new java/util/Timer;
    ((Timer)localObject1).<init>();
    ai = ((Timer)localObject1);
    localObject1 = new android/os/Handler;
    localObject2 = Looper.getMainLooper();
    ((Handler)localObject1).<init>((Looper)localObject2);
    aj = ((Handler)localObject1);
    localObject1 = new org/npci/upi/security/pinactivitycomponent/l;
    ((l)localObject1).<init>(this);
    ak = ((Runnable)localObject1);
    localObject1 = aj;
    localObject2 = ak;
    long l = an;
    ((Handler)localObject1).postDelayed((Runnable)localObject2, l);
  }
  
  public void b(p paramp)
  {
    a(paramp);
  }
  
  public void c(int paramInt)
  {
    Object localObject = h();
    if (localObject != null)
    {
      localObject = h();
      boolean bool = localObject instanceof GetCredential;
      if (bool)
      {
        localObject = (GetCredential)h();
        ((GetCredential)localObject).b(paramInt);
      }
    }
    q localq = new org/npci/upi/security/pinactivitycomponent/q;
    localObject = h();
    localq.<init>((Context)localObject);
    localObject = new java/util/Timer;
    ((Timer)localObject).<init>();
    d = ((Timer)localObject);
    localObject = d;
    i locali = new org/npci/upi/security/pinactivitycomponent/i;
    locali.<init>(this, localq, paramInt);
    ((Timer)localObject).scheduleAtFixedRate(locali, 100, 2000L);
  }
  
  public void q()
  {
    super.q();
    Object localObject = d;
    a((Timer)localObject);
    localObject = i;
    a((Timer)localObject);
    localObject = ai;
    a((Timer)localObject);
    localObject = aj;
    if (localObject != null)
    {
      localObject = ak;
      if (localObject != null)
      {
        localObject = aj;
        Runnable localRunnable = ak;
        ((Handler)localObject).removeCallbacks(localRunnable);
      }
    }
    localObject = h;
    if (localObject != null)
    {
      localObject = h;
      ((PopupWindow)localObject).dismiss();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */