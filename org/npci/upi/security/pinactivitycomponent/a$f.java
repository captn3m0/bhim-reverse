package org.npci.upi.security.pinactivitycomponent;

public final class a$f
{
  public static final int abc_action_bar_home_description = 2131165184;
  public static final int abc_action_bar_home_description_format = 2131165185;
  public static final int abc_action_bar_home_subtitle_description_format = 2131165186;
  public static final int abc_action_bar_up_description = 2131165187;
  public static final int abc_action_menu_overflow_description = 2131165188;
  public static final int abc_action_mode_done = 2131165189;
  public static final int abc_activity_chooser_view_see_all = 2131165190;
  public static final int abc_activitychooserview_choose_application = 2131165191;
  public static final int abc_capital_off = 2131165192;
  public static final int abc_capital_on = 2131165193;
  public static final int abc_search_hint = 2131165194;
  public static final int abc_searchview_description_clear = 2131165195;
  public static final int abc_searchview_description_query = 2131165196;
  public static final int abc_searchview_description_search = 2131165197;
  public static final int abc_searchview_description_submit = 2131165198;
  public static final int abc_searchview_description_voice = 2131165199;
  public static final int abc_shareactionprovider_share_with = 2131165200;
  public static final int abc_shareactionprovider_share_with_application = 2131165201;
  public static final int abc_toolbar_collapse_description = 2131165202;
  public static final int action_hide = 2131165236;
  public static final int action_resend = 2131165237;
  public static final int action_show = 2131165238;
  public static final int app_name = 2131165239;
  public static final int back_button_exit_message = 2131165240;
  public static final int componentMessage = 2131165241;
  public static final int detecting_otp = 2131165242;
  public static final int dismiss = 2131165243;
  public static final int error_msg = 2131165244;
  public static final int go_back = 2131165245;
  public static final int info = 2131165246;
  public static final int info_pins_dont_match = 2131165247;
  public static final int invalid_otp = 2131165248;
  public static final int not_right = 2131165249;
  public static final int npci_atm_title = 2131165250;
  public static final int npci_confirm_mpin_title = 2131165251;
  public static final int npci_forgot_upi_pin = 2131165257;
  public static final int npci_mpin_title = 2131165252;
  public static final int npci_new_mpin_title = 2131165253;
  public static final int npci_otp_title = 2131165254;
  public static final int npci_set_mpin_title = 2131165255;
  public static final int ref_id = 2131165256;
  public static final int status_bar_notification_info_overflow = 2131165235;
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/a$f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */