package org.npci.upi.security.pinactivitycomponent;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;

public class ao
{
  public static String a(Object paramObject)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    a(paramObject, localStringBuilder);
    return localStringBuilder.toString();
  }
  
  private static void a(Object paramObject, StringBuilder paramStringBuilder)
  {
    int i = 0;
    Object localObject1 = null;
    if (paramObject == null)
    {
      localObject1 = "null";
      paramStringBuilder.append((String)localObject1);
    }
    for (;;)
    {
      return;
      Object localObject2 = paramObject.getClass();
      boolean bool1 = ((Class)localObject2).isArray();
      int k;
      Object localObject3;
      if (bool1)
      {
        localObject2 = "[";
        paramStringBuilder.append((String)localObject2);
        for (;;)
        {
          k = Array.getLength(paramObject);
          if (i >= k) {
            break;
          }
          a(Array.get(paramObject, i), paramStringBuilder);
          localObject2 = ",";
          paramStringBuilder.append((String)localObject2);
          i += 1;
        }
        i = paramStringBuilder.length() + -1;
        k = paramStringBuilder.length();
        localObject3 = "]";
        paramStringBuilder.replace(i, k, (String)localObject3);
      }
      else
      {
        localObject3 = String.class;
        bool1 = localObject2.equals(localObject3);
        if (bool1)
        {
          localObject1 = paramStringBuilder.append("\"").append(paramObject);
          localObject2 = "\"";
          ((StringBuilder)localObject1).append((String)localObject2);
        }
        else
        {
          bool1 = ((Class)localObject2).isPrimitive();
          if (!bool1)
          {
            localObject3 = Integer.class;
            bool1 = localObject2.equals(localObject3);
            if (!bool1)
            {
              localObject3 = Long.class;
              bool1 = localObject2.equals(localObject3);
              if (!bool1)
              {
                localObject3 = Short.class;
                bool1 = localObject2.equals(localObject3);
                if (!bool1)
                {
                  localObject3 = Double.class;
                  bool1 = localObject2.equals(localObject3);
                  if (!bool1)
                  {
                    localObject3 = Float.class;
                    bool1 = localObject2.equals(localObject3);
                    if (!bool1)
                    {
                      localObject3 = BigDecimal.class;
                      bool1 = localObject2.equals(localObject3);
                      if (!bool1) {
                        break label293;
                      }
                    }
                  }
                }
              }
            }
          }
          localObject1 = String.valueOf(paramObject);
          paramStringBuilder.append((String)localObject1);
          continue;
          label293:
          localObject3 = "{";
          try
          {
            paramStringBuilder.append((String)localObject3);
            localObject2 = ((Class)localObject2).getDeclaredFields();
            int j = localObject2.length;
            while (i < j)
            {
              Object localObject4 = localObject2[i];
              int m = ((Field)localObject4).getModifiers();
              boolean bool2 = Modifier.isStatic(m);
              if (!bool2)
              {
                bool2 = true;
                ((Field)localObject4).setAccessible(bool2);
                Object localObject5 = "\"";
                localObject5 = paramStringBuilder.append((String)localObject5);
                String str = ((Field)localObject4).getName();
                localObject5 = ((StringBuilder)localObject5).append(str);
                str = "\"";
                localObject5 = ((StringBuilder)localObject5).append(str);
                str = ":";
                ((StringBuilder)localObject5).append(str);
                localObject4 = ((Field)localObject4).get(paramObject);
                a(localObject4, paramStringBuilder);
                localObject4 = ",";
                paramStringBuilder.append((String)localObject4);
              }
              i += 1;
            }
            i = paramStringBuilder.length() + -1;
            k = paramStringBuilder.length();
            localObject3 = "}";
            paramStringBuilder.replace(i, k, (String)localObject3);
          }
          catch (Exception localException)
          {
            localException.printStackTrace();
          }
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/ao.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */