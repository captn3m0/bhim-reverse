package org.npci.upi.security.pinactivitycomponent;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.f.af;
import android.view.View;

class al
  extends AnimatorListenerAdapter
{
  al(GetCredential paramGetCredential, boolean paramBoolean, int paramInt) {}
  
  public void onAnimationEnd(Animator paramAnimator)
  {
    int i = 8;
    super.onAnimationEnd(paramAnimator);
    boolean bool = a;
    if (!bool)
    {
      GetCredential.e(c).setVisibility(i);
      GetCredential.f(c).setVisibility(i);
      TransitionDrawable localTransitionDrawable = GetCredential.d(c);
      localTransitionDrawable.resetTransition();
    }
  }
  
  public void onAnimationStart(Animator paramAnimator)
  {
    int i = 300;
    int j = 0;
    float f1 = 0.0F;
    super.onAnimationStart(paramAnimator);
    boolean bool = a;
    Object localObject;
    if (bool)
    {
      GetCredential.d(c).startTransition(i);
      GetCredential.e(c).setVisibility(0);
      GetCredential.f(c).setVisibility(0);
      localObject = GetCredential.e(c);
      float f2 = af.l((View)localObject);
      j = 0;
      f1 = 0.0F;
      bool = f2 < 0.0F;
      if (!bool)
      {
        localObject = GetCredential.e(c);
        j = b * -1;
        f1 = j;
        ((View)localObject).setY(f1);
      }
    }
    for (;;)
    {
      return;
      localObject = GetCredential.d(c);
      ((TransitionDrawable)localObject).reverseTransition(i);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/al.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */