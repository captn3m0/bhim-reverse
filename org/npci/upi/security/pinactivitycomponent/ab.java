package org.npci.upi.security.pinactivitycomponent;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

public class ab
  extends SQLiteOpenHelper
{
  public ab(Context paramContext)
  {
    super(paramContext, "contactsManager", null, 1);
  }
  
  public String a(String paramString1, String paramString2, String paramString3)
  {
    String str = null;
    List localList = a();
    if (localList != null)
    {
      boolean bool = localList.isEmpty();
      if (!bool) {
        str = ((u)localList.get(0)).a();
      }
    }
    return str;
  }
  
  public List a()
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject = getWritableDatabase();
    int i = 0;
    String str = null;
    Cursor localCursor = ((SQLiteDatabase)localObject).rawQuery("SELECT  * FROM contacts", null);
    boolean bool = localCursor.moveToFirst();
    if (bool) {
      do
      {
        localObject = new org/npci/upi/security/pinactivitycomponent/u;
        ((u)localObject).<init>();
        str = localCursor.getString(1);
        ((u)localObject).a(str);
        str = localCursor.getString(2);
        ((u)localObject).b(str);
        i = 3;
        str = localCursor.getString(i);
        ((u)localObject).c(str);
        localArrayList.add(localObject);
        bool = localCursor.moveToNext();
      } while (bool);
    }
    return localArrayList;
  }
  
  void a(u paramu)
  {
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    String str = paramu.a();
    localContentValues.put("k0", str);
    str = paramu.b();
    localContentValues.put("token", str);
    str = paramu.c();
    localContentValues.put("date", str);
    localSQLiteDatabase.insert("contacts", null, localContentValues);
    localSQLiteDatabase.close();
  }
  
  public int b(u paramu)
  {
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    ContentValues localContentValues = new android/content/ContentValues;
    localContentValues.<init>();
    String str1 = paramu.a();
    localContentValues.put("k0", str1);
    str1 = paramu.b();
    localContentValues.put("token", str1);
    str1 = paramu.c();
    localContentValues.put("date", str1);
    String[] arrayOfString = new String[1];
    String str2 = paramu.a();
    arrayOfString[0] = str2;
    return localSQLiteDatabase.update("contacts", localContentValues, "k0 = ?", arrayOfString);
  }
  
  public String b()
  {
    String str = "";
    List localList = a();
    if (localList != null)
    {
      boolean bool = localList.isEmpty();
      if (!bool) {
        str = ((u)localList.get(0)).b();
      }
    }
    return str;
  }
  
  public void c()
  {
    g.b("DB Handler", "Deleting all");
    SQLiteDatabase localSQLiteDatabase = getWritableDatabase();
    localSQLiteDatabase.execSQL("delete from contacts");
    localSQLiteDatabase.close();
  }
  
  public void onCreate(SQLiteDatabase paramSQLiteDatabase)
  {
    paramSQLiteDatabase.execSQL("CREATE TABLE contacts(id INTEGER PRIMARY KEY,k0 TEXT,token TEXT,date TEXT)");
    Log.e("Dynamic DB", "tables created");
  }
  
  public void onUpgrade(SQLiteDatabase paramSQLiteDatabase, int paramInt1, int paramInt2)
  {
    paramSQLiteDatabase.execSQL("DROP TABLE IF EXISTS contacts");
    onCreate(paramSQLiteDatabase);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/ab.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */