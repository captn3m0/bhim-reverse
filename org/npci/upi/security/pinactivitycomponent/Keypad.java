package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.support.v4.a.a;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.TableLayout;
import android.widget.TableLayout.LayoutParams;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

public class Keypad
  extends TableLayout
{
  private int a = 61;
  private int b;
  private int c;
  private float d;
  private f e;
  
  public Keypad(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public Keypad(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramAttributeSet);
    a();
  }
  
  private int a(float paramFloat)
  {
    return (int)(getResourcesgetDisplayMetricsdensityDpi / 160 * paramFloat);
  }
  
  private void a()
  {
    int i = 3;
    int j = 2;
    float f1 = 3.0F;
    int k = 0;
    float f2 = 0.0F;
    Object localObject1 = null;
    int m = 1;
    int n = b;
    setBackgroundColor(n);
    TableLayout.LayoutParams localLayoutParams = new android/widget/TableLayout$LayoutParams;
    int i1 = 1065353216;
    float f3 = 1.0F;
    localLayoutParams.<init>(-1, 0, f3);
    int i2 = 0;
    Object localObject2 = null;
    for (n = m; i2 < i; n = i1)
    {
      TableRow localTableRow = new android/widget/TableRow;
      localObject3 = getContext();
      localTableRow.<init>((Context)localObject3);
      localTableRow.setLayoutParams(localLayoutParams);
      localTableRow.setWeightSum(f1);
      i1 = n;
      n = 0;
      localImageView = null;
      while (n < i)
      {
        TextView localTextView = new android/widget/TextView;
        Object localObject4 = getContext();
        localTextView.<init>((Context)localObject4);
        localTextView.setGravity(17);
        localObject4 = getItemParams();
        localTextView.setLayoutParams((ViewGroup.LayoutParams)localObject4);
        int i3 = c;
        localTextView.setTextColor(i3);
        float f4 = d;
        localTextView.setTextSize(j, f4);
        localObject4 = String.valueOf(i1);
        localTextView.setText((CharSequence)localObject4);
        localTextView.setClickable(m);
        setClickFeedback(localTextView);
        localObject4 = new org/npci/upi/security/pinactivitycomponent/aq;
        ((aq)localObject4).<init>(this, i1);
        localTextView.setOnClickListener((View.OnClickListener)localObject4);
        localTableRow.addView(localTextView);
        i1 += 1;
        n += 1;
      }
      addView(localTableRow);
      i2 += 1;
    }
    ImageView localImageView = new android/widget/ImageView;
    Object localObject3 = getContext();
    localImageView.<init>((Context)localObject3);
    i1 = a.c.ic_action_backspace;
    localImageView.setImageResource(i1);
    localObject3 = getItemParams();
    localImageView.setLayoutParams((ViewGroup.LayoutParams)localObject3);
    localImageView.setClickable(m);
    setClickFeedback(localImageView);
    localObject3 = new org/npci/upi/security/pinactivitycomponent/ar;
    ((ar)localObject3).<init>(this);
    localImageView.setOnClickListener((View.OnClickListener)localObject3);
    localObject3 = new android/widget/TextView;
    localObject2 = getContext();
    ((TextView)localObject3).<init>((Context)localObject2);
    localObject2 = getItemParams();
    ((TextView)localObject3).setLayoutParams((ViewGroup.LayoutParams)localObject2);
    ((TextView)localObject3).setGravity(17);
    localObject1 = String.valueOf(0);
    ((TextView)localObject3).setText((CharSequence)localObject1);
    k = c;
    ((TextView)localObject3).setTextColor(k);
    f2 = d;
    ((TextView)localObject3).setTextSize(j, f2);
    ((TextView)localObject3).setClickable(m);
    setClickFeedback((View)localObject3);
    localObject1 = new org/npci/upi/security/pinactivitycomponent/c;
    ((c)localObject1).<init>(this);
    ((TextView)localObject3).setOnClickListener((View.OnClickListener)localObject1);
    localObject1 = new android/widget/ImageView;
    localObject2 = getContext();
    ((ImageView)localObject1).<init>((Context)localObject2);
    i2 = a.c.ic_action_submit;
    ((ImageView)localObject1).setImageResource(i2);
    localObject2 = ImageView.ScaleType.CENTER_INSIDE;
    ((ImageView)localObject1).setScaleType((ImageView.ScaleType)localObject2);
    ((ImageView)localObject1).setAdjustViewBounds(m);
    localObject2 = getItemParams();
    float f5 = a;
    int i4 = (int)(a(f5) * 1.25F);
    height = i4;
    ((ImageView)localObject1).setLayoutParams((ViewGroup.LayoutParams)localObject2);
    ((ImageView)localObject1).setClickable(m);
    setClickFeedback((View)localObject1);
    localObject2 = new org/npci/upi/security/pinactivitycomponent/e;
    ((e)localObject2).<init>(this);
    ((ImageView)localObject1).setOnClickListener((View.OnClickListener)localObject2);
    localObject2 = new android/widget/TableRow;
    Context localContext = getContext();
    ((TableRow)localObject2).<init>(localContext);
    ((TableRow)localObject2).setLayoutParams(localLayoutParams);
    ((TableRow)localObject2).setWeightSum(f1);
    ((TableRow)localObject2).addView(localImageView);
    ((TableRow)localObject2).addView((View)localObject3);
    ((TableRow)localObject2).addView((View)localObject1);
    addView((View)localObject2);
  }
  
  private void a(AttributeSet paramAttributeSet)
  {
    Object localObject = getContext();
    int[] arrayOfInt = a.h.Keypad;
    localObject = ((Context)localObject).obtainStyledAttributes(paramAttributeSet, arrayOfInt, 0, 0);
    int i = a.h.Keypad_keypad_bg_color;
    Context localContext = getContext();
    int j = a.b.npci_keypad_bg_color;
    int k = a.c(localContext, j);
    i = ((TypedArray)localObject).getColor(i, k);
    b = i;
    i = a.h.Keypad_key_digit_color;
    localContext = getContext();
    j = a.b.npci_key_digit_color;
    k = a.c(localContext, j);
    i = ((TypedArray)localObject).getColor(i, k);
    c = i;
    i = a.h.Keypad_key_digit_size;
    float f = ((TypedArray)localObject).getDimensionPixelSize(i, 36);
    d = f;
    i = a.h.Keypad_key_digit_height;
    k = a;
    i = ((TypedArray)localObject).getDimensionPixelSize(i, k);
    a = i;
    ((TypedArray)localObject).recycle();
  }
  
  private TableRow.LayoutParams getItemParams()
  {
    TableRow.LayoutParams localLayoutParams = new android/widget/TableRow$LayoutParams;
    float f = a;
    int i = a(f);
    localLayoutParams.<init>(0, i, 1.0F);
    return localLayoutParams;
  }
  
  private void setClickFeedback(View paramView)
  {
    TypedValue localTypedValue = new android/util/TypedValue;
    localTypedValue.<init>();
    Resources.Theme localTheme = getContext().getTheme();
    int i = a.a.selectableItemBackground;
    localTheme.resolveAttribute(i, localTypedValue, true);
    int j = resourceId;
    paramView.setBackgroundResource(j);
  }
  
  public void setOnKeyPressCallback(f paramf)
  {
    e = paramf;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/Keypad.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */