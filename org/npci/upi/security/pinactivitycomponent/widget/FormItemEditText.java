package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.v4.f.af;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.ActionMode.Callback;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.animation.OvershootInterpolator;
import android.widget.EditText;
import java.util.Locale;
import org.npci.upi.security.pinactivitycomponent.a.a;
import org.npci.upi.security.pinactivitycomponent.a.h;

public class FormItemEditText
  extends EditText
{
  private int[][] A;
  private int[] B;
  private ColorStateList C;
  private String a = null;
  private StringBuilder b = null;
  private String c = null;
  private int d = 0;
  private float e = 24.0F;
  private float f;
  private float g = 4.0F;
  private float h = 8.0F;
  private int i;
  private RectF[] j;
  private float[] k;
  private Paint l;
  private Paint m;
  private Paint n;
  private Drawable o;
  private Rect p;
  private boolean q;
  private View.OnClickListener r;
  private k s;
  private boolean t;
  private float u;
  private float v;
  private Paint w;
  private boolean x;
  private boolean y;
  private ColorStateList z;
  
  public FormItemEditText(Context paramContext)
  {
    super(paramContext);
    i = i1;
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    p = ((Rect)localObject1);
    q = false;
    s = null;
    u = 1.0F;
    v = 2.0F;
    x = false;
    y = false;
    localObject1 = new int[i1][];
    Object localObject2 = new int[i2];
    localObject2[0] = 16842913;
    localObject1[0] = localObject2;
    localObject2 = new int[i2];
    localObject2[0] = 16842914;
    localObject1[i2] = localObject2;
    int[] arrayOfInt = new int[i2];
    arrayOfInt[0] = 16842908;
    localObject1[2] = arrayOfInt;
    arrayOfInt = new int[i2];
    arrayOfInt[0] = -16842908;
    localObject1[3] = arrayOfInt;
    A = ((int[][])localObject1);
    localObject1 = new int[i1];
    Object tmp187_185 = localObject1;
    Object tmp188_187 = tmp187_185;
    Object tmp188_187 = tmp187_185;
    tmp188_187[0] = -16711936;
    tmp188_187[1] = -65536;
    tmp188_187[2] = -16777216;
    tmp188_187[3] = -7829368;
    B = ((int[])localObject1);
    localObject1 = new android/content/res/ColorStateList;
    localObject2 = A;
    arrayOfInt = B;
    ((ColorStateList)localObject1).<init>((int[][])localObject2, arrayOfInt);
    C = ((ColorStateList)localObject1);
  }
  
  public FormItemEditText(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    i = i1;
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    p = ((Rect)localObject1);
    q = false;
    s = null;
    u = 1.0F;
    v = 2.0F;
    x = false;
    y = false;
    localObject1 = new int[i1][];
    Object localObject2 = new int[i2];
    localObject2[0] = 16842913;
    localObject1[0] = localObject2;
    localObject2 = new int[i2];
    localObject2[0] = 16842914;
    localObject1[i2] = localObject2;
    int[] arrayOfInt = new int[i2];
    arrayOfInt[0] = 16842908;
    localObject1[2] = arrayOfInt;
    arrayOfInt = new int[i2];
    arrayOfInt[0] = -16842908;
    localObject1[3] = arrayOfInt;
    A = ((int[][])localObject1);
    localObject1 = new int[i1];
    Object tmp194_192 = localObject1;
    Object tmp195_194 = tmp194_192;
    Object tmp195_194 = tmp194_192;
    tmp195_194[0] = -16711936;
    tmp195_194[1] = -65536;
    tmp195_194[2] = -16777216;
    tmp195_194[3] = -7829368;
    B = ((int[])localObject1);
    localObject1 = new android/content/res/ColorStateList;
    localObject2 = A;
    arrayOfInt = B;
    ((ColorStateList)localObject1).<init>((int[][])localObject2, arrayOfInt);
    C = ((ColorStateList)localObject1);
    a(paramContext, paramAttributeSet);
  }
  
  public FormItemEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    i = i1;
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    p = ((Rect)localObject1);
    q = false;
    s = null;
    u = 1.0F;
    v = 2.0F;
    x = false;
    y = false;
    localObject1 = new int[i1][];
    Object localObject2 = new int[i2];
    localObject2[0] = 16842913;
    localObject1[0] = localObject2;
    localObject2 = new int[i2];
    localObject2[0] = 16842914;
    localObject1[i2] = localObject2;
    int[] arrayOfInt = new int[i2];
    arrayOfInt[0] = 16842908;
    localObject1[2] = arrayOfInt;
    arrayOfInt = new int[i2];
    arrayOfInt[0] = -16842908;
    localObject1[3] = arrayOfInt;
    A = ((int[][])localObject1);
    localObject1 = new int[i1];
    Object tmp199_197 = localObject1;
    Object tmp200_199 = tmp199_197;
    Object tmp200_199 = tmp199_197;
    tmp200_199[0] = -16711936;
    tmp200_199[1] = -65536;
    tmp200_199[2] = -16777216;
    tmp200_199[3] = -7829368;
    B = ((int[])localObject1);
    localObject1 = new android/content/res/ColorStateList;
    localObject2 = A;
    arrayOfInt = B;
    ((ColorStateList)localObject1).<init>((int[][])localObject2, arrayOfInt);
    C = ((ColorStateList)localObject1);
    a(paramContext, paramAttributeSet);
  }
  
  public FormItemEditText(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    i = i1;
    Object localObject1 = new android/graphics/Rect;
    ((Rect)localObject1).<init>();
    p = ((Rect)localObject1);
    q = false;
    s = null;
    u = 1.0F;
    v = 2.0F;
    x = false;
    y = false;
    localObject1 = new int[i1][];
    Object localObject2 = new int[i2];
    localObject2[0] = 16842913;
    localObject1[0] = localObject2;
    localObject2 = new int[i2];
    localObject2[0] = 16842914;
    localObject1[i2] = localObject2;
    int[] arrayOfInt = new int[i2];
    arrayOfInt[0] = 16842908;
    localObject1[2] = arrayOfInt;
    arrayOfInt = new int[i2];
    arrayOfInt[0] = -16842908;
    localObject1[3] = arrayOfInt;
    A = ((int[][])localObject1);
    localObject1 = new int[i1];
    Object tmp201_199 = localObject1;
    Object tmp202_201 = tmp201_199;
    Object tmp202_201 = tmp201_199;
    tmp202_201[0] = -16711936;
    tmp202_201[1] = -65536;
    tmp202_201[2] = -16777216;
    tmp202_201[3] = -7829368;
    B = ((int[])localObject1);
    localObject1 = new android/content/res/ColorStateList;
    localObject2 = A;
    arrayOfInt = B;
    ((ColorStateList)localObject1).<init>((int[][])localObject2, arrayOfInt);
    C = ((ColorStateList)localObject1);
    a(paramContext, paramAttributeSet);
  }
  
  private int a(int... paramVarArgs)
  {
    return C.getColorForState(paramVarArgs, -7829368);
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    int i1 = -7829368;
    int i2 = 1;
    float f1 = u;
    f1 = a(f1);
    u = f1;
    f1 = v;
    f1 = a(f1);
    v = f1;
    f1 = e;
    f1 = a(f1);
    e = f1;
    f1 = h;
    int i4 = a(f1);
    f1 = i4;
    h = f1;
    Object localObject1 = a.h.FormItemEditText;
    localObject1 = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1, 0, 0);
    for (;;)
    {
      try
      {
        Object localObject2 = new android/util/TypedValue;
        ((TypedValue)localObject2).<init>();
        int i6 = a.h.FormItemEditText_pinAnimationType;
        ((TypedArray)localObject1).getValue(i6, (TypedValue)localObject2);
        int i8 = data;
        d = i8;
        i8 = a.h.FormItemEditText_pinCharacterMask;
        localObject2 = ((TypedArray)localObject1).getString(i8);
        a = ((String)localObject2);
        i8 = a.h.FormItemEditText_pinRepeatedHint;
        localObject2 = ((TypedArray)localObject1).getString(i8);
        c = ((String)localObject2);
        i8 = a.h.FormItemEditText_pinLineStroke;
        float f2 = u;
        f3 = ((TypedArray)localObject1).getDimension(i8, f2);
        u = f3;
        i8 = a.h.FormItemEditText_pinLineStrokeSelected;
        f2 = v;
        f3 = ((TypedArray)localObject1).getDimension(i8, f2);
        v = f3;
        i8 = a.h.FormItemEditText_pinLineStrokeCentered;
        i6 = 0;
        f2 = 0.0F;
        Rect localRect = null;
        boolean bool4 = ((TypedArray)localObject1).getBoolean(i8, false);
        t = bool4;
        int i9 = a.h.FormItemEditText_pinCharacterSize;
        i6 = 0;
        f2 = 0.0F;
        localRect = null;
        f3 = ((TypedArray)localObject1).getDimension(i9, 0.0F);
        f = f3;
        i9 = a.h.FormItemEditText_pinCharacterSpacing;
        f2 = e;
        f3 = ((TypedArray)localObject1).getDimension(i9, f2);
        e = f3;
        i9 = a.h.FormItemEditText_pinTextBottomPadding;
        f2 = h;
        f3 = ((TypedArray)localObject1).getDimension(i9, f2);
        h = f3;
        i9 = a.h.FormItemEditText_pinBackgroundIsSquare;
        boolean bool3 = q;
        boolean bool5 = ((TypedArray)localObject1).getBoolean(i9, bool3);
        q = bool5;
        i10 = a.h.FormItemEditText_pinBackgroundDrawable;
        localObject2 = ((TypedArray)localObject1).getDrawable(i10);
        o = ((Drawable)localObject2);
        i10 = a.h.FormItemEditText_pinLineColors;
        localObject2 = ((TypedArray)localObject1).getColorStateList(i10);
        if (localObject2 != null) {
          C = ((ColorStateList)localObject2);
        }
        ((TypedArray)localObject1).recycle();
        localObject1 = new android/graphics/Paint;
        localObject2 = getPaint();
        ((Paint)localObject1).<init>((Paint)localObject2);
        l = ((Paint)localObject1);
        localObject1 = new android/graphics/Paint;
        localObject2 = getPaint();
        ((Paint)localObject1).<init>((Paint)localObject2);
        m = ((Paint)localObject1);
        localObject1 = new android/graphics/Paint;
        localObject2 = getPaint();
        ((Paint)localObject1).<init>((Paint)localObject2);
        n = ((Paint)localObject1);
        localObject1 = new android/graphics/Paint;
        localObject2 = getPaint();
        ((Paint)localObject1).<init>((Paint)localObject2);
        w = ((Paint)localObject1);
        localObject1 = w;
        f3 = u;
        ((Paint)localObject1).setStrokeWidth(f3);
        f1 = f;
        setFontSize(f1);
        localObject1 = new android/util/TypedValue;
        ((TypedValue)localObject1).<init>();
        localObject2 = paramContext.getTheme();
        int i7 = a.a.colorControlActivated;
        ((Resources.Theme)localObject2).resolveAttribute(i7, (TypedValue)localObject1, i2);
        i4 = data;
        B[0] = i4;
        B[i2] = i1;
        B[2] = i1;
        setBackgroundResource(0);
        localObject2 = "maxLength";
        i7 = 4;
        f2 = 5.6E-45F;
        i4 = paramAttributeSet.getAttributeIntValue("http://schemas.android.com/apk/res/android", (String)localObject2, i7);
        i = i4;
        f1 = i;
        g = f1;
        localObject1 = new org/npci/upi/security/pinactivitycomponent/widget/d;
        ((d)localObject1).<init>(this);
        super.setOnClickListener((View.OnClickListener)localObject1);
        localObject1 = new org/npci/upi/security/pinactivitycomponent/widget/e;
        ((e)localObject1).<init>(this);
        super.setOnLongClickListener((View.OnLongClickListener)localObject1);
        i4 = getInputType() & 0x80;
        i10 = 128;
        f3 = 1.794E-43F;
        if (i4 == i10)
        {
          localObject1 = a;
          boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
          if (bool1)
          {
            localObject1 = "●";
            a = ((String)localObject1);
            localObject1 = a;
            bool1 = TextUtils.isEmpty((CharSequence)localObject1);
            if (!bool1)
            {
              localObject1 = getMaskChars();
              b = ((StringBuilder)localObject1);
            }
            localObject1 = getPaint();
            localObject2 = "|";
            localRect = p;
            ((TextPaint)localObject1).getTextBounds((String)localObject2, 0, i2, localRect);
            i5 = d;
            i10 = -1;
            f3 = 0.0F / 0.0F;
            if (i5 <= i10) {
              break label923;
            }
            x = i2;
            return;
          }
        }
      }
      finally
      {
        ((TypedArray)localObject1).recycle();
      }
      int i5 = getInputType() & 0x10;
      int i10 = 16;
      float f3 = 2.24E-44F;
      if (i5 == i10)
      {
        localObject1 = a;
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
        if (bool2)
        {
          localObject1 = "●";
          a = ((String)localObject1);
          continue;
          label923:
          int i3 = 0;
          Object localObject4 = null;
        }
      }
    }
  }
  
  private void a(CharSequence paramCharSequence)
  {
    Object localObject1 = m;
    int i1 = 125;
    ((Paint)localObject1).setAlpha(i1);
    int i2 = 2;
    localObject1 = new int[i2];
    Object tmp22_21 = localObject1;
    tmp22_21[0] = 125;
    tmp22_21[1] = 'ÿ';
    localObject1 = ValueAnimator.ofInt((int[])localObject1);
    long l1 = 150L;
    ((ValueAnimator)localObject1).setDuration(l1);
    Object localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/f;
    ((f)localObject2).<init>(this);
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    localObject2 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject2).<init>();
    int i3 = paramCharSequence.length();
    int i4 = i;
    if (i3 == i4)
    {
      localObject3 = s;
      if (localObject3 != null)
      {
        localObject3 = new org/npci/upi/security/pinactivitycomponent/widget/g;
        ((g)localObject3).<init>(this);
        ((AnimatorSet)localObject2).addListener((Animator.AnimatorListener)localObject3);
      }
    }
    Object localObject3 = new Animator[1];
    localObject3[0] = localObject1;
    ((AnimatorSet)localObject2).playTogether((Animator[])localObject3);
    ((AnimatorSet)localObject2).start();
  }
  
  private void a(CharSequence paramCharSequence, int paramInt)
  {
    long l1 = 300L;
    int i1 = 1;
    int i2 = 2;
    Object localObject1 = k;
    float f1 = j[paramInt].bottom;
    float f2 = h;
    f1 -= f2;
    localObject1[paramInt] = f1;
    localObject1 = new float[i2];
    f1 = k[paramInt];
    f2 = getPaint().getTextSize();
    f1 += f2;
    localObject1[0] = f1;
    f1 = k[paramInt];
    localObject1[i1] = f1;
    localObject1 = ValueAnimator.ofFloat((float[])localObject1);
    ((ValueAnimator)localObject1).setDuration(l1);
    Object localObject2 = new android/view/animation/OvershootInterpolator;
    ((OvershootInterpolator)localObject2).<init>();
    ((ValueAnimator)localObject1).setInterpolator((TimeInterpolator)localObject2);
    localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/h;
    ((h)localObject2).<init>(this, paramInt);
    ((ValueAnimator)localObject1).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject2);
    localObject2 = m;
    int i3 = 255;
    f2 = 3.57E-43F;
    ((Paint)localObject2).setAlpha(i3);
    localObject2 = new int[i2];
    Object tmp178_176 = localObject2;
    tmp178_176[0] = 0;
    tmp178_176[1] = 'ÿ';
    localObject2 = ValueAnimator.ofInt((int[])localObject2);
    ((ValueAnimator)localObject2).setDuration(l1);
    Object localObject3 = new org/npci/upi/security/pinactivitycomponent/widget/i;
    ((i)localObject3).<init>(this);
    ((ValueAnimator)localObject2).addUpdateListener((ValueAnimator.AnimatorUpdateListener)localObject3);
    localObject3 = new android/animation/AnimatorSet;
    ((AnimatorSet)localObject3).<init>();
    int i4 = paramCharSequence.length();
    int i5 = i;
    if (i4 == i5)
    {
      localObject4 = s;
      if (localObject4 != null)
      {
        localObject4 = new org/npci/upi/security/pinactivitycomponent/widget/j;
        ((j)localObject4).<init>(this);
        ((AnimatorSet)localObject3).addListener((Animator.AnimatorListener)localObject4);
      }
    }
    Object localObject4 = new Animator[i2];
    localObject4[0] = localObject1;
    localObject4[i1] = localObject2;
    ((AnimatorSet)localObject3).playTogether((Animator[])localObject4);
    ((AnimatorSet)localObject3).start();
  }
  
  private void a(boolean paramBoolean1, boolean paramBoolean2)
  {
    int i1 = 1;
    boolean bool1 = y;
    Object localObject1;
    int i4;
    if (bool1)
    {
      localObject1 = w;
      localObject2 = new int[i1];
      i1 = 16842914;
      localObject2[0] = i1;
      i4 = a((int[])localObject2);
      ((Paint)localObject1).setColor(i4);
      return;
    }
    Object localObject2 = w;
    bool1 = isFocused();
    if (bool1) {}
    for (float f1 = v;; f1 = u)
    {
      ((Paint)localObject2).setStrokeWidth(f1);
      if (!paramBoolean1) {
        break label128;
      }
      localObject1 = w;
      localObject2 = new int[i1];
      i1 = 16842913;
      localObject2[0] = i1;
      i4 = a((int[])localObject2);
      ((Paint)localObject1).setColor(i4);
      break;
    }
    label128:
    if (paramBoolean2)
    {
      bool1 = isFocused();
      if (bool1)
      {
        localObject1 = new int[i1];
        i4 = 16842918;
        localObject1[0] = i4;
      }
      for (int i2 = a((int[])localObject1);; i2 = a((int[])localObject1))
      {
        localObject2 = w;
        ((Paint)localObject2).setColor(i2);
        break;
        localObject1 = new int[i1];
        i4 = -16842918;
        localObject1[0] = i4;
      }
    }
    boolean bool2 = isFocused();
    if (bool2)
    {
      localObject1 = new int[i1];
      i4 = 16842908;
      localObject1[0] = i4;
    }
    for (int i3 = a((int[])localObject1);; i3 = a((int[])localObject1))
    {
      localObject2 = w;
      ((Paint)localObject2).setColor(i3);
      break;
      localObject1 = new int[i1];
      i4 = -16842908;
      localObject1[0] = i4;
    }
  }
  
  private void b(boolean paramBoolean1, boolean paramBoolean2)
  {
    int i1 = 2;
    int i2 = 1;
    boolean bool = y;
    Drawable localDrawable;
    int[] arrayOfInt;
    int i3;
    if (bool)
    {
      localDrawable = o;
      arrayOfInt = new int[i2];
      i3 = 16842914;
      arrayOfInt[0] = i3;
      localDrawable.setState(arrayOfInt);
    }
    for (;;)
    {
      return;
      bool = isFocused();
      if (bool)
      {
        localDrawable = o;
        arrayOfInt = new int[i2];
        i3 = 16842908;
        arrayOfInt[0] = i3;
        localDrawable.setState(arrayOfInt);
        if (paramBoolean2)
        {
          localDrawable = o;
          arrayOfInt = new int[i1];
          int[] tmp105_103 = arrayOfInt;
          tmp105_103[0] = 16842908;
          tmp105_103[1] = 16842913;
          localDrawable.setState(arrayOfInt);
        }
        else if (paramBoolean1)
        {
          localDrawable = o;
          arrayOfInt = new int[i1];
          int[] tmp142_140 = arrayOfInt;
          tmp142_140[0] = 16842908;
          tmp142_140[1] = 16842912;
          localDrawable.setState(arrayOfInt);
        }
      }
      else
      {
        localDrawable = o;
        arrayOfInt = new int[i2];
        i3 = -16842908;
        arrayOfInt[0] = i3;
        localDrawable.setState(arrayOfInt);
      }
    }
  }
  
  private CharSequence getFullText()
  {
    Object localObject = a;
    if (localObject == null) {}
    for (localObject = getText();; localObject = getMaskChars()) {
      return (CharSequence)localObject;
    }
  }
  
  private StringBuilder getMaskChars()
  {
    Object localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      b = ((StringBuilder)localObject1);
    }
    localObject1 = getText();
    int i1 = ((Editable)localObject1).length();
    for (;;)
    {
      StringBuilder localStringBuilder = b;
      int i2 = localStringBuilder.length();
      if (i2 == i1) {
        break;
      }
      localStringBuilder = b;
      i2 = localStringBuilder.length();
      Object localObject2;
      if (i2 < i1)
      {
        localStringBuilder = b;
        localObject2 = a;
        localStringBuilder.append((String)localObject2);
      }
      else
      {
        localStringBuilder = b;
        localObject2 = b;
        int i3 = ((StringBuilder)localObject2).length() + -1;
        localStringBuilder.deleteCharAt(i3);
      }
    }
    return b;
  }
  
  int a(float paramFloat)
  {
    return (int)(getResourcesgetDisplayMetricsdensityDpi / 160 * paramFloat);
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    CharSequence localCharSequence = getFullText();
    int i1 = localCharSequence.length();
    float[] arrayOfFloat = new float[i1];
    Object localObject1 = getPaint();
    int i2 = 0;
    ((TextPaint)localObject1).getTextWidths(localCharSequence, 0, i1, arrayOfFloat);
    int i3 = 0;
    float f1 = 0.0F;
    localObject1 = null;
    Object localObject2 = c;
    Object localObject3;
    Object localObject4;
    int i5;
    if (localObject2 != null)
    {
      localObject3 = new float[c.length()];
      localObject2 = getPaint();
      localObject4 = c;
      ((TextPaint)localObject2).getTextWidths((String)localObject4, (float[])localObject3);
      i5 = localObject3.length;
      i2 = 0;
      localObject2 = null;
      while (i2 < i5)
      {
        f2 = localObject3[i2] + f1;
        i3 = i2 + 1;
        i2 = i3;
        f1 = f2;
      }
    }
    float f3 = f1;
    i2 = 0;
    localObject2 = null;
    f1 = i2;
    float f2 = g;
    int i4 = f1 < f2;
    if (i4 < 0)
    {
      localObject1 = o;
      label200:
      label213:
      float f4;
      int i9;
      Object localObject5;
      float f5;
      Object localObject6;
      float f6;
      int i7;
      if (localObject1 != null)
      {
        if (i2 < i1)
        {
          i4 = 1;
          f1 = Float.MIN_VALUE;
          boolean bool1 = i4;
          f2 = f1;
          if (i2 != i1) {
            break label631;
          }
          i4 = 1;
          f1 = Float.MIN_VALUE;
          b(bool1, i4);
          localObject1 = o;
          localObject4 = j[i2];
          f2 = left;
          int i6 = (int)f2;
          localObject3 = j[i2];
          f4 = top;
          i9 = (int)f4;
          localObject5 = j[i2];
          f5 = right;
          i5 = (int)f5;
          localObject6 = j[i2];
          f6 = bottom;
          int i10 = (int)f6;
          ((Drawable)localObject1).setBounds(i6, i9, i5, i10);
          localObject1 = o;
          ((Drawable)localObject1).draw(paramCanvas);
        }
      }
      else
      {
        localObject1 = j[i2];
        f1 = left;
        f2 = f;
        i9 = 1073741824;
        f4 = 2.0F;
        f2 /= f4;
        f1 += f2;
        if (i1 <= i2) {
          break label710;
        }
        boolean bool2 = x;
        if (bool2)
        {
          i7 = i1 + -1;
          if (i2 == i7) {
            break label643;
          }
        }
        i7 = i2 + 1;
        f4 = arrayOfFloat[i2];
        i5 = 1073741824;
        f4 /= 2.0F;
        f4 = f1 - f4;
        f5 = k[i2];
        localObject6 = l;
        localObject1 = paramCanvas;
        paramCanvas.drawText(localCharSequence, i2, i7, f4, f5, (Paint)localObject6);
        label473:
        localObject1 = o;
        if (localObject1 == null)
        {
          if (i2 >= i1) {
            break label779;
          }
          i4 = 1;
          f1 = Float.MIN_VALUE;
          i7 = i4;
          f2 = f1;
          label505:
          if (i2 != i1) {
            break label800;
          }
          i4 = 1;
          f1 = Float.MIN_VALUE;
        }
      }
      for (;;)
      {
        a(i7, i4);
        f4 = j[i2].left;
        f5 = j[i2].top;
        f6 = j[i2].right;
        localObject1 = j[i2];
        float f7 = bottom;
        Paint localPaint = w;
        localObject4 = paramCanvas;
        paramCanvas.drawLine(f4, f5, f6, f7, localPaint);
        i2 += 1;
        break;
        i4 = 0;
        f1 = 0.0F;
        localObject1 = null;
        int i8 = 0;
        localObject4 = null;
        f2 = 0.0F;
        break label200;
        label631:
        i4 = 0;
        f1 = 0.0F;
        localObject1 = null;
        break label213;
        label643:
        i8 = i2 + 1;
        f4 = arrayOfFloat[i2];
        i5 = 1073741824;
        f4 /= 2.0F;
        f4 = f1 - f4;
        f5 = k[i2];
        localObject6 = m;
        localObject1 = paramCanvas;
        paramCanvas.drawText(localCharSequence, i2, i8, f4, f5, (Paint)localObject6);
        break label473;
        label710:
        localObject4 = c;
        if (localObject4 == null) {
          break label473;
        }
        localObject4 = c;
        i9 = 1073741824;
        f4 = f3 / 2.0F;
        f1 -= f4;
        localObject3 = k;
        f4 = localObject3[i2];
        localObject5 = n;
        paramCanvas.drawText((String)localObject4, f1, f4, (Paint)localObject5);
        break label473;
        label779:
        i4 = 0;
        f1 = 0.0F;
        localObject1 = null;
        i8 = 0;
        localObject4 = null;
        f2 = 0.0F;
        break label505;
        label800:
        i4 = 0;
        f1 = 0.0F;
        localObject1 = null;
      }
    }
  }
  
  protected void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Object localObject1 = getTextColors();
    z = ((ColorStateList)localObject1);
    localObject1 = z;
    ColorStateList localColorStateList;
    if (localObject1 != null)
    {
      localObject1 = m;
      i1 = z.getDefaultColor();
      ((Paint)localObject1).setColor(i1);
      localObject1 = l;
      localColorStateList = z;
      i1 = localColorStateList.getDefaultColor();
      ((Paint)localObject1).setColor(i1);
      localObject1 = n;
      i1 = getCurrentHintTextColor();
      ((Paint)localObject1).setColor(i1);
    }
    int i4 = getWidth();
    int i1 = af.h(this);
    i4 -= i1;
    i1 = af.g(this);
    i4 -= i1;
    float f1 = e;
    int i5 = 0;
    float f2 = 0.0F;
    boolean bool1 = f1 < 0.0F;
    float f3;
    int i2;
    int i6;
    label296:
    label347:
    label363:
    float f4;
    float f5;
    boolean bool3;
    Object localObject2;
    RectF localRectF;
    float f6;
    float f7;
    float f8;
    Object localObject3;
    if (bool1)
    {
      f3 = i4;
      f1 = g * 2.0F;
      i5 = 1065353216;
      f2 = 1.0F;
      f1 -= f2;
      f3 /= f1;
      f = f3;
      localObject1 = new RectF[(int)g];
      j = ((RectF[])localObject1);
      f3 = g;
      localObject1 = new float[(int)f3];
      k = ((float[])localObject1);
      i4 = getHeight();
      i2 = getPaddingBottom();
      i4 -= i2;
      i2 = getPaddingTop();
      i6 = i4 - i2;
      localObject1 = Locale.getDefault();
      i4 = android.support.v4.d.d.a((Locale)localObject1);
      i2 = 1;
      f1 = Float.MIN_VALUE;
      if (i4 != i2) {
        break label850;
      }
      i4 = 1;
      f3 = Float.MIN_VALUE;
      if (i4 == 0) {
        break label862;
      }
      i4 = -1;
      f3 = 0.0F / 0.0F;
      i2 = getWidth();
      i5 = af.g(this);
      f1 = i2 - i5;
      f2 = f;
      f1 -= f2;
      i2 = (int)f1;
      f2 = 0.0F;
      i5 = i2;
      i2 = 0;
      localColorStateList = null;
      f1 = 0.0F;
      f4 = i2;
      f5 = g;
      bool3 = f4 < f5;
      if (!bool3) {
        return;
      }
      localObject2 = j;
      localRectF = new android/graphics/RectF;
      f6 = i5;
      f7 = i6;
      f8 = i5;
      float f9 = f;
      f8 += f9;
      f9 = i6;
      localRectF.<init>(f6, f7, f8, f9);
      localObject2[i2] = localRectF;
      localObject2 = o;
      if (localObject2 != null)
      {
        bool3 = q;
        if (!bool3) {
          break label878;
        }
        localObject2 = j[i2];
        i7 = getPaddingTop();
        f5 = i7;
        top = f5;
        localObject2 = j[i2];
        f5 = i5;
        localObject3 = j[i2];
        f6 = ((RectF)localObject3).height();
        f5 += f6;
        right = f5;
      }
      label543:
      f4 = e;
      int i7 = 0;
      f5 = 0.0F;
      localRectF = null;
      bool3 = f4 < 0.0F;
      if (!bool3) {
        break label953;
      }
      f2 = i5;
      f4 = i4;
      f5 = f;
      f4 *= f5;
      i7 = 1073741824;
      f5 = 2.0F;
      f4 *= f5;
      f2 += f4;
    }
    for (i5 = (int)f2;; i5 = (int)f2)
    {
      localObject2 = k;
      localRectF = j[i2];
      f5 = bottom;
      f6 = h;
      f5 -= f6;
      localObject2[i2] = f5;
      bool3 = t;
      if (bool3)
      {
        localObject2 = j[i2];
        f5 = j[i2].top / 2.0F;
        top = f5;
        localObject2 = j[i2];
        localRectF = j[i2];
        f5 = bottom;
        i8 = 1073741824;
        f6 = 2.0F;
        f5 /= f6;
        bottom = f5;
      }
      i2 += 1;
      break label363;
      f1 = f;
      i5 = 0;
      f2 = 0.0F;
      boolean bool2 = f1 < 0.0F;
      if (bool2) {
        break;
      }
      f3 = i4;
      f1 = e;
      f2 = g;
      i6 = 1065353216;
      float f10 = 1.0F;
      f2 -= f10;
      f1 *= f2;
      f3 -= f1;
      f1 = g;
      f3 /= f1;
      f = f3;
      break;
      label850:
      i4 = 0;
      f3 = 0.0F;
      localObject1 = null;
      break label296;
      label862:
      i4 = 1;
      f3 = Float.MIN_VALUE;
      int i3 = af.g(this);
      break label347;
      label878:
      localObject2 = j[i3];
      f5 = top;
      localObject3 = p;
      int i8 = ((Rect)localObject3).height();
      f6 = i8;
      f7 = h;
      f8 = 2.0F;
      f7 *= f8;
      f6 += f7;
      f5 -= f6;
      top = f5;
      break label543;
      label953:
      f2 = i5;
      f4 = i4;
      f5 = f;
      f6 = e;
      f5 += f6;
      f4 *= f5;
      f2 += f4;
    }
  }
  
  protected void onTextChanged(CharSequence paramCharSequence, int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool = false;
    setError(false);
    Object localObject = j;
    int i1;
    int i2;
    if (localObject != null)
    {
      bool = x;
      if (bool) {}
    }
    else
    {
      localObject = s;
      if (localObject != null)
      {
        i1 = paramCharSequence.length();
        i2 = i;
        if (i1 == i2)
        {
          localObject = s;
          ((k)localObject).a(paramCharSequence);
        }
      }
    }
    for (;;)
    {
      return;
      i1 = d;
      i2 = -1;
      if (i1 == i2)
      {
        invalidate();
      }
      else if (paramInt3 > paramInt2)
      {
        i1 = d;
        if (i1 == 0) {
          a(paramCharSequence);
        } else {
          a(paramCharSequence, paramInt1);
        }
      }
    }
  }
  
  public void setAnimateText(boolean paramBoolean)
  {
    x = paramBoolean;
  }
  
  public void setCharSize(float paramFloat)
  {
    f = paramFloat;
    invalidate();
  }
  
  public void setColorStates(ColorStateList paramColorStateList)
  {
    C = paramColorStateList;
    invalidate();
  }
  
  public void setCustomSelectionActionModeCallback(ActionMode.Callback paramCallback)
  {
    RuntimeException localRuntimeException = new java/lang/RuntimeException;
    localRuntimeException.<init>("setCustomSelectionActionModeCallback() not supported.");
    throw localRuntimeException;
  }
  
  public void setError(boolean paramBoolean)
  {
    y = paramBoolean;
  }
  
  public void setFontSize(float paramFloat)
  {
    l.setTextSize(paramFloat);
    m.setTextSize(paramFloat);
    n.setTextSize(paramFloat);
  }
  
  public void setLineStroke(float paramFloat)
  {
    u = paramFloat;
    invalidate();
  }
  
  public void setLineStrokeCentered(boolean paramBoolean)
  {
    t = paramBoolean;
    invalidate();
  }
  
  public void setLineStrokeSelected(float paramFloat)
  {
    v = paramFloat;
    invalidate();
  }
  
  public void setMargin(int[] paramArrayOfInt)
  {
    ViewGroup.MarginLayoutParams localMarginLayoutParams = (ViewGroup.MarginLayoutParams)getLayoutParams();
    int i1 = paramArrayOfInt[0];
    int i2 = paramArrayOfInt[1];
    int i3 = paramArrayOfInt[2];
    int i4 = paramArrayOfInt[3];
    localMarginLayoutParams.setMargins(i1, i2, i3, i4);
    setLayoutParams(localMarginLayoutParams);
  }
  
  public void setMaxLength(int paramInt)
  {
    i = paramInt;
    float f1 = paramInt;
    g = f1;
    InputFilter[] arrayOfInputFilter = new InputFilter[1];
    InputFilter.LengthFilter localLengthFilter = new android/text/InputFilter$LengthFilter;
    localLengthFilter.<init>(paramInt);
    arrayOfInputFilter[0] = localLengthFilter;
    setFilters(arrayOfInputFilter);
  }
  
  public void setOnClickListener(View.OnClickListener paramOnClickListener)
  {
    r = paramOnClickListener;
  }
  
  public void setOnPinEnteredListener(k paramk)
  {
    s = paramk;
  }
  
  public void setSpace(float paramFloat)
  {
    e = paramFloat;
    invalidate();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/widget/FormItemEditText.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */