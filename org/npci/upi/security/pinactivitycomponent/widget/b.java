package org.npci.upi.security.pinactivitycomponent.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v4.f.af;
import android.support.v4.f.au;
import android.support.v4.f.ay;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import org.npci.upi.security.pinactivitycomponent.a.d;
import org.npci.upi.security.pinactivitycomponent.a.e;
import org.npci.upi.security.pinactivitycomponent.a.h;

public class b
  extends LinearLayout
  implements c
{
  private boolean a = false;
  private String b;
  private String c;
  private int d;
  private TextView e;
  private FormItemEditText f;
  private o g;
  private int h;
  private Object i;
  private LinearLayout j;
  private Button k;
  private ProgressBar l;
  private ImageView m;
  private String n = "";
  private boolean o = false;
  private boolean p;
  private boolean q;
  private RelativeLayout r;
  
  public b(Context paramContext)
  {
    super(paramContext);
    a(paramContext, null);
  }
  
  public au a(View paramView, boolean paramBoolean)
  {
    float f1 = 0.0F;
    Object localObject = null;
    float f2 = 1.0F;
    au localau1 = af.k(paramView);
    float f3;
    au localau2;
    if (paramBoolean)
    {
      f3 = f2;
      localau2 = localau1.d(f3);
      if (paramBoolean) {
        f1 = f2;
      }
      localau2 = localau2.c(f1);
      localObject = new android/view/animation/AccelerateInterpolator;
      ((AccelerateInterpolator)localObject).<init>();
      localau2 = localau2.a((Interpolator)localObject);
      localObject = new org/npci/upi/security/pinactivitycomponent/widget/n;
      ((n)localObject).<init>(this, paramBoolean);
      localau2 = localau2.a((ay)localObject);
      if (!paramBoolean) {
        break label108;
      }
    }
    for (;;)
    {
      return localau2.a(f2);
      localau2 = null;
      f3 = 0.0F;
      break;
      label108:
      f2 = 0.5F;
    }
  }
  
  public void a()
  {
    a = true;
  }
  
  public void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    Object localObject1 = a.h.FormItemView;
    localObject1 = paramContext.obtainStyledAttributes(paramAttributeSet, (int[])localObject1);
    if (localObject1 != null)
    {
      int i1 = a.h.FormItemView_formTitle;
      localObject2 = ((TypedArray)localObject1).getString(i1);
      b = ((String)localObject2);
      i1 = a.h.FormItemView_formValidationError;
      localObject2 = ((TypedArray)localObject1).getString(i1);
      c = ((String)localObject2);
      i1 = a.h.FormItemView_formInputLength;
      int i2 = 6;
      i1 = ((TypedArray)localObject1).getInteger(i1, i2);
      d = i1;
      i1 = a.h.FormItemView_formActionOnTop;
      boolean bool1 = ((TypedArray)localObject1).getBoolean(i1, false);
      p = bool1;
      ((TypedArray)localObject1).recycle();
    }
    int i3 = a.e.layout_form_item;
    inflate(paramContext, i3, this);
    i3 = a.d.form_item_root;
    localObject1 = (RelativeLayout)findViewById(i3);
    r = ((RelativeLayout)localObject1);
    i3 = a.d.form_item_action_bar;
    localObject1 = (LinearLayout)findViewById(i3);
    j = ((LinearLayout)localObject1);
    i3 = a.d.form_item_title;
    localObject1 = (TextView)findViewById(i3);
    e = ((TextView)localObject1);
    i3 = a.d.form_item_input;
    localObject1 = (FormItemEditText)findViewById(i3);
    f = ((FormItemEditText)localObject1);
    i3 = a.d.form_item_button;
    localObject1 = (Button)findViewById(i3);
    k = ((Button)localObject1);
    i3 = a.d.form_item_progress;
    localObject1 = (ProgressBar)findViewById(i3);
    l = ((ProgressBar)localObject1);
    i3 = a.d.form_item_image;
    localObject1 = (ImageView)findViewById(i3);
    m = ((ImageView)localObject1);
    f.setInputType(0);
    localObject1 = b;
    setTitle((String)localObject1);
    i3 = d;
    setInputLength(i3);
    localObject1 = f;
    Object localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/l;
    ((l)localObject2).<init>(this);
    ((FormItemEditText)localObject1).addTextChangedListener((TextWatcher)localObject2);
    localObject1 = f;
    localObject2 = new org/npci/upi/security/pinactivitycomponent/widget/m;
    ((m)localObject2).<init>(this);
    ((FormItemEditText)localObject1).setOnTouchListener((View.OnTouchListener)localObject2);
    boolean bool2 = p;
    setActionBarPositionTop(bool2);
  }
  
  public void a(Drawable paramDrawable, boolean paramBoolean)
  {
    if (paramDrawable != null)
    {
      localImageView = m;
      localImageView.setImageDrawable(paramDrawable);
    }
    ImageView localImageView = m;
    a(localImageView, paramBoolean);
  }
  
  public void a(String paramString, Drawable paramDrawable, View.OnClickListener paramOnClickListener, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    Button localButton1 = null;
    int i1 = TextUtils.isEmpty(paramString);
    Object localObject;
    if (i1 == 0)
    {
      localObject = k;
      ((Button)localObject).setText(paramString);
    }
    Button localButton2 = k;
    Drawable localDrawable1;
    Drawable localDrawable2;
    if (paramInt == 0)
    {
      localDrawable1 = paramDrawable;
      i1 = 1;
      if (paramInt != i1) {
        break label127;
      }
      localDrawable2 = paramDrawable;
      label53:
      i1 = 2;
      if (paramInt != i1) {
        break label133;
      }
      localObject = paramDrawable;
      label66:
      int i2 = 3;
      if (paramInt != i2) {
        break label142;
      }
    }
    for (;;)
    {
      localButton2.setCompoundDrawablesWithIntrinsicBounds(localDrawable1, localDrawable2, (Drawable)localObject, paramDrawable);
      k.setOnClickListener(paramOnClickListener);
      k.setEnabled(paramBoolean2);
      localButton1 = k;
      a(localButton1, paramBoolean1);
      return;
      localDrawable1 = null;
      break;
      label127:
      localDrawable2 = null;
      break label53;
      label133:
      i1 = 0;
      localObject = null;
      break label66;
      label142:
      paramDrawable = null;
    }
  }
  
  public void a(String paramString, View.OnClickListener paramOnClickListener, boolean paramBoolean1, boolean paramBoolean2)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool)
    {
      localButton = k;
      localButton.setText(paramString);
    }
    Button localButton = k;
    a(localButton, paramBoolean1);
    k.setEnabled(paramBoolean2);
    k.setOnClickListener(paramOnClickListener);
  }
  
  public void a(boolean paramBoolean)
  {
    Object localObject = l;
    localObject = a((View)localObject, paramBoolean);
    AccelerateDecelerateInterpolator localAccelerateDecelerateInterpolator = new android/view/animation/AccelerateDecelerateInterpolator;
    localAccelerateDecelerateInterpolator.<init>();
    ((au)localObject).a(localAccelerateDecelerateInterpolator);
    ((au)localObject).c();
  }
  
  public boolean c()
  {
    boolean bool = o;
    String str1;
    if (!bool)
    {
      bool = true;
      o = bool;
      str1 = n;
      setText(str1);
    }
    for (;;)
    {
      return o;
      bool = false;
      o = false;
      str1 = n;
      String str2 = "●";
      str1 = str1.replaceAll(".", str2);
      FormItemEditText localFormItemEditText = f;
      localFormItemEditText.setText(str1);
    }
  }
  
  public boolean d()
  {
    f.requestFocus();
    return true;
  }
  
  public Object getFormDataTag()
  {
    return i;
  }
  
  public FormItemEditText getFormInputView()
  {
    return f;
  }
  
  public o getFormItemListener()
  {
    return g;
  }
  
  public int getInputLength()
  {
    return d;
  }
  
  public String getInputValue()
  {
    boolean bool = a;
    if (!bool)
    {
      bool = o;
      if (!bool) {
        break label31;
      }
    }
    label31:
    for (String str = f.getText().toString();; str = n) {
      return str;
    }
  }
  
  public void setActionBarPositionTop(boolean paramBoolean)
  {
    int i1 = 10;
    int i2 = 8;
    q = paramBoolean;
    RelativeLayout.LayoutParams localLayoutParams = (RelativeLayout.LayoutParams)j.getLayoutParams();
    boolean bool = q;
    if (bool)
    {
      localLayoutParams.addRule(i1);
      localLayoutParams.addRule(i2, 0);
    }
    for (;;)
    {
      j.setLayoutParams(localLayoutParams);
      return;
      localLayoutParams.addRule(i1, 0);
      int i3 = a.d.form_item_input;
      localLayoutParams.addRule(i2, i3);
    }
  }
  
  public void setFormDataTag(Object paramObject)
  {
    i = paramObject;
  }
  
  public void setFormItemListener(o paramo)
  {
    g = paramo;
  }
  
  public void setFormItemTag(int paramInt)
  {
    h = paramInt;
  }
  
  public void setInputLength(int paramInt)
  {
    f.setMaxLength(paramInt);
    d = paramInt;
  }
  
  public void setText(String paramString)
  {
    f.setText(paramString);
    FormItemEditText localFormItemEditText = f;
    int i1 = paramString.length();
    localFormItemEditText.setSelection(i1);
  }
  
  public void setTitle(String paramString)
  {
    e.setText(paramString);
    b = paramString;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/widget/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */