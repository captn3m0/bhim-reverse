package org.npci.upi.security.pinactivitycomponent.widget;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.graphics.Paint;

class i
  implements ValueAnimator.AnimatorUpdateListener
{
  i(FormItemEditText paramFormItemEditText) {}
  
  public void onAnimationUpdate(ValueAnimator paramValueAnimator)
  {
    Integer localInteger = (Integer)paramValueAnimator.getAnimatedValue();
    Paint localPaint = FormItemEditText.b(a);
    int i = localInteger.intValue();
    localPaint.setAlpha(i);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/widget/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */