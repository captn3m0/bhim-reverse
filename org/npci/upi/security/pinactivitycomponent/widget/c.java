package org.npci.upi.security.pinactivitycomponent.widget;

import android.graphics.drawable.Drawable;
import android.view.View.OnClickListener;

public abstract interface c
{
  public abstract void a(String paramString, Drawable paramDrawable, View.OnClickListener paramOnClickListener, int paramInt, boolean paramBoolean1, boolean paramBoolean2);
  
  public abstract boolean c();
  
  public abstract boolean d();
  
  public abstract Object getFormDataTag();
  
  public abstract String getInputValue();
  
  public abstract void setText(String paramString);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/widget/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */