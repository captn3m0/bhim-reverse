package org.npci.upi.security.pinactivitycomponent.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.v4.f.af;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewPropertyAnimator;
import android.widget.FrameLayout;
import java.util.ArrayList;
import org.npci.upi.security.pinactivitycomponent.a.f;

public class a
  extends FrameLayout
  implements c
{
  private ArrayList a;
  private int b;
  private int c;
  private Object d;
  
  public a(Context paramContext)
  {
    super(paramContext);
  }
  
  private void a(View paramView)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 12;
    if (i >= j)
    {
      ViewPropertyAnimator localViewPropertyAnimator = paramView.animate();
      localViewPropertyAnimator.x(0.0F);
    }
    for (;;)
    {
      return;
      af.c(paramView, 0.0F);
    }
  }
  
  private void a(View paramView, boolean paramBoolean)
  {
    int i;
    float f;
    if (paramBoolean)
    {
      i = c * -1;
      int j = Build.VERSION.SDK_INT;
      int k = 12;
      if (j < k) {
        break label54;
      }
      ViewPropertyAnimator localViewPropertyAnimator = paramView.animate();
      f = i;
      localViewPropertyAnimator.x(f);
    }
    for (;;)
    {
      return;
      i = c;
      break;
      label54:
      f = i;
      af.c(paramView, f);
    }
  }
  
  private void e()
  {
    int i = 0;
    Object localObject = null;
    for (int j = 0;; j = i)
    {
      localObject = a;
      i = ((ArrayList)localObject).size();
      if (j >= i) {
        break;
      }
      localObject = (b)a.get(j);
      String str = "";
      ((b)localObject).setText(str);
      i = j + 1;
    }
    b();
  }
  
  public void a(String paramString, Drawable paramDrawable, View.OnClickListener paramOnClickListener, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    int i = 0;
    Object localObject = null;
    for (int j = 0;; j = i)
    {
      localObject = a;
      i = ((ArrayList)localObject).size();
      if (j >= i) {
        break;
      }
      localObject = (b)a.get(j);
      ((b)localObject).a(paramString, paramDrawable, paramOnClickListener, paramInt, paramBoolean1, paramBoolean2);
      i = j + 1;
    }
  }
  
  public void a(ArrayList paramArrayList, o paramo)
  {
    a = paramArrayList;
    Object localObject = (View)a.get(0);
    addView((View)localObject);
    ((b)a.get(0)).setFormItemListener(paramo);
    b = 0;
    localObject = getResources().getDisplayMetrics();
    int i = widthPixels;
    c = i;
    i = 1;
    for (int j = i;; j = i)
    {
      localObject = a;
      i = ((ArrayList)localObject).size();
      if (j >= i) {
        break;
      }
      localObject = (b)a.get(j);
      ((b)localObject).setFormItemListener(paramo);
      int k = c;
      float f = k;
      af.c((View)localObject, f);
      addView((View)localObject);
      i = j + 1;
    }
  }
  
  public boolean a()
  {
    int i = 1;
    int j = b;
    ArrayList localArrayList = a;
    int m = localArrayList.size() + -1;
    Object localObject;
    if (j >= m)
    {
      j = 0;
      localObject = null;
    }
    for (;;)
    {
      return j;
      localObject = a;
      int n = b;
      localObject = (b)((ArrayList)localObject).get(n);
      a((View)localObject, i);
      localObject = a;
      n = b + 1;
      localObject = (b)((ArrayList)localObject).get(n);
      a((View)localObject);
      int k = b + 1;
      b = k;
      localObject = a;
      n = b;
      localObject = (b)((ArrayList)localObject).get(n);
      ((b)localObject).requestFocus();
      k = i;
    }
  }
  
  public boolean b()
  {
    int i = 0;
    int j = b;
    Object localObject;
    if (j == 0)
    {
      j = 0;
      localObject = null;
    }
    for (;;)
    {
      return j;
      localObject = a;
      int m = b;
      localObject = (b)((ArrayList)localObject).get(m);
      a((View)localObject, false);
      localObject = a;
      i = b + -1;
      localObject = (b)((ArrayList)localObject).get(i);
      a((View)localObject);
      int k = b + -1;
      b = k;
      localObject = a;
      i = b;
      localObject = (b)((ArrayList)localObject).get(i);
      ((b)localObject).requestFocus();
      k = 1;
    }
  }
  
  public boolean c()
  {
    ArrayList localArrayList = a;
    int i = b;
    return ((b)localArrayList.get(i)).c();
  }
  
  public boolean d()
  {
    int i = 1;
    boolean bool1 = false;
    Object localObject1 = a;
    int j = b;
    String str = ((b)((ArrayList)localObject1).get(j)).getInputValue();
    localObject1 = a;
    j = b;
    localObject1 = (b)((ArrayList)localObject1).get(j);
    int k = ((b)localObject1).getInputLength();
    j = str.length();
    if (k == j)
    {
      k = b;
      Object localObject2 = a;
      j = ((ArrayList)localObject2).size() + -1;
      if (k == j)
      {
        localObject1 = a;
        j = b;
        localObject1 = (b)((ArrayList)localObject1).get(j);
        ((b)localObject1).requestFocus();
        j = 0;
        localObject2 = null;
        localObject1 = a;
        k = ((ArrayList)localObject1).size();
        if (j < k)
        {
          localObject1 = ((b)a.get(j)).getInputValue();
          boolean bool2 = ((String)localObject1).equals(str);
          if (!bool2)
          {
            e();
            localObject1 = ((b)a.get(j)).getFormItemListener();
            localObject2 = getContext();
            i = a.f.info_pins_dont_match;
            localObject2 = ((Context)localObject2).getString(i);
            ((o)localObject1).a(this, (String)localObject2);
          }
        }
      }
    }
    for (;;)
    {
      return bool1;
      int m = j + 1;
      j = m;
      break;
      bool1 = i;
      continue;
      boolean bool3 = a();
      if (!bool3)
      {
        bool1 = i;
        continue;
        localObject1 = a;
        j = b;
        localObject1 = (b)((ArrayList)localObject1).get(j);
        ((b)localObject1).requestFocus();
      }
    }
  }
  
  public Object getFormDataTag()
  {
    Object localObject = d;
    if (localObject == null) {}
    for (localObject = ((b)a.get(0)).getFormDataTag();; localObject = d) {
      return localObject;
    }
  }
  
  public String getInputValue()
  {
    return ((b)a.get(0)).getInputValue();
  }
  
  public void setFormDataTag(Object paramObject)
  {
    d = paramObject;
  }
  
  public void setText(String paramString)
  {
    int i = 0;
    Object localObject = null;
    for (int j = 0;; j = i)
    {
      localObject = a;
      i = ((ArrayList)localObject).size();
      if (j >= i) {
        break;
      }
      localObject = (b)a.get(j);
      ((b)localObject).setText(paramString);
      i = j + 1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/widget/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */