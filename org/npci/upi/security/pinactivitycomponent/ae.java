package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.res.AssetManager;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class ae
{
  private static ByteArrayOutputStream a(ByteArrayOutputStream paramByteArrayOutputStream, InputStream paramInputStream)
  {
    int i = 4096;
    byte[] arrayOfByte = new byte[i];
    for (;;)
    {
      int j = paramInputStream.read(arrayOfByte);
      int k = -1;
      if (j == k) {
        break;
      }
      k = 0;
      paramByteArrayOutputStream.write(arrayOfByte, 0, j);
    }
    paramByteArrayOutputStream.close();
    paramInputStream.close();
    return paramByteArrayOutputStream;
  }
  
  public static byte[] a(String paramString, Context paramContext)
  {
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      localByteArrayOutputStream.<init>();
      localObject1 = paramContext.getAssets();
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      String str = "npci/";
      localObject2 = ((StringBuilder)localObject2).append(str);
      localObject2 = ((StringBuilder)localObject2).append(paramString);
      localObject2 = ((StringBuilder)localObject2).toString();
      localObject1 = ((AssetManager)localObject1).open((String)localObject2);
      localByteArrayOutputStream = a(localByteArrayOutputStream, (InputStream)localObject1);
      return localByteArrayOutputStream.toByteArray();
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localObject1 = new java/lang/RuntimeException;
      ((RuntimeException)localObject1).<init>(localFileNotFoundException);
      throw ((Throwable)localObject1);
    }
    catch (IOException localIOException)
    {
      localObject1 = new java/lang/RuntimeException;
      ((RuntimeException)localObject1).<init>(localIOException);
      throw ((Throwable)localObject1);
    }
    catch (Exception localException)
    {
      Object localObject1 = new java/lang/RuntimeException;
      ((RuntimeException)localObject1).<init>(localException);
      throw ((Throwable)localObject1);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/ae.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */