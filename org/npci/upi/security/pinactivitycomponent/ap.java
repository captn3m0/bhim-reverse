package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class ap
{
  private final SharedPreferences a;
  
  public ap(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("NPCIPreferences", 0);
    a = localSharedPreferences;
  }
  
  public void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putString(paramString1, paramString2);
    localEditor.commit();
  }
  
  public String b(String paramString1, String paramString2)
  {
    return a.getString(paramString1, paramString2);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/ap.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */