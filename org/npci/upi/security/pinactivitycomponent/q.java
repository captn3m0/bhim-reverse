package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class q
{
  private static final String b = q.class.getName();
  JSONArray a = null;
  private Context c;
  private ap d;
  private List e;
  
  public q(Context paramContext)
  {
    c = paramContext;
    Object localObject1 = new org/npci/upi/security/pinactivitycomponent/ap;
    Object localObject2 = c;
    ((ap)localObject1).<init>((Context)localObject2);
    d = ((ap)localObject1);
    localObject1 = new org/json/JSONArray;
    ((JSONArray)localObject1).<init>();
    a = ((JSONArray)localObject1);
    localObject1 = ae.a("npci_otp_rules.json", paramContext);
    if (localObject1 != null) {}
    try
    {
      localObject2 = new org/json/JSONArray;
      String str = new java/lang/String;
      str.<init>((byte[])localObject1);
      ((JSONArray)localObject2).<init>(str);
      a = ((JSONArray)localObject2);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localObject2 = b;
        g.a((String)localObject2, localException);
      }
    }
  }
  
  /* Error */
  public static String a(String paramString)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 65	java/lang/String:toLowerCase	()Ljava/lang/String;
    //   4: astore_1
    //   5: ldc 67
    //   7: astore_2
    //   8: aload_2
    //   9: invokestatic 73	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   12: astore_2
    //   13: aload_1
    //   14: invokevirtual 77	java/lang/String:getBytes	()[B
    //   17: astore_3
    //   18: aload_1
    //   19: invokevirtual 81	java/lang/String:length	()I
    //   22: istore 4
    //   24: aload_2
    //   25: aload_3
    //   26: iconst_0
    //   27: iload 4
    //   29: invokevirtual 85	java/security/MessageDigest:update	([BII)V
    //   32: new 87	java/math/BigInteger
    //   35: astore_1
    //   36: iconst_1
    //   37: istore 5
    //   39: aload_2
    //   40: invokevirtual 91	java/security/MessageDigest:digest	()[B
    //   43: astore_2
    //   44: aload_1
    //   45: iload 5
    //   47: aload_2
    //   48: invokespecial 94	java/math/BigInteger:<init>	(I[B)V
    //   51: bipush 16
    //   53: istore 6
    //   55: aload_1
    //   56: iload 6
    //   58: invokevirtual 99	java/math/BigInteger:toString	(I)Ljava/lang/String;
    //   61: astore_1
    //   62: aload_1
    //   63: invokevirtual 81	java/lang/String:length	()I
    //   66: istore 6
    //   68: bipush 32
    //   70: istore 5
    //   72: iload 6
    //   74: iload 5
    //   76: if_icmpge +49 -> 125
    //   79: new 102	java/lang/StringBuilder
    //   82: astore_2
    //   83: aload_2
    //   84: invokespecial 103	java/lang/StringBuilder:<init>	()V
    //   87: ldc 105
    //   89: astore_3
    //   90: aload_2
    //   91: aload_3
    //   92: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   95: astore_2
    //   96: aload_2
    //   97: aload_1
    //   98: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   101: astore_1
    //   102: aload_1
    //   103: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   106: astore_1
    //   107: goto -45 -> 62
    //   110: astore_1
    //   111: getstatic 22	org/npci/upi/security/pinactivitycomponent/q:b	Ljava/lang/String;
    //   114: astore_2
    //   115: aload_2
    //   116: aload_1
    //   117: invokestatic 60	org/npci/upi/security/pinactivitycomponent/g:a	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   120: iconst_0
    //   121: istore 4
    //   123: aconst_null
    //   124: astore_1
    //   125: aload_1
    //   126: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	127	0	paramString	String
    //   4	103	1	localObject1	Object
    //   110	7	1	localException	Exception
    //   124	2	1	str	String
    //   7	109	2	localObject2	Object
    //   17	75	3	localObject3	Object
    //   22	100	4	i	int
    //   37	40	5	j	int
    //   53	24	6	k	int
    // Exception table:
    //   from	to	target	type
    //   8	12	110	java/lang/Exception
    //   13	17	110	java/lang/Exception
    //   18	22	110	java/lang/Exception
    //   27	32	110	java/lang/Exception
    //   32	35	110	java/lang/Exception
    //   39	43	110	java/lang/Exception
    //   47	51	110	java/lang/Exception
    //   56	61	110	java/lang/Exception
    //   62	66	110	java/lang/Exception
    //   79	82	110	java/lang/Exception
    //   83	87	110	java/lang/Exception
    //   91	95	110	java/lang/Exception
    //   97	101	110	java/lang/Exception
    //   102	106	110	java/lang/Exception
  }
  
  private String a(ArrayList paramArrayList)
  {
    int i = paramArrayList.size();
    if (i <= 0)
    {
      i = 0;
      localObject1 = null;
      return (String)localObject1;
    }
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = ((StringBuilder)localObject1).append("");
    localObject1 = (String)paramArrayList.get(0);
    localObject2 = (String)localObject1;
    i = 1;
    localObject1 = localObject2;
    int j = i;
    for (;;)
    {
      int k = paramArrayList.size();
      if (j >= k) {
        break;
      }
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append((String)localObject1).append(",");
      localObject1 = (String)paramArrayList.get(j);
      localObject3 = (String)localObject1;
      i = j + 1;
      j = i;
      localObject1 = localObject3;
    }
  }
  
  private p a(int paramInt, String paramString1, String paramString2, JSONObject paramJSONObject)
  {
    String str = null;
    Object localObject1 = "sender";
    for (;;)
    {
      try
      {
        localObject1 = paramJSONObject.getJSONArray((String)localObject1);
        bool1 = a(paramString2, (JSONArray)localObject1);
        if (bool1) {
          continue;
        }
        bool1 = false;
        localObject1 = null;
      }
      catch (JSONException localJSONException)
      {
        Object localObject3;
        boolean bool2;
        int i;
        boolean bool1 = false;
        Object localObject2 = null;
        continue;
        localObject2 = "otp";
        localObject2 = paramJSONObject.get((String)localObject2);
        localObject2 = (String)localObject2;
        continue;
        bool1 = false;
        localObject2 = null;
        continue;
      }
      return (p)localObject1;
      localObject1 = "message";
      localObject1 = paramJSONObject.getString((String)localObject1);
      bool1 = a(paramString1, (String)localObject1);
      if (!bool1)
      {
        bool1 = false;
        localObject1 = null;
      }
      else
      {
        if (paramInt == 0) {
          continue;
        }
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        localObject3 = "\\d{";
        localObject1 = ((StringBuilder)localObject1).append((String)localObject3);
        localObject1 = ((StringBuilder)localObject1).append(paramInt);
        localObject3 = "}";
        localObject1 = ((StringBuilder)localObject1).append((String)localObject3);
        localObject1 = ((StringBuilder)localObject1).toString();
        localObject1 = Pattern.compile((String)localObject1);
        localObject3 = ((Pattern)localObject1).matcher(paramString1);
        localObject1 = new org/npci/upi/security/pinactivitycomponent/p;
        ((p)localObject1).<init>();
        ((p)localObject1).a(paramString1);
        bool2 = ((Matcher)localObject3).find();
        if (!bool2) {
          continue;
        }
        i = ((Matcher)localObject3).groupCount();
        if (0 > i) {
          continue;
        }
        str = ((Matcher)localObject3).group(0);
        ((p)localObject1).b(str);
      }
    }
  }
  
  private boolean a(String paramString1, String paramString2)
  {
    Matcher localMatcher = Pattern.compile(paramString2, 2).matcher(paramString1);
    boolean bool = localMatcher.find();
    if (bool) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localMatcher = null;
    }
  }
  
  private boolean a(String paramString, JSONArray paramJSONArray)
  {
    boolean bool1 = false;
    int i = 0;
    for (;;)
    {
      int j = paramJSONArray.length();
      if (i < j)
      {
        Object localObject = paramJSONArray.getString(i);
        int k = 2;
        localObject = Pattern.compile((String)localObject, k).matcher(paramString);
        boolean bool2 = ((Matcher)localObject).find();
        if (bool2) {
          bool1 = true;
        }
      }
      else
      {
        return bool1;
      }
      i += 1;
    }
  }
  
  private boolean b(String paramString)
  {
    Object localObject = e;
    if (localObject == null)
    {
      localObject = d;
      String str1 = "";
      localObject = ((ap)localObject).b("msgID", str1);
      String str2 = ",";
      localObject = Arrays.asList(((String)localObject).split(str2));
      e = ((List)localObject);
    }
    return e.contains(paramString);
  }
  
  private boolean b(p paramp)
  {
    String str = paramp.c();
    boolean bool = b(str);
    if (!bool)
    {
      str = c(paramp);
      bool = b(str);
      if (!bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  private String c(p paramp)
  {
    return a(paramp.a());
  }
  
  private void c(String paramString)
  {
    if (paramString == null) {}
    for (;;)
    {
      return;
      Object localObject1 = d.b("msgID", "");
      Object localObject2 = new java/util/ArrayList;
      String str = ",";
      localObject1 = Arrays.asList(((String)localObject1).split(str));
      ((ArrayList)localObject2).<init>((Collection)localObject1);
      boolean bool = ((ArrayList)localObject2).contains(paramString);
      if (!bool)
      {
        int i = ((ArrayList)localObject2).size();
        int j = 10;
        if (i >= j)
        {
          i = 0;
          localObject1 = null;
          ((ArrayList)localObject2).remove(0);
        }
        ((ArrayList)localObject2).add(paramString);
        localObject1 = a((ArrayList)localObject2);
        localObject2 = d;
        str = "msgID";
        ((ap)localObject2).a(str, (String)localObject1);
      }
    }
  }
  
  /* Error */
  public p a(int paramInt, long paramLong)
  {
    // Byte code:
    //   0: iconst_2
    //   1: istore 4
    //   3: iconst_1
    //   4: istore 5
    //   6: iconst_0
    //   7: istore 6
    //   9: aconst_null
    //   10: astore 7
    //   12: ldc -8
    //   14: invokestatic 254	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   17: astore 8
    //   19: iconst_4
    //   20: anewarray 49	java/lang/String
    //   23: astore 9
    //   25: aload 9
    //   27: iconst_0
    //   28: ldc_w 257
    //   31: aastore
    //   32: aload 9
    //   34: iload 5
    //   36: ldc_w 259
    //   39: aastore
    //   40: aload 9
    //   42: iload 4
    //   44: ldc_w 261
    //   47: aastore
    //   48: iconst_3
    //   49: istore 10
    //   51: aload 9
    //   53: iload 10
    //   55: ldc_w 264
    //   58: aastore
    //   59: ldc_w 266
    //   62: astore 11
    //   64: iconst_0
    //   65: anewarray 4	java/lang/Object
    //   68: astore 12
    //   70: aload 11
    //   72: aload 12
    //   74: invokestatic 270	java/lang/String:format	(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   77: astore 12
    //   79: ldc_w 272
    //   82: astore 13
    //   84: aload_0
    //   85: getfield 30	org/npci/upi/security/pinactivitycomponent/q:c	Landroid/content/Context;
    //   88: astore 11
    //   90: aload 11
    //   92: invokevirtual 278	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   95: astore 11
    //   97: iconst_1
    //   98: istore 6
    //   100: iload 6
    //   102: anewarray 49	java/lang/String
    //   105: astore 7
    //   107: new 102	java/lang/StringBuilder
    //   110: astore 14
    //   112: aload 14
    //   114: invokespecial 103	java/lang/StringBuilder:<init>	()V
    //   117: ldc 118
    //   119: astore 15
    //   121: aload 14
    //   123: aload 15
    //   125: invokevirtual 109	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   128: astore 14
    //   130: aload 14
    //   132: lload_2
    //   133: invokevirtual 281	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   136: astore 14
    //   138: aload 14
    //   140: invokevirtual 111	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   143: astore 14
    //   145: aload 7
    //   147: iconst_0
    //   148: aload 14
    //   150: aastore
    //   151: aload 11
    //   153: aload 8
    //   155: aload 9
    //   157: aload 12
    //   159: aload 7
    //   161: aload 13
    //   163: invokevirtual 287	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   166: astore 8
    //   168: aload 8
    //   170: invokeinterface 292 1 0
    //   175: istore 10
    //   177: iload 10
    //   179: ifeq +129 -> 308
    //   182: iconst_1
    //   183: istore 10
    //   185: aload 8
    //   187: iload 10
    //   189: invokeinterface 293 2 0
    //   194: astore 11
    //   196: iconst_2
    //   197: istore 16
    //   199: aload 8
    //   201: iload 16
    //   203: invokeinterface 293 2 0
    //   208: astore 9
    //   210: aload_0
    //   211: iload_1
    //   212: aload 11
    //   214: aload 9
    //   216: invokevirtual 296	org/npci/upi/security/pinactivitycomponent/q:a	(ILjava/lang/String;Ljava/lang/String;)Lorg/npci/upi/security/pinactivitycomponent/p;
    //   219: astore 11
    //   221: aload 11
    //   223: ifnull -55 -> 168
    //   226: ldc_w 257
    //   229: astore 9
    //   231: aload 8
    //   233: aload 9
    //   235: invokeinterface 300 2 0
    //   240: istore 16
    //   242: aload 8
    //   244: iload 16
    //   246: invokeinterface 304 2 0
    //   251: lstore 17
    //   253: lload 17
    //   255: invokestatic 310	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   258: astore 9
    //   260: aload 9
    //   262: invokestatic 313	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   265: astore 9
    //   267: aload 11
    //   269: aload 9
    //   271: invokevirtual 315	org/npci/upi/security/pinactivitycomponent/p:c	(Ljava/lang/String;)V
    //   274: aload_0
    //   275: aload 11
    //   277: invokespecial 318	org/npci/upi/security/pinactivitycomponent/q:b	(Lorg/npci/upi/security/pinactivitycomponent/p;)Z
    //   280: istore 16
    //   282: iload 16
    //   284: ifeq -116 -> 168
    //   287: aload_0
    //   288: aload 11
    //   290: invokevirtual 321	org/npci/upi/security/pinactivitycomponent/q:a	(Lorg/npci/upi/security/pinactivitycomponent/p;)V
    //   293: aload 8
    //   295: ifnull +10 -> 305
    //   298: aload 8
    //   300: invokeinterface 324 1 0
    //   305: aload 11
    //   307: areturn
    //   308: aload 8
    //   310: ifnull +10 -> 320
    //   313: aload 8
    //   315: invokeinterface 324 1 0
    //   320: iconst_0
    //   321: istore 10
    //   323: aconst_null
    //   324: astore 11
    //   326: goto -21 -> 305
    //   329: astore 11
    //   331: iconst_0
    //   332: istore 10
    //   334: aconst_null
    //   335: astore 11
    //   337: aload 11
    //   339: ifnull -19 -> 320
    //   342: aload 11
    //   344: invokeinterface 324 1 0
    //   349: goto -29 -> 320
    //   352: astore 11
    //   354: aconst_null
    //   355: astore 8
    //   357: getstatic 22	org/npci/upi/security/pinactivitycomponent/q:b	Ljava/lang/String;
    //   360: astore 9
    //   362: aload 9
    //   364: aload 11
    //   366: invokestatic 60	org/npci/upi/security/pinactivitycomponent/g:a	(Ljava/lang/String;Ljava/lang/Exception;)V
    //   369: aload 8
    //   371: ifnull -51 -> 320
    //   374: aload 8
    //   376: invokeinterface 324 1 0
    //   381: goto -61 -> 320
    //   384: astore 11
    //   386: aconst_null
    //   387: astore 8
    //   389: aload 8
    //   391: ifnull +10 -> 401
    //   394: aload 8
    //   396: invokeinterface 324 1 0
    //   401: aload 11
    //   403: athrow
    //   404: astore 11
    //   406: goto -17 -> 389
    //   409: astore 11
    //   411: goto -54 -> 357
    //   414: astore 11
    //   416: aload 8
    //   418: astore 11
    //   420: goto -83 -> 337
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	423	0	this	q
    //   0	423	1	paramInt	int
    //   0	423	2	paramLong	long
    //   1	42	4	i	int
    //   4	31	5	j	int
    //   7	94	6	k	int
    //   10	150	7	arrayOfString	String[]
    //   17	400	8	localObject1	Object
    //   23	340	9	localObject2	Object
    //   49	5	10	m	int
    //   175	13	10	n	int
    //   321	12	10	i1	int
    //   62	263	11	localObject3	Object
    //   329	1	11	localSecurityException1	SecurityException
    //   335	8	11	localObject4	Object
    //   352	13	11	localException1	Exception
    //   384	18	11	localObject5	Object
    //   404	1	11	localObject6	Object
    //   409	1	11	localException2	Exception
    //   414	1	11	localSecurityException2	SecurityException
    //   418	1	11	localObject7	Object
    //   68	90	12	localObject8	Object
    //   82	80	13	str1	String
    //   110	39	14	localObject9	Object
    //   119	5	15	str2	String
    //   197	48	16	i2	int
    //   280	3	16	bool	boolean
    //   251	3	17	l	long
    // Exception table:
    //   from	to	target	type
    //   84	88	329	java/lang/SecurityException
    //   90	95	329	java/lang/SecurityException
    //   100	105	329	java/lang/SecurityException
    //   107	110	329	java/lang/SecurityException
    //   112	117	329	java/lang/SecurityException
    //   123	128	329	java/lang/SecurityException
    //   132	136	329	java/lang/SecurityException
    //   138	143	329	java/lang/SecurityException
    //   148	151	329	java/lang/SecurityException
    //   161	166	329	java/lang/SecurityException
    //   84	88	352	java/lang/Exception
    //   90	95	352	java/lang/Exception
    //   100	105	352	java/lang/Exception
    //   107	110	352	java/lang/Exception
    //   112	117	352	java/lang/Exception
    //   123	128	352	java/lang/Exception
    //   132	136	352	java/lang/Exception
    //   138	143	352	java/lang/Exception
    //   148	151	352	java/lang/Exception
    //   161	166	352	java/lang/Exception
    //   84	88	384	finally
    //   90	95	384	finally
    //   100	105	384	finally
    //   107	110	384	finally
    //   112	117	384	finally
    //   123	128	384	finally
    //   132	136	384	finally
    //   138	143	384	finally
    //   148	151	384	finally
    //   161	166	384	finally
    //   168	175	404	finally
    //   187	194	404	finally
    //   201	208	404	finally
    //   214	219	404	finally
    //   233	240	404	finally
    //   244	251	404	finally
    //   253	258	404	finally
    //   260	265	404	finally
    //   269	274	404	finally
    //   275	280	404	finally
    //   288	293	404	finally
    //   357	360	404	finally
    //   364	369	404	finally
    //   168	175	409	java/lang/Exception
    //   187	194	409	java/lang/Exception
    //   201	208	409	java/lang/Exception
    //   214	219	409	java/lang/Exception
    //   233	240	409	java/lang/Exception
    //   244	251	409	java/lang/Exception
    //   253	258	409	java/lang/Exception
    //   260	265	409	java/lang/Exception
    //   269	274	409	java/lang/Exception
    //   275	280	409	java/lang/Exception
    //   288	293	409	java/lang/Exception
    //   168	175	414	java/lang/SecurityException
    //   187	194	414	java/lang/SecurityException
    //   201	208	414	java/lang/SecurityException
    //   214	219	414	java/lang/SecurityException
    //   233	240	414	java/lang/SecurityException
    //   244	251	414	java/lang/SecurityException
    //   253	258	414	java/lang/SecurityException
    //   260	265	414	java/lang/SecurityException
    //   269	274	414	java/lang/SecurityException
    //   275	280	414	java/lang/SecurityException
    //   288	293	414	java/lang/SecurityException
  }
  
  public p a(int paramInt, String paramString1, String paramString2)
  {
    i = 0;
    Object localObject1 = null;
    int j = 0;
    str = null;
    for (;;)
    {
      try
      {
        localObject1 = a;
        i = ((JSONArray)localObject1).length();
        if (j >= i) {
          continue;
        }
        localObject1 = a;
        localObject1 = ((JSONArray)localObject1).getJSONObject(j);
        localObject1 = a(paramInt, paramString2, paramString1, (JSONObject)localObject1);
        if (localObject1 == null) {
          continue;
        }
      }
      catch (Exception localException)
      {
        str = b;
        g.a(str, localException);
        i = 0;
        Object localObject2 = null;
        continue;
        i = 0;
        localObject2 = null;
        continue;
      }
      return (p)localObject1;
      i = j + 1;
      j = i;
    }
  }
  
  public void a(p paramp)
  {
    String str = paramp.c();
    if (str != null) {}
    for (str = paramp.c();; str = c(paramp))
    {
      c(str);
      return;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/q.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */