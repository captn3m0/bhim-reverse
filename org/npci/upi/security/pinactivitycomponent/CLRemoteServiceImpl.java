package org.npci.upi.security.pinactivitycomponent;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import org.npci.upi.security.services.b;
import org.npci.upi.security.services.d;

public class CLRemoteServiceImpl
  extends Service
{
  private b a = null;
  private z b = null;
  
  private Bundle a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, d paramd)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putString("keyCode", paramString1);
    localBundle.putString("keyXmlPayload", paramString2);
    localBundle.putString("controls", paramString3);
    localBundle.putString("configuration", paramString4);
    localBundle.putString("salt", paramString5);
    localBundle.putString("payInfo", paramString6);
    localBundle.putString("trust", paramString7);
    localBundle.putString("languagePref", paramString8);
    CLServerResultReceiver localCLServerResultReceiver = new org/npci/upi/security/pinactivitycomponent/CLServerResultReceiver;
    localCLServerResultReceiver.<init>(paramd);
    an.a(localCLServerResultReceiver);
    return localBundle;
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    Object localObject = a;
    Context localContext;
    if (localObject == null)
    {
      localObject = new org/npci/upi/security/pinactivitycomponent/y;
      localContext = getBaseContext();
      ((y)localObject).<init>(this, localContext, null);
      a = ((b)localObject);
    }
    try
    {
      localObject = new org/npci/upi/security/pinactivitycomponent/z;
      localContext = getBaseContext();
      ((z)localObject).<init>(localContext);
      b = ((z)localObject);
      return a;
    }
    catch (Exception localException)
    {
      RuntimeException localRuntimeException = new java/lang/RuntimeException;
      localRuntimeException.<init>("Could not initialize service provider");
      throw localRuntimeException;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/CLRemoteServiceImpl.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */