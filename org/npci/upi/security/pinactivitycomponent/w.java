package org.npci.upi.security.pinactivitycomponent;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.ResultReceiver;
import in.org.npci.commonlibrary.e;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

public class w
{
  private Map a;
  private Context b;
  private Properties c;
  private Properties d;
  private Properties e;
  private t f;
  private ac g;
  private Locale h;
  private e i;
  private Activity j;
  private an k;
  private ab l;
  
  public w(Context paramContext, an paraman, Activity paramActivity)
  {
    Object localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    a = ((Map)localObject1);
    k = paraman;
    localObject1 = paraman.b();
    h = ((Locale)localObject1);
    b = paramContext;
    localObject1 = paraman.c();
    i = ((e)localObject1);
    j = paramActivity;
    localObject1 = a("cl-app.properties");
    e = ((Properties)localObject1);
    localObject1 = a("validation.properties");
    c = ((Properties)localObject1);
    localObject1 = a("version.properties");
    d = ((Properties)localObject1);
    localObject1 = h;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    if (localObject1 != null)
    {
      localObject1 = a;
      localObject2 = h.getLanguage();
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append("cl-messages_");
      localObject4 = h.getLanguage();
      localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
      localObject4 = ".properties";
      localObject3 = (String)localObject4;
      localObject3 = a((String)localObject3);
      ((Map)localObject1).put(localObject2, localObject3);
    }
    for (;;)
    {
      localObject1 = paraman.d();
      l = ((ab)localObject1);
      localObject1 = new org/npci/upi/security/pinactivitycomponent/t;
      ((t)localObject1).<init>(this);
      f = ((t)localObject1);
      if (paraman != null)
      {
        localObject1 = paraman.c();
        if (localObject1 != null)
        {
          localObject1 = paraman.a();
          if (localObject1 != null)
          {
            localObject1 = new org/npci/upi/security/pinactivitycomponent/ac;
            localObject2 = i;
            localObject3 = l;
            localObject4 = paraman.a();
            ((ac)localObject1).<init>((e)localObject2, (ab)localObject3, (String)localObject4);
            g = ((ac)localObject1);
          }
        }
      }
      return;
      localObject1 = new java/util/Locale;
      ((Locale)localObject1).<init>("en_US");
      localObject2 = a;
      localObject3 = ((Locale)localObject1).getLanguage();
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      String str = "cl-messages_";
      localObject4 = ((StringBuilder)localObject4).append(str);
      localObject1 = ((Locale)localObject1).getLanguage();
      localObject1 = ((StringBuilder)localObject4).append((String)localObject1);
      localObject4 = ".properties";
      localObject1 = (String)localObject4;
      localObject1 = a((String)localObject1);
      ((Map)localObject2).put(localObject3, localObject1);
    }
  }
  
  public Properties a(String paramString)
  {
    Properties localProperties = new java/util/Properties;
    localProperties.<init>();
    try
    {
      Object localObject = b;
      localObject = ((Context)localObject).getAssets();
      localObject = ((AssetManager)localObject).open(paramString);
      localProperties.load((InputStream)localObject);
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        String str2 = "AssetsPropertyReader";
        String str1 = localIOException.toString();
        g.a(str2, str1);
      }
    }
    return localProperties;
  }
  
  public t a()
  {
    return f;
  }
  
  public String b(String paramString)
  {
    Object localObject = d;
    if (localObject != null) {}
    for (localObject = d.getProperty(paramString);; localObject = null) {
      return (String)localObject;
    }
  }
  
  public ac b()
  {
    String str1 = null;
    Object localObject1 = g;
    if (localObject1 == null)
    {
      localObject1 = k;
      if (localObject1 != null)
      {
        localObject1 = k.c();
        i = ((e)localObject1);
        localObject1 = new org/npci/upi/security/pinactivitycomponent/ac;
        localObject2 = k.c();
        localObject3 = k.d();
        String str2 = k.a();
        ((ac)localObject1).<init>((e)localObject2, (ab)localObject3, str2);
        g = ((ac)localObject1);
      }
    }
    g.b("Common Library", "get Encryptor");
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = ((StringBuilder)localObject2).append("Input Analyzer :");
    Object localObject3 = k;
    localObject2 = localObject3;
    g.b("Common Library", (String)localObject2);
    localObject2 = "Common Library";
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject1 = ((StringBuilder)localObject1).append("Input Analyzer Key Code:");
    localObject3 = k;
    localObject1 = localObject3;
    if (localObject1 == null)
    {
      localObject1 = null;
      g.b((String)localObject2, (String)localObject1);
      localObject1 = "Common Library";
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("Input Analyzer Common Library:");
      localObject3 = k;
      localObject2 = localObject3;
      if (localObject2 != null) {
        break label225;
      }
    }
    for (;;)
    {
      g.b((String)localObject1, str1);
      return g;
      localObject1 = k.a();
      break;
      label225:
      str1 = k.c().toString();
    }
  }
  
  public Activity c()
  {
    return j;
  }
  
  public ResultReceiver d()
  {
    Object localObject = k;
    if (localObject == null) {}
    for (localObject = null;; localObject = k.e()) {
      return (ResultReceiver)localObject;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/w.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */