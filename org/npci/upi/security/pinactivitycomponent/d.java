package org.npci.upi.security.pinactivitycomponent;

import android.content.Context;

public class d
  extends Exception
{
  String a = "CLException";
  private String b;
  private String c;
  private Context d;
  
  public d(Context paramContext, String paramString1, String paramString2)
  {
    b = paramString1;
    c = paramString2;
    d = paramContext;
    a(paramContext, paramString2);
  }
  
  public d(Context paramContext, String paramString1, String paramString2, Throwable paramThrowable)
  {
    super(paramThrowable);
    b = paramString1;
    c = paramString2;
    d = paramContext;
    a(paramContext, paramString2);
  }
  
  public String a()
  {
    return c;
  }
  
  /* Error */
  public void a(Context paramContext, String paramString)
  {
    // Byte code:
    //   0: new 32	java/util/Properties
    //   3: astore_3
    //   4: aload_3
    //   5: invokespecial 33	java/util/Properties:<init>	()V
    //   8: aload_1
    //   9: invokevirtual 39	android/content/Context:getAssets	()Landroid/content/res/AssetManager;
    //   12: astore 4
    //   14: iconst_0
    //   15: istore 5
    //   17: aconst_null
    //   18: astore 6
    //   20: ldc 41
    //   22: astore 7
    //   24: aload 4
    //   26: aload 7
    //   28: invokevirtual 47	android/content/res/AssetManager:open	(Ljava/lang/String;)Ljava/io/InputStream;
    //   31: astore 4
    //   33: aload_3
    //   34: aload 4
    //   36: invokevirtual 51	java/util/Properties:load	(Ljava/io/InputStream;)V
    //   39: aload_3
    //   40: aload_2
    //   41: invokevirtual 55	java/util/Properties:getProperty	(Ljava/lang/String;)Ljava/lang/String;
    //   44: astore 4
    //   46: aload_0
    //   47: getfield 18	org/npci/upi/security/pinactivitycomponent/d:a	Ljava/lang/String;
    //   50: astore 6
    //   52: new 57	java/lang/StringBuilder
    //   55: astore_3
    //   56: aload_3
    //   57: invokespecial 58	java/lang/StringBuilder:<init>	()V
    //   60: aload_3
    //   61: ldc 60
    //   63: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: aload 4
    //   68: invokevirtual 64	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: invokevirtual 68	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   74: astore 4
    //   76: aload 6
    //   78: aload 4
    //   80: invokestatic 74	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   83: pop
    //   84: aload_1
    //   85: invokevirtual 78	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   88: astore 4
    //   90: getstatic 84	org/npci/upi/security/pinactivitycomponent/a$f:error_msg	I
    //   93: istore 5
    //   95: aload 4
    //   97: iload 5
    //   99: invokevirtual 90	android/content/res/Resources:getString	(I)Ljava/lang/String;
    //   102: astore_3
    //   103: aload_1
    //   104: astore 4
    //   106: aload_1
    //   107: checkcast 92	android/app/Activity
    //   110: astore 4
    //   112: getstatic 97	org/npci/upi/security/pinactivitycomponent/a$d:error_layout	I
    //   115: istore 5
    //   117: aload 4
    //   119: iload 5
    //   121: invokevirtual 101	android/app/Activity:findViewById	(I)Landroid/view/View;
    //   124: checkcast 103	android/widget/RelativeLayout
    //   127: astore 4
    //   129: aload_1
    //   130: checkcast 92	android/app/Activity
    //   133: astore_1
    //   134: getstatic 106	org/npci/upi/security/pinactivitycomponent/a$d:error_message	I
    //   137: istore 5
    //   139: aload_1
    //   140: iload 5
    //   142: invokevirtual 101	android/app/Activity:findViewById	(I)Landroid/view/View;
    //   145: checkcast 108	android/widget/TextView
    //   148: astore 6
    //   150: aload 4
    //   152: iconst_0
    //   153: invokevirtual 112	android/widget/RelativeLayout:setVisibility	(I)V
    //   156: aload 6
    //   158: aload_3
    //   159: invokevirtual 116	android/widget/TextView:setText	(Ljava/lang/CharSequence;)V
    //   162: return
    //   163: astore 4
    //   165: aload_0
    //   166: getfield 18	org/npci/upi/security/pinactivitycomponent/d:a	Ljava/lang/String;
    //   169: astore 7
    //   171: aload 4
    //   173: invokevirtual 121	java/io/IOException:getLocalizedMessage	()Ljava/lang/String;
    //   176: astore 4
    //   178: aload 7
    //   180: aload 4
    //   182: invokestatic 74	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   185: pop
    //   186: aconst_null
    //   187: astore 4
    //   189: goto -156 -> 33
    //   192: astore 4
    //   194: aload_0
    //   195: getfield 18	org/npci/upi/security/pinactivitycomponent/d:a	Ljava/lang/String;
    //   198: astore 6
    //   200: aload 4
    //   202: invokevirtual 121	java/io/IOException:getLocalizedMessage	()Ljava/lang/String;
    //   205: astore 4
    //   207: aload 6
    //   209: aload 4
    //   211: invokestatic 74	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   214: pop
    //   215: goto -176 -> 39
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	218	0	this	d
    //   0	218	1	paramContext	Context
    //   0	218	2	paramString	String
    //   3	156	3	localObject1	Object
    //   12	139	4	localObject2	Object
    //   163	9	4	localIOException1	java.io.IOException
    //   176	12	4	str1	String
    //   192	9	4	localIOException2	java.io.IOException
    //   205	5	4	str2	String
    //   15	126	5	i	int
    //   18	190	6	localObject3	Object
    //   22	157	7	str3	String
    // Exception table:
    //   from	to	target	type
    //   26	31	163	java/io/IOException
    //   34	39	192	java/io/IOException
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/pinactivitycomponent/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */