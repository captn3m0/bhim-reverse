package org.npci.upi.security.services;

import android.os.IBinder;
import android.os.Parcel;

class c
  implements a
{
  private IBinder a;
  
  c(IBinder paramIBinder)
  {
    a = paramIBinder;
  }
  
  public String a(String paramString1, String paramString2)
  {
    Parcel localParcel1 = Parcel.obtain();
    Parcel localParcel2 = Parcel.obtain();
    Object localObject1 = "org.npci.upi.security.services.CLRemoteService";
    try
    {
      localParcel1.writeInterfaceToken((String)localObject1);
      localParcel1.writeString(paramString1);
      localParcel1.writeString(paramString2);
      localObject1 = a;
      int i = 1;
      ((IBinder)localObject1).transact(i, localParcel1, localParcel2, 0);
      localParcel2.readException();
      localObject1 = localParcel2.readString();
      return (String)localObject1;
    }
    finally
    {
      localParcel2.recycle();
      localParcel1.recycle();
    }
  }
  
  /* Error */
  public void a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, d paramd)
  {
    // Byte code:
    //   0: invokestatic 20	android/os/Parcel:obtain	()Landroid/os/Parcel;
    //   3: astore 10
    //   5: invokestatic 20	android/os/Parcel:obtain	()Landroid/os/Parcel;
    //   8: astore 11
    //   10: ldc 22
    //   12: astore 12
    //   14: aload 10
    //   16: aload 12
    //   18: invokevirtual 26	android/os/Parcel:writeInterfaceToken	(Ljava/lang/String;)V
    //   21: aload 10
    //   23: aload_1
    //   24: invokevirtual 29	android/os/Parcel:writeString	(Ljava/lang/String;)V
    //   27: aload 10
    //   29: aload_2
    //   30: invokevirtual 29	android/os/Parcel:writeString	(Ljava/lang/String;)V
    //   33: aload 10
    //   35: aload_3
    //   36: invokevirtual 29	android/os/Parcel:writeString	(Ljava/lang/String;)V
    //   39: aload 10
    //   41: aload 4
    //   43: invokevirtual 29	android/os/Parcel:writeString	(Ljava/lang/String;)V
    //   46: aload 10
    //   48: aload 5
    //   50: invokevirtual 29	android/os/Parcel:writeString	(Ljava/lang/String;)V
    //   53: aload 10
    //   55: aload 6
    //   57: invokevirtual 29	android/os/Parcel:writeString	(Ljava/lang/String;)V
    //   60: aload 10
    //   62: aload 7
    //   64: invokevirtual 29	android/os/Parcel:writeString	(Ljava/lang/String;)V
    //   67: aload 10
    //   69: aload 8
    //   71: invokevirtual 29	android/os/Parcel:writeString	(Ljava/lang/String;)V
    //   74: aload 9
    //   76: ifnull +59 -> 135
    //   79: aload 9
    //   81: invokeinterface 52 1 0
    //   86: astore 12
    //   88: aload 10
    //   90: aload 12
    //   92: invokevirtual 56	android/os/Parcel:writeStrongBinder	(Landroid/os/IBinder;)V
    //   95: aload_0
    //   96: getfield 14	org/npci/upi/security/services/c:a	Landroid/os/IBinder;
    //   99: astore 12
    //   101: iconst_3
    //   102: istore 13
    //   104: aload 12
    //   106: iload 13
    //   108: aload 10
    //   110: aload 11
    //   112: iconst_0
    //   113: invokeinterface 36 5 0
    //   118: pop
    //   119: aload 11
    //   121: invokevirtual 39	android/os/Parcel:readException	()V
    //   124: aload 11
    //   126: invokevirtual 46	android/os/Parcel:recycle	()V
    //   129: aload 10
    //   131: invokevirtual 46	android/os/Parcel:recycle	()V
    //   134: return
    //   135: aconst_null
    //   136: astore 12
    //   138: goto -50 -> 88
    //   141: astore 12
    //   143: aload 11
    //   145: invokevirtual 46	android/os/Parcel:recycle	()V
    //   148: aload 10
    //   150: invokevirtual 46	android/os/Parcel:recycle	()V
    //   153: aload 12
    //   155: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	156	0	this	c
    //   0	156	1	paramString1	String
    //   0	156	2	paramString2	String
    //   0	156	3	paramString3	String
    //   0	156	4	paramString4	String
    //   0	156	5	paramString5	String
    //   0	156	6	paramString6	String
    //   0	156	7	paramString7	String
    //   0	156	8	paramString8	String
    //   0	156	9	paramd	d
    //   3	146	10	localParcel1	Parcel
    //   8	136	11	localParcel2	Parcel
    //   12	125	12	localObject1	Object
    //   141	13	12	localObject2	Object
    //   102	5	13	i	int
    // Exception table:
    //   from	to	target	type
    //   16	21	141	finally
    //   23	27	141	finally
    //   29	33	141	finally
    //   35	39	141	finally
    //   41	46	141	finally
    //   48	53	141	finally
    //   55	60	141	finally
    //   62	67	141	finally
    //   69	74	141	finally
    //   79	86	141	finally
    //   90	95	141	finally
    //   95	99	141	finally
    //   112	119	141	finally
    //   119	124	141	finally
  }
  
  public boolean a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    boolean bool = false;
    Object localObject1 = null;
    Parcel localParcel1 = Parcel.obtain();
    Parcel localParcel2 = Parcel.obtain();
    Object localObject3 = "org.npci.upi.security.services.CLRemoteService";
    try
    {
      localParcel1.writeInterfaceToken((String)localObject3);
      localParcel1.writeString(paramString1);
      localParcel1.writeString(paramString2);
      localParcel1.writeString(paramString3);
      localParcel1.writeString(paramString4);
      localObject3 = a;
      int i = 2;
      ((IBinder)localObject3).transact(i, localParcel1, localParcel2, 0);
      localParcel2.readException();
      int j = localParcel2.readInt();
      if (j != 0) {
        bool = true;
      }
      return bool;
    }
    finally
    {
      localParcel2.recycle();
      localParcel1.recycle();
    }
  }
  
  public IBinder asBinder()
  {
    return a;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/services/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */