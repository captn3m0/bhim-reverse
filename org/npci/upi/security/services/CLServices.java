package org.npci.upi.security.services;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class CLServices
{
  private static final Uri GET_CHALLENGE_URI = Uri.parse("content://org.npci.upi.security.pinactivitycomponent.clservices/getChallenge");
  private static final Uri GET_CREDENTIAL_URI = Uri.parse("content://org.npci.upi.security.pinactivitycomponent.clservices/getCredential");
  private static final String PROVIDER_NAME = "org.npci.upi.security.pinactivitycomponent.clservices";
  private static final Uri REGISTER_APP_URI = Uri.parse("content://org.npci.upi.security.pinactivitycomponent.clservices/registerApp");
  private static CLServices clServices = null;
  private a clRemoteService = null;
  private Context mContext;
  private ServiceConnectionStatusNotifier notifier;
  private ServiceConnection serviceConnection;
  
  private CLServices(Context paramContext, ServiceConnectionStatusNotifier paramServiceConnectionStatusNotifier)
  {
    Object localObject1 = new org/npci/upi/security/services/CLServices$1;
    ((CLServices.1)localObject1).<init>(this);
    serviceConnection = ((ServiceConnection)localObject1);
    mContext = paramContext;
    notifier = paramServiceConnectionStatusNotifier;
    localObject1 = new android/content/Intent;
    ((Intent)localObject1).<init>();
    ((Intent)localObject1).setAction("org.npci.upi.security.services.CLRemoteService");
    Object localObject2 = mContext.getPackageName();
    ((Intent)localObject1).setPackage((String)localObject2);
    localObject2 = mContext;
    ServiceConnection localServiceConnection = serviceConnection;
    ((Context)localObject2).bindService((Intent)localObject1, localServiceConnection, 1);
  }
  
  public static void initService(Context paramContext, ServiceConnectionStatusNotifier paramServiceConnectionStatusNotifier)
  {
    Object localObject = clServices;
    if (localObject != null)
    {
      localObject = new java/lang/RuntimeException;
      ((RuntimeException)localObject).<init>("Service already initiated");
      throw ((Throwable)localObject);
    }
    localObject = new org/npci/upi/security/services/CLServices;
    ((CLServices)localObject).<init>(paramContext, paramServiceConnectionStatusNotifier);
    clServices = (CLServices)localObject;
  }
  
  public String getChallenge(String paramString1, String paramString2)
  {
    String str1 = null;
    Object localObject = CLServices.class.getName();
    String str2 = "GetChallenge called";
    Log.d((String)localObject, str2);
    if (paramString1 != null)
    {
      localObject = paramString1.trim();
      boolean bool = ((String)localObject).isEmpty();
      if ((!bool) && (paramString2 != null))
      {
        localObject = paramString2.trim();
        bool = ((String)localObject).isEmpty();
        if (!bool) {
          break label86;
        }
      }
    }
    localObject = CLServices.class.getName();
    str2 = "In-sufficient arguments provided";
    Log.d((String)localObject, str2);
    for (;;)
    {
      return str1;
      try
      {
        label86:
        localObject = clRemoteService;
        str1 = ((a)localObject).a(paramString1, paramString2);
      }
      catch (RemoteException localRemoteException)
      {
        localRemoteException.printStackTrace();
      }
    }
  }
  
  public void getCredential(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, CLRemoteResultReceiver paramCLRemoteResultReceiver)
  {
    Object localObject1 = CLServices.class.getName();
    Object localObject2 = "Get Credential called";
    Log.d((String)localObject1, (String)localObject2);
    try
    {
      localObject1 = clRemoteService;
      localObject2 = paramCLRemoteResultReceiver.getBinder();
      d locald = e.a((IBinder)localObject2);
      localObject2 = paramString1;
      ((a)localObject1).a(paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8, locald);
      return;
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        localRemoteException.printStackTrace();
      }
    }
  }
  
  public boolean registerApp(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    boolean bool1 = false;
    Object localObject = CLServices.class.getName();
    String str = "Register App called";
    Log.d((String)localObject, str);
    if (paramString1 != null)
    {
      localObject = paramString1.trim();
      boolean bool2 = ((String)localObject).isEmpty();
      if ((!bool2) && (paramString2 != null))
      {
        localObject = paramString2.trim();
        bool2 = ((String)localObject).isEmpty();
        if ((!bool2) && (paramString3 != null))
        {
          localObject = paramString3.trim();
          bool2 = ((String)localObject).isEmpty();
          if ((!bool2) && (paramString4 != null))
          {
            localObject = paramString4.trim();
            bool2 = ((String)localObject).isEmpty();
            if (!bool2) {
              break label134;
            }
          }
        }
      }
    }
    localObject = CLServices.class.getName();
    str = "In-sufficient arguments provided";
    Log.d((String)localObject, str);
    for (;;)
    {
      return bool1;
      try
      {
        label134:
        localObject = clRemoteService;
        bool1 = ((a)localObject).a(paramString1, paramString2, paramString3, paramString4);
      }
      catch (RemoteException localRemoteException)
      {
        localRemoteException.printStackTrace();
      }
    }
  }
  
  public void unbindService()
  {
    Context localContext = mContext;
    ServiceConnection localServiceConnection = serviceConnection;
    localContext.unbindService(localServiceConnection);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/services/CLServices.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */