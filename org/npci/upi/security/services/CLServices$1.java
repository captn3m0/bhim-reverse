package org.npci.upi.security.services;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

class CLServices$1
  implements ServiceConnection
{
  CLServices$1(CLServices paramCLServices) {}
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    Object localObject1 = a;
    Object localObject2 = b.a(paramIBinder);
    CLServices.access$002((CLServices)localObject1, (a)localObject2);
    localObject1 = CLServices.access$200(a);
    localObject2 = CLServices.access$100();
    ((ServiceConnectionStatusNotifier)localObject1).serviceConnected((CLServices)localObject2);
    Log.d("Remote Service", "Service Connected");
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    CLServices.access$002(a, null);
    CLServices.access$200(a).serviceDisconnected();
    Log.d("Remote Service", "Service Disconnected");
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/services/CLServices$1.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */