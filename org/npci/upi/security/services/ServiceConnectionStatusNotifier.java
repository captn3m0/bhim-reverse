package org.npci.upi.security.services;

public abstract interface ServiceConnectionStatusNotifier
{
  public abstract void serviceConnected(CLServices paramCLServices);
  
  public abstract void serviceDisconnected();
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/services/ServiceConnectionStatusNotifier.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */