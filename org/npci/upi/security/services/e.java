package org.npci.upi.security.services;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public abstract class e
  extends Binder
  implements d
{
  public e()
  {
    attachInterface(this, "org.npci.upi.security.services.CLResultReceiver");
  }
  
  public static d a(IBinder paramIBinder)
  {
    Object localObject;
    if (paramIBinder == null) {
      localObject = null;
    }
    for (;;)
    {
      return (d)localObject;
      localObject = paramIBinder.queryLocalInterface("org.npci.upi.security.services.CLResultReceiver");
      if (localObject != null)
      {
        boolean bool = localObject instanceof d;
        if (bool)
        {
          localObject = (d)localObject;
          continue;
        }
      }
      localObject = new org/npci/upi/security/services/f;
      ((f)localObject).<init>(paramIBinder);
    }
  }
  
  public IBinder asBinder()
  {
    return this;
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
  {
    boolean bool1 = false;
    Object localObject = null;
    boolean bool2 = true;
    switch (paramInt1)
    {
    default: 
      bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
    }
    for (;;)
    {
      return bool1;
      localObject = "org.npci.upi.security.services.CLResultReceiver";
      paramParcel2.writeString((String)localObject);
      bool1 = bool2;
      continue;
      String str = "org.npci.upi.security.services.CLResultReceiver";
      paramParcel1.enforceInterface(str);
      int i = paramParcel1.readInt();
      if (i != 0) {
        localObject = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      a((Bundle)localObject);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      str = "org.npci.upi.security.services.CLResultReceiver";
      paramParcel1.enforceInterface(str);
      i = paramParcel1.readInt();
      if (i != 0) {
        localObject = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      b((Bundle)localObject);
      paramParcel2.writeNoException();
      bool1 = bool2;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/services/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */