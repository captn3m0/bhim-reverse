package org.npci.upi.security.services;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;

public abstract class b
  extends Binder
  implements a
{
  public b()
  {
    attachInterface(this, "org.npci.upi.security.services.CLRemoteService");
  }
  
  public static a a(IBinder paramIBinder)
  {
    Object localObject;
    if (paramIBinder == null) {
      localObject = null;
    }
    for (;;)
    {
      return (a)localObject;
      localObject = paramIBinder.queryLocalInterface("org.npci.upi.security.services.CLRemoteService");
      if (localObject != null)
      {
        boolean bool = localObject instanceof a;
        if (bool)
        {
          localObject = (a)localObject;
          continue;
        }
      }
      localObject = new org/npci/upi/security/services/c;
      ((c)localObject).<init>(paramIBinder);
    }
  }
  
  public IBinder asBinder()
  {
    return this;
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
  {
    int i = 1;
    switch (paramInt1)
    {
    default: 
      i = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
    }
    for (;;)
    {
      return i;
      Object localObject = "org.npci.upi.security.services.CLRemoteService";
      paramParcel2.writeString((String)localObject);
      continue;
      paramParcel1.enforceInterface("org.npci.upi.security.services.CLRemoteService");
      localObject = paramParcel1.readString();
      String str1 = paramParcel1.readString();
      localObject = a((String)localObject, str1);
      paramParcel2.writeNoException();
      paramParcel2.writeString((String)localObject);
      continue;
      paramParcel1.enforceInterface("org.npci.upi.security.services.CLRemoteService");
      localObject = paramParcel1.readString();
      str1 = paramParcel1.readString();
      String str2 = paramParcel1.readString();
      String str3 = paramParcel1.readString();
      int j = a((String)localObject, str1, str2, str3);
      paramParcel2.writeNoException();
      if (j != 0) {
        j = i;
      }
      for (;;)
      {
        paramParcel2.writeInt(j);
        break;
        int k = 0;
        localObject = null;
      }
      paramParcel1.enforceInterface("org.npci.upi.security.services.CLRemoteService");
      str1 = paramParcel1.readString();
      str2 = paramParcel1.readString();
      str3 = paramParcel1.readString();
      String str4 = paramParcel1.readString();
      String str5 = paramParcel1.readString();
      String str6 = paramParcel1.readString();
      String str7 = paramParcel1.readString();
      String str8 = paramParcel1.readString();
      d locald = e.a(paramParcel1.readStrongBinder());
      localObject = this;
      a(str1, str2, str3, str4, str5, str6, str7, str8, locald);
      paramParcel2.writeNoException();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/org/npci/upi/security/services/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */