package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import in.juspay.widget.qrscanner.a.b;
import in.juspay.widget.qrscanner.com.google.zxing.f;

public class h
{
  private static final String a = h.class.getSimpleName();
  private in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b b;
  private HandlerThread c;
  private Handler d;
  private e e;
  private Handler f;
  private Rect g;
  private boolean h = false;
  private final Object i;
  private final Handler.Callback j;
  private final in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.k k;
  
  public h(in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b paramb, e parame, Handler paramHandler)
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    i = localObject;
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$1;
    ((h.1)localObject).<init>(this);
    j = ((Handler.Callback)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2;
    ((h.2)localObject).<init>(this);
    k = ((in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.k)localObject);
    n.a();
    b = paramb;
    e = parame;
    f = paramHandler;
  }
  
  private void b(m paramm)
  {
    System.currentTimeMillis();
    Object localObject1 = null;
    Object localObject2 = g;
    paramm.a((Rect)localObject2);
    localObject2 = a(paramm);
    if (localObject2 != null) {
      localObject1 = e.a((f)localObject2);
    }
    int m;
    if (localObject1 != null)
    {
      System.currentTimeMillis();
      localObject2 = f;
      if (localObject2 != null)
      {
        localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/b;
        ((b)localObject2).<init>((in.juspay.widget.qrscanner.com.google.zxing.k)localObject1, paramm);
        localObject1 = f;
        m = a.b.zxing_decode_succeeded;
        localObject1 = Message.obtain((Handler)localObject1, m, localObject2);
        localObject2 = new android/os/Bundle;
        ((Bundle)localObject2).<init>();
        ((Message)localObject1).setData((Bundle)localObject2);
        ((Message)localObject1).sendToTarget();
      }
    }
    for (;;)
    {
      localObject1 = f;
      if (localObject1 != null)
      {
        localObject1 = e.a();
        localObject2 = f;
        m = a.b.zxing_possible_result_points;
        localObject1 = Message.obtain((Handler)localObject2, m, localObject1);
        ((Message)localObject1).sendToTarget();
      }
      c();
      return;
      localObject1 = f;
      if (localObject1 != null)
      {
        localObject1 = f;
        int n = a.b.zxing_decode_failed;
        localObject1 = Message.obtain((Handler)localObject1, n);
        ((Message)localObject1).sendToTarget();
      }
    }
  }
  
  private void c()
  {
    in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b localb = b;
    boolean bool = localb.f();
    if (bool)
    {
      localb = b;
      in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.k localk = k;
      localb.a(localk);
    }
  }
  
  protected f a(m paramm)
  {
    Object localObject = g;
    if (localObject == null) {}
    for (localObject = null;; localObject = paramm.b()) {
      return (f)localObject;
    }
  }
  
  public void a()
  {
    n.a();
    Object localObject1 = new android/os/HandlerThread;
    Object localObject2 = a;
    ((HandlerThread)localObject1).<init>((String)localObject2);
    c = ((HandlerThread)localObject1);
    c.start();
    localObject1 = new android/os/Handler;
    localObject2 = c.getLooper();
    Handler.Callback localCallback = j;
    ((Handler)localObject1).<init>((Looper)localObject2, localCallback);
    d = ((Handler)localObject1);
    h = true;
    c();
  }
  
  public void a(Rect paramRect)
  {
    g = paramRect;
  }
  
  public void a(e parame)
  {
    e = parame;
  }
  
  public void b()
  {
    n.a();
    Object localObject1 = i;
    Object localObject2 = null;
    try
    {
      h = false;
      localObject2 = d;
      ((Handler)localObject2).removeCallbacksAndMessages(null);
      localObject2 = c;
      ((HandlerThread)localObject2).quit();
      return;
    }
    finally {}
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */