package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import in.juspay.widget.qrscanner.a.b;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.n;

public class b
{
  private static final String a = b.class.getSimpleName();
  private f b;
  private e c;
  private c d;
  private Handler e;
  private h f;
  private boolean g = false;
  private d h;
  private Runnable i;
  private Runnable j;
  private Runnable k;
  private Runnable l;
  
  public b(Context paramContext)
  {
    Object localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
    ((d)localObject).<init>();
    h = ((d)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$3;
    ((b.3)localObject).<init>(this);
    i = ((Runnable)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$4;
    ((b.4)localObject).<init>(this);
    j = ((Runnable)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$5;
    ((b.5)localObject).<init>(this);
    k = ((Runnable)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$6;
    ((b.6)localObject).<init>(this);
    l = ((Runnable)localObject);
    n.a();
    localObject = f.a();
    b = ((f)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c;
    ((c)localObject).<init>(paramContext);
    d = ((c)localObject);
    localObject = d;
    d locald = h;
    ((c)localObject).a(locald);
  }
  
  private void a(Exception paramException)
  {
    Object localObject = e;
    if (localObject != null)
    {
      localObject = e;
      int m = a.b.zxing_camera_error;
      localObject = ((Handler)localObject).obtainMessage(m, paramException);
      ((Message)localObject).sendToTarget();
    }
  }
  
  private l g()
  {
    return d.h();
  }
  
  private void h()
  {
    boolean bool = g;
    if (!bool)
    {
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("CameraInstance is not open");
      throw localIllegalStateException;
    }
  }
  
  public h a()
  {
    return f;
  }
  
  public void a(Handler paramHandler)
  {
    e = paramHandler;
  }
  
  public void a(d paramd)
  {
    boolean bool = g;
    if (!bool)
    {
      h = paramd;
      c localc = d;
      localc.a(paramd);
    }
  }
  
  public void a(e parame)
  {
    c = parame;
  }
  
  public void a(h paramh)
  {
    f = paramh;
    d.a(paramh);
  }
  
  public void a(k paramk)
  {
    h();
    f localf = b;
    b.2 local2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$2;
    local2.<init>(this, paramk);
    localf.a(local2);
  }
  
  public void a(boolean paramBoolean)
  {
    n.a();
    boolean bool = g;
    if (bool)
    {
      f localf = b;
      b.1 local1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b$1;
      local1.<init>(this, paramBoolean);
      localf.a(local1);
    }
  }
  
  public void b()
  {
    n.a();
    g = true;
    f localf = b;
    Runnable localRunnable = i;
    localf.b(localRunnable);
  }
  
  public void c()
  {
    n.a();
    h();
    f localf = b;
    Runnable localRunnable = j;
    localf.a(localRunnable);
  }
  
  public void d()
  {
    n.a();
    h();
    f localf = b;
    Runnable localRunnable = k;
    localf.a(localRunnable);
  }
  
  public void e()
  {
    n.a();
    boolean bool = g;
    if (bool)
    {
      f localf = b;
      Runnable localRunnable = l;
      localf.a(localRunnable);
    }
    g = false;
  }
  
  public boolean f()
  {
    return g;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */