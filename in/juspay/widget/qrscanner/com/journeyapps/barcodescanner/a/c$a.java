package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a;

import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.m;

final class c$a
  implements Camera.PreviewCallback
{
  private k b;
  private l c;
  
  public c$a(c paramc) {}
  
  public void a(k paramk)
  {
    b = paramk;
  }
  
  public void a(l paraml)
  {
    c = paraml;
  }
  
  public void onPreviewFrame(byte[] paramArrayOfByte, Camera paramCamera)
  {
    Object localObject1 = c;
    k localk = b;
    Object localObject2;
    int i;
    if ((localObject1 != null) && (localk != null))
    {
      localObject2 = paramCamera.getParameters();
      i = ((Camera.Parameters)localObject2).getPreviewFormat();
    }
    for (;;)
    {
      try
      {
        localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m;
        int j = a;
        int k = b;
        localObject1 = a;
        int m = ((c)localObject1).g();
        localObject1 = paramArrayOfByte;
        ((m)localObject2).<init>(paramArrayOfByte, j, k, i, m);
        localk.a((m)localObject2);
        return;
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        localk.a(localIllegalArgumentException);
        continue;
      }
      if (localk != null)
      {
        Exception localException = new java/lang/Exception;
        localObject1 = "No resolution available";
        localException.<init>((String)localObject1);
        localk.a(localException);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */