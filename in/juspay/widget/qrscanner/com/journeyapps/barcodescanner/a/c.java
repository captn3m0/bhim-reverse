package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.Size;
import android.os.Build;
import android.os.Build.VERSION;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class c
{
  private static final String a = c.class.getSimpleName();
  private Camera b;
  private Camera.CameraInfo c;
  private a d;
  private in.juspay.widget.qrscanner.com.google.zxing.a.a.a e;
  private boolean f;
  private String g;
  private d h;
  private h i;
  private l j;
  private l k;
  private int l;
  private Context m;
  private final c.a n;
  
  public c(Context paramContext)
  {
    Object localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
    ((d)localObject).<init>();
    h = ((d)localObject);
    l = -1;
    m = paramContext;
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c$a;
    ((c.a)localObject).<init>(this);
    n = ((c.a)localObject);
  }
  
  private static List a(Camera.Parameters paramParameters)
  {
    Object localObject1 = paramParameters.getSupportedPreviewSizes();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = paramParameters.getPreviewSize();
      if (localObject1 != null)
      {
        localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
        int i1 = width;
        int i2 = height;
        ((l)localObject2).<init>(i1, i2);
        localArrayList.add(localObject2);
      }
    }
    for (localObject1 = localArrayList;; localObject1 = localArrayList)
    {
      return (List)localObject1;
      localObject2 = ((List)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (Camera.Size)((Iterator)localObject2).next();
        l locall = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
        int i4 = width;
        int i3 = height;
        locall.<init>(i4, i3);
        localArrayList.add(locall);
      }
    }
  }
  
  private void a(int paramInt)
  {
    b.setDisplayOrientation(paramInt);
  }
  
  private void b(boolean paramBoolean)
  {
    Camera.Parameters localParameters = j();
    if (localParameters == null) {
      return;
    }
    if (paramBoolean) {}
    Object localObject1 = h.g();
    in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.a(localParameters, (d.a)localObject1, paramBoolean);
    int i1;
    if (!paramBoolean)
    {
      in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.a(localParameters, false);
      localObject1 = h;
      boolean bool1 = ((d)localObject1).b();
      if (bool1) {
        in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.f(localParameters);
      }
      localObject1 = h;
      bool1 = ((d)localObject1).c();
      if (bool1) {
        in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.e(localParameters);
      }
      localObject1 = h;
      bool1 = ((d)localObject1).e();
      if (bool1)
      {
        i1 = Build.VERSION.SDK_INT;
        i3 = 15;
        if (i1 >= i3)
        {
          in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.d(localParameters);
          in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.b(localParameters);
          in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.c(localParameters);
        }
      }
    }
    localObject1 = a(localParameters);
    int i3 = ((List)localObject1).size();
    if (i3 == 0)
    {
      i1 = 0;
      localObject1 = null;
      j = null;
    }
    for (;;)
    {
      localObject1 = Build.DEVICE;
      Object localObject2 = "glass-1";
      boolean bool2 = ((String)localObject1).equals(localObject2);
      if (bool2) {
        in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.a(localParameters);
      }
      localObject1 = b;
      ((Camera)localObject1).setParameters(localParameters);
      break;
      localObject2 = i;
      boolean bool3 = f();
      localObject1 = ((h)localObject2).a((List)localObject1, bool3);
      j = ((l)localObject1);
      localObject1 = j;
      int i2 = a;
      localObject2 = j;
      i3 = b;
      localParameters.setPreviewSize(i2, i3);
    }
  }
  
  private Camera.Parameters j()
  {
    Camera.Parameters localParameters = b.getParameters();
    String str = g;
    if (str == null)
    {
      str = localParameters.flatten();
      g = str;
    }
    for (;;)
    {
      return localParameters;
      str = g;
      localParameters.unflatten(str);
    }
  }
  
  private int k()
  {
    int i1 = 0;
    Object localObject = i;
    int i2 = ((h)localObject).a();
    switch (i2)
    {
    default: 
      localObject = c;
      i2 = facing;
      int i3 = 1;
      if (i2 == i3)
      {
        localObject = c;
        i2 = orientation;
        i1 = (i1 + i2) % 360;
      }
      break;
    }
    for (i1 = (360 - i1) % 360;; i1 = (i2 - i1 + 360) % 360)
    {
      return i1;
      i1 = 90;
      break;
      i1 = 180;
      break;
      i1 = 270;
      break;
      localObject = c;
      i2 = orientation;
    }
  }
  
  private void l()
  {
    try
    {
      i1 = k();
      l = i1;
      i1 = l;
      a(i1);
    }
    catch (Exception localException3)
    {
      int i1;
      Object localObject;
      for (;;) {}
    }
    i1 = 0;
    localObject = null;
    try
    {
      b(false);
      localObject = b.getParameters().getPreviewSize();
      if (localObject == null)
      {
        localObject = j;
        k = ((l)localObject);
        localObject = n;
        locall = k;
        ((c.a)localObject).a(locall);
        return;
      }
    }
    catch (Exception localException1)
    {
      for (;;)
      {
        i1 = 1;
        try
        {
          b(i1);
        }
        catch (Exception localException2) {}
        continue;
        l locall = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
        int i3 = width;
        int i2 = height;
        locall.<init>(i3, i2);
        k = locall;
      }
    }
  }
  
  public void a()
  {
    int i1 = h.a();
    Object localObject = in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.a.b(i1);
    b = ((Camera)localObject);
    localObject = b;
    if (localObject == null)
    {
      localObject = new java/lang/RuntimeException;
      ((RuntimeException)localObject).<init>("Failed to open camera");
      throw ((Throwable)localObject);
    }
    i1 = in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.a.a(h.a());
    Camera.CameraInfo localCameraInfo = new android/hardware/Camera$CameraInfo;
    localCameraInfo.<init>();
    c = localCameraInfo;
    localCameraInfo = c;
    Camera.getCameraInfo(i1, localCameraInfo);
  }
  
  public void a(d paramd)
  {
    h = paramd;
  }
  
  public void a(e parame)
  {
    Camera localCamera = b;
    parame.a(localCamera);
  }
  
  public void a(h paramh)
  {
    i = paramh;
  }
  
  public void a(k paramk)
  {
    Camera localCamera = b;
    if (localCamera != null)
    {
      boolean bool = f;
      if (bool)
      {
        n.a(paramk);
        c.a locala = n;
        localCamera.setOneShotPreviewCallback(locala);
      }
    }
  }
  
  public void a(boolean paramBoolean)
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      boolean bool1 = i();
      if (paramBoolean != bool1)
      {
        localObject1 = d;
        if (localObject1 != null)
        {
          localObject1 = d;
          ((a)localObject1).b();
        }
        localObject1 = b.getParameters();
        in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.a((Camera.Parameters)localObject1, paramBoolean);
        Object localObject2 = h;
        boolean bool2 = ((d)localObject2).d();
        if (bool2) {
          in.juspay.widget.qrscanner.com.google.zxing.a.a.a.a.b((Camera.Parameters)localObject1, paramBoolean);
        }
        localObject2 = b;
        ((Camera)localObject2).setParameters((Camera.Parameters)localObject1);
        localObject1 = d;
        if (localObject1 != null)
        {
          localObject1 = d;
          ((a)localObject1).a();
        }
      }
    }
  }
  
  public void b()
  {
    Object localObject = b;
    if (localObject == null)
    {
      localObject = new java/lang/RuntimeException;
      ((RuntimeException)localObject).<init>("Camera not open");
      throw ((Throwable)localObject);
    }
    l();
  }
  
  public void c()
  {
    Object localObject1 = b;
    if (localObject1 != null)
    {
      boolean bool1 = f;
      if (!bool1)
      {
        ((Camera)localObject1).startPreview();
        boolean bool2 = true;
        f = bool2;
        localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a;
        Object localObject2 = b;
        d locald = h;
        ((a)localObject1).<init>((Camera)localObject2, locald);
        d = ((a)localObject1);
        localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/a/a/a;
        localObject2 = m;
        locald = h;
        ((in.juspay.widget.qrscanner.com.google.zxing.a.a.a)localObject1).<init>((Context)localObject2, this, locald);
        e = ((in.juspay.widget.qrscanner.com.google.zxing.a.a.a)localObject1);
        localObject1 = e;
        ((in.juspay.widget.qrscanner.com.google.zxing.a.a.a)localObject1).a();
      }
    }
  }
  
  public void d()
  {
    Object localObject = d;
    if (localObject != null)
    {
      localObject = d;
      ((a)localObject).b();
      d = null;
    }
    localObject = e;
    if (localObject != null)
    {
      localObject = e;
      ((in.juspay.widget.qrscanner.com.google.zxing.a.a.a)localObject).b();
      e = null;
    }
    localObject = b;
    if (localObject != null)
    {
      boolean bool = f;
      if (bool)
      {
        b.stopPreview();
        n.a(null);
        bool = false;
        localObject = null;
        f = false;
      }
    }
  }
  
  public void e()
  {
    Camera localCamera = b;
    if (localCamera != null)
    {
      b.release();
      localCamera = null;
      b = null;
    }
  }
  
  public boolean f()
  {
    int i1 = l;
    int i3 = -1;
    IllegalStateException localIllegalStateException;
    if (i1 == i3)
    {
      localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Rotation not calculated yet. Call configure() first.");
      throw localIllegalStateException;
    }
    i1 = l % 180;
    if (i1 != 0) {
      i1 = 1;
    }
    for (;;)
    {
      return i1;
      int i2 = 0;
      localIllegalStateException = null;
    }
  }
  
  public int g()
  {
    return l;
  }
  
  public l h()
  {
    l locall = k;
    boolean bool;
    if (locall == null)
    {
      bool = false;
      locall = null;
    }
    for (;;)
    {
      return locall;
      bool = f();
      if (bool) {
        locall = k.a();
      } else {
        locall = k;
      }
    }
  }
  
  public boolean i()
  {
    boolean bool1 = false;
    Object localObject = b.getParameters();
    if (localObject != null)
    {
      localObject = ((Camera.Parameters)localObject).getFlashMode();
      if (localObject != null)
      {
        String str = "on";
        boolean bool2 = str.equals(localObject);
        if (!bool2)
        {
          str = "torch";
          boolean bool3 = str.equals(localObject);
          if (!bool3) {}
        }
        else
        {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */