package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;

class f
{
  private static final String a = f.class.getSimpleName();
  private static f b;
  private Handler c;
  private HandlerThread d;
  private int e = 0;
  private final Object f;
  
  private f()
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    f = localObject;
  }
  
  public static f a()
  {
    f localf = b;
    if (localf == null)
    {
      localf = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f;
      localf.<init>();
      b = localf;
    }
    return b;
  }
  
  private void c()
  {
    synchronized (f)
    {
      Object localObject2 = c;
      if (localObject2 != null) {
        break label102;
      }
      int i = e;
      if (i <= 0)
      {
        localObject2 = new java/lang/IllegalStateException;
        localObject5 = "CameraThread is not open";
        ((IllegalStateException)localObject2).<init>((String)localObject5);
        throw ((Throwable)localObject2);
      }
    }
    Object localObject4 = new android/os/HandlerThread;
    Object localObject5 = "CameraThread";
    ((HandlerThread)localObject4).<init>((String)localObject5);
    d = ((HandlerThread)localObject4);
    localObject4 = d;
    ((HandlerThread)localObject4).start();
    localObject4 = new android/os/Handler;
    localObject5 = d;
    localObject5 = ((HandlerThread)localObject5).getLooper();
    ((Handler)localObject4).<init>((Looper)localObject5);
    c = ((Handler)localObject4);
    label102:
  }
  
  private void d()
  {
    synchronized (f)
    {
      HandlerThread localHandlerThread = d;
      localHandlerThread.quit();
      localHandlerThread = null;
      d = null;
      localHandlerThread = null;
      c = null;
      return;
    }
  }
  
  protected void a(Runnable paramRunnable)
  {
    synchronized (f)
    {
      c();
      Handler localHandler = c;
      localHandler.post(paramRunnable);
      return;
    }
  }
  
  protected void b()
  {
    synchronized (f)
    {
      int i = e + -1;
      e = i;
      i = e;
      if (i == 0) {
        d();
      }
      return;
    }
  }
  
  protected void b(Runnable paramRunnable)
  {
    synchronized (f)
    {
      int i = e + 1;
      e = i;
      a(paramRunnable);
      return;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */