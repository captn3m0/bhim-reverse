package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Build.VERSION;
import android.view.SurfaceHolder;

public class e
{
  private SurfaceHolder a;
  private SurfaceTexture b;
  
  public e(SurfaceTexture paramSurfaceTexture)
  {
    if (paramSurfaceTexture == null)
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("surfaceTexture may not be null");
      throw localIllegalArgumentException;
    }
    b = paramSurfaceTexture;
  }
  
  public e(SurfaceHolder paramSurfaceHolder)
  {
    if (paramSurfaceHolder == null)
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("surfaceHolder may not be null");
      throw localIllegalArgumentException;
    }
    a = paramSurfaceHolder;
  }
  
  public void a(Camera paramCamera)
  {
    Object localObject = a;
    if (localObject != null)
    {
      localObject = a;
      paramCamera.setPreviewDisplay((SurfaceHolder)localObject);
    }
    for (;;)
    {
      return;
      int i = Build.VERSION.SDK_INT;
      int j = 11;
      if (i < j) {
        break;
      }
      localObject = b;
      paramCamera.setPreviewTexture((SurfaceTexture)localObject);
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("SurfaceTexture not supported.");
    throw ((Throwable)localObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */