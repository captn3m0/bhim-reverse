package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a;

import android.hardware.Camera;
import android.hardware.Camera.AutoFocusCallback;
import android.hardware.Camera.Parameters;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Message;
import java.util.ArrayList;
import java.util.Collection;

public final class a
{
  private static final String a = a.class.getSimpleName();
  private static final Collection h;
  private boolean b;
  private boolean c;
  private final boolean d;
  private final Camera e;
  private Handler f;
  private int g;
  private final Handler.Callback i;
  private final Camera.AutoFocusCallback j;
  
  static
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(2);
    h = localArrayList;
    h.add("auto");
    h.add("macro");
  }
  
  public a(Camera paramCamera, d paramd)
  {
    g = k;
    Object localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a$1;
    ((a.1)localObject1).<init>(this);
    i = ((Handler.Callback)localObject1);
    localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a$2;
    ((a.2)localObject1).<init>(this);
    j = ((Camera.AutoFocusCallback)localObject1);
    localObject1 = new android/os/Handler;
    Object localObject2 = i;
    ((Handler)localObject1).<init>((Handler.Callback)localObject2);
    f = ((Handler)localObject1);
    e = paramCamera;
    localObject1 = paramCamera.getParameters().getFocusMode();
    boolean bool1 = paramd.f();
    if (bool1)
    {
      localObject2 = h;
      boolean bool2 = ((Collection)localObject2).contains(localObject1);
      if (!bool2) {}
    }
    for (;;)
    {
      d = k;
      a();
      return;
      k = 0;
    }
  }
  
  private void c()
  {
    try
    {
      boolean bool = b;
      if (!bool)
      {
        Handler localHandler = f;
        int k = g;
        bool = localHandler.hasMessages(k);
        if (!bool)
        {
          localHandler = f;
          Object localObject2 = f;
          int m = g;
          localObject2 = ((Handler)localObject2).obtainMessage(m);
          long l = 2000L;
          localHandler.sendMessageDelayed((Message)localObject2, l);
        }
      }
      return;
    }
    finally {}
  }
  
  private void d()
  {
    boolean bool = d;
    if (bool)
    {
      bool = b;
      if (!bool)
      {
        bool = c;
        if (bool) {}
      }
    }
    try
    {
      Camera localCamera = e;
      Camera.AutoFocusCallback localAutoFocusCallback = j;
      localCamera.autoFocus(localAutoFocusCallback);
      bool = true;
      c = bool;
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;)
      {
        c();
      }
    }
  }
  
  private void e()
  {
    Handler localHandler = f;
    int k = g;
    localHandler.removeMessages(k);
  }
  
  public void a()
  {
    b = false;
    d();
  }
  
  public void b()
  {
    b = true;
    Camera localCamera = null;
    c = false;
    e();
    boolean bool = d;
    if (bool) {}
    try
    {
      localCamera = e;
      localCamera.cancelAutoFocus();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;) {}
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */