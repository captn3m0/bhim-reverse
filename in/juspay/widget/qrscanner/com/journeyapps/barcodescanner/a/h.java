package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a;

import android.graphics.Rect;
import java.util.List;

public class h
{
  private static final String a = h.class.getSimpleName();
  private in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l b;
  private int c;
  private boolean d = false;
  private l e;
  
  public h(int paramInt, in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l paraml)
  {
    i locali = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;
    locali.<init>();
    e = locali;
    c = paramInt;
    b = paraml;
  }
  
  public int a()
  {
    return c;
  }
  
  public Rect a(in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l paraml)
  {
    l locall = e;
    in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l locall1 = b;
    return locall.b(paraml, locall1);
  }
  
  public in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l a(List paramList, boolean paramBoolean)
  {
    in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l locall = a(paramBoolean);
    return e.a(paramList, locall);
  }
  
  public in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l a(boolean paramBoolean)
  {
    in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.l locall = b;
    if (locall == null) {
      locall = null;
    }
    for (;;)
    {
      return locall;
      if (paramBoolean) {
        locall = b.a();
      } else {
        locall = b;
      }
    }
  }
  
  public void a(l paraml)
  {
    e = paraml;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */