package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.graphics.Rect;
import in.juspay.widget.qrscanner.com.google.zxing.h;

public class m
{
  private byte[] a;
  private int b;
  private int c;
  private int d;
  private int e;
  private Rect f;
  
  public m(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    a = paramArrayOfByte;
    b = paramInt1;
    c = paramInt2;
    e = paramInt4;
    d = paramInt3;
    int i = paramInt1 * paramInt2;
    int j = paramArrayOfByte.length;
    if (i > j)
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = ((StringBuilder)localObject).append("Image data does not match the resolution. ").append(paramInt1).append("x").append(paramInt2).append(" > ");
      int k = paramArrayOfByte.length;
      localObject = k;
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    }
  }
  
  public static byte[] a(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    switch (paramInt1)
    {
    }
    for (;;)
    {
      return paramArrayOfByte;
      paramArrayOfByte = a(paramArrayOfByte, paramInt2, paramInt3);
      continue;
      paramArrayOfByte = b(paramArrayOfByte, paramInt2, paramInt3);
      continue;
      paramArrayOfByte = c(paramArrayOfByte, paramInt2, paramInt3);
    }
  }
  
  public static byte[] a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = 0;
    byte[] arrayOfByte = new byte[paramInt1 * paramInt2];
    int j = 0;
    int k = 0;
    while (j < paramInt1)
    {
      i = paramInt2 + -1;
      while (i >= 0)
      {
        int m = i * paramInt1 + j;
        m = paramArrayOfByte[m];
        arrayOfByte[k] = m;
        k += 1;
        i += -1;
      }
      i = j + 1;
      j = i;
    }
    return arrayOfByte;
  }
  
  public static byte[] b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1 * paramInt2;
    byte[] arrayOfByte = new byte[i];
    int j = i + -1;
    int k = 0;
    while (k < i)
    {
      int m = paramArrayOfByte[k];
      arrayOfByte[j] = m;
      j += -1;
      k += 1;
    }
    return arrayOfByte;
  }
  
  public static byte[] c(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = paramInt1 * paramInt2;
    byte[] arrayOfByte = new byte[i];
    int j = i + -1;
    i = 0;
    for (int k = 0; k < paramInt1; k = i)
    {
      i = paramInt2 + -1;
      while (i >= 0)
      {
        int m = i * paramInt1 + k;
        m = paramArrayOfByte[m];
        arrayOfByte[j] = m;
        j += -1;
        i += -1;
      }
      i = k + 1;
    }
    return arrayOfByte;
  }
  
  public void a(Rect paramRect)
  {
    f = paramRect;
  }
  
  public boolean a()
  {
    int i = e % 180;
    if (i != 0) {}
    int j;
    for (i = 1;; j = 0) {
      return i;
    }
  }
  
  public h b()
  {
    int i = e;
    byte[] arrayOfByte = a;
    int j = b;
    int k = c;
    arrayOfByte = a(i, arrayOfByte, j, k);
    boolean bool = a();
    h localh;
    Rect localRect1;
    int m;
    Rect localRect2;
    int n;
    Rect localRect3;
    int i1;
    Rect localRect4;
    int i2;
    if (bool)
    {
      localh = new in/juspay/widget/qrscanner/com/google/zxing/h;
      j = c;
      k = b;
      localRect1 = f;
      m = left;
      localRect2 = f;
      n = top;
      localRect3 = f;
      i1 = localRect3.width();
      localRect4 = f;
      i2 = localRect4.height();
      localh.<init>(arrayOfByte, j, k, m, n, i1, i2, false);
    }
    for (;;)
    {
      return localh;
      localh = new in/juspay/widget/qrscanner/com/google/zxing/h;
      j = b;
      k = c;
      localRect1 = f;
      m = left;
      localRect2 = f;
      n = top;
      localRect3 = f;
      i1 = localRect3.width();
      localRect4 = f;
      i2 = localRect4.height();
      localh.<init>(arrayOfByte, j, k, m, n, i1, i2, false);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/m.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */