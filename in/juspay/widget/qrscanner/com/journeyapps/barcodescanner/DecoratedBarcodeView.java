package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.FrameLayout;
import android.widget.TextView;
import in.juspay.widget.qrscanner.a.b;
import in.juspay.widget.qrscanner.a.c;
import in.juspay.widget.qrscanner.a.f;

public class DecoratedBarcodeView
  extends FrameLayout
{
  private BarcodeView a;
  private ViewfinderView b;
  private TextView c;
  private DecoratedBarcodeView.a d;
  
  public DecoratedBarcodeView(Context paramContext)
  {
    super(paramContext);
    e();
  }
  
  public DecoratedBarcodeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    a(paramAttributeSet);
  }
  
  public DecoratedBarcodeView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    a(paramAttributeSet);
  }
  
  private void a(AttributeSet paramAttributeSet)
  {
    Object localObject1 = getContext();
    Object localObject2 = a.f.zxing_view;
    localObject1 = ((Context)localObject1).obtainStyledAttributes(paramAttributeSet, (int[])localObject2);
    int i = a.f.zxing_view_zxing_scanner_layout;
    int j = a.c.zxing_barcode_scanner;
    i = ((TypedArray)localObject1).getResourceId(i, j);
    ((TypedArray)localObject1).recycle();
    inflate(getContext(), i, this);
    int k = a.b.zxing_barcode_surface;
    localObject1 = (BarcodeView)findViewById(k);
    a = ((BarcodeView)localObject1);
    localObject1 = a;
    if (localObject1 == null)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("There is no a com.journeyapps.barcodescanner.BarcodeView on provided layout with the id \"zxing_barcode_surface\".");
      throw ((Throwable)localObject1);
    }
    a.a(paramAttributeSet);
    k = a.b.zxing_viewfinder_view;
    localObject1 = (ViewfinderView)findViewById(k);
    b = ((ViewfinderView)localObject1);
    localObject1 = b;
    if (localObject1 == null)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("There is no a com.journeyapps.barcodescanner.ViewfinderView on provided layout with the id \"zxing_viewfinder_view\".");
      throw ((Throwable)localObject1);
    }
    localObject1 = b;
    localObject2 = a;
    ((ViewfinderView)localObject1).setCameraPreview((c)localObject2);
    k = a.b.zxing_status_view;
    localObject1 = (TextView)findViewById(k);
    c = ((TextView)localObject1);
  }
  
  private void e()
  {
    a(null);
  }
  
  public void a()
  {
    a.d();
  }
  
  public void a(a parama)
  {
    BarcodeView localBarcodeView = a;
    DecoratedBarcodeView.b localb = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView$b;
    localb.<init>(this, parama);
    localBarcodeView.a(localb);
  }
  
  public void b()
  {
    a.e();
  }
  
  public void c()
  {
    Object localObject = a;
    boolean bool = true;
    ((BarcodeView)localObject).setTorch(bool);
    localObject = d;
    if (localObject != null)
    {
      localObject = d;
      ((DecoratedBarcodeView.a)localObject).a();
    }
  }
  
  public void d()
  {
    a.setTorch(false);
    DecoratedBarcodeView.a locala = d;
    if (locala != null)
    {
      locala = d;
      locala.b();
    }
  }
  
  public BarcodeView getBarcodeView()
  {
    int i = a.b.zxing_barcode_surface;
    return (BarcodeView)findViewById(i);
  }
  
  public TextView getStatusView()
  {
    return c;
  }
  
  public ViewfinderView getViewFinder()
  {
    return b;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool = true;
    switch (paramInt)
    {
    default: 
      bool = super.onKeyDown(paramInt, paramKeyEvent);
    }
    for (;;)
    {
      return bool;
      d();
      continue;
      c();
    }
  }
  
  public void setStatusText(String paramString)
  {
    TextView localTextView = c;
    if (localTextView != null)
    {
      localTextView = c;
      localTextView.setText(paramString);
    }
  }
  
  public void setTorchListener(DecoratedBarcodeView.a parama)
  {
    d = parama;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/DecoratedBarcodeView.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */