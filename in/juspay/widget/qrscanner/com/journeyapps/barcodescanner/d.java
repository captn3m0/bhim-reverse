package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.os.Handler;
import in.juspay.widget.qrscanner.a.e;
import in.juspay.widget.qrscanner.com.google.zxing.a.a.b;
import in.juspay.widget.qrscanner.com.google.zxing.a.a.c;

public class d
{
  private static final String a = d.class.getSimpleName();
  private static int b = 250;
  private Activity c;
  private DecoratedBarcodeView d;
  private int e = -1;
  private boolean f = false;
  private boolean g = false;
  private c h;
  private b i;
  private Handler j;
  private a k = null;
  private final c.a l;
  private boolean m;
  
  public d(Activity paramActivity, DecoratedBarcodeView paramDecoratedBarcodeView)
  {
    Object localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$1;
    ((d.1)localObject1).<init>(this);
    l = ((c.a)localObject1);
    m = false;
    c = paramActivity;
    d = paramDecoratedBarcodeView;
    localObject1 = paramDecoratedBarcodeView.getBarcodeView();
    Object localObject2 = l;
    ((BarcodeView)localObject1).a((c.a)localObject2);
    localObject1 = new android/os/Handler;
    ((Handler)localObject1).<init>();
    j = ((Handler)localObject1);
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/a/a/c;
    localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$2;
    ((d.2)localObject2).<init>(this);
    ((c)localObject1).<init>(paramActivity, (Runnable)localObject2);
    h = ((c)localObject1);
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/a/a/b;
    ((b)localObject1).<init>(paramActivity);
    i = ((b)localObject1);
  }
  
  private void f()
  {
    c.finish();
  }
  
  public void a()
  {
    DecoratedBarcodeView localDecoratedBarcodeView = d;
    a locala = k;
    localDecoratedBarcodeView.a(locala);
  }
  
  public void a(a parama)
  {
    k = parama;
  }
  
  public void b()
  {
    d.b();
    h.b();
  }
  
  public void c()
  {
    d.a();
    h.c();
  }
  
  public void d()
  {
    g = true;
    h.c();
  }
  
  protected void e()
  {
    Object localObject1 = c;
    boolean bool = ((Activity)localObject1).isFinishing();
    if (!bool)
    {
      bool = g;
      if (!bool) {
        break label24;
      }
    }
    for (;;)
    {
      return;
      label24:
      localObject1 = new android/app/AlertDialog$Builder;
      Object localObject2 = c;
      ((AlertDialog.Builder)localObject1).<init>((Context)localObject2);
      localObject2 = c;
      int n = a.e.zxing_app_name;
      localObject2 = ((Activity)localObject2).getString(n);
      ((AlertDialog.Builder)localObject1).setTitle((CharSequence)localObject2);
      localObject2 = c;
      n = a.e.zxing_msg_camera_framework_bug;
      localObject2 = ((Activity)localObject2).getString(n);
      ((AlertDialog.Builder)localObject1).setMessage((CharSequence)localObject2);
      int i1 = a.e.zxing_button_ok;
      d.3 local3 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$3;
      local3.<init>(this);
      ((AlertDialog.Builder)localObject1).setPositiveButton(i1, local3);
      localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d$4;
      ((d.4)localObject2).<init>(this);
      ((AlertDialog.Builder)localObject1).setOnCancelListener((DialogInterface.OnCancelListener)localObject2);
      ((AlertDialog.Builder)localObject1).show();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */