package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.WindowManager;

public class k
{
  private int a;
  private WindowManager b;
  private OrientationEventListener c;
  private j d;
  
  public void a()
  {
    OrientationEventListener localOrientationEventListener = c;
    if (localOrientationEventListener != null)
    {
      localOrientationEventListener = c;
      localOrientationEventListener.disable();
    }
    c = null;
    b = null;
    d = null;
  }
  
  public void a(Context paramContext, j paramj)
  {
    a();
    Context localContext = paramContext.getApplicationContext();
    d = paramj;
    Object localObject = (WindowManager)localContext.getSystemService("window");
    b = ((WindowManager)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k$1;
    ((k.1)localObject).<init>(this, localContext, 3);
    c = ((OrientationEventListener)localObject);
    c.enable();
    int i = b.getDefaultDisplay().getRotation();
    a = i;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */