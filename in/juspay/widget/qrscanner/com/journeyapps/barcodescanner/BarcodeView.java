package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Handler.Callback;
import android.util.AttributeSet;
import in.juspay.widget.qrscanner.com.google.zxing.d;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b;
import java.util.HashMap;
import java.util.Map;

public class BarcodeView
  extends c
{
  private BarcodeView.a a;
  private a b;
  private h c;
  private f d;
  private Handler e;
  private final Handler.Callback f;
  
  public BarcodeView(Context paramContext)
  {
    super(paramContext);
    Object localObject = BarcodeView.a.a;
    a = ((BarcodeView.a)localObject);
    b = null;
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;
    ((BarcodeView.1)localObject).<init>(this);
    f = ((Handler.Callback)localObject);
    a(paramContext, null);
  }
  
  public BarcodeView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject = BarcodeView.a.a;
    a = ((BarcodeView.a)localObject);
    b = null;
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;
    ((BarcodeView.1)localObject).<init>(this);
    f = ((Handler.Callback)localObject);
    a(paramContext, paramAttributeSet);
  }
  
  public BarcodeView(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    Object localObject = BarcodeView.a.a;
    a = ((BarcodeView.a)localObject);
    b = null;
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView$1;
    ((BarcodeView.1)localObject).<init>(this);
    f = ((Handler.Callback)localObject);
    a(paramContext, paramAttributeSet);
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet)
  {
    Object localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;
    ((i)localObject).<init>();
    d = ((f)localObject);
    localObject = new android/os/Handler;
    Handler.Callback localCallback = f;
    ((Handler)localObject).<init>(localCallback);
    e = ((Handler)localObject);
  }
  
  private e i()
  {
    Object localObject1 = d;
    if (localObject1 == null)
    {
      localObject1 = b();
      d = ((f)localObject1);
    }
    localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/g;
    ((g)localObject1).<init>();
    Object localObject2 = new java/util/HashMap;
    ((HashMap)localObject2).<init>();
    d locald = d.j;
    ((Map)localObject2).put(locald, localObject1);
    localObject2 = d.a((Map)localObject2);
    ((g)localObject1).a((e)localObject2);
    return (e)localObject2;
  }
  
  private void j()
  {
    k();
    Object localObject1 = a;
    Object localObject2 = BarcodeView.a.a;
    if (localObject1 != localObject2)
    {
      boolean bool = h();
      if (bool)
      {
        localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h;
        localObject2 = getCameraInstance();
        e locale = i();
        Handler localHandler = e;
        ((h)localObject1).<init>((b)localObject2, locale, localHandler);
        c = ((h)localObject1);
        localObject1 = c;
        localObject2 = getPreviewFramingRect();
        ((h)localObject1).a((Rect)localObject2);
        localObject1 = c;
        ((h)localObject1).a();
      }
    }
  }
  
  private void k()
  {
    h localh = c;
    if (localh != null)
    {
      c.b();
      localh = null;
      c = null;
    }
  }
  
  public void a()
  {
    BarcodeView.a locala = BarcodeView.a.a;
    a = locala;
    b = null;
    k();
  }
  
  public void a(a parama)
  {
    BarcodeView.a locala = BarcodeView.a.b;
    a = locala;
    b = parama;
    j();
  }
  
  protected f b()
  {
    i locali = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/i;
    locali.<init>();
    return locali;
  }
  
  protected void c()
  {
    super.c();
    j();
  }
  
  public void d()
  {
    k();
    super.d();
  }
  
  public f getDecoderFactory()
  {
    return d;
  }
  
  public void setDecoderFactory(f paramf)
  {
    n.a();
    d = paramf;
    h localh = c;
    if (localh != null)
    {
      localh = c;
      e locale = i();
      localh.a(locale);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/BarcodeView.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */