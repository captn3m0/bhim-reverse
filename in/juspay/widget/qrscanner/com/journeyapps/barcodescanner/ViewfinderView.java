package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import in.juspay.widget.qrscanner.a.a;
import in.juspay.widget.qrscanner.a.f;
import in.juspay.widget.qrscanner.com.google.zxing.m;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ViewfinderView
  extends View
{
  protected static final String a = ViewfinderView.class.getSimpleName();
  protected static final int[] b;
  protected final Paint c;
  protected Bitmap d;
  protected final int e;
  protected final int f;
  protected final int g;
  protected final int h;
  protected int i;
  protected List j;
  protected List k;
  protected c l;
  protected Rect m;
  protected Rect n;
  
  static
  {
    int[] arrayOfInt = new int[8];
    arrayOfInt[0] = 0;
    arrayOfInt[1] = 64;
    arrayOfInt[2] = '';
    arrayOfInt[3] = 'À';
    arrayOfInt[4] = 'ÿ';
    arrayOfInt[5] = 'À';
    arrayOfInt[6] = '';
    arrayOfInt[7] = 64;
    b = arrayOfInt;
  }
  
  public ViewfinderView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject1 = new android/graphics/Paint;
    ((Paint)localObject1).<init>(1);
    c = ((Paint)localObject1);
    localObject1 = getResources();
    Object localObject2 = getContext();
    int[] arrayOfInt = a.f.zxing_finder;
    localObject2 = ((Context)localObject2).obtainStyledAttributes(paramAttributeSet, arrayOfInt);
    int i1 = a.f.zxing_finder_zxing_viewfinder_mask;
    int i2 = a.a.zxing_viewfinder_mask;
    i2 = ((Resources)localObject1).getColor(i2);
    i1 = ((TypedArray)localObject2).getColor(i1, i2);
    e = i1;
    i1 = a.f.zxing_finder_zxing_result_view;
    i2 = a.a.zxing_result_view;
    i2 = ((Resources)localObject1).getColor(i2);
    i1 = ((TypedArray)localObject2).getColor(i1, i2);
    f = i1;
    i1 = a.f.zxing_finder_zxing_viewfinder_laser;
    i2 = a.a.zxing_viewfinder_laser;
    i2 = ((Resources)localObject1).getColor(i2);
    i1 = ((TypedArray)localObject2).getColor(i1, i2);
    g = i1;
    i1 = a.f.zxing_finder_zxing_possible_result_points;
    i2 = a.a.zxing_possible_result_points;
    int i3 = ((Resources)localObject1).getColor(i2);
    i3 = ((TypedArray)localObject2).getColor(i1, i3);
    h = i3;
    ((TypedArray)localObject2).recycle();
    i = 0;
    localObject1 = new java/util/ArrayList;
    ((ArrayList)localObject1).<init>(5);
    j = ((List)localObject1);
    k = null;
  }
  
  protected void a()
  {
    Object localObject = l;
    if (localObject == null) {}
    for (;;)
    {
      return;
      localObject = l.getFramingRect();
      Rect localRect = l.getPreviewFramingRect();
      if ((localObject != null) && (localRect != null))
      {
        m = ((Rect)localObject);
        n = localRect;
      }
    }
  }
  
  public void a(m paramm)
  {
    List localList = j;
    localList.add(paramm);
    int i1 = localList.size();
    int i2 = 20;
    if (i1 > i2)
    {
      i2 = 0;
      i1 += -10;
      localList = localList.subList(0, i1);
      localList.clear();
    }
  }
  
  public void onDraw(Canvas paramCanvas)
  {
    int i1 = 160;
    int i2 = 0;
    float f1 = 0.0F;
    Object localObject1 = null;
    a();
    Object localObject2 = m;
    if (localObject2 != null)
    {
      localObject2 = n;
      if (localObject2 != null) {
        break label39;
      }
    }
    for (;;)
    {
      return;
      label39:
      Rect localRect = m;
      Object localObject3 = n;
      int i3 = paramCanvas.getWidth();
      int i4 = paramCanvas.getHeight();
      Object localObject4 = c;
      localObject2 = d;
      if (localObject2 != null) {}
      float f6;
      for (int i5 = f;; i5 = e)
      {
        ((Paint)localObject4).setColor(i5);
        f2 = i3;
        f3 = top;
        localPaint1 = c;
        localObject2 = paramCanvas;
        i8 = 0;
        paramCanvas.drawRect(0.0F, 0.0F, f2, f3, localPaint1);
        f4 = top;
        f2 = left;
        f3 = bottom + 1;
        localPaint1 = c;
        paramCanvas.drawRect(0.0F, f4, f2, f3, localPaint1);
        f2 = right + 1;
        f3 = top;
        float f5 = i3;
        f6 = bottom + 1;
        Paint localPaint2 = c;
        localObject4 = paramCanvas;
        paramCanvas.drawRect(f2, f3, f5, f6, localPaint2);
        i5 = bottom + 1;
        f4 = i5;
        f2 = i3;
        f3 = i4;
        localPaint1 = c;
        paramCanvas.drawRect(0.0F, f4, f2, f3, localPaint1);
        localObject2 = d;
        if (localObject2 == null) {
          break label328;
        }
        c.setAlpha(i1);
        localObject2 = d;
        localObject1 = c;
        paramCanvas.drawBitmap((Bitmap)localObject2, null, localRect, (Paint)localObject1);
        break;
      }
      label328:
      localObject2 = c;
      i2 = g;
      ((Paint)localObject2).setColor(i2);
      localObject2 = c;
      localObject1 = b;
      int i8 = i;
      i2 = localObject1[i8];
      ((Paint)localObject2).setAlpha(i2);
      i5 = i + 1;
      localObject1 = b;
      i2 = localObject1.length;
      i5 %= i2;
      i = i5;
      i5 = localRect.height() / 2;
      i2 = top;
      i5 += i2;
      f1 = left + 2;
      float f4 = i5 + -1;
      int i9 = right + -1;
      float f2 = i9;
      float f3 = i5 + 2;
      Paint localPaint1 = c;
      localObject2 = paramCanvas;
      paramCanvas.drawRect(f1, f4, f2, f3, localPaint1);
      float f7 = localRect.width();
      i2 = ((Rect)localObject3).width();
      f1 = i2;
      f1 = f7 / f1;
      i5 = localRect.height();
      f7 = i5;
      i8 = ((Rect)localObject3).height();
      f4 = i8;
      f4 = f7 / f4;
      localObject2 = j;
      Object localObject5 = k;
      int i10 = left;
      int i11 = top;
      boolean bool3 = ((List)localObject2).isEmpty();
      float f8;
      if (bool3)
      {
        k = null;
        if (localObject5 != null)
        {
          c.setAlpha(80);
          localObject2 = c;
          i12 = h;
          ((Paint)localObject2).setColor(i12);
          i12 = 1077936128;
          f6 = 3.0F;
          localObject5 = ((List)localObject5).iterator();
          for (;;)
          {
            boolean bool1 = ((Iterator)localObject5).hasNext();
            if (!bool1) {
              break;
            }
            localObject2 = (m)((Iterator)localObject5).next();
            i13 = (int)(((m)localObject2).a() * f1) + i10;
            f8 = i13;
            int i6 = (int)(((m)localObject2).b() * f4) + i11;
            f7 = i6;
            localObject3 = c;
            paramCanvas.drawCircle(f8, f7, f6, (Paint)localObject3);
          }
        }
      }
      else
      {
        Object localObject6 = new java/util/ArrayList;
        f8 = 7.0E-45F;
        ((ArrayList)localObject6).<init>(5);
        j = ((List)localObject6);
        k = ((List)localObject2);
        c.setAlpha(i1);
        localObject6 = c;
        i13 = h;
        ((Paint)localObject6).setColor(i13);
        localObject6 = ((List)localObject2).iterator();
        for (;;)
        {
          boolean bool2 = ((Iterator)localObject6).hasNext();
          if (!bool2) {
            break;
          }
          localObject2 = (m)((Iterator)localObject6).next();
          i13 = (int)(((m)localObject2).a() * f1) + i10;
          f8 = i13;
          i7 = (int)(((m)localObject2).b() * f4) + i11;
          f7 = i7;
          float f9 = 6.0F;
          Paint localPaint3 = c;
          paramCanvas.drawCircle(f8, f7, f9, localPaint3);
        }
      }
      long l1 = 80;
      i10 = left + -6;
      i11 = top + -6;
      int i12 = right + 6;
      int i7 = bottom;
      int i13 = i7 + 6;
      localObject1 = this;
      postInvalidateDelayed(l1, i10, i11, i12, i13);
    }
  }
  
  public void setCameraPreview(c paramc)
  {
    l = paramc;
    ViewfinderView.1 local1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView$1;
    local1.<init>(this);
    paramc.a(local1);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/ViewfinderView.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */