package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.TextureView.SurfaceTextureListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import in.juspay.widget.qrscanner.a.f;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.b;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.d;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.e;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.g;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.h;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.i;
import java.util.ArrayList;
import java.util.List;

public class c
  extends ViewGroup
{
  private static final String a = c.class.getSimpleName();
  private final c.a A;
  private b b;
  private WindowManager c;
  private Handler d;
  private boolean e = false;
  private SurfaceView f;
  private TextureView g;
  private boolean h = false;
  private k i;
  private int j = -1;
  private List k;
  private h l;
  private d m;
  private l n;
  private l o;
  private Rect p;
  private l q;
  private Rect r;
  private Rect s;
  private l t;
  private double u;
  private in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l v;
  private boolean w;
  private final SurfaceHolder.Callback x;
  private final Handler.Callback y;
  private j z;
  
  public c(Context paramContext)
  {
    super(paramContext);
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    k = ((List)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
    ((d)localObject).<init>();
    m = ((d)localObject);
    r = null;
    s = null;
    t = null;
    u = 0.1D;
    v = null;
    w = false;
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;
    ((c.2)localObject).<init>(this);
    x = ((SurfaceHolder.Callback)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;
    ((c.3)localObject).<init>(this);
    y = ((Handler.Callback)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;
    ((c.4)localObject).<init>(this);
    z = ((j)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;
    ((c.5)localObject).<init>(this);
    A = ((c.a)localObject);
    a(paramContext, null, 0, 0);
  }
  
  public c(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    k = ((List)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
    ((d)localObject).<init>();
    m = ((d)localObject);
    r = null;
    s = null;
    t = null;
    u = 0.1D;
    v = null;
    w = false;
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;
    ((c.2)localObject).<init>(this);
    x = ((SurfaceHolder.Callback)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;
    ((c.3)localObject).<init>(this);
    y = ((Handler.Callback)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;
    ((c.4)localObject).<init>(this);
    z = ((j)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;
    ((c.5)localObject).<init>(this);
    A = ((c.a)localObject);
    a(paramContext, paramAttributeSet, 0, 0);
  }
  
  public c(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    k = ((List)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/d;
    ((d)localObject).<init>();
    m = ((d)localObject);
    r = null;
    s = null;
    t = null;
    u = 0.1D;
    v = null;
    w = false;
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$2;
    ((c.2)localObject).<init>(this);
    x = ((SurfaceHolder.Callback)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$3;
    ((c.3)localObject).<init>(this);
    y = ((Handler.Callback)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$4;
    ((c.4)localObject).<init>(this);
    z = ((j)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$5;
    ((c.5)localObject).<init>(this);
    A = ((c.a)localObject);
    a(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  private TextureView.SurfaceTextureListener a()
  {
    c.1 local1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c$1;
    local1.<init>(this);
    return local1;
  }
  
  private void a(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2)
  {
    Object localObject = getBackground();
    if (localObject == null)
    {
      int i1 = -16777216;
      setBackgroundColor(i1);
    }
    a(paramAttributeSet);
    localObject = (WindowManager)paramContext.getSystemService("window");
    c = ((WindowManager)localObject);
    localObject = new android/os/Handler;
    Handler.Callback localCallback = y;
    ((Handler)localObject).<init>(localCallback);
    d = ((Handler)localObject);
    localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k;
    ((k)localObject).<init>();
    i = ((k)localObject);
  }
  
  private void a(e parame)
  {
    boolean bool = h;
    if (!bool)
    {
      Object localObject = b;
      if (localObject != null)
      {
        b.a(parame);
        b.d();
        bool = true;
        h = bool;
        c();
        localObject = A;
        ((c.a)localObject).b();
      }
    }
  }
  
  private void a(l paraml)
  {
    n = paraml;
    Object localObject1 = b;
    if (localObject1 != null)
    {
      localObject1 = b.a();
      if (localObject1 == null)
      {
        localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/h;
        int i1 = getDisplayRotation();
        ((h)localObject1).<init>(i1, paraml);
        l = ((h)localObject1);
        localObject1 = l;
        Object localObject2 = getPreviewScalingStrategy();
        ((h)localObject1).a((in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l)localObject2);
        localObject1 = b;
        localObject2 = l;
        ((b)localObject1).a((h)localObject2);
        localObject1 = b;
        ((b)localObject1).c();
        boolean bool2 = w;
        if (bool2)
        {
          localObject1 = b;
          boolean bool1 = w;
          ((b)localObject1).a(bool1);
        }
      }
    }
  }
  
  private void b()
  {
    boolean bool = f();
    if (bool)
    {
      int i1 = getDisplayRotation();
      int i2 = j;
      if (i1 != i2)
      {
        d();
        e();
      }
    }
  }
  
  private void b(l paraml)
  {
    o = paraml;
    l locall = n;
    if (locall != null)
    {
      j();
      requestLayout();
      k();
    }
  }
  
  private int getDisplayRotation()
  {
    return c.getDefaultDisplay().getRotation();
  }
  
  private void i()
  {
    boolean bool = e;
    int i1;
    int i2;
    Object localObject1;
    Object localObject2;
    if (bool)
    {
      i1 = Build.VERSION.SDK_INT;
      i2 = 14;
      if (i1 >= i2)
      {
        localObject1 = new android/view/TextureView;
        localObject2 = getContext();
        ((TextureView)localObject1).<init>((Context)localObject2);
        g = ((TextureView)localObject1);
        localObject1 = g;
        localObject2 = a();
        ((TextureView)localObject1).setSurfaceTextureListener((TextureView.SurfaceTextureListener)localObject2);
        localObject1 = g;
        addView((View)localObject1);
      }
    }
    for (;;)
    {
      return;
      localObject1 = new android/view/SurfaceView;
      localObject2 = getContext();
      ((SurfaceView)localObject1).<init>((Context)localObject2);
      f = ((SurfaceView)localObject1);
      i1 = Build.VERSION.SDK_INT;
      i2 = 11;
      if (i1 < i2)
      {
        localObject1 = f.getHolder();
        i2 = 3;
        ((SurfaceHolder)localObject1).setType(i2);
      }
      localObject1 = f.getHolder();
      localObject2 = x;
      ((SurfaceHolder)localObject1).addCallback((SurfaceHolder.Callback)localObject2);
      localObject1 = f;
      addView((View)localObject1);
    }
  }
  
  private void j()
  {
    int i1 = 0;
    Rect localRect1 = null;
    Object localObject1 = n;
    if (localObject1 != null)
    {
      localObject1 = o;
      if (localObject1 != null)
      {
        localObject1 = l;
        if (localObject1 != null) {
          break label59;
        }
      }
    }
    s = null;
    r = null;
    p = null;
    localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("containerSize or previewSize is not set yet");
    throw ((Throwable)localObject1);
    label59:
    int i2 = o.a;
    l locall = o;
    int i3 = b;
    int i4 = n.a;
    int i5 = n.b;
    Object localObject2 = l;
    Object localObject3 = o;
    localObject2 = ((h)localObject2).a((l)localObject3);
    p = ((Rect)localObject2);
    localObject2 = new android/graphics/Rect;
    ((Rect)localObject2).<init>(0, 0, i4, i5);
    Rect localRect2 = p;
    localRect2 = a((Rect)localObject2, localRect2);
    r = localRect2;
    localRect2 = new android/graphics/Rect;
    Rect localRect3 = r;
    localRect2.<init>(localRect3);
    i5 = -p.left;
    localObject2 = p;
    int i6 = -top;
    localRect2.offset(i5, i6);
    localRect3 = new android/graphics/Rect;
    i6 = left * i2;
    localObject3 = p;
    int i7 = ((Rect)localObject3).width();
    i6 /= i7;
    i7 = top * i3;
    i1 = p.height();
    i7 /= i1;
    i1 = right;
    i2 *= i1;
    localRect1 = p;
    i1 = localRect1.width();
    i2 /= i1;
    i4 = bottom;
    i3 *= i4;
    localRect2 = p;
    i4 = localRect2.height();
    i3 /= i4;
    localRect3.<init>(i6, i7, i2, i3);
    s = localRect3;
    localObject1 = s;
    i2 = ((Rect)localObject1).width();
    if (i2 > 0)
    {
      localObject1 = s;
      i2 = ((Rect)localObject1).height();
      if (i2 > 0) {}
    }
    else
    {
      s = null;
      r = null;
    }
    for (;;)
    {
      return;
      localObject1 = A;
      ((c.a)localObject1).a();
    }
  }
  
  private void k()
  {
    Object localObject1 = q;
    Object localObject2;
    Object localObject3;
    int i1;
    if (localObject1 != null)
    {
      localObject1 = o;
      if (localObject1 != null)
      {
        localObject1 = p;
        if (localObject1 != null)
        {
          localObject1 = f;
          if (localObject1 == null) {
            break label112;
          }
          localObject1 = q;
          localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
          localObject3 = p;
          i1 = ((Rect)localObject3).width();
          Rect localRect = p;
          int i2 = localRect.height();
          ((l)localObject2).<init>(i1, i2);
          boolean bool = ((l)localObject1).equals(localObject2);
          if (!bool) {
            break label112;
          }
          localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;
          localObject2 = f.getHolder();
          ((e)localObject1).<init>((SurfaceHolder)localObject2);
          a((e)localObject1);
        }
      }
    }
    for (;;)
    {
      return;
      label112:
      localObject1 = g;
      if (localObject1 != null)
      {
        int i3 = Build.VERSION.SDK_INT;
        int i4 = 14;
        if (i3 >= i4)
        {
          localObject1 = g.getSurfaceTexture();
          if (localObject1 != null)
          {
            localObject1 = o;
            if (localObject1 != null)
            {
              localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
              i4 = g.getWidth();
              localObject3 = g;
              i1 = ((TextureView)localObject3).getHeight();
              ((l)localObject1).<init>(i4, i1);
              localObject2 = o;
              localObject1 = a((l)localObject1, (l)localObject2);
              localObject2 = g;
              ((TextureView)localObject2).setTransform((Matrix)localObject1);
            }
            localObject1 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/e;
            localObject2 = g.getSurfaceTexture();
            ((e)localObject1).<init>((SurfaceTexture)localObject2);
            a((e)localObject1);
          }
        }
      }
    }
  }
  
  private void l()
  {
    b localb = b;
    if (localb != null) {}
    for (;;)
    {
      return;
      localb = g();
      b = localb;
      localb = b;
      Handler localHandler = d;
      localb.a(localHandler);
      localb = b;
      localb.b();
      int i1 = getDisplayRotation();
      j = i1;
    }
  }
  
  protected Matrix a(l paraml1, l paraml2)
  {
    float f1 = 2.0F;
    int i1 = 1065353216;
    float f2 = 1.0F;
    int i2 = a;
    float f3 = i2;
    float f4 = b;
    f3 /= f4;
    int i3 = a;
    f4 = i3;
    float f5 = b;
    f4 /= f5;
    boolean bool = f3 < f4;
    if (bool) {
      f3 = f4 / f3;
    }
    for (;;)
    {
      Matrix localMatrix = new android/graphics/Matrix;
      localMatrix.<init>();
      localMatrix.setScale(f3, f2);
      f5 = a;
      f3 *= f5;
      f5 = b;
      f2 *= f5;
      f3 = (a - f3) / f1;
      f2 = (b - f2) / f1;
      localMatrix.postTranslate(f3, f2);
      return localMatrix;
      float f6 = f3 / f4;
      i2 = i1;
      f3 = f2;
      f2 = f6;
    }
  }
  
  protected Rect a(Rect paramRect1, Rect paramRect2)
  {
    Rect localRect = new android/graphics/Rect;
    localRect.<init>(paramRect1);
    localRect.intersect(paramRect2);
    l locall1 = t;
    int i1;
    int i2;
    if (locall1 != null)
    {
      i1 = localRect.width();
      l locall2 = t;
      i2 = a;
      i1 = (i1 - i2) / 2;
      i1 = Math.max(0, i1);
      i2 = localRect.height();
      l locall3 = t;
      int i3 = b;
      i2 = (i2 - i3) / 2;
      i2 = Math.max(0, i2);
      localRect.inset(i1, i2);
    }
    for (;;)
    {
      return localRect;
      double d1 = localRect.width();
      double d2 = u;
      d1 *= d2;
      d2 = localRect.height();
      double d3 = u;
      d2 *= d3;
      d1 = Math.min(d1, d2);
      i1 = (int)d1;
      localRect.inset(i1, i1);
      i1 = localRect.height();
      i2 = localRect.width();
      if (i1 > i2)
      {
        i1 = localRect.height();
        i2 = localRect.width();
        i1 = (i1 - i2) / 2;
        localRect.inset(0, i1);
      }
    }
  }
  
  protected void a(AttributeSet paramAttributeSet)
  {
    int i1 = 1;
    float f1 = -1.0F;
    Object localObject1 = getContext();
    Object localObject2 = a.f.zxing_camera_preview;
    localObject1 = ((Context)localObject1).obtainStyledAttributes(paramAttributeSet, (int[])localObject2);
    int i2 = a.f.zxing_camera_preview_zxing_framing_rect_width;
    float f2 = ((TypedArray)localObject1).getDimension(i2, f1);
    i2 = (int)f2;
    int i4 = a.f.zxing_camera_preview_zxing_framing_rect_height;
    float f3 = ((TypedArray)localObject1).getDimension(i4, f1);
    i4 = (int)f3;
    if ((i2 > 0) && (i4 > 0))
    {
      l locall = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
      locall.<init>(i2, i4);
      t = locall;
    }
    i2 = a.f.zxing_camera_preview_zxing_use_texture_view;
    boolean bool = ((TypedArray)localObject1).getBoolean(i2, i1);
    e = bool;
    int i3 = a.f.zxing_camera_preview_zxing_preview_scaling_strategy;
    i4 = -1;
    f3 = 0.0F / 0.0F;
    i3 = ((TypedArray)localObject1).getInteger(i3, i4);
    if (i3 == i1)
    {
      localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;
      ((g)localObject2).<init>();
      v = ((in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l)localObject2);
    }
    for (;;)
    {
      ((TypedArray)localObject1).recycle();
      return;
      i4 = 2;
      f3 = 2.8E-45F;
      if (i3 == i4)
      {
        localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;
        ((i)localObject2).<init>();
        v = ((in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l)localObject2);
      }
      else
      {
        int i5 = 3;
        f3 = 4.2E-45F;
        if (i3 == i5)
        {
          localObject2 = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/j;
          ((in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.j)localObject2).<init>();
          v = ((in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l)localObject2);
        }
      }
    }
  }
  
  public void a(c.a parama)
  {
    k.add(parama);
  }
  
  protected void c() {}
  
  public void d()
  {
    n.a();
    int i1 = -1;
    j = i1;
    Object localObject = b;
    if (localObject != null)
    {
      b.e();
      b = null;
      i1 = 0;
      localObject = null;
      h = false;
    }
    localObject = q;
    if (localObject == null)
    {
      localObject = f;
      if (localObject != null)
      {
        localObject = f.getHolder();
        SurfaceHolder.Callback localCallback = x;
        ((SurfaceHolder)localObject).removeCallback(localCallback);
      }
    }
    localObject = q;
    if (localObject == null)
    {
      localObject = g;
      if (localObject != null)
      {
        i1 = Build.VERSION.SDK_INT;
        int i2 = 14;
        if (i1 >= i2)
        {
          localObject = g;
          ((TextureView)localObject).setSurfaceTextureListener(null);
        }
      }
    }
    n = null;
    o = null;
    s = null;
    i.a();
    A.c();
  }
  
  public void e()
  {
    n.a();
    l();
    Object localObject1 = q;
    if (localObject1 != null) {
      k();
    }
    for (;;)
    {
      requestLayout();
      localObject1 = i;
      Object localObject2 = getContext();
      Object localObject3 = z;
      ((k)localObject1).a((Context)localObject2, (j)localObject3);
      return;
      localObject1 = f;
      if (localObject1 != null)
      {
        localObject1 = f.getHolder();
        localObject2 = x;
        ((SurfaceHolder)localObject1).addCallback((SurfaceHolder.Callback)localObject2);
      }
      else
      {
        localObject1 = g;
        if (localObject1 != null)
        {
          int i1 = Build.VERSION.SDK_INT;
          int i2 = 14;
          if (i1 >= i2)
          {
            localObject1 = g;
            boolean bool = ((TextureView)localObject1).isAvailable();
            if (bool)
            {
              localObject1 = a();
              localObject2 = g.getSurfaceTexture();
              localObject3 = g;
              int i3 = ((TextureView)localObject3).getWidth();
              TextureView localTextureView = g;
              int i4 = localTextureView.getHeight();
              ((TextureView.SurfaceTextureListener)localObject1).onSurfaceTextureAvailable((SurfaceTexture)localObject2, i3, i4);
            }
            else
            {
              localObject1 = g;
              localObject2 = a();
              ((TextureView)localObject1).setSurfaceTextureListener((TextureView.SurfaceTextureListener)localObject2);
            }
          }
        }
      }
    }
  }
  
  protected boolean f()
  {
    b localb = b;
    boolean bool;
    if (localb != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localb = null;
    }
  }
  
  protected b g()
  {
    b localb = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/b;
    Object localObject = getContext();
    localb.<init>((Context)localObject);
    localObject = m;
    localb.a((d)localObject);
    return localb;
  }
  
  public b getCameraInstance()
  {
    return b;
  }
  
  public d getCameraSettings()
  {
    return m;
  }
  
  public Rect getFramingRect()
  {
    return r;
  }
  
  public l getFramingRectSize()
  {
    return t;
  }
  
  public double getMarginFraction()
  {
    return u;
  }
  
  public Rect getPreviewFramingRect()
  {
    return s;
  }
  
  public in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l getPreviewScalingStrategy()
  {
    Object localObject = v;
    if (localObject != null) {
      localObject = v;
    }
    for (;;)
    {
      return (in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l)localObject;
      localObject = g;
      if (localObject != null)
      {
        localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/g;
        ((g)localObject).<init>();
      }
      else
      {
        localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/a/i;
        ((i)localObject).<init>();
      }
    }
  }
  
  public boolean h()
  {
    return h;
  }
  
  protected void onAttachedToWindow()
  {
    super.onAttachedToWindow();
    i();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i1 = 0;
    Rect localRect1 = null;
    Object localObject = new in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/l;
    int i2 = paramInt3 - paramInt1;
    int i3 = paramInt4 - paramInt2;
    ((l)localObject).<init>(i2, i3);
    a((l)localObject);
    localObject = f;
    if (localObject != null)
    {
      localObject = p;
      if (localObject == null)
      {
        localObject = f;
        i2 = getWidth();
        i3 = getHeight();
        ((SurfaceView)localObject).layout(0, 0, i2, i3);
      }
    }
    for (;;)
    {
      return;
      localObject = f;
      Rect localRect2 = p;
      i2 = left;
      Rect localRect3 = p;
      i3 = top;
      localRect1 = p;
      i1 = right;
      Rect localRect4 = p;
      int i4 = bottom;
      ((SurfaceView)localObject).layout(i2, i3, i1, i4);
      continue;
      localObject = g;
      if (localObject != null)
      {
        int i5 = Build.VERSION.SDK_INT;
        i2 = 14;
        if (i5 >= i2)
        {
          localObject = g;
          i2 = getWidth();
          i3 = getHeight();
          ((TextureView)localObject).layout(0, 0, i2, i3);
        }
      }
    }
  }
  
  protected void onRestoreInstanceState(Parcelable paramParcelable)
  {
    boolean bool = paramParcelable instanceof Bundle;
    if (!bool) {
      super.onRestoreInstanceState(paramParcelable);
    }
    for (;;)
    {
      return;
      paramParcelable = (Bundle)paramParcelable;
      Object localObject = paramParcelable.getParcelable("super");
      super.onRestoreInstanceState((Parcelable)localObject);
      localObject = "torch";
      bool = paramParcelable.getBoolean((String)localObject);
      setTorch(bool);
    }
  }
  
  protected Parcelable onSaveInstanceState()
  {
    Parcelable localParcelable = super.onSaveInstanceState();
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    localBundle.putParcelable("super", localParcelable);
    boolean bool = w;
    localBundle.putBoolean("torch", bool);
    return localBundle;
  }
  
  public void setCameraSettings(d paramd)
  {
    m = paramd;
  }
  
  public void setFramingRectSize(l paraml)
  {
    t = paraml;
  }
  
  public void setMarginFraction(double paramDouble)
  {
    double d1 = 0.5D;
    boolean bool = paramDouble < d1;
    if (!bool)
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("The margin fraction must be less than 0.5");
      throw localIllegalArgumentException;
    }
    u = paramDouble;
  }
  
  public void setPreviewScalingStrategy(in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.l paraml)
  {
    v = paraml;
  }
  
  public void setTorch(boolean paramBoolean)
  {
    w = paramBoolean;
    b localb = b;
    if (localb != null)
    {
      localb = b;
      localb.a(paramBoolean);
    }
  }
  
  public void setUseTextureView(boolean paramBoolean)
  {
    e = paramBoolean;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */