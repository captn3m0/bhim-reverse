package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.content.Context;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.WindowManager;

class k$1
  extends OrientationEventListener
{
  k$1(k paramk, Context paramContext, int paramInt)
  {
    super(paramContext, paramInt);
  }
  
  public void onOrientationChanged(int paramInt)
  {
    Object localObject1 = k.a(a);
    j localj = k.b(a);
    Object localObject2 = k.a(a);
    if ((localObject2 != null) && (localj != null))
    {
      localObject1 = ((WindowManager)localObject1).getDefaultDisplay();
      int i = ((Display)localObject1).getRotation();
      localObject2 = a;
      int j = k.c((k)localObject2);
      if (i != j)
      {
        localObject2 = a;
        k.a((k)localObject2, i);
        localj.a(i);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/k$1.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */