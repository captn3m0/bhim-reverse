package in.juspay.widget.qrscanner.com.journeyapps.barcodescanner;

import android.os.Handler;
import android.os.Message;
import in.juspay.widget.qrscanner.a.b;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.k;

class h$2
  implements k
{
  h$2(h paramh) {}
  
  public void a(m paramm)
  {
    Object localObject1 = a;
    synchronized (h.b((h)localObject1))
    {
      localObject1 = a;
      boolean bool = h.c((h)localObject1);
      if (bool)
      {
        localObject1 = a;
        localObject1 = h.d((h)localObject1);
        int i = a.b.zxing_decode;
        localObject1 = ((Handler)localObject1).obtainMessage(i, paramm);
        ((Message)localObject1).sendToTarget();
      }
      return;
    }
  }
  
  public void a(Exception paramException)
  {
    Object localObject1 = a;
    synchronized (h.b((h)localObject1))
    {
      localObject1 = a;
      boolean bool = h.c((h)localObject1);
      if (bool)
      {
        localObject1 = a;
        localObject1 = h.d((h)localObject1);
        int i = a.b.zxing_preview_failed;
        localObject1 = ((Handler)localObject1).obtainMessage(i);
        ((Message)localObject1).sendToTarget();
      }
      return;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/journeyapps/barcodescanner/h$2.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */