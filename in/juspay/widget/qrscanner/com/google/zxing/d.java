package in.juspay.widget.qrscanner.com.google.zxing;

import java.util.List;

public enum d
{
  private final Class l;
  
  static
  {
    int n = 4;
    int i1 = 3;
    int i2 = 2;
    int i3 = 1;
    Object localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("OTHER", 0, Object.class);
    a = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("PURE_BARCODE", i3, Void.class);
    b = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("POSSIBLE_FORMATS", i2, List.class);
    c = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("TRY_HARDER", i1, Void.class);
    d = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("CHARACTER_SET", n, String.class);
    e = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("ALLOWED_LENGTHS", 5, int[].class);
    f = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("ASSUME_CODE_39_CHECK_DIGIT", 6, Void.class);
    g = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("ASSUME_GS1", 7, Void.class);
    h = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("RETURN_CODABAR_START_END", 8, Void.class);
    i = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("NEED_RESULT_POINT_CALLBACK", 9, n.class);
    j = (d)localObject;
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/d;
    ((d)localObject).<init>("ALLOWED_EAN_EXTENSIONS", 10, int[].class);
    k = (d)localObject;
    localObject = new d[11];
    d locald1 = a;
    localObject[0] = locald1;
    locald1 = b;
    localObject[i3] = locald1;
    locald1 = c;
    localObject[i2] = locald1;
    locald1 = d;
    localObject[i1] = locald1;
    locald1 = e;
    localObject[n] = locald1;
    d locald2 = f;
    localObject[5] = locald2;
    locald2 = g;
    localObject[6] = locald2;
    locald2 = h;
    localObject[7] = locald2;
    locald2 = i;
    localObject[8] = locald2;
    locald2 = j;
    localObject[9] = locald2;
    locald2 = k;
    localObject[10] = locald2;
    m = (d[])localObject;
  }
  
  private d(Class paramClass)
  {
    l = paramClass;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */