package in.juspay.widget.qrscanner.com.google.zxing;

import in.juspay.widget.qrscanner.com.google.zxing.common.a.a;

public class m
{
  private final float a;
  private final float b;
  
  public m(float paramFloat1, float paramFloat2)
  {
    a = paramFloat1;
    b = paramFloat2;
  }
  
  public static float a(m paramm1, m paramm2)
  {
    float f1 = a;
    float f2 = b;
    float f3 = a;
    float f4 = b;
    return a.a(f1, f2, f3, f4);
  }
  
  private static float a(m paramm1, m paramm2, m paramm3)
  {
    float f1 = a;
    float f2 = b;
    float f3 = a - f1;
    float f4 = b - f2;
    f3 *= f4;
    f2 = b - f2;
    f1 = (a - f1) * f2;
    return f3 - f1;
  }
  
  public static void a(m[] paramArrayOfm)
  {
    int i = 2;
    int j = 1;
    Object localObject1 = paramArrayOfm[0];
    m localm1 = paramArrayOfm[j];
    float f1 = a((m)localObject1, localm1);
    localm1 = paramArrayOfm[j];
    Object localObject2 = paramArrayOfm[i];
    float f2 = a(localm1, (m)localObject2);
    localObject2 = paramArrayOfm[0];
    m localm2 = paramArrayOfm[i];
    float f3 = a((m)localObject2, localm2);
    boolean bool1 = f2 < f1;
    if (!bool1)
    {
      bool1 = f2 < f3;
      if (!bool1)
      {
        localm1 = paramArrayOfm[0];
        localObject2 = paramArrayOfm[j];
        localObject1 = paramArrayOfm[i];
        float f4 = a((m)localObject2, localm1, (m)localObject1);
        bool1 = f4 < 0.0F;
        if (!bool1) {
          break label191;
        }
      }
    }
    for (;;)
    {
      paramArrayOfm[0] = localObject1;
      paramArrayOfm[j] = localm1;
      paramArrayOfm[i] = localObject2;
      return;
      boolean bool2 = f3 < f2;
      if (!bool2)
      {
        boolean bool3 = f3 < f1;
        if (!bool3)
        {
          localm1 = paramArrayOfm[j];
          localObject2 = paramArrayOfm[0];
          localObject1 = paramArrayOfm[i];
          break;
        }
      }
      localm1 = paramArrayOfm[i];
      localObject2 = paramArrayOfm[0];
      localObject1 = paramArrayOfm[j];
      break;
      label191:
      Object localObject3 = localObject1;
      localObject1 = localObject2;
      localObject2 = localObject3;
    }
  }
  
  public final float a()
  {
    return a;
  }
  
  public final float b()
  {
    return b;
  }
  
  public final boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    boolean bool2 = paramObject instanceof m;
    if (bool2)
    {
      paramObject = (m)paramObject;
      float f1 = a;
      float f2 = a;
      bool2 = f1 < f2;
      if (!bool2)
      {
        f1 = b;
        f2 = b;
        bool2 = f1 < f2;
        if (!bool2) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  public final int hashCode()
  {
    int i = Float.floatToIntBits(a) * 31;
    int j = Float.floatToIntBits(b);
    return i + j;
  }
  
  public final String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append("(");
    float f = a;
    localStringBuilder = localStringBuilder.append(f).append(',');
    f = b;
    return f + ')';
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/m.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */