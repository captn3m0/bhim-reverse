package in.juspay.widget.qrscanner.com.google.zxing;

public final class NotFoundException
  extends ReaderException
{
  private static final NotFoundException c;
  
  static
  {
    NotFoundException localNotFoundException = new in/juspay/widget/qrscanner/com/google/zxing/NotFoundException;
    localNotFoundException.<init>();
    c = localNotFoundException;
    localNotFoundException = c;
    StackTraceElement[] arrayOfStackTraceElement = b;
    localNotFoundException.setStackTrace(arrayOfStackTraceElement);
  }
  
  public static NotFoundException a()
  {
    return c;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/NotFoundException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */