package in.juspay.widget.qrscanner.com.google.zxing;

public final class i
  extends f
{
  private final byte[] a;
  private final int b;
  private final int c;
  private final int d;
  private final int e;
  
  public i(int paramInt1, int paramInt2, int[] paramArrayOfInt)
  {
    super(paramInt1, paramInt2);
    b = paramInt1;
    c = paramInt2;
    d = 0;
    e = 0;
    int j = paramInt1 * paramInt2;
    byte[] arrayOfByte1 = new byte[j];
    a = arrayOfByte1;
    while (i < j)
    {
      int k = paramArrayOfInt[i];
      int m = k >> 16 & 0xFF;
      int n = k >> 7 & 0x1FE;
      k &= 0xFF;
      byte[] arrayOfByte2 = a;
      m += n;
      k = (byte)((k + m) / 4);
      arrayOfByte2[i] = k;
      i += 1;
    }
  }
  
  public byte[] a()
  {
    int i = 0;
    int j = b();
    int k = c();
    int m = b;
    byte[] arrayOfByte1;
    if (j == m)
    {
      m = c;
      if (k == m) {
        arrayOfByte1 = a;
      }
    }
    for (;;)
    {
      return arrayOfByte1;
      int n = j * k;
      arrayOfByte1 = new byte[n];
      int i1 = e;
      int i2 = b;
      i1 *= i2;
      i2 = d;
      i1 += i2;
      i2 = b;
      if (j == i2)
      {
        byte[] arrayOfByte2 = a;
        System.arraycopy(arrayOfByte2, i1, arrayOfByte1, 0, n);
      }
      else
      {
        while (i < k)
        {
          n = i * j;
          byte[] arrayOfByte3 = a;
          System.arraycopy(arrayOfByte3, i1, arrayOfByte1, n, j);
          n = b;
          i1 += n;
          i += 1;
        }
      }
    }
  }
  
  public byte[] a(int paramInt, byte[] paramArrayOfByte)
  {
    if (paramInt >= 0)
    {
      i = c();
      if (paramInt < i) {}
    }
    else
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = "Requested row is outside the image: " + paramInt;
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    }
    int i = b();
    if (paramArrayOfByte != null)
    {
      j = paramArrayOfByte.length;
      if (j >= i) {}
    }
    else
    {
      paramArrayOfByte = new byte[i];
    }
    int j = e + paramInt;
    int k = b;
    j *= k;
    k = d;
    j += k;
    System.arraycopy(a, j, paramArrayOfByte, 0, i);
    return paramArrayOfByte;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */