package in.juspay.widget.qrscanner.com.google.zxing;

public final class WriterException
  extends Exception
{
  public WriterException() {}
  
  public WriterException(String paramString)
  {
    super(paramString);
  }
  
  public WriterException(Throwable paramThrowable)
  {
    super(paramThrowable);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/WriterException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */