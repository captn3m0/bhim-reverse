package in.juspay.widget.qrscanner.com.google.zxing.common;

import java.util.Arrays;

public final class b
  implements Cloneable
{
  private final int a;
  private final int b;
  private final int c;
  private final int[] d;
  
  public b(int paramInt)
  {
    this(paramInt, paramInt);
  }
  
  public b(int paramInt1, int paramInt2)
  {
    if ((paramInt1 < i) || (paramInt2 < i))
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("Both dimensions must be greater than 0");
      throw ((Throwable)localObject);
    }
    a = paramInt1;
    b = paramInt2;
    i = (paramInt1 + 31) / 32;
    c = i;
    Object localObject = new int[c * paramInt2];
    d = ((int[])localObject);
  }
  
  private b(int paramInt1, int paramInt2, int paramInt3, int[] paramArrayOfInt)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
    d = paramArrayOfInt;
  }
  
  private String a(String paramString1, String paramString2, String paramString3)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = b;
    int j = a + 1;
    i *= j;
    localStringBuilder.<init>(i);
    i = 0;
    for (;;)
    {
      j = b;
      if (i >= j) {
        break;
      }
      j = 0;
      int k = a;
      if (j < k)
      {
        boolean bool = a(j, i);
        if (bool) {}
        for (String str = paramString1;; str = paramString2)
        {
          localStringBuilder.append(str);
          j += 1;
          break;
        }
      }
      localStringBuilder.append(paramString3);
      i += 1;
    }
    return localStringBuilder.toString();
  }
  
  public String a(String paramString1, String paramString2)
  {
    return a(paramString1, paramString2, "\n");
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = 1;
    IllegalArgumentException localIllegalArgumentException;
    if ((paramInt2 < 0) || (paramInt1 < 0))
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Left and top must be nonnegative");
      throw localIllegalArgumentException;
    }
    if ((paramInt4 < i) || (paramInt3 < i))
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Height and width must be at least 1");
      throw localIllegalArgumentException;
    }
    int j = paramInt1 + paramInt3;
    int k = paramInt2 + paramInt4;
    int m = b;
    if (k <= m)
    {
      m = a;
      if (j <= m) {}
    }
    else
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("The region must fit inside the matrix");
      throw localIllegalArgumentException;
    }
    for (;;)
    {
      paramInt2 += 1;
      if (paramInt2 >= k) {
        break;
      }
      m = c;
      int n = paramInt2 * m;
      m = paramInt1;
      while (m < j)
      {
        int[] arrayOfInt = d;
        int i1 = m / 32 + n;
        int i2 = arrayOfInt[i1];
        int i3 = m & 0x1F;
        i3 = i << i3;
        i2 |= i3;
        arrayOfInt[i1] = i2;
        m += 1;
      }
    }
  }
  
  public boolean a(int paramInt1, int paramInt2)
  {
    int i = c * paramInt2;
    int k = paramInt1 / 32;
    i += k;
    int[] arrayOfInt = d;
    i = arrayOfInt[i];
    int m = paramInt1 & 0x1F;
    i = i >>> m & 0x1;
    if (i != 0) {}
    int j;
    for (i = 1;; j = 0) {
      return i;
    }
  }
  
  public int[] a()
  {
    int i = 0;
    int j = 0;
    int[] arrayOfInt1 = null;
    for (;;)
    {
      arrayOfInt2 = d;
      k = arrayOfInt2.length;
      if (j >= k) {
        break;
      }
      arrayOfInt2 = d;
      k = arrayOfInt2[j];
      if (k != 0) {
        break;
      }
      j += 1;
    }
    int[] arrayOfInt2 = d;
    int k = arrayOfInt2.length;
    if (j == k)
    {
      j = 0;
      arrayOfInt1 = null;
    }
    for (;;)
    {
      return arrayOfInt1;
      k = c;
      k = j / k;
      int m = c;
      m = j % m * 32;
      int[] arrayOfInt3 = d;
      int n = arrayOfInt3[j];
      j = 0;
      arrayOfInt1 = null;
      for (;;)
      {
        int i1 = 31 - j;
        i1 = n << i1;
        if (i1 != 0) {
          break;
        }
        j += 1;
      }
      m += j;
      j = 2;
      arrayOfInt1 = new int[j];
      arrayOfInt1[0] = m;
      i = 1;
      arrayOfInt1[i] = k;
    }
  }
  
  public void b(int paramInt1, int paramInt2)
  {
    int i = c * paramInt2;
    int j = paramInt1 / 32;
    i += j;
    int[] arrayOfInt = d;
    int k = arrayOfInt[i];
    int m = paramInt1 & 0x1F;
    int n = 1 << m;
    k |= n;
    arrayOfInt[i] = k;
  }
  
  public int[] b()
  {
    int[] arrayOfInt1 = d;
    int i = arrayOfInt1.length + -1;
    int j;
    while (i >= 0)
    {
      int[] arrayOfInt2 = d;
      j = arrayOfInt2[i];
      if (j != 0) {
        break;
      }
      i += -1;
    }
    if (i < 0)
    {
      i = 0;
      arrayOfInt1 = null;
    }
    for (;;)
    {
      return arrayOfInt1;
      j = c;
      j = i / j;
      int k = c;
      k = i % k * 32;
      int[] arrayOfInt3 = d;
      int m = arrayOfInt3[i];
      i = 31;
      for (;;)
      {
        int n = m >>> i;
        if (n != 0) {
          break;
        }
        i += -1;
      }
      k += i;
      i = 2;
      arrayOfInt1 = new int[i];
      m = 0;
      arrayOfInt3 = null;
      arrayOfInt1[0] = k;
      k = 1;
      arrayOfInt1[k] = j;
    }
  }
  
  public int c()
  {
    return a;
  }
  
  public void c(int paramInt1, int paramInt2)
  {
    int i = c * paramInt2;
    int j = paramInt1 / 32;
    i += j;
    int[] arrayOfInt = d;
    int k = arrayOfInt[i];
    int m = paramInt1 & 0x1F;
    int n = 1 << m;
    k ^= n;
    arrayOfInt[i] = k;
  }
  
  public int d()
  {
    return b;
  }
  
  public b e()
  {
    b localb = new in/juspay/widget/qrscanner/com/google/zxing/common/b;
    int i = a;
    int j = b;
    int k = c;
    int[] arrayOfInt = (int[])d.clone();
    localb.<init>(i, j, k, arrayOfInt);
    return localb;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    boolean bool2 = paramObject instanceof b;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      paramObject = (b)paramObject;
      int i = a;
      int j = a;
      if (i == j)
      {
        i = b;
        j = b;
        if (i == j)
        {
          i = c;
          j = c;
          if (i == j)
          {
            int[] arrayOfInt1 = d;
            int[] arrayOfInt2 = d;
            boolean bool3 = Arrays.equals(arrayOfInt1, arrayOfInt2);
            if (bool3) {
              bool1 = true;
            }
          }
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = a * 31;
    int j = a;
    i = (i + j) * 31;
    j = b;
    i = (i + j) * 31;
    j = c;
    i = (i + j) * 31;
    j = Arrays.hashCode(d);
    return i + j;
  }
  
  public String toString()
  {
    return a("X ", "  ");
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */