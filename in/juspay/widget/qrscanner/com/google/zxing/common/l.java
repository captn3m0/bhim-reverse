package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.d;
import java.nio.charset.Charset;
import java.util.Map;

public final class l
{
  private static final String a = Charset.defaultCharset().name();
  private static final boolean b;
  
  static
  {
    String str1 = "SJIS";
    String str2 = a;
    boolean bool = str1.equalsIgnoreCase(str2);
    if (!bool)
    {
      str1 = "EUC_JP";
      str2 = a;
      bool = str1.equalsIgnoreCase(str2);
      if (!bool) {}
    }
    else
    {
      bool = true;
    }
    for (;;)
    {
      b = bool;
      return;
      bool = false;
      str1 = null;
    }
  }
  
  public static String a(byte[] paramArrayOfByte, Map paramMap)
  {
    Object localObject;
    if (paramMap != null)
    {
      localObject = d.e;
      boolean bool1 = paramMap.containsKey(localObject);
      if (bool1)
      {
        localObject = d.e;
        localObject = paramMap.get(localObject).toString();
        return (String)localObject;
      }
    }
    int k = paramArrayOfByte.length;
    int m = k;
    int n = 1;
    int i1 = 1;
    float f1 = Float.MIN_VALUE;
    int i2 = 1;
    float f2 = Float.MIN_VALUE;
    int i3 = 0;
    int i4 = 0;
    int i5 = 0;
    int i6 = 0;
    int i7 = 0;
    float f3 = 0.0F;
    int i8 = 0;
    int i9 = 0;
    int i10 = 0;
    float f4 = 0.0F;
    int i11 = 0;
    int i12 = 0;
    int i13 = 0;
    int i = paramArrayOfByte.length;
    int i14 = 3;
    label159:
    int i15;
    label176:
    int i16;
    if (i > i14)
    {
      localObject = null;
      i = paramArrayOfByte[0];
      i14 = -17;
      if (i == i14)
      {
        i = paramArrayOfByte[1];
        i14 = -69;
        if (i == i14)
        {
          i = paramArrayOfByte[2];
          i14 = -65;
          if (i == i14)
          {
            i = 1;
            i15 = 0;
            i14 = 0;
            i12 = 0;
            i10 = i2;
            f4 = f2;
            k = i15;
            if ((i15 >= m) || ((n == 0) && (i1 == 0) && (i10 == 0))) {
              break label1065;
            }
            i2 = paramArrayOfByte[i15];
            k = i2;
            k = i2 & 0xFF;
            i16 = k;
            if (i10 == 0) {
              break label1362;
            }
            if (i3 <= 0) {
              break label482;
            }
            k &= 0x80;
            i2 = k;
            if (k != 0) {
              break label465;
            }
            i10 = 0;
            f4 = 0.0F;
            i2 = 0;
            f2 = 0.0F;
          }
        }
      }
    }
    for (;;)
    {
      label263:
      label312:
      int i17;
      int i18;
      if (n != 0)
      {
        i10 = 127;
        f4 = 1.78E-43F;
        k = i16;
        if (i16 > i10)
        {
          i10 = 160;
          f4 = 2.24E-43F;
          if (i16 < i10)
          {
            i10 = 0;
            f4 = 0.0F;
            n = 0;
          }
        }
      }
      else
      {
        if (i1 == 0) {
          break label1327;
        }
        if (i7 <= 0) {
          break label772;
        }
        i10 = 64;
        f4 = 9.0E-44F;
        k = i16;
        if (i16 >= i10)
        {
          i10 = 127;
          f4 = 1.78E-43F;
          if (i16 != i10)
          {
            i10 = 252;
            f4 = 3.53E-43F;
            if (i16 <= i10) {
              break label727;
            }
          }
        }
        f4 = 0.0F;
        i17 = i11;
        i11 = i12;
        i12 = i17;
        i18 = i9;
        i9 = i8;
        i8 = i7;
        i7 = 0;
        f3 = 0.0F;
        i10 = i18;
      }
      for (;;)
      {
        i15 += 1;
        i1 = i7;
        f1 = f3;
        i7 = i8;
        i8 = i9;
        i9 = i10;
        i10 = i2;
        f4 = f2;
        i17 = i11;
        i11 = i12;
        i12 = i17;
        break label176;
        i = 0;
        localObject = null;
        break label159;
        label465:
        i3 += -1;
        i2 = i10;
        f2 = f4;
        break label263;
        label482:
        k &= 0x80;
        i2 = k;
        if (k == 0) {
          break label1362;
        }
        i2 = i16 & 0x40;
        if (i2 == 0)
        {
          i10 = 0;
          f4 = 0.0F;
          i2 = 0;
          f2 = 0.0F;
          break label263;
        }
        i3 += 1;
        i2 = i16 & 0x20;
        if (i2 == 0)
        {
          i4 += 1;
          i2 = i10;
          f2 = f4;
          break label263;
        }
        i3 += 1;
        i2 = i16 & 0x10;
        if (i2 == 0)
        {
          i5 += 1;
          i2 = i10;
          f2 = f4;
          break label263;
        }
        i3 += 1;
        i2 = i16 & 0x8;
        if (i2 == 0)
        {
          i6 += 1;
          i2 = i10;
          f2 = f4;
          break label263;
        }
        i10 = 0;
        f4 = 0.0F;
        i2 = 0;
        f2 = 0.0F;
        break label263;
        i10 = 159;
        f4 = 2.23E-43F;
        k = i16;
        if (i16 <= i10) {
          break label312;
        }
        i10 = 192;
        f4 = 2.69E-43F;
        if (i16 >= i10)
        {
          i10 = 215;
          f4 = 3.01E-43F;
          if (i16 != i10)
          {
            i10 = 247;
            f4 = 3.46E-43F;
            if (i16 != i10) {
              break label312;
            }
          }
        }
        i10 = i13 + 1;
        i13 = i10;
        break label312;
        label727:
        i10 = i7 + -1;
        i7 = i1;
        f3 = f1;
        i17 = i12;
        i12 = i11;
        i11 = i17;
        i18 = i8;
        i8 = i10;
        i10 = i9;
        i9 = i18;
        continue;
        label772:
        i10 = 128;
        f4 = 1.794E-43F;
        k = i16;
        if (i16 != i10)
        {
          i10 = 160;
          f4 = 2.24E-43F;
          if (i16 != i10)
          {
            i10 = 239;
            f4 = 3.35E-43F;
            if (i16 <= i10) {
              break label864;
            }
          }
        }
        f4 = 0.0F;
        i17 = i11;
        i11 = i12;
        i12 = i17;
        i18 = i9;
        i9 = i8;
        i8 = i7;
        i7 = 0;
        f3 = 0.0F;
        i10 = i18;
        continue;
        label864:
        i10 = 160;
        f4 = 2.24E-43F;
        if (i16 > i10)
        {
          i10 = 224;
          f4 = 3.14E-43F;
          if (i16 < i10)
          {
            i8 += 1;
            i10 = 0;
            f4 = 0.0F;
            i12 = i9 + 1;
            if (i12 <= i11) {
              break label1293;
            }
            i11 = 0;
            i9 = i8;
            i10 = i12;
            i8 = i7;
            i7 = i1;
            f3 = f1;
            continue;
          }
        }
        i10 = 127;
        f4 = 1.78E-43F;
        k = i16;
        if (i16 > i10)
        {
          i9 = i7 + 1;
          i10 = 0;
          f4 = 0.0F;
          i12 += 1;
          if (i12 > i14)
          {
            i14 = i12;
            i7 = i1;
            f3 = f1;
            i17 = i12;
            i12 = i11;
            i11 = i14;
            i18 = i9;
            i9 = i8;
            i8 = i18;
          }
        }
        else
        {
          i10 = 0;
          f4 = 0.0F;
          i9 = i8;
          i8 = i7;
          i7 = i1;
          f3 = f1;
          i17 = 0;
          i12 = i11;
          i11 = 0;
          continue;
          label1065:
          if ((i10 != 0) && (i3 > 0)) {}
          for (i12 = 0;; i12 = i10)
          {
            if ((i1 != 0) && (i7 > 0))
            {
              i1 = 0;
              f1 = 0.0F;
            }
            if (i12 != 0) {
              if (i == 0)
              {
                i = i4 + i5 + i6;
                if (i <= 0) {}
              }
              else
              {
                localObject = "UTF8";
                break;
              }
            }
            int j;
            if (i1 != 0)
            {
              boolean bool2 = b;
              if (!bool2)
              {
                j = 3;
                if (i11 < j)
                {
                  j = 3;
                  if (i14 < j) {
                    break label1157;
                  }
                }
              }
              localObject = "SJIS";
              break;
            }
            label1157:
            if ((n != 0) && (i1 != 0))
            {
              j = 2;
              if (i11 == j)
              {
                j = 2;
                if (i8 == j) {}
              }
              else
              {
                j = i13 * 10;
                k = m;
                if (j < m) {
                  break label1205;
                }
              }
              localObject = "SJIS";
              break;
              label1205:
              localObject = "ISO8859_1";
              break;
            }
            if (n != 0)
            {
              localObject = "ISO8859_1";
              break;
            }
            if (i1 != 0)
            {
              localObject = "SJIS";
              break;
            }
            if (i12 != 0)
            {
              localObject = "UTF8";
              break;
            }
            localObject = a;
            break;
          }
        }
        i7 = i1;
        f3 = f1;
        i17 = i12;
        i12 = i11;
        i11 = i17;
        i18 = i9;
        i9 = i8;
        i8 = i18;
        continue;
        label1293:
        i9 = i8;
        i8 = i7;
        i7 = i1;
        f3 = f1;
        i17 = i11;
        i11 = 0;
        i10 = i12;
        i12 = i17;
        continue;
        label1327:
        i10 = i9;
        i9 = i8;
        i8 = i7;
        i7 = i1;
        f3 = f1;
        i17 = i11;
        i11 = i12;
        i12 = i17;
      }
      label1362:
      i2 = i10;
      f2 = f4;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/l.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */