package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;

public abstract class i
{
  private static i a;
  
  static
  {
    f localf = new in/juspay/widget/qrscanner/com/google/zxing/common/f;
    localf.<init>();
    a = localf;
  }
  
  public static i a()
  {
    return a;
  }
  
  protected static void a(b paramb, float[] paramArrayOfFloat)
  {
    int i = -1;
    int j = 1;
    float f1 = Float.MIN_VALUE;
    int k = paramb.c();
    int m = paramb.d();
    int n = 0;
    float f2 = 0.0F;
    int i1 = j;
    float f3 = f1;
    int i2 = paramArrayOfFloat.length;
    float f4;
    if ((n < i2) && (i1 != 0))
    {
      f3 = paramArrayOfFloat[n];
      i1 = (int)f3;
      i2 = n + 1;
      f4 = paramArrayOfFloat[i2];
      i2 = (int)f4;
      if ((i1 < i) || (i1 > k) || (i2 < i) || (i2 > m)) {
        throw NotFoundException.a();
      }
      if (i1 == i)
      {
        paramArrayOfFloat[n] = 0.0F;
        i1 = j;
        f3 = f1;
      }
    }
    for (;;)
    {
      label125:
      if (i2 == i)
      {
        i1 = n + 1;
        paramArrayOfFloat[i1] = 0.0F;
        i1 = j;
        f3 = f1;
      }
      for (;;)
      {
        n += 2;
        break;
        if (i1 != k) {
          break label442;
        }
        f3 = k + -1;
        paramArrayOfFloat[n] = f3;
        i1 = j;
        f3 = f1;
        break label125;
        if (i2 == m)
        {
          i1 = n + 1;
          i2 = m + -1;
          f4 = i2;
          paramArrayOfFloat[i1] = f4;
          i1 = j;
          f3 = f1;
        }
      }
      i1 = paramArrayOfFloat.length + -2;
      n = j;
      f2 = f1;
      if ((i1 >= 0) && (n != 0))
      {
        f2 = paramArrayOfFloat[i1];
        n = (int)f2;
        i2 = i1 + 1;
        f4 = paramArrayOfFloat[i2];
        i2 = (int)f4;
        if ((n < i) || (n > k) || (i2 < i) || (i2 > m)) {
          throw NotFoundException.a();
        }
        if (n == i)
        {
          paramArrayOfFloat[i1] = 0.0F;
          n = j;
          f2 = f1;
        }
      }
      for (;;)
      {
        label328:
        if (i2 == i)
        {
          n = i1 + 1;
          paramArrayOfFloat[n] = 0.0F;
          n = j;
          f2 = f1;
        }
        for (;;)
        {
          i1 += -2;
          break;
          if (n != k) {
            break label433;
          }
          f2 = k + -1;
          paramArrayOfFloat[i1] = f2;
          n = j;
          f2 = f1;
          break label328;
          if (i2 == m)
          {
            n = i1 + 1;
            i2 = m + -1;
            f4 = i2;
            paramArrayOfFloat[n] = f4;
            n = j;
            f2 = f1;
          }
        }
        return;
        label433:
        n = 0;
        f2 = 0.0F;
      }
      label442:
      i1 = 0;
      f3 = 0.0F;
    }
  }
  
  public abstract b a(b paramb, int paramInt1, int paramInt2, k paramk);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */