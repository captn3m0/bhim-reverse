package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;

public final class f
  extends i
{
  public b a(b paramb, int paramInt1, int paramInt2, k paramk)
  {
    float f1 = 0.5F;
    if ((paramInt1 <= 0) || (paramInt2 <= 0)) {
      throw NotFoundException.a();
    }
    b localb = new in/juspay/widget/qrscanner/com/google/zxing/common/b;
    localb.<init>(paramInt1, paramInt2);
    int i = paramInt1 * 2;
    float[] arrayOfFloat = new float[i];
    for (int j = 0; j < paramInt2; j = i)
    {
      int k = arrayOfFloat.length;
      float f2 = j + f1;
      i = 0;
      float f3;
      int m;
      while (i < k)
      {
        f3 = i / 2 + f1;
        arrayOfFloat[i] = f3;
        m = i + 1;
        arrayOfFloat[m] = f2;
        i += 2;
      }
      paramk.a(arrayOfFloat);
      a(paramb, arrayOfFloat);
      i = 0;
      while (i < k) {
        try
        {
          f2 = arrayOfFloat[i];
          int n = (int)f2;
          m = i + 1;
          f3 = arrayOfFloat[m];
          m = (int)f3;
          boolean bool = paramb.a(n, m);
          if (bool)
          {
            int i1 = i / 2;
            localb.b(i1, j);
          }
          i += 2;
        }
        catch (ArrayIndexOutOfBoundsException localArrayIndexOutOfBoundsException)
        {
          throw NotFoundException.a();
        }
      }
      i = j + 1;
    }
    return localb;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */