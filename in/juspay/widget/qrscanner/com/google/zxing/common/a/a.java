package in.juspay.widget.qrscanner.com.google.zxing.common.a;

public final class a
{
  public static float a(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4)
  {
    float f1 = paramFloat1 - paramFloat3;
    float f2 = paramFloat2 - paramFloat4;
    f1 *= f1;
    f2 *= f2;
    return (float)Math.sqrt(f1 + f2);
  }
  
  public static float a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = paramInt1 - paramInt3;
    int j = paramInt2 - paramInt4;
    i *= i;
    j *= j;
    return (float)Math.sqrt(i + j);
  }
  
  public static int a(float paramFloat)
  {
    float f = 0.0F;
    boolean bool = paramFloat < 0.0F;
    int i;
    if (bool) {
      i = -1090519040;
    }
    for (f = -0.5F;; f = 0.5F)
    {
      return (int)(f + paramFloat);
      i = 1056964608;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/a/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */