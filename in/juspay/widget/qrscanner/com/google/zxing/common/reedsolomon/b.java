package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

final class b
{
  private final a a;
  private final int[] b;
  
  b(a parama, int[] paramArrayOfInt)
  {
    int j = paramArrayOfInt.length;
    Object localObject;
    if (j == 0)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>();
      throw ((Throwable)localObject);
    }
    a = parama;
    int k = paramArrayOfInt.length;
    if (k > i)
    {
      j = paramArrayOfInt[0];
      if (j == 0)
      {
        j = i;
        while (j < k)
        {
          int m = paramArrayOfInt[j];
          if (m != 0) {
            break;
          }
          j += 1;
        }
        if (j == k)
        {
          localObject = new int[i];
          localObject[0] = 0;
          b = ((int[])localObject);
        }
      }
    }
    for (;;)
    {
      return;
      i = k - j;
      int[] arrayOfInt1 = new int[i];
      b = arrayOfInt1;
      arrayOfInt1 = b;
      int[] arrayOfInt2 = b;
      k = arrayOfInt2.length;
      System.arraycopy(paramArrayOfInt, j, arrayOfInt1, 0, k);
      continue;
      b = paramArrayOfInt;
    }
  }
  
  int a(int paramInt)
  {
    int[] arrayOfInt = b;
    int i = b.length + -1 - paramInt;
    return arrayOfInt[i];
  }
  
  b a(int paramInt1, int paramInt2)
  {
    Object localObject;
    if (paramInt1 < 0)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>();
      throw ((Throwable)localObject);
    }
    if (paramInt2 == 0) {
      localObject = a.a();
    }
    for (;;)
    {
      return (b)localObject;
      int i = b.length;
      int[] arrayOfInt1 = new int[i + paramInt1];
      int j = 0;
      localObject = null;
      while (j < i)
      {
        a locala1 = a;
        int[] arrayOfInt2 = b;
        int k = arrayOfInt2[j];
        int m = locala1.c(k, paramInt2);
        arrayOfInt1[j] = m;
        j += 1;
      }
      localObject = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
      a locala2 = a;
      ((b)localObject).<init>(locala2, arrayOfInt1);
    }
  }
  
  b a(b paramb)
  {
    int i = 0;
    Object localObject1 = a;
    Object localObject2 = a;
    boolean bool = localObject1.equals(localObject2);
    if (!bool)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("GenericGFPolys do not have same GenericGF field");
      throw ((Throwable)localObject1);
    }
    bool = c();
    if (bool) {}
    for (;;)
    {
      return paramb;
      bool = paramb.c();
      if (!bool) {
        break;
      }
      paramb = this;
    }
    localObject1 = b;
    localObject2 = b;
    int j = localObject1.length;
    int k = localObject2.length;
    if (j > k) {}
    for (;;)
    {
      int[] arrayOfInt = new int[localObject1.length];
      j = localObject1.length;
      k = localObject2.length;
      k = j - k;
      System.arraycopy(localObject1, 0, arrayOfInt, 0, k);
      j = k;
      for (;;)
      {
        i = localObject1.length;
        if (j >= i) {
          break;
        }
        i = j - k;
        i = localObject2[i];
        int m = localObject1[j];
        i = a.b(i, m);
        arrayOfInt[j] = i;
        j += 1;
      }
      paramb = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
      localObject1 = a;
      paramb.<init>((a)localObject1, arrayOfInt);
      break;
      Object localObject3 = localObject2;
      localObject2 = localObject1;
      localObject1 = localObject3;
    }
  }
  
  int[] a()
  {
    return b;
  }
  
  int b()
  {
    return b.length + -1;
  }
  
  int b(int paramInt)
  {
    int i = 1;
    int j = 0;
    if (paramInt == 0) {
      i = a(0);
    }
    for (;;)
    {
      return i;
      int m;
      if (paramInt == i)
      {
        int[] arrayOfInt1 = b;
        int k = arrayOfInt1.length;
        for (i = 0; j < k; i = m)
        {
          m = arrayOfInt1[j];
          m = a.b(i, m);
          j += 1;
        }
      }
      else
      {
        j = b[0];
        int[] arrayOfInt2 = b;
        int n = arrayOfInt2.length;
        int i1 = i;
        i = j;
        j = i1;
        while (j < n)
        {
          i = a.c(paramInt, i);
          arrayOfInt2 = b;
          m = arrayOfInt2[j];
          m = a.b(i, m);
          j += 1;
          i = m;
        }
      }
    }
  }
  
  b b(b paramb)
  {
    a locala1 = null;
    Object localObject = a;
    a locala2 = a;
    boolean bool = localObject.equals(locala2);
    if (!bool)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("GenericGFPolys do not have same GenericGF field");
      throw ((Throwable)localObject);
    }
    bool = c();
    if (!bool)
    {
      bool = paramb.c();
      if (!bool) {}
    }
    else
    {
      localObject = a.a();
    }
    for (;;)
    {
      return (b)localObject;
      int[] arrayOfInt1 = b;
      int j = arrayOfInt1.length;
      int[] arrayOfInt2 = b;
      int k = arrayOfInt2.length;
      int i = j + k + -1;
      int[] arrayOfInt3 = new int[i];
      int m = 0;
      locala2 = null;
      while (m < j)
      {
        int n = arrayOfInt1[m];
        i = 0;
        localObject = null;
        while (i < k)
        {
          int i1 = m + i;
          int i2 = m + i;
          i2 = arrayOfInt3[i2];
          a locala3 = a;
          int i3 = arrayOfInt2[i];
          int i4 = locala3.c(n, i3);
          i2 = a.b(i2, i4);
          arrayOfInt3[i1] = i2;
          i += 1;
        }
        i = m + 1;
        m = i;
      }
      localObject = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
      locala1 = a;
      ((b)localObject).<init>(locala1, arrayOfInt3);
    }
  }
  
  b c(int paramInt)
  {
    Object localObject;
    if (paramInt == 0)
    {
      localObject = a;
      this = ((a)localObject).a();
    }
    for (;;)
    {
      return this;
      int i = 1;
      if (paramInt != i)
      {
        int j = b.length;
        int[] arrayOfInt1 = new int[j];
        i = 0;
        localObject = null;
        while (i < j)
        {
          a locala1 = a;
          int[] arrayOfInt2 = b;
          int k = arrayOfInt2[i];
          int m = locala1.c(k, paramInt);
          arrayOfInt1[i] = m;
          i += 1;
        }
        localObject = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
        a locala2 = a;
        ((b)localObject).<init>(locala2, arrayOfInt1);
        this = (b)localObject;
      }
    }
  }
  
  boolean c()
  {
    boolean bool = false;
    int[] arrayOfInt = b;
    int i = arrayOfInt[0];
    if (i == 0) {
      bool = true;
    }
    return bool;
  }
  
  b[] c(b paramb)
  {
    Object localObject1 = a;
    Object localObject2 = a;
    boolean bool1 = localObject1.equals(localObject2);
    if (!bool1)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("GenericGFPolys do not have same GenericGF field");
      throw ((Throwable)localObject1);
    }
    bool1 = paramb.c();
    if (bool1)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("Divide by 0");
      throw ((Throwable)localObject1);
    }
    localObject1 = a.a();
    int i = paramb.b();
    i = paramb.a(i);
    Object localObject3 = a;
    int j = ((a)localObject3).c(i);
    localObject2 = localObject1;
    b localb1;
    for (localObject1 = this;; localObject1 = ((b)localObject1).a(localb1))
    {
      int k = ((b)localObject1).b();
      int n = paramb.b();
      if (k < n) {
        break;
      }
      boolean bool2 = ((b)localObject1).c();
      if (bool2) {
        break;
      }
      int m = ((b)localObject1).b();
      n = paramb.b();
      m -= n;
      a locala1 = a;
      int i1 = ((b)localObject1).b();
      i1 = ((b)localObject1).a(i1);
      n = locala1.c(i1, j);
      localb1 = paramb.a(m, n);
      a locala2 = a;
      b localb2 = locala2.a(m, n);
      localObject2 = ((b)localObject2).a(localb2);
    }
    localObject3 = new b[2];
    localObject3[0] = localObject2;
    localObject3[1] = localObject1;
    return (b[])localObject3;
  }
  
  public String toString()
  {
    int i = 1;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int j = b() * 8;
    localStringBuilder.<init>(j);
    j = b();
    int m = j;
    if (m >= 0)
    {
      j = a(m);
      Object localObject;
      if (j != 0)
      {
        if (j >= 0) {
          break label128;
        }
        localObject = " - ";
        localStringBuilder.append((String)localObject);
        j = -j;
        label61:
        if ((m == 0) || (j != i))
        {
          localObject = a;
          j = ((a)localObject).b(j);
          if (j != 0) {
            break label153;
          }
          j = 49;
          localStringBuilder.append(j);
        }
        label97:
        if (m != 0)
        {
          if (m != i) {
            break label190;
          }
          char c = 'x';
          localStringBuilder.append(c);
        }
      }
      for (;;)
      {
        int k = m + -1;
        m = k;
        break;
        label128:
        int n = localStringBuilder.length();
        if (n <= 0) {
          break label61;
        }
        localObject = " + ";
        localStringBuilder.append((String)localObject);
        break label61;
        label153:
        if (k == i)
        {
          k = 97;
          localStringBuilder.append(k);
          break label97;
        }
        localObject = "a^";
        localStringBuilder.append((String)localObject);
        localStringBuilder.append(k);
        break label97;
        label190:
        String str = "x^";
        localStringBuilder.append(str);
        localStringBuilder.append(m);
      }
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */