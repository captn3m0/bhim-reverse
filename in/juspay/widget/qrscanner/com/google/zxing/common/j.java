package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.f;
import java.lang.reflect.Array;

public final class j
  extends h
{
  private b a;
  
  public j(f paramf)
  {
    super(paramf);
  }
  
  private static int a(int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt1 < paramInt2) {}
    for (;;)
    {
      return paramInt2;
      if (paramInt1 > paramInt3) {
        paramInt2 = paramInt3;
      } else {
        paramInt2 = paramInt1;
      }
    }
  }
  
  private static void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, b paramb)
  {
    int i = 8;
    int j = paramInt2 * paramInt4 + paramInt1;
    int k = j;
    int m = 0;
    while (m < i)
    {
      j = 0;
      while (j < i)
      {
        int n = k + j;
        n = paramArrayOfByte[n] & 0xFF;
        if (n <= paramInt3)
        {
          n = paramInt1 + j;
          int i1 = paramInt2 + m;
          paramb.b(n, i1);
        }
        j += 1;
      }
      m += 1;
      j = k + paramInt4;
      k = j;
    }
  }
  
  private static void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[][] paramArrayOfInt, b paramb)
  {
    int i = 0;
    int j = 0;
    int k;
    if (j < paramInt2)
    {
      i = j << 3;
      k = paramInt4 + -8;
      if (i <= k) {
        break label283;
      }
    }
    for (;;)
    {
      i = 0;
      int m = 0;
      label38:
      int n;
      if (m < paramInt1)
      {
        i = m << 3;
        n = paramInt3 + -8;
        if (i <= n) {
          break label276;
        }
      }
      for (;;)
      {
        int i1 = paramInt1 + -3;
        int i2 = a(m, 2, i1);
        i1 = paramInt2 + -3;
        int i3 = a(j, 2, i1);
        i1 = 0;
        i = -2;
        for (;;)
        {
          int i4 = 2;
          if (i > i4) {
            break;
          }
          i4 = i3 + i;
          int[] arrayOfInt = paramArrayOfInt[i4];
          int i5 = i2 + -2;
          i5 = arrayOfInt[i5];
          int i6 = i2 + -1;
          i6 = arrayOfInt[i6];
          i5 += i6;
          i6 = arrayOfInt[i2];
          i5 += i6;
          i6 = i2 + 1;
          i6 = arrayOfInt[i6];
          i5 += i6;
          i6 = i2 + 2;
          i4 = arrayOfInt[i6] + i5;
          i1 += i4;
          i += 1;
        }
        i1 /= 25;
        i2 = paramInt3;
        a(paramArrayOfByte, n, k, i1, paramInt3, paramb);
        i = m + 1;
        m = i;
        break label38;
        i = j + 1;
        j = i;
        break;
        return;
        label276:
        n = i;
      }
      label283:
      k = i;
    }
  }
  
  private static int[][] a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Object localObject1 = { paramInt2, paramInt1 };
    localObject1 = (int[][])Array.newInstance(Integer.TYPE, (int[])localObject1);
    int i = 0;
    int j = 0;
    int k;
    if (j < paramInt2)
    {
      k = j << 3;
      i = paramInt4 + -8;
      if (k <= i) {
        break label576;
      }
    }
    for (;;)
    {
      k = 0;
      Object localObject2 = null;
      int m = 0;
      label67:
      int n;
      if (m < paramInt1)
      {
        n = m << 3;
        k = paramInt3 + -8;
        if (n <= k) {
          break label569;
        }
      }
      for (;;)
      {
        int i1 = 0;
        int i2 = 255;
        n = 0;
        Object localObject3 = null;
        int i3 = 0;
        int i4 = i * paramInt3 + k;
        label118:
        k = 8;
        int i5;
        label138:
        int i6;
        if (i3 < k)
        {
          k = 0;
          localObject2 = null;
          i5 = 0;
          k = 8;
          if (i5 < k)
          {
            k = i4 + i5;
            k = paramArrayOfByte[k] & 0xFF;
            i6 = i1 + k;
            if (k >= i2) {
              break label562;
            }
          }
        }
        label548:
        label562:
        for (i1 = k;; i1 = i2)
        {
          if (k > n) {}
          for (;;)
          {
            i5 += 1;
            i2 = i1;
            n = k;
            i1 = i6;
            break label138;
            k = n - i2;
            i5 = 24;
            if (k > i5)
            {
              i3 += 1;
              k = i4 + paramInt3;
              i4 = i3;
              for (i3 = i1;; i3 = i1)
              {
                i1 = 8;
                if (i4 >= i1) {
                  break;
                }
                i7 = 0;
                i1 = i3;
                i3 = 0;
                for (;;)
                {
                  i5 = 8;
                  if (i3 >= i5) {
                    break;
                  }
                  i5 = k + i3;
                  i5 = paramArrayOfByte[i5] & 0xFF;
                  i1 += i5;
                  i3 += 1;
                }
                i4 += 1;
                k += paramInt3;
              }
            }
            k = i4;
            i4 = i3;
            i3 = i1;
            i1 = i4 + 1;
            i4 = k + paramInt3;
            int i7 = i1;
            i1 = i3;
            i3 = i7;
            break label118;
            k = i1 >> 6;
            n -= i2;
            i4 = 24;
            if (n <= i4)
            {
              n = i2 / 2;
              if ((j <= 0) || (m <= 0)) {
                break label548;
              }
              k = j + -1;
              localObject2 = localObject1[k];
              k = localObject2[m];
              Object localObject4 = localObject1[j];
              i3 = m + -1;
              i4 = localObject4[i3] * 2;
              k += i4;
              i4 = j + -1;
              localObject4 = localObject1[i4];
              i3 = m + -1;
              i4 = localObject4[i3];
              k = (k + i4) / 4;
              if (i2 >= k) {
                break label548;
              }
            }
            for (;;)
            {
              localObject3 = localObject1[j];
              localObject3[m] = k;
              k = m + 1;
              m = k;
              break label67;
              i = j + 1;
              j = i;
              break;
              return (int[][])localObject1;
              k = n;
            }
            k = n;
          }
        }
        label569:
        k = n;
      }
      label576:
      i = k;
    }
  }
  
  public b b()
  {
    int i = 40;
    Object localObject = a;
    if (localObject != null)
    {
      localObject = a;
      return (b)localObject;
    }
    localObject = a();
    int j = ((f)localObject).b();
    int k = ((f)localObject).c();
    b localb;
    if ((j >= i) && (k >= i))
    {
      localObject = ((f)localObject).a();
      i = j >> 3;
      int m = j & 0x7;
      if (m != 0) {
        i += 1;
      }
      m = k >> 3;
      int n = k & 0x7;
      if (n != 0) {
        m += 1;
      }
      int[][] arrayOfInt = a((byte[])localObject, i, m, j, k);
      localb = new in/juspay/widget/qrscanner/com/google/zxing/common/b;
      localb.<init>(j, k);
      a((byte[])localObject, i, m, j, k, arrayOfInt, localb);
    }
    for (a = localb;; a = ((b)localObject))
    {
      localObject = a;
      break;
      localObject = super.b();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/j.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */