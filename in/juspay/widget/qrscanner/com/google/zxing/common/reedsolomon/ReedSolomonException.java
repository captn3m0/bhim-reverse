package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

public final class ReedSolomonException
  extends Exception
{
  public ReedSolomonException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */