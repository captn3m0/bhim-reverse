package in.juspay.widget.qrscanner.com.google.zxing.common;

import java.util.Arrays;

public final class a
  implements Cloneable
{
  private int[] a;
  private int b;
  
  public a()
  {
    b = 0;
    int[] arrayOfInt = new int[1];
    a = arrayOfInt;
  }
  
  a(int[] paramArrayOfInt, int paramInt)
  {
    a = paramArrayOfInt;
    b = paramInt;
  }
  
  private void b(int paramInt)
  {
    int[] arrayOfInt1 = a;
    int i = arrayOfInt1.length * 32;
    if (paramInt > i)
    {
      arrayOfInt1 = c(paramInt);
      int[] arrayOfInt2 = a;
      int[] arrayOfInt3 = a;
      int j = arrayOfInt3.length;
      System.arraycopy(arrayOfInt2, 0, arrayOfInt1, 0, j);
      a = arrayOfInt1;
    }
  }
  
  private static int[] c(int paramInt)
  {
    return new int[(paramInt + 31) / 32];
  }
  
  public int a()
  {
    return b;
  }
  
  public void a(int paramInt1, int paramInt2)
  {
    int i = 1;
    IllegalArgumentException localIllegalArgumentException;
    if (paramInt2 >= 0)
    {
      j = 32;
      if (paramInt2 <= j) {}
    }
    else
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Num bits must be between 0 and 32");
      throw localIllegalArgumentException;
    }
    int j = b + paramInt2;
    b(j);
    if (paramInt2 > 0)
    {
      j = paramInt2 + -1;
      j = paramInt1 >> j & 0x1;
      if (j == i) {
        j = i;
      }
      for (;;)
      {
        a(j);
        paramInt2 += -1;
        break;
        j = 0;
        localIllegalArgumentException = null;
      }
    }
  }
  
  public void a(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    int i = 0;
    int m;
    for (int j = paramInt1; i < paramInt3; j = m)
    {
      int k = 0;
      m = j;
      j = 0;
      for (;;)
      {
        int n = 8;
        if (k >= n) {
          break;
        }
        boolean bool = a(m);
        if (bool)
        {
          int i2 = 7 - k;
          int i1 = 1 << i2;
          j |= i1;
        }
        m += 1;
        k += 1;
      }
      k = paramInt2 + i;
      j = (byte)j;
      paramArrayOfByte[k] = j;
      i += 1;
    }
  }
  
  public void a(a parama)
  {
    int i = b;
    int j = b + i;
    b(j);
    j = 0;
    while (j < i)
    {
      boolean bool = parama.a(j);
      a(bool);
      j += 1;
    }
  }
  
  public void a(boolean paramBoolean)
  {
    int i = b + 1;
    b(i);
    if (paramBoolean)
    {
      int[] arrayOfInt = a;
      int j = b / 32;
      int k = arrayOfInt[j];
      int m = b & 0x1F;
      int n = 1 << m;
      k |= n;
      arrayOfInt[j] = k;
    }
    i = b + 1;
    b = i;
  }
  
  public boolean a(int paramInt)
  {
    int i = 1;
    int[] arrayOfInt = a;
    int j = paramInt / 32;
    int m = arrayOfInt[j];
    j = paramInt & 0x1F;
    int k = i << j;
    m &= k;
    if (m != 0) {}
    for (;;)
    {
      return i;
      i = 0;
    }
  }
  
  public int b()
  {
    return (b + 7) / 8;
  }
  
  public void b(a parama)
  {
    int i = b;
    int j = b;
    if (i != j)
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Sizes don't match");
      throw localIllegalArgumentException;
    }
    i = 0;
    IllegalArgumentException localIllegalArgumentException = null;
    for (;;)
    {
      int[] arrayOfInt1 = a;
      j = arrayOfInt1.length;
      if (i >= j) {
        break;
      }
      arrayOfInt1 = a;
      int k = arrayOfInt1[i];
      int[] arrayOfInt2 = a;
      int m = arrayOfInt2[i];
      k ^= m;
      arrayOfInt1[i] = k;
      i += 1;
    }
  }
  
  public a c()
  {
    a locala = new in/juspay/widget/qrscanner/com/google/zxing/common/a;
    int[] arrayOfInt = (int[])a.clone();
    int i = b;
    locala.<init>(arrayOfInt, i);
    return locala;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    boolean bool2 = paramObject instanceof a;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      paramObject = (a)paramObject;
      int i = b;
      int j = b;
      if (i == j)
      {
        int[] arrayOfInt1 = a;
        int[] arrayOfInt2 = a;
        boolean bool3 = Arrays.equals(arrayOfInt1, arrayOfInt2);
        if (bool3) {
          bool1 = true;
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = b * 31;
    int j = Arrays.hashCode(a);
    return i + j;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = b;
    localStringBuilder.<init>(i);
    i = 0;
    int j = b;
    if (i < j)
    {
      j = i & 0x7;
      if (j == 0)
      {
        j = 32;
        localStringBuilder.append(j);
      }
      boolean bool = a(i);
      if (bool) {}
      for (char c = 'X';; c = '.')
      {
        localStringBuilder.append(c);
        i += 1;
        break;
      }
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */