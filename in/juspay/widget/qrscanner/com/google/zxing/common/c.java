package in.juspay.widget.qrscanner.com.google.zxing.common;

public final class c
{
  private final byte[] a;
  private int b;
  private int c;
  
  public c(byte[] paramArrayOfByte)
  {
    a = paramArrayOfByte;
  }
  
  public int a()
  {
    int i = a.length;
    int j = b;
    i = (i - j) * 8;
    j = c;
    return i - j;
  }
  
  public int a(int paramInt)
  {
    int i = 255;
    int j = 8;
    int k = 0;
    byte[] arrayOfByte1 = null;
    int m = 1;
    if (paramInt >= m)
    {
      m = 32;
      if (paramInt <= m)
      {
        m = a();
        if (paramInt <= m) {
          break label65;
        }
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    String str = String.valueOf(paramInt);
    localIllegalArgumentException.<init>(str);
    throw localIllegalArgumentException;
    label65:
    m = c;
    int i1;
    byte[] arrayOfByte2;
    int i2;
    int i3;
    if (m > 0)
    {
      m = c;
      n = 8 - m;
      if (paramInt < n)
      {
        m = paramInt;
        n -= m;
        i1 = 8 - m;
        i1 = i >> i1 << n;
        arrayOfByte2 = a;
        i2 = b;
        i3 = arrayOfByte2[i2];
        n = (i1 & i3) >> n;
        paramInt -= m;
        i1 = c;
        m += i1;
        c = m;
        m = c;
        if (m == j)
        {
          c = 0;
          m = b + 1;
          b = m;
        }
        m = n;
      }
    }
    for (int n = paramInt;; n = paramInt)
    {
      if (n > 0)
      {
        for (;;)
        {
          if (n >= j)
          {
            m <<= 8;
            arrayOfByte1 = a;
            i1 = b;
            k = arrayOfByte1[i1] & 0xFF;
            m |= k;
            k = b + 1;
            b = k;
            n += -8;
            continue;
            m = n;
            break;
          }
        }
        if (n > 0)
        {
          k = 8 - n;
          i1 = i >> k << k;
          m <<= n;
          arrayOfByte2 = a;
          i2 = b;
          i3 = arrayOfByte2[i2];
          i1 &= i3;
          k = i1 >> k;
          m |= k;
          k = c;
          n += k;
          c = n;
        }
      }
      return m;
      m = 0;
      localIllegalArgumentException = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */