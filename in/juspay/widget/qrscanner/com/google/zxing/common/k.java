package in.juspay.widget.qrscanner.com.google.zxing.common;

public final class k
{
  private final float a;
  private final float b;
  private final float c;
  private final float d;
  private final float e;
  private final float f;
  private final float g;
  private final float h;
  private final float i;
  
  private k(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9)
  {
    a = paramFloat1;
    b = paramFloat4;
    c = paramFloat7;
    d = paramFloat2;
    e = paramFloat5;
    f = paramFloat8;
    g = paramFloat3;
    h = paramFloat6;
    i = paramFloat9;
  }
  
  public static k a(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8)
  {
    float f1 = paramFloat1 - paramFloat3 + paramFloat5 - paramFloat7;
    float f2 = paramFloat2 - paramFloat4 + paramFloat6 - paramFloat8;
    float f3 = 0.0F;
    boolean bool = f1 < 0.0F;
    k localk;
    float f4;
    float f5;
    float f6;
    float f7;
    float f8;
    float f9;
    float f10;
    if (!bool)
    {
      f3 = 0.0F;
      bool = f2 < 0.0F;
      if (!bool)
      {
        localk = new in/juspay/widget/qrscanner/com/google/zxing/common/k;
        f2 = paramFloat3 - paramFloat1;
        f3 = paramFloat5 - paramFloat3;
        f4 = paramFloat4 - paramFloat2;
        f5 = paramFloat6 - paramFloat4;
        f6 = 0.0F;
        f7 = 0.0F;
        f8 = 1.0F;
        f9 = paramFloat1;
        f10 = paramFloat2;
        localk.<init>(f2, f3, paramFloat1, f4, f5, paramFloat2, 0.0F, 0.0F, f8);
      }
    }
    for (;;)
    {
      return localk;
      f3 = paramFloat3 - paramFloat5;
      f9 = paramFloat7 - paramFloat5;
      f4 = paramFloat4 - paramFloat6;
      f5 = paramFloat8 - paramFloat6;
      f10 = f3 * f5;
      f6 = f9 * f4;
      f10 -= f6;
      f5 *= f1;
      f9 *= f2;
      f6 = (f5 - f9) / f10;
      f2 *= f3;
      f1 *= f4;
      f1 = f2 - f1;
      f7 = f1 / f10;
      localk = new in/juspay/widget/qrscanner/com/google/zxing/common/k;
      f2 = paramFloat3 - paramFloat1;
      f3 = f6 * paramFloat3;
      f2 += f3;
      f3 = paramFloat7 - paramFloat1;
      f9 = f7 * paramFloat7;
      f3 += f9;
      f9 = paramFloat4 - paramFloat2;
      f4 = f6 * paramFloat4 + f9;
      f9 = paramFloat8 - paramFloat2;
      f5 = f7 * paramFloat8 + f9;
      f8 = 1.0F;
      f9 = paramFloat1;
      f10 = paramFloat2;
      localk.<init>(f2, f3, paramFloat1, f4, f5, paramFloat2, f6, f7, f8);
    }
  }
  
  public static k a(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8, float paramFloat9, float paramFloat10, float paramFloat11, float paramFloat12, float paramFloat13, float paramFloat14, float paramFloat15, float paramFloat16)
  {
    k localk = b(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramFloat7, paramFloat8);
    return a(paramFloat9, paramFloat10, paramFloat11, paramFloat12, paramFloat13, paramFloat14, paramFloat15, paramFloat16).a(localk);
  }
  
  public static k b(float paramFloat1, float paramFloat2, float paramFloat3, float paramFloat4, float paramFloat5, float paramFloat6, float paramFloat7, float paramFloat8)
  {
    return a(paramFloat1, paramFloat2, paramFloat3, paramFloat4, paramFloat5, paramFloat6, paramFloat7, paramFloat8).a();
  }
  
  k a()
  {
    k localk = new in/juspay/widget/qrscanner/com/google/zxing/common/k;
    float f1 = e;
    float f2 = i;
    f1 *= f2;
    f2 = f;
    float f3 = h;
    f2 *= f3;
    f1 -= f2;
    f2 = f;
    f3 = g;
    f2 *= f3;
    f3 = d;
    float f4 = i;
    f3 *= f4;
    f2 -= f3;
    f3 = d;
    f4 = h;
    f3 *= f4;
    f4 = e;
    float f5 = g;
    f4 *= f5;
    f3 -= f4;
    f4 = c;
    f5 = h;
    f4 *= f5;
    f5 = b;
    float f6 = i;
    f5 *= f6;
    f4 -= f5;
    f5 = a;
    f6 = i;
    f5 *= f6;
    f6 = c;
    float f7 = g;
    f6 *= f7;
    f5 -= f6;
    f6 = b;
    f7 = g;
    f6 *= f7;
    f7 = a;
    float f8 = h;
    f7 *= f8;
    f6 -= f7;
    f7 = b;
    f8 = f;
    f7 *= f8;
    f8 = c;
    float f9 = e;
    f8 *= f9;
    f7 -= f8;
    f8 = c;
    f9 = d;
    f8 *= f9;
    f9 = a;
    float f10 = f;
    f9 *= f10;
    f8 -= f9;
    f9 = a;
    f10 = e;
    f9 *= f10;
    f10 = b;
    float f11 = d;
    f10 *= f11;
    f9 -= f10;
    localk.<init>(f1, f2, f3, f4, f5, f6, f7, f8, f9);
    return localk;
  }
  
  k a(k paramk)
  {
    k localk = new in/juspay/widget/qrscanner/com/google/zxing/common/k;
    float f1 = a;
    float f2 = a;
    f1 *= f2;
    f2 = d;
    float f3 = b;
    f2 *= f3;
    f1 += f2;
    f2 = g;
    f3 = c;
    f2 *= f3;
    f1 += f2;
    f2 = a;
    f3 = d;
    f2 *= f3;
    f3 = d;
    float f4 = e;
    f3 *= f4;
    f2 += f3;
    f3 = g;
    f4 = f;
    f3 *= f4;
    f2 += f3;
    f3 = a;
    f4 = g;
    f3 *= f4;
    f4 = d;
    float f5 = h;
    f4 *= f5;
    f3 += f4;
    f4 = g;
    f5 = i;
    f4 *= f5;
    f3 += f4;
    f4 = b;
    f5 = a;
    f4 *= f5;
    f5 = e;
    float f6 = b;
    f5 *= f6;
    f4 += f5;
    f5 = h;
    f6 = c;
    f5 *= f6;
    f4 += f5;
    f5 = b;
    f6 = d;
    f5 *= f6;
    f6 = e;
    float f7 = e;
    f6 *= f7;
    f5 += f6;
    f6 = h;
    f7 = f;
    f6 *= f7;
    f5 += f6;
    f6 = b;
    f7 = g;
    f6 *= f7;
    f7 = e;
    float f8 = h;
    f7 *= f8;
    f6 += f7;
    f7 = h;
    f8 = i;
    f7 *= f8;
    f6 += f7;
    f7 = c;
    f8 = a;
    f7 *= f8;
    f8 = f;
    float f9 = b;
    f8 *= f9;
    f7 += f8;
    f8 = i;
    f9 = c;
    f8 *= f9;
    f7 += f8;
    f8 = c;
    f9 = d;
    f8 *= f9;
    f9 = f;
    float f10 = e;
    f9 *= f10;
    f8 += f9;
    f9 = i;
    f10 = f;
    f9 *= f10;
    f8 += f9;
    f9 = c;
    f10 = g;
    f9 *= f10;
    f10 = f;
    float f11 = h;
    f10 *= f11;
    f9 += f10;
    f10 = i;
    f11 = i;
    f10 *= f11;
    f9 += f10;
    localk.<init>(f1, f2, f3, f4, f5, f6, f7, f8, f9);
    return localk;
  }
  
  public void a(float[] paramArrayOfFloat)
  {
    int j = paramArrayOfFloat.length;
    float f1 = a;
    float f2 = b;
    float f3 = c;
    float f4 = d;
    float f5 = e;
    float f6 = f;
    float f7 = g;
    float f8 = h;
    float f9 = i;
    int k = 0;
    while (k < j)
    {
      float f10 = paramArrayOfFloat[k];
      int m = k + 1;
      float f11 = paramArrayOfFloat[m];
      float f12 = f3 * f10;
      float f13 = f6 * f11;
      f12 = f12 + f13 + f9;
      f13 = f1 * f10;
      float f14 = f4 * f11;
      f13 = (f13 + f14 + f7) / f12;
      paramArrayOfFloat[k] = f13;
      int n = k + 1;
      f10 *= f2;
      f11 *= f5;
      f10 = (f10 + f11 + f8) / f12;
      paramArrayOfFloat[n] = f10;
      k += 2;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/k.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */