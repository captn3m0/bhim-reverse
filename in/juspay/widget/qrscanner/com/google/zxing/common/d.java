package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.FormatException;
import java.util.HashMap;
import java.util.Map;

public enum d
{
  private static final Map B;
  private static final Map C;
  private final int[] D;
  private final String[] E;
  
  static
  {
    int i1 = 4;
    int i2 = 3;
    int i3 = 2;
    int i4 = 1;
    Object localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    Object localObject2 = new int[i3];
    Object tmp20_18 = localObject2;
    tmp20_18[0] = 0;
    tmp20_18[1] = 2;
    Object localObject3 = new String[0];
    ((d)localObject1).<init>("Cp437", 0, (int[])localObject2, (String[])localObject3);
    a = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    localObject2 = new int[i3];
    Object tmp62_60 = localObject2;
    tmp62_60[0] = 1;
    tmp62_60[1] = 3;
    localObject3 = new String[i4];
    localObject3[0] = "ISO-8859-1";
    ((d)localObject1).<init>("ISO8859_1", i4, (int[])localObject2, (String[])localObject3);
    b = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    localObject2 = new String[i4];
    localObject2[0] = "ISO-8859-2";
    ((d)localObject1).<init>("ISO8859_2", i3, i1, (String[])localObject2);
    c = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    localObject3 = new String[i4];
    localObject3[0] = "ISO-8859-3";
    ((d)localObject1).<init>("ISO8859_3", i2, 5, (String[])localObject3);
    d = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    localObject3 = new String[i4];
    localObject3[0] = "ISO-8859-4";
    ((d)localObject1).<init>("ISO8859_4", i1, 6, (String[])localObject3);
    e = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    String[] arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-5";
    ((d)localObject1).<init>("ISO8859_5", 5, 7, arrayOfString);
    f = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-6";
    ((d)localObject1).<init>("ISO8859_6", 6, 8, arrayOfString);
    g = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-7";
    ((d)localObject1).<init>("ISO8859_7", 7, 9, arrayOfString);
    h = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-8";
    ((d)localObject1).<init>("ISO8859_8", 8, 10, arrayOfString);
    i = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-9";
    ((d)localObject1).<init>("ISO8859_9", 9, 11, arrayOfString);
    j = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-10";
    ((d)localObject1).<init>("ISO8859_10", 10, 12, arrayOfString);
    k = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-11";
    ((d)localObject1).<init>("ISO8859_11", 11, 13, arrayOfString);
    l = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-13";
    ((d)localObject1).<init>("ISO8859_13", 12, 15, arrayOfString);
    m = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-14";
    ((d)localObject1).<init>("ISO8859_14", 13, 16, arrayOfString);
    n = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-15";
    ((d)localObject1).<init>("ISO8859_15", 14, 17, arrayOfString);
    o = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "ISO-8859-16";
    ((d)localObject1).<init>("ISO8859_16", 15, 18, arrayOfString);
    p = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "Shift_JIS";
    ((d)localObject1).<init>("SJIS", 16, 20, arrayOfString);
    q = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "windows-1250";
    ((d)localObject1).<init>("Cp1250", 17, 21, arrayOfString);
    r = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "windows-1251";
    ((d)localObject1).<init>("Cp1251", 18, 22, arrayOfString);
    s = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "windows-1252";
    ((d)localObject1).<init>("Cp1252", 19, 23, arrayOfString);
    t = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "windows-1256";
    ((d)localObject1).<init>("Cp1256", 20, 24, arrayOfString);
    u = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i3];
    arrayOfString[0] = "UTF-16BE";
    arrayOfString[i4] = "UnicodeBig";
    ((d)localObject1).<init>("UnicodeBigUnmarked", 21, 25, arrayOfString);
    v = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i4];
    arrayOfString[0] = "UTF-8";
    ((d)localObject1).<init>("UTF8", 22, 26, arrayOfString);
    w = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    localObject3 = new int[i3];
    Object tmp845_843 = localObject3;
    tmp845_843[0] = 27;
    tmp845_843[1] = 'ª';
    arrayOfString = new String[i4];
    arrayOfString[0] = "US-ASCII";
    ((d)localObject1).<init>("ASCII", 23, (int[])localObject3, arrayOfString);
    x = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    ((d)localObject1).<init>("Big5", 24, 28);
    y = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    arrayOfString = new String[i2];
    arrayOfString[0] = "GB2312";
    arrayOfString[i4] = "EUC_CN";
    arrayOfString[i3] = "GBK";
    ((d)localObject1).<init>("GB18030", 25, 29, arrayOfString);
    z = (d)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/d;
    int i5 = 26;
    arrayOfString = new String[i4];
    Object localObject4 = "EUC-KR";
    arrayOfString[0] = localObject4;
    ((d)localObject1).<init>("EUC_KR", i5, 30, arrayOfString);
    A = (d)localObject1;
    int i6 = 27;
    localObject1 = new d[i6];
    d locald = a;
    localObject1[0] = locald;
    locald = b;
    localObject1[i4] = locald;
    locald = c;
    localObject1[i3] = locald;
    locald = d;
    localObject1[i2] = locald;
    locald = e;
    localObject1[i1] = locald;
    localObject2 = f;
    localObject1[5] = localObject2;
    localObject2 = g;
    localObject1[6] = localObject2;
    localObject2 = h;
    localObject1[7] = localObject2;
    localObject2 = i;
    localObject1[8] = localObject2;
    localObject2 = j;
    localObject1[9] = localObject2;
    localObject2 = k;
    localObject1[10] = localObject2;
    localObject2 = l;
    localObject1[11] = localObject2;
    localObject2 = m;
    localObject1[12] = localObject2;
    localObject2 = n;
    localObject1[13] = localObject2;
    localObject2 = o;
    localObject1[14] = localObject2;
    localObject2 = p;
    localObject1[15] = localObject2;
    localObject2 = q;
    localObject1[16] = localObject2;
    localObject2 = r;
    localObject1[17] = localObject2;
    localObject2 = s;
    localObject1[18] = localObject2;
    localObject2 = t;
    localObject1[19] = localObject2;
    localObject2 = u;
    localObject1[20] = localObject2;
    localObject2 = v;
    localObject1[21] = localObject2;
    localObject2 = w;
    localObject1[22] = localObject2;
    localObject2 = x;
    localObject1[23] = localObject2;
    localObject2 = y;
    localObject1[24] = localObject2;
    localObject2 = z;
    localObject1[25] = localObject2;
    localObject2 = A;
    localObject1[26] = localObject2;
    F = (d[])localObject1;
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    B = (Map)localObject1;
    localObject1 = new java/util/HashMap;
    ((HashMap)localObject1).<init>();
    C = (Map)localObject1;
    localObject2 = values();
    int i7 = localObject2.length;
    int i8 = 0;
    locald = null;
    while (i8 < i7)
    {
      arrayOfString = localObject2[i8];
      localObject4 = D;
      i4 = localObject4.length;
      i6 = 0;
      localObject1 = null;
      Map localMap;
      Integer localInteger;
      while (i6 < i4)
      {
        i3 = localObject4[i6];
        localMap = B;
        localInteger = Integer.valueOf(i3);
        localMap.put(localInteger, arrayOfString);
        i6 += 1;
      }
      localObject1 = C;
      localObject4 = arrayOfString.name();
      ((Map)localObject1).put(localObject4, arrayOfString);
      localObject4 = E;
      i4 = localObject4.length;
      i6 = 0;
      localObject1 = null;
      while (i6 < i4)
      {
        localInteger = localObject4[i6];
        localMap = C;
        localMap.put(localInteger, arrayOfString);
        i6 += 1;
      }
      i6 = i8 + 1;
      i8 = i6;
    }
  }
  
  private d(int paramInt1)
  {
    this(arrayOfInt, arrayOfString);
  }
  
  private d(int paramInt1, String... paramVarArgs)
  {
    int[] arrayOfInt = new int[1];
    arrayOfInt[0] = paramInt1;
    D = arrayOfInt;
    E = paramVarArgs;
  }
  
  private d(int[] paramArrayOfInt, String... paramVarArgs)
  {
    D = paramArrayOfInt;
    E = paramVarArgs;
  }
  
  public static d a(int paramInt)
  {
    if (paramInt >= 0)
    {
      int i1 = 900;
      if (paramInt < i1) {}
    }
    else
    {
      throw FormatException.a();
    }
    Map localMap = B;
    Integer localInteger = Integer.valueOf(paramInt);
    return (d)localMap.get(localInteger);
  }
  
  public static d a(String paramString)
  {
    return (d)C.get(paramString);
  }
  
  public int a()
  {
    return D[0];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */