package in.juspay.widget.qrscanner.com.google.zxing.common;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.f;

public class h
  extends in.juspay.widget.qrscanner.com.google.zxing.b
{
  private static final byte[] a = new byte[0];
  private byte[] b;
  private final int[] c;
  
  public h(f paramf)
  {
    super(paramf);
    Object localObject = a;
    b = ((byte[])localObject);
    localObject = new int[32];
    c = ((int[])localObject);
  }
  
  private static int a(int[] paramArrayOfInt)
  {
    int i = 0;
    int j = paramArrayOfInt.length;
    int k = 0;
    int m = 0;
    int n = 0;
    int i1 = 0;
    while (k < j)
    {
      i2 = paramArrayOfInt[k];
      if (i2 > m)
      {
        m = paramArrayOfInt[k];
        n = k;
      }
      i2 = paramArrayOfInt[k];
      if (i2 > i1) {
        i1 = paramArrayOfInt[k];
      }
      k += 1;
    }
    k = 0;
    int i2 = 0;
    int i3;
    if (i < j)
    {
      m = i - n;
      i3 = paramArrayOfInt[i] * m;
      m *= i3;
      if (m <= k) {
        break label256;
      }
    }
    for (k = i;; k = i2)
    {
      i += 1;
      i2 = k;
      k = m;
      break;
      if (n > i2) {}
      for (;;)
      {
        m = n - i2;
        k = j / 16;
        if (m <= k) {
          throw NotFoundException.a();
        }
        j = n + -1;
        k = -1;
        i = n + -1;
        if (i > i2)
        {
          m = i - i2;
          m *= m;
          i3 = n - i;
          m *= i3;
          i3 = paramArrayOfInt[i];
          i3 = i1 - i3;
          m *= i3;
          if (m <= k) {
            break label233;
          }
        }
        for (k = i;; k = j)
        {
          i += -1;
          j = k;
          k = m;
          break;
          return j << 3;
          label233:
          m = k;
        }
        int i4 = i2;
        i2 = n;
        n = i4;
      }
      label256:
      m = k;
    }
  }
  
  private void a(int paramInt)
  {
    byte[] arrayOfByte = b;
    int i = arrayOfByte.length;
    if (i < paramInt)
    {
      arrayOfByte = new byte[paramInt];
      b = arrayOfByte;
    }
    i = 0;
    arrayOfByte = null;
    for (;;)
    {
      int j = 32;
      if (i >= j) {
        break;
      }
      int[] arrayOfInt = c;
      arrayOfInt[i] = 0;
      i += 1;
    }
  }
  
  public b b()
  {
    Object localObject = a();
    int i = ((f)localObject).b();
    int j = ((f)localObject).c();
    b localb = new in/juspay/widget/qrscanner/com/google/zxing/common/b;
    localb.<init>(i, j);
    a(i);
    int[] arrayOfInt = c;
    int k = 1;
    int n;
    for (int m = k;; m = k)
    {
      k = 5;
      if (m >= k) {
        break;
      }
      k = j * m / 5;
      byte[] arrayOfByte = b;
      arrayOfByte = ((f)localObject).a(k, arrayOfByte);
      n = i * 4 / 5;
      k = i / 5;
      while (k < n)
      {
        int i1 = (arrayOfByte[k] & 0xFF) >> 3;
        int i2 = arrayOfInt[i1] + 1;
        arrayOfInt[i1] = i2;
        k += 1;
      }
      k = m + 1;
    }
    int i3 = a(arrayOfInt);
    localObject = ((f)localObject).a();
    for (m = 0; m < j; m = k)
    {
      int i4 = m * i;
      k = 0;
      while (k < i)
      {
        n = i4 + k;
        n = localObject[n] & 0xFF;
        if (n < i3) {
          localb.b(k, m);
        }
        k += 1;
      }
      k = m + 1;
    }
    return localb;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */