package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

public final class c
{
  private final a a;
  
  public c(a parama)
  {
    a = parama;
  }
  
  private int[] a(b paramb)
  {
    int i = 0;
    int j = 1;
    int k = paramb.b();
    int[] arrayOfInt;
    if (k == j)
    {
      arrayOfInt = new int[j];
      j = paramb.a(j);
      arrayOfInt[0] = j;
    }
    for (Object localObject = arrayOfInt;; localObject = arrayOfInt)
    {
      return (int[])localObject;
      arrayOfInt = new int[k];
      for (;;)
      {
        a locala = a;
        int m = locala.c();
        if ((j >= m) || (i >= k)) {
          break;
        }
        m = paramb.b(j);
        if (m == 0)
        {
          locala = a;
          m = locala.c(j);
          arrayOfInt[i] = m;
          i += 1;
        }
        j += 1;
      }
      if (i != k)
      {
        localObject = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;
        ((ReedSolomonException)localObject).<init>("Error locator degree does not match number of roots");
        throw ((Throwable)localObject);
      }
    }
  }
  
  private int[] a(b paramb, int[] paramArrayOfInt)
  {
    int i = paramArrayOfInt.length;
    int[] arrayOfInt = new int[i];
    int j = 0;
    a locala1;
    int k;
    int m;
    int n;
    label44:
    label97:
    a locala2;
    if (j < i)
    {
      locala1 = a;
      k = paramArrayOfInt[j];
      m = locala1.c(k);
      k = 1;
      n = 0;
      if (n < i)
      {
        if (j == n) {
          break label249;
        }
        locala1 = a;
        int i1 = paramArrayOfInt[n];
        i2 = locala1.c(i1, m);
        i1 = i2 & 0x1;
        if (i1 == 0)
        {
          i2 |= 0x1;
          locala2 = a;
        }
      }
    }
    label249:
    for (int i2 = locala2.c(k, i2);; i2 = k)
    {
      n += 1;
      k = i2;
      break label44;
      i2 &= 0xFFFFFFFE;
      break label97;
      locala1 = a;
      n = paramb.b(m);
      locala2 = a;
      k = locala2.c(k);
      i2 = locala1.c(n, k);
      arrayOfInt[j] = i2;
      locala1 = a;
      i2 = locala1.d();
      if (i2 != 0)
      {
        locala1 = a;
        k = arrayOfInt[j];
        i2 = locala1.c(k, m);
        arrayOfInt[j] = i2;
      }
      i2 = j + 1;
      j = i2;
      break;
      return arrayOfInt;
    }
  }
  
  private b[] a(b paramb1, b paramb2, int paramInt)
  {
    int i = paramb1.b();
    int j = paramb2.b();
    if (i < j) {}
    for (;;)
    {
      Object localObject1 = a.a();
      Object localObject2 = a.b();
      for (;;)
      {
        int k = paramb1.b();
        int m = paramInt / 2;
        if (k < m) {
          break;
        }
        boolean bool1 = paramb1.c();
        if (bool1)
        {
          localObject2 = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;
          ((ReedSolomonException)localObject2).<init>("r_{i-1} was zero");
          throw ((Throwable)localObject2);
        }
        localObject3 = a.a();
        m = paramb1.b();
        m = paramb1.a(m);
        a locala1 = a;
        int n = locala1.c(m);
        Object localObject4 = localObject3;
        b localb2;
        for (localObject3 = paramb2;; localObject3 = ((b)localObject3).a(localb2))
        {
          int i1 = ((b)localObject3).b();
          int i3 = paramb1.b();
          if (i1 < i3) {
            break;
          }
          boolean bool2 = ((b)localObject3).c();
          if (bool2) {
            break;
          }
          int i2 = ((b)localObject3).b();
          i3 = paramb1.b();
          i2 -= i3;
          a locala2 = a;
          int i4 = ((b)localObject3).b();
          i4 = ((b)localObject3).a(i4);
          i3 = locala2.c(i4, n);
          b localb1 = a.a(i2, i3);
          localObject4 = ((b)localObject4).a(localb1);
          localb2 = paramb1.a(i2, i3);
        }
        localObject4 = ((b)localObject4).b((b)localObject2);
        localObject1 = ((b)localObject4).a((b)localObject1);
        m = ((b)localObject3).b();
        n = paramb1.b();
        if (m >= n)
        {
          localObject2 = new java/lang/IllegalStateException;
          ((IllegalStateException)localObject2).<init>("Division algorithm failed to reduce polynomial?");
          throw ((Throwable)localObject2);
        }
        paramb2 = paramb1;
        paramb1 = (b)localObject3;
        localObject5 = localObject2;
        localObject2 = localObject1;
        localObject1 = localObject5;
      }
      j = ((b)localObject2).a(0);
      if (j == 0)
      {
        localObject2 = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;
        ((ReedSolomonException)localObject2).<init>("sigmaTilde(0) was zero");
        throw ((Throwable)localObject2);
      }
      j = a.c(j);
      localObject2 = ((b)localObject2).c(j);
      localObject1 = paramb1.c(j);
      Object localObject3 = new b[2];
      localObject3[0] = localObject2;
      localObject3[1] = localObject1;
      return (b[])localObject3;
      Object localObject5 = paramb2;
      paramb2 = paramb1;
      paramb1 = (b)localObject5;
    }
  }
  
  public void a(int[] paramArrayOfInt, int paramInt)
  {
    int i = 1;
    int j = 0;
    Object localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
    Object localObject2 = a;
    ((b)localObject1).<init>((a)localObject2, paramArrayOfInt);
    int[] arrayOfInt = new int[paramInt];
    int k = 0;
    Object localObject3 = null;
    int m = i;
    while (k < paramInt)
    {
      a locala1 = a;
      a locala2 = a;
      int n = locala2.d() + k;
      int i1 = locala1.a(n);
      i1 = ((b)localObject1).b(i1);
      n = arrayOfInt.length + -1 - k;
      arrayOfInt[n] = i1;
      if (i1 != 0)
      {
        m = 0;
        localObject2 = null;
      }
      k += 1;
    }
    if (m != 0) {
      return;
    }
    localObject2 = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
    localObject3 = a;
    ((b)localObject2).<init>((a)localObject3, arrayOfInt);
    localObject3 = a.a(paramInt, i);
    localObject2 = a((b)localObject3, (b)localObject2, paramInt);
    localObject3 = localObject2[0];
    localObject2 = localObject2[i];
    localObject3 = a((b)localObject3);
    localObject2 = a((b)localObject2, (int[])localObject3);
    for (;;)
    {
      i = localObject3.length;
      if (j >= i) {
        break;
      }
      i = paramArrayOfInt.length + -1;
      localObject1 = a;
      int i2 = localObject3[j];
      int i3 = ((a)localObject1).b(i2);
      i -= i3;
      if (i < 0)
      {
        localObject2 = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/ReedSolomonException;
        ((ReedSolomonException)localObject2).<init>("Bad error location");
        throw ((Throwable)localObject2);
      }
      i3 = paramArrayOfInt[i];
      i2 = localObject2[j];
      i3 = a.b(i3, i2);
      paramArrayOfInt[i] = i3;
      j += 1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */