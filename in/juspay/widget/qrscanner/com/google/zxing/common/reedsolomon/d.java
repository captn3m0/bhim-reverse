package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

import java.util.ArrayList;
import java.util.List;

public final class d
{
  private final a a;
  private final List b;
  
  public d(a parama)
  {
    a = parama;
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    b = ((List)localObject);
    localObject = b;
    b localb = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
    int[] arrayOfInt = new int[i];
    arrayOfInt[0] = i;
    localb.<init>(parama, arrayOfInt);
    ((List)localObject).add(localb);
  }
  
  private b a(int paramInt)
  {
    int i = 1;
    Object localObject1 = b;
    int j = ((List)localObject1).size();
    if (paramInt >= j)
    {
      localObject1 = b;
      int k = b.size() + -1;
      localObject1 = (b)((List)localObject1).get(k);
      k = b.size();
      Object localObject2 = localObject1;
      j = k;
      while (j <= paramInt)
      {
        Object localObject3 = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
        a locala1 = a;
        int m = 2;
        int[] arrayOfInt = new int[m];
        arrayOfInt[0] = i;
        a locala2 = a;
        int n = j + -1;
        a locala3 = a;
        int i1 = locala3.d();
        n += i1;
        int i2 = locala2.a(n);
        arrayOfInt[i] = i2;
        ((b)localObject3).<init>(locala1, arrayOfInt);
        localObject2 = ((b)localObject2).b((b)localObject3);
        localObject3 = b;
        ((List)localObject3).add(localObject2);
        j += 1;
      }
    }
    return (b)b.get(paramInt);
  }
  
  public void a(int[] paramArrayOfInt, int paramInt)
  {
    int i = 1;
    if (paramInt == 0)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("No error correction bytes");
      throw ((Throwable)localObject);
    }
    int j = paramArrayOfInt.length;
    int k = j - paramInt;
    if (k <= 0)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("No data bytes provided");
      throw ((Throwable)localObject);
    }
    Object localObject = a(paramInt);
    int[] arrayOfInt = new int[k];
    System.arraycopy(paramArrayOfInt, 0, arrayOfInt, 0, k);
    b localb = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
    a locala = a;
    localb.<init>(locala, arrayOfInt);
    arrayOfInt = localb.a(paramInt, i).c(localObject)[i].a();
    j = arrayOfInt.length;
    int m = paramInt - j;
    j = 0;
    localObject = null;
    while (j < m)
    {
      int n = k + j;
      paramArrayOfInt[n] = 0;
      j += 1;
    }
    j = k + m;
    k = arrayOfInt.length;
    System.arraycopy(arrayOfInt, 0, paramArrayOfInt, j, k);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */