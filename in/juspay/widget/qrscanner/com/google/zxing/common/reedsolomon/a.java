package in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon;

public final class a
{
  public static final a a;
  public static final a b;
  public static final a c;
  public static final a d;
  public static final a e;
  public static final a f;
  public static final a g = f;
  public static final a h = c;
  private final int[] i;
  private final int[] j;
  private final b k;
  private final b l;
  private final int m;
  private final int n;
  private final int o;
  
  static
  {
    int i1 = 256;
    int i2 = 1;
    a locala = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;
    locala.<init>(4201, 4096, i2);
    a = locala;
    locala = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;
    locala.<init>(1033, 1024, i2);
    b = locala;
    locala = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;
    locala.<init>(67, 64, i2);
    c = locala;
    locala = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;
    locala.<init>(19, 16, i2);
    d = locala;
    locala = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;
    locala.<init>(285, i1, 0);
    e = locala;
    locala = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a;
    locala.<init>(301, i1, i2);
    f = locala;
  }
  
  public a(int paramInt1, int paramInt2, int paramInt3)
  {
    n = paramInt1;
    m = paramInt2;
    o = paramInt3;
    Object localObject = new int[paramInt2];
    i = ((int[])localObject);
    localObject = new int[paramInt2];
    j = ((int[])localObject);
    int i2 = 0;
    int[] arrayOfInt1 = null;
    int i3 = i1;
    int[] arrayOfInt2;
    int i4;
    while (i2 < paramInt2)
    {
      arrayOfInt2 = i;
      arrayOfInt2[i2] = i3;
      i3 *= 2;
      if (i3 >= paramInt2)
      {
        i3 ^= paramInt1;
        i4 = paramInt2 + -1;
        i3 &= i4;
      }
      i2 += 1;
    }
    i3 = 0;
    localObject = null;
    for (;;)
    {
      i2 = paramInt2 + -1;
      if (i3 >= i2) {
        break;
      }
      arrayOfInt1 = j;
      arrayOfInt2 = i;
      i4 = arrayOfInt2[i3];
      arrayOfInt1[i4] = i3;
      i3 += 1;
    }
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
    arrayOfInt1 = new int[i1];
    arrayOfInt1[0] = 0;
    ((b)localObject).<init>(this, arrayOfInt1);
    k = ((b)localObject);
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
    arrayOfInt1 = new int[i1];
    arrayOfInt1[0] = i1;
    ((b)localObject).<init>(this, arrayOfInt1);
    l = ((b)localObject);
  }
  
  static int b(int paramInt1, int paramInt2)
  {
    return paramInt1 ^ paramInt2;
  }
  
  int a(int paramInt)
  {
    return i[paramInt];
  }
  
  b a()
  {
    return k;
  }
  
  b a(int paramInt1, int paramInt2)
  {
    Object localObject;
    if (paramInt1 < 0)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>();
      throw ((Throwable)localObject);
    }
    if (paramInt2 == 0) {
      localObject = k;
    }
    for (;;)
    {
      return (b)localObject;
      int[] arrayOfInt = new int[paramInt1 + 1];
      arrayOfInt[0] = paramInt2;
      localObject = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/b;
      ((b)localObject).<init>(this, arrayOfInt);
    }
  }
  
  int b(int paramInt)
  {
    if (paramInt == 0)
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>();
      throw localIllegalArgumentException;
    }
    return j[paramInt];
  }
  
  b b()
  {
    return l;
  }
  
  public int c()
  {
    return m;
  }
  
  int c(int paramInt)
  {
    if (paramInt == 0)
    {
      localObject = new java/lang/ArithmeticException;
      ((ArithmeticException)localObject).<init>();
      throw ((Throwable)localObject);
    }
    Object localObject = i;
    int i1 = m;
    int i2 = j[paramInt];
    i1 = i1 - i2 + -1;
    return localObject[i1];
  }
  
  int c(int paramInt1, int paramInt2)
  {
    int i1;
    int[] arrayOfInt1;
    if ((paramInt1 == 0) || (paramInt2 == 0))
    {
      i1 = 0;
      arrayOfInt1 = null;
    }
    for (;;)
    {
      return i1;
      arrayOfInt1 = i;
      int[] arrayOfInt2 = j;
      int i2 = arrayOfInt2[paramInt1];
      int[] arrayOfInt3 = j;
      int i3 = arrayOfInt3[paramInt2];
      i2 += i3;
      i3 = m + -1;
      i2 %= i3;
      i1 = arrayOfInt1[i2];
    }
  }
  
  public int d()
  {
    return o;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append("GF(0x");
    String str = Integer.toHexString(n);
    localStringBuilder = localStringBuilder.append(str).append(',');
    int i1 = m;
    return i1 + ')';
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */