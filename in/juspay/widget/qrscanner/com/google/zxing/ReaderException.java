package in.juspay.widget.qrscanner.com.google.zxing;

public abstract class ReaderException
  extends Exception
{
  protected static final boolean a;
  protected static final StackTraceElement[] b;
  
  static
  {
    String str = System.getProperty("surefire.test.class.path");
    boolean bool;
    if (str != null) {
      bool = true;
    }
    for (;;)
    {
      a = bool;
      b = new StackTraceElement[0];
      return;
      bool = false;
      str = null;
    }
  }
  
  public final Throwable fillInStackTrace()
  {
    return null;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/ReaderException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */