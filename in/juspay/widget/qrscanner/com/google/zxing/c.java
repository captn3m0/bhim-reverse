package in.juspay.widget.qrscanner.com.google.zxing;

public final class c
{
  private final b a;
  private in.juspay.widget.qrscanner.com.google.zxing.common.b b;
  
  public c(b paramb)
  {
    if (paramb == null)
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Binarizer must be non-null.");
      throw localIllegalArgumentException;
    }
    a = paramb;
  }
  
  public in.juspay.widget.qrscanner.com.google.zxing.common.b a()
  {
    in.juspay.widget.qrscanner.com.google.zxing.common.b localb = b;
    if (localb == null)
    {
      localb = a.b();
      b = localb;
    }
    return b;
  }
  
  public String toString()
  {
    try
    {
      localObject = a();
      localObject = ((in.juspay.widget.qrscanner.com.google.zxing.common.b)localObject).toString();
    }
    catch (NotFoundException localNotFoundException)
    {
      for (;;)
      {
        Object localObject;
        String str = "";
      }
    }
    return (String)localObject;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */