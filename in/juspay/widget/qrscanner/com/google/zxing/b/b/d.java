package in.juspay.widget.qrscanner.com.google.zxing.b.b;

import in.juspay.widget.qrscanner.com.google.zxing.m;

public final class d
  extends m
{
  private final float a;
  private final int b;
  
  d(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    this(paramFloat1, paramFloat2, paramFloat3, 1);
  }
  
  private d(float paramFloat1, float paramFloat2, float paramFloat3, int paramInt)
  {
    super(paramFloat1, paramFloat2);
    a = paramFloat3;
    b = paramInt;
  }
  
  boolean a(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    boolean bool1 = false;
    float f1 = b();
    f1 = Math.abs(paramFloat2 - f1);
    boolean bool2 = f1 < paramFloat1;
    if (!bool2)
    {
      f1 = a();
      f1 = Math.abs(paramFloat3 - f1);
      bool2 = f1 < paramFloat1;
      if (!bool2)
      {
        f1 = a;
        f1 = Math.abs(paramFloat1 - f1);
        float f2 = 1.0F;
        boolean bool3 = f1 < f2;
        if (bool3)
        {
          f2 = a;
          bool2 = f1 < f2;
          if (bool2) {}
        }
        else
        {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
  
  d b(float paramFloat1, float paramFloat2, float paramFloat3)
  {
    int i = b + 1;
    float f1 = b;
    float f2 = a();
    f1 = f1 * f2 + paramFloat2;
    f2 = i;
    f1 /= f2;
    f2 = b;
    float f3 = b();
    f2 = f2 * f3 + paramFloat1;
    f3 = i;
    f2 /= f3;
    f3 = b;
    float f4 = a;
    f3 = f3 * f4 + paramFloat3;
    f4 = i;
    f3 /= f4;
    d locald = new in/juspay/widget/qrscanner/com/google/zxing/b/b/d;
    locald.<init>(f1, f2, f3, i);
    return locald;
  }
  
  public float c()
  {
    return a;
  }
  
  int d()
  {
    return b;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/b/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */