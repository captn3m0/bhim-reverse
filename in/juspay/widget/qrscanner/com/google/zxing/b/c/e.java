package in.juspay.widget.qrscanner.com.google.zxing.b.c;

import in.juspay.widget.qrscanner.com.google.zxing.WriterException;
import in.juspay.widget.qrscanner.com.google.zxing.b.a.j;
import in.juspay.widget.qrscanner.com.google.zxing.common.a;

final class e
{
  private static final int[][] a;
  private static final int[][] b;
  private static final int[][] c;
  private static final int[][] d;
  
  static
  {
    int i = 1;
    int j = 5;
    int k = 2;
    int m = 7;
    int[][] arrayOfInt = new int[m][];
    int[] arrayOfInt1 = new int[m];
    int[] tmp22_20 = arrayOfInt1;
    int[] tmp23_22 = tmp22_20;
    int[] tmp23_22 = tmp22_20;
    tmp23_22[0] = 1;
    tmp23_22[1] = 1;
    int[] tmp30_23 = tmp23_22;
    int[] tmp30_23 = tmp23_22;
    tmp30_23[2] = 1;
    tmp30_23[3] = 1;
    tmp30_23[4] = 1;
    int[] tmp40_30 = tmp30_23;
    tmp40_30[5] = 1;
    tmp40_30[6] = 1;
    arrayOfInt[0] = arrayOfInt1;
    arrayOfInt1 = new int[m];
    int[] tmp61_59 = arrayOfInt1;
    int[] tmp62_61 = tmp61_59;
    int[] tmp62_61 = tmp61_59;
    tmp62_61[0] = 1;
    tmp62_61[1] = 0;
    int[] tmp69_62 = tmp62_61;
    int[] tmp69_62 = tmp62_61;
    tmp69_62[2] = 0;
    tmp69_62[3] = 0;
    tmp69_62[4] = 0;
    int[] tmp79_69 = tmp69_62;
    tmp79_69[5] = 0;
    tmp79_69[6] = 1;
    arrayOfInt[i] = arrayOfInt1;
    arrayOfInt1 = new int[m];
    int[] tmp100_98 = arrayOfInt1;
    int[] tmp101_100 = tmp100_98;
    int[] tmp101_100 = tmp100_98;
    tmp101_100[0] = 1;
    tmp101_100[1] = 0;
    int[] tmp108_101 = tmp101_100;
    int[] tmp108_101 = tmp101_100;
    tmp108_101[2] = 1;
    tmp108_101[3] = 1;
    tmp108_101[4] = 1;
    int[] tmp118_108 = tmp108_101;
    tmp118_108[5] = 0;
    tmp118_108[6] = 1;
    arrayOfInt[k] = arrayOfInt1;
    int[] arrayOfInt2 = new int[m];
    int[] tmp139_137 = arrayOfInt2;
    int[] tmp140_139 = tmp139_137;
    int[] tmp140_139 = tmp139_137;
    tmp140_139[0] = 1;
    tmp140_139[1] = 0;
    int[] tmp147_140 = tmp140_139;
    int[] tmp147_140 = tmp140_139;
    tmp147_140[2] = 1;
    tmp147_140[3] = 1;
    tmp147_140[4] = 1;
    int[] tmp157_147 = tmp147_140;
    tmp157_147[5] = 0;
    tmp157_147[6] = 1;
    arrayOfInt[3] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp178_176 = arrayOfInt2;
    int[] tmp179_178 = tmp178_176;
    int[] tmp179_178 = tmp178_176;
    tmp179_178[0] = 1;
    tmp179_178[1] = 0;
    int[] tmp186_179 = tmp179_178;
    int[] tmp186_179 = tmp179_178;
    tmp186_179[2] = 1;
    tmp186_179[3] = 1;
    tmp186_179[4] = 1;
    int[] tmp196_186 = tmp186_179;
    tmp196_186[5] = 0;
    tmp196_186[6] = 1;
    arrayOfInt[4] = arrayOfInt2;
    arrayOfInt1 = new int[m];
    int[] tmp217_215 = arrayOfInt1;
    int[] tmp218_217 = tmp217_215;
    int[] tmp218_217 = tmp217_215;
    tmp218_217[0] = 1;
    tmp218_217[1] = 0;
    int[] tmp225_218 = tmp218_217;
    int[] tmp225_218 = tmp218_217;
    tmp225_218[2] = 0;
    tmp225_218[3] = 0;
    tmp225_218[4] = 0;
    int[] tmp235_225 = tmp225_218;
    tmp235_225[5] = 0;
    tmp235_225[6] = 1;
    arrayOfInt[j] = arrayOfInt1;
    arrayOfInt2 = new int[m];
    int[] tmp256_254 = arrayOfInt2;
    int[] tmp257_256 = tmp256_254;
    int[] tmp257_256 = tmp256_254;
    tmp257_256[0] = 1;
    tmp257_256[1] = 1;
    int[] tmp264_257 = tmp257_256;
    int[] tmp264_257 = tmp257_256;
    tmp264_257[2] = 1;
    tmp264_257[3] = 1;
    tmp264_257[4] = 1;
    int[] tmp274_264 = tmp264_257;
    tmp274_264[5] = 1;
    tmp274_264[6] = 1;
    arrayOfInt[6] = arrayOfInt2;
    a = arrayOfInt;
    arrayOfInt = new int[j][];
    arrayOfInt1 = new int[j];
    int[] tmp307_305 = arrayOfInt1;
    int[] tmp308_307 = tmp307_305;
    int[] tmp308_307 = tmp307_305;
    tmp308_307[0] = 1;
    tmp308_307[1] = 1;
    tmp308_307[2] = 1;
    int[] tmp318_308 = tmp308_307;
    tmp318_308[3] = 1;
    tmp318_308[4] = 1;
    arrayOfInt[0] = arrayOfInt1;
    arrayOfInt1 = new int[j];
    int[] tmp338_336 = arrayOfInt1;
    int[] tmp339_338 = tmp338_336;
    int[] tmp339_338 = tmp338_336;
    tmp339_338[0] = 1;
    tmp339_338[1] = 0;
    tmp339_338[2] = 0;
    int[] tmp349_339 = tmp339_338;
    tmp349_339[3] = 0;
    tmp349_339[4] = 1;
    arrayOfInt[i] = arrayOfInt1;
    arrayOfInt1 = new int[j];
    int[] tmp369_367 = arrayOfInt1;
    int[] tmp370_369 = tmp369_367;
    int[] tmp370_369 = tmp369_367;
    tmp370_369[0] = 1;
    tmp370_369[1] = 0;
    tmp370_369[2] = 1;
    int[] tmp380_370 = tmp370_369;
    tmp380_370[3] = 0;
    tmp380_370[4] = 1;
    arrayOfInt[k] = arrayOfInt1;
    arrayOfInt2 = new int[j];
    int[] tmp400_398 = arrayOfInt2;
    int[] tmp401_400 = tmp400_398;
    int[] tmp401_400 = tmp400_398;
    tmp401_400[0] = 1;
    tmp401_400[1] = 0;
    tmp401_400[2] = 0;
    int[] tmp411_401 = tmp401_400;
    tmp411_401[3] = 0;
    tmp411_401[4] = 1;
    arrayOfInt[3] = arrayOfInt2;
    arrayOfInt2 = new int[j];
    int[] tmp431_429 = arrayOfInt2;
    int[] tmp432_431 = tmp431_429;
    int[] tmp432_431 = tmp431_429;
    tmp432_431[0] = 1;
    tmp432_431[1] = 1;
    tmp432_431[2] = 1;
    int[] tmp442_432 = tmp432_431;
    tmp442_432[3] = 1;
    tmp442_432[4] = 1;
    arrayOfInt[4] = arrayOfInt2;
    b = arrayOfInt;
    arrayOfInt = new int[40][];
    arrayOfInt1 = new int[m];
    int[] tmp474_472 = arrayOfInt1;
    int[] tmp475_474 = tmp474_472;
    int[] tmp475_474 = tmp474_472;
    tmp475_474[0] = -1;
    tmp475_474[1] = -1;
    int[] tmp482_475 = tmp475_474;
    int[] tmp482_475 = tmp475_474;
    tmp482_475[2] = -1;
    tmp482_475[3] = -1;
    tmp482_475[4] = -1;
    int[] tmp492_482 = tmp482_475;
    tmp492_482[5] = -1;
    tmp492_482[6] = -1;
    arrayOfInt[0] = arrayOfInt1;
    arrayOfInt1 = new int[m];
    int[] tmp513_511 = arrayOfInt1;
    int[] tmp514_513 = tmp513_511;
    int[] tmp514_513 = tmp513_511;
    tmp514_513[0] = 6;
    tmp514_513[1] = 18;
    int[] tmp523_514 = tmp514_513;
    int[] tmp523_514 = tmp514_513;
    tmp523_514[2] = -1;
    tmp523_514[3] = -1;
    tmp523_514[4] = -1;
    int[] tmp533_523 = tmp523_514;
    tmp533_523[5] = -1;
    tmp533_523[6] = -1;
    arrayOfInt[i] = arrayOfInt1;
    arrayOfInt1 = new int[m];
    int[] tmp554_552 = arrayOfInt1;
    int[] tmp555_554 = tmp554_552;
    int[] tmp555_554 = tmp554_552;
    tmp555_554[0] = 6;
    tmp555_554[1] = 22;
    int[] tmp564_555 = tmp555_554;
    int[] tmp564_555 = tmp555_554;
    tmp564_555[2] = -1;
    tmp564_555[3] = -1;
    tmp564_555[4] = -1;
    int[] tmp574_564 = tmp564_555;
    tmp574_564[5] = -1;
    tmp574_564[6] = -1;
    arrayOfInt[k] = arrayOfInt1;
    arrayOfInt2 = new int[m];
    int[] tmp595_593 = arrayOfInt2;
    int[] tmp596_595 = tmp595_593;
    int[] tmp596_595 = tmp595_593;
    tmp596_595[0] = 6;
    tmp596_595[1] = 26;
    int[] tmp605_596 = tmp596_595;
    int[] tmp605_596 = tmp596_595;
    tmp605_596[2] = -1;
    tmp605_596[3] = -1;
    tmp605_596[4] = -1;
    int[] tmp615_605 = tmp605_596;
    tmp615_605[5] = -1;
    tmp615_605[6] = -1;
    arrayOfInt[3] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp636_634 = arrayOfInt2;
    int[] tmp637_636 = tmp636_634;
    int[] tmp637_636 = tmp636_634;
    tmp637_636[0] = 6;
    tmp637_636[1] = 30;
    int[] tmp646_637 = tmp637_636;
    int[] tmp646_637 = tmp637_636;
    tmp646_637[2] = -1;
    tmp646_637[3] = -1;
    tmp646_637[4] = -1;
    int[] tmp656_646 = tmp646_637;
    tmp656_646[5] = -1;
    tmp656_646[6] = -1;
    arrayOfInt[4] = arrayOfInt2;
    arrayOfInt1 = new int[m];
    int[] tmp677_675 = arrayOfInt1;
    int[] tmp678_677 = tmp677_675;
    int[] tmp678_677 = tmp677_675;
    tmp678_677[0] = 6;
    tmp678_677[1] = 34;
    int[] tmp687_678 = tmp678_677;
    int[] tmp687_678 = tmp678_677;
    tmp687_678[2] = -1;
    tmp687_678[3] = -1;
    tmp687_678[4] = -1;
    int[] tmp697_687 = tmp687_678;
    tmp697_687[5] = -1;
    tmp697_687[6] = -1;
    arrayOfInt[j] = arrayOfInt1;
    arrayOfInt2 = new int[m];
    int[] tmp718_716 = arrayOfInt2;
    int[] tmp719_718 = tmp718_716;
    int[] tmp719_718 = tmp718_716;
    tmp719_718[0] = 6;
    tmp719_718[1] = 22;
    int[] tmp728_719 = tmp719_718;
    int[] tmp728_719 = tmp719_718;
    tmp728_719[2] = 38;
    tmp728_719[3] = -1;
    tmp728_719[4] = -1;
    int[] tmp739_728 = tmp728_719;
    tmp739_728[5] = -1;
    tmp739_728[6] = -1;
    arrayOfInt[6] = arrayOfInt2;
    arrayOfInt1 = new int[m];
    int[] tmp761_759 = arrayOfInt1;
    int[] tmp762_761 = tmp761_759;
    int[] tmp762_761 = tmp761_759;
    tmp762_761[0] = 6;
    tmp762_761[1] = 24;
    int[] tmp771_762 = tmp762_761;
    int[] tmp771_762 = tmp762_761;
    tmp771_762[2] = 42;
    tmp771_762[3] = -1;
    tmp771_762[4] = -1;
    int[] tmp782_771 = tmp771_762;
    tmp782_771[5] = -1;
    tmp782_771[6] = -1;
    arrayOfInt[m] = arrayOfInt1;
    arrayOfInt2 = new int[m];
    int[] tmp803_801 = arrayOfInt2;
    int[] tmp804_803 = tmp803_801;
    int[] tmp804_803 = tmp803_801;
    tmp804_803[0] = 6;
    tmp804_803[1] = 26;
    int[] tmp813_804 = tmp804_803;
    int[] tmp813_804 = tmp804_803;
    tmp813_804[2] = 46;
    tmp813_804[3] = -1;
    tmp813_804[4] = -1;
    int[] tmp824_813 = tmp813_804;
    tmp824_813[5] = -1;
    tmp824_813[6] = -1;
    arrayOfInt[8] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp846_844 = arrayOfInt2;
    int[] tmp847_846 = tmp846_844;
    int[] tmp847_846 = tmp846_844;
    tmp847_846[0] = 6;
    tmp847_846[1] = 28;
    int[] tmp856_847 = tmp847_846;
    int[] tmp856_847 = tmp847_846;
    tmp856_847[2] = 50;
    tmp856_847[3] = -1;
    tmp856_847[4] = -1;
    int[] tmp867_856 = tmp856_847;
    tmp867_856[5] = -1;
    tmp867_856[6] = -1;
    arrayOfInt[9] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp889_887 = arrayOfInt2;
    int[] tmp890_889 = tmp889_887;
    int[] tmp890_889 = tmp889_887;
    tmp890_889[0] = 6;
    tmp890_889[1] = 30;
    int[] tmp899_890 = tmp890_889;
    int[] tmp899_890 = tmp890_889;
    tmp899_890[2] = 54;
    tmp899_890[3] = -1;
    tmp899_890[4] = -1;
    int[] tmp910_899 = tmp899_890;
    tmp910_899[5] = -1;
    tmp910_899[6] = -1;
    arrayOfInt[10] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp932_930 = arrayOfInt2;
    int[] tmp933_932 = tmp932_930;
    int[] tmp933_932 = tmp932_930;
    tmp933_932[0] = 6;
    tmp933_932[1] = 32;
    int[] tmp942_933 = tmp933_932;
    int[] tmp942_933 = tmp933_932;
    tmp942_933[2] = 58;
    tmp942_933[3] = -1;
    tmp942_933[4] = -1;
    int[] tmp953_942 = tmp942_933;
    tmp953_942[5] = -1;
    tmp953_942[6] = -1;
    arrayOfInt[11] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp975_973 = arrayOfInt2;
    int[] tmp976_975 = tmp975_973;
    int[] tmp976_975 = tmp975_973;
    tmp976_975[0] = 6;
    tmp976_975[1] = 34;
    int[] tmp985_976 = tmp976_975;
    int[] tmp985_976 = tmp976_975;
    tmp985_976[2] = 62;
    tmp985_976[3] = -1;
    tmp985_976[4] = -1;
    int[] tmp996_985 = tmp985_976;
    tmp996_985[5] = -1;
    tmp996_985[6] = -1;
    arrayOfInt[12] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1018_1016 = arrayOfInt2;
    int[] tmp1019_1018 = tmp1018_1016;
    int[] tmp1019_1018 = tmp1018_1016;
    tmp1019_1018[0] = 6;
    tmp1019_1018[1] = 26;
    int[] tmp1028_1019 = tmp1019_1018;
    int[] tmp1028_1019 = tmp1019_1018;
    tmp1028_1019[2] = 46;
    tmp1028_1019[3] = 66;
    tmp1028_1019[4] = -1;
    int[] tmp1040_1028 = tmp1028_1019;
    tmp1040_1028[5] = -1;
    tmp1040_1028[6] = -1;
    arrayOfInt[13] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1062_1060 = arrayOfInt2;
    int[] tmp1063_1062 = tmp1062_1060;
    int[] tmp1063_1062 = tmp1062_1060;
    tmp1063_1062[0] = 6;
    tmp1063_1062[1] = 26;
    int[] tmp1072_1063 = tmp1063_1062;
    int[] tmp1072_1063 = tmp1063_1062;
    tmp1072_1063[2] = 48;
    tmp1072_1063[3] = 70;
    tmp1072_1063[4] = -1;
    int[] tmp1084_1072 = tmp1072_1063;
    tmp1084_1072[5] = -1;
    tmp1084_1072[6] = -1;
    arrayOfInt[14] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1106_1104 = arrayOfInt2;
    int[] tmp1107_1106 = tmp1106_1104;
    int[] tmp1107_1106 = tmp1106_1104;
    tmp1107_1106[0] = 6;
    tmp1107_1106[1] = 26;
    int[] tmp1116_1107 = tmp1107_1106;
    int[] tmp1116_1107 = tmp1107_1106;
    tmp1116_1107[2] = 50;
    tmp1116_1107[3] = 74;
    tmp1116_1107[4] = -1;
    int[] tmp1128_1116 = tmp1116_1107;
    tmp1128_1116[5] = -1;
    tmp1128_1116[6] = -1;
    arrayOfInt[15] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1150_1148 = arrayOfInt2;
    int[] tmp1151_1150 = tmp1150_1148;
    int[] tmp1151_1150 = tmp1150_1148;
    tmp1151_1150[0] = 6;
    tmp1151_1150[1] = 30;
    int[] tmp1160_1151 = tmp1151_1150;
    int[] tmp1160_1151 = tmp1151_1150;
    tmp1160_1151[2] = 54;
    tmp1160_1151[3] = 78;
    tmp1160_1151[4] = -1;
    int[] tmp1172_1160 = tmp1160_1151;
    tmp1172_1160[5] = -1;
    tmp1172_1160[6] = -1;
    arrayOfInt[16] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1194_1192 = arrayOfInt2;
    int[] tmp1195_1194 = tmp1194_1192;
    int[] tmp1195_1194 = tmp1194_1192;
    tmp1195_1194[0] = 6;
    tmp1195_1194[1] = 30;
    int[] tmp1204_1195 = tmp1195_1194;
    int[] tmp1204_1195 = tmp1195_1194;
    tmp1204_1195[2] = 56;
    tmp1204_1195[3] = 82;
    tmp1204_1195[4] = -1;
    int[] tmp1216_1204 = tmp1204_1195;
    tmp1216_1204[5] = -1;
    tmp1216_1204[6] = -1;
    arrayOfInt[17] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1238_1236 = arrayOfInt2;
    int[] tmp1239_1238 = tmp1238_1236;
    int[] tmp1239_1238 = tmp1238_1236;
    tmp1239_1238[0] = 6;
    tmp1239_1238[1] = 30;
    int[] tmp1248_1239 = tmp1239_1238;
    int[] tmp1248_1239 = tmp1239_1238;
    tmp1248_1239[2] = 58;
    tmp1248_1239[3] = 86;
    tmp1248_1239[4] = -1;
    int[] tmp1260_1248 = tmp1248_1239;
    tmp1260_1248[5] = -1;
    tmp1260_1248[6] = -1;
    arrayOfInt[18] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1282_1280 = arrayOfInt2;
    int[] tmp1283_1282 = tmp1282_1280;
    int[] tmp1283_1282 = tmp1282_1280;
    tmp1283_1282[0] = 6;
    tmp1283_1282[1] = 34;
    int[] tmp1292_1283 = tmp1283_1282;
    int[] tmp1292_1283 = tmp1283_1282;
    tmp1292_1283[2] = 62;
    tmp1292_1283[3] = 90;
    tmp1292_1283[4] = -1;
    int[] tmp1304_1292 = tmp1292_1283;
    tmp1304_1292[5] = -1;
    tmp1304_1292[6] = -1;
    arrayOfInt[19] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1326_1324 = arrayOfInt2;
    int[] tmp1327_1326 = tmp1326_1324;
    int[] tmp1327_1326 = tmp1326_1324;
    tmp1327_1326[0] = 6;
    tmp1327_1326[1] = 28;
    int[] tmp1336_1327 = tmp1327_1326;
    int[] tmp1336_1327 = tmp1327_1326;
    tmp1336_1327[2] = 50;
    tmp1336_1327[3] = 72;
    tmp1336_1327[4] = 94;
    int[] tmp1349_1336 = tmp1336_1327;
    tmp1349_1336[5] = -1;
    tmp1349_1336[6] = -1;
    arrayOfInt[20] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1371_1369 = arrayOfInt2;
    int[] tmp1372_1371 = tmp1371_1369;
    int[] tmp1372_1371 = tmp1371_1369;
    tmp1372_1371[0] = 6;
    tmp1372_1371[1] = 26;
    int[] tmp1381_1372 = tmp1372_1371;
    int[] tmp1381_1372 = tmp1372_1371;
    tmp1381_1372[2] = 50;
    tmp1381_1372[3] = 74;
    tmp1381_1372[4] = 98;
    int[] tmp1394_1381 = tmp1381_1372;
    tmp1394_1381[5] = -1;
    tmp1394_1381[6] = -1;
    arrayOfInt[21] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1416_1414 = arrayOfInt2;
    int[] tmp1417_1416 = tmp1416_1414;
    int[] tmp1417_1416 = tmp1416_1414;
    tmp1417_1416[0] = 6;
    tmp1417_1416[1] = 30;
    int[] tmp1426_1417 = tmp1417_1416;
    int[] tmp1426_1417 = tmp1417_1416;
    tmp1426_1417[2] = 54;
    tmp1426_1417[3] = 78;
    tmp1426_1417[4] = 102;
    int[] tmp1439_1426 = tmp1426_1417;
    tmp1439_1426[5] = -1;
    tmp1439_1426[6] = -1;
    arrayOfInt[22] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1461_1459 = arrayOfInt2;
    int[] tmp1462_1461 = tmp1461_1459;
    int[] tmp1462_1461 = tmp1461_1459;
    tmp1462_1461[0] = 6;
    tmp1462_1461[1] = 28;
    int[] tmp1471_1462 = tmp1462_1461;
    int[] tmp1471_1462 = tmp1462_1461;
    tmp1471_1462[2] = 54;
    tmp1471_1462[3] = 80;
    tmp1471_1462[4] = 106;
    int[] tmp1484_1471 = tmp1471_1462;
    tmp1484_1471[5] = -1;
    tmp1484_1471[6] = -1;
    arrayOfInt[23] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1506_1504 = arrayOfInt2;
    int[] tmp1507_1506 = tmp1506_1504;
    int[] tmp1507_1506 = tmp1506_1504;
    tmp1507_1506[0] = 6;
    tmp1507_1506[1] = 32;
    int[] tmp1516_1507 = tmp1507_1506;
    int[] tmp1516_1507 = tmp1507_1506;
    tmp1516_1507[2] = 58;
    tmp1516_1507[3] = 84;
    tmp1516_1507[4] = 110;
    int[] tmp1529_1516 = tmp1516_1507;
    tmp1529_1516[5] = -1;
    tmp1529_1516[6] = -1;
    arrayOfInt[24] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1551_1549 = arrayOfInt2;
    int[] tmp1552_1551 = tmp1551_1549;
    int[] tmp1552_1551 = tmp1551_1549;
    tmp1552_1551[0] = 6;
    tmp1552_1551[1] = 30;
    int[] tmp1561_1552 = tmp1552_1551;
    int[] tmp1561_1552 = tmp1552_1551;
    tmp1561_1552[2] = 58;
    tmp1561_1552[3] = 86;
    tmp1561_1552[4] = 114;
    int[] tmp1574_1561 = tmp1561_1552;
    tmp1574_1561[5] = -1;
    tmp1574_1561[6] = -1;
    arrayOfInt[25] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1596_1594 = arrayOfInt2;
    int[] tmp1597_1596 = tmp1596_1594;
    int[] tmp1597_1596 = tmp1596_1594;
    tmp1597_1596[0] = 6;
    tmp1597_1596[1] = 34;
    int[] tmp1606_1597 = tmp1597_1596;
    int[] tmp1606_1597 = tmp1597_1596;
    tmp1606_1597[2] = 62;
    tmp1606_1597[3] = 90;
    tmp1606_1597[4] = 118;
    int[] tmp1619_1606 = tmp1606_1597;
    tmp1619_1606[5] = -1;
    tmp1619_1606[6] = -1;
    arrayOfInt[26] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1641_1639 = arrayOfInt2;
    int[] tmp1642_1641 = tmp1641_1639;
    int[] tmp1642_1641 = tmp1641_1639;
    tmp1642_1641[0] = 6;
    tmp1642_1641[1] = 26;
    int[] tmp1651_1642 = tmp1642_1641;
    int[] tmp1651_1642 = tmp1642_1641;
    tmp1651_1642[2] = 50;
    tmp1651_1642[3] = 74;
    tmp1651_1642[4] = 98;
    int[] tmp1664_1651 = tmp1651_1642;
    tmp1664_1651[5] = 122;
    tmp1664_1651[6] = -1;
    arrayOfInt[27] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1687_1685 = arrayOfInt2;
    int[] tmp1688_1687 = tmp1687_1685;
    int[] tmp1688_1687 = tmp1687_1685;
    tmp1688_1687[0] = 6;
    tmp1688_1687[1] = 30;
    int[] tmp1697_1688 = tmp1688_1687;
    int[] tmp1697_1688 = tmp1688_1687;
    tmp1697_1688[2] = 54;
    tmp1697_1688[3] = 78;
    tmp1697_1688[4] = 102;
    int[] tmp1710_1697 = tmp1697_1688;
    tmp1710_1697[5] = 126;
    tmp1710_1697[6] = -1;
    arrayOfInt[28] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1733_1731 = arrayOfInt2;
    int[] tmp1734_1733 = tmp1733_1731;
    int[] tmp1734_1733 = tmp1733_1731;
    tmp1734_1733[0] = 6;
    tmp1734_1733[1] = 26;
    int[] tmp1743_1734 = tmp1734_1733;
    int[] tmp1743_1734 = tmp1734_1733;
    tmp1743_1734[2] = 52;
    tmp1743_1734[3] = 78;
    tmp1743_1734[4] = 104;
    int[] tmp1756_1743 = tmp1743_1734;
    tmp1756_1743[5] = '';
    tmp1756_1743[6] = -1;
    arrayOfInt[29] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1780_1778 = arrayOfInt2;
    int[] tmp1781_1780 = tmp1780_1778;
    int[] tmp1781_1780 = tmp1780_1778;
    tmp1781_1780[0] = 6;
    tmp1781_1780[1] = 30;
    int[] tmp1790_1781 = tmp1781_1780;
    int[] tmp1790_1781 = tmp1781_1780;
    tmp1790_1781[2] = 56;
    tmp1790_1781[3] = 82;
    tmp1790_1781[4] = 108;
    int[] tmp1803_1790 = tmp1790_1781;
    tmp1803_1790[5] = '';
    tmp1803_1790[6] = -1;
    arrayOfInt[30] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1827_1825 = arrayOfInt2;
    int[] tmp1828_1827 = tmp1827_1825;
    int[] tmp1828_1827 = tmp1827_1825;
    tmp1828_1827[0] = 6;
    tmp1828_1827[1] = 34;
    int[] tmp1837_1828 = tmp1828_1827;
    int[] tmp1837_1828 = tmp1828_1827;
    tmp1837_1828[2] = 60;
    tmp1837_1828[3] = 86;
    tmp1837_1828[4] = 112;
    int[] tmp1850_1837 = tmp1837_1828;
    tmp1850_1837[5] = '';
    tmp1850_1837[6] = -1;
    arrayOfInt[31] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1874_1872 = arrayOfInt2;
    int[] tmp1875_1874 = tmp1874_1872;
    int[] tmp1875_1874 = tmp1874_1872;
    tmp1875_1874[0] = 6;
    tmp1875_1874[1] = 30;
    int[] tmp1884_1875 = tmp1875_1874;
    int[] tmp1884_1875 = tmp1875_1874;
    tmp1884_1875[2] = 58;
    tmp1884_1875[3] = 86;
    tmp1884_1875[4] = 114;
    int[] tmp1897_1884 = tmp1884_1875;
    tmp1897_1884[5] = '';
    tmp1897_1884[6] = -1;
    arrayOfInt[32] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1921_1919 = arrayOfInt2;
    int[] tmp1922_1921 = tmp1921_1919;
    int[] tmp1922_1921 = tmp1921_1919;
    tmp1922_1921[0] = 6;
    tmp1922_1921[1] = 34;
    int[] tmp1931_1922 = tmp1922_1921;
    int[] tmp1931_1922 = tmp1922_1921;
    tmp1931_1922[2] = 62;
    tmp1931_1922[3] = 90;
    tmp1931_1922[4] = 118;
    int[] tmp1944_1931 = tmp1931_1922;
    tmp1944_1931[5] = '';
    tmp1944_1931[6] = -1;
    arrayOfInt[33] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp1968_1966 = arrayOfInt2;
    int[] tmp1969_1968 = tmp1968_1966;
    int[] tmp1969_1968 = tmp1968_1966;
    tmp1969_1968[0] = 6;
    tmp1969_1968[1] = 30;
    int[] tmp1978_1969 = tmp1969_1968;
    int[] tmp1978_1969 = tmp1969_1968;
    tmp1978_1969[2] = 54;
    tmp1978_1969[3] = 78;
    tmp1978_1969[4] = 102;
    int[] tmp1991_1978 = tmp1978_1969;
    tmp1991_1978[5] = 126;
    tmp1991_1978[6] = '';
    arrayOfInt[34] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp2016_2014 = arrayOfInt2;
    int[] tmp2017_2016 = tmp2016_2014;
    int[] tmp2017_2016 = tmp2016_2014;
    tmp2017_2016[0] = 6;
    tmp2017_2016[1] = 24;
    int[] tmp2026_2017 = tmp2017_2016;
    int[] tmp2026_2017 = tmp2017_2016;
    tmp2026_2017[2] = 50;
    tmp2026_2017[3] = 76;
    tmp2026_2017[4] = 102;
    int[] tmp2039_2026 = tmp2026_2017;
    tmp2039_2026[5] = '';
    tmp2039_2026[6] = '';
    arrayOfInt[35] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp2065_2063 = arrayOfInt2;
    int[] tmp2066_2065 = tmp2065_2063;
    int[] tmp2066_2065 = tmp2065_2063;
    tmp2066_2065[0] = 6;
    tmp2066_2065[1] = 28;
    int[] tmp2075_2066 = tmp2066_2065;
    int[] tmp2075_2066 = tmp2066_2065;
    tmp2075_2066[2] = 54;
    tmp2075_2066[3] = 80;
    tmp2075_2066[4] = 106;
    int[] tmp2088_2075 = tmp2075_2066;
    tmp2088_2075[5] = '';
    tmp2088_2075[6] = '';
    arrayOfInt[36] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp2114_2112 = arrayOfInt2;
    int[] tmp2115_2114 = tmp2114_2112;
    int[] tmp2115_2114 = tmp2114_2112;
    tmp2115_2114[0] = 6;
    tmp2115_2114[1] = 32;
    int[] tmp2124_2115 = tmp2115_2114;
    int[] tmp2124_2115 = tmp2115_2114;
    tmp2124_2115[2] = 58;
    tmp2124_2115[3] = 84;
    tmp2124_2115[4] = 110;
    int[] tmp2137_2124 = tmp2124_2115;
    tmp2137_2124[5] = '';
    tmp2137_2124[6] = '¢';
    arrayOfInt[37] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp2163_2161 = arrayOfInt2;
    int[] tmp2164_2163 = tmp2163_2161;
    int[] tmp2164_2163 = tmp2163_2161;
    tmp2164_2163[0] = 6;
    tmp2164_2163[1] = 26;
    int[] tmp2173_2164 = tmp2164_2163;
    int[] tmp2173_2164 = tmp2164_2163;
    tmp2173_2164[2] = 54;
    tmp2173_2164[3] = 82;
    tmp2173_2164[4] = 110;
    int[] tmp2186_2173 = tmp2173_2164;
    tmp2186_2173[5] = '';
    tmp2186_2173[6] = '¦';
    arrayOfInt[38] = arrayOfInt2;
    arrayOfInt2 = new int[m];
    int[] tmp2212_2210 = arrayOfInt2;
    int[] tmp2213_2212 = tmp2212_2210;
    int[] tmp2213_2212 = tmp2212_2210;
    tmp2213_2212[0] = 6;
    tmp2213_2212[1] = 30;
    int[] tmp2222_2213 = tmp2213_2212;
    int[] tmp2222_2213 = tmp2213_2212;
    tmp2222_2213[2] = 58;
    tmp2222_2213[3] = 86;
    tmp2222_2213[4] = 114;
    int[] tmp2235_2222 = tmp2222_2213;
    tmp2235_2222[5] = '';
    tmp2235_2222[6] = 'ª';
    arrayOfInt[39] = arrayOfInt2;
    c = arrayOfInt;
    arrayOfInt = new int[15][];
    arrayOfInt1 = new int[k];
    int[] tmp2273_2271 = arrayOfInt1;
    tmp2273_2271[0] = 8;
    tmp2273_2271[1] = 0;
    arrayOfInt[0] = arrayOfInt1;
    arrayOfInt1 = new int[k];
    int[] tmp2294_2292 = arrayOfInt1;
    tmp2294_2292[0] = 8;
    tmp2294_2292[1] = 1;
    arrayOfInt[i] = arrayOfInt1;
    arrayOfInt1 = new int[k];
    int[] tmp2315_2313 = arrayOfInt1;
    tmp2315_2313[0] = 8;
    tmp2315_2313[1] = 2;
    arrayOfInt[k] = arrayOfInt1;
    arrayOfInt2 = new int[k];
    int[] tmp2336_2334 = arrayOfInt2;
    tmp2336_2334[0] = 8;
    tmp2336_2334[1] = 3;
    arrayOfInt[3] = arrayOfInt2;
    arrayOfInt2 = new int[k];
    int[] tmp2357_2355 = arrayOfInt2;
    tmp2357_2355[0] = 8;
    tmp2357_2355[1] = 4;
    arrayOfInt[4] = arrayOfInt2;
    arrayOfInt1 = new int[k];
    int[] tmp2378_2376 = arrayOfInt1;
    tmp2378_2376[0] = 8;
    tmp2378_2376[1] = 5;
    arrayOfInt[j] = arrayOfInt1;
    arrayOfInt2 = new int[k];
    int[] tmp2399_2397 = arrayOfInt2;
    tmp2399_2397[0] = 8;
    tmp2399_2397[1] = 7;
    arrayOfInt[6] = arrayOfInt2;
    arrayOfInt1 = new int[k];
    int[] tmp2422_2420 = arrayOfInt1;
    tmp2422_2420[0] = 8;
    tmp2422_2420[1] = 8;
    arrayOfInt[m] = arrayOfInt1;
    arrayOfInt2 = new int[k];
    int[] tmp2444_2442 = arrayOfInt2;
    tmp2444_2442[0] = 7;
    tmp2444_2442[1] = 8;
    arrayOfInt[8] = arrayOfInt2;
    arrayOfInt2 = new int[k];
    int[] tmp2467_2465 = arrayOfInt2;
    tmp2467_2465[0] = 5;
    tmp2467_2465[1] = 8;
    arrayOfInt[9] = arrayOfInt2;
    arrayOfInt2 = new int[k];
    int[] tmp2489_2487 = arrayOfInt2;
    tmp2489_2487[0] = 4;
    tmp2489_2487[1] = 8;
    arrayOfInt[10] = arrayOfInt2;
    arrayOfInt2 = new int[k];
    int[] tmp2511_2509 = arrayOfInt2;
    tmp2511_2509[0] = 3;
    tmp2511_2509[1] = 8;
    arrayOfInt[11] = arrayOfInt2;
    arrayOfInt2 = new int[k];
    int[] tmp2533_2531 = arrayOfInt2;
    tmp2533_2531[0] = 2;
    tmp2533_2531[1] = 8;
    arrayOfInt[12] = arrayOfInt2;
    arrayOfInt2 = new int[k];
    int[] tmp2555_2553 = arrayOfInt2;
    tmp2555_2553[0] = 1;
    tmp2555_2553[1] = 8;
    arrayOfInt[13] = arrayOfInt2;
    arrayOfInt2 = new int[k];
    int[] tmp2577_2575 = arrayOfInt2;
    tmp2577_2575[0] = 0;
    tmp2577_2575[1] = 8;
    arrayOfInt[14] = arrayOfInt2;
    d = arrayOfInt;
  }
  
  static int a(int paramInt)
  {
    int i = Integer.numberOfLeadingZeros(paramInt);
    return 32 - i;
  }
  
  static int a(int paramInt1, int paramInt2)
  {
    if (paramInt2 == 0)
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("0 polynomial");
      throw localIllegalArgumentException;
    }
    int i = a(paramInt2);
    int j = i + -1;
    j = paramInt1 << j;
    for (;;)
    {
      int k = a(j);
      if (k < i) {
        break;
      }
      k = a(j) - i;
      k = paramInt2 << k;
      j ^= k;
    }
    return j;
  }
  
  private static void a(int paramInt1, int paramInt2, b paramb)
  {
    int i = 0;
    WriterException localWriterException = null;
    for (;;)
    {
      int j = 8;
      if (i >= j) {
        break;
      }
      j = paramInt1 + i;
      boolean bool = b(paramb.a(j, paramInt2));
      if (!bool)
      {
        localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
        localWriterException.<init>();
        throw localWriterException;
      }
      int k = paramInt1 + i;
      paramb.a(k, paramInt2, 0);
      i += 1;
    }
  }
  
  static void a(in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf, int paramInt, b paramb)
  {
    int i = 8;
    a locala = new in/juspay/widget/qrscanner/com/google/zxing/common/a;
    locala.<init>();
    a(paramf, paramInt, locala);
    int j = 0;
    int k = locala.a();
    if (j < k)
    {
      k = locala.a() + -1 - j;
      boolean bool = locala.a(k);
      int[] arrayOfInt1 = d[j];
      int m = arrayOfInt1[0];
      int[] arrayOfInt2 = d[j];
      int n = 1;
      int i1 = arrayOfInt2[n];
      paramb.a(m, i1, bool);
      if (j < i)
      {
        m = paramb.b() - j + -1;
        paramb.a(m, i, bool);
      }
      for (;;)
      {
        j += 1;
        break;
        m = paramb.a() + -7;
        i1 = j + -8;
        m += i1;
        paramb.a(i, m, bool);
      }
    }
  }
  
  static void a(in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf, int paramInt, a parama)
  {
    int i = 15;
    boolean bool = f.b(paramInt);
    if (!bool)
    {
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      ((WriterException)localObject1).<init>("Invalid mask pattern");
      throw ((Throwable)localObject1);
    }
    int j = paramf.a() << 3 | paramInt;
    parama.a(j, 5);
    j = a(j, 1335);
    parama.a(j, 10);
    Object localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/a;
    ((a)localObject1).<init>();
    int k = 21522;
    ((a)localObject1).a(k, i);
    parama.b((a)localObject1);
    j = parama.a();
    if (j != i)
    {
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("should not happen but we got: ");
      i = parama.a();
      localObject2 = i;
      ((WriterException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
  }
  
  static void a(j paramj, b paramb)
  {
    d(paramb);
    c(paramb);
    c(paramj, paramb);
    b(paramb);
  }
  
  static void a(j paramj, a parama)
  {
    int i = paramj.a();
    parama.a(i, 6);
    i = a(paramj.a(), 7973);
    parama.a(i, 12);
    i = parama.a();
    int j = 18;
    if (i != j)
    {
      WriterException localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = ((StringBuilder)localObject).append("should not happen but we got: ");
      int k = parama.a();
      localObject = k;
      localWriterException.<init>((String)localObject);
      throw localWriterException;
    }
  }
  
  static void a(b paramb)
  {
    paramb.a((byte)-1);
  }
  
  static void a(a parama, int paramInt, b paramb)
  {
    int i = -1;
    Object localObject = null;
    int j = paramb.b() + -1;
    int i1 = paramb.a() + -1;
    int i3 = i;
    int i4 = 0;
    int i5;
    int i6;
    if (j > 0)
    {
      i5 = 6;
      if (j != i5) {
        break label364;
      }
      j += -1;
      i5 = i1;
      i6 = j;
      i1 = i4;
    }
    for (;;)
    {
      label169:
      WriterException localWriterException;
      if (i5 >= 0)
      {
        j = paramb.a();
        if (i5 < j)
        {
          int i7;
          for (i4 = 0;; i4 = k)
          {
            j = 2;
            if (i4 >= j) {
              break label236;
            }
            i7 = i6 - i4;
            boolean bool1 = b(paramb.a(i7, i5));
            if (bool1) {
              break;
            }
            k = i4 + 1;
          }
          int k = parama.a();
          int m;
          if (i1 < k)
          {
            m = parama.a(i1);
            i1 += 1;
            i8 = m;
            m = i1;
            i1 = i8;
            if (paramInt != i)
            {
              boolean bool2 = d.a(paramInt, i7, i5);
              if (bool2)
              {
                if (i1 != 0) {
                  break label227;
                }
                i1 = 1;
              }
            }
          }
          for (;;)
          {
            paramb.a(i7, i5, i1);
            i1 = m;
            break;
            m = i1;
            i2 = 0;
            localWriterException = null;
            break label169;
            label227:
            i2 = 0;
            localWriterException = null;
          }
          label236:
          n = i5 + i3;
          i5 = n;
          continue;
        }
      }
      i3 = -i3;
      int n = i5 + i3;
      i4 = i6 + -2;
      int i8 = n;
      n = i4;
      i4 = i2;
      int i2 = i8;
      break;
      i2 = parama.a();
      if (i4 != i2)
      {
        localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        localObject = ((StringBuilder)localObject).append("Not all bits consumed: ").append(i4).append('/');
        n = parama.a();
        localObject = n;
        localWriterException.<init>((String)localObject);
        throw localWriterException;
      }
      return;
      label364:
      i5 = i2;
      i6 = n;
      i2 = i4;
    }
  }
  
  static void a(a parama, in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf, j paramj, int paramInt, b paramb)
  {
    a(paramb);
    a(paramj, paramb);
    a(paramf, paramInt, paramb);
    b(paramj, paramb);
    a(parama, paramInt, paramb);
  }
  
  private static void b(int paramInt1, int paramInt2, b paramb)
  {
    int i = 0;
    WriterException localWriterException = null;
    for (;;)
    {
      int j = 7;
      if (i >= j) {
        break;
      }
      j = paramInt2 + i;
      boolean bool = b(paramb.a(paramInt1, j));
      if (!bool)
      {
        localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
        localWriterException.<init>();
        throw localWriterException;
      }
      int k = paramInt2 + i;
      paramb.a(paramInt1, k, 0);
      i += 1;
    }
  }
  
  static void b(j paramj, b paramb)
  {
    int i = paramj.a();
    int j = 7;
    if (i < j) {
      return;
    }
    a locala = new in/juspay/widget/qrscanner/com/google/zxing/common/a;
    locala.<init>();
    a(paramj, locala);
    i = 17;
    int k = 0;
    for (;;)
    {
      j = 6;
      if (k >= j) {
        break;
      }
      j = i;
      i = 0;
      for (;;)
      {
        int m = 3;
        if (i >= m) {
          break;
        }
        boolean bool = locala.a(j);
        j += -1;
        int n = paramb.a() + -11 + i;
        paramb.a(k, n, bool);
        n = paramb.a() + -11 + i;
        paramb.a(n, k, bool);
        i += 1;
      }
      k += 1;
      i = j;
    }
  }
  
  private static void b(b paramb)
  {
    int i = 6;
    int j = 8;
    for (;;)
    {
      int k = paramb.b() + -8;
      if (j >= k) {
        break;
      }
      k = (j + 1) % 2;
      boolean bool = b(paramb.a(j, i));
      if (bool) {
        paramb.a(j, i, k);
      }
      bool = b(paramb.a(i, j));
      if (bool) {
        paramb.a(i, j, k);
      }
      j += 1;
    }
  }
  
  private static boolean b(int paramInt)
  {
    int i = -1;
    if (paramInt == i) {}
    for (i = 1;; i = 0) {
      return i;
    }
  }
  
  private static void c(int paramInt1, int paramInt2, b paramb)
  {
    int i = 5;
    int k;
    for (int j = 0; j < i; j = k)
    {
      k = 0;
      while (k < i)
      {
        int m = paramInt1 + k;
        int n = paramInt2 + j;
        int[] arrayOfInt = b[j];
        int i1 = arrayOfInt[k];
        paramb.a(m, n, i1);
        k += 1;
      }
      k = j + 1;
    }
  }
  
  private static void c(j paramj, b paramb)
  {
    int i = -1;
    int j = paramj.a();
    int k = 2;
    if (j < k) {}
    for (;;)
    {
      return;
      j = paramj.a() + -1;
      int[] arrayOfInt1 = c[j];
      int[] arrayOfInt2 = c[j];
      int m = arrayOfInt2.length;
      for (k = 0; k < m; k = j)
      {
        j = 0;
        arrayOfInt2 = null;
        if (j < m)
        {
          int n = arrayOfInt1[k];
          int i1 = arrayOfInt1[j];
          if ((i1 == i) || (n == i)) {}
          for (;;)
          {
            j += 1;
            break;
            boolean bool = b(paramb.a(i1, n));
            if (bool)
            {
              i1 += -2;
              n += -2;
              c(i1, n, paramb);
            }
          }
        }
        j = k + 1;
      }
    }
  }
  
  private static void c(b paramb)
  {
    int i = 8;
    int j = paramb.a() + -8;
    j = paramb.a(i, j);
    if (j == 0)
    {
      WriterException localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      localWriterException.<init>();
      throw localWriterException;
    }
    j = paramb.a() + -8;
    paramb.a(i, j, 1);
  }
  
  private static void d(int paramInt1, int paramInt2, b paramb)
  {
    int i = 7;
    int k;
    for (int j = 0; j < i; j = k)
    {
      k = 0;
      while (k < i)
      {
        int m = paramInt1 + k;
        int n = paramInt2 + j;
        int[] arrayOfInt = a[j];
        int i1 = arrayOfInt[k];
        paramb.a(m, n, i1);
        k += 1;
      }
      k = j + 1;
    }
  }
  
  private static void d(b paramb)
  {
    int i = 7;
    int j = a[0].length;
    d(0, 0, paramb);
    d(paramb.b() - j, 0, paramb);
    j = paramb.b() - j;
    d(0, j, paramb);
    j = 8;
    a(0, i, paramb);
    a(paramb.b() - j, i, paramb);
    j = paramb.b() - j;
    a(0, j, paramb);
    b(i, 0, paramb);
    b(paramb.a() - i + -1, 0, paramb);
    j = paramb.a() - i;
    b(i, j, paramb);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/c/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */