package in.juspay.widget.qrscanner.com.google.zxing.b.b;

import java.io.Serializable;
import java.util.Comparator;

final class e$b
  implements Serializable, Comparator
{
  private final float a;
  
  private e$b(float paramFloat)
  {
    a = paramFloat;
  }
  
  public int a(d paramd1, d paramd2)
  {
    float f1 = paramd2.c();
    float f2 = a;
    f1 = Math.abs(f1 - f2);
    f2 = paramd1.c();
    float f3 = a;
    f2 = Math.abs(f2 - f3);
    boolean bool1 = f1 < f2;
    int i;
    if (bool1)
    {
      i = -1;
      f1 = 0.0F / 0.0F;
    }
    for (;;)
    {
      return i;
      boolean bool2 = f1 < f2;
      if (!bool2)
      {
        bool2 = false;
        f1 = 0.0F;
      }
      else
      {
        bool2 = true;
        f1 = Float.MIN_VALUE;
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/b/e$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */