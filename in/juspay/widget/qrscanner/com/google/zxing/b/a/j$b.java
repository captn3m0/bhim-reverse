package in.juspay.widget.qrscanner.com.google.zxing.b.a;

public final class j$b
{
  private final int a;
  private final j.a[] b;
  
  j$b(int paramInt, j.a... paramVarArgs)
  {
    a = paramInt;
    b = paramVarArgs;
  }
  
  public int a()
  {
    return a;
  }
  
  public int b()
  {
    int i = 0;
    j.a[] arrayOfa = b;
    int j = arrayOfa.length;
    int k = 0;
    while (i < j)
    {
      j.a locala = arrayOfa[i];
      int m = locala.a();
      k += m;
      i += 1;
    }
    return k;
  }
  
  public int c()
  {
    int i = a;
    int j = b();
    return i * j;
  }
  
  public j.a[] d()
  {
    return b;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/a/j$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */