package in.juspay.widget.qrscanner.com.google.zxing.b.c;

import in.juspay.widget.qrscanner.com.google.zxing.b.a.h;
import in.juspay.widget.qrscanner.com.google.zxing.b.a.j;

public final class f
{
  private h a;
  private in.juspay.widget.qrscanner.com.google.zxing.b.a.f b;
  private j c;
  private int d = -1;
  private b e;
  
  public static boolean b(int paramInt)
  {
    if (paramInt >= 0)
    {
      i = 8;
      if (paramInt >= i) {}
    }
    for (int i = 1;; i = 0) {
      return i;
    }
  }
  
  public b a()
  {
    return e;
  }
  
  public void a(int paramInt)
  {
    d = paramInt;
  }
  
  public void a(in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf)
  {
    b = paramf;
  }
  
  public void a(h paramh)
  {
    a = paramh;
  }
  
  public void a(j paramj)
  {
    c = paramj;
  }
  
  public void a(b paramb)
  {
    e = paramb;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>(200);
    localStringBuilder.append("<<\n");
    localStringBuilder.append(" mode: ");
    Object localObject = a;
    localStringBuilder.append(localObject);
    localStringBuilder.append("\n ecLevel: ");
    localObject = b;
    localStringBuilder.append(localObject);
    localStringBuilder.append("\n version: ");
    localObject = c;
    localStringBuilder.append(localObject);
    localStringBuilder.append("\n maskPattern: ");
    int i = d;
    localStringBuilder.append(i);
    localObject = e;
    if (localObject == null)
    {
      localObject = "\n matrix: null\n";
      localStringBuilder.append((String)localObject);
    }
    for (;;)
    {
      localStringBuilder.append(">>\n");
      return localStringBuilder.toString();
      localStringBuilder.append("\n matrix:\n");
      localObject = e;
      localStringBuilder.append(localObject);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/c/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */