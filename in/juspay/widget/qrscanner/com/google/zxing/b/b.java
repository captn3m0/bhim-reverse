package in.juspay.widget.qrscanner.com.google.zxing.b;

import in.juspay.widget.qrscanner.com.google.zxing.a;
import in.juspay.widget.qrscanner.com.google.zxing.b.c.c;
import in.juspay.widget.qrscanner.com.google.zxing.e;
import java.util.Map;

public final class b
{
  private static in.juspay.widget.qrscanner.com.google.zxing.common.b a(in.juspay.widget.qrscanner.com.google.zxing.b.c.f paramf, int paramInt1, int paramInt2, int paramInt3)
  {
    in.juspay.widget.qrscanner.com.google.zxing.b.c.b localb = paramf.a();
    IllegalStateException localIllegalStateException;
    if (localb == null)
    {
      localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>();
      throw localIllegalStateException;
    }
    int i = localb.b();
    int j = localb.a();
    int k = paramInt3 * 2 + i;
    int m = paramInt3 * 2 + j;
    int n = Math.max(paramInt1, k);
    int i1 = Math.max(paramInt2, m);
    k = n / k;
    m = i1 / m;
    int i2 = Math.min(k, m);
    k = i * i2;
    m = (n - k) / 2;
    k = j * i2;
    k = (i1 - k) / 2;
    in.juspay.widget.qrscanner.com.google.zxing.common.b localb1 = new in/juspay/widget/qrscanner/com/google/zxing/common/b;
    localb1.<init>(n, i1);
    n = 0;
    i1 = k;
    for (int i3 = 0; i3 < j; i3 = n)
    {
      localIllegalStateException = null;
      n = 0;
      k = m;
      while (n < i)
      {
        int i4 = localb.a(n, i3);
        int i5 = 1;
        if (i4 == i5) {
          localb1.a(k, i1, i2, i2);
        }
        n += 1;
        k += i2;
      }
      n = i3 + 1;
      k = i1 + i2;
      i1 = k;
    }
    return localb1;
  }
  
  public in.juspay.widget.qrscanner.com.google.zxing.common.b a(String paramString, a parama, int paramInt1, int paramInt2, Map paramMap)
  {
    int i = paramString.isEmpty();
    if (i != 0)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("Found empty contents");
      throw ((Throwable)localObject1);
    }
    Object localObject1 = a.l;
    Object localObject2;
    if (parama != localObject1)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Can only encode QR_CODE, but got " + parama;
      ((IllegalArgumentException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    if ((paramInt1 < 0) || (paramInt2 < 0))
    {
      localObject1 = new java/lang/IllegalArgumentException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Requested dimensions are too small: " + paramInt1 + 'x' + paramInt2;
      ((IllegalArgumentException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    localObject1 = in.juspay.widget.qrscanner.com.google.zxing.b.a.f.a;
    int j = 4;
    if (paramMap != null)
    {
      e locale = e.a;
      boolean bool = paramMap.containsKey(locale);
      if (bool)
      {
        localObject1 = e.a;
        localObject1 = in.juspay.widget.qrscanner.com.google.zxing.b.a.f.valueOf(paramMap.get(localObject1).toString());
      }
      locale = e.e;
      bool = paramMap.containsKey(locale);
      if (bool)
      {
        localObject2 = e.e;
        j = Integer.parseInt(paramMap.get(localObject2).toString());
        localObject2 = localObject1;
      }
    }
    for (i = j;; i = j)
    {
      return a(c.a(paramString, (in.juspay.widget.qrscanner.com.google.zxing.b.a.f)localObject2, paramMap), paramInt1, paramInt2, i);
      localObject2 = localObject1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */