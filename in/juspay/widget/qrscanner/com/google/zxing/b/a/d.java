package in.juspay.widget.qrscanner.com.google.zxing.b.a;

import in.juspay.widget.qrscanner.com.google.zxing.FormatException;
import in.juspay.widget.qrscanner.com.google.zxing.common.c;
import in.juspay.widget.qrscanner.com.google.zxing.common.e;
import in.juspay.widget.qrscanner.com.google.zxing.common.l;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

final class d
{
  private static final char[] a = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ $%*+-./:".toCharArray();
  
  private static char a(int paramInt)
  {
    char[] arrayOfChar = a;
    int i = arrayOfChar.length;
    if (paramInt >= i) {
      throw FormatException.a();
    }
    return a[paramInt];
  }
  
  private static int a(c paramc)
  {
    int i = 8;
    int j = paramc.a(i);
    int k = j & 0x80;
    if (k == 0) {
      j &= 0x7F;
    }
    for (;;)
    {
      return j;
      k = j & 0xC0;
      int m = 128;
      if (k == m)
      {
        k = paramc.a(i);
        j = (j & 0x3F) << 8 | k;
      }
      else
      {
        k = j & 0xE0;
        m = 192;
        if (k != m) {
          break;
        }
        k = paramc.a(16);
        j = (j & 0x1F) << 16 | k;
      }
    }
    throw FormatException.a();
  }
  
  static e a(byte[] paramArrayOfByte, j paramj, f paramf, Map paramMap)
  {
    Object localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/c;
    ((c)localObject1).<init>(paramArrayOfByte);
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>(50);
    Object localObject3 = new java/util/ArrayList;
    ((ArrayList)localObject3).<init>(1);
    int i = -1;
    Object localObject4 = null;
    int j = 0;
    float f1 = 0.0F;
    Object localObject5 = null;
    int k = -1;
    int m = i;
    int n = 0;
    for (float f2 = 0.0F;; f2 = f1)
    {
      for (;;)
      {
        Object localObject6;
        try
        {
          j = ((c)localObject1).a();
          i = 4;
          if (j < i)
          {
            localObject5 = h.a;
            localObject6 = localObject5;
            localObject5 = h.a;
            if (localObject6 == localObject5) {
              break label692;
            }
            localObject5 = h.h;
            if (localObject6 != localObject5)
            {
              localObject5 = h.i;
              if (localObject6 != localObject5) {}
            }
            else
            {
              f2 = Float.MIN_VALUE;
              j = 1;
              f1 = f2;
              i = m;
              n = k;
              h localh = h.a;
              if (localObject6 != localh) {
                break;
              }
              localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/e;
              localObject5 = ((StringBuilder)localObject2).toString();
              boolean bool = ((List)localObject3).isEmpty();
              if (!bool) {
                break label676;
              }
              localObject4 = null;
              if (paramf != null) {
                break label683;
              }
              localObject3 = null;
              localObject2 = paramArrayOfByte;
              ((e)localObject1).<init>(paramArrayOfByte, (String)localObject5, (List)localObject4, (String)localObject3, i, n);
              return (e)localObject1;
            }
          }
          else
          {
            j = 4;
            f1 = 5.6E-45F;
            j = ((c)localObject1).a(j);
            localObject5 = h.a(j);
            localObject6 = localObject5;
            continue;
          }
          localObject5 = h.d;
          if (localObject6 != localObject5) {
            break label340;
          }
          j = ((c)localObject1).a();
          i = 16;
          if (j < i)
          {
            localObject1 = FormatException.a();
            throw ((Throwable)localObject1);
          }
        }
        catch (IllegalArgumentException localIllegalArgumentException)
        {
          throw FormatException.a();
        }
        j = 8;
        f1 = 1.1E-44F;
        m = localIllegalArgumentException.a(j);
        j = 8;
        f1 = 1.1E-44F;
        k = localIllegalArgumentException.a(j);
        j = n;
        f1 = f2;
        i = m;
        n = k;
        continue;
        label340:
        localObject5 = h.f;
        FormatException localFormatException;
        if (localObject6 == localObject5)
        {
          j = a(localIllegalArgumentException);
          localObject4 = in.juspay.widget.qrscanner.com.google.zxing.common.d.a(j);
          if (localObject4 == null)
          {
            localFormatException = FormatException.a();
            throw localFormatException;
          }
          j = n;
          f1 = f2;
          i = m;
          n = k;
        }
        else
        {
          localObject5 = h.j;
          if (localObject6 == localObject5)
          {
            j = 4;
            f1 = 5.6E-45F;
            j = localFormatException.a(j);
            i = ((h)localObject6).a(paramj);
            i = localFormatException.a(i);
            int i1 = 1;
            if (j == i1) {
              a(localFormatException, (StringBuilder)localObject2, i);
            }
            j = n;
            f1 = f2;
            i = m;
            n = k;
          }
          else
          {
            j = ((h)localObject6).a(paramj);
            j = localFormatException.a(j);
            Object localObject7 = h.b;
            if (localObject6 == localObject7)
            {
              c(localFormatException, (StringBuilder)localObject2, j);
              j = n;
              f1 = f2;
              i = m;
              n = k;
            }
            else
            {
              localObject7 = h.c;
              if (localObject6 == localObject7)
              {
                a(localFormatException, (StringBuilder)localObject2, j, n);
                j = n;
                f1 = f2;
                i = m;
                n = k;
              }
              else
              {
                localObject7 = h.e;
                if (localObject6 == localObject7)
                {
                  localObject7 = paramMap;
                  a(localFormatException, (StringBuilder)localObject2, j, (in.juspay.widget.qrscanner.com.google.zxing.common.d)localObject4, (Collection)localObject3, paramMap);
                  j = n;
                  f1 = f2;
                  i = m;
                  n = k;
                }
                else
                {
                  localObject7 = h.g;
                  if (localObject6 == localObject7)
                  {
                    b(localFormatException, (StringBuilder)localObject2, j);
                    j = n;
                    f1 = f2;
                    i = m;
                    n = k;
                  }
                  else
                  {
                    localFormatException = FormatException.a();
                    throw localFormatException;
                    label676:
                    localObject4 = localObject3;
                    continue;
                    label683:
                    localObject3 = paramf.toString();
                    continue;
                    label692:
                    j = n;
                    f1 = f2;
                    i = m;
                    n = k;
                  }
                }
              }
            }
          }
        }
      }
      k = n;
      m = i;
      n = j;
    }
  }
  
  private static void a(c paramc, StringBuilder paramStringBuilder, int paramInt)
  {
    int i = paramInt * 13;
    int j = paramc.a();
    if (i > j) {
      throw FormatException.a();
    }
    byte[] arrayOfByte = new byte[paramInt * 2];
    i = 0;
    String str1 = null;
    j = 0;
    String str2 = null;
    if (paramInt > 0)
    {
      i = paramc.a(13);
      int k = i / 96 << 8;
      i = i % 96 | k;
      k = 959;
      if (i < k)
      {
        k = 41377;
        i += k;
      }
      for (;;)
      {
        k = (byte)(i >> 8 & 0xFF);
        arrayOfByte[j] = k;
        k = j + 1;
        i = (byte)(i & 0xFF);
        arrayOfByte[k] = i;
        i = j + 2;
        paramInt += -1;
        j = i;
        break;
        k = 42657;
        i += k;
      }
    }
    try
    {
      str1 = new java/lang/String;
      str2 = "GB2312";
      str1.<init>(arrayOfByte, str2);
      paramStringBuilder.append(str1);
      return;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw FormatException.a();
    }
  }
  
  private static void a(c paramc, StringBuilder paramStringBuilder, int paramInt, in.juspay.widget.qrscanner.com.google.zxing.common.d paramd, Collection paramCollection, Map paramMap)
  {
    int i = paramInt * 8;
    int j = paramc.a();
    if (i > j) {
      throw FormatException.a();
    }
    byte[] arrayOfByte = new byte[paramInt];
    i = 0;
    String str1 = null;
    while (i < paramInt)
    {
      int k = (byte)paramc.a(8);
      arrayOfByte[i] = k;
      i += 1;
    }
    if (paramd == null) {}
    for (str1 = l.a(arrayOfByte, paramMap);; str1 = paramd.name()) {
      try
      {
        String str2 = new java/lang/String;
        str2.<init>(arrayOfByte, str1);
        paramStringBuilder.append(str2);
        paramCollection.add(arrayOfByte);
        return;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        throw FormatException.a();
      }
    }
  }
  
  private static void a(c paramc, StringBuilder paramStringBuilder, int paramInt, boolean paramBoolean)
  {
    int i = 37;
    int j = 11;
    int k = 6;
    int m = 1;
    int n = paramStringBuilder.length();
    while (paramInt > m)
    {
      int i1 = paramc.a();
      if (i1 < j) {
        throw FormatException.a();
      }
      i1 = paramc.a(j);
      char c = a(i1 / 45);
      paramStringBuilder.append(c);
      i1 = a(i1 % 45);
      paramStringBuilder.append(i1);
      paramInt += -2;
    }
    if (paramInt == m)
    {
      int i2 = paramc.a();
      if (i2 < k) {
        throw FormatException.a();
      }
      i2 = a(paramc.a(k));
      paramStringBuilder.append(i2);
    }
    if (paramBoolean)
    {
      int i3 = paramStringBuilder.length();
      if (n < i3)
      {
        i3 = paramStringBuilder.charAt(n);
        if (i3 == i)
        {
          i3 = paramStringBuilder.length() + -1;
          if (n >= i3) {
            break label225;
          }
          i3 = n + 1;
          i3 = paramStringBuilder.charAt(i3);
          if (i3 != i) {
            break label225;
          }
          i3 = n + 1;
          paramStringBuilder.deleteCharAt(i3);
        }
        for (;;)
        {
          n += 1;
          break;
          label225:
          i3 = 29;
          paramStringBuilder.setCharAt(n, i3);
        }
      }
    }
  }
  
  private static void b(c paramc, StringBuilder paramStringBuilder, int paramInt)
  {
    int i = paramInt * 13;
    int j = paramc.a();
    if (i > j) {
      throw FormatException.a();
    }
    byte[] arrayOfByte = new byte[paramInt * 2];
    i = 0;
    String str1 = null;
    j = 0;
    String str2 = null;
    if (paramInt > 0)
    {
      i = paramc.a(13);
      int k = i / 192 << 8;
      i = i % 192 | k;
      k = 7936;
      if (i < k)
      {
        k = 33088;
        i += k;
      }
      for (;;)
      {
        k = (byte)(i >> 8);
        arrayOfByte[j] = k;
        k = j + 1;
        i = (byte)i;
        arrayOfByte[k] = i;
        i = j + 2;
        paramInt += -1;
        j = i;
        break;
        k = 49472;
        i += k;
      }
    }
    try
    {
      str1 = new java/lang/String;
      str2 = "SJIS";
      str1.<init>(arrayOfByte, str2);
      paramStringBuilder.append(str1);
      return;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      throw FormatException.a();
    }
  }
  
  private static void c(c paramc, StringBuilder paramStringBuilder, int paramInt)
  {
    int i = 7;
    int j = 4;
    int k = 10;
    int n;
    for (;;)
    {
      m = 3;
      if (paramInt < m) {
        break;
      }
      m = paramc.a();
      if (m < k) {
        throw FormatException.a();
      }
      m = paramc.a(k);
      n = 1000;
      if (m >= n) {
        throw FormatException.a();
      }
      n = a(m / 100);
      paramStringBuilder.append(n);
      n = a(m / 10 % 10);
      paramStringBuilder.append(n);
      m = a(m % 10);
      paramStringBuilder.append(m);
      paramInt += -3;
    }
    int m = 2;
    if (paramInt == m)
    {
      m = paramc.a();
      if (m < i) {
        throw FormatException.a();
      }
      m = paramc.a(i);
      n = 100;
      if (m >= n) {
        throw FormatException.a();
      }
      n = a(m / 10);
      paramStringBuilder.append(n);
      m = a(m % 10);
      paramStringBuilder.append(m);
    }
    for (;;)
    {
      return;
      m = 1;
      if (paramInt == m)
      {
        m = paramc.a();
        if (m < j) {
          throw FormatException.a();
        }
        m = paramc.a(j);
        if (m >= k) {
          throw FormatException.a();
        }
        m = a(m);
        paramStringBuilder.append(m);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/a/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */