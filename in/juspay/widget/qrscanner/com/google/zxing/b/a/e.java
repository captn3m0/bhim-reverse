package in.juspay.widget.qrscanner.com.google.zxing.b.a;

import in.juspay.widget.qrscanner.com.google.zxing.ChecksumException;
import in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.ReedSolomonException;
import in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.c;
import java.util.Map;

public final class e
{
  private final c a;
  
  public e()
  {
    c localc = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/c;
    in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.a locala = in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.a.e;
    localc.<init>(locala);
    a = localc;
  }
  
  private in.juspay.widget.qrscanner.com.google.zxing.common.e a(a parama, Map paramMap)
  {
    j localj = parama.b();
    f localf = parama.a().a();
    b[] arrayOfb = b.a(parama.c(), localj, localf);
    int i = arrayOfb.length;
    int j = 0;
    int k = 0;
    b localb1 = null;
    while (j < i)
    {
      localb2 = arrayOfb[j];
      m = localb2.a();
      k += m;
      j += 1;
    }
    byte[] arrayOfByte1 = new byte[k];
    int n = arrayOfb.length;
    int m = 0;
    b localb2 = null;
    for (j = 0; m < n; j = k)
    {
      localb1 = arrayOfb[m];
      byte[] arrayOfByte2 = localb1.b();
      int i1 = localb1.a();
      a(arrayOfByte2, i1);
      k = j;
      j = 0;
      while (j < i1)
      {
        i = k + 1;
        int i2 = arrayOfByte2[j];
        arrayOfByte1[k] = i2;
        j += 1;
        k = i;
      }
      m += 1;
    }
    return d.a(arrayOfByte1, localj, localf, paramMap);
  }
  
  private void a(byte[] paramArrayOfByte, int paramInt)
  {
    int i = 0;
    int j = paramArrayOfByte.length;
    int[] arrayOfInt = new int[j];
    int k = 0;
    c localc = null;
    while (k < j)
    {
      int m = paramArrayOfByte[k] & 0xFF;
      arrayOfInt[k] = m;
      k += 1;
    }
    try
    {
      localc = a;
      j = paramArrayOfByte.length - paramInt;
      localc.a(arrayOfInt, j);
      while (i < paramInt)
      {
        k = (byte)arrayOfInt[i];
        paramArrayOfByte[i] = k;
        i += 1;
      }
      return;
    }
    catch (ReedSolomonException localReedSolomonException)
    {
      throw ChecksumException.a();
    }
  }
  
  /* Error */
  public in.juspay.widget.qrscanner.com.google.zxing.common.e a(in.juspay.widget.qrscanner.com.google.zxing.common.b paramb, Map paramMap)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aconst_null
    //   3: astore 4
    //   5: new 25	in/juspay/widget/qrscanner/com/google/zxing/b/a/a
    //   8: astore 5
    //   10: aload 5
    //   12: aload_1
    //   13: invokespecial 72	in/juspay/widget/qrscanner/com/google/zxing/b/a/a:<init>	(Lin/juspay/widget/qrscanner/com/google/zxing/common/b;)V
    //   16: aload_0
    //   17: aload 5
    //   19: aload_2
    //   20: invokespecial 75	in/juspay/widget/qrscanner/com/google/zxing/b/a/e:a	(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
    //   23: astore 4
    //   25: aload 4
    //   27: areturn
    //   28: astore 6
    //   30: aconst_null
    //   31: astore 7
    //   33: aload 5
    //   35: invokevirtual 78	in/juspay/widget/qrscanner/com/google/zxing/b/a/a:d	()V
    //   38: iconst_1
    //   39: istore_3
    //   40: aload 5
    //   42: iload_3
    //   43: invokevirtual 82	in/juspay/widget/qrscanner/com/google/zxing/b/a/a:a	(Z)V
    //   46: aload 5
    //   48: invokevirtual 29	in/juspay/widget/qrscanner/com/google/zxing/b/a/a:b	()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/j;
    //   51: pop
    //   52: aload 5
    //   54: invokevirtual 32	in/juspay/widget/qrscanner/com/google/zxing/b/a/a:a	()Lin/juspay/widget/qrscanner/com/google/zxing/b/a/g;
    //   57: pop
    //   58: aload 5
    //   60: invokevirtual 84	in/juspay/widget/qrscanner/com/google/zxing/b/a/a:e	()V
    //   63: aload_0
    //   64: aload 5
    //   66: aload_2
    //   67: invokespecial 75	in/juspay/widget/qrscanner/com/google/zxing/b/a/e:a	(Lin/juspay/widget/qrscanner/com/google/zxing/b/a/a;Ljava/util/Map;)Lin/juspay/widget/qrscanner/com/google/zxing/common/e;
    //   70: astore 4
    //   72: new 86	in/juspay/widget/qrscanner/com/google/zxing/b/a/i
    //   75: astore 5
    //   77: iconst_1
    //   78: istore 8
    //   80: aload 5
    //   82: iload 8
    //   84: invokespecial 88	in/juspay/widget/qrscanner/com/google/zxing/b/a/i:<init>	(Z)V
    //   87: aload 4
    //   89: aload 5
    //   91: invokevirtual 93	in/juspay/widget/qrscanner/com/google/zxing/common/e:a	(Ljava/lang/Object;)V
    //   94: goto -69 -> 25
    //   97: astore 4
    //   99: aload 6
    //   101: ifnull +14 -> 115
    //   104: aload 6
    //   106: athrow
    //   107: astore 7
    //   109: aconst_null
    //   110: astore 6
    //   112: goto -79 -> 33
    //   115: aload 7
    //   117: ifnull +6 -> 123
    //   120: aload 7
    //   122: athrow
    //   123: aload 4
    //   125: athrow
    //   126: astore 4
    //   128: goto -29 -> 99
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	131	0	this	e
    //   0	131	1	paramb	in.juspay.widget.qrscanner.com.google.zxing.common.b
    //   0	131	2	paramMap	Map
    //   1	42	3	bool1	boolean
    //   3	85	4	locale	in.juspay.widget.qrscanner.com.google.zxing.common.e
    //   97	27	4	localFormatException1	in.juspay.widget.qrscanner.com.google.zxing.FormatException
    //   126	1	4	localChecksumException1	ChecksumException
    //   8	82	5	localObject1	Object
    //   28	77	6	localFormatException2	in.juspay.widget.qrscanner.com.google.zxing.FormatException
    //   110	1	6	localObject2	Object
    //   31	1	7	localObject3	Object
    //   107	14	7	localChecksumException2	ChecksumException
    //   78	5	8	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   19	23	28	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   33	38	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   42	46	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   46	52	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   52	58	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   58	63	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   66	70	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   72	75	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   82	87	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   89	94	97	in/juspay/widget/qrscanner/com/google/zxing/FormatException
    //   19	23	107	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   33	38	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   42	46	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   46	52	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   52	58	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   58	63	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   66	70	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   72	75	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   82	87	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
    //   89	94	126	in/juspay/widget/qrscanner/com/google/zxing/ChecksumException
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/a/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */