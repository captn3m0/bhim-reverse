package in.juspay.widget.qrscanner.com.google.zxing.b.a;

final class b
{
  private final int a;
  private final byte[] b;
  
  private b(int paramInt, byte[] paramArrayOfByte)
  {
    a = paramInt;
    b = paramArrayOfByte;
  }
  
  static b[] a(byte[] paramArrayOfByte, j paramj, f paramf)
  {
    int i = paramArrayOfByte.length;
    int j = paramj.c();
    if (i != j)
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>();
      throw localIllegalArgumentException;
    }
    j.b localb = paramj.a(paramf);
    Object localObject = localb.d();
    int k = localObject.length;
    i = 0;
    IllegalArgumentException localIllegalArgumentException = null;
    j = 0;
    byte[] arrayOfByte1 = null;
    while (i < k)
    {
      arrayOfByte2 = localObject[i];
      m = arrayOfByte2.a();
      j += m;
      i += 1;
    }
    b[] arrayOfb = new b[j];
    int n = localObject.length;
    int m = 0;
    byte[] arrayOfByte2 = null;
    i = 0;
    localIllegalArgumentException = null;
    byte[] arrayOfByte3;
    while (m < n)
    {
      arrayOfByte3 = localObject[m];
      j = i;
      i = 0;
      localIllegalArgumentException = null;
      for (;;)
      {
        k = arrayOfByte3.a();
        if (i >= k) {
          break;
        }
        int i1 = arrayOfByte3.b();
        int i2 = localb.a() + i1;
        k = j + 1;
        b localb1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/b;
        byte[] arrayOfByte4 = new byte[i2];
        localb1.<init>(i1, arrayOfByte4);
        arrayOfb[j] = localb1;
        i += 1;
        j = k;
      }
      m += 1;
      i = j;
    }
    arrayOfByte1 = 0b;
    k = arrayOfByte1.length;
    j = arrayOfb.length + -1;
    int i3;
    if (j >= 0)
    {
      arrayOfByte2 = b;
      m = arrayOfByte2.length;
      if (m != k) {}
    }
    else
    {
      n = j + 1;
      j = localb.a();
      k -= j;
      i3 = 0;
      localObject = null;
      j = 0;
      arrayOfByte1 = null;
    }
    int i4;
    for (;;)
    {
      if (i3 >= k) {
        break label393;
      }
      m = j;
      j = 0;
      arrayOfByte1 = null;
      for (;;)
      {
        if (j < i)
        {
          localb = arrayOfb[j];
          arrayOfByte3 = b;
          i4 = m + 1;
          m = paramArrayOfByte[m];
          arrayOfByte3[i3] = m;
          j += 1;
          m = i4;
          continue;
          j += -1;
          break;
        }
      }
      i3 += 1;
      j = m;
    }
    label393:
    m = n;
    while (m < i)
    {
      localb = arrayOfb[m];
      localObject = b;
      i4 = j + 1;
      j = paramArrayOfByte[j];
      localObject[k] = j;
      m += 1;
      j = i4;
    }
    arrayOfByte2 = 0b;
    int i5 = arrayOfByte2.length;
    while (k < i5)
    {
      m = 0;
      arrayOfByte2 = null;
      i4 = j;
      if (m < i)
      {
        if (m < n) {}
        for (j = k;; j = k + 1)
        {
          localObject = arrayOfb[m];
          byte[] arrayOfByte5 = b;
          i3 = i4 + 1;
          i4 = paramArrayOfByte[i4];
          arrayOfByte5[j] = i4;
          j = m + 1;
          m = j;
          i4 = i3;
          break;
        }
      }
      k += 1;
      j = i4;
    }
    return arrayOfb;
  }
  
  int a()
  {
    return a;
  }
  
  byte[] b()
  {
    return b;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/a/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */