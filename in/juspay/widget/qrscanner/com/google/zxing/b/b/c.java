package in.juspay.widget.qrscanner.com.google.zxing.b.b;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.b.a.j;
import in.juspay.widget.qrscanner.com.google.zxing.common.g;
import in.juspay.widget.qrscanner.com.google.zxing.common.i;
import in.juspay.widget.qrscanner.com.google.zxing.common.k;
import in.juspay.widget.qrscanner.com.google.zxing.m;
import in.juspay.widget.qrscanner.com.google.zxing.n;
import java.util.Map;

public class c
{
  private final in.juspay.widget.qrscanner.com.google.zxing.common.b a;
  private n b;
  
  public c(in.juspay.widget.qrscanner.com.google.zxing.common.b paramb)
  {
    a = paramb;
  }
  
  private float a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = 0;
    float f1 = 0.0F;
    in.juspay.widget.qrscanner.com.google.zxing.common.b localb1 = null;
    int j = 1065353216;
    float f2 = 1.0F;
    float f3 = b(paramInt1, paramInt2, paramInt3, paramInt4);
    int k = paramInt3 - paramInt1;
    k = paramInt1 - k;
    float f4;
    float f5;
    int m;
    in.juspay.widget.qrscanner.com.google.zxing.common.b localb2;
    if (k < 0)
    {
      f4 = paramInt1;
      k = paramInt1 - k;
      f5 = k;
      f5 = f4 / f5;
      m = 0;
      localb2 = null;
      f4 = 0.0F;
    }
    for (;;)
    {
      float f6 = paramInt2;
      int n = paramInt4 - paramInt2;
      float f7 = n;
      f5 *= f7;
      f5 = f6 - f5;
      k = (int)f5;
      if (k < 0)
      {
        f6 = paramInt2;
        k = paramInt2 - k;
        f5 = k;
        f5 = f6 / f5;
      }
      for (;;)
      {
        f6 = paramInt1;
        f4 = m - paramInt1;
        k = (int)(f5 * f4 + f6);
        return b(paramInt1, paramInt2, k, i) + f3 - f2;
        localb2 = a;
        m = localb2.c();
        if (k < m) {
          break label338;
        }
        localb2 = a;
        f4 = localb2.c() + -1 - paramInt1;
        f5 = k - paramInt1;
        f4 /= f5;
        in.juspay.widget.qrscanner.com.google.zxing.common.b localb3 = a;
        k = localb3.c() + -1;
        f5 = f4;
        m = k;
        break;
        localb1 = a;
        i = localb1.d();
        if (k >= i)
        {
          f1 = a.d() + -1 - paramInt2;
          k -= paramInt2;
          f5 = k;
          f5 = f1 / f5;
          localb1 = a;
          i = localb1.d() + -1;
        }
        else
        {
          i = k;
          k = j;
          f5 = f2;
        }
      }
      label338:
      m = k;
      k = j;
      f5 = f2;
    }
  }
  
  private float a(m paramm1, m paramm2)
  {
    float f1 = 7.0F;
    int i = (int)paramm1.a();
    int j = (int)paramm1.b();
    int k = (int)paramm2.a();
    int m = (int)paramm2.b();
    float f2 = a(i, j, k, m);
    j = (int)paramm2.a();
    float f3 = paramm2.b();
    k = (int)f3;
    float f4 = paramm1.a();
    m = (int)f4;
    float f5 = paramm1.b();
    int n = (int)f5;
    float f6 = a(j, k, m, n);
    boolean bool = Float.isNaN(f2);
    if (bool) {
      f2 = f6 / f1;
    }
    for (;;)
    {
      return f2;
      bool = Float.isNaN(f6);
      if (bool)
      {
        f2 /= f1;
      }
      else
      {
        f2 += f6;
        j = 1096810496;
        f6 = 14.0F;
        f2 /= f6;
      }
    }
  }
  
  private static int a(m paramm1, m paramm2, m paramm3, float paramFloat)
  {
    float f1 = m.a(paramm1, paramm2) / paramFloat;
    int i = in.juspay.widget.qrscanner.com.google.zxing.common.a.a.a(f1);
    float f2 = m.a(paramm1, paramm3) / paramFloat;
    int j = in.juspay.widget.qrscanner.com.google.zxing.common.a.a.a(f2);
    i = (i + j) / 2 + 7;
    j = i & 0x3;
    switch (j)
    {
    case 1: 
    default: 
    case 0: 
    case 2: 
      for (;;)
      {
        return i;
        i += 1;
        continue;
        i += -1;
      }
    }
    throw NotFoundException.a();
  }
  
  private static in.juspay.widget.qrscanner.com.google.zxing.common.b a(in.juspay.widget.qrscanner.com.google.zxing.common.b paramb, k paramk, int paramInt)
  {
    return i.a().a(paramb, paramInt, paramInt, paramk);
  }
  
  private static k a(m paramm1, m paramm2, m paramm3, m paramm4, int paramInt)
  {
    float f1 = paramInt;
    float f2 = 3.5F;
    float f3 = f1 - f2;
    float f4;
    float f5;
    float f6;
    if (paramm4 != null)
    {
      f4 = paramm4.a();
      f5 = paramm4.b();
      f1 = 3.0F;
      f6 = f3 - f1;
    }
    for (float f7 = f6;; f7 = f3)
    {
      float f8 = paramm1.a();
      float f9 = paramm1.b();
      float f10 = paramm2.a();
      float f11 = paramm2.b();
      float f12 = paramm3.a();
      float f13 = paramm3.b();
      return k.a(3.5F, 3.5F, f3, 3.5F, f7, f6, 3.5F, f3, f8, f9, f10, f11, f4, f5, f12, f13);
      f1 = paramm2.a();
      f2 = paramm1.a();
      f1 -= f2;
      f2 = paramm3.a();
      f4 = f1 + f2;
      f1 = paramm2.b();
      f2 = paramm1.b();
      f1 -= f2;
      f2 = paramm3.b();
      f5 = f1 + f2;
      f6 = f3;
    }
  }
  
  private float b(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = Math.abs(paramInt4 - paramInt2);
    int j = Math.abs(paramInt3 - paramInt1);
    float f;
    int k;
    if (i > j)
    {
      i = 1;
      f = Float.MIN_VALUE;
      k = i;
      if (k == 0) {
        break label395;
      }
    }
    for (;;)
    {
      int m = Math.abs(paramInt4 - paramInt2);
      int n = Math.abs(paramInt3 - paramInt1);
      i = -m;
      int i1 = i / 2;
      int i2;
      label85:
      label97:
      int i5;
      int i6;
      int i7;
      label133:
      int i8;
      if (paramInt2 < paramInt4)
      {
        i = 1;
        f = Float.MIN_VALUE;
        i2 = i;
        if (paramInt1 >= paramInt3) {
          break label233;
        }
        i = 1;
        f = Float.MIN_VALUE;
        i3 = 0;
        int i4 = paramInt4 + i2;
        i5 = paramInt2;
        i6 = i1;
        i1 = paramInt1;
        if (i5 == i4) {
          break label388;
        }
        if (k == 0) {
          break label244;
        }
        i7 = i1;
        if (k == 0) {
          break label251;
        }
        i8 = i5;
        label142:
        j = 1;
        if (i3 != j) {
          break label258;
        }
      }
      label203:
      label233:
      label244:
      label251:
      label258:
      for (j = 1;; j = 0)
      {
        Object localObject = this;
        localObject = a;
        int i9 = ((in.juspay.widget.qrscanner.com.google.zxing.common.b)localObject).a(i7, i8);
        if (j != i9) {
          break label381;
        }
        j = 2;
        if (i3 != j) {
          break label264;
        }
        f = in.juspay.widget.qrscanner.com.google.zxing.common.a.a.a(i5, i1, paramInt2, paramInt1);
        return f;
        i = 0;
        f = 0.0F;
        k = 0;
        break;
        i = -1;
        f = 0.0F / 0.0F;
        i2 = i;
        break label85;
        i = -1;
        f = 0.0F / 0.0F;
        break label97;
        i7 = i5;
        break label133;
        i10 = i1;
        break label142;
      }
      label264:
      int i10 = i3 + 1;
      label270:
      int i3 = i6 + n;
      if (i3 > 0) {
        if (i1 != paramInt3) {}
      }
      label381:
      label388:
      for (i = i10;; i = i3)
      {
        j = 2;
        if (i == j)
        {
          i = paramInt4 + i2;
          f = in.juspay.widget.qrscanner.com.google.zxing.common.a.a.a(i, paramInt3, paramInt2, paramInt1);
          break label203;
          j = i1 + i;
        }
        for (i1 = i3 - m;; i1 = i3)
        {
          i5 += i2;
          i3 = i10;
          i6 = i1;
          i1 = j;
          break;
          i = 2143289344;
          f = 0.0F / 0.0F;
          break label203;
          j = i1;
        }
        i10 = i3;
        break label270;
      }
      label395:
      int i11 = paramInt4;
      paramInt4 = paramInt3;
      paramInt3 = i11;
      int i12 = paramInt2;
      paramInt2 = paramInt1;
      paramInt1 = i12;
    }
  }
  
  protected final float a(m paramm1, m paramm2, m paramm3)
  {
    float f1 = a(paramm1, paramm2);
    float f2 = a(paramm1, paramm3);
    return (f1 + f2) / 2.0F;
  }
  
  protected final a a(float paramFloat1, int paramInt1, int paramInt2, float paramFloat2)
  {
    int i = 0;
    float f1 = 3.0F;
    float f2 = paramFloat2 * paramFloat1;
    int j = (int)f2;
    int k = paramInt1 - j;
    int n = Math.max(0, k);
    in.juspay.widget.qrscanner.com.google.zxing.common.b localb = a;
    k = localb.c() + -1;
    int i1 = paramInt1 + j;
    int i2 = Math.min(k, i1);
    float f3 = i2 - n;
    float f4 = paramFloat1 * f1;
    boolean bool2 = f3 < f4;
    if (bool2) {
      throw NotFoundException.a();
    }
    int m = paramInt2 - j;
    i1 = Math.max(0, m);
    localb = a;
    m = localb.d() + -1;
    j += paramInt2;
    i = Math.min(m, j);
    f2 = i - i1;
    f3 = paramFloat1 * f1;
    boolean bool1 = f2 < f3;
    if (bool1) {
      throw NotFoundException.a();
    }
    b localb1 = new in/juspay/widget/qrscanner/com/google/zxing/b/b/b;
    localb = a;
    i2 -= n;
    i -= i1;
    n localn = b;
    f1 = paramFloat1;
    localb1.<init>(localb, n, i1, i2, i, paramFloat1, localn);
    return localb1.a();
  }
  
  protected final g a(f paramf)
  {
    d locald1 = paramf.b();
    d locald2 = paramf.c();
    d locald3 = paramf.a();
    float f1 = a(locald1, locald2, locald3);
    float f2 = 1.0F;
    boolean bool = f1 < f2;
    if (bool) {
      throw NotFoundException.a();
    }
    int j = a(locald1, locald2, locald3, f1);
    Object localObject1 = j.a(j);
    int k = ((j)localObject1).d() + -7;
    bool = false;
    f2 = 0.0F;
    Object localObject2 = null;
    localObject1 = ((j)localObject1).b();
    int m = localObject1.length;
    float f3;
    float f4;
    int n;
    if (m > 0)
    {
      f3 = locald2.a();
      f4 = locald1.a();
      f3 -= f4;
      f4 = locald3.a();
      f3 += f4;
      f4 = locald2.b();
      float f5 = locald1.b();
      f4 -= f5;
      f5 = locald3.b();
      f4 += f5;
      float f6 = k;
      f6 = 3.0F / f6;
      f6 = 1.0F - f6;
      f5 = locald1.a();
      float f7 = locald1.a();
      n = (int)((f3 - f7) * f6 + f5);
      f3 = locald1.b();
      f7 = locald1.b();
      f4 -= f7;
      f6 *= f4;
      k = (int)(f3 + f6);
      m = 4;
      f3 = 5.6E-45F;
      int i1 = 16;
      f4 = 2.24E-44F;
      if (m <= i1) {
        f4 = m;
      }
    }
    for (;;)
    {
      try
      {
        localObject2 = a(f1, n, k, f4);
        localObject1 = a(locald1, locald2, locald3, (m)localObject2, j);
        in.juspay.widget.qrscanner.com.google.zxing.common.b localb = a(a, (k)localObject1, j);
        if (localObject2 != null) {
          break label385;
        }
        int i = 3;
        f2 = 4.2E-45F;
        localObject2 = new m[i];
        localObject1 = null;
        localObject2[0] = locald3;
        localObject2[1] = locald1;
        m = 2;
        f3 = 2.8E-45F;
        localObject2[m] = locald2;
        localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/common/g;
        ((g)localObject1).<init>(localb, (m[])localObject2);
        return (g)localObject1;
      }
      catch (NotFoundException localNotFoundException)
      {
        m <<= 1;
      }
      break;
      label385:
      m = 4;
      f3 = 5.6E-45F;
      localObject1 = new m[m];
      j = 0;
      localObject1[0] = locald3;
      int i2 = 1;
      localObject1[i2] = locald1;
      localObject1[2] = locald2;
      int i3 = 3;
      localObject1[i3] = localObject2;
      localObject2 = localObject1;
    }
  }
  
  public final g a(Map paramMap)
  {
    if (paramMap == null) {}
    for (Object localObject = null;; localObject = (n)paramMap.get(localObject))
    {
      b = ((n)localObject);
      localObject = new in/juspay/widget/qrscanner/com/google/zxing/b/b/e;
      in.juspay.widget.qrscanner.com.google.zxing.common.b localb = a;
      n localn = b;
      ((e)localObject).<init>(localb, localn);
      localObject = ((e)localObject).a(paramMap);
      return a((f)localObject);
      localObject = in.juspay.widget.qrscanner.com.google.zxing.d.j;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/b/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */