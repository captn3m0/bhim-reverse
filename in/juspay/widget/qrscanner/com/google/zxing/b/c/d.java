package in.juspay.widget.qrscanner.com.google.zxing.b.c;

final class d
{
  static int a(b paramb)
  {
    int i = a(paramb, true);
    int j = a(paramb, false);
    return i + j;
  }
  
  private static int a(b paramb, boolean paramBoolean)
  {
    int i = 5;
    int j;
    int k;
    label23:
    byte[][] arrayOfByte;
    int m;
    int n;
    if (paramBoolean)
    {
      j = paramb.a();
      k = j;
      if (!paramBoolean) {
        break label136;
      }
      j = paramb.b();
      arrayOfByte = paramb.c();
      m = 0;
      n = 0;
    }
    for (;;)
    {
      if (m >= k) {
        break label236;
      }
      int i1 = -1;
      int i2 = 0;
      int i3 = 0;
      label51:
      if (i2 < j)
      {
        byte[] arrayOfByte1;
        int i4;
        label75:
        int i5;
        if (paramBoolean)
        {
          arrayOfByte1 = arrayOfByte[m];
          i4 = arrayOfByte1[i2];
          if (i4 != i1) {
            break label161;
          }
          i4 = i3 + 1;
          i5 = i1;
          i1 = i4;
          i4 = n;
        }
        for (n = i5;; n = i5)
        {
          i2 += 1;
          i3 = i1;
          i1 = n;
          n = i4;
          break label51;
          j = paramb.b();
          k = j;
          break;
          label136:
          j = paramb.a();
          break label23;
          arrayOfByte1 = arrayOfByte[i2];
          i4 = arrayOfByte1[m];
          break label75;
          label161:
          if (i3 >= i)
          {
            i1 = i3 + -5 + 3;
            n += i1;
          }
          i1 = 1;
          i5 = i4;
          i4 = n;
        }
      }
      if (i3 >= i)
      {
        i1 = i3 + -5 + 3;
        n += i1;
      }
      i1 = m + 1;
      m = i1;
    }
    label236:
    return n;
  }
  
  static boolean a(int paramInt1, int paramInt2, int paramInt3)
  {
    IllegalArgumentException localIllegalArgumentException;
    int i;
    switch (paramInt1)
    {
    default: 
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = "Invalid mask pattern: " + paramInt1;
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    case 0: 
      i = paramInt3 + paramInt2 & 0x1;
      if (i == 0) {
        i = 1;
      }
      break;
    }
    for (;;)
    {
      return i;
      int j = paramInt3 & 0x1;
      break;
      j = paramInt2 % 3;
      break;
      j = (paramInt3 + paramInt2) % 3;
      break;
      j = paramInt3 / 2;
      int k = paramInt2 / 3;
      j = j + k & 0x1;
      break;
      j = paramInt3 * paramInt2;
      k = j & 0x1;
      j = j % 3 + k;
      break;
      j = paramInt3 * paramInt2;
      k = j & 0x1;
      j = j % 3 + k & 0x1;
      break;
      j = paramInt3 * paramInt2 % 3;
      k = paramInt3 + paramInt2 & 0x1;
      j = j + k & 0x1;
      break;
      j = 0;
      localIllegalArgumentException = null;
    }
  }
  
  private static boolean a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = 1;
    boolean bool = false;
    int j = Math.max(paramInt1, 0);
    int k = paramArrayOfByte.length;
    k = Math.min(paramInt2, k);
    if (j < k)
    {
      int m = paramArrayOfByte[j];
      if (m != i) {}
    }
    for (;;)
    {
      return bool;
      j += 1;
      break;
      bool = i;
    }
  }
  
  private static boolean a(byte[][] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    int i = 1;
    boolean bool = false;
    int j = Math.max(paramInt2, 0);
    int k = paramArrayOfByte.length;
    k = Math.min(paramInt3, k);
    if (j < k)
    {
      byte[] arrayOfByte = paramArrayOfByte[j];
      int m = arrayOfByte[paramInt1];
      if (m != i) {}
    }
    for (;;)
    {
      return bool;
      j += 1;
      break;
      bool = i;
    }
  }
  
  static int b(b paramb)
  {
    byte[][] arrayOfByte = paramb.c();
    int i = paramb.b();
    int j = paramb.a();
    int k = 0;
    int m = 0;
    for (;;)
    {
      int n = j + -1;
      if (k >= n) {
        break;
      }
      n = 0;
      for (;;)
      {
        int i1 = i + -1;
        if (n >= i1) {
          break;
        }
        byte[] arrayOfByte1 = arrayOfByte[k];
        i1 = arrayOfByte1[n];
        byte[] arrayOfByte2 = arrayOfByte[k];
        int i2 = n + 1;
        int i3 = arrayOfByte2[i2];
        if (i1 == i3)
        {
          i3 = k + 1;
          arrayOfByte2 = arrayOfByte[i3];
          i3 = arrayOfByte2[n];
          if (i1 == i3)
          {
            i3 = k + 1;
            arrayOfByte2 = arrayOfByte[i3];
            i2 = n + 1;
            i3 = arrayOfByte2[i2];
            if (i1 == i3) {
              m += 1;
            }
          }
        }
        n += 1;
      }
      n = k + 1;
      k = n;
    }
    return m * 3;
  }
  
  static int c(b paramb)
  {
    int i = 1;
    byte[][] arrayOfByte = paramb.c();
    int j = paramb.b();
    int k = paramb.a();
    int m = 0;
    int n = 0;
    while (m < k)
    {
      int i1 = 0;
      while (i1 < j)
      {
        byte[] arrayOfByte1 = arrayOfByte[m];
        int i2 = i1 + 6;
        int i3;
        if (i2 < j)
        {
          i2 = arrayOfByte1[i1];
          if (i2 == i)
          {
            i2 = i1 + 1;
            i2 = arrayOfByte1[i2];
            if (i2 == 0)
            {
              i2 = i1 + 2;
              i2 = arrayOfByte1[i2];
              if (i2 == i)
              {
                i2 = i1 + 3;
                i2 = arrayOfByte1[i2];
                if (i2 == i)
                {
                  i2 = i1 + 4;
                  i2 = arrayOfByte1[i2];
                  if (i2 == i)
                  {
                    i2 = i1 + 5;
                    i2 = arrayOfByte1[i2];
                    if (i2 == 0)
                    {
                      i2 = i1 + 6;
                      i2 = arrayOfByte1[i2];
                      if (i2 == i)
                      {
                        i2 = i1 + -4;
                        boolean bool1 = a(arrayOfByte1, i2, i1);
                        if (!bool1)
                        {
                          i3 = i1 + 7;
                          int i4 = i1 + 11;
                          boolean bool2 = a(arrayOfByte1, i3, i4);
                          if (!bool2) {}
                        }
                        else
                        {
                          n += 1;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        int i5 = m + 6;
        if (i5 < k)
        {
          arrayOfByte1 = arrayOfByte[m];
          i5 = arrayOfByte1[i1];
          if (i5 == i)
          {
            i5 = m + 1;
            arrayOfByte1 = arrayOfByte[i5];
            i5 = arrayOfByte1[i1];
            if (i5 == 0)
            {
              i5 = m + 2;
              arrayOfByte1 = arrayOfByte[i5];
              i5 = arrayOfByte1[i1];
              if (i5 == i)
              {
                i5 = m + 3;
                arrayOfByte1 = arrayOfByte[i5];
                i5 = arrayOfByte1[i1];
                if (i5 == i)
                {
                  i5 = m + 4;
                  arrayOfByte1 = arrayOfByte[i5];
                  i5 = arrayOfByte1[i1];
                  if (i5 == i)
                  {
                    i5 = m + 5;
                    arrayOfByte1 = arrayOfByte[i5];
                    i5 = arrayOfByte1[i1];
                    if (i5 == 0)
                    {
                      i5 = m + 6;
                      arrayOfByte1 = arrayOfByte[i5];
                      i5 = arrayOfByte1[i1];
                      if (i5 == i)
                      {
                        i5 = m + -4;
                        boolean bool3 = a(arrayOfByte, i1, i5, m);
                        if (!bool3)
                        {
                          int i6 = m + 7;
                          i3 = m + 11;
                          boolean bool4 = a(arrayOfByte, i1, i6, i3);
                          if (!bool4) {}
                        }
                        else
                        {
                          n += 1;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        i1 += 1;
      }
      i1 = m + 1;
      m = i1;
    }
    return n * 40;
  }
  
  static int d(b paramb)
  {
    int i = 0;
    byte[][] arrayOfByte = paramb.c();
    int j = paramb.b();
    int k = paramb.a();
    int m = 0;
    int n = 0;
    while (m < k)
    {
      byte[] arrayOfByte1 = arrayOfByte[m];
      i1 = 0;
      while (i1 < j)
      {
        int i2 = arrayOfByte1[i1];
        int i3 = 1;
        if (i2 == i3) {
          n += 1;
        }
        i1 += 1;
      }
      i1 = m + 1;
      m = i1;
    }
    int i1 = paramb.a();
    i = paramb.b();
    i1 *= i;
    return Math.abs(n * 2 - i1) * 10 / i1 * 10;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/c/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */