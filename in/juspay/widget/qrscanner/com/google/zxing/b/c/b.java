package in.juspay.widget.qrscanner.com.google.zxing.b.c;

import java.lang.reflect.Array;

public final class b
{
  private final byte[][] a;
  private final int b;
  private final int c;
  
  public b(int paramInt1, int paramInt2)
  {
    Object localObject = { paramInt2, paramInt1 };
    localObject = (byte[][])Array.newInstance(Byte.TYPE, (int[])localObject);
    a = ((byte[][])localObject);
    b = paramInt1;
    c = paramInt2;
  }
  
  public byte a(int paramInt1, int paramInt2)
  {
    return a[paramInt2][paramInt1];
  }
  
  public int a()
  {
    return c;
  }
  
  public void a(byte paramByte)
  {
    int i = 0;
    for (;;)
    {
      int j = c;
      if (i >= j) {
        break;
      }
      j = 0;
      for (;;)
      {
        int k = b;
        if (j >= k) {
          break;
        }
        byte[] arrayOfByte = a[i];
        arrayOfByte[j] = paramByte;
        j += 1;
      }
      i += 1;
    }
  }
  
  public void a(int paramInt1, int paramInt2, int paramInt3)
  {
    byte[] arrayOfByte = a[paramInt2];
    int i = (byte)paramInt3;
    arrayOfByte[paramInt1] = i;
  }
  
  public void a(int paramInt1, int paramInt2, boolean paramBoolean)
  {
    byte[][] arrayOfByte = a;
    byte[] arrayOfByte1 = arrayOfByte[paramInt2];
    int i;
    if (paramBoolean) {
      i = 1;
    }
    for (;;)
    {
      i = (byte)i;
      arrayOfByte1[paramInt1] = i;
      return;
      i = 0;
      arrayOfByte = null;
    }
  }
  
  public int b()
  {
    return b;
  }
  
  public byte[][] c()
  {
    return a;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = b * 2;
    int j = c;
    i = i * j + 2;
    localStringBuilder.<init>(i);
    i = 0;
    for (;;)
    {
      j = c;
      if (i >= j) {
        break;
      }
      j = 0;
      int k = b;
      if (j < k)
      {
        Object localObject = a[i];
        int m = localObject[j];
        switch (m)
        {
        default: 
          localObject = "  ";
          localStringBuilder.append((String)localObject);
        }
        for (;;)
        {
          j += 1;
          break;
          localObject = " 0";
          localStringBuilder.append((String)localObject);
          continue;
          localObject = " 1";
          localStringBuilder.append((String)localObject);
        }
      }
      j = 10;
      localStringBuilder.append(j);
      i += 1;
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/c/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */