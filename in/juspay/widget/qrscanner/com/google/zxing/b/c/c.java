package in.juspay.widget.qrscanner.com.google.zxing.b.c;

import in.juspay.widget.qrscanner.com.google.zxing.WriterException;
import in.juspay.widget.qrscanner.com.google.zxing.b.a.h;
import in.juspay.widget.qrscanner.com.google.zxing.b.a.j;
import in.juspay.widget.qrscanner.com.google.zxing.b.a.j.b;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

public final class c
{
  private static final int[] a;
  
  static
  {
    int[] arrayOfInt = new int[96];
    arrayOfInt[0] = -1;
    arrayOfInt[1] = -1;
    arrayOfInt[2] = -1;
    arrayOfInt[3] = -1;
    arrayOfInt[4] = -1;
    arrayOfInt[5] = -1;
    arrayOfInt[6] = -1;
    arrayOfInt[7] = -1;
    arrayOfInt[8] = -1;
    arrayOfInt[9] = -1;
    arrayOfInt[10] = -1;
    arrayOfInt[11] = -1;
    arrayOfInt[12] = -1;
    arrayOfInt[13] = -1;
    arrayOfInt[14] = -1;
    arrayOfInt[15] = -1;
    arrayOfInt[16] = -1;
    arrayOfInt[17] = -1;
    arrayOfInt[18] = -1;
    arrayOfInt[19] = -1;
    arrayOfInt[20] = -1;
    arrayOfInt[21] = -1;
    arrayOfInt[22] = -1;
    arrayOfInt[23] = -1;
    arrayOfInt[24] = -1;
    arrayOfInt[25] = -1;
    arrayOfInt[26] = -1;
    arrayOfInt[27] = -1;
    arrayOfInt[28] = -1;
    arrayOfInt[29] = -1;
    arrayOfInt[30] = -1;
    arrayOfInt[31] = -1;
    arrayOfInt[32] = 36;
    arrayOfInt[33] = -1;
    arrayOfInt[34] = -1;
    arrayOfInt[35] = -1;
    arrayOfInt[36] = 37;
    arrayOfInt[37] = 38;
    arrayOfInt[38] = -1;
    arrayOfInt[39] = -1;
    arrayOfInt[40] = -1;
    arrayOfInt[41] = -1;
    arrayOfInt[42] = 39;
    arrayOfInt[43] = 40;
    arrayOfInt[44] = -1;
    arrayOfInt[45] = 41;
    arrayOfInt[46] = 42;
    arrayOfInt[47] = 43;
    arrayOfInt[48] = 0;
    arrayOfInt[49] = 1;
    arrayOfInt[50] = 2;
    arrayOfInt[51] = 3;
    arrayOfInt[52] = 4;
    arrayOfInt[53] = 5;
    arrayOfInt[54] = 6;
    arrayOfInt[55] = 7;
    arrayOfInt[56] = 8;
    arrayOfInt[57] = 9;
    arrayOfInt[58] = 44;
    arrayOfInt[59] = -1;
    arrayOfInt[60] = -1;
    arrayOfInt[61] = -1;
    arrayOfInt[62] = -1;
    arrayOfInt[63] = -1;
    arrayOfInt[64] = -1;
    arrayOfInt[65] = 10;
    arrayOfInt[66] = 11;
    arrayOfInt[67] = 12;
    arrayOfInt[68] = 13;
    arrayOfInt[69] = 14;
    arrayOfInt[70] = 15;
    arrayOfInt[71] = 16;
    arrayOfInt[72] = 17;
    arrayOfInt[73] = 18;
    arrayOfInt[74] = 19;
    arrayOfInt[75] = 20;
    arrayOfInt[76] = 21;
    arrayOfInt[77] = 22;
    arrayOfInt[78] = 23;
    arrayOfInt[79] = 24;
    arrayOfInt[80] = 25;
    arrayOfInt[81] = 26;
    arrayOfInt[82] = 27;
    arrayOfInt[83] = 28;
    arrayOfInt[84] = 29;
    arrayOfInt[85] = 30;
    arrayOfInt[86] = 31;
    arrayOfInt[87] = 32;
    arrayOfInt[88] = 33;
    arrayOfInt[89] = 34;
    arrayOfInt[90] = 35;
    arrayOfInt[91] = -1;
    arrayOfInt[92] = -1;
    arrayOfInt[93] = -1;
    arrayOfInt[94] = -1;
    arrayOfInt[95] = -1;
    a = arrayOfInt;
  }
  
  static int a(int paramInt)
  {
    int[] arrayOfInt = a;
    int i = arrayOfInt.length;
    if (paramInt < i) {
      arrayOfInt = a;
    }
    for (i = arrayOfInt[paramInt];; i = -1) {
      return i;
    }
  }
  
  private static int a(h paramh, in.juspay.widget.qrscanner.com.google.zxing.common.a parama1, in.juspay.widget.qrscanner.com.google.zxing.common.a parama2, j paramj)
  {
    int i = parama1.a();
    int j = paramh.a(paramj);
    i += j;
    j = parama2.a();
    return i + j;
  }
  
  private static int a(b paramb)
  {
    int i = d.a(paramb);
    int j = d.b(paramb);
    i += j;
    j = d.c(paramb);
    i += j;
    j = d.d(paramb);
    return i + j;
  }
  
  private static int a(in.juspay.widget.qrscanner.com.google.zxing.common.a parama, in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf, j paramj, b paramb)
  {
    int i = -1 >>> 1;
    int j = -1;
    int k = 0;
    int m = 8;
    if (k < m)
    {
      e.a(parama, paramf, paramj, k, paramb);
      m = a(paramb);
      if (m >= i) {
        break label64;
      }
      j = k;
    }
    for (;;)
    {
      k += 1;
      i = m;
      break;
      return j;
      label64:
      m = i;
    }
  }
  
  private static h a(String paramString1, String paramString2)
  {
    int i = 1;
    int j = 0;
    h localh = null;
    String str = "Shift_JIS";
    boolean bool = str.equals(paramString2);
    if (bool)
    {
      bool = a(paramString1);
      if (bool) {
        localh = h.g;
      }
    }
    for (;;)
    {
      return localh;
      bool = false;
      str = null;
      int m = 0;
      int n = paramString1.length();
      int k;
      if (j < n)
      {
        n = paramString1.charAt(j);
        int i1 = 48;
        if (n >= i1)
        {
          i1 = 57;
          if (n <= i1) {
            m = i;
          }
        }
        for (;;)
        {
          j += 1;
          break;
          k = a(n);
          n = -1;
          if (k == n) {
            break label126;
          }
          k = i;
        }
        label126:
        localh = h.e;
      }
      else if (k != 0)
      {
        localh = h.c;
      }
      else if (m != 0)
      {
        localh = h.b;
      }
      else
      {
        localh = h.e;
      }
    }
  }
  
  private static j a(int paramInt, in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf)
  {
    int i = 1;
    for (;;)
    {
      int j = 40;
      if (i > j) {
        break;
      }
      j localj = j.b(i);
      boolean bool = a(paramInt, localj, paramf);
      if (bool) {
        return localj;
      }
      i += 1;
    }
    WriterException localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
    localWriterException.<init>("Data too big");
    throw localWriterException;
  }
  
  private static j a(in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf, h paramh, in.juspay.widget.qrscanner.com.google.zxing.common.a parama1, in.juspay.widget.qrscanner.com.google.zxing.common.a parama2)
  {
    j localj = j.b(1);
    localj = a(a(paramh, parama1, parama2, localj), paramf);
    return a(a(paramh, parama1, parama2, localj), paramf);
  }
  
  public static f a(String paramString, in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf, Map paramMap)
  {
    Object localObject1 = "ISO-8859-1";
    boolean bool1;
    if (paramMap != null)
    {
      localObject2 = in.juspay.widget.qrscanner.com.google.zxing.e.b;
      bool1 = paramMap.containsKey(localObject2);
      if (bool1) {
        bool1 = true;
      }
    }
    h localh;
    Object localObject3;
    Object localObject4;
    for (;;)
    {
      if (bool1)
      {
        localObject1 = in.juspay.widget.qrscanner.com.google.zxing.e.b;
        localObject1 = paramMap.get(localObject1).toString();
      }
      localh = a(paramString, (String)localObject1);
      localObject3 = new in/juspay/widget/qrscanner/com/google/zxing/common/a;
      ((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject3).<init>();
      localObject4 = h.e;
      if (localh == localObject4) {
        if (!bool1)
        {
          localObject2 = "ISO-8859-1";
          bool1 = ((String)localObject2).equals(localObject1);
          if (bool1) {}
        }
        else
        {
          localObject2 = in.juspay.widget.qrscanner.com.google.zxing.common.d.a((String)localObject1);
          if (localObject2 != null) {
            a((in.juspay.widget.qrscanner.com.google.zxing.common.d)localObject2, (in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject3);
          }
        }
      }
      a(localh, (in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject3);
      localObject4 = new in/juspay/widget/qrscanner/com/google/zxing/common/a;
      ((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject4).<init>();
      a(paramString, localh, (in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject4, (String)localObject1);
      if (paramMap == null) {
        break;
      }
      localObject1 = in.juspay.widget.qrscanner.com.google.zxing.e.f;
      boolean bool2 = paramMap.containsKey(localObject1);
      if (!bool2) {
        break;
      }
      localObject1 = in.juspay.widget.qrscanner.com.google.zxing.e.f;
      int j = Integer.parseInt(paramMap.get(localObject1).toString());
      localObject1 = j.b(j);
      bool1 = a(a(localh, (in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject3, (in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject4, (j)localObject1), (j)localObject1, paramf);
      if (bool1) {
        break label246;
      }
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      ((WriterException)localObject1).<init>("Data too big for requested version");
      throw ((Throwable)localObject1);
      bool1 = false;
      localObject2 = null;
    }
    localObject1 = a(paramf, localh, (in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject3, (in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject4);
    label246:
    in.juspay.widget.qrscanner.com.google.zxing.common.a locala = new in/juspay/widget/qrscanner/com/google/zxing/common/a;
    locala.<init>();
    locala.a((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject3);
    Object localObject2 = h.e;
    if (localh == localObject2) {}
    for (int i = ((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject4).b();; i = paramString.length())
    {
      a(i, (j)localObject1, localh, locala);
      locala.a((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject4);
      localObject2 = ((j)localObject1).a(paramf);
      int k = ((j)localObject1).c();
      int m = ((j.b)localObject2).c();
      k -= m;
      a(k, locala);
      m = ((j)localObject1).c();
      i = ((j.b)localObject2).b();
      localObject2 = a(locala, m, k, i);
      localObject3 = new in/juspay/widget/qrscanner/com/google/zxing/b/c/f;
      ((f)localObject3).<init>();
      ((f)localObject3).a(paramf);
      ((f)localObject3).a(localh);
      ((f)localObject3).a((j)localObject1);
      int n = ((j)localObject1).d();
      localObject4 = new in/juspay/widget/qrscanner/com/google/zxing/b/c/b;
      ((b)localObject4).<init>(n, n);
      n = a((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject2, paramf, (j)localObject1, (b)localObject4);
      ((f)localObject3).a(n);
      e.a((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject2, paramf, (j)localObject1, n, (b)localObject4);
      ((f)localObject3).a((b)localObject4);
      return (f)localObject3;
    }
  }
  
  static in.juspay.widget.qrscanner.com.google.zxing.common.a a(in.juspay.widget.qrscanner.com.google.zxing.common.a parama, int paramInt1, int paramInt2, int paramInt3)
  {
    int i = parama.b();
    if (i != paramInt2)
    {
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      ((WriterException)localObject1).<init>("Number of bits and data bytes does not match");
      throw ((Throwable)localObject1);
    }
    int m = 0;
    Object localObject2 = null;
    int n = 0;
    Object localObject3 = null;
    i = 0;
    Object localObject1 = null;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>(paramInt3);
    int i1 = 0;
    Iterator localIterator = null;
    int i2 = 0;
    int i3 = 0;
    for (int i4 = 0; i1 < paramInt3; i4 = m)
    {
      int[] arrayOfInt = new int[1];
      Object localObject4 = new int[1];
      i = paramInt1;
      n = paramInt2;
      m = paramInt3;
      a(paramInt1, paramInt2, paramInt3, i1, arrayOfInt, (int[])localObject4);
      localObject1 = null;
      i = arrayOfInt[0];
      localObject3 = new byte[i];
      m = i4 * 8;
      parama.a(m, (byte[])localObject3, 0, i);
      m = localObject4[0];
      localObject2 = a((byte[])localObject3, m);
      localObject4 = new in/juspay/widget/qrscanner/com/google/zxing/b/c/a;
      ((a)localObject4).<init>((byte[])localObject3, (byte[])localObject2);
      localArrayList.add(localObject4);
      n = Math.max(i3, i);
      i = localObject2.length;
      i = Math.max(i2, i);
      localObject2 = null;
      m = arrayOfInt[0] + i4;
      i1 += 1;
      i2 = i;
      i3 = n;
    }
    if (paramInt2 != i4)
    {
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      ((WriterException)localObject1).<init>("Data bytes does not match offset");
      throw ((Throwable)localObject1);
    }
    localObject2 = new in/juspay/widget/qrscanner/com/google/zxing/common/a;
    ((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject2).<init>();
    i = 0;
    localObject1 = null;
    n = 0;
    localObject3 = null;
    int i5;
    while (n < i3)
    {
      localIterator = localArrayList.iterator();
      for (;;)
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = ((a)localIterator.next()).a();
        i5 = localObject1.length;
        if (n < i5)
        {
          j = localObject1[n];
          i5 = 8;
          ((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject2).a(j, i5);
        }
      }
      j = n + 1;
      n = j;
    }
    int j = 0;
    localObject1 = null;
    n = 0;
    localObject3 = null;
    while (n < i2)
    {
      localIterator = localArrayList.iterator();
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        localObject1 = ((a)localIterator.next()).b();
        i5 = localObject1.length;
        if (n < i5)
        {
          k = localObject1[n];
          i5 = 8;
          ((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject2).a(k, i5);
        }
      }
      k = n + 1;
      n = k;
    }
    int k = ((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject2).b();
    if (paramInt1 != k)
    {
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append("Interleaving error: ").append(paramInt1).append(" and ");
      m = ((in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject2).b();
      localObject3 = m + " differ.";
      ((WriterException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
    }
    return (in.juspay.widget.qrscanner.com.google.zxing.common.a)localObject2;
  }
  
  static void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfInt1, int[] paramArrayOfInt2)
  {
    WriterException localWriterException;
    if (paramInt4 >= paramInt3)
    {
      localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      localWriterException.<init>("Block ID too large");
      throw localWriterException;
    }
    int i = paramInt1 % paramInt3;
    int j = paramInt3 - i;
    int k = paramInt1 / paramInt3;
    int m = k + 1;
    int n = paramInt2 / paramInt3;
    int i1 = n + 1;
    k -= n;
    m -= i1;
    if (k != m)
    {
      localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      localWriterException.<init>("EC bytes mismatch");
      throw localWriterException;
    }
    int i2 = j + i;
    if (paramInt3 != i2)
    {
      localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      localWriterException.<init>("RS blocks mismatch");
      throw localWriterException;
    }
    i2 = (n + k) * j;
    int i3 = i1 + m;
    i = i * i3 + i2;
    if (paramInt1 != i)
    {
      localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      localWriterException.<init>("Total bytes mismatch");
      throw localWriterException;
    }
    if (paramInt4 < j)
    {
      paramArrayOfInt1[0] = n;
      paramArrayOfInt2[0] = k;
    }
    for (;;)
    {
      return;
      paramArrayOfInt1[0] = i1;
      paramArrayOfInt2[0] = m;
    }
  }
  
  static void a(int paramInt, j paramj, h paramh, in.juspay.widget.qrscanner.com.google.zxing.common.a parama)
  {
    int i = 1;
    int j = paramh.a(paramj);
    int k = i << j;
    if (paramInt >= k)
    {
      WriterException localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder = localStringBuilder.append(paramInt).append(" is bigger than ");
      j = (i << j) + -1;
      String str = j;
      localWriterException.<init>(str);
      throw localWriterException;
    }
    parama.a(paramInt, j);
  }
  
  static void a(int paramInt, in.juspay.widget.qrscanner.com.google.zxing.common.a parama)
  {
    int i = 8;
    int j = 0;
    WriterException localWriterException = null;
    int k = paramInt * 8;
    int m = parama.a();
    if (m > k)
    {
      localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = ((StringBuilder)localObject).append("data bits cannot fit in the QR Code");
      n = parama.a();
      localObject = n + " > " + k;
      localWriterException.<init>((String)localObject);
      throw localWriterException;
    }
    m = 0;
    Object localObject = null;
    for (;;)
    {
      n = 4;
      if (m >= n) {
        break;
      }
      n = parama.a();
      if (n >= k) {
        break;
      }
      parama.a(false);
      m += 1;
    }
    m = parama.a() & 0x7;
    if (m > 0) {
      while (m < i)
      {
        parama.a(false);
        m += 1;
      }
    }
    m = parama.b();
    int n = paramInt - m;
    m = 0;
    localObject = null;
    if (m < n)
    {
      j = m & 0x1;
      if (j == 0) {}
      for (j = 236;; j = 17)
      {
        parama.a(j, i);
        j = m + 1;
        m = j;
        break;
      }
    }
    j = parama.a();
    if (j != k)
    {
      localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      localWriterException.<init>("Bits size does not equal capacity");
      throw localWriterException;
    }
  }
  
  static void a(h paramh, in.juspay.widget.qrscanner.com.google.zxing.common.a parama)
  {
    int i = paramh.a();
    parama.a(i, 4);
  }
  
  private static void a(in.juspay.widget.qrscanner.com.google.zxing.common.d paramd, in.juspay.widget.qrscanner.com.google.zxing.common.a parama)
  {
    int i = h.f.a();
    parama.a(i, 4);
    i = paramd.a();
    parama.a(i, 8);
  }
  
  static void a(CharSequence paramCharSequence, in.juspay.widget.qrscanner.com.google.zxing.common.a parama)
  {
    int i = paramCharSequence.length();
    int j = 0;
    while (j < i)
    {
      int k = paramCharSequence.charAt(j) + '￐';
      int m = j + 2;
      if (m < i)
      {
        m = j + 1;
        m = paramCharSequence.charAt(m) + '￐';
        int n = j + 2;
        n = paramCharSequence.charAt(n) + '￐';
        k *= 100;
        m *= 10;
        k = k + m + n;
        m = 10;
        parama.a(k, m);
        j += 3;
      }
      else
      {
        m = j + 1;
        if (m < i)
        {
          m = j + 1;
          m = paramCharSequence.charAt(m) + '￐';
          k = k * 10 + m;
          m = 7;
          parama.a(k, m);
          j += 2;
        }
        else
        {
          m = 4;
          parama.a(k, m);
          j += 1;
        }
      }
    }
  }
  
  static void a(String paramString1, h paramh, in.juspay.widget.qrscanner.com.google.zxing.common.a parama, String paramString2)
  {
    Object localObject1 = c.1.a;
    int i = paramh.ordinal();
    int j = localObject1[i];
    switch (j)
    {
    default: 
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Invalid mode: " + paramh;
      ((WriterException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    case 1: 
      a(paramString1, parama);
    }
    for (;;)
    {
      return;
      b(paramString1, parama);
      continue;
      a(paramString1, parama, paramString2);
      continue;
      a(paramString1, parama);
    }
  }
  
  static void a(String paramString, in.juspay.widget.qrscanner.com.google.zxing.common.a parama)
  {
    int i = 33088;
    int j = -1;
    Object localObject = "Shift_JIS";
    for (;;)
    {
      int n;
      try
      {
        byte[] arrayOfByte = paramString.getBytes((String)localObject);
        int k = arrayOfByte.length;
        m = 0;
        localObject = null;
        n = 0;
        if (n >= k) {
          break label226;
        }
        m = arrayOfByte[n] & 0xFF;
        i1 = n + 1;
        i1 = arrayOfByte[i1] & 0xFF;
        m = m << 8 | i1;
        if (m >= i)
        {
          i1 = 40956;
          if (m <= i1)
          {
            m -= i;
            if (m != j) {
              break label179;
            }
            localObject = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
            ((WriterException)localObject).<init>("Invalid byte sequence");
            throw ((Throwable)localObject);
          }
        }
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        WriterException localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
        localWriterException.<init>(localUnsupportedEncodingException);
        throw localWriterException;
      }
      int i1 = 57408;
      if (m >= i1)
      {
        i1 = 60351;
        if (m <= i1)
        {
          i1 = 49472;
          m -= i1;
          continue;
          label179:
          i1 = (m >> 8) * 192;
          m = (m & 0xFF) + i1;
          i1 = 13;
          parama.a(m, i1);
          m = n + 2;
          n = m;
          continue;
          label226:
          return;
        }
      }
      int m = j;
    }
  }
  
  static void a(String paramString1, in.juspay.widget.qrscanner.com.google.zxing.common.a parama, String paramString2)
  {
    try
    {
      Object localObject1 = paramString1.getBytes(paramString2);
      int i = localObject1.length;
      int j = 0;
      Object localObject2 = null;
      while (j < i)
      {
        int k = localObject1[j];
        int m = 8;
        parama.a(k, m);
        j += 1;
      }
      return;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
      ((WriterException)localObject1).<init>(localUnsupportedEncodingException);
      throw ((Throwable)localObject1);
    }
  }
  
  private static boolean a(int paramInt, j paramj, in.juspay.widget.qrscanner.com.google.zxing.b.a.f paramf)
  {
    int i = paramj.c();
    j.b localb = paramj.a(paramf);
    int k = localb.c();
    i -= k;
    int m = (paramInt + 7) / 8;
    if (i >= m) {}
    int j;
    for (i = 1;; j = 0) {
      return i;
    }
  }
  
  private static boolean a(String paramString)
  {
    boolean bool = false;
    String str = "Shift_JIS";
    for (;;)
    {
      try
      {
        arrayOfByte = paramString.getBytes(str);
        i = arrayOfByte.length;
        j = i % 2;
        if (j == 0) {
          continue;
        }
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        byte[] arrayOfByte;
        int i;
        int j;
        int k;
        int m;
        continue;
      }
      return bool;
      j = 0;
      str = null;
      if (j < i)
      {
        k = arrayOfByte[j] & 0xFF;
        m = 129;
        if (k >= m)
        {
          m = 159;
          if (k <= m) {}
        }
        else
        {
          m = 224;
          if (k < m) {
            continue;
          }
          m = 235;
          if (k > m) {
            continue;
          }
        }
        j += 2;
      }
      else
      {
        bool = true;
      }
    }
  }
  
  static byte[] a(byte[] paramArrayOfByte, int paramInt)
  {
    int i = 0;
    int j = paramArrayOfByte.length;
    int[] arrayOfInt = new int[j + paramInt];
    int k = 0;
    Object localObject = null;
    int m;
    while (k < j)
    {
      m = paramArrayOfByte[k] & 0xFF;
      arrayOfInt[k] = m;
      k += 1;
    }
    localObject = new in/juspay/widget/qrscanner/com/google/zxing/common/reedsolomon/d;
    in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.a locala = in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.a.e;
    ((in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.d)localObject).<init>(locala);
    ((in.juspay.widget.qrscanner.com.google.zxing.common.reedsolomon.d)localObject).a(arrayOfInt, paramInt);
    localObject = new byte[paramInt];
    while (i < paramInt)
    {
      m = j + i;
      m = (byte)arrayOfInt[m];
      localObject[i] = m;
      i += 1;
    }
    return (byte[])localObject;
  }
  
  static void b(CharSequence paramCharSequence, in.juspay.widget.qrscanner.com.google.zxing.common.a parama)
  {
    int i = -1;
    int j = paramCharSequence.length();
    int k = 0;
    WriterException localWriterException = null;
    while (k < j)
    {
      int m = a(paramCharSequence.charAt(k));
      if (m == i)
      {
        localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
        localWriterException.<init>();
        throw localWriterException;
      }
      int n = k + 1;
      if (n < j)
      {
        n = k + 1;
        n = a(paramCharSequence.charAt(n));
        if (n == i)
        {
          localWriterException = new in/juspay/widget/qrscanner/com/google/zxing/WriterException;
          localWriterException.<init>();
          throw localWriterException;
        }
        m = m * 45 + n;
        n = 11;
        parama.a(m, n);
        k += 2;
      }
      else
      {
        n = 6;
        parama.a(m, n);
        k += 1;
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/c/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */