package in.juspay.widget.qrscanner.com.google.zxing.b.a;

import in.juspay.widget.qrscanner.com.google.zxing.FormatException;
import in.juspay.widget.qrscanner.com.google.zxing.common.b;

final class a
{
  private final b a;
  private j b;
  private g c;
  private boolean d;
  
  a(b paramb)
  {
    int i = paramb.d();
    int j = 21;
    if (i >= j)
    {
      i &= 0x3;
      j = 1;
      if (i == j) {}
    }
    else
    {
      throw FormatException.a();
    }
    a = paramb;
  }
  
  private int a(int paramInt1, int paramInt2, int paramInt3)
  {
    boolean bool1 = d;
    b localb;
    if (bool1)
    {
      localb = a;
      bool1 = localb.a(paramInt2, paramInt1);
      if (!bool1) {
        break label59;
      }
    }
    label59:
    int j;
    for (int i = paramInt3 << 1 | 0x1;; j = paramInt3 << 1)
    {
      return i;
      localb = a;
      boolean bool2 = localb.a(paramInt1, paramInt2);
      break;
    }
  }
  
  g a()
  {
    int i = 7;
    int j = 0;
    int k = 8;
    Object localObject = c;
    if (localObject != null) {}
    for (localObject = c;; localObject = c)
    {
      return (g)localObject;
      int m = 0;
      localObject = null;
      int n = 0;
      for (;;)
      {
        i1 = 6;
        if (m >= i1) {
          break;
        }
        n = a(m, k, n);
        m += 1;
      }
      m = a(i, k, n);
      m = a(k, k, m);
      n = a(k, i, m);
      m = 5;
      while (m >= 0)
      {
        n = a(k, m, n);
        m += -1;
      }
      localObject = a;
      int i1 = ((b)localObject).d();
      i = i1 + -7;
      m = i1 + -1;
      while (m >= i)
      {
        j = a(k, m, j);
        m += -1;
      }
      m = i1 + -8;
      while (m < i1)
      {
        j = a(m, k, j);
        m += 1;
      }
      localObject = g.b(n, j);
      c = ((g)localObject);
      localObject = c;
      if (localObject == null) {
        break;
      }
    }
    throw FormatException.a();
  }
  
  void a(boolean paramBoolean)
  {
    b = null;
    c = null;
    d = paramBoolean;
  }
  
  j b()
  {
    int i = 5;
    float f = 7.0E-45F;
    int j = 0;
    Object localObject = b;
    if (localObject != null) {
      localObject = b;
    }
    for (;;)
    {
      return (j)localObject;
      localObject = a;
      int k = ((b)localObject).d();
      int m = (k + -17) / 4;
      int n = 6;
      if (m <= n)
      {
        localObject = j.b(m);
      }
      else
      {
        int i1 = k + -11;
        int i2 = i;
        n = 0;
        while (i2 >= 0)
        {
          m = k + -9;
          while (m >= i1)
          {
            n = a(m, i2, n);
            m += -1;
          }
          m = i2 + -1;
          i2 = m;
        }
        localObject = j.c(n);
        if (localObject != null)
        {
          n = ((j)localObject).d();
          if (n == k)
          {
            b = ((j)localObject);
            continue;
          }
        }
        int i3 = i;
        i = 0;
        f = 0.0F;
        for (j = i3; j >= 0; j = m)
        {
          m = k + -9;
          while (m >= i1)
          {
            i = a(j, m, i);
            m += -1;
          }
          m = j + -1;
        }
        localObject = j.c(i);
        if (localObject == null) {
          break;
        }
        i = ((j)localObject).d();
        if (i != k) {
          break;
        }
        b = ((j)localObject);
      }
    }
    throw FormatException.a();
  }
  
  byte[] c()
  {
    g localg = a();
    j localj = b();
    Object localObject = c.values();
    int i = localg.b();
    localg = localObject[i];
    int j = a.d();
    localObject = a;
    localg.a((b)localObject, j);
    b localb1 = localj.e();
    int k = 1;
    byte[] arrayOfByte = new byte[localj.c()];
    i = j + -1;
    int m = 0;
    int n = 0;
    int i1 = 0;
    for (int i2 = k; i > 0; i2 = k)
    {
      k = 6;
      if (i == k) {
        i += -1;
      }
      int i3 = 0;
      while (i3 < j)
      {
        if (i2 != 0) {}
        for (k = j + -1 - i3;; k = i3)
        {
          int i4 = 0;
          for (;;)
          {
            int i5 = 2;
            if (i4 >= i5) {
              break;
            }
            i5 = i - i4;
            boolean bool = localb1.a(i5, k);
            if (!bool)
            {
              m += 1;
              n <<= 1;
              b localb2 = a;
              int i7 = i - i4;
              bool = localb2.a(i7, k);
              if (bool) {
                n |= 0x1;
              }
              int i6 = 8;
              if (m == i6)
              {
                m = i1 + 1;
                n = (byte)n;
                arrayOfByte[i1] = n;
                n = 0;
                i1 = m;
                m = 0;
              }
            }
            i4 += 1;
          }
        }
        i3 += 1;
      }
      k = i2 ^ 0x1;
      i += -2;
    }
    i = localj.c();
    if (i1 != i) {
      throw FormatException.a();
    }
    return arrayOfByte;
  }
  
  void d()
  {
    Object localObject = c;
    if (localObject == null) {}
    for (;;)
    {
      return;
      localObject = c.values();
      int i = c.b();
      localObject = localObject[i];
      b localb1 = a;
      i = localb1.d();
      b localb2 = a;
      ((c)localObject).a(localb2, i);
    }
  }
  
  void e()
  {
    int i = 0;
    for (;;)
    {
      b localb1 = a;
      int j = localb1.c();
      if (i >= j) {
        break;
      }
      j = i + 1;
      for (;;)
      {
        b localb2 = a;
        int k = localb2.d();
        if (j >= k) {
          break;
        }
        localb2 = a;
        boolean bool1 = localb2.a(i, j);
        b localb3 = a;
        boolean bool2 = localb3.a(j, i);
        if (bool1 != bool2)
        {
          a.c(j, i);
          localb2 = a;
          localb2.c(i, j);
        }
        j += 1;
      }
      i += 1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/a/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */