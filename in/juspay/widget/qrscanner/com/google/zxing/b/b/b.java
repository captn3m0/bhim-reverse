package in.juspay.widget.qrscanner.com.google.zxing.b.b;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.m;
import in.juspay.widget.qrscanner.com.google.zxing.n;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

final class b
{
  private final in.juspay.widget.qrscanner.com.google.zxing.common.b a;
  private final List b;
  private final int c;
  private final int d;
  private final int e;
  private final int f;
  private final float g;
  private final int[] h;
  private final n i;
  
  b(in.juspay.widget.qrscanner.com.google.zxing.common.b paramb, int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat, n paramn)
  {
    a = paramb;
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>(5);
    b = ((List)localObject);
    c = paramInt1;
    d = paramInt2;
    e = paramInt3;
    f = paramInt4;
    g = paramFloat;
    localObject = new int[3];
    h = ((int[])localObject);
    i = paramn;
  }
  
  private float a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int j = 2;
    float f1 = 0.0F / 0.0F;
    int k = 1;
    in.juspay.widget.qrscanner.com.google.zxing.common.b localb = a;
    int m = localb.d();
    int[] arrayOfInt = h;
    arrayOfInt[0] = 0;
    arrayOfInt[k] = 0;
    arrayOfInt[j] = 0;
    int n = paramInt1;
    int i1;
    while (n >= 0)
    {
      boolean bool1 = localb.a(paramInt2, n);
      if (!bool1) {
        break;
      }
      i1 = arrayOfInt[k];
      if (i1 > paramInt3) {
        break;
      }
      i1 = arrayOfInt[k] + 1;
      arrayOfInt[k] = i1;
      n += -1;
    }
    if (n >= 0)
    {
      i1 = arrayOfInt[k];
      if (i1 <= paramInt3) {
        break label129;
      }
    }
    for (;;)
    {
      return f1;
      label129:
      while (n >= 0)
      {
        boolean bool2 = localb.a(paramInt2, n);
        if (bool2) {
          break;
        }
        int i2 = arrayOfInt[0];
        if (i2 > paramInt3) {
          break;
        }
        i2 = arrayOfInt[0] + 1;
        arrayOfInt[0] = i2;
        n += -1;
      }
      n = arrayOfInt[0];
      if (n <= paramInt3)
      {
        n = paramInt1 + 1;
        int i3;
        while (n < m)
        {
          boolean bool3 = localb.a(paramInt2, n);
          if (!bool3) {
            break;
          }
          i3 = arrayOfInt[k];
          if (i3 > paramInt3) {
            break;
          }
          i3 = arrayOfInt[k] + 1;
          arrayOfInt[k] = i3;
          n += 1;
        }
        if (n != m)
        {
          i3 = arrayOfInt[k];
          if (i3 <= paramInt3)
          {
            while (n < m)
            {
              boolean bool4 = localb.a(paramInt2, n);
              if (bool4) {
                break;
              }
              int i4 = arrayOfInt[j];
              if (i4 > paramInt3) {
                break;
              }
              i4 = arrayOfInt[j] + 1;
              arrayOfInt[j] = i4;
              n += 1;
            }
            int i5 = arrayOfInt[j];
            if (i5 <= paramInt3)
            {
              i5 = arrayOfInt[0];
              m = arrayOfInt[k];
              i5 += m;
              m = arrayOfInt[j];
              i5 = Math.abs(i5 + m - paramInt4) * 5;
              m = paramInt4 * 2;
              if (i5 < m)
              {
                boolean bool5 = a(arrayOfInt);
                if (bool5) {
                  f1 = a(arrayOfInt, n);
                }
              }
            }
          }
        }
      }
    }
  }
  
  private static float a(int[] paramArrayOfInt, int paramInt)
  {
    int j = paramArrayOfInt[2];
    float f1 = paramInt - j;
    float f2 = paramArrayOfInt[1] / 2.0F;
    return f1 - f2;
  }
  
  private a a(int[] paramArrayOfInt, int paramInt1, int paramInt2)
  {
    int j = 2;
    boolean bool1 = false;
    int k = 1;
    int m = paramArrayOfInt[0];
    int i1 = paramArrayOfInt[k];
    m += i1;
    i1 = paramArrayOfInt[j];
    m += i1;
    float f1 = a(paramArrayOfInt, paramInt2);
    int i2 = (int)f1;
    int i3 = paramArrayOfInt[k] * 2;
    float f2 = a(paramInt1, i2, i3, m);
    boolean bool2 = Float.isNaN(f2);
    float f3;
    float f4;
    boolean bool3;
    if (!bool2)
    {
      int n = paramArrayOfInt[0];
      i3 = paramArrayOfInt[k];
      n += i3;
      i3 = paramArrayOfInt[j];
      n += i3;
      f3 = n;
      i3 = 1077936128;
      f4 = f3 / 3.0F;
      localObject = b;
      Iterator localIterator = ((List)localObject).iterator();
      do
      {
        bool3 = localIterator.hasNext();
        if (!bool3) {
          break;
        }
        localObject = (a)localIterator.next();
        bool1 = ((a)localObject).a(f4, f2, f1);
      } while (!bool1);
    }
    for (Object localObject = ((a)localObject).b(f2, f1, f4);; localObject = null)
    {
      return (a)localObject;
      localObject = new in/juspay/widget/qrscanner/com/google/zxing/b/b/a;
      ((a)localObject).<init>(f1, f2, f4);
      b.add(localObject);
      n localn = i;
      if (localn != null)
      {
        localn = i;
        localn.a((m)localObject);
      }
      bool3 = false;
      f3 = 0.0F;
    }
  }
  
  private boolean a(int[] paramArrayOfInt)
  {
    boolean bool1 = false;
    float f1 = g;
    float f2 = f1 / 2.0F;
    int j = 0;
    int k = 3;
    float f3 = 4.2E-45F;
    if (j < k)
    {
      f3 = paramArrayOfInt[j];
      f3 = Math.abs(f1 - f3);
      boolean bool2 = f3 < f2;
      if (bool2) {}
    }
    for (;;)
    {
      return bool1;
      j += 1;
      break;
      bool1 = true;
    }
  }
  
  a a()
  {
    int j = 2;
    int k = 1;
    int m = c;
    int n = f;
    int i1 = e;
    int i4 = m + i1;
    i1 = d;
    int i5 = n / 2;
    int i8 = i1 + i5;
    i1 = 3;
    int[] arrayOfInt = new int[i1];
    int i9 = 0;
    int i10;
    Object localObject;
    int i11;
    if (i9 < n)
    {
      i1 = i9 & 0x1;
      if (i1 == 0) {}
      for (i1 = (i9 + 1) / 2;; i1 = -((i9 + 1) / 2))
      {
        i10 = i8 + i1;
        arrayOfInt[0] = 0;
        arrayOfInt[k] = 0;
        arrayOfInt[j] = 0;
        i1 = m;
        while (i1 < i4)
        {
          in.juspay.widget.qrscanner.com.google.zxing.common.b localb1 = a;
          i6 = localb1.a(i1, i10);
          if (i6 != 0) {
            break;
          }
          i1 += 1;
        }
      }
      int i6 = i1;
      i1 = 0;
      localObject = null;
      for (;;)
      {
        if (i6 < i4)
        {
          in.juspay.widget.qrscanner.com.google.zxing.common.b localb2 = a;
          boolean bool4 = localb2.a(i6, i10);
          if (bool4)
          {
            label218:
            int i7;
            if (i1 == k)
            {
              i11 = arrayOfInt[k] + 1;
              arrayOfInt[k] = i11;
              i6 += 1;
              continue;
            }
            if (i1 == j)
            {
              boolean bool1 = a(arrayOfInt);
              if (bool1)
              {
                localObject = a(arrayOfInt, i10, i7);
                if (localObject == null) {
                  break;
                }
              }
            }
          }
        }
      }
    }
    for (;;)
    {
      return (a)localObject;
      int i2 = arrayOfInt[j];
      arrayOfInt[0] = i2;
      arrayOfInt[k] = k;
      arrayOfInt[j] = 0;
      i2 = k;
      break label218;
      i2 += 1;
      i11 = arrayOfInt[i2] + 1;
      arrayOfInt[i2] = i11;
      break label218;
      if (i2 == k) {
        i2 += 1;
      }
      i11 = arrayOfInt[i2] + 1;
      arrayOfInt[i2] = i11;
      break label218;
      boolean bool2 = a(arrayOfInt);
      if (bool2)
      {
        localObject = a(arrayOfInt, i10, i4);
        if (localObject != null) {}
      }
      else
      {
        int i3 = i9 + 1;
        i9 = i3;
        break;
        localObject = b;
        boolean bool3 = ((List)localObject).isEmpty();
        if (bool3) {
          break label431;
        }
        localObject = (a)b.get(0);
      }
    }
    label431:
    throw NotFoundException.a();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/b/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */