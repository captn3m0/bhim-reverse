package in.juspay.widget.qrscanner.com.google.zxing.b;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.b.a.i;
import in.juspay.widget.qrscanner.com.google.zxing.common.b;
import in.juspay.widget.qrscanner.com.google.zxing.common.g;
import in.juspay.widget.qrscanner.com.google.zxing.d;
import in.juspay.widget.qrscanner.com.google.zxing.j;
import in.juspay.widget.qrscanner.com.google.zxing.k;
import in.juspay.widget.qrscanner.com.google.zxing.l;
import in.juspay.widget.qrscanner.com.google.zxing.m;
import java.util.Map;

public class a
  implements j
{
  private static final m[] a = new m[0];
  private final in.juspay.widget.qrscanner.com.google.zxing.b.a.e b;
  
  public a()
  {
    in.juspay.widget.qrscanner.com.google.zxing.b.a.e locale = new in/juspay/widget/qrscanner/com/google/zxing/b/a/e;
    locale.<init>();
    b = locale;
  }
  
  private static float a(int[] paramArrayOfInt, b paramb)
  {
    int i = 1;
    float f1 = Float.MIN_VALUE;
    int j = paramb.d();
    int k = paramb.c();
    int m = paramArrayOfInt[0];
    int i1 = paramArrayOfInt[i];
    int i2 = i;
    float f2 = f1;
    int i3 = i1;
    int i4 = m;
    i1 = 0;
    float f3 = 0.0F;
    int n;
    if ((i4 < k) && (i3 < j))
    {
      boolean bool = paramb.a(i4, i3);
      if (i2 == bool) {
        break label190;
      }
      n = i1 + 1;
      i1 = 5;
      f3 = 7.0E-45F;
      if (n != i1) {}
    }
    else
    {
      if ((i4 != k) && (i3 != j)) {
        break label175;
      }
      throw NotFoundException.a();
    }
    label127:
    float f4;
    if (i2 == 0)
    {
      i1 = i;
      f3 = f1;
      int i5 = n;
      n = i1;
      f4 = f3;
      i1 = i5;
    }
    for (;;)
    {
      i4 += 1;
      i3 += 1;
      i2 = n;
      f2 = f4;
      break;
      i1 = 0;
      f3 = 0.0F;
      break label127;
      label175:
      i1 = paramArrayOfInt[0];
      return (i4 - i1) / 7.0F;
      label190:
      n = i2;
      f4 = f2;
    }
  }
  
  private static b a(b paramb)
  {
    int i = 1;
    float f1 = Float.MIN_VALUE;
    int[] arrayOfInt1 = paramb.a();
    int[] arrayOfInt2 = paramb.b();
    if ((arrayOfInt1 == null) || (arrayOfInt2 == null)) {
      throw NotFoundException.a();
    }
    float f2 = a(arrayOfInt1, paramb);
    int j = arrayOfInt1[i];
    int k = arrayOfInt2[i];
    i = arrayOfInt1[0];
    int m = arrayOfInt2[0];
    if ((i >= m) || (j >= k)) {
      throw NotFoundException.a();
    }
    int n = k - j;
    int i1 = m - i;
    if (n != i1)
    {
      m = k - j + i;
      n = paramb.c();
      if (m >= n) {
        throw NotFoundException.a();
      }
    }
    i1 = Math.round((m - i + 1) / f2);
    n = k - j + 1;
    float f3 = n / f2;
    int i2 = Math.round(f3);
    if ((i1 <= 0) || (i2 <= 0)) {
      throw NotFoundException.a();
    }
    if (i2 != i1) {
      throw NotFoundException.a();
    }
    f3 = f2 / 2.0F;
    int i3 = (int)f3;
    n = j + i3;
    j = i + i3;
    f1 = (i1 + -1) * f2;
    i = (int)f1 + j;
    m = i - m;
    if (m > 0)
    {
      if (m > i3) {
        throw NotFoundException.a();
      }
      m = j - m;
    }
    for (i = m;; i = j)
    {
      float f4 = (i2 + -1) * f2;
      m = (int)f4 + n - k;
      if (m > 0) {
        if (m > i3) {
          throw NotFoundException.a();
        }
      }
      for (m = n - m;; m = n)
      {
        b localb = new in/juspay/widget/qrscanner/com/google/zxing/common/b;
        localb.<init>(i1, i2);
        for (j = 0; j < i2; j = n)
        {
          n = (int)(j * f2);
          i3 = m + n;
          n = 0;
          arrayOfInt2 = null;
          f3 = 0.0F;
          while (n < i1)
          {
            float f5 = n * f2;
            int i4 = (int)f5 + i;
            boolean bool = paramb.a(i4, i3);
            if (bool) {
              localb.b(n, j);
            }
            n += 1;
          }
          n = j + 1;
        }
        return localb;
      }
    }
  }
  
  public k a(in.juspay.widget.qrscanner.com.google.zxing.c paramc)
  {
    return a(paramc, null);
  }
  
  public final k a(in.juspay.widget.qrscanner.com.google.zxing.c paramc, Map paramMap)
  {
    Object localObject1;
    boolean bool1;
    Object localObject3;
    if (paramMap != null)
    {
      localObject1 = d.b;
      bool1 = paramMap.containsKey(localObject1);
      if (bool1)
      {
        localObject1 = a(paramc.a());
        localObject2 = b.a((b)localObject1, paramMap);
        localObject1 = a;
        localObject3 = localObject2;
      }
    }
    for (Object localObject2 = localObject1;; localObject2 = localObject1)
    {
      localObject1 = ((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).e();
      bool1 = localObject1 instanceof i;
      if (bool1)
      {
        localObject1 = (i)((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).e();
        ((i)localObject1).a((m[])localObject2);
      }
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/k;
      Object localObject4 = ((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).b();
      byte[] arrayOfByte = ((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).a();
      in.juspay.widget.qrscanner.com.google.zxing.a locala = in.juspay.widget.qrscanner.com.google.zxing.a.l;
      ((k)localObject1).<init>((String)localObject4, arrayOfByte, (m[])localObject2, locala);
      localObject2 = ((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).c();
      if (localObject2 != null)
      {
        localObject4 = l.c;
        ((k)localObject1).a((l)localObject4, localObject2);
      }
      localObject2 = ((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).d();
      if (localObject2 != null)
      {
        localObject4 = l.d;
        ((k)localObject1).a((l)localObject4, localObject2);
      }
      boolean bool2 = ((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).f();
      if (bool2)
      {
        localObject2 = l.j;
        int i = ((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).h();
        localObject4 = Integer.valueOf(i);
        ((k)localObject1).a((l)localObject2, localObject4);
        localObject2 = l.k;
        int j = ((in.juspay.widget.qrscanner.com.google.zxing.common.e)localObject3).g();
        localObject3 = Integer.valueOf(j);
        ((k)localObject1).a((l)localObject2, localObject3);
      }
      return (k)localObject1;
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/b/c;
      localObject2 = paramc.a();
      ((in.juspay.widget.qrscanner.com.google.zxing.b.b.c)localObject1).<init>((b)localObject2);
      localObject1 = ((in.juspay.widget.qrscanner.com.google.zxing.b.b.c)localObject1).a(paramMap);
      localObject2 = b;
      localObject3 = ((g)localObject1).a();
      localObject2 = ((in.juspay.widget.qrscanner.com.google.zxing.b.a.e)localObject2).a((b)localObject3, paramMap);
      localObject1 = ((g)localObject1).b();
      localObject3 = localObject2;
    }
  }
  
  public void a() {}
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */