package in.juspay.widget.qrscanner.com.google.zxing.b.b;

import in.juspay.widget.qrscanner.com.google.zxing.NotFoundException;
import in.juspay.widget.qrscanner.com.google.zxing.common.b;
import in.juspay.widget.qrscanner.com.google.zxing.m;
import in.juspay.widget.qrscanner.com.google.zxing.n;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class e
{
  private final b a;
  private final List b;
  private boolean c;
  private final int[] d;
  private final n e;
  
  public e(b paramb, n paramn)
  {
    a = paramb;
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    b = ((List)localObject);
    localObject = new int[5];
    d = ((int[])localObject);
    e = paramn;
  }
  
  private static float a(int[] paramArrayOfInt, int paramInt)
  {
    int i = paramArrayOfInt[4];
    i = paramInt - i;
    int j = paramArrayOfInt[3];
    float f1 = i - j;
    float f2 = paramArrayOfInt[2] / 2.0F;
    return f1 - f2;
  }
  
  private boolean a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int[] arrayOfInt = a();
    int i = 0;
    b localb1 = null;
    b localb2;
    int j;
    int k;
    while ((paramInt1 >= i) && (paramInt2 >= i))
    {
      localb2 = a;
      j = paramInt2 - i;
      k = paramInt1 - i;
      boolean bool5 = localb2.a(j, k);
      if (!bool5) {
        break;
      }
      int i2 = 2;
      j = arrayOfInt[i2] + 1;
      arrayOfInt[i2] = j;
      i += 1;
    }
    if ((paramInt1 < i) || (paramInt2 < i))
    {
      i = 0;
      localb1 = null;
    }
    for (;;)
    {
      return i;
      int i3;
      while ((paramInt1 >= i) && (paramInt2 >= i))
      {
        localb2 = a;
        j = paramInt2 - i;
        k = paramInt1 - i;
        boolean bool6 = localb2.a(j, k);
        if (bool6) {
          break;
        }
        i3 = arrayOfInt[1];
        if (i3 > paramInt3) {
          break;
        }
        i3 = 1;
        j = arrayOfInt[i3] + 1;
        arrayOfInt[i3] = j;
        i += 1;
      }
      if ((paramInt1 >= i) && (paramInt2 >= i))
      {
        i3 = arrayOfInt[1];
        if (i3 <= paramInt3) {}
      }
      else
      {
        i = 0;
        localb1 = null;
        continue;
      }
      int i4;
      while ((paramInt1 >= i) && (paramInt2 >= i))
      {
        localb2 = a;
        j = paramInt2 - i;
        k = paramInt1 - i;
        boolean bool7 = localb2.a(j, k);
        if (!bool7) {
          break;
        }
        localb2 = null;
        i4 = arrayOfInt[0];
        if (i4 > paramInt3) {
          break;
        }
        i4 = 0;
        localb2 = null;
        j = arrayOfInt[0] + 1;
        arrayOfInt[0] = j;
        i += 1;
      }
      localb1 = null;
      i = arrayOfInt[0];
      if (i > paramInt3)
      {
        i = 0;
        localb1 = null;
      }
      else
      {
        i4 = a.d();
        localb1 = a;
        j = localb1.c();
        i = 1;
        b localb3;
        int i5;
        int i6;
        for (;;)
        {
          k = paramInt1 + i;
          if (k >= i4) {
            break;
          }
          k = paramInt2 + i;
          if (k >= j) {
            break;
          }
          localb3 = a;
          i5 = paramInt2 + i;
          i6 = paramInt1 + i;
          boolean bool2 = localb3.a(i5, i6);
          if (!bool2) {
            break;
          }
          m = 2;
          i5 = arrayOfInt[m] + 1;
          arrayOfInt[m] = i5;
          i += 1;
        }
        int m = paramInt1 + i;
        if (m < i4)
        {
          m = paramInt2 + i;
          if (m < j) {}
        }
        else
        {
          i = 0;
          localb1 = null;
          continue;
        }
        for (;;)
        {
          m = paramInt1 + i;
          if (m >= i4) {
            break;
          }
          m = paramInt2 + i;
          if (m >= j) {
            break;
          }
          localb3 = a;
          i5 = paramInt2 + i;
          i6 = paramInt1 + i;
          boolean bool3 = localb3.a(i5, i6);
          if (bool3) {
            break;
          }
          n = arrayOfInt[3];
          if (n >= paramInt3) {
            break;
          }
          n = 3;
          i5 = arrayOfInt[n] + 1;
          arrayOfInt[n] = i5;
          i += 1;
        }
        int n = paramInt1 + i;
        if (n < i4)
        {
          n = paramInt2 + i;
          if (n < j)
          {
            n = arrayOfInt[3];
            if (n < paramInt3) {
              break label635;
            }
          }
        }
        i = 0;
        localb1 = null;
        continue;
        for (;;)
        {
          label635:
          n = paramInt1 + i;
          if (n >= i4) {
            break;
          }
          n = paramInt2 + i;
          if (n >= j) {
            break;
          }
          localb3 = a;
          i5 = paramInt2 + i;
          i6 = paramInt1 + i;
          boolean bool4 = localb3.a(i5, i6);
          if (!bool4) {
            break;
          }
          int i1 = arrayOfInt[4];
          if (i1 >= paramInt3) {
            break;
          }
          i1 = 4;
          i5 = arrayOfInt[i1] + 1;
          arrayOfInt[i1] = i5;
          i += 1;
        }
        i = arrayOfInt[4];
        if (i >= paramInt3)
        {
          i = 0;
          localb1 = null;
        }
        else
        {
          localb1 = null;
          i = arrayOfInt[0];
          i4 = arrayOfInt[1];
          i += i4;
          i4 = arrayOfInt[2];
          i += i4;
          i4 = arrayOfInt[3];
          i += i4;
          i4 = arrayOfInt[4];
          i = Math.abs(i + i4 - paramInt4);
          i4 = paramInt4 * 2;
          if (i < i4)
          {
            bool1 = a(arrayOfInt);
            if (bool1)
            {
              bool1 = true;
              continue;
            }
          }
          boolean bool1 = false;
          localb1 = null;
        }
      }
    }
  }
  
  protected static boolean a(int[] paramArrayOfInt)
  {
    boolean bool1 = true;
    float f1 = 3.0F;
    boolean bool2 = false;
    int i = 0;
    float f2 = 0.0F;
    int j = 0;
    float f3 = 0.0F;
    int k = 5;
    float f4 = 7.0E-45F;
    if (i < k)
    {
      k = paramArrayOfInt[i];
      if (k != 0) {}
    }
    do
    {
      return bool2;
      j += k;
      i += 1;
      break;
      i = 7;
      f2 = 9.8E-45F;
    } while (j < i);
    f2 = j / 7.0F;
    j = 1073741824;
    f3 = f2 / 2.0F;
    f4 = paramArrayOfInt[0];
    f4 = Math.abs(f2 - f4);
    boolean bool4 = f4 < f3;
    if (bool4)
    {
      f4 = paramArrayOfInt[bool1];
      f4 = Math.abs(f2 - f4);
      bool4 = f4 < f3;
      if (bool4)
      {
        f4 = f1 * f2;
        int n = paramArrayOfInt[2];
        float f5 = n;
        f4 = Math.abs(f4 - f5);
        f5 = f1 * f3;
        bool4 = f4 < f5;
        if (bool4)
        {
          f4 = paramArrayOfInt[3];
          f4 = Math.abs(f2 - f4);
          bool4 = f4 < f3;
          if (bool4)
          {
            int m = paramArrayOfInt[4];
            f4 = m;
            f2 = Math.abs(f2 - f4);
            boolean bool3 = f2 < f3;
            if (!bool3) {}
          }
        }
      }
    }
    for (;;)
    {
      bool2 = bool1;
      break;
      bool1 = false;
    }
  }
  
  private int[] a()
  {
    d[0] = 0;
    d[1] = 0;
    d[2] = 0;
    d[3] = 0;
    d[4] = 0;
    return d;
  }
  
  private float b(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = 4;
    int j = 3;
    int k = 1;
    float f = 0.0F / 0.0F;
    b localb = a;
    int m = localb.d();
    int[] arrayOfInt = a();
    int n = paramInt1;
    int i7;
    while (n >= 0)
    {
      boolean bool1 = localb.a(paramInt2, n);
      if (!bool1) {
        break;
      }
      int i1 = 2;
      i7 = arrayOfInt[i1] + 1;
      arrayOfInt[i1] = i7;
      n += -1;
    }
    if (n < 0) {}
    for (;;)
    {
      return f;
      int i2;
      while (n >= 0)
      {
        boolean bool2 = localb.a(paramInt2, n);
        if (bool2) {
          break;
        }
        i2 = arrayOfInt[k];
        if (i2 > paramInt3) {
          break;
        }
        i2 = arrayOfInt[k] + 1;
        arrayOfInt[k] = i2;
        n += -1;
      }
      if (n >= 0)
      {
        i2 = arrayOfInt[k];
        if (i2 <= paramInt3)
        {
          while (n >= 0)
          {
            boolean bool3 = localb.a(paramInt2, n);
            if (!bool3) {
              break;
            }
            int i3 = arrayOfInt[0];
            if (i3 > paramInt3) {
              break;
            }
            i3 = arrayOfInt[0] + 1;
            arrayOfInt[0] = i3;
            n += -1;
          }
          n = arrayOfInt[0];
          if (n <= paramInt3)
          {
            n = paramInt1 + 1;
            while (n < m)
            {
              boolean bool4 = localb.a(paramInt2, n);
              if (!bool4) {
                break;
              }
              int i4 = 2;
              i7 = arrayOfInt[i4] + 1;
              arrayOfInt[i4] = i7;
              n += 1;
            }
            if (n != m)
            {
              int i5;
              while (n < m)
              {
                boolean bool5 = localb.a(paramInt2, n);
                if (bool5) {
                  break;
                }
                i5 = arrayOfInt[j];
                if (i5 >= paramInt3) {
                  break;
                }
                i5 = arrayOfInt[j] + 1;
                arrayOfInt[j] = i5;
                n += 1;
              }
              if (n != m)
              {
                i5 = arrayOfInt[j];
                if (i5 < paramInt3)
                {
                  while (n < m)
                  {
                    boolean bool6 = localb.a(paramInt2, n);
                    if (!bool6) {
                      break;
                    }
                    int i6 = arrayOfInt[i];
                    if (i6 >= paramInt3) {
                      break;
                    }
                    i6 = arrayOfInt[i] + 1;
                    arrayOfInt[i] = i6;
                    n += 1;
                  }
                  int i8 = arrayOfInt[i];
                  if (i8 < paramInt3)
                  {
                    i8 = arrayOfInt[0];
                    m = arrayOfInt[k];
                    i8 += m;
                    m = arrayOfInt[2];
                    i8 += m;
                    m = arrayOfInt[j];
                    i8 += m;
                    m = arrayOfInt[i];
                    i8 = Math.abs(i8 + m - paramInt4) * 5;
                    m = paramInt4 * 2;
                    if (i8 < m)
                    {
                      boolean bool7 = a(arrayOfInt);
                      if (bool7) {
                        f = a(arrayOfInt, n);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  private int b()
  {
    int i = 1;
    float f1 = 0.0F;
    Object localObject1 = b;
    int j = ((List)localObject1).size();
    float f2;
    if (j <= i)
    {
      j = 0;
      localObject1 = null;
      f2 = 0.0F;
      return j;
    }
    float f3 = 0.0F;
    Object localObject2 = null;
    localObject1 = b;
    Iterator localIterator = ((List)localObject1).iterator();
    label53:
    boolean bool = localIterator.hasNext();
    if (bool)
    {
      localObject1 = (d)localIterator.next();
      int m = ((d)localObject1).d();
      int n = 2;
      if (m < n) {
        break label180;
      }
      if (localObject2 != null) {}
    }
    for (;;)
    {
      localObject2 = localObject1;
      break label53;
      c = i;
      f1 = ((m)localObject2).a();
      float f4 = ((d)localObject1).a();
      f1 = Math.abs(f1 - f4);
      f3 = ((m)localObject2).b();
      f2 = ((d)localObject1).b();
      f2 = Math.abs(f3 - f2);
      f2 = f1 - f2;
      int k = (int)f2 / 2;
      break;
      k = 0;
      localObject1 = null;
      f2 = 0.0F;
      break;
      label180:
      localObject1 = localObject2;
    }
  }
  
  private float c(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = 4;
    int j = 3;
    int k = 1;
    float f = 0.0F / 0.0F;
    b localb = a;
    int m = localb.c();
    int[] arrayOfInt = a();
    int n = paramInt1;
    int i7;
    while (n >= 0)
    {
      boolean bool1 = localb.a(n, paramInt2);
      if (!bool1) {
        break;
      }
      int i1 = 2;
      i7 = arrayOfInt[i1] + 1;
      arrayOfInt[i1] = i7;
      n += -1;
    }
    if (n < 0) {}
    for (;;)
    {
      return f;
      int i2;
      while (n >= 0)
      {
        boolean bool2 = localb.a(n, paramInt2);
        if (bool2) {
          break;
        }
        i2 = arrayOfInt[k];
        if (i2 > paramInt3) {
          break;
        }
        i2 = arrayOfInt[k] + 1;
        arrayOfInt[k] = i2;
        n += -1;
      }
      if (n >= 0)
      {
        i2 = arrayOfInt[k];
        if (i2 <= paramInt3)
        {
          while (n >= 0)
          {
            boolean bool3 = localb.a(n, paramInt2);
            if (!bool3) {
              break;
            }
            int i3 = arrayOfInt[0];
            if (i3 > paramInt3) {
              break;
            }
            i3 = arrayOfInt[0] + 1;
            arrayOfInt[0] = i3;
            n += -1;
          }
          n = arrayOfInt[0];
          if (n <= paramInt3)
          {
            n = paramInt1 + 1;
            while (n < m)
            {
              boolean bool4 = localb.a(n, paramInt2);
              if (!bool4) {
                break;
              }
              int i4 = 2;
              i7 = arrayOfInt[i4] + 1;
              arrayOfInt[i4] = i7;
              n += 1;
            }
            if (n != m)
            {
              int i5;
              while (n < m)
              {
                boolean bool5 = localb.a(n, paramInt2);
                if (bool5) {
                  break;
                }
                i5 = arrayOfInt[j];
                if (i5 >= paramInt3) {
                  break;
                }
                i5 = arrayOfInt[j] + 1;
                arrayOfInt[j] = i5;
                n += 1;
              }
              if (n != m)
              {
                i5 = arrayOfInt[j];
                if (i5 < paramInt3)
                {
                  while (n < m)
                  {
                    boolean bool6 = localb.a(n, paramInt2);
                    if (!bool6) {
                      break;
                    }
                    int i6 = arrayOfInt[i];
                    if (i6 >= paramInt3) {
                      break;
                    }
                    i6 = arrayOfInt[i] + 1;
                    arrayOfInt[i] = i6;
                    n += 1;
                  }
                  int i8 = arrayOfInt[i];
                  if (i8 < paramInt3)
                  {
                    i8 = arrayOfInt[0];
                    m = arrayOfInt[k];
                    i8 += m;
                    m = arrayOfInt[2];
                    i8 += m;
                    m = arrayOfInt[j];
                    i8 += m;
                    m = arrayOfInt[i];
                    i8 = Math.abs(i8 + m - paramInt4) * 5;
                    if (i8 < paramInt4)
                    {
                      boolean bool7 = a(arrayOfInt);
                      if (bool7) {
                        f = a(arrayOfInt, n);
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  private boolean c()
  {
    float f1 = 0.0F;
    boolean bool1 = false;
    int i = b.size();
    Object localObject = b;
    Iterator localIterator1 = ((List)localObject).iterator();
    int j = 0;
    float f2 = 0.0F;
    int k = 0;
    float f3 = 0.0F;
    boolean bool2 = localIterator1.hasNext();
    float f4;
    if (bool2)
    {
      localObject = (d)localIterator1.next();
      int n = ((d)localObject).d();
      int i1 = 2;
      if (n < i1) {
        break label227;
      }
      k += 1;
      f4 = ((d)localObject).c() + f2;
    }
    for (j = k;; j = k)
    {
      k = j;
      f2 = f4;
      break;
      int m = 3;
      f4 = 4.2E-45F;
      if (k < m) {}
      for (;;)
      {
        return bool1;
        f4 = i;
        f3 = f2 / f4;
        localObject = b;
        Iterator localIterator2 = ((List)localObject).iterator();
        for (;;)
        {
          bool3 = localIterator2.hasNext();
          if (!bool3) {
            break;
          }
          localObject = (d)localIterator2.next();
          f4 = Math.abs(((d)localObject).c() - f3);
          f1 += f4;
        }
        f4 = 0.05F * f2;
        boolean bool3 = f1 < f4;
        if (!bool3) {
          bool1 = true;
        }
      }
      label227:
      f4 = f2;
    }
  }
  
  private d[] d()
  {
    int i = 2;
    int j = 1;
    float f1 = 0.0F;
    e.a locala = null;
    int k = 3;
    Object localObject1 = b;
    int m = ((List)localObject1).size();
    if (m < k) {
      throw NotFoundException.a();
    }
    int n;
    float f3;
    float f4;
    if (m > k)
    {
      localObject1 = b;
      Iterator localIterator = ((List)localObject1).iterator();
      n = 0;
      localObject2 = null;
      float f2 = 0.0F;
      f3 = 0.0F;
      for (;;)
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = (d)localIterator.next();
        f4 = ((d)localObject1).c();
        f3 += f4;
        f4 = f4 * f4 + f2;
        f2 = f4;
      }
      f4 = m;
      f3 /= f4;
      f4 = m;
      f4 = f2 / f4;
      f2 = f3 * f3;
      double d1 = Math.sqrt(f4 - f2);
      f4 = (float)d1;
      localObject2 = b;
      e.b localb = new in/juspay/widget/qrscanner/com/google/zxing/b/b/e$b;
      localIterator = null;
      localb.<init>(f3, null);
      Collections.sort((List)localObject2, localb);
      float f5 = Math.max(0.2F * f3, f4);
      n = 0;
      localObject2 = null;
      f2 = 0.0F;
      for (;;)
      {
        localObject1 = b;
        int i1 = ((List)localObject1).size();
        if (n >= i1) {
          break;
        }
        localObject1 = b;
        i1 = ((List)localObject1).size();
        if (i1 <= k) {
          break;
        }
        localObject1 = (d)b.get(n);
        f4 = Math.abs(((d)localObject1).c() - f3);
        boolean bool2 = f4 < f5;
        if (bool2)
        {
          localObject1 = b;
          ((List)localObject1).remove(n);
          n += -1;
        }
        i2 = n + 1;
        n = i2;
      }
    }
    localObject1 = b;
    int i2 = ((List)localObject1).size();
    if (i2 > k)
    {
      localObject1 = b;
      localObject2 = ((List)localObject1).iterator();
      for (;;)
      {
        boolean bool3 = ((Iterator)localObject2).hasNext();
        if (!bool3) {
          break;
        }
        localObject1 = (d)((Iterator)localObject2).next();
        f4 = ((d)localObject1).c();
        f1 += f4;
      }
      int i3 = b.size();
      f4 = i3;
      f4 = f1 / f4;
      localObject2 = b;
      locala = new in/juspay/widget/qrscanner/com/google/zxing/b/b/e$a;
      f3 = 0.0F;
      locala.<init>(f4, null);
      Collections.sort((List)localObject2, locala);
      localObject1 = b;
      localObject2 = b;
      n = ((List)localObject2).size();
      localObject1 = ((List)localObject1).subList(k, n);
      ((List)localObject1).clear();
    }
    Object localObject2 = new d[k];
    localObject1 = (d)b.get(0);
    localObject2[0] = localObject1;
    localObject1 = (d)b.get(j);
    localObject2[j] = localObject1;
    localObject1 = (d)b.get(i);
    localObject2[i] = localObject1;
    return (d[])localObject2;
  }
  
  final f a(Map paramMap)
  {
    Object localObject1;
    boolean bool1;
    label47:
    Object localObject2;
    int k;
    int m;
    int i2;
    int i3;
    int i;
    int[] arrayOfInt;
    int i4;
    label165:
    b localb;
    boolean bool5;
    if (paramMap != null)
    {
      localObject1 = in.juspay.widget.qrscanner.com.google.zxing.d.d;
      bool1 = paramMap.containsKey(localObject1);
      if (bool1)
      {
        bool1 = true;
        boolean bool2 = bool1;
        if (paramMap == null) {
          break label251;
        }
        localObject1 = in.juspay.widget.qrscanner.com.google.zxing.d.b;
        bool1 = paramMap.containsKey(localObject1);
        if (!bool1) {
          break label251;
        }
        bool1 = true;
        int j = a.d();
        localObject2 = a;
        k = ((b)localObject2).c();
        m = j * 3 / 228;
        i2 = 3;
        if ((m < i2) || (bool2)) {
          m = 3;
        }
        i3 = 0;
        i = 5;
        arrayOfInt = new int[i];
        i2 = m + -1;
        i4 = m;
        if ((i2 >= j) || (i3 != 0)) {
          break label672;
        }
        arrayOfInt[0] = 0;
        arrayOfInt[1] = 0;
        arrayOfInt[2] = 0;
        arrayOfInt[3] = 0;
        arrayOfInt[4] = 0;
        m = 0;
        localObject2 = null;
        i = 0;
        if (i >= k) {
          break label606;
        }
        localb = a;
        boolean bool4 = localb.a(i, i2);
        if (!bool4) {
          break label258;
        }
        bool4 = m & 0x1;
        bool5 = true;
        if (bool4 == bool5) {
          m += 1;
        }
        i5 = arrayOfInt[m] + 1;
        arrayOfInt[m] = i5;
      }
    }
    for (;;)
    {
      label232:
      i += 1;
      break label165;
      bool1 = false;
      localObject1 = null;
      i = 0;
      break;
      label251:
      bool1 = false;
      localObject1 = null;
      break label47;
      label258:
      i5 = m & 0x1;
      if (i5 != 0) {
        break label587;
      }
      i5 = 4;
      if (m != i5) {
        break label562;
      }
      int n = a(arrayOfInt);
      if (n == 0) {
        break label501;
      }
      n = a(arrayOfInt, i2, i, bool1);
      if (n == 0) {
        break label440;
      }
      i4 = 2;
      n = c;
      if (n == 0) {
        break label377;
      }
      n = c();
      label329:
      localb = null;
      arrayOfInt[0] = 0;
      arrayOfInt[1] = 0;
      arrayOfInt[2] = 0;
      arrayOfInt[3] = 0;
      i5 = 4;
      bool5 = false;
      arrayOfInt[i5] = 0;
      i3 = n;
      n = 0;
      localObject2 = null;
    }
    label377:
    int i1 = b();
    int i5 = arrayOfInt[2];
    if (i1 > i5)
    {
      i = arrayOfInt[2];
      i1 = i1 - i - i4;
      i = i2 + i1;
      i1 = k + -1;
    }
    for (;;)
    {
      i2 = i;
      i = i1;
      i1 = i3;
      break label329;
      label440:
      localObject2 = null;
      i5 = arrayOfInt[2];
      arrayOfInt[0] = i5;
      i5 = arrayOfInt[3];
      arrayOfInt[1] = i5;
      i5 = arrayOfInt[4];
      arrayOfInt[2] = i5;
      arrayOfInt[3] = 1;
      i5 = 0;
      localb = null;
      arrayOfInt[4] = 0;
      i1 = 3;
      break label232;
      label501:
      localObject2 = null;
      i5 = arrayOfInt[2];
      arrayOfInt[0] = i5;
      i5 = arrayOfInt[3];
      arrayOfInt[1] = i5;
      i5 = arrayOfInt[4];
      arrayOfInt[2] = i5;
      arrayOfInt[3] = 1;
      i5 = 0;
      localb = null;
      arrayOfInt[4] = 0;
      i1 = 3;
      break label232;
      label562:
      i1 += 1;
      i5 = arrayOfInt[i1] + 1;
      arrayOfInt[i1] = i5;
      break label232;
      label587:
      i5 = arrayOfInt[i1] + 1;
      arrayOfInt[i1] = i5;
      break label232;
      label606:
      boolean bool3 = a(arrayOfInt);
      if (bool3)
      {
        bool3 = a(arrayOfInt, i2, k, bool1);
        if (bool3)
        {
          localObject2 = null;
          i4 = arrayOfInt[0];
          bool3 = c;
          if (bool3) {
            i3 = c();
          }
        }
      }
      i2 += i4;
      break;
      label672:
      localObject1 = d();
      m.a((m[])localObject1);
      localObject2 = new in/juspay/widget/qrscanner/com/google/zxing/b/b/f;
      ((f)localObject2).<init>((d[])localObject1);
      return (f)localObject2;
      bool3 = i;
      i = i2;
    }
  }
  
  protected final boolean a(int[] paramArrayOfInt, int paramInt1, int paramInt2, boolean paramBoolean)
  {
    boolean bool1 = true;
    int i = 2;
    boolean bool3 = false;
    List localList = null;
    int j = paramArrayOfInt[0];
    int k = paramArrayOfInt[bool1];
    j += k;
    k = paramArrayOfInt[i];
    j += k;
    k = paramArrayOfInt[3];
    j += k;
    k = paramArrayOfInt[4];
    j += k;
    float f1 = a(paramArrayOfInt, paramInt2);
    int i1 = (int)f1;
    int i2 = paramArrayOfInt[i];
    float f2 = b(paramInt1, i1, i2, j);
    boolean bool5 = Float.isNaN(f2);
    float f3;
    int n;
    float f5;
    n localn;
    if (!bool5)
    {
      k = (int)f1;
      int i3 = (int)f2;
      int i4 = paramArrayOfInt[i];
      f3 = c(k, i3, i4, j);
      boolean bool4 = Float.isNaN(f3);
      if (!bool4) {
        if (paramBoolean)
        {
          int m = (int)f2;
          i4 = (int)f3;
          i = paramArrayOfInt[i];
          n = a(m, i4, i, j);
          if (n == 0) {}
        }
        else
        {
          float f4 = j;
          f5 = f4 / 7.0F;
          n = 0;
          localn = null;
          f1 = 0.0F;
        }
      }
    }
    for (;;)
    {
      Object localObject = b;
      j = ((List)localObject).size();
      if (n < j)
      {
        localObject = (d)b.get(n);
        boolean bool2 = ((d)localObject).a(f5, f2, f3);
        if (bool2)
        {
          localList = b;
          localObject = ((d)localObject).b(f2, f3, f5);
          localList.set(n, localObject);
          bool3 = bool1;
        }
      }
      else
      {
        if (!bool3)
        {
          localObject = new in/juspay/widget/qrscanner/com/google/zxing/b/b/d;
          ((d)localObject).<init>(f3, f2, f5);
          b.add(localObject);
          localn = e;
          if (localn != null)
          {
            localn = e;
            localn.a((m)localObject);
          }
        }
        bool3 = bool1;
        return bool3;
      }
      j = n + 1;
      n = j;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/b/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */