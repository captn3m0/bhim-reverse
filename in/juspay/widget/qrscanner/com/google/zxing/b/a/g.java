package in.juspay.widget.qrscanner.com.google.zxing.b.a;

final class g
{
  private static final int[][] a;
  private final f b;
  private final byte c;
  
  static
  {
    int i = 2;
    int[][] arrayOfInt = new int[32][];
    int[] arrayOfInt1 = new int[i];
    int[] tmp13_12 = arrayOfInt1;
    tmp13_12[0] = '吒';
    tmp13_12[1] = 0;
    arrayOfInt[0] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp31_30 = arrayOfInt1;
    tmp31_30[0] = '儥';
    tmp31_30[1] = 1;
    arrayOfInt[1] = arrayOfInt1;
    int[] arrayOfInt2 = new int[i];
    int[] tmp49_48 = arrayOfInt2;
    tmp49_48[0] = '幼';
    tmp49_48[1] = 2;
    arrayOfInt[i] = arrayOfInt2;
    arrayOfInt1 = new int[i];
    int[] tmp67_66 = arrayOfInt1;
    tmp67_66[0] = '孋';
    tmp67_66[1] = 3;
    arrayOfInt[3] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp85_84 = arrayOfInt1;
    tmp85_84[0] = '䗹';
    tmp85_84[1] = 4;
    arrayOfInt[4] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp103_102 = arrayOfInt1;
    tmp103_102[0] = '䃎';
    tmp103_102[1] = 5;
    arrayOfInt[5] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp121_120 = arrayOfInt1;
    tmp121_120[0] = '侗';
    tmp121_120[1] = 6;
    arrayOfInt[6] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp141_140 = arrayOfInt1;
    tmp141_140[0] = '䪠';
    tmp141_140[1] = 7;
    arrayOfInt[7] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp161_160 = arrayOfInt1;
    tmp161_160[0] = '矄';
    tmp161_160[1] = 8;
    arrayOfInt[8] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp181_180 = arrayOfInt1;
    tmp181_180[0] = '狳';
    tmp181_180[1] = 9;
    arrayOfInt[9] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp201_200 = arrayOfInt1;
    tmp201_200[0] = '綪';
    tmp201_200[1] = 10;
    arrayOfInt[10] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp221_220 = arrayOfInt1;
    tmp221_220[0] = '碝';
    tmp221_220[1] = 11;
    arrayOfInt[11] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp241_240 = arrayOfInt1;
    tmp241_240[0] = '是';
    tmp241_240[1] = 12;
    arrayOfInt[12] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp261_260 = arrayOfInt1;
    tmp261_260[0] = '挘';
    tmp261_260[1] = 13;
    arrayOfInt[13] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp281_280 = arrayOfInt1;
    tmp281_280[0] = '汁';
    tmp281_280[1] = 14;
    arrayOfInt[14] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp301_300 = arrayOfInt1;
    tmp301_300[0] = '楶';
    tmp301_300[1] = 15;
    arrayOfInt[15] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp321_320 = arrayOfInt1;
    tmp321_320[0] = 'ᚉ';
    tmp321_320[1] = 16;
    arrayOfInt[16] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp341_340 = arrayOfInt1;
    tmp341_340[0] = 'Ꮎ';
    tmp341_340[1] = 17;
    arrayOfInt[17] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp361_360 = arrayOfInt1;
    tmp361_360[0] = '᳧';
    tmp361_360[1] = 18;
    arrayOfInt[18] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp381_380 = arrayOfInt1;
    tmp381_380[0] = '᧐';
    tmp381_380[1] = 19;
    arrayOfInt[19] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp401_400 = arrayOfInt1;
    tmp401_400[0] = 'ݢ';
    tmp401_400[1] = 20;
    arrayOfInt[20] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp421_420 = arrayOfInt1;
    tmp421_420[0] = 'ɕ';
    tmp421_420[1] = 21;
    arrayOfInt[21] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp441_440 = arrayOfInt1;
    tmp441_440[0] = 'ഌ';
    tmp441_440[1] = 22;
    arrayOfInt[22] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp461_460 = arrayOfInt1;
    tmp461_460[0] = '࠻';
    tmp461_460[1] = 23;
    arrayOfInt[23] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp481_480 = arrayOfInt1;
    tmp481_480[0] = '㕟';
    tmp481_480[1] = 24;
    arrayOfInt[24] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp501_500 = arrayOfInt1;
    tmp501_500[0] = 'と';
    tmp501_500[1] = 25;
    arrayOfInt[25] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp521_520 = arrayOfInt1;
    tmp521_520[0] = '㼱';
    tmp521_520[1] = 26;
    arrayOfInt[26] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp541_540 = arrayOfInt1;
    tmp541_540[0] = '㨆';
    tmp541_540[1] = 27;
    arrayOfInt[27] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp561_560 = arrayOfInt1;
    tmp561_560[0] = '⒴';
    tmp561_560[1] = 28;
    arrayOfInt[28] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp581_580 = arrayOfInt1;
    tmp581_580[0] = 'Ↄ';
    tmp581_580[1] = 29;
    arrayOfInt[29] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp601_600 = arrayOfInt1;
    tmp601_600[0] = '⻚';
    tmp601_600[1] = 30;
    arrayOfInt[30] = arrayOfInt1;
    arrayOfInt1 = new int[i];
    int[] tmp621_620 = arrayOfInt1;
    tmp621_620[0] = '⯭';
    tmp621_620[1] = 31;
    arrayOfInt[31] = arrayOfInt1;
    a = arrayOfInt;
  }
  
  private g(int paramInt)
  {
    f localf = f.a(paramInt >> 3 & 0x3);
    b = localf;
    byte b1 = (byte)(paramInt & 0x7);
    c = b1;
  }
  
  static int a(int paramInt1, int paramInt2)
  {
    return Integer.bitCount(paramInt1 ^ paramInt2);
  }
  
  static g b(int paramInt1, int paramInt2)
  {
    g localg = c(paramInt1, paramInt2);
    if (localg != null) {}
    for (;;)
    {
      return localg;
      int i = paramInt1 ^ 0x5412;
      int j = paramInt2 ^ 0x5412;
      localg = c(i, j);
    }
  }
  
  private static g c(int paramInt1, int paramInt2)
  {
    int i = 1;
    int j = -1 >>> 1;
    int[][] arrayOfInt = a;
    int k = arrayOfInt.length;
    int m = 0;
    int n = 0;
    int[] arrayOfInt1;
    int i1;
    g localg;
    label72:
    int i2;
    if (m < k)
    {
      arrayOfInt1 = arrayOfInt[m];
      i1 = arrayOfInt1[0];
      if ((i1 == paramInt1) || (i1 == paramInt2))
      {
        localg = new in/juspay/widget/qrscanner/com/google/zxing/b/a/g;
        n = arrayOfInt1[i];
        localg.<init>(n);
        return localg;
      }
      i2 = a(paramInt1, i1);
      if (i2 >= j) {
        break label181;
      }
    }
    for (j = arrayOfInt1[i];; j = n)
    {
      if (paramInt1 != paramInt2)
      {
        n = a(paramInt2, i1);
        if (n < i2) {
          j = arrayOfInt1[i];
        }
      }
      for (;;)
      {
        i2 = m + 1;
        m = i2;
        int i3 = j;
        j = n;
        n = i3;
        break;
        i2 = 3;
        if (j <= i2)
        {
          localg = new in/juspay/widget/qrscanner/com/google/zxing/b/a/g;
          localg.<init>(n);
          break label72;
        }
        j = 0;
        localg = null;
        break label72;
        n = i2;
      }
      label181:
      i2 = j;
    }
  }
  
  f a()
  {
    return b;
  }
  
  byte b()
  {
    return c;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    boolean bool2 = paramObject instanceof g;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      paramObject = (g)paramObject;
      f localf1 = b;
      f localf2 = b;
      if (localf1 == localf2)
      {
        int i = c;
        int j = c;
        if (i == j) {
          bool1 = true;
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = b.ordinal() << 3;
    int j = c;
    return i | j;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/a/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */