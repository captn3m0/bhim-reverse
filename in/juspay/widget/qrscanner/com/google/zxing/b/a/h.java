package in.juspay.widget.qrscanner.com.google.zxing.b.a;

public enum h
{
  private final int[] k;
  private final int l;
  
  static
  {
    int n = 4;
    int i1 = 2;
    int i2 = 1;
    int i3 = 3;
    Object localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    Object localObject2 = new int[i3];
    Object tmp20_18 = localObject2;
    tmp20_18[0] = 0;
    Object tmp24_20 = tmp20_18;
    tmp24_20[1] = 0;
    tmp24_20[2] = 0;
    ((h)localObject1).<init>("TERMINATOR", 0, (int[])localObject2, 0);
    a = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    localObject2 = new int[i3];
    Object tmp59_57 = localObject2;
    tmp59_57[0] = 10;
    Object tmp64_59 = tmp59_57;
    tmp64_59[1] = 12;
    tmp64_59[2] = 14;
    ((h)localObject1).<init>("NUMERIC", i2, (int[])localObject2, i2);
    b = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    localObject2 = new int[i3];
    Object tmp101_99 = localObject2;
    tmp101_99[0] = 9;
    Object tmp106_101 = tmp101_99;
    tmp106_101[1] = 11;
    tmp106_101[2] = 13;
    ((h)localObject1).<init>("ALPHANUMERIC", i1, (int[])localObject2, i1);
    c = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    localObject2 = new int[i3];
    Object tmp143_141 = localObject2;
    tmp143_141[0] = 0;
    Object tmp147_143 = tmp143_141;
    tmp147_143[1] = 0;
    tmp147_143[2] = 0;
    ((h)localObject1).<init>("STRUCTURED_APPEND", i3, (int[])localObject2, i3);
    d = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    localObject2 = new int[i3];
    Object tmp182_180 = localObject2;
    tmp182_180[0] = 8;
    Object tmp187_182 = tmp182_180;
    tmp187_182[1] = 16;
    tmp187_182[2] = 16;
    ((h)localObject1).<init>("BYTE", n, (int[])localObject2, n);
    e = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    int[] arrayOfInt = new int[i3];
    int[] tmp224_222 = arrayOfInt;
    tmp224_222[0] = 0;
    int[] tmp228_224 = tmp224_222;
    tmp228_224[1] = 0;
    tmp228_224[2] = 0;
    ((h)localObject1).<init>("ECI", 5, arrayOfInt, 7);
    f = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    arrayOfInt = new int[i3];
    int[] tmp264_262 = arrayOfInt;
    tmp264_262[0] = 8;
    int[] tmp269_264 = tmp264_262;
    tmp269_264[1] = 10;
    tmp269_264[2] = 12;
    ((h)localObject1).<init>("KANJI", 6, arrayOfInt, 8);
    g = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    arrayOfInt = new int[i3];
    int[] tmp308_306 = arrayOfInt;
    tmp308_306[0] = 0;
    int[] tmp312_308 = tmp308_306;
    tmp312_308[1] = 0;
    tmp312_308[2] = 0;
    ((h)localObject1).<init>("FNC1_FIRST_POSITION", 7, arrayOfInt, 5);
    h = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    arrayOfInt = new int[i3];
    int[] tmp348_346 = arrayOfInt;
    tmp348_346[0] = 0;
    int[] tmp352_348 = tmp348_346;
    tmp352_348[1] = 0;
    tmp352_348[2] = 0;
    ((h)localObject1).<init>("FNC1_SECOND_POSITION", 8, arrayOfInt, 9);
    i = (h)localObject1;
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/a/h;
    arrayOfInt = new int[i3];
    int[] tmp389_387 = arrayOfInt;
    tmp389_387[0] = 8;
    int[] tmp394_389 = tmp389_387;
    tmp394_389[1] = 10;
    tmp394_389[2] = 12;
    ((h)localObject1).<init>("HANZI", 9, arrayOfInt, 13);
    j = (h)localObject1;
    localObject1 = new h[10];
    h localh = a;
    localObject1[0] = localh;
    localh = b;
    localObject1[i2] = localh;
    localh = c;
    localObject1[i1] = localh;
    localh = d;
    localObject1[i3] = localh;
    localh = e;
    localObject1[n] = localh;
    localObject2 = f;
    localObject1[5] = localObject2;
    localObject2 = g;
    localObject1[6] = localObject2;
    localObject2 = h;
    localObject1[7] = localObject2;
    localObject2 = i;
    localObject1[8] = localObject2;
    localObject2 = j;
    localObject1[9] = localObject2;
    m = (h[])localObject1;
  }
  
  private h(int[] paramArrayOfInt, int paramInt1)
  {
    k = paramArrayOfInt;
    l = paramInt1;
  }
  
  public static h a(int paramInt)
  {
    Object localObject;
    switch (paramInt)
    {
    case 6: 
    case 10: 
    case 11: 
    case 12: 
    default: 
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>();
      throw ((Throwable)localObject);
    case 0: 
      localObject = a;
    }
    for (;;)
    {
      return (h)localObject;
      localObject = b;
      continue;
      localObject = c;
      continue;
      localObject = d;
      continue;
      localObject = e;
      continue;
      localObject = h;
      continue;
      localObject = f;
      continue;
      localObject = g;
      continue;
      localObject = i;
      continue;
      localObject = j;
    }
  }
  
  public int a()
  {
    return l;
  }
  
  public int a(j paramj)
  {
    int n = paramj.a();
    int i1 = 9;
    if (n <= i1) {
      n = 0;
    }
    for (;;)
    {
      return k[n];
      i1 = 26;
      if (n <= i1) {
        n = 1;
      } else {
        n = 2;
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/b/a/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */