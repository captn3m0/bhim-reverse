package in.juspay.widget.qrscanner.com.google.zxing;

public final class ChecksumException
  extends ReaderException
{
  private static final ChecksumException c;
  
  static
  {
    ChecksumException localChecksumException = new in/juspay/widget/qrscanner/com/google/zxing/ChecksumException;
    localChecksumException.<init>();
    c = localChecksumException;
    localChecksumException = c;
    StackTraceElement[] arrayOfStackTraceElement = b;
    localChecksumException.setStackTrace(arrayOfStackTraceElement);
  }
  
  public static ChecksumException a()
  {
    boolean bool = a;
    ChecksumException localChecksumException;
    if (bool)
    {
      localChecksumException = new in/juspay/widget/qrscanner/com/google/zxing/ChecksumException;
      localChecksumException.<init>();
    }
    for (;;)
    {
      return localChecksumException;
      localChecksumException = c;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/ChecksumException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */