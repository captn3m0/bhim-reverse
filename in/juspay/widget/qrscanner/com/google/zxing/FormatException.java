package in.juspay.widget.qrscanner.com.google.zxing;

public final class FormatException
  extends ReaderException
{
  private static final FormatException c;
  
  static
  {
    FormatException localFormatException = new in/juspay/widget/qrscanner/com/google/zxing/FormatException;
    localFormatException.<init>();
    c = localFormatException;
    localFormatException = c;
    StackTraceElement[] arrayOfStackTraceElement = b;
    localFormatException.setStackTrace(arrayOfStackTraceElement);
  }
  
  public static FormatException a()
  {
    boolean bool = a;
    FormatException localFormatException;
    if (bool)
    {
      localFormatException = new in/juspay/widget/qrscanner/com/google/zxing/FormatException;
      localFormatException.<init>();
    }
    for (;;)
    {
      return localFormatException;
      localFormatException = c;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/FormatException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */