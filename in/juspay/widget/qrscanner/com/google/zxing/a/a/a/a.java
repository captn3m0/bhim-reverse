package in.juspay.widget.qrscanner.com.google.zxing.a.a.a;

import android.graphics.Rect;
import android.hardware.Camera.Area;
import android.hardware.Camera.Parameters;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.d.a;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

public final class a
{
  private static final Pattern a = Pattern.compile(";");
  
  private static String a(String paramString, Collection paramCollection, String... paramVarArgs)
  {
    int j;
    String str;
    int k;
    if (paramCollection != null)
    {
      int i = paramVarArgs.length;
      j = 0;
      str = null;
      k = 0;
      if (k < i)
      {
        str = paramVarArgs[k];
        boolean bool = paramCollection.contains(str);
        if (!bool) {}
      }
    }
    for (;;)
    {
      return str;
      j = k + 1;
      k = j;
      break;
      j = 0;
      str = null;
    }
  }
  
  private static List a(int paramInt)
  {
    Camera.Area localArea = new android/hardware/Camera$Area;
    Rect localRect = new android/graphics/Rect;
    int i = -paramInt;
    int j = -paramInt;
    localRect.<init>(i, j, paramInt, paramInt);
    localArea.<init>(localRect, 1);
    return Collections.singletonList(localArea);
  }
  
  public static void a(Camera.Parameters paramParameters)
  {
    a(paramParameters, 10, 20);
  }
  
  public static void a(Camera.Parameters paramParameters, int paramInt1, int paramInt2)
  {
    int i = 1;
    Object localObject = paramParameters.getSupportedPreviewFpsRange();
    boolean bool1;
    int[] arrayOfInt;
    if (localObject != null)
    {
      bool1 = ((List)localObject).isEmpty();
      if (!bool1)
      {
        bool1 = false;
        arrayOfInt = null;
        Iterator localIterator = ((List)localObject).iterator();
        int m;
        int n;
        do
        {
          int i1;
          do
          {
            boolean bool2 = localIterator.hasNext();
            if (!bool2) {
              break;
            }
            localObject = (int[])localIterator.next();
            m = localObject[0];
            n = localObject[i];
            i1 = paramInt1 * 1000;
          } while (m < i1);
          m = paramInt2 * 1000;
        } while (n > m);
      }
    }
    for (;;)
    {
      if (localObject == null) {}
      for (;;)
      {
        return;
        arrayOfInt = new int[2];
        paramParameters.getPreviewFpsRange(arrayOfInt);
        bool1 = Arrays.equals(arrayOfInt, (int[])localObject);
        if (!bool1)
        {
          int j = localObject[0];
          k = localObject[i];
          paramParameters.setPreviewFpsRange(j, k);
        }
      }
      int k = 0;
      localObject = null;
    }
  }
  
  public static void a(Camera.Parameters paramParameters, d.a parama, boolean paramBoolean)
  {
    int i = 2;
    int j = 1;
    Object localObject1 = paramParameters.getSupportedFocusModes();
    String str1 = null;
    Object localObject2;
    String str2;
    if (!paramBoolean)
    {
      localObject2 = d.a.a;
      if (parama != localObject2) {}
    }
    else
    {
      localObject2 = new String[j];
      str2 = "auto";
      localObject2[0] = str2;
      str1 = a("focus mode", (Collection)localObject1, (String[])localObject2);
      if ((!paramBoolean) && (str1 == null))
      {
        localObject2 = new String[i];
        localObject2[0] = "macro";
        str2 = "edof";
        localObject2[j] = str2;
        str1 = a("focus mode", (Collection)localObject1, (String[])localObject2);
      }
      if (str1 != null)
      {
        localObject1 = paramParameters.getFocusMode();
        boolean bool = str1.equals(localObject1);
        if (!bool) {
          break label268;
        }
      }
    }
    for (;;)
    {
      return;
      localObject2 = d.a.b;
      if (parama == localObject2)
      {
        int k = 3;
        localObject2 = new String[k];
        localObject2[0] = "continuous-picture";
        localObject2[j] = "continuous-video";
        str2 = "auto";
        localObject2[i] = str2;
        str1 = a("focus mode", (Collection)localObject1, (String[])localObject2);
        break;
      }
      localObject2 = d.a.c;
      if (parama == localObject2)
      {
        localObject2 = new String[j];
        str2 = "infinity";
        localObject2[0] = str2;
        str1 = a("focus mode", (Collection)localObject1, (String[])localObject2);
        break;
      }
      localObject2 = d.a.d;
      if (parama != localObject2) {
        break;
      }
      localObject2 = new String[j];
      str2 = "macro";
      localObject2[0] = str2;
      str1 = a("focus mode", (Collection)localObject1, (String[])localObject2);
      break;
      label268:
      paramParameters.setFocusMode(str1);
    }
  }
  
  public static void a(Camera.Parameters paramParameters, boolean paramBoolean)
  {
    int i = 1;
    Object localObject = paramParameters.getSupportedFlashModes();
    String str1;
    String[] arrayOfString;
    String str2;
    if (paramBoolean)
    {
      str1 = "flash mode";
      int j = 2;
      arrayOfString = new String[j];
      arrayOfString[0] = "torch";
      str2 = "on";
      arrayOfString[i] = str2;
      localObject = a(str1, (Collection)localObject, arrayOfString);
      if (localObject != null)
      {
        str1 = paramParameters.getFlashMode();
        boolean bool = ((String)localObject).equals(str1);
        if (!bool) {
          break label106;
        }
      }
    }
    for (;;)
    {
      return;
      str1 = "flash mode";
      arrayOfString = new String[i];
      str2 = "off";
      arrayOfString[0] = str2;
      localObject = a(str1, (Collection)localObject, arrayOfString);
      break;
      label106:
      paramParameters.setFlashMode((String)localObject);
    }
  }
  
  public static void b(Camera.Parameters paramParameters)
  {
    int i = paramParameters.getMaxNumFocusAreas();
    if (i > 0)
    {
      i = 400;
      List localList = a(i);
      paramParameters.setFocusAreas(localList);
    }
  }
  
  public static void b(Camera.Parameters paramParameters, boolean paramBoolean)
  {
    int i = 0;
    float f1 = 0.0F;
    int j = paramParameters.getMinExposureCompensation();
    int k = paramParameters.getMaxExposureCompensation();
    float f2 = paramParameters.getExposureCompensationStep();
    if ((j != 0) || (k != 0))
    {
      boolean bool = f2 < 0.0F;
      if (bool)
      {
        if (!paramBoolean) {
          break label93;
        }
        f1 /= f2;
        i = Math.round(f1);
        float f3 = i;
        f2 *= f3;
        i = Math.max(Math.min(i, k), j);
        j = paramParameters.getExposureCompensation();
        if (j != i) {
          break label102;
        }
      }
    }
    for (;;)
    {
      return;
      label93:
      i = 1069547520;
      f1 = 1.5F;
      break;
      label102:
      paramParameters.setExposureCompensation(i);
    }
  }
  
  public static void c(Camera.Parameters paramParameters)
  {
    int i = paramParameters.getMaxNumMeteringAreas();
    if (i > 0)
    {
      i = 400;
      List localList = a(i);
      paramParameters.setMeteringAreas(localList);
    }
  }
  
  public static void d(Camera.Parameters paramParameters)
  {
    boolean bool = paramParameters.isVideoStabilizationSupported();
    if (bool)
    {
      bool = paramParameters.getVideoStabilization();
      if (!bool) {
        break label19;
      }
    }
    for (;;)
    {
      return;
      label19:
      bool = true;
      paramParameters.setVideoStabilization(bool);
    }
  }
  
  public static void e(Camera.Parameters paramParameters)
  {
    String str1 = "barcode";
    Object localObject = paramParameters.getSceneMode();
    boolean bool = str1.equals(localObject);
    if (bool) {}
    for (;;)
    {
      return;
      localObject = paramParameters.getSupportedSceneModes();
      int i = 1;
      String[] arrayOfString = new String[i];
      String str2 = "barcode";
      arrayOfString[0] = str2;
      str1 = a("scene mode", (Collection)localObject, arrayOfString);
      if (str1 != null) {
        paramParameters.setSceneMode(str1);
      }
    }
  }
  
  public static void f(Camera.Parameters paramParameters)
  {
    String str1 = "negative";
    Object localObject = paramParameters.getColorEffect();
    boolean bool = str1.equals(localObject);
    if (bool) {}
    for (;;)
    {
      return;
      localObject = paramParameters.getSupportedColorEffects();
      int i = 1;
      String[] arrayOfString = new String[i];
      String str2 = "negative";
      arrayOfString[0] = str2;
      str1 = a("color effect", (Collection)localObject, arrayOfString);
      if (str1 != null) {
        paramParameters.setColorEffect(str1);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/a/a/a/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */