package in.juspay.widget.qrscanner.com.google.zxing.a.a;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;

public final class c
{
  private static final String a = c.class.getSimpleName();
  private final Context b;
  private final BroadcastReceiver c;
  private boolean d = false;
  private Handler e;
  private Runnable f;
  private boolean g;
  
  public c(Context paramContext, Runnable paramRunnable)
  {
    b = paramContext;
    f = paramRunnable;
    Object localObject = new in/juspay/widget/qrscanner/com/google/zxing/a/a/c$a;
    ((c.a)localObject).<init>(this, null);
    c = ((BroadcastReceiver)localObject);
    localObject = new android/os/Handler;
    ((Handler)localObject).<init>();
    e = ((Handler)localObject);
  }
  
  private void a(boolean paramBoolean)
  {
    g = paramBoolean;
    boolean bool = d;
    if (bool) {
      a();
    }
  }
  
  private void d()
  {
    boolean bool = d;
    if (bool)
    {
      Context localContext = b;
      BroadcastReceiver localBroadcastReceiver = c;
      localContext.unregisterReceiver(localBroadcastReceiver);
      bool = false;
      localContext = null;
      d = false;
    }
  }
  
  private void e()
  {
    boolean bool = d;
    if (!bool)
    {
      Context localContext = b;
      BroadcastReceiver localBroadcastReceiver = c;
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      String str = "android.intent.action.BATTERY_CHANGED";
      localIntentFilter.<init>(str);
      localContext.registerReceiver(localBroadcastReceiver, localIntentFilter);
      bool = true;
      d = bool;
    }
  }
  
  private void f()
  {
    e.removeCallbacksAndMessages(null);
  }
  
  public void a()
  {
    f();
    boolean bool = g;
    if (bool)
    {
      Handler localHandler = e;
      Runnable localRunnable = f;
      long l = 300000L;
      localHandler.postDelayed(localRunnable, l);
    }
  }
  
  public void b()
  {
    e();
    a();
  }
  
  public void c()
  {
    f();
    d();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/a/a/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */