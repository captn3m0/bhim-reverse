package in.juspay.widget.qrscanner.com.google.zxing.a.a;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.c;
import in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.a.d;

public final class a
  implements SensorEventListener
{
  private c a;
  private d b;
  private Sensor c;
  private Context d;
  private Handler e;
  
  public a(Context paramContext, c paramc, d paramd)
  {
    d = paramContext;
    a = paramc;
    b = paramd;
    Handler localHandler = new android/os/Handler;
    localHandler.<init>();
    e = localHandler;
  }
  
  private void a(boolean paramBoolean)
  {
    Handler localHandler = e;
    a.1 local1 = new in/juspay/widget/qrscanner/com/google/zxing/a/a/a$1;
    local1.<init>(this, paramBoolean);
    localHandler.post(local1);
  }
  
  public void a()
  {
    Object localObject = b;
    boolean bool = ((d)localObject).h();
    if (bool)
    {
      localObject = (SensorManager)d.getSystemService("sensor");
      int i = 5;
      Sensor localSensor = ((SensorManager)localObject).getDefaultSensor(i);
      c = localSensor;
      localSensor = c;
      if (localSensor != null)
      {
        localSensor = c;
        int j = 3;
        ((SensorManager)localObject).registerListener(this, localSensor, j);
      }
    }
  }
  
  public void b()
  {
    Object localObject = c;
    if (localObject != null)
    {
      localObject = d;
      String str = "sensor";
      ((SensorManager)((Context)localObject).getSystemService(str)).unregisterListener(this);
      localObject = null;
      c = null;
    }
  }
  
  public void onAccuracyChanged(Sensor paramSensor, int paramInt) {}
  
  public void onSensorChanged(SensorEvent paramSensorEvent)
  {
    float[] arrayOfFloat = values;
    float f1 = arrayOfFloat[0];
    c localc = a;
    float f2;
    boolean bool2;
    if (localc != null)
    {
      f2 = 45.0F;
      boolean bool1 = f1 < f2;
      if (bool1) {
        break label48;
      }
      bool2 = true;
      f1 = Float.MIN_VALUE;
      a(bool2);
    }
    for (;;)
    {
      return;
      label48:
      int i = 1138819072;
      f2 = 450.0F;
      bool2 = f1 < f2;
      if (!bool2) {
        a(false);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/a/a/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */