package in.juspay.widget.qrscanner.com.google.zxing.a.a;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import in.juspay.widget.qrscanner.a.d;
import java.io.FileDescriptor;
import java.io.IOException;

public final class b
{
  private static final String a = b.class.getSimpleName();
  private final Context b;
  private boolean c = true;
  private boolean d = false;
  
  public b(Activity paramActivity)
  {
    paramActivity.setVolumeControlStream(3);
    Context localContext = paramActivity.getApplicationContext();
    b = localContext;
  }
  
  public MediaPlayer a()
  {
    localMediaPlayer = new android/media/MediaPlayer;
    localMediaPlayer.<init>();
    int i = 3;
    float f1 = 4.2E-45F;
    localMediaPlayer.setAudioStreamType(i);
    Object localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/a/a/b$1;
    ((b.1)localObject1).<init>(this);
    localMediaPlayer.setOnCompletionListener((MediaPlayer.OnCompletionListener)localObject1);
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/a/a/b$2;
    ((b.2)localObject1).<init>(this);
    localMediaPlayer.setOnErrorListener((MediaPlayer.OnErrorListener)localObject1);
    for (;;)
    {
      try
      {
        localObject1 = b;
        localObject1 = ((Context)localObject1).getResources();
        j = a.d.zxing_beep;
        localAssetFileDescriptor = ((Resources)localObject1).openRawResourceFd(j);
      }
      catch (IOException localIOException)
      {
        int j;
        AssetFileDescriptor localAssetFileDescriptor;
        long l1;
        long l2;
        float f2;
        localMediaPlayer.release();
        localMediaPlayer = null;
        continue;
      }
      try
      {
        localObject1 = localAssetFileDescriptor.getFileDescriptor();
        l1 = localAssetFileDescriptor.getStartOffset();
        l2 = localAssetFileDescriptor.getLength();
        localMediaPlayer.setDataSource((FileDescriptor)localObject1, l1, l2);
        localAssetFileDescriptor.close();
        i = 1036831949;
        f1 = 0.1F;
        j = 1036831949;
        f2 = 0.1F;
        localMediaPlayer.setVolume(f1, f2);
        localMediaPlayer.prepare();
        return localMediaPlayer;
      }
      finally
      {
        localAssetFileDescriptor.close();
      }
    }
  }
  
  public void a(boolean paramBoolean)
  {
    c = paramBoolean;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/a/a/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */