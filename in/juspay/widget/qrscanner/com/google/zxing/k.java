package in.juspay.widget.qrscanner.com.google.zxing;

import java.util.EnumMap;
import java.util.Map;

public final class k
{
  private final String a;
  private final byte[] b;
  private final int c;
  private m[] d;
  private final a e;
  private Map f;
  private final long g;
  
  public k(String paramString, byte[] paramArrayOfByte, int paramInt, m[] paramArrayOfm, a parama, long paramLong)
  {
    a = paramString;
    b = paramArrayOfByte;
    c = paramInt;
    d = paramArrayOfm;
    e = parama;
    f = null;
    g = paramLong;
  }
  
  public k(String paramString, byte[] paramArrayOfByte, m[] paramArrayOfm, a parama)
  {
    this(paramString, paramArrayOfByte, paramArrayOfm, parama, l);
  }
  
  public k(String paramString, byte[] paramArrayOfByte, m[] paramArrayOfm, a parama, long paramLong) {}
  
  public String a()
  {
    return a;
  }
  
  public void a(l paraml, Object paramObject)
  {
    Object localObject = f;
    if (localObject == null)
    {
      localObject = new java/util/EnumMap;
      Class localClass = l.class;
      ((EnumMap)localObject).<init>(localClass);
      f = ((Map)localObject);
    }
    f.put(paraml, paramObject);
  }
  
  public String toString()
  {
    return a;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/k.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */