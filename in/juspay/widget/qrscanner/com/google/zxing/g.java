package in.juspay.widget.qrscanner.com.google.zxing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public final class g
  implements j
{
  private Map a;
  private j[] b;
  
  private k c(c paramc)
  {
    j[] arrayOfj1 = b;
    if (arrayOfj1 != null)
    {
      j[] arrayOfj2 = b;
      int i = arrayOfj2.length;
      int j = 0;
      arrayOfj1 = null;
      while (j < i)
      {
        j localj = arrayOfj2[j];
        try
        {
          Map localMap = a;
          return localj.a(paramc, localMap);
        }
        catch (ReaderException localReaderException)
        {
          j += 1;
        }
      }
    }
    throw NotFoundException.a();
  }
  
  public k a(c paramc)
  {
    a(null);
    return c(paramc);
  }
  
  public k a(c paramc, Map paramMap)
  {
    a(paramMap);
    return c(paramc);
  }
  
  public void a()
  {
    j[] arrayOfj1 = b;
    if (arrayOfj1 != null)
    {
      j[] arrayOfj2 = b;
      int i = arrayOfj2.length;
      int j = 0;
      arrayOfj1 = null;
      while (j < i)
      {
        j localj = arrayOfj2[j];
        localj.a();
        j += 1;
      }
    }
  }
  
  public void a(Map paramMap)
  {
    a = paramMap;
    boolean bool;
    if (paramMap != null)
    {
      localObject = d.d;
      bool = paramMap.containsKey(localObject);
      if (!bool) {}
    }
    if (paramMap == null) {
      bool = false;
    }
    for (Object localObject = null;; localObject = (Collection)paramMap.get(localObject))
    {
      ArrayList localArrayList = new java/util/ArrayList;
      localArrayList.<init>();
      if (localObject != null)
      {
        a locala = a.l;
        bool = ((Collection)localObject).contains(locala);
        if (bool)
        {
          localObject = new in/juspay/widget/qrscanner/com/google/zxing/b/a;
          ((in.juspay.widget.qrscanner.com.google.zxing.b.a)localObject).<init>();
          localArrayList.add(localObject);
        }
      }
      bool = localArrayList.isEmpty();
      if (bool)
      {
        localObject = new in/juspay/widget/qrscanner/com/google/zxing/b/a;
        ((in.juspay.widget.qrscanner.com.google.zxing.b.a)localObject).<init>();
        localArrayList.add(localObject);
      }
      localObject = new j[localArrayList.size()];
      localObject = (j[])localArrayList.toArray((Object[])localObject);
      b = ((j[])localObject);
      return;
      localObject = d.c;
    }
  }
  
  public k b(c paramc)
  {
    j[] arrayOfj = b;
    if (arrayOfj == null)
    {
      arrayOfj = null;
      a(null);
    }
    return c(paramc);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */