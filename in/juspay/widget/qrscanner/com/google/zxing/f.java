package in.juspay.widget.qrscanner.com.google.zxing;

public abstract class f
{
  private final int a;
  private final int b;
  
  protected f(int paramInt1, int paramInt2)
  {
    a = paramInt1;
    b = paramInt2;
  }
  
  public abstract byte[] a();
  
  public abstract byte[] a(int paramInt, byte[] paramArrayOfByte);
  
  public final int b()
  {
    return a;
  }
  
  public final int c()
  {
    return b;
  }
  
  public final String toString()
  {
    byte[] arrayOfByte1 = new byte[a];
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    int i = b;
    int j = a + 1;
    i *= j;
    localStringBuilder.<init>(i);
    byte[] arrayOfByte2 = arrayOfByte1;
    int k = 0;
    arrayOfByte1 = null;
    for (;;)
    {
      j = b;
      if (k >= j) {
        break;
      }
      arrayOfByte2 = a(k, arrayOfByte2);
      j = 0;
      int n = a;
      if (j < n)
      {
        int i1 = arrayOfByte2[j] & 0xFF;
        int i3 = 64;
        if (i1 < i3) {
          i1 = 35;
        }
        for (;;)
        {
          localStringBuilder.append(i1);
          j += 1;
          break;
          int i4 = 128;
          int i2;
          if (i1 < i4)
          {
            i2 = 43;
          }
          else
          {
            int i5 = 192;
            if (i2 < i5) {
              i2 = 46;
            } else {
              i2 = 32;
            }
          }
        }
      }
      j = 10;
      localStringBuilder.append(j);
      int m;
      k += 1;
    }
    return localStringBuilder.toString();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/com/google/zxing/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */