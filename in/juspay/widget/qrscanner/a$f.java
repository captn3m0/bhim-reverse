package in.juspay.widget.qrscanner;

public final class a$f
{
  public static final int[] zxing_camera_preview;
  public static final int zxing_camera_preview_zxing_framing_rect_height = 1;
  public static final int zxing_camera_preview_zxing_framing_rect_width = 0;
  public static final int zxing_camera_preview_zxing_preview_scaling_strategy = 3;
  public static final int zxing_camera_preview_zxing_use_texture_view = 2;
  public static final int[] zxing_finder;
  public static final int zxing_finder_zxing_possible_result_points = 0;
  public static final int zxing_finder_zxing_result_view = 1;
  public static final int zxing_finder_zxing_viewfinder_laser = 2;
  public static final int zxing_finder_zxing_viewfinder_mask = 3;
  public static final int[] zxing_view;
  public static final int zxing_view_zxing_scanner_layout;
  
  static
  {
    int i = 4;
    int[] arrayOfInt = new int[i];
    int[] tmp7_6 = arrayOfInt;
    int[] tmp8_7 = tmp7_6;
    int[] tmp8_7 = tmp7_6;
    tmp8_7[0] = 2130772204;
    tmp8_7[1] = 2130772205;
    tmp8_7[2] = 2130772206;
    tmp8_7[3] = 2130772207;
    zxing_camera_preview = arrayOfInt;
    arrayOfInt = new int[i];
    int[] tmp34_33 = arrayOfInt;
    int[] tmp35_34 = tmp34_33;
    int[] tmp35_34 = tmp34_33;
    tmp35_34[0] = 2130772208;
    tmp35_34[1] = 2130772209;
    tmp35_34[2] = 2130772210;
    tmp35_34[3] = 2130772211;
    zxing_finder = arrayOfInt;
    arrayOfInt = new int[1];
    arrayOfInt[0] = 2130772212;
    zxing_view = arrayOfInt;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/widget/qrscanner/a$f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */