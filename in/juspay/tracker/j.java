package in.juspay.tracker;

import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class j
{
  private static String a = j.class.getName();
  private static j f = null;
  private String b;
  private String c;
  private DisplayMetrics d;
  private String e;
  private Map g;
  private String h;
  
  public j()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    g = localHashMap;
  }
  
  public static j a()
  {
    synchronized (j.class)
    {
      j localj = f;
      if (localj == null)
      {
        localj = new in/juspay/tracker/j;
        localj.<init>();
        f = localj;
      }
      localj = f;
      return localj;
    }
  }
  
  public static boolean e()
  {
    boolean bool1 = true;
    Object localObject = Build.TAGS;
    String str;
    boolean bool2;
    if (localObject != null)
    {
      str = "test-keys";
      bool2 = ((String)localObject).contains(str);
      if (!bool2) {}
    }
    for (;;)
    {
      return bool1;
      try
      {
        localObject = new java/io/File;
        str = "/system/app/Superuser.apk";
        ((File)localObject).<init>(str);
        bool2 = ((File)localObject).exists();
        if (bool2) {
          continue;
        }
      }
      catch (Exception localException)
      {
        for (;;) {}
      }
      bool1 = false;
    }
  }
  
  private DisplayMetrics i(Context paramContext)
  {
    try
    {
      localObject1 = d;
      if (localObject1 == null)
      {
        localObject1 = paramContext.getResources();
        localObject1 = ((Resources)localObject1).getDisplayMetrics();
        d = ((DisplayMetrics)localObject1);
      }
      localObject1 = d;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        String str1 = a;
        String str2 = "Exception caught trying to get display metrics";
        f.a(str1, str2, localException);
        Object localObject2 = null;
      }
    }
    return (DisplayMetrics)localObject1;
  }
  
  public String a(Context paramContext)
  {
    for (Object localObject1 = "wifi";; localObject1 = "cellular")
    {
      try
      {
        paramContext.getSystemService((String)localObject1);
        localObject1 = "connectivity";
        localObject1 = paramContext.getSystemService((String)localObject1);
        localObject1 = (ConnectivityManager)localObject1;
        int i = 1;
        localObject1 = ((ConnectivityManager)localObject1).getNetworkInfo(i);
        bool = ((NetworkInfo)localObject1).isConnected();
        if (!bool) {
          continue;
        }
        localObject1 = "wifi";
      }
      catch (Exception localException)
      {
        for (;;)
        {
          String str1 = a;
          String str2 = "Exception trying to get network info";
          f.a(str1, str2, localException);
          boolean bool = false;
          Object localObject2 = null;
        }
      }
      return (String)localObject1;
    }
  }
  
  public void a(String paramString)
  {
    b = paramString;
  }
  
  public boolean a(Context paramContext, String paramString)
  {
    boolean bool = false;
    for (;;)
    {
      try
      {
        localObject1 = paramContext.getPackageManager();
        localObject2 = paramContext.getPackageName();
        int i = ((PackageManager)localObject1).checkPermission(paramString, (String)localObject2);
        if (i != 0) {
          continue;
        }
        bool = true;
      }
      finally
      {
        Object localObject1;
        Object localObject2 = a;
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = ((StringBuilder)localObject3).append("Exception trying to fetch permission info: ").append(paramString);
        String str = " returning FALSE";
        localObject3 = str;
        f.a((String)localObject2, (String)localObject3, localThrowable);
        continue;
      }
      return bool;
      localObject1 = a;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject3 = "Permission not found: ";
      localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
      localObject2 = ((StringBuilder)localObject2).append(paramString);
      localObject2 = ((StringBuilder)localObject2).toString();
      f.a((String)localObject1, (String)localObject2);
    }
  }
  
  public int b(Context paramContext)
  {
    Object localObject = "phone";
    try
    {
      localObject = paramContext.getSystemService((String)localObject);
      localObject = (TelephonyManager)localObject;
      i = ((TelephonyManager)localObject).getNetworkType();
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1 = a;
        String str2 = "Exception trying to get network type";
        f.a(str1, str2, localException);
        int i = -1;
      }
    }
    return i;
  }
  
  public String b()
  {
    return b;
  }
  
  public void b(String paramString)
  {
    c = paramString;
  }
  
  public String c()
  {
    return c;
  }
  
  public String c(Context paramContext)
  {
    try
    {
      localObject1 = paramContext.getPackageManager();
      str1 = paramContext.getPackageName();
      str2 = null;
      localObject1 = ((PackageManager)localObject1).getPackageInfo(str1, 0);
      localObject1 = versionName;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        Object localObject1;
        String str1 = a;
        String str2 = "Exception trying to getVersionName";
        f.a(str1, str2, localNameNotFoundException);
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  public void c(String paramString)
  {
    e = paramString;
  }
  
  public String d()
  {
    return e;
  }
  
  public String d(Context paramContext)
  {
    Object localObject = i(paramContext);
    int i;
    if (localObject != null) {
      i = heightPixels;
    }
    for (localObject = String.valueOf(i);; localObject = null)
    {
      return (String)localObject;
      i = 0;
    }
  }
  
  public void d(String paramString)
  {
    h = paramString;
  }
  
  public String e(Context paramContext)
  {
    Object localObject = i(paramContext);
    int i;
    if (localObject != null) {
      i = widthPixels;
    }
    for (localObject = String.valueOf(i);; localObject = null)
    {
      return (String)localObject;
      i = 0;
    }
  }
  
  public String f(Context paramContext)
  {
    Object localObject = i(paramContext);
    float f1;
    if (localObject != null) {
      f1 = xdpi;
    }
    for (localObject = String.valueOf(f1);; localObject = null)
    {
      return (String)localObject;
      f1 = 0.0F;
    }
  }
  
  public String g(Context paramContext)
  {
    for (Object localObject1 = "android.permission.READ_PHONE_STATE";; localObject1 = null)
    {
      try
      {
        bool = a(paramContext, (String)localObject1);
        if (!bool) {
          break label47;
        }
        localObject1 = "phone";
        localObject1 = paramContext.getSystemService((String)localObject1);
        localObject1 = (TelephonyManager)localObject1;
        localObject3 = a.a();
        localObject1 = ((TelephonyManager)localObject1).getDeviceId();
        localObject1 = ((a)localObject3).a((String)localObject1);
      }
      catch (Exception localException)
      {
        for (;;)
        {
          label47:
          Object localObject3 = a;
          String str = "Exception trying to get device id";
          f.a((String)localObject3, str, localException);
          boolean bool = false;
          Object localObject2 = null;
        }
      }
      return (String)localObject1;
      bool = false;
    }
  }
  
  public boolean h(Context paramContext)
  {
    int i = 1;
    if (paramContext != null) {}
    for (;;)
    {
      try
      {
        Object localObject1 = paramContext.getContentResolver();
        String str = "development_settings_enabled";
        int j = Settings.Secure.getInt((ContentResolver)localObject1, str, 0);
        if (j == i) {
          return i;
        }
        i = 0;
        Object localObject2 = null;
        continue;
        i = 0;
      }
      catch (Exception localException)
      {
        localObject1 = a;
        str = "Exception while getting dev options enabled";
        f.a((String)localObject1, str, localException);
      }
      Object localObject3 = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/tracker/j.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */