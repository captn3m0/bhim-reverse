package in.juspay.tracker;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.util.Log;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import org.apache.http.conn.util.InetAddressUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class e
{
  private static final String a;
  private static long b;
  private static long c;
  private static long d;
  private static long e;
  private static List f;
  private static Map g;
  private static boolean p = false;
  private static e w = null;
  private Timer h;
  private TimerTask i;
  private e.c j;
  private int k = 0;
  private String l;
  private long m;
  private String n;
  private String o;
  private boolean q = true;
  private long r = 0L;
  private Timer s = null;
  private volatile boolean t = false;
  private e.a u;
  private String v;
  private Context x;
  private String y;
  
  static
  {
    long l1 = 3000L;
    a = e.class.getName();
    b = l1;
    c = 5000L;
    d = 10000L;
    e = l1;
    Object localObject = new java/util/concurrent/CopyOnWriteArrayList;
    ((CopyOnWriteArrayList)localObject).<init>();
    f = (List)localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    g = (Map)localObject;
  }
  
  private e()
  {
    Object localObject = e.a.a;
    u = ((e.a)localObject);
    localObject = UUID.randomUUID().toString();
    l = ((String)localObject);
    localObject = new java/util/Date;
    ((Date)localObject).<init>();
    long l1 = ((Date)localObject).getTime();
    m = l1;
    localObject = new in/juspay/tracker/e$b;
    ((e.b)localObject).<init>(this);
    i = ((TimerTask)localObject);
    localObject = new in/juspay/tracker/e$c;
    ((e.c)localObject).<init>(this);
    j = ((e.c)localObject);
  }
  
  public static e a()
  {
    synchronized (e.class)
    {
      e locale = w;
      if (locale == null)
      {
        locale = new in/juspay/tracker/e;
        locale.<init>();
        w = locale;
        locale = w;
        locale.l();
      }
      locale = w;
      return locale;
    }
  }
  
  public static void a(e.a parama)
  {
    w = null;
    w = a();
    wu = parama;
  }
  
  private boolean a(Map paramMap, JSONArray paramJSONArray)
  {
    boolean bool1 = false;
    int i1 = 0;
    for (;;)
    {
      int i2 = paramJSONArray.length();
      if (i1 < i2) {}
      try
      {
        JSONObject localJSONObject = paramJSONArray.getJSONObject(i1);
        Iterator localIterator = localJSONObject.keys();
        boolean bool2;
        do
        {
          boolean bool3;
          do
          {
            bool2 = localIterator.hasNext();
            if (!bool2) {
              break;
            }
            localObject1 = localIterator.next();
            localObject1 = (String)localObject1;
            bool3 = paramMap.containsKey(localObject1);
          } while (!bool3);
          Object localObject2 = paramMap.get(localObject1);
          localObject2 = (String)localObject2;
          Object localObject1 = localJSONObject.get((String)localObject1);
          bool2 = ((String)localObject2).equals(localObject1);
        } while (!bool2);
        bool1 = true;
        return bool1;
      }
      catch (JSONException localJSONException)
      {
        localJSONException.printStackTrace();
        int i3 = i1 + 1;
        i1 = i3;
      }
    }
  }
  
  private void b(Map paramMap)
  {
    Object localObject1 = l;
    paramMap.put("session_id", localObject1);
    localObject1 = n;
    paramMap.put("bank", localObject1);
    int i1 = k + 1;
    k = i1;
    localObject1 = String.valueOf(i1);
    paramMap.put("sn", localObject1);
    Object localObject2 = a;
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject1 = ((StringBuilder)localObject1).append("Analytics: ");
    String str = paramMap.toString();
    localObject1 = str;
    f.b((String)localObject2, (String)localObject1);
    boolean bool = c(paramMap);
    if (bool)
    {
      localObject2 = f;
      ((List)localObject2).add(paramMap);
    }
  }
  
  private boolean c(Map paramMap)
  {
    boolean bool1 = true;
    float f1 = Float.MIN_VALUE;
    boolean bool2 = false;
    float f2 = 0.0F;
    Object localObject1 = "log_level";
    boolean bool3 = paramMap.containsKey(localObject1);
    int i1;
    if (bool3)
    {
      localObject1 = Integer.valueOf((String)paramMap.get("log_level"));
      i1 = ((Integer)localObject1).intValue();
    }
    for (;;)
    {
      Object localObject2 = i.a();
      int i2 = ((i)localObject2).e();
      if (i1 < i2)
      {
        i1 = 0;
        localObject1 = null;
      }
      for (float f3 = 0.0F;; f3 = f1)
      {
        localObject2 = i.a().d();
        boolean bool5 = a(paramMap, (JSONArray)localObject2);
        if (bool5) {}
        for (;;)
        {
          localObject1 = i.a().c();
          bool4 = a(paramMap, (JSONArray)localObject1);
          if (bool4) {}
          for (;;)
          {
            return bool1;
            bool1 = bool2;
            f1 = f2;
          }
          bool2 = bool4;
          f2 = f3;
        }
        bool4 = bool1;
      }
      boolean bool4 = false;
      localObject1 = null;
      f3 = 0.0F;
    }
  }
  
  public static String e()
  {
    try
    {
      localObject1 = NetworkInterface.getNetworkInterfaces();
      localObject1 = Collections.list((Enumeration)localObject1);
      localObject2 = ((List)localObject1).iterator();
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break label128;
      }
      localObject1 = ((Iterator)localObject2).next();
      localObject1 = (NetworkInterface)localObject1;
      localObject1 = ((NetworkInterface)localObject1).getInetAddresses();
      localObject1 = Collections.list((Enumeration)localObject1);
      localObject3 = ((List)localObject1).iterator();
      boolean bool2;
      do
      {
        do
        {
          bool1 = ((Iterator)localObject3).hasNext();
          if (!bool1) {
            break;
          }
          localObject1 = ((Iterator)localObject3).next();
          localObject1 = (InetAddress)localObject1;
          bool2 = ((InetAddress)localObject1).isLoopbackAddress();
        } while (bool2);
        localObject1 = ((InetAddress)localObject1).getHostAddress();
        localObject1 = ((String)localObject1).toUpperCase();
        bool2 = InetAddressUtils.isIPv4Address((String)localObject1);
      } while (!bool2);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject2 = a;
        Object localObject3 = "Failed to Retreive IP address";
        f.a((String)localObject2, (String)localObject3, localException);
        label128:
        String str = "";
      }
    }
    return (String)localObject1;
  }
  
  private void k()
  {
    Timer localTimer = h;
    Object localObject;
    long l1;
    long l2;
    if (localTimer == null)
    {
      localTimer = new java/util/Timer;
      localTimer.<init>();
      h = localTimer;
      localTimer = h;
      localObject = i;
      l1 = b;
      l2 = c;
      localTimer.schedule((TimerTask)localObject, l1, l2);
    }
    localTimer = s;
    if (localTimer == null)
    {
      localTimer = new java/util/Timer;
      localTimer.<init>();
      s = localTimer;
      localTimer = s;
      localObject = j;
      l1 = b;
      l2 = e;
      localTimer.schedule((TimerTask)localObject, l1, l2);
    }
  }
  
  private void l()
  {
    Object localObject1 = w;
    if (localObject1 != null)
    {
      localObject1 = g.entrySet();
      Iterator localIterator = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (Map.Entry)localIterator.next();
        Object localObject2 = new in/juspay/tracker/b;
        ((b)localObject2).<init>();
        Object localObject3 = b.b.b;
        localObject2 = ((b)localObject2).a((b.b)localObject3);
        localObject3 = b.a.h;
        localObject3 = ((b)localObject2).a((b.a)localObject3);
        localObject2 = (String)((Map.Entry)localObject1).getKey();
        localObject2 = ((b)localObject3).b((String)localObject2);
        localObject1 = (String)((Map.Entry)localObject1).getValue();
        localObject1 = ((b)localObject2).c((String)localObject1);
        localObject2 = a();
        ((e)localObject2).a((b)localObject1);
      }
      localObject1 = g;
      ((Map)localObject1).clear();
    }
  }
  
  public void a(Context paramContext)
  {
    x = paramContext;
  }
  
  public void a(b paramb)
  {
    Object localObject = g.a().a(paramb);
    if (localObject == null) {}
    for (;;)
    {
      return;
      HashMap localHashMap = new java/util/HashMap;
      localHashMap.<init>();
      localHashMap.put("type", "event");
      long l1 = a.getTime();
      String str1 = String.valueOf(l1);
      localHashMap.put("at", str1);
      str1 = b;
      localHashMap.put("category", str1);
      str1 = c;
      localHashMap.put("action", str1);
      str1 = d;
      localHashMap.put("label", str1);
      localObject = e;
      localHashMap.put("value", localObject);
      localObject = "pageId";
      int i1 = h.h;
      String str2 = String.valueOf(i1);
      localHashMap.put(localObject, str2);
      b(localHashMap);
    }
  }
  
  public void a(c paramc)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("type", "Exception");
    String str = String.valueOf(paramc.c().getTime());
    localHashMap.put("at", str);
    str = paramc.b().getLocalizedMessage();
    localHashMap.put("message", str);
    str = Log.getStackTraceString(paramc.b());
    localHashMap.put("stackTrace", str);
    str = paramc.a();
    localHashMap.put("description", str);
    str = String.valueOf(h.h);
    localHashMap.put("pageId", str);
    str = String.valueOf(2);
    localHashMap.put("log_level", str);
    b(localHashMap);
  }
  
  public void a(String paramString)
  {
    v = paramString;
  }
  
  public void a(Date paramDate, String paramString)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    localHashMap.put("type", "jsError");
    String str = String.valueOf(paramDate.getTime());
    localHashMap.put("at", str);
    localHashMap.put("stackTrace", paramString);
    str = String.valueOf(h.h);
    localHashMap.put("pageId", str);
    b(localHashMap);
  }
  
  public void a(Map paramMap)
  {
    b(paramMap);
  }
  
  public void a(boolean paramBoolean)
  {
    long l1 = System.currentTimeMillis();
    r = l1;
    q = paramBoolean;
  }
  
  public String b()
  {
    return l;
  }
  
  public void b(String paramString)
  {
    n = paramString;
  }
  
  public void c(String paramString)
  {
    b localb = new in/juspay/tracker/b;
    localb.<init>();
    Object localObject = b.b.b;
    localb = localb.a((b.b)localObject);
    localObject = b.a.h;
    localb = localb.a((b.a)localObject).b(paramString);
    a().a(localb);
  }
  
  public boolean c()
  {
    boolean bool1 = false;
    long l1 = r;
    long l2 = d;
    l1 += l2;
    l2 = System.currentTimeMillis();
    boolean bool2 = l1 < l2;
    if (bool2) {
      bool1 = q;
    }
    for (;;)
    {
      return bool1;
      a(false);
    }
  }
  
  public void d()
  {
    try
    {
      Object localObject1 = i.a();
      localObject1 = ((i)localObject1).f();
      localObject3 = "interval_start";
      long l1 = b;
      l1 = ((JSONObject)localObject1).optLong((String)localObject3, l1);
      b = l1;
      localObject3 = "interval_batch";
      l1 = c;
      long l2 = ((JSONObject)localObject1).optLong((String)localObject3, l1);
      c = l2;
      localObject1 = new in/juspay/tracker/b;
      ((b)localObject1).<init>();
      localObject3 = b.b.d;
      localObject1 = ((b)localObject1).a((b.b)localObject3);
      localObject3 = b.a.h;
      localObject1 = ((b)localObject1).a((b.a)localObject3);
      localObject3 = "log_push_config";
      localObject1 = ((b)localObject1).b((String)localObject3);
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      str = "START_INTERVAL = ";
      localObject3 = ((StringBuilder)localObject3).append(str);
      l1 = b;
      localObject3 = ((StringBuilder)localObject3).append(l1);
      str = " BATCH_INTERVAL = ";
      localObject3 = ((StringBuilder)localObject3).append(str);
      l1 = c;
      localObject3 = ((StringBuilder)localObject3).append(l1);
      localObject3 = ((StringBuilder)localObject3).toString();
      localObject1 = ((b)localObject1).c((String)localObject3);
      localObject3 = a();
      ((e)localObject3).a((b)localObject1);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject3 = a;
        String str = "Exception while setting timer interval";
        f.a((String)localObject3, str, localException);
        k();
      }
    }
    finally
    {
      k();
    }
  }
  
  public Map f()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    for (Object localObject1 = "at";; localObject1 = null)
    {
      try
      {
        long l1 = System.currentTimeMillis();
        localObject2 = String.valueOf(l1);
        localHashMap.put(localObject1, localObject2);
        localObject1 = "brand";
        localObject2 = Build.BRAND;
        localObject2 = String.valueOf(localObject2);
        localHashMap.put(localObject1, localObject2);
        localObject1 = "model";
        localObject2 = Build.MODEL;
        localObject2 = String.valueOf(localObject2);
        localHashMap.put(localObject1, localObject2);
        localObject1 = "manufacturer";
        localObject2 = Build.MANUFACTURER;
        localHashMap.put(localObject1, localObject2);
        localObject1 = "os";
        localObject2 = "android";
        localHashMap.put(localObject1, localObject2);
        localObject1 = "os_version";
        localObject2 = Build.VERSION.RELEASE;
        localObject2 = String.valueOf(localObject2);
        localHashMap.put(localObject1, localObject2);
        localObject1 = "locale";
        localObject2 = Locale.getDefault();
        localObject2 = ((Locale)localObject2).getDisplayLanguage();
        localHashMap.put(localObject1, localObject2);
        localObject1 = "app_name";
        localObject2 = g();
        localHashMap.put(localObject1, localObject2);
        localObject1 = "client_id";
        localObject2 = v;
        localHashMap.put(localObject1, localObject2);
        localObject1 = "godel_version";
        localObject2 = j.a();
        localObject2 = ((j)localObject2).b();
        localHashMap.put(localObject1, localObject2);
        localObject1 = "godel_build_version";
        localObject2 = j.a();
        localObject2 = ((j)localObject2).d();
        localHashMap.put(localObject1, localObject2);
        localObject1 = "godel_remotes_version";
        localObject2 = j.a();
        localObject2 = ((j)localObject2).c();
        localHashMap.put(localObject1, localObject2);
        localObject1 = "invocation_type";
        localObject2 = y;
        localHashMap.put(localObject1, localObject2);
        localObject1 = "ip_address";
        localObject2 = e();
        localHashMap.put(localObject1, localObject2);
        localObject1 = x;
        if (localObject1 != null)
        {
          localObject1 = "device_id";
          localObject2 = j.a();
          localObject3 = x;
          localObject2 = ((j)localObject2).g((Context)localObject3);
          localHashMap.put(localObject1, localObject2);
          localObject1 = "screen_width";
          localObject2 = j.a();
          localObject3 = x;
          localObject2 = ((j)localObject2).e((Context)localObject3);
          localHashMap.put(localObject1, localObject2);
          localObject1 = "screen_height";
          localObject2 = j.a();
          localObject3 = x;
          localObject2 = ((j)localObject2).d((Context)localObject3);
          localHashMap.put(localObject1, localObject2);
          localObject1 = "screen_ppi";
          localObject2 = j.a();
          localObject3 = x;
          localObject2 = ((j)localObject2).f((Context)localObject3);
          localHashMap.put(localObject1, localObject2);
          localObject1 = "network_info";
          localObject2 = j.a();
          localObject3 = x;
          localObject2 = ((j)localObject2).a((Context)localObject3);
          localHashMap.put(localObject1, localObject2);
          localObject1 = "network_type";
          localObject2 = j.a();
          localObject3 = x;
          int i1 = ((j)localObject2).b((Context)localObject3);
          localObject2 = String.valueOf(i1);
          localHashMap.put(localObject1, localObject2);
          localObject1 = "app_version";
          localObject2 = j.a();
          localObject3 = x;
          localObject2 = ((j)localObject2).c((Context)localObject3);
          localHashMap.put(localObject1, localObject2);
          localObject1 = x;
          localObject1 = ((Context)localObject1).getApplicationInfo();
          int i3 = flags & 0x2;
          if (i3 == 0) {
            break label728;
          }
          i3 = 1;
          localObject2 = "app_debuggable";
          localObject1 = String.valueOf(i3);
          localHashMap.put(localObject2, localObject1);
          localObject1 = "dev_options_enabled";
          localObject2 = j.a();
          localObject3 = x;
          bool = ((j)localObject2).h((Context)localObject3);
          localObject2 = String.valueOf(bool);
          localHashMap.put(localObject1, localObject2);
        }
        localObject1 = "is_rooted";
        j.a();
        boolean bool = j.e();
        localObject2 = String.valueOf(bool);
        localHashMap.put(localObject1, localObject2);
        localObject1 = "log_level";
        int i2 = 2;
        localObject2 = String.valueOf(i2);
        localHashMap.put(localObject1, localObject2);
      }
      finally
      {
        for (;;)
        {
          label728:
          int i4;
          Object localObject2 = a;
          Object localObject3 = "Exception while creatingSession Data Map";
          f.a((String)localObject2, (String)localObject3, localThrowable);
        }
      }
      return localHashMap;
      i4 = 0;
    }
  }
  
  public String g()
  {
    Object localObject = o;
    if (localObject == null)
    {
      localObject = x.getApplicationInfo();
      PackageManager localPackageManager = x.getPackageManager();
      localObject = String.valueOf(((ApplicationInfo)localObject).loadLabel(localPackageManager));
      o = ((String)localObject);
    }
    return o;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/tracker/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */