package in.juspay.tracker;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class a
{
  private static String a = "AES";
  private static byte[] b;
  private static a c;
  private static final String d = a.class.getName();
  
  static
  {
    byte[] arrayOfByte = new byte[16];
    arrayOfByte[0] = -52;
    arrayOfByte[1] = 51;
    arrayOfByte[2] = -68;
    arrayOfByte[3] = -121;
    arrayOfByte[4] = -44;
    arrayOfByte[5] = -114;
    arrayOfByte[6] = -59;
    arrayOfByte[7] = -20;
    arrayOfByte[8] = -79;
    arrayOfByte[9] = 22;
    arrayOfByte[10] = 34;
    arrayOfByte[11] = -77;
    arrayOfByte[12] = -48;
    arrayOfByte[13] = -75;
    arrayOfByte[14] = 45;
    arrayOfByte[15] = 93;
    b = arrayOfByte;
  }
  
  public static a a()
  {
    synchronized (a.class)
    {
      a locala = c;
      if (locala == null)
      {
        locala = new in/juspay/tracker/a;
        locala.<init>();
        c = locala;
      }
      locala = c;
      return locala;
    }
  }
  
  private Key b()
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    byte[] arrayOfByte = b;
    String str = a;
    localSecretKeySpec.<init>(arrayOfByte, str);
    return localSecretKeySpec;
  }
  
  private String f(byte[] paramArrayOfByte)
  {
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    localStringBuffer.<init>();
    int i = 0;
    for (;;)
    {
      int j = paramArrayOfByte.length;
      if (i >= j) {
        break;
      }
      j = paramArrayOfByte[i] & 0xFF;
      String str = Integer.toHexString(j);
      int k = str.length();
      int m = 1;
      if (k == m)
      {
        k = 48;
        localStringBuffer.append(k);
      }
      localStringBuffer.append(str);
      i += 1;
    }
    return localStringBuffer.toString();
  }
  
  public String a(String paramString)
  {
    Object localObject1 = "SHA-256";
    try
    {
      localObject1 = MessageDigest.getInstance((String)localObject1);
      localObject3 = paramString.getBytes();
      ((MessageDigest)localObject1).update((byte[])localObject3);
      localObject1 = ((MessageDigest)localObject1).digest();
      localObject1 = f((byte[])localObject1);
      localObject3 = d;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      String str = "result is ";
      localObject4 = ((StringBuilder)localObject4).append(str);
      localObject4 = ((StringBuilder)localObject4).append((String)localObject1);
      localObject4 = ((StringBuilder)localObject4).toString();
      f.b((String)localObject3, (String)localObject4);
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      for (;;)
      {
        Object localObject3 = d;
        Object localObject4 = "Exception caught trying to SHA-256 hash";
        f.a((String)localObject3, (String)localObject4, localNoSuchAlgorithmException);
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  public String a(byte[] paramArrayOfByte)
  {
    Object localObject1 = "MD5";
    try
    {
      localObject1 = MessageDigest.getInstance((String)localObject1);
      ((MessageDigest)localObject1).update(paramArrayOfByte);
      localObject3 = ((MessageDigest)localObject1).digest();
      StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
      localStringBuilder1.<init>();
      int i = localObject3.length;
      j = 0;
      localObject1 = null;
      int k = 0;
      str1 = null;
      while (k < i)
      {
        j = localObject3[k] & 0xFF;
        for (localObject1 = Integer.toHexString(j);; localObject1 = ((StringBuilder)localObject1).toString())
        {
          int m = ((String)localObject1).length();
          int n = 2;
          if (m >= n) {
            break;
          }
          StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
          localStringBuilder2.<init>();
          String str2 = "0";
          localStringBuilder2 = localStringBuilder2.append(str2);
          localObject1 = localStringBuilder2.append((String)localObject1);
        }
        localStringBuilder1.append((String)localObject1);
        j = k + 1;
        k = j;
      }
      localObject1 = localStringBuilder1.toString();
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      for (;;)
      {
        String str1 = d;
        Object localObject3 = "Exception trying to calculate md5sum from given string";
        f.a(str1, (String)localObject3, localNoSuchAlgorithmException);
        int j = 0;
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  public String b(String paramString)
  {
    byte[] arrayOfByte = paramString.getBytes();
    return a(arrayOfByte);
  }
  
  public byte[] b(byte[] paramArrayOfByte)
  {
    Key localKey = b();
    Cipher localCipher = Cipher.getInstance(a);
    localCipher.init(1, localKey);
    return localCipher.doFinal(paramArrayOfByte);
  }
  
  public byte[] c(byte[] paramArrayOfByte)
  {
    int i = 8;
    int j = 0;
    SecureRandom localSecureRandom = new java/security/SecureRandom;
    localSecureRandom.<init>();
    byte[] arrayOfByte1 = new byte[i];
    localSecureRandom.nextBytes(arrayOfByte1);
    int k = paramArrayOfByte.length;
    byte[] arrayOfByte2 = new byte[k + i];
    int m = 0;
    int n = 0;
    localSecureRandom = null;
    if (j < k)
    {
      int i1 = k + i;
      if (m < i1)
      {
        int i2;
        if (m > 0)
        {
          i1 = m % 10;
          i2 = 9;
          if ((i1 == i2) && (n < i))
          {
            i1 = arrayOfByte1[n];
            arrayOfByte2[m] = i1;
            n += 1;
          }
        }
        for (;;)
        {
          m += 1;
          break;
          i1 = paramArrayOfByte[j];
          i2 = j % i;
          i2 = arrayOfByte1[i2];
          i1 = (byte)(i1 ^ i2);
          arrayOfByte2[m] = i1;
          j += 1;
        }
      }
    }
    return arrayOfByte2;
  }
  
  public byte[] d(byte[] paramArrayOfByte)
  {
    try
    {
      arrayOfByte = d.a(paramArrayOfByte);
      arrayOfByte = b(arrayOfByte);
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      for (;;)
      {
        byte[] arrayOfByte;
        localFileNotFoundException.printStackTrace();
        Object localObject = null;
      }
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localIOException.printStackTrace();
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
    return arrayOfByte;
  }
  
  public byte[] e(byte[] paramArrayOfByte)
  {
    try
    {
      arrayOfByte = d.a(paramArrayOfByte);
      arrayOfByte = c(arrayOfByte);
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      for (;;)
      {
        byte[] arrayOfByte;
        localFileNotFoundException.printStackTrace();
        Object localObject = null;
      }
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localIOException.printStackTrace();
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
    return arrayOfByte;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/tracker/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */