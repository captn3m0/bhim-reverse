package in.juspay.tracker;

import java.io.IOException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class RestClient
{
  private static final String a = RestClient.class.getName();
  private static DefaultHttpClient b;
  private static DefaultHttpClient c;
  
  static
  {
    a();
  }
  
  private static DefaultHttpClient a(HttpParams paramHttpParams)
  {
    Object localObject1 = new org/apache/http/conn/scheme/SchemeRegistry;
    ((SchemeRegistry)localObject1).<init>();
    SSLSocketFactory localSSLSocketFactory = SSLSocketFactory.getSocketFactory();
    Object localObject2 = (X509HostnameVerifier)SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
    localSSLSocketFactory.setHostnameVerifier((X509HostnameVerifier)localObject2);
    localObject2 = new org/apache/http/conn/scheme/Scheme;
    ((Scheme)localObject2).<init>("https", localSSLSocketFactory, 443);
    ((SchemeRegistry)localObject1).register((Scheme)localObject2);
    localObject2 = new org/apache/http/conn/scheme/Scheme;
    PlainSocketFactory localPlainSocketFactory = PlainSocketFactory.getSocketFactory();
    ((Scheme)localObject2).<init>("http", localPlainSocketFactory, 80);
    ((SchemeRegistry)localObject1).register((Scheme)localObject2);
    localObject2 = new org/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    ((ThreadSafeClientConnManager)localObject2).<init>(paramHttpParams, (SchemeRegistry)localObject1);
    localObject1 = new org/apache/http/impl/client/DefaultHttpClient;
    ((DefaultHttpClient)localObject1).<init>((ClientConnectionManager)localObject2, paramHttpParams);
    return (DefaultHttpClient)localObject1;
  }
  
  public static void a()
  {
    int i = 10000;
    f.a(a, "Default http client");
    BasicHttpParams localBasicHttpParams = new org/apache/http/params/BasicHttpParams;
    localBasicHttpParams.<init>();
    HttpProtocolParams.setContentCharset(localBasicHttpParams, "UTF-8");
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, i);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, 30000);
    b = a(localBasicHttpParams);
    localBasicHttpParams = new org/apache/http/params/BasicHttpParams;
    localBasicHttpParams.<init>();
    HttpProtocolParams.setContentCharset(localBasicHttpParams, "UTF-8");
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 5000);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, i);
    c = a(localBasicHttpParams);
  }
  
  public static void a(String paramString1, String paramString2)
  {
    HttpPost localHttpPost = new org/apache/http/client/methods/HttpPost;
    localHttpPost.<init>(paramString1);
    localHttpPost.setHeader("Content-Type", "application/x-godel-gzip-encrypted");
    Object localObject1 = a.a();
    Object localObject2 = paramString2.getBytes("UTF-8");
    localObject1 = ((a)localObject1).d((byte[])localObject2);
    localObject2 = new org/apache/http/entity/ByteArrayEntity;
    ((ByteArrayEntity)localObject2).<init>((byte[])localObject1);
    localHttpPost.setEntity((HttpEntity)localObject2);
    a(localHttpPost);
  }
  
  private static boolean a(HttpResponse paramHttpResponse)
  {
    StatusLine localStatusLine = paramHttpResponse.getStatusLine();
    int i = localStatusLine.getStatusCode();
    int k = 200;
    if (i >= k)
    {
      int m = 300;
      if (i < m) {
        i = 1;
      }
    }
    for (;;)
    {
      return i;
      int j = 0;
      localStatusLine = null;
    }
  }
  
  private static byte[] a(HttpRequestBase paramHttpRequestBase)
  {
    Object localObject1 = a;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = ((StringBuilder)localObject2).append("Executing ");
    Object localObject3 = paramHttpRequestBase.getMethod();
    localObject2 = ((StringBuilder)localObject2).append((String)localObject3).append(" ");
    localObject3 = paramHttpRequestBase.getURI();
    localObject2 = localObject3;
    f.b((String)localObject1, (String)localObject2);
    localObject1 = "Accept-Encoding";
    localObject2 = "gzip";
    try
    {
      paramHttpRequestBase.setHeader((String)localObject1, (String)localObject2);
      localObject1 = b;
      localObject1 = ((DefaultHttpClient)localObject1).execute(paramHttpRequestBase);
      localObject2 = a;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      Object localObject4 = "Got response for ";
      localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
      localObject4 = paramHttpRequestBase.getURI();
      localObject3 = ((StringBuilder)localObject3).append(localObject4);
      localObject4 = ", response code ";
      localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
      localObject4 = ((HttpResponse)localObject1).getStatusLine();
      int i = ((StatusLine)localObject4).getStatusCode();
      localObject3 = ((StringBuilder)localObject3).append(i);
      localObject3 = ((StringBuilder)localObject3).toString();
      f.b((String)localObject2, (String)localObject3);
      return a(paramHttpRequestBase, (HttpResponse)localObject1);
    }
    catch (IOException localIOException)
    {
      localObject2 = new java/lang/RuntimeException;
      String str = localIOException.getMessage();
      ((RuntimeException)localObject2).<init>(str);
      throw ((Throwable)localObject2);
    }
  }
  
  /* Error */
  private static byte[] a(HttpRequestBase paramHttpRequestBase, HttpResponse paramHttpResponse)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 221	in/juspay/tracker/RestClient:a	(Lorg/apache/http/HttpResponse;)Z
    //   4: istore_2
    //   5: iload_2
    //   6: ifeq +225 -> 231
    //   9: aload_1
    //   10: invokeinterface 225 1 0
    //   15: astore_3
    //   16: aload_3
    //   17: invokeinterface 231 1 0
    //   22: astore_3
    //   23: aload_3
    //   24: ifnull +192 -> 216
    //   27: aload_3
    //   28: invokeinterface 236 1 0
    //   33: astore_3
    //   34: ldc -61
    //   36: astore 4
    //   38: aload_3
    //   39: aload 4
    //   41: invokevirtual 240	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   44: istore_2
    //   45: iload_2
    //   46: ifeq +170 -> 216
    //   49: getstatic 17	in/juspay/tracker/RestClient:a	Ljava/lang/String;
    //   52: astore_3
    //   53: ldc -14
    //   55: astore 4
    //   57: aload_3
    //   58: aload 4
    //   60: invokestatic 80	in/juspay/tracker/f:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   63: new 244	java/util/zip/GZIPInputStream
    //   66: astore 4
    //   68: aload_1
    //   69: invokeinterface 225 1 0
    //   74: astore_3
    //   75: aload_3
    //   76: invokeinterface 248 1 0
    //   81: astore_3
    //   82: aload 4
    //   84: aload_3
    //   85: invokespecial 251	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   88: sipush 1024
    //   91: istore_2
    //   92: iload_2
    //   93: newarray <illegal type>
    //   95: astore_3
    //   96: new 254	java/io/ByteArrayOutputStream
    //   99: astore 5
    //   101: aload 5
    //   103: invokespecial 255	java/io/ByteArrayOutputStream:<init>	()V
    //   106: aload 4
    //   108: aload_3
    //   109: invokevirtual 259	java/util/zip/GZIPInputStream:read	([B)I
    //   112: istore 6
    //   114: iload 6
    //   116: iflt +65 -> 181
    //   119: aload 5
    //   121: aload_3
    //   122: iconst_0
    //   123: iload 6
    //   125: invokevirtual 263	java/io/ByteArrayOutputStream:write	([BII)V
    //   128: goto -22 -> 106
    //   131: astore_3
    //   132: aload 4
    //   134: ifnull +25 -> 159
    //   137: getstatic 17	in/juspay/tracker/RestClient:a	Ljava/lang/String;
    //   140: astore 5
    //   142: ldc_w 265
    //   145: astore 7
    //   147: aload 5
    //   149: aload 7
    //   151: invokestatic 80	in/juspay/tracker/f:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   154: aload 4
    //   156: invokevirtual 268	java/util/zip/GZIPInputStream:close	()V
    //   159: aload_3
    //   160: athrow
    //   161: astore_3
    //   162: new 212	java/lang/RuntimeException
    //   165: astore 4
    //   167: aload_3
    //   168: invokevirtual 271	java/io/UnsupportedEncodingException:getMessage	()Ljava/lang/String;
    //   171: astore_3
    //   172: aload 4
    //   174: aload_3
    //   175: invokespecial 218	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
    //   178: aload 4
    //   180: athrow
    //   181: aload 5
    //   183: invokevirtual 275	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   186: astore_3
    //   187: aload 4
    //   189: ifnull +25 -> 214
    //   192: getstatic 17	in/juspay/tracker/RestClient:a	Ljava/lang/String;
    //   195: astore 5
    //   197: ldc_w 265
    //   200: astore 7
    //   202: aload 5
    //   204: aload 7
    //   206: invokestatic 80	in/juspay/tracker/f:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   209: aload 4
    //   211: invokevirtual 268	java/util/zip/GZIPInputStream:close	()V
    //   214: aload_3
    //   215: areturn
    //   216: aload_1
    //   217: invokeinterface 225 1 0
    //   222: astore_3
    //   223: aload_3
    //   224: invokestatic 280	org/apache/http/util/EntityUtils:toByteArray	(Lorg/apache/http/HttpEntity;)[B
    //   227: astore_3
    //   228: goto -14 -> 214
    //   231: new 282	in/juspay/tracker/RestClient$UnsuccessfulRestCall
    //   234: astore_3
    //   235: aload_3
    //   236: aload_0
    //   237: aload_1
    //   238: invokespecial 285	in/juspay/tracker/RestClient$UnsuccessfulRestCall:<init>	(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;)V
    //   241: aload_3
    //   242: athrow
    //   243: astore_3
    //   244: new 212	java/lang/RuntimeException
    //   247: astore 4
    //   249: aload_3
    //   250: invokevirtual 288	org/apache/http/client/ClientProtocolException:getMessage	()Ljava/lang/String;
    //   253: astore_3
    //   254: aload 4
    //   256: aload_3
    //   257: invokespecial 218	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
    //   260: aload 4
    //   262: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	263	0	paramHttpRequestBase	HttpRequestBase
    //   0	263	1	paramHttpResponse	HttpResponse
    //   4	42	2	bool	boolean
    //   91	2	2	i	int
    //   15	107	3	localObject1	Object
    //   131	29	3	localObject2	Object
    //   161	7	3	localUnsupportedEncodingException	java.io.UnsupportedEncodingException
    //   171	71	3	localObject3	Object
    //   243	7	3	localClientProtocolException	org.apache.http.client.ClientProtocolException
    //   253	4	3	str1	String
    //   36	225	4	localObject4	Object
    //   99	104	5	localObject5	Object
    //   112	12	6	j	int
    //   145	60	7	str2	String
    // Exception table:
    //   from	to	target	type
    //   108	112	131	finally
    //   123	128	131	finally
    //   181	186	131	finally
    //   0	4	161	java/io/UnsupportedEncodingException
    //   9	15	161	java/io/UnsupportedEncodingException
    //   16	22	161	java/io/UnsupportedEncodingException
    //   27	33	161	java/io/UnsupportedEncodingException
    //   39	44	161	java/io/UnsupportedEncodingException
    //   49	52	161	java/io/UnsupportedEncodingException
    //   58	63	161	java/io/UnsupportedEncodingException
    //   63	66	161	java/io/UnsupportedEncodingException
    //   68	74	161	java/io/UnsupportedEncodingException
    //   75	81	161	java/io/UnsupportedEncodingException
    //   84	88	161	java/io/UnsupportedEncodingException
    //   92	95	161	java/io/UnsupportedEncodingException
    //   96	99	161	java/io/UnsupportedEncodingException
    //   101	106	161	java/io/UnsupportedEncodingException
    //   137	140	161	java/io/UnsupportedEncodingException
    //   149	154	161	java/io/UnsupportedEncodingException
    //   154	159	161	java/io/UnsupportedEncodingException
    //   159	161	161	java/io/UnsupportedEncodingException
    //   192	195	161	java/io/UnsupportedEncodingException
    //   204	209	161	java/io/UnsupportedEncodingException
    //   209	214	161	java/io/UnsupportedEncodingException
    //   216	222	161	java/io/UnsupportedEncodingException
    //   223	227	161	java/io/UnsupportedEncodingException
    //   231	234	161	java/io/UnsupportedEncodingException
    //   237	241	161	java/io/UnsupportedEncodingException
    //   241	243	161	java/io/UnsupportedEncodingException
    //   0	4	243	org/apache/http/client/ClientProtocolException
    //   9	15	243	org/apache/http/client/ClientProtocolException
    //   16	22	243	org/apache/http/client/ClientProtocolException
    //   27	33	243	org/apache/http/client/ClientProtocolException
    //   39	44	243	org/apache/http/client/ClientProtocolException
    //   49	52	243	org/apache/http/client/ClientProtocolException
    //   58	63	243	org/apache/http/client/ClientProtocolException
    //   63	66	243	org/apache/http/client/ClientProtocolException
    //   68	74	243	org/apache/http/client/ClientProtocolException
    //   75	81	243	org/apache/http/client/ClientProtocolException
    //   84	88	243	org/apache/http/client/ClientProtocolException
    //   92	95	243	org/apache/http/client/ClientProtocolException
    //   96	99	243	org/apache/http/client/ClientProtocolException
    //   101	106	243	org/apache/http/client/ClientProtocolException
    //   137	140	243	org/apache/http/client/ClientProtocolException
    //   149	154	243	org/apache/http/client/ClientProtocolException
    //   154	159	243	org/apache/http/client/ClientProtocolException
    //   159	161	243	org/apache/http/client/ClientProtocolException
    //   192	195	243	org/apache/http/client/ClientProtocolException
    //   204	209	243	org/apache/http/client/ClientProtocolException
    //   209	214	243	org/apache/http/client/ClientProtocolException
    //   216	222	243	org/apache/http/client/ClientProtocolException
    //   223	227	243	org/apache/http/client/ClientProtocolException
    //   231	234	243	org/apache/http/client/ClientProtocolException
    //   237	241	243	org/apache/http/client/ClientProtocolException
    //   241	243	243	org/apache/http/client/ClientProtocolException
  }
  
  public static void b(String paramString1, String paramString2)
  {
    HttpPost localHttpPost = new org/apache/http/client/methods/HttpPost;
    localHttpPost.<init>(paramString1);
    localHttpPost.setHeader("Content-Type", "application/x-godel-gzip-encoded");
    Object localObject1 = a.a();
    Object localObject2 = paramString2.getBytes("UTF-8");
    localObject1 = ((a)localObject1).e((byte[])localObject2);
    localObject2 = new org/apache/http/entity/ByteArrayEntity;
    ((ByteArrayEntity)localObject2).<init>((byte[])localObject1);
    localHttpPost.setEntity((HttpEntity)localObject2);
    a(localHttpPost);
  }
  
  public static void c(String paramString1, String paramString2)
  {
    HttpPost localHttpPost = new org/apache/http/client/methods/HttpPost;
    localHttpPost.<init>(paramString1);
    localHttpPost.setHeader("Content-Type", "application/x-godel-gzip");
    byte[] arrayOfByte = d.a(paramString2.getBytes("UTF-8"));
    ByteArrayEntity localByteArrayEntity = new org/apache/http/entity/ByteArrayEntity;
    localByteArrayEntity.<init>(arrayOfByte);
    localHttpPost.setEntity(localByteArrayEntity);
    a(localHttpPost);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/tracker/RestClient.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */