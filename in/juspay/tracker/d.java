package in.juspay.tracker;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class d
{
  private static String a = d.class.toString();
  
  public static byte[] a(byte[] paramArrayOfByte)
  {
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      int i = paramArrayOfByte.length;
      localByteArrayOutputStream.<init>(i);
      localObject = new java/util/zip/GZIPOutputStream;
      ((GZIPOutputStream)localObject).<init>(localByteArrayOutputStream);
      ((GZIPOutputStream)localObject).write(paramArrayOfByte);
      localByteArrayOutputStream.close();
      ((GZIPOutputStream)localObject).close();
      localObject = a;
      String str = "Gzipping complete";
      f.a((String)localObject, str);
      return localByteArrayOutputStream.toByteArray();
    }
    catch (IOException localIOException)
    {
      f.a(a, "Could not gzip", localIOException);
      Object localObject = new java/lang/RuntimeException;
      ((RuntimeException)localObject).<init>(localIOException);
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/tracker/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */