package in.juspay.tracker;

import java.security.SecureRandom;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class g
{
  private static g b;
  private final String a;
  private JSONObject c;
  
  g()
  {
    String str = g.class.getName();
    a = str;
    c = null;
  }
  
  public static g a()
  {
    g localg = b;
    if (localg == null)
    {
      localg = new in/juspay/tracker/g;
      localg.<init>();
      b = localg;
    }
    return b;
  }
  
  private JSONObject a(String paramString)
  {
    for (;;)
    {
      try
      {
        localObject1 = i.a();
        localObject3 = ((i)localObject1).g();
        if (localObject3 != null) {
          continue;
        }
        bool1 = false;
        localObject1 = null;
      }
      catch (JSONException localJSONException)
      {
        Object localObject1;
        boolean bool2;
        Object localObject3 = a;
        Object localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        String str = "Error while getting rules for event ";
        localObject4 = str + paramString;
        f.a((String)localObject3, (String)localObject4, localJSONException);
        boolean bool1 = false;
        Object localObject2 = null;
        continue;
      }
      return (JSONObject)localObject1;
      localObject4 = ((JSONObject)localObject3).keys();
      bool1 = ((Iterator)localObject4).hasNext();
      if (bool1)
      {
        localObject1 = ((Iterator)localObject4).next();
        localObject1 = (String)localObject1;
        bool2 = paramString.contains((CharSequence)localObject1);
        if (bool2) {
          localObject1 = ((JSONObject)localObject3).getJSONObject((String)localObject1);
        }
      }
      else
      {
        localObject1 = b(paramString);
      }
    }
  }
  
  private boolean a(double paramDouble)
  {
    boolean bool1 = true;
    double d1 = 1.0D;
    boolean bool2 = paramDouble < d1;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      d1 = 0.0D;
      bool2 = paramDouble < d1;
      if (!bool2)
      {
        bool1 = false;
      }
      else
      {
        SecureRandom localSecureRandom = new java/security/SecureRandom;
        localSecureRandom.<init>();
        int i = 100;
        d1 = localSecureRandom.nextInt(i);
        double d2 = 100.0D * paramDouble;
        bool2 = d1 < d2;
        if (!bool2) {
          bool1 = false;
        }
      }
    }
  }
  
  private JSONObject b(String paramString)
  {
    Object localObject1 = "account_info";
    for (;;)
    {
      try
      {
        bool = paramString.contains((CharSequence)localObject1);
        if (!bool) {
          continue;
        }
        localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>();
        str1 = "logHashed";
        i = 1;
        ((JSONObject)localObject1).put(str1, i);
        str1 = "value";
        double d = 0.0D;
        ((JSONObject)localObject1).put(str1, d);
      }
      catch (JSONException localJSONException)
      {
        int i;
        String str1 = a;
        String str2 = "Error while getting default rules ";
        f.a(str1, str2, localJSONException);
        boolean bool = false;
        Object localObject2 = null;
        continue;
      }
      return (JSONObject)localObject1;
      localObject1 = new org/json/JSONObject;
      ((JSONObject)localObject1).<init>();
      str1 = "logHashed";
      i = 0;
      str2 = null;
      ((JSONObject)localObject1).put(str1, false);
      str1 = "value";
      i = 1;
      ((JSONObject)localObject1).put(str1, i);
    }
  }
  
  public b a(b paramb)
  {
    for (;;)
    {
      try
      {
        Object localObject1 = paramb.a();
        if (localObject1 != null)
        {
          localObject1 = paramb.a();
          localObject1 = a((String)localObject1);
          if (localObject1 != null)
          {
            localObject2 = "logHashed";
            str = null;
            boolean bool1 = ((JSONObject)localObject1).optBoolean((String)localObject2, false);
            if (bool1)
            {
              localObject2 = paramb.b();
              if (localObject2 != null)
              {
                localObject2 = a.a();
                str = paramb.b();
                localObject2 = ((a)localObject2).b(str);
                paramb.c((String)localObject2);
              }
            }
            localObject2 = "value";
            double d1 = 0.0D;
            double d2 = ((JSONObject)localObject1).optDouble((String)localObject2, d1);
            boolean bool2 = a(d2);
            if (!bool2) {
              continue;
            }
          }
        }
      }
      catch (Exception localException)
      {
        Object localObject2 = a;
        String str = "Error in getLogEvent ";
        f.a((String)localObject2, str, localException);
        paramb = null;
        continue;
      }
      return paramb;
      paramb = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/tracker/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */