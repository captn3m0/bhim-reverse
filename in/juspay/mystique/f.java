package in.juspay.mystique;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.security.MessageDigest;

public class f
{
  private static final String a = f.class.getSimpleName();
  
  private static ByteArrayOutputStream a(ByteArrayOutputStream paramByteArrayOutputStream, InputStream paramInputStream)
  {
    int i = 4096;
    byte[] arrayOfByte = new byte[i];
    for (;;)
    {
      int j = paramInputStream.read(arrayOfByte);
      int k = -1;
      if (j == k) {
        break;
      }
      k = 0;
      paramByteArrayOutputStream.write(arrayOfByte, 0, j);
    }
    paramByteArrayOutputStream.close();
    paramInputStream.close();
    return paramByteArrayOutputStream;
  }
  
  public static String a(byte[] paramArrayOfByte)
  {
    Object localObject = MessageDigest.getInstance("MD5");
    ((MessageDigest)localObject).update(paramArrayOfByte);
    byte[] arrayOfByte = ((MessageDigest)localObject).digest();
    StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
    localStringBuilder1.<init>();
    int i = arrayOfByte.length;
    int j = 0;
    localObject = null;
    for (int k = 0; k < i; k = j)
    {
      j = arrayOfByte[k] & 0xFF;
      StringBuilder localStringBuilder2;
      for (localObject = Integer.toHexString(j);; localObject = (String)localObject)
      {
        int m = ((String)localObject).length();
        int n = 2;
        if (m >= n) {
          break;
        }
        localStringBuilder2 = new java/lang/StringBuilder;
        localStringBuilder2.<init>();
        String str = "0";
        localStringBuilder2 = localStringBuilder2.append(str);
      }
      localStringBuilder1.append((String)localObject);
      j = k + 1;
    }
    return localStringBuilder1.toString();
  }
  
  public static void a(Context paramContext, String paramString, byte[] paramArrayOfByte)
  {
    try
    {
      FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
      File localFile = new java/io/File;
      Object localObject3 = "juspay";
      localObject3 = paramContext.getDir((String)localObject3, 0);
      localFile.<init>((File)localObject3, paramString);
      localFileOutputStream.<init>(localFile);
      localFileOutputStream.close();
    }
    finally
    {
      try
      {
        localFileOutputStream.write(paramArrayOfByte);
        localFileOutputStream.close();
        return;
      }
      finally {}
      localObject1 = finally;
      localFileOutputStream = null;
    }
    throw ((Throwable)localObject1);
  }
  
  public static byte[] a(Context paramContext, String paramString)
  {
    byte[] arrayOfByte = b(paramContext, paramString);
    if (arrayOfByte == null) {
      arrayOfByte = c(paramContext, paramString);
    }
    return arrayOfByte;
  }
  
  public static byte[] a(String paramString1, String paramString2)
  {
    Object localObject1 = new java/io/File;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str = Environment.getExternalStorageDirectory().getAbsolutePath();
    localObject2 = str + paramString1;
    ((File)localObject1).<init>((String)localObject2, paramString2);
    localObject2 = new java/io/FileInputStream;
    ((FileInputStream)localObject2).<init>((File)localObject1);
    localObject1 = new java/io/ByteArrayOutputStream;
    ((ByteArrayOutputStream)localObject1).<init>();
    return a((ByteArrayOutputStream)localObject1, (InputStream)localObject2).toByteArray();
  }
  
  public static byte[] b(Context paramContext, String paramString)
  {
    Object localObject1 = new java/io/File;
    Object localObject2 = paramContext.getDir("juspay", 0);
    ((File)localObject1).<init>((File)localObject2, paramString);
    localObject2 = new java/io/FileInputStream;
    ((FileInputStream)localObject2).<init>((File)localObject1);
    localObject1 = new java/io/ByteArrayOutputStream;
    ((ByteArrayOutputStream)localObject1).<init>();
    return a((ByteArrayOutputStream)localObject1, (InputStream)localObject2).toByteArray();
  }
  
  public static byte[] c(Context paramContext, String paramString)
  {
    InputStream localInputStream = paramContext.getAssets().open(paramString, 0);
    ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
    localByteArrayOutputStream.<init>();
    return a(localByteArrayOutputStream, localInputStream).toByteArray();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */