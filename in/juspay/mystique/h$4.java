package in.juspay.mystique;

import android.os.AsyncTask;

class h$4
  extends AsyncTask
{
  h$4(h paramh, String paramString1, String paramString2, String paramString3, String paramString4) {}
  
  /* Error */
  protected Object doInBackground(Object[] paramArrayOfObject)
  {
    // Byte code:
    //   0: new 27	java/util/HashMap
    //   3: astore_2
    //   4: aload_2
    //   5: invokespecial 28	java/util/HashMap:<init>	()V
    //   8: new 30	org/json/JSONObject
    //   11: astore_3
    //   12: aload_0
    //   13: getfield 17	in/juspay/mystique/h$4:b	Ljava/lang/String;
    //   16: astore 4
    //   18: aload_3
    //   19: aload 4
    //   21: invokespecial 33	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   24: aload_3
    //   25: invokevirtual 37	org/json/JSONObject:keys	()Ljava/util/Iterator;
    //   28: astore 5
    //   30: aload 5
    //   32: invokeinterface 43 1 0
    //   37: istore 6
    //   39: iload 6
    //   41: ifeq +79 -> 120
    //   44: aload 5
    //   46: invokeinterface 47 1 0
    //   51: astore 4
    //   53: aload 4
    //   55: checkcast 49	java/lang/String
    //   58: astore 4
    //   60: aload_3
    //   61: aload 4
    //   63: invokevirtual 53	org/json/JSONObject:getString	(Ljava/lang/String;)Ljava/lang/String;
    //   66: astore 7
    //   68: aload_2
    //   69: aload 4
    //   71: aload 7
    //   73: invokevirtual 57	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   76: pop
    //   77: goto -47 -> 30
    //   80: astore 4
    //   82: new 59	java/lang/StringBuilder
    //   85: astore_2
    //   86: aload_2
    //   87: invokespecial 60	java/lang/StringBuilder:<init>	()V
    //   90: ldc 62
    //   92: astore_3
    //   93: aload_2
    //   94: aload_3
    //   95: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   98: astore_2
    //   99: aload 4
    //   101: invokevirtual 72	org/json/JSONException:getLocalizedMessage	()Ljava/lang/String;
    //   104: astore 4
    //   106: aload_2
    //   107: aload 4
    //   109: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: invokevirtual 75	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   115: astore 4
    //   117: aload 4
    //   119: areturn
    //   120: new 49	java/lang/String
    //   123: astore 4
    //   125: aload_0
    //   126: getfield 19	in/juspay/mystique/h$4:c	Ljava/lang/String;
    //   129: astore_3
    //   130: aload_0
    //   131: getfield 21	in/juspay/mystique/h$4:d	Ljava/lang/String;
    //   134: astore 5
    //   136: aload_3
    //   137: aload 5
    //   139: aload_2
    //   140: invokestatic 80	in/juspay/mystique/RestClient:a	(Ljava/lang/String;Ljava/lang/String;Ljava/util/HashMap;)[B
    //   143: astore_2
    //   144: aload 4
    //   146: aload_2
    //   147: invokespecial 83	java/lang/String:<init>	([B)V
    //   150: goto -33 -> 117
    //   153: astore 4
    //   155: new 59	java/lang/StringBuilder
    //   158: astore_2
    //   159: aload_2
    //   160: invokespecial 60	java/lang/StringBuilder:<init>	()V
    //   163: ldc 62
    //   165: astore_3
    //   166: aload_2
    //   167: aload_3
    //   168: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: astore_2
    //   172: aload 4
    //   174: invokevirtual 86	java/lang/Exception:getLocalizedMessage	()Ljava/lang/String;
    //   177: astore 4
    //   179: aload_2
    //   180: aload 4
    //   182: invokevirtual 66	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   185: invokevirtual 75	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   188: astore 4
    //   190: goto -73 -> 117
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	193	0	this	4
    //   0	193	1	paramArrayOfObject	Object[]
    //   3	177	2	localObject1	Object
    //   11	157	3	localObject2	Object
    //   16	54	4	localObject3	Object
    //   80	20	4	localJSONException	org.json.JSONException
    //   104	41	4	str1	String
    //   153	20	4	localException	Exception
    //   177	12	4	str2	String
    //   28	110	5	localObject4	Object
    //   37	3	6	bool	boolean
    //   66	6	7	str3	String
    // Exception table:
    //   from	to	target	type
    //   8	11	80	org/json/JSONException
    //   12	16	80	org/json/JSONException
    //   19	24	80	org/json/JSONException
    //   24	28	80	org/json/JSONException
    //   30	37	80	org/json/JSONException
    //   44	51	80	org/json/JSONException
    //   53	58	80	org/json/JSONException
    //   61	66	80	org/json/JSONException
    //   71	77	80	org/json/JSONException
    //   120	123	80	org/json/JSONException
    //   125	129	80	org/json/JSONException
    //   130	134	80	org/json/JSONException
    //   139	143	80	org/json/JSONException
    //   146	150	80	org/json/JSONException
    //   8	11	153	java/lang/Exception
    //   12	16	153	java/lang/Exception
    //   19	24	153	java/lang/Exception
    //   24	28	153	java/lang/Exception
    //   30	37	153	java/lang/Exception
    //   44	51	153	java/lang/Exception
    //   53	58	153	java/lang/Exception
    //   61	66	153	java/lang/Exception
    //   71	77	153	java/lang/Exception
    //   120	123	153	java/lang/Exception
    //   125	129	153	java/lang/Exception
    //   130	134	153	java/lang/Exception
    //   139	143	153	java/lang/Exception
    //   146	150	153	java/lang/Exception
  }
  
  protected void onPostExecute(Object paramObject)
  {
    Object localObject1;
    Object localObject2;
    String str;
    if (paramObject != null)
    {
      localObject1 = paramObject;
      localObject1 = (String)paramObject;
      localObject2 = "ERR:";
      boolean bool = ((String)localObject1).startsWith((String)localObject2);
      if (!bool) {
        break label91;
      }
      localObject1 = h.c(e);
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("window.callUICallback(\"");
      str = a;
      localObject2 = ((StringBuilder)localObject2).append(str).append("\", 'error' ,\"").append(paramObject);
      str = "\");";
      localObject2 = str;
      ((d)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      return;
      label91:
      localObject1 = h.c(e);
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("window.callUICallback(\"");
      str = a;
      localObject2 = ((StringBuilder)localObject2).append(str).append("\", ").append(paramObject);
      str = ");";
      localObject2 = str;
      ((d)localObject1).a((String)localObject2);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/h$4.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */