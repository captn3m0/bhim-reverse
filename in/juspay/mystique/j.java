package in.juspay.mystique;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

public class j
{
  private g a;
  private Activity b;
  private View c;
  private View d;
  private ViewGroup e;
  private e f;
  private d g;
  private c h;
  
  j(Activity paramActivity, d paramd)
  {
    g = paramd;
    System.currentTimeMillis();
    b = paramActivity;
    Object localObject = paramd.d();
    f = ((e)localObject);
    localObject = d.c();
    h = ((c)localObject);
    localObject = new in/juspay/mystique/g;
    Activity localActivity = b;
    c localc = h;
    e locale = f;
    ((g)localObject).<init>(localActivity, localc, locale, paramd);
    a = ((g)localObject);
  }
  
  private View a(View paramView)
  {
    Object localObject1;
    if (paramView != null)
    {
      localObject1 = e;
      ((ViewGroup)localObject1).addView(paramView);
    }
    for (;;)
    {
      return e;
      localObject1 = f;
      String str1 = "ERROR";
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append(" isNull : fn__Render -  instance null ");
      String str2 = b();
      localObject2 = str2;
      ((e)localObject1).a(str1, (String)localObject2);
    }
  }
  
  private void b(View paramView)
  {
    int i = e.indexOfChild(paramView);
    e.removeViewAt(i);
  }
  
  public View a(JSONObject paramJSONObject)
  {
    int i = 1;
    Object localObject1 = paramJSONObject.getString("type");
    Object localObject2 = paramJSONObject.getJSONObject("props");
    Object localObject3 = "props";
    boolean bool1 = paramJSONObject.has((String)localObject3);
    if (bool1) {
      a((String)localObject1, (JSONObject)localObject2);
    }
    Class localClass1 = Class.forName((String)localObject1);
    localObject1 = new Class[i];
    localObject1[0] = Context.class;
    localObject1 = localClass1.getConstructor((Class[])localObject1);
    localObject3 = new Object[i];
    Object localObject4 = b;
    localObject3[0] = localObject4;
    localObject3 = ((Constructor)localObject1).newInstance((Object[])localObject3);
    localObject4 = ((JSONObject)localObject2).keys();
    Object localObject5;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject4).hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = (String)((Iterator)localObject4).next();
      localObject5 = a;
      ((g)localObject5).a((String)localObject1, (JSONObject)localObject2, localObject3);
    }
    localObject1 = "children";
    localObject2 = paramJSONObject.getJSONArray((String)localObject1);
    if (localObject2 != null)
    {
      int j = ((JSONArray)localObject2).length();
      if (j != 0)
      {
        j = 0;
        localObject1 = null;
        for (;;)
        {
          int k = ((JSONArray)localObject2).length();
          if (j >= k) {
            break;
          }
          localObject4 = ((JSONArray)localObject2).getJSONObject(j);
          localObject4 = a((JSONObject)localObject4);
          if (localObject4 != null)
          {
            Object localObject6 = new Class[i];
            Class localClass2 = View.class;
            localObject6[0] = localClass2;
            localObject5 = localClass1.getMethod("addView", (Class[])localObject6);
            localObject6 = new Object[i];
            localObject6[0] = localObject4;
            ((Method)localObject5).invoke(localObject3, (Object[])localObject6);
          }
          j += 1;
        }
      }
    }
    localObject1 = localObject3;
    return (View)localObject3;
  }
  
  public Object a(Object paramObject, String paramString1, String paramString2, String paramString3)
  {
    a.b("modifyDom");
    a.a("");
    g localg = a;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = "ln: " + paramString2 + " " + paramString3;
    localg.c((String)localObject);
    return a.a(paramObject, paramString1);
  }
  
  public void a()
  {
    a.b();
  }
  
  public void a(String paramString, ViewGroup paramViewGroup)
  {
    e = paramViewGroup;
    Object localObject = new org/json/JSONObject;
    ((JSONObject)localObject).<init>(paramString);
    localObject = a((JSONObject)localObject);
    c = ((View)localObject);
    localObject = d;
    if (localObject != null)
    {
      localObject = d;
      View localView = c;
      if (localObject != localView)
      {
        localObject = d;
        b((View)localObject);
      }
    }
    localObject = c;
    a((View)localObject);
    localObject = c;
    d = ((View)localObject);
  }
  
  public void a(String paramString, JSONObject paramJSONObject)
  {
    a.b(paramString);
    String str = "node_id";
    boolean bool = paramJSONObject.has(str);
    g localg;
    if (bool)
    {
      str = paramJSONObject.getString("node_id");
      localg = a;
      localg.a(str);
    }
    str = "__filename";
    bool = paramJSONObject.has(str);
    if (bool)
    {
      str = paramJSONObject.getString("__filename");
      localg = a;
      localg.c(str);
    }
  }
  
  public void a(String paramString, JSONObject paramJSONObject, int paramInt, boolean paramBoolean)
  {
    Object localObject1 = b.getResources();
    Object localObject2 = "id";
    Object localObject3 = b.getPackageName();
    int i = ((Resources)localObject1).getIdentifier(paramString, (String)localObject2, (String)localObject3);
    if (paramInt >= 0)
    {
      localObject2 = b;
      localObject1 = (ViewGroup)((Activity)localObject2).findViewById(i);
      if (paramBoolean) {
        ((ViewGroup)localObject1).removeAllViews();
      }
      localObject2 = a(paramJSONObject);
      if (localObject2 != null) {
        ((ViewGroup)localObject1).addView((View)localObject2, paramInt);
      }
    }
    for (;;)
    {
      return;
      localObject1 = f;
      localObject2 = "ERROR";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__addViewToParent - child null ");
      String str = b();
      localObject3 = str;
      ((e)localObject1).a((String)localObject2, (String)localObject3);
      continue;
      localObject1 = "props";
      boolean bool = paramJSONObject.has((String)localObject1);
      if (bool)
      {
        localObject1 = paramJSONObject.getString("type");
        localObject2 = paramJSONObject.getJSONObject("props");
        a((String)localObject1, (JSONObject)localObject2);
      }
      localObject1 = f;
      localObject2 = "ERROR";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__addViewToParent - negative index ");
      str = b();
      localObject3 = str;
      ((e)localObject1).a((String)localObject2, (String)localObject3);
    }
  }
  
  public String b()
  {
    return a.a();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/j.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */