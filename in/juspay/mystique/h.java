package in.juspay.mystique;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import java.io.File;

public class h
{
  private static final String h = h.class.getName();
  private Activity a;
  private j b;
  private ViewGroup c;
  private c d;
  private e e;
  private String f;
  private d g;
  
  h(Activity paramActivity, ViewGroup paramViewGroup, d paramd)
  {
    a = paramActivity;
    g = paramd;
    Object localObject = new in/juspay/mystique/j;
    Activity localActivity = a;
    ((j)localObject).<init>(localActivity, paramd);
    b = ((j)localObject);
    c = paramViewGroup;
    localObject = d.c();
    d = ((c)localObject);
    localObject = paramd.d();
    e = ((e)localObject);
  }
  
  public void Render(String paramString1, String paramString2)
  {
    Activity localActivity = a;
    h.1 local1 = new in/juspay/mystique/h$1;
    local1.<init>(this, paramString1, paramString2);
    localActivity.runOnUiThread(local1);
  }
  
  public j a()
  {
    return b;
  }
  
  public void a(View paramView, String paramString)
  {
    Activity localActivity = a;
    h.8 local8 = new in/juspay/mystique/h$8;
    local8.<init>(this, paramView, paramString);
    localActivity.runOnUiThread(local8);
  }
  
  public void a(View paramView, String[] paramArrayOfString, String paramString)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 11;
    if (i >= j)
    {
      PopupMenu localPopupMenu = new android/widget/PopupMenu;
      Object localObject = a;
      localPopupMenu.<init>((Context)localObject, paramView);
      for (localObject = Integer.valueOf(0);; localObject = Integer.valueOf(i))
      {
        int k = ((Integer)localObject).intValue();
        int m = paramArrayOfString.length;
        if (k >= m) {
          break;
        }
        Menu localMenu = localPopupMenu.getMenu();
        m = ((Integer)localObject).intValue();
        int n = ((Integer)localObject).intValue();
        String str = paramArrayOfString[n];
        localMenu.add(0, m, 0, str);
        i = ((Integer)localObject).intValue() + 1;
      }
      localObject = new in/juspay/mystique/h$12;
      ((h.12)localObject).<init>(this, paramString);
      localPopupMenu.setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)localObject);
      localPopupMenu.show();
    }
  }
  
  public void addViewToParent(String paramString1, String paramString2, int paramInt, String paramString3)
  {
    addViewToParent(paramString1, paramString2, paramInt, paramString3, false);
  }
  
  public void addViewToParent(String paramString1, String paramString2, int paramInt, String paramString3, boolean paramBoolean)
  {
    Activity localActivity = a;
    h.5 local5 = new in/juspay/mystique/h$5;
    local5.<init>(this, paramString1, paramString2, paramInt, paramBoolean, paramString3);
    localActivity.runOnUiThread(local5);
  }
  
  public void callAPI(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    h.4 local4 = new in/juspay/mystique/h$4;
    Object localObject = this;
    local4.<init>(this, paramString4, paramString3, paramString1, paramString2);
    localObject = new Object[0];
    local4.execute((Object[])localObject);
  }
  
  public void dismissPopUp()
  {
    b.a();
  }
  
  public void downloadFile(String paramString1, String paramString2)
  {
    h.2 local2 = new in/juspay/mystique/h$2;
    local2.<init>(this, paramString1, paramString2);
    Object[] arrayOfObject = new Object[0];
    local2.execute(arrayOfObject);
  }
  
  public String fetchData(String paramString)
  {
    return a.getSharedPreferences("DUI", 0).getString(paramString, "null");
  }
  
  public void generateUIElement(String paramString1, int paramInt, String[] paramArrayOfString, String paramString2)
  {
    Activity localActivity = a;
    h.11 local11 = new in/juspay/mystique/h$11;
    local11.<init>(this, paramString1, paramInt, paramArrayOfString, paramString2);
    localActivity.runOnUiThread(local11);
  }
  
  public String getAssetBaseFilePath()
  {
    return "/android_asset";
  }
  
  public String getExternalStorageBaseFilePath()
  {
    return Environment.getExternalStorageDirectory().getAbsolutePath();
  }
  
  public String getInternalStorageBaseFilePath()
  {
    return a.getDir("juspay", 0).getAbsolutePath();
  }
  
  public String getScreenDimensions()
  {
    DisplayMetrics localDisplayMetrics = new android/util/DisplayMetrics;
    localDisplayMetrics.<init>();
    a.getWindowManager().getDefaultDisplay().getMetrics(localDisplayMetrics);
    int i = heightPixels;
    int j = widthPixels;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    return "{\"width\":" + j + ",\"height\":" + i + " }";
  }
  
  public String getState()
  {
    String str = f;
    if (str != null) {}
    for (str = f;; str = "{}") {
      return str;
    }
  }
  
  public boolean isFilePresent(String paramString)
  {
    File localFile = new java/io/File;
    localFile.<init>(paramString);
    return localFile.exists();
  }
  
  public String loadFile(String paramString)
  {
    for (;;)
    {
      try
      {
        localObject = a;
        arrayOfByte = f.a((Context)localObject, paramString);
        if (arrayOfByte != null) {
          continue;
        }
        localObject = "";
      }
      catch (Exception localException)
      {
        Object localObject;
        byte[] arrayOfByte;
        String str = "";
        continue;
      }
      return (String)localObject;
      localObject = new java/lang/String;
      ((String)localObject).<init>(arrayOfByte);
    }
  }
  
  public String loadFileFromExternalStorage(String paramString1, String paramString2)
  {
    try
    {
      str1 = new java/lang/String;
      byte[] arrayOfByte = f.a(paramString1, paramString2);
      str1.<init>(arrayOfByte);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1;
        String str2 = "";
      }
    }
    return str1;
  }
  
  public void run(String paramString1, String paramString2)
  {
    try
    {
      Object localObject1 = b;
      localObject3 = a;
      str = "";
      localObject4 = "";
      ((j)localObject1).a(localObject3, paramString1, str, (String)localObject4);
      if (paramString2 != null)
      {
        localObject1 = g;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        str = "window.callUICallback(";
        localObject3 = ((StringBuilder)localObject3).append(str);
        localObject3 = ((StringBuilder)localObject3).append(paramString2);
        str = ",'success');";
        localObject3 = ((StringBuilder)localObject3).append(str);
        localObject3 = ((StringBuilder)localObject3).toString();
        ((d)localObject1).a((String)localObject3);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject3;
        String str;
        Object localObject4;
        Object localObject2;
        if (localException != null)
        {
          localObject2 = localException.getClass().getName();
          d.b("runInUI", (String)localObject2);
          localObject3 = e;
          str = "runInUI";
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          localObject2 = ((StringBuilder)localObject4).append((String)localObject2).append(" - ");
          localObject4 = b.b();
          localObject2 = (String)localObject4;
          ((e)localObject3).a(str, (String)localObject2);
        }
        if (paramString2 != null)
        {
          localObject2 = g;
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          localObject3 = ((StringBuilder)localObject3).append("window.callUICallback(").append(paramString2);
          str = ",'failure');";
          localObject3 = str;
          ((d)localObject2).a((String)localObject3);
        }
      }
    }
  }
  
  public void runInUI(String paramString1, String paramString2)
  {
    Activity localActivity = a;
    h.7 local7 = new in/juspay/mystique/h$7;
    local7.<init>(this, paramString1, paramString2);
    localActivity.runOnUiThread(local7);
  }
  
  public void runInUI(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Activity localActivity = a;
    h.6 local6 = new in/juspay/mystique/h$6;
    local6.<init>(this, paramString1, paramString3, paramString4, paramString2);
    localActivity.runOnUiThread(local6);
  }
  
  public void saveData(String paramString1, String paramString2)
  {
    a.getSharedPreferences("DUI", 0).edit().putString(paramString1, paramString2).commit();
  }
  
  public String saveFileToInternalStorage(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      localObject1 = a;
      localObject2 = paramString2.getBytes();
      f.a((Context)localObject1, paramString1, (byte[])localObject2);
      localObject1 = "SUCCESS";
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        String str2 = "FAILURE : ";
        localObject2 = ((StringBuilder)localObject2).append(str2);
        String str1 = localException.getMessage();
        str1 = str1;
      }
    }
    return (String)localObject1;
  }
  
  public void saveState(String paramString)
  {
    f = paramString;
  }
  
  public void setImage(int paramInt, String paramString)
  {
    Activity localActivity = a;
    h.9 local9 = new in/juspay/mystique/h$9;
    local9.<init>(this, paramInt, paramString);
    localActivity.runOnUiThread(local9);
  }
  
  public void setState(String paramString)
  {
    f = paramString;
  }
  
  public void showLoading()
  {
    Activity localActivity = a;
    h.3 local3 = new in/juspay/mystique/h$3;
    local3.<init>(this);
    localActivity.runOnUiThread(local3);
  }
  
  public void throwError(String paramString)
  {
    d.b("throwError", paramString);
  }
  
  public void toggleKeyboard(int paramInt, String paramString)
  {
    Activity localActivity = a;
    h.10 local10 = new in/juspay/mystique/h$10;
    local10.<init>(this, paramInt, paramString);
    localActivity.runOnUiThread(local10);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */