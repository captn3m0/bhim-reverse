package in.juspay.mystique;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.text.InputFilter;
import android.text.InputFilter.LengthFilter;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CalendarView.OnDateChangeListener;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class g
{
  static HashMap a;
  private static float b;
  private static float c;
  private static float d;
  private static float e;
  private static HashMap i;
  private static final String j;
  private static final Map w;
  private Activity f;
  private PopupMenu g;
  private e h;
  private String k = "-1";
  private String l = "";
  private String m = "";
  private String n = "";
  private d o;
  private String p = ":";
  private String q = ",";
  private Pattern r;
  private String s;
  private String t;
  private String u;
  private Pattern v;
  private c x;
  
  static
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    i = (HashMap)localObject;
    j = g.class.getName();
    localObject = new java/util/Hashtable;
    ((Hashtable)localObject).<init>();
    w = (Map)localObject;
    localObject = w;
    Class localClass = Boolean.TYPE;
    ((Map)localObject).put(Boolean.class, localClass);
    localObject = w;
    localClass = Character.TYPE;
    ((Map)localObject).put(Character.class, localClass);
    localObject = w;
    localClass = Byte.TYPE;
    ((Map)localObject).put(Byte.class, localClass);
    localObject = w;
    localClass = Short.TYPE;
    ((Map)localObject).put(Short.class, localClass);
    localObject = w;
    localClass = Integer.TYPE;
    ((Map)localObject).put(Integer.class, localClass);
    localObject = w;
    localClass = Long.TYPE;
    ((Map)localObject).put(Long.class, localClass);
    localObject = w;
    localClass = Float.TYPE;
    ((Map)localObject).put(Float.class, localClass);
    localObject = w;
    localClass = Double.TYPE;
    ((Map)localObject).put(Double.class, localClass);
    localObject = w;
    localClass = Void.TYPE;
    ((Map)localObject).put(Void.class, localClass);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    a = (HashMap)localObject;
  }
  
  g(Activity paramActivity, c paramc, e parame, d paramd)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = ((StringBuilder)localObject).append("(?<!\\\\)");
    String str = Pattern.quote(",");
    localObject = Pattern.compile(str);
    r = ((Pattern)localObject);
    s = "->";
    t = "_";
    u = "=";
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = ((StringBuilder)localObject).append("(?<!\\\\)");
    str = Pattern.quote(";");
    localObject = Pattern.compile(str);
    v = ((Pattern)localObject);
    o = paramd;
    f = paramActivity;
    x = paramc;
    h = parame;
    i.put("duiObj", paramd);
  }
  
  private int a(String paramString1, String paramString2, int paramInt)
  {
    int i1 = -1;
    String str = paramString1.substring(paramInt);
    int i2 = str.indexOf(paramString2);
    if ((i2 != i1) && (i2 != 0))
    {
      int i3 = paramString1.length();
      if (i2 < i3)
      {
        i3 = i2 + paramInt + -1;
        i3 = paramString1.charAt(i3);
        int i4 = 92;
        if (i3 == i4)
        {
          i2 += paramInt;
          i3 = paramString2.length();
          i2 += i3;
          i2 = a(paramString1, paramString2, i2);
        }
      }
    }
    for (;;)
    {
      return i2;
      if (i2 != i1) {
        i2 += paramInt;
      }
    }
  }
  
  private Object a(Object paramObject1, Object paramObject2, String paramString)
  {
    int i1 = 3;
    int i3 = -1;
    Object localObject1 = null;
    boolean bool2 = true;
    int i4 = 0;
    l = paramString;
    Object localObject2 = s;
    int i5 = a(paramString, (String)localObject2, 0);
    Object localObject3;
    Object localObject4;
    boolean bool3;
    if (i5 != i3)
    {
      localObject2 = s;
      localObject2 = a(paramString, localObject2)[0];
      localObject3 = t;
      int i8 = a((String)localObject2, (String)localObject3, 0);
      if (i8 == i3) {
        break label2795;
      }
      localObject3 = ((String)localObject2).substring(0, i1);
      localObject4 = "get";
      bool3 = ((String)localObject3).equals(localObject4);
      if (!bool3) {
        break label2795;
      }
      localObject3 = t;
      localObject3 = a((String)localObject2, (String)localObject3);
      localObject2 = localObject3[bool2];
      localObject3 = localObject3[0];
    }
    for (;;)
    {
      localObject4 = p;
      int i9 = a(paramString, (String)localObject4, 0);
      Object localObject5;
      Object localObject6;
      if (i9 != i3)
      {
        localObject4 = s;
        localObject5 = a(paramString, localObject4)[bool2];
        localObject4 = p;
        localObject6 = a((String)localObject5, (String)localObject4);
        localObject4 = localObject6[0];
        localObject6 = localObject6[bool2];
        int i11 = ((String)localObject3).hashCode();
        switch (i11)
        {
        default: 
          label264:
          i1 = i3;
          switch (i1)
          {
          default: 
            label268:
            localObject2 = "var_";
            i5 = a((String)localObject4, (String)localObject2, 0);
            if (i5 != i3)
            {
              localObject2 = t;
              localObject2 = a(localObject4, localObject2)[bool2];
              localObject2 = Class.forName((String)localObject4).getDeclaredField((String)localObject2);
              ((Field)localObject2).setAccessible(bool2);
              localObject3 = p;
              localObject3 = a(localObject5, localObject3)[bool2];
              localObject3 = g((String)localObject3);
              ((Field)localObject2).set(null, localObject3);
            }
            break;
          }
          break;
        }
      }
      for (;;)
      {
        return paramObject2;
        localObject4 = s;
        localObject4 = a(paramString, localObject4)[bool2];
        localObject5 = localObject4;
        localObject6 = null;
        break;
        String str1 = "this";
        boolean bool1 = ((String)localObject3).equals(str1);
        if (!bool1) {
          break label264;
        }
        bool1 = false;
        str1 = null;
        break label268;
        str1 = "parent";
        bool1 = ((String)localObject3).equals(str1);
        if (!bool1) {
          break label264;
        }
        bool1 = bool2;
        break label268;
        str1 = "ctx";
        bool1 = ((String)localObject3).equals(str1);
        if (!bool1) {
          break label264;
        }
        int i2 = 2;
        break label268;
        String str2 = "get";
        boolean bool5 = ((String)localObject3).equals(str2);
        if (!bool5) {
          break label264;
        }
        break label268;
        if (localObject6 != null)
        {
          localObject2 = paramObject1.getClass();
          localObject2 = a((Class)localObject2, (String)localObject5);
          if (localObject2 != null)
          {
            localObject1 = d((String)localObject6);
            paramObject2 = ((Method)localObject2).invoke(paramObject1, (Object[])localObject1);
          }
          else
          {
            localObject2 = x;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - classMethodDetails  ").append((String)localObject5).append(" ");
            localObject4 = a();
            localObject3 = (String)localObject4;
            ((c)localObject2).b("WARNING", (String)localObject3);
            localObject2 = h;
            localObject1 = "WARNING";
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - classMethodDetails  ").append((String)localObject5).append(" ");
            localObject4 = a();
            localObject3 = (String)localObject4;
            ((e)localObject2).a((String)localObject1, (String)localObject3);
          }
        }
        else
        {
          localObject2 = paramObject1.getClass();
          localObject2 = a((Class)localObject2, (String)localObject5);
          if (localObject2 != null)
          {
            paramObject2 = ((Method)localObject2).invoke(paramObject1, null);
          }
          else
          {
            localObject2 = x;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - this  classMethodDetails ").append((String)localObject5).append(" ");
            localObject4 = a();
            localObject3 = (String)localObject4;
            ((c)localObject2).b("WARNING", (String)localObject3);
            localObject2 = h;
            localObject1 = "WARNING";
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - this  classMethodDetails ").append((String)localObject5).append(" ");
            localObject4 = a();
            localObject3 = (String)localObject4;
            ((e)localObject2).a((String)localObject1, (String)localObject3);
            continue;
            if (localObject6 != null)
            {
              localObject2 = paramObject1.getClass();
              localObject2 = a((Class)localObject2, (String)localObject5);
              if (localObject2 != null)
              {
                localObject1 = d((String)localObject6);
                paramObject2 = ((Method)localObject2).invoke(paramObject1, (Object[])localObject1);
              }
              else
              {
                localObject2 = x;
                localObject3 = new java/lang/StringBuilder;
                ((StringBuilder)localObject3).<init>();
                localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - parent  classMethodDetails ").append((String)localObject5).append(" ");
                localObject4 = a();
                localObject3 = (String)localObject4;
                ((c)localObject2).b("WARNING", (String)localObject3);
                localObject2 = h;
                localObject1 = "WARNING";
                localObject3 = new java/lang/StringBuilder;
                ((StringBuilder)localObject3).<init>();
                localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - parent  classMethodDetails ").append((String)localObject5).append(" ");
                localObject4 = a();
                localObject3 = (String)localObject4;
                ((e)localObject2).a((String)localObject1, (String)localObject3);
              }
            }
            else
            {
              localObject2 = paramObject1.getClass();
              localObject2 = a((Class)localObject2, (String)localObject5);
              if (localObject2 != null)
              {
                paramObject2 = ((Method)localObject2).invoke(paramObject1, null);
              }
              else
              {
                localObject2 = x;
                localObject3 = new java/lang/StringBuilder;
                ((StringBuilder)localObject3).<init>();
                localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - parent  classMethodDetails ").append((String)localObject5).append(" ");
                localObject4 = a();
                localObject3 = (String)localObject4;
                ((c)localObject2).b("WARNING", (String)localObject3);
                localObject2 = h;
                localObject1 = "WARNING";
                localObject3 = new java/lang/StringBuilder;
                ((StringBuilder)localObject3).<init>();
                localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - parent classMethodDetails  ").append((String)localObject5).append(" ");
                localObject4 = a();
                localObject3 = (String)localObject4;
                ((e)localObject2).a((String)localObject1, (String)localObject3);
                continue;
                if (localObject6 != null)
                {
                  localObject2 = f.getClass();
                  localObject2 = a((Class)localObject2, (String)localObject5);
                  if (localObject2 != null)
                  {
                    localObject1 = f;
                    localObject3 = d((String)localObject6);
                    paramObject2 = ((Method)localObject2).invoke(localObject1, (Object[])localObject3);
                  }
                  else
                  {
                    localObject2 = x;
                    localObject3 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject3).<init>();
                    localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - ctx  classMethodDetails ").append((String)localObject5).append(" ");
                    localObject4 = a();
                    localObject3 = (String)localObject4;
                    ((c)localObject2).b("WARNING", (String)localObject3);
                    localObject2 = h;
                    localObject1 = "WARNING";
                    localObject3 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject3).<init>();
                    localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - ctx  classMethodDetails ").append((String)localObject5).append(" ");
                    localObject4 = a();
                    localObject3 = (String)localObject4;
                    ((e)localObject2).a((String)localObject1, (String)localObject3);
                  }
                }
                else
                {
                  localObject2 = f.getClass();
                  localObject2 = a((Class)localObject2, (String)localObject5);
                  if (localObject2 != null)
                  {
                    localObject3 = f;
                    paramObject2 = ((Method)localObject2).invoke(localObject3, null);
                  }
                  else
                  {
                    localObject2 = x;
                    localObject3 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject3).<init>();
                    localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - ctx classMethodDetails  ").append((String)localObject5).append(" ");
                    localObject4 = a();
                    localObject3 = (String)localObject4;
                    ((c)localObject2).b("WARNING", (String)localObject3);
                    localObject2 = h;
                    localObject1 = "WARNING";
                    localObject3 = new java/lang/StringBuilder;
                    ((StringBuilder)localObject3).<init>();
                    localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - ctx classMethodDetails  ").append((String)localObject5).append(" ");
                    localObject4 = a();
                    localObject3 = (String)localObject4;
                    ((e)localObject2).a((String)localObject1, (String)localObject3);
                    continue;
                    if (localObject2 != null)
                    {
                      localObject3 = i.get(localObject2);
                      str1 = t;
                      i4 = a((String)localObject4, str1, 0);
                      if ((i4 == i3) && (localObject3 != null))
                      {
                        if (localObject6 != null)
                        {
                          localObject2 = localObject3.getClass();
                          localObject2 = a((Class)localObject2, (String)localObject5);
                          if (localObject2 != null)
                          {
                            localObject1 = d((String)localObject6);
                            paramObject2 = ((Method)localObject2).invoke(localObject3, (Object[])localObject1);
                          }
                          else
                          {
                            localObject2 = x;
                            localObject3 = new java/lang/StringBuilder;
                            ((StringBuilder)localObject3).<init>();
                            localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - get classMethodDetails ").append((String)localObject5).append(" ");
                            localObject4 = a();
                            localObject3 = (String)localObject4;
                            ((c)localObject2).b("WARNING", (String)localObject3);
                            localObject2 = h;
                            localObject1 = "WARNING";
                            localObject3 = new java/lang/StringBuilder;
                            ((StringBuilder)localObject3).<init>();
                            localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - get classMethodDetails ").append((String)localObject5).append(" ");
                            localObject4 = a();
                            localObject3 = (String)localObject4;
                            ((e)localObject2).a((String)localObject1, (String)localObject3);
                          }
                        }
                        else
                        {
                          localObject2 = localObject3.getClass();
                          localObject2 = a((Class)localObject2, (String)localObject5);
                          if (localObject2 != null)
                          {
                            paramObject2 = ((Method)localObject2).invoke(localObject3, null);
                          }
                          else
                          {
                            localObject2 = x;
                            localObject3 = new java/lang/StringBuilder;
                            ((StringBuilder)localObject3).<init>();
                            localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - get classMethodDetails : ").append((String)localObject5).append(" ");
                            localObject4 = a();
                            localObject3 = (String)localObject4;
                            ((c)localObject2).b("WARNING", (String)localObject3);
                            localObject2 = h;
                            localObject1 = "WARNING";
                            localObject3 = new java/lang/StringBuilder;
                            ((StringBuilder)localObject3).<init>();
                            localObject3 = ((StringBuilder)localObject3).append(" isNull : fn__runCommand - get classMethodDetails : ").append((String)localObject5).append(" ");
                            localObject4 = a();
                            localObject3 = (String)localObject4;
                            ((e)localObject2).a((String)localObject1, (String)localObject3);
                          }
                        }
                      }
                      else if (localObject3 != null)
                      {
                        localObject1 = t;
                        localObject1 = a(localObject4, localObject1)[bool2];
                        localObject2 = i.get(localObject2);
                        localObject3 = p;
                        localObject3 = a(localObject5, localObject3)[bool2];
                        paramObject2 = a(localObject2, (String)localObject1, (String)localObject3);
                      }
                      else
                      {
                        localObject1 = x;
                        localObject4 = new java/lang/StringBuilder;
                        ((StringBuilder)localObject4).<init>();
                        localObject4 = ((StringBuilder)localObject4).append(" isNull : fn__runCommand - get_").append((String)localObject2).append(" is null ");
                        localObject5 = a();
                        localObject4 = (String)localObject5;
                        ((c)localObject1).b("WARNING", (String)localObject4);
                        localObject1 = h;
                        localObject3 = "WARNING";
                        localObject4 = new java/lang/StringBuilder;
                        ((StringBuilder)localObject4).<init>();
                        localObject5 = " isNull : fn__runCommand - get_";
                        localObject2 = ((StringBuilder)localObject4).append((String)localObject5).append((String)localObject2).append(" is null ");
                        localObject4 = a();
                        localObject2 = (String)localObject4;
                        ((e)localObject1).a((String)localObject3, (String)localObject2);
                        continue;
                        localObject2 = "new";
                        int i6 = ((String)localObject5).equals(localObject2);
                        if (i6 == 0)
                        {
                          localObject2 = p;
                          localObject2 = a(localObject5, localObject2)[0];
                          localObject4 = "new";
                          i6 = ((String)localObject2).equals(localObject4);
                          if (i6 == 0) {}
                        }
                        else
                        {
                          if (localObject6 != null)
                          {
                            localObject2 = "in.juspay.mystique.DuiInvocationHandler";
                            i6 = ((String)localObject3).equals(localObject2);
                            if (i6 != 0)
                            {
                              localObject2 = d((String)localObject6);
                              paramObject2 = new in/juspay/mystique/b;
                              localObject2 = localObject2[0];
                              localObject1 = o;
                              ((b)paramObject2).<init>(localObject2, (d)localObject1);
                              continue;
                            }
                            localObject1 = e((String)localObject6);
                            localObject3 = Class.forName((String)localObject3).getConstructors();
                            i6 = 0;
                            localObject2 = null;
                            for (;;)
                            {
                              i9 = localObject3.length;
                              if (i6 >= i9) {
                                break;
                              }
                              localObject4 = localObject3[i6].getParameterTypes();
                              int i10 = localObject4.length;
                              int i12 = h((String)localObject6);
                              if (i10 == i12)
                              {
                                localObject4 = localObject3[i6].getParameterTypes();
                                boolean bool4 = a((Class[])localObject4, (Class[])localObject1);
                                if (bool4)
                                {
                                  localObject2 = localObject3[i6];
                                  localObject1 = d((String)localObject6);
                                  paramObject2 = ((Constructor)localObject2).newInstance((Object[])localObject1);
                                  break;
                                }
                              }
                              i6 += 1;
                            }
                          }
                          localObject2 = Class.forName((String)localObject3);
                          paramObject2 = ((Class)localObject2).newInstance();
                          continue;
                        }
                        localObject2 = Class.forName((String)localObject3);
                        localObject2 = a((Class)localObject2, (String)localObject5);
                        if (localObject2 != null)
                        {
                          localObject3 = ((Method)localObject2).getName();
                          localObject4 = "forName";
                          bool3 = ((String)localObject3).equals(localObject4);
                          if (bool3)
                          {
                            localObject2 = (String)g((String)localObject6);
                            paramObject2 = Class.forName((String)localObject2);
                          }
                          else
                          {
                            localObject3 = d((String)localObject6);
                            paramObject2 = ((Method)localObject2).invoke(null, (Object[])localObject3);
                            continue;
                            if (paramObject2 == null)
                            {
                              localObject2 = p;
                              i7 = a(paramString, (String)localObject2, 0);
                              if (i7 != i3)
                              {
                                localObject2 = p;
                                localObject2 = a(paramString, localObject2)[bool2];
                                localObject1 = paramObject1.getClass();
                                localObject1 = a((Class)localObject1, paramString);
                                localObject2 = d((String)localObject2);
                                paramObject2 = ((Method)localObject1).invoke(paramObject1, (Object[])localObject2);
                              }
                              else
                              {
                                localObject2 = paramObject1.getClass();
                                localObject2 = a((Class)localObject2, paramString);
                                paramObject2 = ((Method)localObject2).invoke(paramObject1, null);
                              }
                            }
                            else
                            {
                              localObject2 = p;
                              i7 = a(paramString, (String)localObject2, 0);
                              if (i7 != i3)
                              {
                                localObject2 = p;
                                localObject2 = a(paramString, localObject2)[bool2];
                                localObject1 = paramObject2.getClass();
                                localObject1 = a((Class)localObject1, paramString);
                                localObject2 = d((String)localObject2);
                                paramObject2 = ((Method)localObject1).invoke(paramObject2, (Object[])localObject2);
                              }
                              else
                              {
                                localObject2 = paramObject2.getClass();
                                localObject2 = a((Class)localObject2, paramString);
                                paramObject2 = ((Method)localObject2).invoke(paramObject2, null);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      label2795:
      localObject3 = localObject2;
      int i7 = 0;
      localObject2 = null;
    }
  }
  
  private Object a(Object paramObject, String paramString1, String paramString2)
  {
    int i1 = 0;
    Object localObject1 = null;
    label40:
    Object localObject4;
    int i2;
    Object localObject3;
    int i3;
    Object localObject5;
    try
    {
      Object localObject2 = paramObject.getClass();
      localObject1 = ((Class)localObject2).getField(paramString1);
      if (localObject1 == null) {
        break label118;
      }
      localObject2 = g(paramString2);
      ((Field)localObject1).set(paramObject, localObject2);
      return paramObject;
    }
    catch (NoSuchFieldException localNoSuchFieldException)
    {
      localObject4 = paramObject.getClass().getFields();
      i2 = localObject4.length;
      localObject3 = null;
      i3 = 0;
      localObject5 = null;
    }
    label67:
    if (i3 < i2)
    {
      localObject3 = localObject4[i3];
      String str = ((Field)localObject3).getName();
      boolean bool = str.equals(paramString1);
      if (!bool) {
        break label174;
      }
    }
    for (;;)
    {
      i1 = i3 + 1;
      i3 = i1;
      localObject1 = localObject3;
      break label67;
      break;
      label118:
      localObject3 = x;
      localObject1 = j;
      localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      localObject4 = "Couldn't set field for ";
      localObject5 = (String)localObject4 + paramString1;
      ((c)localObject3).a((String)localObject1, (String)localObject5);
      break label40;
      label174:
      localObject3 = localObject1;
    }
  }
  
  private Method a(Class paramClass, String paramString)
  {
    i1 = 1;
    localObject1 = null;
    if (paramClass == null) {}
    for (;;)
    {
      return (Method)localObject1;
      Object localObject2 = p;
      int i2 = a(paramString, (String)localObject2, 0);
      int i3 = -1;
      if (i2 != i3)
      {
        localObject2 = p;
        localObject2 = a(paramString, (String)localObject2);
        paramString = localObject2[0];
      }
      for (localObject2 = localObject2[i1];; localObject2 = null)
      {
        if (localObject2 != null) {
          localObject1 = e((String)localObject2);
        }
        localObject2 = new in/juspay/mystique/g$a;
        ((g.a)localObject2).<init>(paramClass, paramString, (Class[])localObject1);
        localHashMap = a;
        boolean bool = localHashMap.containsKey(localObject2);
        if (!bool) {
          break label135;
        }
        localObject1 = (Method)a.get(localObject2);
        break;
        i2 = 0;
      }
      try
      {
        label135:
        localObject1 = a(paramClass, paramString, (Class[])localObject1);
      }
      catch (NoSuchMethodException localNoSuchMethodException)
      {
        for (;;)
        {
          if (localObject1 != null)
          {
            int i4 = localObject1.length;
            if (i4 == i1)
            {
              localObject1 = localObject1[0];
              localObject1 = a(paramClass, paramString, (Class)localObject1);
              continue;
            }
          }
          localObject1 = b(paramClass, paramString, (Class[])localObject1);
        }
      }
      HashMap localHashMap = a;
      localHashMap.put(localObject2, localObject1);
    }
  }
  
  private Method a(Class paramClass1, String paramString, Class paramClass2)
  {
    String str = null;
    int i1 = a(paramClass2);
    if (i1 != 0) {
      i1 = 1;
    }
    for (;;)
    {
      int i3;
      try
      {
        localObject1 = new Class[i1];
        i3 = 0;
        Object localObject2 = w;
        localObject2 = ((Map)localObject2).get(paramClass2);
        localObject2 = (Class)localObject2;
        localObject1[0] = localObject2;
        localObject2 = paramClass1.getMethod(paramString, (Class[])localObject1);
        return (Method)localObject2;
      }
      catch (NoSuchMethodException localNoSuchMethodException1) {}
      label139:
      do
      {
        localObject1 = paramClass2.getInterfaces();
        i3 = localObject1.length;
        i1 = 0;
        Object localObject3 = null;
        for (;;)
        {
          if (i1 >= i3) {
            break label139;
          }
          Object localObject4 = localObject1[i1];
          int i4 = 1;
          try
          {
            Class[] arrayOfClass = new Class[i4];
            arrayOfClass[0] = localObject4;
            localObject3 = paramClass1.getMethod(paramString, arrayOfClass);
          }
          catch (NoSuchMethodException localNoSuchMethodException3)
          {
            i1 += 1;
          }
        }
        i2 = 1;
        try
        {
          localObject3 = new Class[i2];
          localObject1 = null;
          localObject3[0] = paramClass2;
          localObject3 = paramClass1.getMethod(paramString, (Class[])localObject3);
        }
        catch (NoSuchMethodException localNoSuchMethodException2)
        {
          paramClass2 = paramClass2.getSuperclass();
        }
      } while (paramClass2 != null);
      c localc = x;
      str = j;
      Object localObject1 = "Never reach here";
      localc.b(str, (String)localObject1);
      int i2 = 0;
      localc = null;
    }
  }
  
  private Method a(Class paramClass, String paramString, Class[] paramArrayOfClass)
  {
    return paramClass.getMethod(paramString, paramArrayOfClass);
  }
  
  public static boolean a(Class paramClass)
  {
    return w.containsKey(paramClass);
  }
  
  private boolean a(Class[] paramArrayOfClass1, Class[] paramArrayOfClass2)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    int i1 = 0;
    int i2 = paramArrayOfClass1.length;
    Object localObject1;
    Object localObject2;
    boolean bool3;
    if (i1 < i2)
    {
      localObject1 = paramArrayOfClass2[i1];
      if (localObject1 != null)
      {
        localObject1 = paramArrayOfClass1[i1];
        if (localObject1 != null)
        {
          localObject1 = paramArrayOfClass1[i1];
          localObject2 = Object.class;
          bool3 = localObject1.equals(localObject2);
          if (bool3)
          {
            localObject1 = paramArrayOfClass2[i1];
            bool3 = ((Class)localObject1).isPrimitive();
            if (!bool3) {}
          }
          else
          {
            localObject1 = paramArrayOfClass1[i1];
            localObject2 = paramArrayOfClass2[i1];
            bool3 = localObject1.equals(localObject2);
            if (!bool3)
            {
              localObject1 = paramArrayOfClass1[i1];
              bool3 = ((Class)localObject1).isPrimitive();
              if (!bool3) {
                break label243;
              }
              localObject1 = paramArrayOfClass2[i1];
              bool3 = ((Class)localObject1).isArray();
              if (bool3) {
                break label243;
              }
            }
          }
        }
      }
    }
    for (;;)
    {
      try
      {
        localObject1 = paramArrayOfClass2[i1];
        localObject2 = "TYPE";
        localObject1 = ((Class)localObject1).getField((String)localObject2);
        localObject2 = null;
        localObject1 = ((Field)localObject1).get(null);
        localObject1 = (Class)localObject1;
        localObject2 = paramArrayOfClass1[i1];
        bool3 = localObject1.equals(localObject2);
        if (bool3) {
          break label230;
        }
      }
      catch (Exception localException)
      {
        label203:
        localObject1 = localException.getMessage();
        localObject2 = "java.lang.NoSuchFieldException";
        bool3 = ((String)localObject1).equals(localObject2);
        if (bool3) {
          continue;
        }
      }
      return bool2;
      label230:
      label243:
      boolean bool4;
      label302:
      do
      {
        do
        {
          int i3 = i1 + 1;
          i1 = i3;
          break;
          localObject1 = paramArrayOfClass1[i1];
          localObject2 = ClassLoader.class;
          bool4 = localObject1.equals(localObject2);
          if (!bool4) {
            break label302;
          }
          localObject1 = paramArrayOfClass2[i1].getName();
          localObject2 = "dalvik.system.PathClassLoader";
          bool4 = ((String)localObject1).equals(localObject2);
        } while (!bool4);
        bool2 = bool1;
        break label203;
        localObject1 = paramArrayOfClass1[i1];
        localObject2 = paramArrayOfClass2[i1];
        bool4 = localObject1.equals(localObject2);
        if (!bool4) {
          break label203;
        }
        localObject1 = paramArrayOfClass1[i1];
        localObject2 = paramArrayOfClass2[i1];
        bool4 = ((Class)localObject1).isAssignableFrom((Class)localObject2);
      } while (bool4);
      continue;
      bool2 = bool1;
    }
  }
  
  private String[] a(String paramString1, String paramString2)
  {
    int i1 = 1;
    int i2 = a(paramString1, paramString2, 0);
    int i3 = -1;
    String[] arrayOfString;
    if (i2 == i3)
    {
      arrayOfString = new String[i1];
      arrayOfString[0] = paramString1;
    }
    for (;;)
    {
      return arrayOfString;
      i3 = 2;
      arrayOfString = new String[i3];
      String str1 = paramString1.substring(0, i2);
      arrayOfString[0] = str1;
      int i4 = paramString2.length();
      i2 += i4;
      String str2 = paramString1.substring(i2);
      arrayOfString[i1] = str2;
    }
  }
  
  private Method b(Class paramClass, String paramString, Class[] paramArrayOfClass)
  {
    Method[] arrayOfMethod = paramClass.getMethods();
    int i1 = arrayOfMethod.length;
    int i2 = 0;
    Method localMethod = null;
    int i3 = 0;
    if (i3 < i1)
    {
      localMethod = arrayOfMethod[i3];
      Object localObject = localMethod.getName();
      boolean bool1 = ((String)localObject).equals(paramString);
      if ((bool1) && (paramArrayOfClass != null))
      {
        localObject = localMethod.getParameterTypes();
        int i4 = localObject.length;
        int i5 = paramArrayOfClass.length;
        if (i4 == i5)
        {
          localObject = localMethod.getParameterTypes();
          boolean bool2 = a((Class[])localObject, paramArrayOfClass);
          if (!bool2) {}
        }
      }
    }
    for (;;)
    {
      return localMethod;
      i2 = i3 + 1;
      i3 = i2;
      break;
      i2 = 0;
      localMethod = null;
    }
  }
  
  private Object[] d(String paramString)
  {
    int i1 = 0;
    Object localObject1 = null;
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject2 = q;
    int i2 = a(paramString, (String)localObject2, 0);
    int i3 = -1;
    if (i2 == i3)
    {
      localObject1 = g(paramString);
      localArrayList.add(localObject1);
    }
    for (;;)
    {
      return localArrayList.toArray();
      localObject2 = r.split(paramString);
      i3 = localObject2.length;
      while (i1 < i3)
      {
        Object localObject3 = localObject2[i1];
        localObject3 = g((String)localObject3);
        localArrayList.add(localObject3);
        i1 += 1;
      }
    }
  }
  
  private Class[] e(String paramString)
  {
    int i1 = 1;
    int i2 = 0;
    int i3;
    Object localObject;
    if (paramString == null)
    {
      i3 = 0;
      localObject = null;
    }
    for (;;)
    {
      return (Class[])localObject;
      localObject = q;
      i3 = a(paramString, (String)localObject, 0);
      int i4 = -1;
      if (i3 != i4)
      {
        localObject = r;
        String[] arrayOfString = ((Pattern)localObject).split(paramString);
        i3 = arrayOfString.length;
        if (i3 > i1)
        {
          i3 = arrayOfString.length;
          arrayOfClass = new Class[i3];
          for (;;)
          {
            i3 = arrayOfString.length;
            if (i2 >= i3) {
              break;
            }
            localObject = arrayOfString[i2];
            localObject = (Class)f((String)localObject);
            arrayOfClass[i2] = localObject;
            i3 = i2 + 1;
            i2 = i3;
          }
          localObject = arrayOfClass;
          continue;
        }
      }
      Class[] arrayOfClass = new Class[i1];
      localObject = (Class)f(paramString);
      arrayOfClass[0] = localObject;
      localObject = arrayOfClass;
    }
  }
  
  private Object f(String paramString)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    Object localObject1 = null;
    String str1;
    Object localObject2;
    Object localObject3;
    if (paramString != null)
    {
      str1 = t;
      localObject2 = a(paramString, str1);
      localObject3 = localObject2[0];
      int i12 = -1;
      int i13 = ((String)localObject3).hashCode();
      switch (i13)
      {
      default: 
        bool2 = i12;
        label163:
        switch (bool2)
        {
        }
        break;
      }
    }
    for (;;)
    {
      localObject1 = String.class;
      for (;;)
      {
        return localObject1;
        String str2 = "i";
        boolean bool13 = ((String)localObject3).equals(str2);
        if (!bool13) {
          break;
        }
        break label163;
        localObject1 = "b";
        bool2 = ((String)localObject3).equals(localObject1);
        if (!bool2) {
          break;
        }
        bool2 = bool1;
        break label163;
        localObject1 = "cs";
        bool2 = ((String)localObject3).equals(localObject1);
        if (!bool2) {
          break;
        }
        int i1 = 2;
        break label163;
        localObject1 = "f";
        boolean bool3 = ((String)localObject3).equals(localObject1);
        if (!bool3) {
          break;
        }
        int i2 = 3;
        break label163;
        localObject1 = "dp";
        boolean bool4 = ((String)localObject3).equals(localObject1);
        if (!bool4) {
          break;
        }
        int i3 = 4;
        break label163;
        localObject1 = "sp";
        boolean bool5 = ((String)localObject3).equals(localObject1);
        if (!bool5) {
          break;
        }
        int i4 = 5;
        break label163;
        localObject1 = "l";
        boolean bool6 = ((String)localObject3).equals(localObject1);
        if (!bool6) {
          break;
        }
        int i5 = 6;
        break label163;
        localObject1 = "get";
        boolean bool7 = ((String)localObject3).equals(localObject1);
        if (!bool7) {
          break;
        }
        int i6 = 7;
        break label163;
        localObject1 = "dpf";
        boolean bool8 = ((String)localObject3).equals(localObject1);
        if (!bool8) {
          break;
        }
        int i7 = 8;
        break label163;
        localObject1 = "strget";
        boolean bool9 = ((String)localObject3).equals(localObject1);
        if (!bool9) {
          break;
        }
        int i8 = 9;
        break label163;
        localObject1 = "ctx";
        boolean bool10 = ((String)localObject3).equals(localObject1);
        if (!bool10) {
          break;
        }
        int i9 = 10;
        break label163;
        localObject1 = "s";
        boolean bool11 = ((String)localObject3).equals(localObject1);
        if (!bool11) {
          break;
        }
        int i10 = 11;
        break label163;
        localObject1 = "null";
        boolean bool12 = ((String)localObject3).equals(localObject1);
        if (!bool12) {
          break;
        }
        int i11 = 12;
        break label163;
        localObject1 = Integer.TYPE;
        continue;
        localObject1 = Boolean.TYPE;
        continue;
        localObject1 = CharSequence.class;
        continue;
        localObject1 = Float.TYPE;
        continue;
        localObject1 = Integer.TYPE;
        continue;
        localObject1 = Float.class;
        continue;
        localObject1 = Long.TYPE;
        continue;
        localObject1 = i;
        str1 = localObject2[bool1];
        localObject1 = ((HashMap)localObject1).get(str1);
        if (localObject1 != null)
        {
          localObject1 = localObject1.getClass();
        }
        else
        {
          localObject1 = x;
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          localObject4 = ((StringBuilder)localObject4).append(" isNull : fn__getClassType - ").append(paramString).append(" ");
          localObject2 = a();
          localObject4 = (String)localObject2;
          ((c)localObject1).b("WARNING", (String)localObject4);
          localObject1 = h;
          str1 = "WARNING";
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          localObject4 = ((StringBuilder)localObject4).append(" isNull : fn__getClassType - ").append(paramString).append(" ");
          localObject2 = a();
          localObject4 = (String)localObject2;
          ((e)localObject1).a(str1, (String)localObject4);
          localObject1 = Float.TYPE;
          continue;
          localObject1 = CharSequence.class;
          continue;
          localObject1 = Context.class;
          continue;
          localObject1 = String.class;
          continue;
          i11 = 0;
          localObject1 = null;
        }
      }
      localObject1 = x;
      Object localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject4 = ((StringBuilder)localObject4).append(" isNull : fn__getClassType -  toConvert ");
      localObject2 = a();
      localObject4 = (String)localObject2;
      ((c)localObject1).b("WARNING", (String)localObject4);
      localObject1 = h;
      str1 = "WARNING";
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject4 = ((StringBuilder)localObject4).append(" isNull : fn__getClassType -  toConvert ");
      localObject2 = a();
      localObject4 = (String)localObject2;
      ((e)localObject1).a(str1, (String)localObject4);
    }
  }
  
  private Object g(String paramString)
  {
    float f1 = 0.0F;
    Object localObject1 = null;
    boolean bool1 = true;
    boolean bool2 = false;
    Object localObject2 = null;
    int i11 = 92;
    int i12 = -1;
    Object localObject3;
    Object localObject4;
    if (paramString != null)
    {
      x.a("getValue!", paramString);
      localObject3 = t;
      localObject3 = a(paramString, (String)localObject3);
      localObject4 = localObject3[0];
      localObject3 = localObject3[bool1];
      int i13 = ((String)localObject3).indexOf(i11);
      String str1;
      String str2;
      if (i13 != i12)
      {
        str1 = ";";
        i13 = ((String)localObject3).indexOf(str1);
        if (i13 != i12)
        {
          str1 = "\\\\;";
          str2 = ";";
          localObject3 = ((String)localObject3).replace(str1, str2);
        }
      }
      i13 = ((String)localObject3).indexOf(i11);
      if (i13 != i12)
      {
        str1 = "_";
        i13 = ((String)localObject3).indexOf(str1);
        if (i13 != i12)
        {
          str1 = "\\\\_";
          str2 = "_";
          localObject3 = ((String)localObject3).replace(str1, str2);
        }
      }
      i13 = ((String)localObject3).indexOf(i11);
      if (i13 != i12)
      {
        str1 = ":";
        i13 = ((String)localObject3).indexOf(str1);
        if (i13 != i12)
        {
          str1 = "\\\\:";
          str2 = ":";
          localObject3 = ((String)localObject3).replace(str1, str2);
        }
      }
      i13 = ((String)localObject3).indexOf(i11);
      if (i13 != i12)
      {
        str1 = ",";
        i13 = ((String)localObject3).indexOf(str1);
        if (i13 != i12)
        {
          str1 = "\\\\,";
          str2 = ",";
          localObject3 = ((String)localObject3).replace(str1, str2);
        }
      }
      i13 = ((String)localObject3).indexOf(i11);
      if (i13 != i12)
      {
        str1 = "=";
        i13 = ((String)localObject3).indexOf(str1);
        if (i13 != i12)
        {
          str1 = "\\\\=";
          str2 = "=";
          localObject3 = ((String)localObject3).replace(str1, str2);
        }
      }
      if (localObject3 != null)
      {
        i13 = ((String)localObject4).hashCode();
        switch (i13)
        {
        default: 
          bool2 = i12;
          label468:
          switch (bool2)
          {
          }
          break;
        }
      }
    }
    for (;;)
    {
      return localObject3;
      String str3 = "i";
      bool1 = ((String)localObject4).equals(str3);
      if (!bool1) {
        break;
      }
      break label468;
      localObject2 = "b";
      bool2 = ((String)localObject4).equals(localObject2);
      if (!bool2) {
        break;
      }
      bool2 = bool1;
      break label468;
      localObject2 = "f";
      bool2 = ((String)localObject4).equals(localObject2);
      if (!bool2) {
        break;
      }
      int i1 = 2;
      break label468;
      localObject2 = "s";
      boolean bool3 = ((String)localObject4).equals(localObject2);
      if (!bool3) {
        break;
      }
      int i2 = 3;
      break label468;
      localObject2 = "sp";
      boolean bool4 = ((String)localObject4).equals(localObject2);
      if (!bool4) {
        break;
      }
      int i3 = 4;
      break label468;
      localObject2 = "dp";
      boolean bool5 = ((String)localObject4).equals(localObject2);
      if (!bool5) {
        break;
      }
      int i4 = 5;
      break label468;
      localObject2 = "dpf";
      boolean bool6 = ((String)localObject4).equals(localObject2);
      if (!bool6) {
        break;
      }
      int i5 = 6;
      break label468;
      localObject2 = "l";
      boolean bool7 = ((String)localObject4).equals(localObject2);
      if (!bool7) {
        break;
      }
      int i6 = 7;
      break label468;
      localObject2 = "get";
      boolean bool8 = ((String)localObject4).equals(localObject2);
      if (!bool8) {
        break;
      }
      int i7 = 8;
      break label468;
      localObject2 = "ctx";
      boolean bool9 = ((String)localObject4).equals(localObject2);
      if (!bool9) {
        break;
      }
      int i8 = 9;
      break label468;
      localObject2 = "null";
      boolean bool10 = ((String)localObject4).equals(localObject2);
      if (!bool10) {
        break;
      }
      int i9 = 10;
      break label468;
      localObject2 = "strget";
      boolean bool11 = ((String)localObject4).equals(localObject2);
      if (!bool11) {
        break;
      }
      int i10 = 11;
      break label468;
      int i14 = Integer.parseInt((String)localObject3);
      localObject3 = Integer.valueOf(i14);
      continue;
      boolean bool12 = Boolean.parseBoolean((String)localObject3);
      localObject3 = Boolean.valueOf(bool12);
      continue;
      float f2 = Float.parseFloat((String)localObject3);
      localObject3 = Float.valueOf(f2);
      continue;
      f2 = Float.parseFloat((String)localObject3);
      localObject1 = f.getResources().getDisplayMetrics();
      f1 = scaledDensity;
      f2 *= f1;
      localObject3 = Float.valueOf(f2);
      continue;
      int i15 = Integer.parseInt((String)localObject3);
      i15 = a(i15);
      localObject3 = Integer.valueOf(i15);
      continue;
      f2 = Float.parseFloat((String)localObject3);
      f2 = a(f2);
      localObject3 = Float.valueOf(f2);
      continue;
      long l1 = Long.parseLong((String)localObject3);
      localObject3 = Long.valueOf(l1);
      continue;
      localObject1 = i;
      localObject3 = ((HashMap)localObject1).get(localObject3);
      continue;
      localObject3 = f;
      continue;
      i15 = 0;
      localObject3 = null;
      f2 = 0.0F;
      continue;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject2 = i;
      localObject3 = ((HashMap)localObject2).get(localObject3);
      localObject3 = ((StringBuilder)localObject1).append(localObject3);
      localObject1 = "";
      localObject3 = (String)localObject1;
      continue;
      localObject1 = x;
      Object localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      localObject5 = ((StringBuilder)localObject5).append(" isNull : fn__getValue - value ");
      str3 = a();
      localObject5 = str3;
      ((c)localObject1).b("WARNING", (String)localObject5);
      localObject1 = h;
      localObject2 = "WARNING";
      localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      localObject5 = ((StringBuilder)localObject5).append(" isNull : fn__getValue - value ");
      str3 = a();
      localObject5 = str3;
      ((e)localObject1).a((String)localObject2, (String)localObject5);
      continue;
      localObject3 = x;
      localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      localObject5 = ((StringBuilder)localObject5).append(" isNull : fn__getValue - value ");
      str3 = a();
      localObject5 = str3;
      ((c)localObject3).b("WARNING", (String)localObject5);
      localObject3 = h;
      localObject2 = "WARNING";
      localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      localObject5 = ((StringBuilder)localObject5).append(" isNull : fn__getValue - value ");
      str3 = a();
      localObject5 = str3;
      ((e)localObject3).a((String)localObject2, (String)localObject5);
      i15 = 0;
      localObject3 = null;
      f2 = 0.0F;
    }
  }
  
  private int h(String paramString)
  {
    return r.split(paramString).length;
  }
  
  public float a(float paramFloat)
  {
    int i1 = 0;
    float f1 = 0.0F;
    DisplayMetrics localDisplayMetrics = null;
    boolean bool = paramFloat < 0.0F;
    if (bool)
    {
      localDisplayMetrics = f.getResources().getDisplayMetrics();
      i1 = Math.round(density * paramFloat);
      f1 = i1;
    }
    return f1;
  }
  
  public int a(int paramInt)
  {
    DisplayMetrics localDisplayMetrics;
    float f2;
    int i1;
    if (paramInt > 0)
    {
      localDisplayMetrics = f.getResources().getDisplayMetrics();
      float f1 = paramInt;
      f2 = density * f1;
      i1 = Math.round(f2);
    }
    for (;;)
    {
      return i1;
      i1 = 0;
      f2 = 0.0F;
      localDisplayMetrics = null;
    }
  }
  
  public Object a(Object paramObject, String paramString)
  {
    int i1 = 1;
    String[] arrayOfString = v.split(paramString);
    Object localObject1 = null;
    int i2 = arrayOfString.length;
    int i3 = 0;
    if (i3 < i2)
    {
      Object localObject2 = arrayOfString[i3];
      Object localObject3 = "";
      boolean bool = ((String)localObject2).equals(localObject3);
      if (!bool)
      {
        localObject3 = u;
        int i4 = a((String)localObject2, (String)localObject3, 0);
        int i5 = -1;
        if (i4 == i5) {
          break label236;
        }
        localObject3 = u;
        localObject2 = a((String)localObject2, (String)localObject3);
        localObject3 = localObject2[0];
        Object localObject4 = t;
        localObject3 = a(localObject3, localObject4)[i1];
        localObject2 = localObject2[i1];
        localObject2 = a(paramObject, localObject1, (String)localObject2);
        i.put(localObject3, localObject2);
        localObject4 = x;
        String str1 = j;
        Object localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        String str2 = "setting ";
        localObject3 = ((StringBuilder)localObject5).append(str2).append((String)localObject3);
        localObject5 = " to ";
        localObject3 = ((StringBuilder)localObject3).append((String)localObject5);
        localObject2 = localObject2;
        ((c)localObject4).a(str1, (String)localObject2);
      }
      for (;;)
      {
        i3 += 1;
        break;
        label236:
        localObject1 = a(paramObject, localObject1, (String)localObject2);
      }
    }
    return paramObject;
  }
  
  public String a()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = k;
    localStringBuilder = localStringBuilder.append(str).append(" - ");
    str = m;
    localStringBuilder = localStringBuilder.append(str).append("-");
    str = n;
    localStringBuilder = localStringBuilder.append(str).append(" - ");
    str = l;
    return str;
  }
  
  public void a(String paramString)
  {
    k = paramString;
  }
  
  public void a(String paramString1, Object paramObject, String paramString2)
  {
    g.6 local6 = new in/juspay/mystique/g$6;
    local6.<init>(this, paramString1, paramString2, paramObject);
    Object[] arrayOfObject = new Object[0];
    local6.execute(arrayOfObject);
  }
  
  public void a(String paramString, JSONObject paramJSONObject, Object paramObject)
  {
    localObject1 = null;
    int i1 = 11;
    int i2 = -1;
    int i3 = 1;
    int i4 = 0;
    localObject2 = null;
    Object localObject3 = "pattern";
    for (;;)
    {
      try
      {
        boolean bool1 = paramString.equals(localObject3);
        int i12;
        Object localObject8;
        int i14;
        Object localObject9;
        if (bool1)
        {
          localObject3 = paramObject.getClass();
          localObject5 = "setFilters";
          i12 = 1;
          localObject6 = new Class[i12];
          i13 = 0;
          localObject7 = null;
          localObject8 = InputFilter[].class;
          localObject6[0] = localObject8;
          localObject5 = ((Class)localObject3).getMethod((String)localObject5, (Class[])localObject6);
          localObject3 = "pattern";
          localObject3 = paramJSONObject.getString((String)localObject3);
          localObject6 = ",";
          localObject3 = ((String)localObject3).split((String)localObject6);
          i12 = 0;
          localObject6 = null;
          localObject6 = localObject3[0];
          i13 = localObject3.length;
          if (i13 == i3)
          {
            int i5 = 10000;
            localObject7 = new in/juspay/mystique/g$1;
            ((g.1)localObject7).<init>(this, (String)localObject6);
            i12 = 2;
            localObject6 = new InputFilter[i12];
            i14 = 0;
            localObject8 = null;
            localObject6[0] = localObject7;
            i13 = 1;
            localObject8 = new android/text/InputFilter$LengthFilter;
            ((InputFilter.LengthFilter)localObject8).<init>(i5);
            localObject6[i13] = localObject8;
            i5 = 1;
            localObject3 = new Object[i5];
            i13 = 0;
            localObject7 = null;
            localObject3[0] = localObject6;
            ((Method)localObject5).invoke(paramObject, (Object[])localObject3);
          }
        }
        else
        {
          localObject3 = "onKeyUp";
          boolean bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = "onKeyUp";
            localObject3 = paramJSONObject.getString((String)localObject3);
            localObject5 = paramObject.getClass();
            localObject6 = "setOnKeyListener";
            i13 = 1;
            localObject7 = new Class[i13];
            i14 = 0;
            localObject8 = null;
            localObject9 = View.OnKeyListener.class;
            localObject7[0] = localObject9;
            localObject5 = ((Class)localObject5).getMethod((String)localObject6, (Class[])localObject7);
            i12 = 1;
            localObject6 = new Object[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = new in/juspay/mystique/g$7;
            ((g.7)localObject8).<init>(this, (String)localObject3);
            localObject6[0] = localObject8;
            ((Method)localObject5).invoke(paramObject, (Object[])localObject6);
          }
          localObject3 = "onLongPress";
          bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = "onLongPress";
            localObject3 = paramJSONObject.getString((String)localObject3);
            localObject5 = paramObject.getClass();
            localObject6 = "setOnLongClickListener";
            i13 = 1;
            localObject7 = new Class[i13];
            i14 = 0;
            localObject8 = null;
            localObject9 = View.OnLongClickListener.class;
            localObject7[0] = localObject9;
            localObject5 = ((Class)localObject5).getMethod((String)localObject6, (Class[])localObject7);
            i12 = 1;
            localObject6 = new Object[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = new in/juspay/mystique/g$8;
            ((g.8)localObject8).<init>(this, (String)localObject3);
            localObject6[0] = localObject8;
            ((Method)localObject5).invoke(paramObject, (Object[])localObject6);
          }
          localObject3 = "onClick";
          bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = "onClick";
            localObject3 = paramJSONObject.getString((String)localObject3);
            localObject5 = paramObject.getClass();
            localObject6 = "setOnClickListener";
            i13 = 1;
            localObject7 = new Class[i13];
            i14 = 0;
            localObject8 = null;
            localObject9 = View.OnClickListener.class;
            localObject7[0] = localObject9;
            localObject5 = ((Class)localObject5).getMethod((String)localObject6, (Class[])localObject7);
            i12 = 1;
            localObject6 = new Object[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = new in/juspay/mystique/g$9;
            ((g.9)localObject8).<init>(this, (String)localObject3);
            localObject6[0] = localObject8;
            ((Method)localObject5).invoke(paramObject, (Object[])localObject6);
          }
          localObject3 = "onItemClick";
          bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = "onItemClick";
            localObject3 = paramJSONObject.getString((String)localObject3);
            localObject5 = paramObject.getClass();
            localObject6 = "setOnItemSelectedListener";
            i13 = 1;
            localObject7 = new Class[i13];
            i14 = 0;
            localObject8 = null;
            localObject9 = AdapterView.OnItemSelectedListener.class;
            localObject7[0] = localObject9;
            localObject5 = ((Class)localObject5).getMethod((String)localObject6, (Class[])localObject7);
            i12 = 1;
            localObject6 = new Object[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = new in/juspay/mystique/g$10;
            ((g.10)localObject8).<init>(this, (String)localObject3);
            localObject6[0] = localObject8;
            ((Method)localObject5).invoke(paramObject, (Object[])localObject6);
          }
          localObject3 = "onChange";
          bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = paramObject.getClass();
            localObject5 = "addTextChangedListener";
            i12 = 1;
            localObject6 = new Class[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = TextWatcher.class;
            localObject6[0] = localObject8;
            localObject3 = ((Class)localObject3).getMethod((String)localObject5, (Class[])localObject6);
            localObject5 = "onChange";
            localObject5 = paramJSONObject.getString((String)localObject5);
            i12 = 1;
            localObject6 = new Object[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = new in/juspay/mystique/g$11;
            ((g.11)localObject8).<init>(this, (String)localObject5);
            localObject6[0] = localObject8;
            ((Method)localObject3).invoke(paramObject, (Object[])localObject6);
          }
          localObject3 = "onFocus";
          bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = paramObject.getClass();
            localObject5 = "setOnFocusChangeListener";
            i12 = 1;
            localObject6 = new Class[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = View.OnFocusChangeListener.class;
            localObject6[0] = localObject8;
            localObject3 = ((Class)localObject3).getMethod((String)localObject5, (Class[])localObject6);
            localObject5 = "onFocus";
            localObject5 = paramJSONObject.getString((String)localObject5);
            i12 = 1;
            localObject6 = new Object[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = new in/juspay/mystique/g$12;
            ((g.12)localObject8).<init>(this, (String)localObject5);
            localObject6[0] = localObject8;
            ((Method)localObject3).invoke(paramObject, (Object[])localObject6);
          }
          localObject3 = "onTouch";
          bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = "onTouch";
            localObject3 = paramJSONObject.getString((String)localObject3);
            localObject5 = paramObject.getClass();
            localObject6 = "setOnTouchListener";
            i13 = 1;
            localObject7 = new Class[i13];
            i14 = 0;
            localObject8 = null;
            localObject9 = View.OnTouchListener.class;
            localObject7[0] = localObject9;
            localObject5 = ((Class)localObject5).getMethod((String)localObject6, (Class[])localObject7);
            i12 = 1;
            localObject6 = new Object[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = new in/juspay/mystique/g$13;
            ((g.13)localObject8).<init>(this, (String)localObject3);
            localObject6[0] = localObject8;
            ((Method)localObject5).invoke(paramObject, (Object[])localObject6);
          }
          localObject3 = "onDateChange";
          bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = "onDateChange";
            localObject3 = paramJSONObject.getString((String)localObject3);
            localObject5 = paramObject.getClass();
            localObject6 = "setOnDateChangeListener";
            i13 = 1;
            localObject7 = new Class[i13];
            i14 = 0;
            localObject8 = null;
            localObject9 = CalendarView.OnDateChangeListener.class;
            localObject7[0] = localObject9;
            localObject5 = ((Class)localObject5).getMethod((String)localObject6, (Class[])localObject7);
            i12 = Build.VERSION.SDK_INT;
            if (i12 >= i1)
            {
              i12 = 1;
              localObject6 = new Object[i12];
              i13 = 0;
              localObject7 = null;
              localObject8 = new in/juspay/mystique/g$14;
              ((g.14)localObject8).<init>(this, (String)localObject3);
              localObject6[0] = localObject8;
              ((Method)localObject5).invoke(paramObject, (Object[])localObject6);
            }
          }
          localObject3 = "onSwipe";
          bool2 = paramString.equals(localObject3);
          if (bool2)
          {
            localObject3 = "onSwipe";
            localObject3 = paramJSONObject.getString((String)localObject3);
            localObject5 = paramObject.getClass();
            localObject6 = "setOnTouchListener";
            i13 = 1;
            localObject7 = new Class[i13];
            i14 = 0;
            localObject8 = null;
            localObject9 = View.OnTouchListener.class;
            localObject7[0] = localObject9;
            localObject5 = ((Class)localObject5).getMethod((String)localObject6, (Class[])localObject7);
            i12 = 1;
            localObject6 = new Object[i12];
            i13 = 0;
            localObject7 = null;
            localObject8 = new in/juspay/mystique/g$2;
            ((g.2)localObject8).<init>(this, (String)localObject3);
            localObject6[0] = localObject8;
            ((Method)localObject5).invoke(paramObject, (Object[])localObject6);
          }
          localObject3 = "popupMenu";
          bool2 = paramString.equals(localObject3);
          if (!bool2) {
            continue;
          }
          i6 = Build.VERSION.SDK_INT;
          if (i6 < i1) {
            continue;
          }
          localObject3 = "popupMenu";
          localObject3 = paramJSONObject.getString((String)localObject3);
          localObject5 = r;
          localObject5 = ((Pattern)localObject5).toString();
          localObject5 = ((String)localObject3).split((String)localObject5);
          localObject3 = "onMenuItemClick";
          localObject6 = paramJSONObject.getString((String)localObject3);
          localObject7 = new android/widget/PopupMenu;
          localObject8 = f;
          localObject10 = paramObject;
          localObject10 = (View)paramObject;
          localObject3 = localObject10;
          ((PopupMenu)localObject7).<init>((Context)localObject8, (View)localObject10);
          g = ((PopupMenu)localObject7);
          i6 = 0;
          localObject3 = null;
          i13 = localObject5.length;
          if (i6 >= i13) {
            continue;
          }
          localObject7 = localObject5[i6];
          localObject8 = "\\";
          i13 = ((String)localObject7).indexOf((String)localObject8);
          if (i13 != i2)
          {
            localObject7 = localObject5[i6];
            localObject8 = ",";
            i13 = ((String)localObject7).indexOf((String)localObject8);
            if (i13 != i2)
            {
              localObject7 = localObject5[i6];
              localObject8 = "\\\\,";
              localObject9 = ",";
              localObject7 = ((String)localObject7).replace((CharSequence)localObject8, (CharSequence)localObject9);
              localObject5[i6] = localObject7;
            }
          }
          localObject7 = g;
          localObject7 = ((PopupMenu)localObject7).getMenu();
          i14 = 0;
          localObject8 = null;
          i3 = 0;
          localObject9 = null;
          CharSequence localCharSequence = localObject5[i6];
          ((Menu)localObject7).add(0, i6, 0, localCharSequence);
          i6 += 1;
          continue;
        }
        int i13 = 1;
        localObject3 = localObject3[i13];
        localObject3 = ((String)localObject3).trim();
        int i6 = Integer.parseInt((String)localObject3);
        continue;
        localObject3 = g;
        localObject5 = new in/juspay/mystique/g$3;
        ((g.3)localObject5).<init>(this, (String)localObject6);
        ((PopupMenu)localObject3).setOnMenuItemClickListener((PopupMenu.OnMenuItemClickListener)localObject5);
        localObject5 = g;
        localObject6 = j;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        Object localObject7 = "parseKeys: ";
        localObject7 = ((StringBuilder)localObject3).append((String)localObject7);
        localObject10 = paramObject;
        localObject10 = (View)paramObject;
        localObject3 = localObject10;
        localObject3 = ((StringBuilder)localObject7).append(localObject10);
        localObject3 = ((StringBuilder)localObject3).toString();
        Log.d((String)localObject6, (String)localObject3);
        localObject10 = paramObject;
        localObject10 = (View)paramObject;
        localObject3 = localObject10;
        localObject6 = new in/juspay/mystique/g$4;
        ((g.4)localObject6).<init>(this, (PopupMenu)localObject5);
        ((View)localObject10).setOnClickListener((View.OnClickListener)localObject6);
        localObject3 = "localImage";
        boolean bool3 = paramString.equals(localObject3);
        int i15;
        if (bool3)
        {
          localObject3 = "localImage";
          localObject3 = paramJSONObject.getString((String)localObject3);
          localObject5 = ",";
          localObject7 = ((String)localObject3).split((String)localObject5);
          i12 = 0;
          localObject6 = null;
          i15 = 0;
          localObject5 = null;
          int i7 = localObject7.length;
          if ((i12 < i7) && (localObject5 == null))
          {
            localObject3 = localObject7[i12];
            localObject8 = "assets";
            boolean bool4 = ((String)localObject3).startsWith((String)localObject8);
            if (bool4)
            {
              localObject3 = f;
              localObject5 = localObject7[i12];
              localObject8 = "/";
              localObject5 = ((String)localObject5).split((String)localObject8);
              i14 = 1;
              localObject5 = localObject5[i14];
              localObject3 = f.c((Context)localObject3, (String)localObject5);
              i15 = 0;
              localObject5 = null;
              i14 = localObject3.length;
              localObject3 = BitmapFactory.decodeByteArray((byte[])localObject3, 0, i14);
              i15 = i12 + 1;
              i12 = i15;
              localObject5 = localObject3;
              continue;
            }
            localObject3 = localObject7[i12];
            localObject8 = "drawable";
            bool4 = ((String)localObject3).startsWith((String)localObject8);
            if (bool4)
            {
              localObject3 = f;
              localObject3 = ((Activity)localObject3).getResources();
              localObject5 = localObject7[i12];
              localObject8 = "/";
              localObject5 = ((String)localObject5).split((String)localObject8);
              i14 = 1;
              localObject5 = localObject5[i14];
              localObject8 = "drawable";
              localObject9 = f;
              localObject9 = ((Activity)localObject9).getPackageName();
              int i8 = ((Resources)localObject3).getIdentifier((String)localObject5, (String)localObject8, (String)localObject9);
              localObject5 = f;
              localObject5 = ((Activity)localObject5).getResources();
              localObject3 = BitmapFactory.decodeResource((Resources)localObject5, i8);
              continue;
            }
            localObject3 = localObject7[i12];
            localObject8 = "internal";
            boolean bool5 = ((String)localObject3).startsWith((String)localObject8);
            if (bool5)
            {
              localObject3 = f;
              localObject5 = localObject7[i12];
              localObject8 = "/";
              localObject5 = ((String)localObject5).split((String)localObject8);
              i14 = 1;
              localObject5 = localObject5[i14];
              localObject3 = f.b((Context)localObject3, (String)localObject5);
              i15 = 0;
              localObject5 = null;
              i14 = localObject3.length;
              localObject3 = BitmapFactory.decodeByteArray((byte[])localObject3, 0, i14);
              continue;
            }
            localObject3 = localObject7[i12];
            localObject8 = "http";
            bool5 = ((String)localObject3).startsWith((String)localObject8);
            if (bool5)
            {
              localObject3 = localObject7[i12];
              localObject8 = "/";
              int i9 = ((String)localObject3).lastIndexOf((String)localObject8);
              localObject8 = localObject7[i12];
              i9 += 1;
              localObject3 = ((String)localObject8).substring(i9);
              localObject8 = localObject7[i12];
              a((String)localObject8, paramObject, (String)localObject3);
            }
            localObject3 = localObject5;
            continue;
          }
          localObject10 = paramObject;
          localObject10 = (ImageView)paramObject;
          localObject3 = localObject10;
          ((ImageView)localObject10).setImageBitmap((Bitmap)localObject5);
        }
        localObject3 = "localBackgoundImage";
        int i10 = paramString.equals(localObject3);
        if (i10 != 0)
        {
          localObject3 = "localImage";
          localObject3 = paramJSONObject.getString((String)localObject3);
          localObject5 = ",";
          localObject6 = ((String)localObject3).split((String)localObject5);
          i10 = 0;
          localObject3 = null;
          i15 = 0;
          localObject5 = null;
          i4 = localObject6.length;
          if ((i10 < i4) && (localObject5 == null))
          {
            localObject5 = localObject6[i10];
            localObject5 = BitmapFactory.decodeFile((String)localObject5);
            i10 += 1;
            continue;
          }
          int i11 = Build.VERSION.SDK_INT;
          i4 = 16;
          if (i11 >= i4)
          {
            localObject10 = paramObject;
            localObject10 = (View)paramObject;
            localObject3 = localObject10;
            localObject2 = new android/graphics/drawable/BitmapDrawable;
            localObject1 = f;
            localObject1 = ((Activity)localObject1).getResources();
            ((BitmapDrawable)localObject2).<init>((Resources)localObject1, (Bitmap)localObject5);
            ((View)localObject10).setBackground((Drawable)localObject2);
          }
        }
        else
        {
          localObject3 = "runInUI";
          boolean bool6 = paramString.equals(localObject3);
          if (bool6)
          {
            localObject3 = paramJSONObject.getString(paramString);
            a(paramObject, (String)localObject3);
          }
          localObject3 = "afterRender";
          bool6 = paramString.equals(localObject3);
          if (bool6)
          {
            localObject3 = "id";
            localObject3 = paramJSONObject.getString((String)localObject3);
            localObject5 = new java/lang/StringBuilder;
            ((StringBuilder)localObject5).<init>();
            localObject2 = "javascript:window.callUICallback('";
            localObject5 = ((StringBuilder)localObject5).append((String)localObject2);
            localObject2 = "afterRender";
            localObject2 = paramJSONObject.getString((String)localObject2);
            localObject5 = ((StringBuilder)localObject5).append((String)localObject2);
            localObject2 = "', '";
            localObject5 = ((StringBuilder)localObject5).append((String)localObject2);
            localObject3 = ((StringBuilder)localObject5).append((String)localObject3);
            localObject5 = "');";
            localObject3 = ((StringBuilder)localObject3).append((String)localObject5);
            localObject3 = ((StringBuilder)localObject3).toString();
            localObject5 = o;
            ((d)localObject5).a((String)localObject3);
          }
          return;
        }
      }
      catch (Exception localException)
      {
        Object localObject10;
        if (localException == null) {
          continue;
        }
        Object localObject4 = localException.getClass().getName();
        Object localObject5 = h;
        localObject2 = "WARNING";
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        Object localObject6 = " excep: fn__parseKeys  - ";
        localObject4 = ((StringBuilder)localObject1).append((String)localObject6).append((String)localObject4).append(" - ");
        localObject1 = a();
        localObject4 = (String)localObject1;
        ((e)localObject5).a((String)localObject2, (String)localObject4);
        continue;
      }
      localObject10 = paramObject;
      localObject10 = (View)paramObject;
      localObject3 = localObject10;
      localObject2 = new android/graphics/drawable/BitmapDrawable;
      localObject1 = f;
      localObject1 = ((Activity)localObject1).getResources();
      ((BitmapDrawable)localObject2).<init>((Resources)localObject1, (Bitmap)localObject5);
      ((View)localObject10).setBackgroundDrawable((Drawable)localObject2);
    }
  }
  
  public void b()
  {
    Activity localActivity = f;
    g.5 local5 = new in/juspay/mystique/g$5;
    local5.<init>(this);
    localActivity.runOnUiThread(local5);
  }
  
  public void b(String paramString)
  {
    m = paramString;
  }
  
  public void c(String paramString)
  {
    n = paramString;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */