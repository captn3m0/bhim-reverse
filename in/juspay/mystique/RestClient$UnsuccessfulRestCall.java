package in.juspay.mystique;

import java.net.URI;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;

public class RestClient$UnsuccessfulRestCall
  extends RuntimeException
{
  private final HttpResponse a;
  private final HttpRequestBase b;
  
  public RestClient$UnsuccessfulRestCall(HttpRequestBase paramHttpRequestBase, HttpResponse paramHttpResponse)
  {
    super((String)localObject);
    a = paramHttpResponse;
    b = paramHttpRequestBase;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/RestClient$UnsuccessfulRestCall.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */