package in.juspay.mystique;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class RestClient
{
  private static final String a = RestClient.class.getName();
  private static DefaultHttpClient b;
  private static DefaultHttpClient c;
  
  static
  {
    a();
  }
  
  private static DefaultHttpClient a(HttpParams paramHttpParams)
  {
    Object localObject1 = new org/apache/http/conn/scheme/SchemeRegistry;
    ((SchemeRegistry)localObject1).<init>();
    SSLSocketFactory localSSLSocketFactory = SSLSocketFactory.getSocketFactory();
    Object localObject2 = (X509HostnameVerifier)SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
    localSSLSocketFactory.setHostnameVerifier((X509HostnameVerifier)localObject2);
    localObject2 = new org/apache/http/conn/scheme/Scheme;
    ((Scheme)localObject2).<init>("https", localSSLSocketFactory, 443);
    ((SchemeRegistry)localObject1).register((Scheme)localObject2);
    localObject2 = new org/apache/http/conn/scheme/Scheme;
    PlainSocketFactory localPlainSocketFactory = PlainSocketFactory.getSocketFactory();
    ((Scheme)localObject2).<init>("http", localPlainSocketFactory, 80);
    ((SchemeRegistry)localObject1).register((Scheme)localObject2);
    localObject2 = new org/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    ((ThreadSafeClientConnManager)localObject2).<init>(paramHttpParams, (SchemeRegistry)localObject1);
    localObject1 = new org/apache/http/impl/client/DefaultHttpClient;
    ((DefaultHttpClient)localObject1).<init>((ClientConnectionManager)localObject2, paramHttpParams);
    return (DefaultHttpClient)localObject1;
  }
  
  public static void a()
  {
    int i = 10000;
    BasicHttpParams localBasicHttpParams = new org/apache/http/params/BasicHttpParams;
    localBasicHttpParams.<init>();
    HttpProtocolParams.setContentCharset(localBasicHttpParams, "UTF-8");
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, i);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, 30000);
    b = a(localBasicHttpParams);
    localBasicHttpParams = new org/apache/http/params/BasicHttpParams;
    localBasicHttpParams.<init>();
    HttpProtocolParams.setContentCharset(localBasicHttpParams, "UTF-8");
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 5000);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, i);
    c = a(localBasicHttpParams);
  }
  
  private static boolean a(HttpResponse paramHttpResponse)
  {
    StatusLine localStatusLine = paramHttpResponse.getStatusLine();
    int i = localStatusLine.getStatusCode();
    int k = 200;
    if (i >= k)
    {
      int m = 300;
      if (i < m) {
        i = 1;
      }
    }
    for (;;)
    {
      return i;
      int j = 0;
      localStatusLine = null;
    }
  }
  
  public static byte[] a(String paramString1, String paramString2, HashMap paramHashMap)
  {
    HttpPost localHttpPost = new org/apache/http/client/methods/HttpPost;
    localHttpPost.<init>(paramString1);
    if (paramHashMap != null)
    {
      localObject1 = paramHashMap.entrySet();
      Iterator localIterator = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (Map.Entry)localIterator.next();
        localObject2 = (String)((Map.Entry)localObject1).getKey();
        localObject1 = (String)((Map.Entry)localObject1).getValue();
        localHttpPost.setHeader((String)localObject2, (String)localObject1);
      }
    }
    Object localObject1 = paramString2.getBytes("UTF-8");
    Object localObject2 = new org/apache/http/entity/ByteArrayEntity;
    ((ByteArrayEntity)localObject2).<init>((byte[])localObject1);
    localHttpPost.setEntity((HttpEntity)localObject2);
    return a(localHttpPost);
  }
  
  public static byte[] a(String paramString, Map paramMap)
  {
    HttpGet localHttpGet = new org/apache/http/client/methods/HttpGet;
    localHttpGet.<init>();
    Object localObject1 = paramMap.entrySet();
    Iterator localIterator = ((Set)localObject1).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (Map.Entry)localIterator.next();
      localObject2 = (String)((Map.Entry)localObject1).getKey();
      localObject1 = (String)((Map.Entry)localObject1).getValue();
      localHttpGet.setHeader((String)localObject2, (String)localObject1);
    }
    localObject1 = URI.create(paramString);
    localHttpGet.setURI((URI)localObject1);
    localObject1 = b.execute(localHttpGet);
    Object localObject2 = ((HttpResponse)localObject1).getStatusLine();
    int j = ((StatusLine)localObject2).getStatusCode();
    int k = 200;
    if (j == k) {
      localObject1 = a(localHttpGet, (HttpResponse)localObject1);
    }
    for (;;)
    {
      return (byte[])localObject1;
      localObject1 = ((HttpResponse)localObject1).getStatusLine();
      int i = ((StatusLine)localObject1).getStatusCode();
      j = 304;
      if (i == j)
      {
        i = 0;
        localObject1 = null;
      }
      else
      {
        i = 0;
        localObject1 = null;
      }
    }
  }
  
  private static byte[] a(HttpRequestBase paramHttpRequestBase)
  {
    Object localObject1 = "Accept-Encoding";
    String str = "gzip";
    try
    {
      paramHttpRequestBase.setHeader((String)localObject1, str);
      localObject1 = b;
      localObject1 = ((DefaultHttpClient)localObject1).execute(paramHttpRequestBase);
      localObject1 = a(paramHttpRequestBase, (HttpResponse)localObject1);
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        Object localObject2 = null;
      }
    }
    return (byte[])localObject1;
  }
  
  /* Error */
  private static byte[] a(HttpRequestBase paramHttpRequestBase, HttpResponse paramHttpResponse)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 211	in/juspay/mystique/RestClient:a	(Lorg/apache/http/HttpResponse;)Z
    //   4: istore_2
    //   5: iload_2
    //   6: ifeq +177 -> 183
    //   9: aload_1
    //   10: invokeinterface 215 1 0
    //   15: astore_3
    //   16: aload_3
    //   17: invokeinterface 221 1 0
    //   22: astore_3
    //   23: aload_3
    //   24: ifnull +144 -> 168
    //   27: aload_3
    //   28: invokeinterface 225 1 0
    //   33: astore_3
    //   34: ldc -53
    //   36: astore 4
    //   38: aload_3
    //   39: aload 4
    //   41: invokevirtual 229	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   44: istore_2
    //   45: iload_2
    //   46: ifeq +122 -> 168
    //   49: new 231	java/util/zip/GZIPInputStream
    //   52: astore 4
    //   54: aload_1
    //   55: invokeinterface 215 1 0
    //   60: astore_3
    //   61: aload_3
    //   62: invokeinterface 235 1 0
    //   67: astore_3
    //   68: aload 4
    //   70: aload_3
    //   71: invokespecial 238	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   74: sipush 1024
    //   77: istore_2
    //   78: iload_2
    //   79: newarray <illegal type>
    //   81: astore_3
    //   82: new 241	java/io/ByteArrayOutputStream
    //   85: astore 5
    //   87: aload 5
    //   89: invokespecial 242	java/io/ByteArrayOutputStream:<init>	()V
    //   92: aload 4
    //   94: aload_3
    //   95: invokevirtual 246	java/util/zip/GZIPInputStream:read	([B)I
    //   98: istore 6
    //   100: iload 6
    //   102: iflt +48 -> 150
    //   105: aload 5
    //   107: aload_3
    //   108: iconst_0
    //   109: iload 6
    //   111: invokevirtual 250	java/io/ByteArrayOutputStream:write	([BII)V
    //   114: goto -22 -> 92
    //   117: astore_3
    //   118: aload 4
    //   120: ifnull +8 -> 128
    //   123: aload 4
    //   125: invokevirtual 253	java/util/zip/GZIPInputStream:close	()V
    //   128: aload_3
    //   129: athrow
    //   130: astore_3
    //   131: new 255	java/lang/RuntimeException
    //   134: astore 4
    //   136: aload_3
    //   137: invokevirtual 260	java/io/UnsupportedEncodingException:getMessage	()Ljava/lang/String;
    //   140: astore_3
    //   141: aload 4
    //   143: aload_3
    //   144: invokespecial 261	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
    //   147: aload 4
    //   149: athrow
    //   150: aload 5
    //   152: invokevirtual 265	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   155: astore_3
    //   156: aload 4
    //   158: ifnull +8 -> 166
    //   161: aload 4
    //   163: invokevirtual 253	java/util/zip/GZIPInputStream:close	()V
    //   166: aload_3
    //   167: areturn
    //   168: aload_1
    //   169: invokeinterface 215 1 0
    //   174: astore_3
    //   175: aload_3
    //   176: invokestatic 270	org/apache/http/util/EntityUtils:toByteArray	(Lorg/apache/http/HttpEntity;)[B
    //   179: astore_3
    //   180: goto -14 -> 166
    //   183: new 272	in/juspay/mystique/RestClient$UnsuccessfulRestCall
    //   186: astore_3
    //   187: aload_3
    //   188: aload_0
    //   189: aload_1
    //   190: invokespecial 275	in/juspay/mystique/RestClient$UnsuccessfulRestCall:<init>	(Lorg/apache/http/client/methods/HttpRequestBase;Lorg/apache/http/HttpResponse;)V
    //   193: aload_3
    //   194: athrow
    //   195: astore_3
    //   196: new 255	java/lang/RuntimeException
    //   199: astore 4
    //   201: aload_3
    //   202: invokevirtual 278	org/apache/http/client/ClientProtocolException:getMessage	()Ljava/lang/String;
    //   205: astore_3
    //   206: aload 4
    //   208: aload_3
    //   209: invokespecial 261	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
    //   212: aload 4
    //   214: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	215	0	paramHttpRequestBase	HttpRequestBase
    //   0	215	1	paramHttpResponse	HttpResponse
    //   4	42	2	bool	boolean
    //   77	2	2	i	int
    //   15	93	3	localObject1	Object
    //   117	12	3	localObject2	Object
    //   130	7	3	localUnsupportedEncodingException	java.io.UnsupportedEncodingException
    //   140	54	3	localObject3	Object
    //   195	7	3	localClientProtocolException	org.apache.http.client.ClientProtocolException
    //   205	4	3	str	String
    //   36	177	4	localObject4	Object
    //   85	66	5	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    //   98	12	6	j	int
    // Exception table:
    //   from	to	target	type
    //   94	98	117	finally
    //   109	114	117	finally
    //   150	155	117	finally
    //   0	4	130	java/io/UnsupportedEncodingException
    //   9	15	130	java/io/UnsupportedEncodingException
    //   16	22	130	java/io/UnsupportedEncodingException
    //   27	33	130	java/io/UnsupportedEncodingException
    //   39	44	130	java/io/UnsupportedEncodingException
    //   49	52	130	java/io/UnsupportedEncodingException
    //   54	60	130	java/io/UnsupportedEncodingException
    //   61	67	130	java/io/UnsupportedEncodingException
    //   70	74	130	java/io/UnsupportedEncodingException
    //   78	81	130	java/io/UnsupportedEncodingException
    //   82	85	130	java/io/UnsupportedEncodingException
    //   87	92	130	java/io/UnsupportedEncodingException
    //   123	128	130	java/io/UnsupportedEncodingException
    //   128	130	130	java/io/UnsupportedEncodingException
    //   161	166	130	java/io/UnsupportedEncodingException
    //   168	174	130	java/io/UnsupportedEncodingException
    //   175	179	130	java/io/UnsupportedEncodingException
    //   183	186	130	java/io/UnsupportedEncodingException
    //   189	193	130	java/io/UnsupportedEncodingException
    //   193	195	130	java/io/UnsupportedEncodingException
    //   0	4	195	org/apache/http/client/ClientProtocolException
    //   9	15	195	org/apache/http/client/ClientProtocolException
    //   16	22	195	org/apache/http/client/ClientProtocolException
    //   27	33	195	org/apache/http/client/ClientProtocolException
    //   39	44	195	org/apache/http/client/ClientProtocolException
    //   49	52	195	org/apache/http/client/ClientProtocolException
    //   54	60	195	org/apache/http/client/ClientProtocolException
    //   61	67	195	org/apache/http/client/ClientProtocolException
    //   70	74	195	org/apache/http/client/ClientProtocolException
    //   78	81	195	org/apache/http/client/ClientProtocolException
    //   82	85	195	org/apache/http/client/ClientProtocolException
    //   87	92	195	org/apache/http/client/ClientProtocolException
    //   123	128	195	org/apache/http/client/ClientProtocolException
    //   128	130	195	org/apache/http/client/ClientProtocolException
    //   161	166	195	org/apache/http/client/ClientProtocolException
    //   168	174	195	org/apache/http/client/ClientProtocolException
    //   175	179	195	org/apache/http/client/ClientProtocolException
    //   183	186	195	org/apache/http/client/ClientProtocolException
    //   189	193	195	org/apache/http/client/ClientProtocolException
    //   193	195	195	org/apache/http/client/ClientProtocolException
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/RestClient.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */