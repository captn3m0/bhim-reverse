package in.juspay.mystique;

import java.util.Arrays;

class g$a
{
  Class a;
  String b;
  Class[] c;
  
  public g$a(Class paramClass, String paramString, Class[] paramArrayOfClass)
  {
    a = paramClass;
    b = paramString;
    c = paramArrayOfClass;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    Class[] arrayOfClass = null;
    if (this == paramObject) {
      bool1 = true;
    }
    for (;;)
    {
      return bool1;
      if (paramObject != null)
      {
        Object localObject1 = getClass();
        Object localObject2 = paramObject.getClass();
        if (localObject1 == localObject2)
        {
          paramObject = (a)paramObject;
          localObject1 = a;
          localObject2 = a;
          boolean bool2 = localObject1.equals(localObject2);
          if (bool2)
          {
            localObject1 = b;
            localObject2 = b;
            bool2 = ((String)localObject1).equals(localObject2);
            if (bool2)
            {
              arrayOfClass = c;
              localObject1 = c;
              bool1 = Arrays.equals(arrayOfClass, (Object[])localObject1);
            }
          }
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = a.hashCode() * 31;
    String str = b;
    int j = str.hashCode();
    i += j;
    j = i * 31;
    Class[] arrayOfClass = c;
    if (arrayOfClass != null)
    {
      arrayOfClass = c;
      i = Arrays.hashCode(arrayOfClass);
    }
    for (;;)
    {
      return i + j;
      i = 0;
      arrayOfClass = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/g$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */