package in.juspay.mystique;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

public final class d
{
  private static c b;
  private WebView a;
  private Activity c;
  private e d;
  private h e;
  private FrameLayout f;
  
  public d(Activity paramActivity, FrameLayout paramFrameLayout, Bundle paramBundle, e parame)
  {
    Object localObject1 = new in/juspay/mystique/a;
    ((a)localObject1).<init>();
    b = (c)localObject1;
    d = parame;
    Object localObject2;
    String str;
    if ((paramActivity != null) && (paramFrameLayout != null))
    {
      c = paramActivity;
      f = paramFrameLayout;
      localObject1 = new android/webkit/WebView;
      localObject2 = paramActivity.getApplicationContext();
      ((WebView)localObject1).<init>((Context)localObject2);
      a = ((WebView)localObject1);
      f();
      a.setVisibility(8);
      localObject1 = f;
      localObject2 = a;
      ((FrameLayout)localObject1).addView((View)localObject2);
      localObject1 = a.getSettings();
      boolean bool = true;
      ((WebSettings)localObject1).setJavaScriptEnabled(bool);
      localObject1 = new in/juspay/mystique/h;
      ((h)localObject1).<init>(paramActivity, paramFrameLayout, this);
      e = ((h)localObject1);
      localObject1 = a;
      localObject2 = e;
      str = "Android";
      ((WebView)localObject1).addJavascriptInterface(localObject2, str);
    }
    for (;;)
    {
      return;
      localObject1 = b;
      localObject2 = "DynamicUI";
      str = "container or activity null";
      ((c)localObject1).b((String)localObject2, str);
    }
  }
  
  private String a(Object paramObject)
  {
    Object localObject1 = "";
    paramObject = (Exception)paramObject;
    StackTraceElement[] arrayOfStackTraceElement = ((Exception)paramObject).getStackTrace();
    int i = 0;
    for (;;)
    {
      int j = arrayOfStackTraceElement.length;
      if (i >= j) {
        break;
      }
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject1 = ((StringBuilder)localObject2).append((String)localObject1);
      localObject2 = arrayOfStackTraceElement[i].toString();
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2);
      localObject2 = "\n";
      localObject1 = (String)localObject2;
      i += 1;
    }
    return (String)localObject1;
  }
  
  public static c c()
  {
    return b;
  }
  
  private void f()
  {
    boolean bool1 = true;
    Object localObject1 = c;
    Object localObject2;
    int j;
    int k;
    if (localObject1 != null)
    {
      localObject1 = c.getPackageName();
      localObject2 = c.getResources();
      String str1 = "is_dui_debuggable";
      String str2 = "string";
      int i = ((Resources)localObject2).getIdentifier(str1, str2, (String)localObject1);
      if (i != 0)
      {
        localObject2 = c;
        localObject1 = ((Activity)localObject2).getString(i);
        if (localObject1 == null) {
          break label179;
        }
        localObject2 = "true";
        boolean bool2 = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (!bool2) {
          break label179;
        }
        j = Build.VERSION.SDK_INT;
        k = 19;
        if (j >= k) {
          WebView.setWebContentsDebuggingEnabled(bool1);
        }
        localObject1 = a;
        localObject2 = new android/webkit/WebChromeClient;
        ((WebChromeClient)localObject2).<init>();
        ((WebView)localObject1).setWebChromeClient((WebChromeClient)localObject2);
        localObject1 = new android/webkit/WebViewClient;
        ((WebViewClient)localObject1).<init>();
        localObject2 = a;
        ((WebView)localObject2).setWebViewClient((WebViewClient)localObject1);
      }
    }
    for (;;)
    {
      j = Build.VERSION.SDK_INT;
      k = 16;
      if (j >= k)
      {
        a.getSettings().setAllowFileAccessFromFileURLs(bool1);
        localObject1 = a.getSettings();
        ((WebSettings)localObject1).setAllowUniversalAccessFromFileURLs(bool1);
      }
      return;
      label179:
      localObject1 = a;
      localObject2 = new in/juspay/mystique/d$1;
      ((d.1)localObject2).<init>(this);
      ((WebView)localObject1).setWebChromeClient((WebChromeClient)localObject2);
    }
  }
  
  public void a()
  {
    a.loadDataWithBaseURL("http://juspay.in", "<html></html>", "text/html", "utf-8", null);
    FrameLayout localFrameLayout = f;
    WebView localWebView = a;
    localFrameLayout.removeView(localWebView);
    a.removeAllViews();
    a.destroy();
    c = null;
  }
  
  public void a(Object paramObject, String paramString)
  {
    a.addJavascriptInterface(paramObject, paramString);
  }
  
  public void a(String paramString)
  {
    Activity localActivity = c;
    if (localActivity != null)
    {
      localActivity = c;
      d.2 local2 = new in/juspay/mystique/d$2;
      local2.<init>(this, paramString);
      localActivity.runOnUiThread(local2);
    }
  }
  
  public h b()
  {
    return e;
  }
  
  public void b(String paramString)
  {
    Activity localActivity = c;
    if (localActivity != null)
    {
      localActivity = c;
      d.3 local3 = new in/juspay/mystique/d$3;
      local3.<init>(this, paramString);
      localActivity.runOnUiThread(local3);
    }
  }
  
  public void c(String paramString)
  {
    Object localObject = e;
    if (localObject != null)
    {
      e.setState(paramString);
      return;
    }
    localObject = new java/lang/Exception;
    ((Exception)localObject).<init>("JS-Interface not initailised");
    throw ((Throwable)localObject);
  }
  
  public e d()
  {
    return d;
  }
  
  public void d(String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = "window.onActivityLifeCycleEvent('" + paramString + "')";
    a((String)localObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/juspay/mystique/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */