package in.org.npci.commonlibrary;

import in.org.npci.commonlibrary.a.b;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.xml.security.Init;

public class e
{
  private static List a;
  private i b;
  private k c;
  private b d;
  private String e;
  
  public e(String paramString)
  {
    Object localObject1 = "";
    e = ((String)localObject1);
    Init.b();
    try
    {
      localObject1 = new in/org/npci/commonlibrary/a/b;
      ((b)localObject1).<init>();
      d = ((b)localObject1);
      localObject1 = d;
      boolean bool = ((b)localObject1).a(paramString);
      if (bool)
      {
        localObject1 = System.out;
        localObject2 = "XML Validated";
        ((PrintStream)localObject1).println((String)localObject2);
        localObject1 = new in/org/npci/commonlibrary/k;
        ((k)localObject1).<init>(paramString);
        c = ((k)localObject1);
        localObject1 = c;
        localObject1 = ((k)localObject1).a();
        a = (List)localObject1;
      }
    }
    catch (f localf1)
    {
      localf1.printStackTrace();
      throw localf1;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      f localf2 = new in/org/npci/commonlibrary/f;
      localObject2 = g.g;
      localf2.<init>((g)localObject2);
      throw localf2;
    }
    try
    {
      localObject1 = new in/org/npci/commonlibrary/i;
      ((i)localObject1).<init>();
      b = ((i)localObject1);
      return;
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      localNoSuchAlgorithmException.printStackTrace();
      f localf3 = new in/org/npci/commonlibrary/f;
      localObject2 = g.g;
      localf3.<init>((g)localObject2);
      throw localf3;
    }
    catch (NoSuchPaddingException localNoSuchPaddingException)
    {
      for (;;) {}
    }
    localObject1 = System.out;
    localObject2 = "XML Not Validated";
    ((PrintStream)localObject1).println((String)localObject2);
    localObject1 = new in/org/npci/commonlibrary/f;
    localObject2 = g.f;
    ((f)localObject1).<init>((g)localObject2);
    throw ((Throwable)localObject1);
  }
  
  private String a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Object localObject1 = new java/lang/StringBuilder;
    int i = 500;
    ((StringBuilder)localObject1).<init>(i);
    try
    {
      localObject2 = b;
      Object localObject3 = b;
      localObject3 = ((i)localObject3).a(paramString3);
      Object localObject4 = b;
      localObject4 = ((i)localObject4).b(paramString4);
      localObject2 = ((i)localObject2).a((byte[])localObject3, (byte[])localObject4);
      int j = 2;
      localObject2 = a.b((byte[])localObject2, j);
      localObject3 = ((StringBuilder)localObject1).append(paramString2);
      localObject4 = "|";
      localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
      localObject3 = ((StringBuilder)localObject3).append(paramString1);
      localObject4 = "|";
      localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
      ((StringBuilder)localObject3).append((String)localObject2);
      return ((StringBuilder)localObject1).toString();
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      localObject1 = new in/org/npci/commonlibrary/f;
      Object localObject2 = g.g;
      ((f)localObject1).<init>((g)localObject2);
      throw ((Throwable)localObject1);
    }
  }
  
  private byte[] a(String paramString)
  {
    byte[] arrayOfByte1 = paramString.getBytes();
    byte[] arrayOfByte2 = null;
    try
    {
      Object localObject1 = e;
      localObject1 = b((String)localObject1);
      Object localObject2 = "RSA/ECB/PKCS1Padding";
      localObject2 = Cipher.getInstance((String)localObject2);
      int i = 1;
      ((Cipher)localObject2).init(i, (Key)localObject1);
      arrayOfByte2 = ((Cipher)localObject2).doFinal(arrayOfByte1);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localException.printStackTrace();
      }
    }
    return arrayOfByte2;
  }
  
  private PublicKey b(String paramString)
  {
    byte[] arrayOfByte = a.a(paramString.getBytes("utf-8"), 2);
    X509EncodedKeySpec localX509EncodedKeySpec = new java/security/spec/X509EncodedKeySpec;
    localX509EncodedKeySpec.<init>(arrayOfByte);
    return KeyFactory.getInstance("RSA").generatePublic(localX509EncodedKeySpec);
  }
  
  public Message a(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    boolean bool1;
    if (paramString1 != null)
    {
      bool1 = paramString1.isEmpty();
      if (bool1)
      {
        localObject1 = new in/org/npci/commonlibrary/f;
        localObject2 = g.a;
        ((f)localObject1).<init>((g)localObject2);
        throw ((Throwable)localObject1);
      }
    }
    Object localObject2 = new java/util/ArrayList;
    ((ArrayList)localObject2).<init>();
    Object localObject1 = a;
    Object localObject3 = ((List)localObject1).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject3).hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (j)((Iterator)localObject3).next();
      String str1 = ((j)localObject1).b();
      boolean bool2 = str1.equals(paramString1);
      if (bool2) {
        ((List)localObject2).add(localObject1);
      }
    }
    int i = ((List)localObject2).size();
    if (i == 0)
    {
      localObject1 = new in/org/npci/commonlibrary/f;
      localObject2 = g.b;
      ((f)localObject1).<init>((g)localObject2);
      throw ((Throwable)localObject1);
    }
    localObject1 = new java/util/Random;
    ((Random)localObject1).<init>();
    int j = ((List)localObject2).size();
    i = ((Random)localObject1).nextInt(j);
    localObject1 = (j)((List)localObject2).get(i);
    localObject2 = ((j)localObject1).c();
    e = ((String)localObject2);
    localObject2 = a(paramString2, paramString3, paramString4, paramString5);
    localObject2 = a.b(a((String)localObject2), 2);
    localObject3 = new in/org/npci/commonlibrary/Message;
    Data localData = new in/org/npci/commonlibrary/Data;
    String str2 = ((j)localObject1).a();
    localObject1 = ((j)localObject1).b();
    localData.<init>(str2, (String)localObject1, (String)localObject2);
    ((Message)localObject3).<init>("", "", localData);
    return (Message)localObject3;
  }
  
  public void a(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      Object localObject1 = new in/org/npci/commonlibrary/i;
      ((i)localObject1).<init>();
      localObject2 = ((i)localObject1).a(paramString2);
      int i = 2;
      localObject2 = a.b((byte[])localObject2, i);
      i = 2;
      byte[] arrayOfByte1 = a.a(paramString1, i);
      byte[] arrayOfByte2 = ((i)localObject1).b(paramString3);
      localObject1 = ((i)localObject1).b(arrayOfByte1, arrayOfByte2);
      i = 2;
      localObject1 = a.b((byte[])localObject1, i);
      if (localObject1 != null)
      {
        boolean bool = ((String)localObject1).equalsIgnoreCase((String)localObject2);
        if (!bool)
        {
          localObject1 = new in/org/npci/commonlibrary/f;
          localObject2 = g.h;
          ((f)localObject1).<init>((g)localObject2);
          throw ((Throwable)localObject1);
        }
      }
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      localInvalidKeyException.printStackTrace();
      localf = new in/org/npci/commonlibrary/f;
      localObject2 = g.g;
      localf.<init>((g)localObject2);
      throw localf;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      f localf = new in/org/npci/commonlibrary/f;
      Object localObject2 = g.g;
      localf.<init>((g)localObject2);
      throw localf;
      return;
    }
    catch (BadPaddingException localBadPaddingException)
    {
      for (;;) {}
    }
    catch (InvalidAlgorithmParameterException localInvalidAlgorithmParameterException)
    {
      for (;;) {}
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      for (;;) {}
    }
    catch (NoSuchPaddingException localNoSuchPaddingException)
    {
      for (;;) {}
    }
    catch (IllegalBlockSizeException localIllegalBlockSizeException)
    {
      for (;;) {}
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;) {}
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/commonlibrary/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */