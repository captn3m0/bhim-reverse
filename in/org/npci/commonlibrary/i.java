package in.org.npci.commonlibrary;

import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class i
{
  Cipher a;
  byte[] b;
  byte[] c;
  
  public i()
  {
    Object localObject = Cipher.getInstance("AES/CBC/PKCS5Padding");
    a = ((Cipher)localObject);
    localObject = new byte[32];
    b = ((byte[])localObject);
    localObject = new byte[16];
    c = ((byte[])localObject);
  }
  
  public byte[] a(String paramString)
  {
    MessageDigest localMessageDigest = MessageDigest.getInstance("SHA-256");
    byte[] arrayOfByte = paramString.getBytes("UTF-8");
    localMessageDigest.update(arrayOfByte);
    return localMessageDigest.digest();
  }
  
  public byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    Object localObject = new byte[16];
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    localIvParameterSpec.<init>((byte[])localObject);
    localObject = Cipher.getInstance("AES/CBC/PKCS5Padding");
    ((Cipher)localObject).init(1, localSecretKeySpec, localIvParameterSpec);
    return ((Cipher)localObject).doFinal(paramArrayOfByte1);
  }
  
  public byte[] b(String paramString)
  {
    byte[] arrayOfByte = new byte[paramString.length() / 2];
    int i = 0;
    for (;;)
    {
      int j = arrayOfByte.length;
      if (i >= j) {
        break;
      }
      j = i * 2;
      int k = j + 2;
      String str = paramString.substring(j, k);
      k = 16;
      j = (byte)Integer.parseInt(str, k);
      arrayOfByte[i] = j;
      i += 1;
    }
    return arrayOfByte;
  }
  
  public byte[] b(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    Object localObject = new byte[16];
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    localIvParameterSpec.<init>((byte[])localObject);
    localObject = Cipher.getInstance("AES/CBC/PKCS5Padding");
    ((Cipher)localObject).init(2, localSecretKeySpec, localIvParameterSpec);
    return ((Cipher)localObject).doFinal(paramArrayOfByte1);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/commonlibrary/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */