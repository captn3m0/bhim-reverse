package in.org.npci.commonlibrary.a;

import java.io.StringReader;
import java.security.PublicKey;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.apache.xml.security.signature.XMLSignature;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class a
{
  public static Document a(String paramString)
  {
    Object localObject = DocumentBuilderFactory.newInstance();
    ((DocumentBuilderFactory)localObject).setNamespaceAware(true);
    localObject = ((DocumentBuilderFactory)localObject).newDocumentBuilder();
    InputSource localInputSource = new org/xml/sax/InputSource;
    StringReader localStringReader = new java/io/StringReader;
    localStringReader.<init>(paramString);
    localInputSource.<init>(localStringReader);
    return ((DocumentBuilder)localObject).parse(localInputSource);
  }
  
  public static boolean a(Document paramDocument, PublicKey paramPublicKey)
  {
    Object localObject1 = "Signature";
    Object localObject2 = paramDocument.getElementsByTagNameNS("http://www.w3.org/2000/09/xmldsig#", (String)localObject1);
    int i = ((NodeList)localObject2).getLength();
    if (i == 0)
    {
      localObject2 = new java/lang/Exception;
      ((Exception)localObject2).<init>("Signature not found");
      throw ((Throwable)localObject2);
    }
    localObject1 = new org/apache/xml/security/signature/XMLSignature;
    localObject2 = (Element)((NodeList)localObject2).item(0);
    ((XMLSignature)localObject1).<init>((Element)localObject2, "");
    return ((XMLSignature)localObject1).a(paramPublicKey);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/commonlibrary/a/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */