package in.org.npci.commonlibrary.a;

import java.io.PrintStream;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class b
{
  private Certificate a;
  
  public b()
  {
    Object localObject = "signer.crt";
    try
    {
      localObject = b((String)localObject);
      a = ((Certificate)localObject);
      return;
    }
    catch (CertificateException localCertificateException)
    {
      for (;;)
      {
        PrintStream localPrintStream = System.out;
        String str = "Error in loading exception";
        localPrintStream.println(str);
        localCertificateException.printStackTrace();
      }
    }
  }
  
  /* Error */
  private Certificate b(String paramString)
  {
    // Byte code:
    //   0: ldc 39
    //   2: invokestatic 45	java/security/cert/CertificateFactory:getInstance	(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;
    //   5: astore_2
    //   6: aload_0
    //   7: invokevirtual 49	java/lang/Object:getClass	()Ljava/lang/Class;
    //   10: invokevirtual 55	java/lang/Class:getClassLoader	()Ljava/lang/ClassLoader;
    //   13: aload_1
    //   14: invokevirtual 61	java/lang/ClassLoader:getResourceAsStream	(Ljava/lang/String;)Ljava/io/InputStream;
    //   17: astore_3
    //   18: new 63	java/io/BufferedInputStream
    //   21: astore 4
    //   23: aload 4
    //   25: aload_3
    //   26: invokespecial 66	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   29: aload_2
    //   30: aload 4
    //   32: invokevirtual 70	java/security/cert/CertificateFactory:generateCertificate	(Ljava/io/InputStream;)Ljava/security/cert/Certificate;
    //   35: astore_2
    //   36: aload 4
    //   38: invokevirtual 75	java/io/InputStream:close	()V
    //   41: aload_3
    //   42: invokevirtual 75	java/io/InputStream:close	()V
    //   45: aload_2
    //   46: areturn
    //   47: astore_2
    //   48: aload 4
    //   50: invokevirtual 75	java/io/InputStream:close	()V
    //   53: aload_3
    //   54: invokevirtual 75	java/io/InputStream:close	()V
    //   57: aload_2
    //   58: athrow
    //   59: astore 4
    //   61: goto -20 -> 41
    //   64: astore_3
    //   65: goto -20 -> 45
    //   68: astore 4
    //   70: goto -17 -> 53
    //   73: astore_3
    //   74: goto -17 -> 57
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	77	0	this	b
    //   0	77	1	paramString	String
    //   5	41	2	localObject1	Object
    //   47	11	2	localObject2	Object
    //   17	37	3	localInputStream	java.io.InputStream
    //   64	1	3	localIOException1	java.io.IOException
    //   73	1	3	localIOException2	java.io.IOException
    //   21	28	4	localBufferedInputStream	java.io.BufferedInputStream
    //   59	1	4	localIOException3	java.io.IOException
    //   68	1	4	localIOException4	java.io.IOException
    // Exception table:
    //   from	to	target	type
    //   30	35	47	finally
    //   36	41	59	java/io/IOException
    //   41	45	64	java/io/IOException
    //   48	53	68	java/io/IOException
    //   53	57	73	java/io/IOException
  }
  
  public boolean a(String paramString)
  {
    boolean bool1 = false;
    Object localObject1 = a;
    if (localObject1 == null) {
      localObject1 = "signer.crt";
    }
    try
    {
      localObject1 = b((String)localObject1);
      a = ((Certificate)localObject1);
    }
    catch (CertificateException localCertificateException)
    {
      for (;;)
      {
        label51:
        localObject3 = System.out;
        localObject4 = "Error in loading certificate.";
        ((PrintStream)localObject3).println((String)localObject4);
        localCertificateException.printStackTrace();
      }
    }
    try
    {
      localObject1 = a.a(paramString);
      localObject3 = a;
      localObject3 = ((Certificate)localObject3).getPublicKey();
      bool2 = a.a((Document)localObject1, (PublicKey)localObject3);
    }
    catch (ParserConfigurationException localParserConfigurationException)
    {
      localObject3 = System.err;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      String str = "Parsing failed for message:";
      localObject4 = str + paramString;
      ((PrintStream)localObject3).println((String)localObject4);
      localParserConfigurationException.printStackTrace();
      bool2 = false;
      localObject2 = null;
      break label51;
    }
    catch (Exception localException)
    {
      localException.printStackTrace();
      bool2 = false;
      Object localObject2 = null;
      break label51;
    }
    catch (SAXException localSAXException)
    {
      for (;;) {}
    }
    bool1 = bool2;
    return bool1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/commonlibrary/a/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */