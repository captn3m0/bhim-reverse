package in.org.npci.commonlibrary;

import java.io.UnsupportedEncodingException;

public class a
{
  static
  {
    Class localClass = a.class;
    boolean bool = localClass.desiredAssertionStatus();
    if (!bool) {
      bool = true;
    }
    for (;;)
    {
      a = bool;
      return;
      bool = false;
      localClass = null;
    }
  }
  
  public static byte[] a(String paramString, int paramInt)
  {
    return a(paramString.getBytes(), paramInt);
  }
  
  public static byte[] a(byte[] paramArrayOfByte, int paramInt)
  {
    int i = paramArrayOfByte.length;
    return a(paramArrayOfByte, 0, i, paramInt);
  }
  
  public static byte[] a(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    c localc = new in/org/npci/commonlibrary/c;
    Object localObject = new byte[paramInt2 * 3 / 4];
    localc.<init>(paramInt3, (byte[])localObject);
    boolean bool = localc.a(paramArrayOfByte, paramInt1, paramInt2, true);
    if (!bool)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("bad base-64");
      throw ((Throwable)localObject);
    }
    int i = b;
    byte[] arrayOfByte = a;
    int j = arrayOfByte.length;
    if (i == j) {
      localObject = a;
    }
    for (;;)
    {
      return (byte[])localObject;
      i = b;
      localObject = new byte[i];
      arrayOfByte = a;
      int k = b;
      System.arraycopy(arrayOfByte, 0, localObject, 0, k);
    }
  }
  
  public static String b(byte[] paramArrayOfByte, int paramInt)
  {
    try
    {
      String str1 = new java/lang/String;
      localObject = c(paramArrayOfByte, paramInt);
      String str2 = "US-ASCII";
      str1.<init>((byte[])localObject, str2);
      return str1;
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      Object localObject = new java/lang/AssertionError;
      ((AssertionError)localObject).<init>(localUnsupportedEncodingException);
      throw ((Throwable)localObject);
    }
  }
  
  public static byte[] b(byte[] paramArrayOfByte, int paramInt1, int paramInt2, int paramInt3)
  {
    int i = 1;
    d locald = new in/org/npci/commonlibrary/d;
    AssertionError localAssertionError = null;
    locald.<init>(paramInt3, null);
    int j = paramInt2 / 3 * 4;
    boolean bool1 = d;
    int i1;
    if (bool1)
    {
      int k = paramInt2 % 3;
      if (k > 0) {
        j += 4;
      }
      boolean bool2 = e;
      if ((bool2) && (paramInt2 > 0))
      {
        i1 = (paramInt2 + -1) / 57 + 1;
        bool2 = f;
        if (!bool2) {
          break label225;
        }
      }
    }
    int n;
    label225:
    for (int m = 2;; n = i)
    {
      m *= i1;
      j += m;
      byte[] arrayOfByte = new byte[j];
      a = arrayOfByte;
      locald.a(paramArrayOfByte, paramInt1, paramInt2, i);
      boolean bool3 = a;
      if (bool3) {
        break label232;
      }
      n = b;
      if (n == j) {
        break label232;
      }
      localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>();
      throw localAssertionError;
      n = paramInt2 % 3;
      switch (n)
      {
      case 0: 
      default: 
        break;
      case 1: 
        j += 2;
        break;
      case 2: 
        j += 3;
        break;
      }
    }
    label232:
    return a;
  }
  
  public static byte[] c(byte[] paramArrayOfByte, int paramInt)
  {
    int i = paramArrayOfByte.length;
    return b(paramArrayOfByte, 0, i, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/commonlibrary/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */