package in.org.npci.commonlibrary;

public enum g
{
  private final int i;
  private final String j;
  
  static
  {
    int m = 4;
    int n = 3;
    int i1 = 2;
    int i2 = 1;
    Object localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("KEY_CODE_EMPTY", 0, 1001, "Your organization key is empty. Please provide a organization key.");
    a = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("KEY_CODE_INVALID", i2, 1002, "Your organization key is invalid. Please contact your system administrator or UPI support team.");
    b = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("PUBLICKEY_NOT_FOUND", i1, 1003, "Public key file not found please contact your system administrator UPI support team");
    c = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("PARSER_MISCONFIG", n, 1004, "XML Parser configuration error.Keys.xml may not be formatted correctly.");
    d = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("XML_PATH_ERROR", m, 1005, "XML File is not found or cannot be read.");
    e = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("KEYS_NOT_VALID", 5, 1006, "Keys are not valid. Please contact your system administrator UPI support team");
    f = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("UNKNOWN_ERROR", 6, 1007, "Unknown error occurred.");
    g = (g)localObject;
    localObject = new in/org/npci/commonlibrary/g;
    ((g)localObject).<init>("TRUST_NOT_VALID", 7, 1008, "Trust is not valid");
    h = (g)localObject;
    localObject = new g[8];
    g localg1 = a;
    localObject[0] = localg1;
    localg1 = b;
    localObject[i2] = localg1;
    localg1 = c;
    localObject[i1] = localg1;
    localg1 = d;
    localObject[n] = localg1;
    localg1 = e;
    localObject[m] = localg1;
    g localg2 = f;
    localObject[5] = localg2;
    localg2 = g;
    localObject[6] = localg2;
    localg2 = h;
    localObject[7] = localg2;
    k = (g[])localObject;
  }
  
  private g(int paramInt1, String paramString1)
  {
    i = paramInt1;
    j = paramString1;
  }
  
  public String a()
  {
    return j;
  }
  
  public int b()
  {
    return i;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int m = i;
    localStringBuilder = localStringBuilder.append(m).append(": ");
    String str = j;
    return str;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/commonlibrary/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */