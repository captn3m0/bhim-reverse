package in.org.npci.commonlibrary;

import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

class k
  extends DefaultHandler
{
  private static List a;
  private static j b = null;
  private static String c = null;
  
  static
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    a = localArrayList;
  }
  
  public k() {}
  
  public k(String paramString)
  {
    Object localObject1 = SAXParserFactory.newInstance();
    try
    {
      localObject1 = ((SAXParserFactory)localObject1).newSAXParser();
      localObject2 = new org/xml/sax/InputSource;
      StringReader localStringReader = new java/io/StringReader;
      localStringReader.<init>(paramString);
      ((InputSource)localObject2).<init>(localStringReader);
      ((SAXParser)localObject1).parse((InputSource)localObject2, this);
      return;
    }
    catch (ParserConfigurationException localParserConfigurationException)
    {
      f localf = new in/org/npci/commonlibrary/f;
      Object localObject2 = g.d;
      localf.<init>((g)localObject2);
      throw localf;
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    catch (SAXException localSAXException)
    {
      for (;;) {}
    }
  }
  
  public List a()
  {
    return a;
  }
  
  public void characters(char[] paramArrayOfChar, int paramInt1, int paramInt2)
  {
    c = String.copyValueOf(paramArrayOfChar, paramInt1, paramInt2).trim();
  }
  
  public void endElement(String paramString1, String paramString2, String paramString3)
  {
    int i = -1;
    int j = paramString3.hashCode();
    switch (j)
    {
    default: 
      switch (i)
      {
      }
      break;
    }
    for (;;)
    {
      return;
      Object localObject1 = "key";
      boolean bool = paramString3.equals(localObject1);
      if (!bool) {
        break;
      }
      i = 0;
      Object localObject2 = null;
      break;
      localObject1 = "keyValue";
      bool = paramString3.equals(localObject1);
      if (!bool) {
        break;
      }
      i = 1;
      break;
      localObject2 = a;
      localObject1 = b;
      ((List)localObject2).add(localObject1);
      continue;
      localObject2 = b;
      localObject1 = c;
      ((j)localObject2).c((String)localObject1);
    }
  }
  
  protected void finalize()
  {
    System.out.println("KeyParser Destroyed");
  }
  
  public void startElement(String paramString1, String paramString2, String paramString3, Attributes paramAttributes)
  {
    int i = -1;
    int j = paramString3.hashCode();
    switch (j)
    {
    default: 
      switch (i)
      {
      }
      break;
    }
    for (;;)
    {
      return;
      String str = "key";
      boolean bool = paramString3.equals(str);
      if (!bool) {
        break;
      }
      i = 0;
      j localj = null;
      break;
      localj = new in/org/npci/commonlibrary/j;
      localj.<init>();
      b = localj;
      localj = b;
      str = paramAttributes.getValue("ki");
      localj.a(str);
      localj = b;
      str = paramAttributes.getValue("owner");
      localj.b(str);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/commonlibrary/k.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */