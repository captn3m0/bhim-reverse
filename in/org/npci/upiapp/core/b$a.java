package in.org.npci.upiapp.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import in.org.npci.upiapp.HomeActivity;
import in.org.npci.upiapp.a.a;
import java.util.Date;
import java.util.Map;

class b$a
  extends BroadcastReceiver
{
  private b$a(b paramb) {}
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    try
    {
      Object localObject1 = paramIntent.getAction();
      Object localObject2 = "android.provider.Telephony.SMS_RECEIVED";
      boolean bool = ((String)localObject1).equals(localObject2);
      String str1;
      if (bool)
      {
        localObject1 = paramIntent.getExtras();
        if (localObject1 != null)
        {
          localObject2 = "pdus";
          localObject1 = ((Bundle)localObject1).get((String)localObject2);
          localObject1 = (Object[])localObject1;
          Object localObject3 = localObject1;
          localObject3 = (Object[])localObject1;
          Object localObject4 = localObject3;
          int i = localObject3.length;
          SmsMessage[] arrayOfSmsMessage = new SmsMessage[i];
          i = 0;
          localObject1 = null;
          for (int j = 0;; j = i)
          {
            i = arrayOfSmsMessage.length;
            if (j >= i) {
              break;
            }
            localObject1 = localObject4[j];
            localObject1 = (byte[])localObject1;
            localObject1 = (byte[])localObject1;
            localObject1 = SmsMessage.createFromPdu((byte[])localObject1);
            arrayOfSmsMessage[j] = localObject1;
            localObject1 = arrayOfSmsMessage[j];
            localObject1 = ((SmsMessage)localObject1).getOriginatingAddress();
            localObject2 = ((String)localObject1).toUpperCase();
            localObject1 = arrayOfSmsMessage[j];
            localObject1 = ((SmsMessage)localObject1).getMessageBody();
            str1 = ((String)localObject1).toUpperCase();
            Date localDate = new java/util/Date;
            localObject1 = arrayOfSmsMessage[j];
            long l = ((SmsMessage)localObject1).getTimestampMillis();
            localDate.<init>(l);
            localObject1 = d.b();
            String str2 = HomeActivity.s();
            Object localObject5 = a;
            localObject5 = ((b)localObject5).a();
            localObject1 = ((d)localObject1).a((String)localObject2, str1, localDate, str2, (Map)localObject5);
            if (localObject1 == null)
            {
              localObject1 = d.b();
              str2 = "BHIM";
              localObject5 = a;
              localObject5 = ((b)localObject5).a();
              localObject1 = ((d)localObject1).a((String)localObject2, str1, localDate, str2, (Map)localObject5);
            }
            if (localObject1 != null)
            {
              localObject2 = a;
              ((b)localObject2).a((c)localObject1);
            }
            i = j + 1;
          }
        }
      }
      return;
    }
    catch (Exception localException)
    {
      localObject2 = "OtpReader";
      str1 = "Exception in onOtpReceived";
      a.a((String)localObject2, str1, localException);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/b$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */