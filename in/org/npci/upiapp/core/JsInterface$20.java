package in.org.npci.upiapp.core;

import android.os.AsyncTask;
import android.util.Base64;
import in.juspay.mystique.d;
import in.org.npci.upiapp.a.a;
import in.org.npci.upiapp.models.ApiResponse;
import in.org.npci.upiapp.utils.RestClient;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

class JsInterface$20
  extends AsyncTask
{
  JsInterface$20(JsInterface paramJsInterface, String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, boolean paramBoolean) {}
  
  protected ApiResponse a(Object[] paramArrayOfObject)
  {
    Object localObject1 = JsInterface.a();
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = ((StringBuilder)localObject3).append("Now calling API :");
    Object localObject4 = b;
    localObject3 = (String)localObject4;
    a.a((String)localObject1, (String)localObject3);
    localObject1 = JsInterface.a();
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = ((StringBuilder)localObject3).append("API Request parameter : URL ->");
    localObject4 = b;
    localObject3 = ((StringBuilder)localObject3).append((String)localObject4).append("- Method -> ");
    localObject4 = c;
    localObject3 = (String)localObject4;
    a.a((String)localObject1, (String)localObject3);
    localObject1 = JsInterface.a();
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = ((StringBuilder)localObject3).append("API Request parameter : Data ->");
    localObject4 = d;
    localObject3 = ((StringBuilder)localObject3).append((String)localObject4).append("- Header -> ");
    localObject4 = e;
    localObject3 = (String)localObject4;
    a.a((String)localObject1, (String)localObject3);
    localObject3 = new java/util/HashMap;
    ((HashMap)localObject3).<init>();
    Object localObject5;
    boolean bool1;
    Object localObject2;
    try
    {
      localObject4 = new org/json/JSONObject;
      localObject1 = e;
      ((JSONObject)localObject4).<init>((String)localObject1);
      localObject5 = ((JSONObject)localObject4).keys();
      for (;;)
      {
        bool1 = ((Iterator)localObject5).hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = ((Iterator)localObject5).next();
        localObject1 = (String)localObject1;
        String str = ((JSONObject)localObject4).getString((String)localObject1);
        ((HashMap)localObject3).put(localObject1, str);
      }
      int i;
      return (ApiResponse)localObject2;
    }
    catch (Exception localException)
    {
      localObject3 = localException;
      a.a(localException);
      localObject2 = new in/org/npci/upiapp/models/ApiResponse;
      ((ApiResponse)localObject2).<init>();
      i = -1;
      ((ApiResponse)localObject2).setStatusCode(i);
      localObject3 = ((Exception)localObject3).getLocalizedMessage().getBytes();
      ((ApiResponse)localObject2).setData((byte[])localObject3);
    }
    for (;;)
    {
      localObject2 = JsInterface.a();
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject5 = "API Request Header : Header -> ";
      localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
      localObject4 = ((StringBuilder)localObject4).append(localObject3);
      localObject4 = ((StringBuilder)localObject4).toString();
      a.a((String)localObject2, (String)localObject4);
      localObject2 = c;
      localObject4 = "POST";
      bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject4);
      boolean bool2;
      if (bool1)
      {
        localObject2 = JsInterface.a();
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "METHOD - ";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject5 = c;
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject4 = ((StringBuilder)localObject4).toString();
        a.a((String)localObject2, (String)localObject4);
        localObject2 = JsInterface.a();
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "URL - ";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject5 = b;
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject4 = ((StringBuilder)localObject4).toString();
        a.a((String)localObject2, (String)localObject4);
        localObject2 = JsInterface.a();
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "REQUEST-BODY - ";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject5 = d;
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject4 = ((StringBuilder)localObject4).toString();
        a.a((String)localObject2, (String)localObject4);
        localObject2 = JsInterface.a();
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "HEADER - ";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject4 = ((StringBuilder)localObject4).append(localObject3);
        localObject4 = ((StringBuilder)localObject4).toString();
        a.a((String)localObject2, (String)localObject4);
        localObject2 = b;
        localObject4 = d;
        bool2 = f;
        localObject2 = RestClient.a((String)localObject2, (String)localObject4, (HashMap)localObject3, bool2);
      }
      else
      {
        localObject2 = c;
        localObject4 = "GET";
        bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject4);
        if (bool1)
        {
          localObject2 = JsInterface.a();
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          localObject5 = "METHOD:  - ";
          localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
          localObject5 = c;
          localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
          localObject4 = ((StringBuilder)localObject4).toString();
          a.a((String)localObject2, (String)localObject4);
          localObject2 = JsInterface.a();
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          localObject5 = "URL - ";
          localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
          localObject5 = b;
          localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
          localObject4 = ((StringBuilder)localObject4).toString();
          a.a((String)localObject2, (String)localObject4);
          localObject2 = b;
          localObject4 = new java/util/HashMap;
          ((HashMap)localObject4).<init>();
          bool2 = f;
          localObject2 = RestClient.a((String)localObject2, (Map)localObject4, (HashMap)localObject3, bool2);
        }
        else
        {
          localObject2 = c;
          localObject4 = "DELETE";
          bool1 = ((String)localObject2).equalsIgnoreCase((String)localObject4);
          if (bool1)
          {
            localObject2 = JsInterface.a();
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>();
            localObject5 = "METHOD:  - ";
            localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
            localObject5 = c;
            localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
            localObject4 = ((StringBuilder)localObject4).toString();
            a.a((String)localObject2, (String)localObject4);
            localObject2 = JsInterface.a();
            localObject4 = new java/lang/StringBuilder;
            ((StringBuilder)localObject4).<init>();
            localObject5 = "URL - ";
            localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
            localObject5 = b;
            localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
            localObject4 = ((StringBuilder)localObject4).toString();
            a.a((String)localObject2, (String)localObject4);
            localObject2 = b;
            localObject4 = new java/util/HashMap;
            ((HashMap)localObject4).<init>();
            bool2 = f;
            localObject2 = RestClient.b((String)localObject2, (Map)localObject4, (HashMap)localObject3, bool2);
          }
          else
          {
            bool1 = false;
            localObject2 = null;
          }
        }
      }
    }
  }
  
  protected void onPostExecute(Object paramObject)
  {
    int i = 2;
    int j;
    int k;
    if (paramObject != null)
    {
      paramObject = (ApiResponse)paramObject;
      str1 = "";
      j = ((ApiResponse)paramObject).getStatusCode();
      k = 200;
      if (j < k) {
        break label242;
      }
      j = ((ApiResponse)paramObject).getStatusCode();
      k = 304;
      if (j > k) {
        break label242;
      }
    }
    label242:
    for (String str1 = "success";; str1 = "failure")
    {
      Object localObject1 = JsInterface.a();
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("Response Success: ");
      Object localObject3 = a;
      localObject2 = (String)localObject3;
      a.a((String)localObject1, (String)localObject2);
      localObject1 = ((ApiResponse)paramObject).getData();
      if (localObject1 == null) {
        localObject1 = "{}".getBytes();
      }
      localObject1 = Base64.encodeToString((byte[])localObject1, i);
      localObject2 = "window.callJSCallback('%s','%s','%s','%s','%s');";
      int m = 5;
      localObject3 = new Object[m];
      String str2 = a;
      localObject3[0] = str2;
      int n = 1;
      localObject3[n] = str1;
      localObject3[i] = localObject1;
      k = ((ApiResponse)paramObject).getStatusCode();
      localObject1 = Integer.valueOf(k);
      localObject3[3] = localObject1;
      j = 4;
      localObject1 = Base64.encodeToString(b.getBytes(), i);
      localObject3[j] = localObject1;
      str1 = String.format((String)localObject2, (Object[])localObject3);
      a.a(JsInterface.a(), str1);
      localObject1 = JsInterface.c(g);
      ((d)localObject1).a(str1);
      return;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/JsInterface$20.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */