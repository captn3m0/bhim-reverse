package in.org.npci.upiapp.core;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.util.Property;
import android.view.View;

class JsInterface$9
  implements Runnable
{
  JsInterface$9(JsInterface paramJsInterface, String paramString, int paramInt1, int paramInt2) {}
  
  public void run()
  {
    int i = 1;
    Object localObject1 = JsInterface.b(d);
    int j = Integer.parseInt(a);
    localObject1 = ((Activity)localObject1).findViewById(j);
    Object localObject2 = d;
    float f1 = b;
    j = ((JsInterface)localObject2).a(f1);
    Object localObject3 = View.TRANSLATION_X;
    Keyframe[] arrayOfKeyframe = new Keyframe[8];
    Keyframe localKeyframe1 = Keyframe.ofFloat(0.0F, 0.0F);
    arrayOfKeyframe[0] = localKeyframe1;
    float f2 = -j;
    localKeyframe1 = Keyframe.ofFloat(0.1F, f2);
    arrayOfKeyframe[i] = localKeyframe1;
    float f3 = j;
    Keyframe localKeyframe2 = Keyframe.ofFloat(0.26F, f3);
    arrayOfKeyframe[2] = localKeyframe2;
    f3 = -j;
    localKeyframe2 = Keyframe.ofFloat(0.42F, f3);
    arrayOfKeyframe[3] = localKeyframe2;
    f3 = j;
    localKeyframe2 = Keyframe.ofFloat(0.58F, f3);
    arrayOfKeyframe[4] = localKeyframe2;
    f3 = -j;
    localKeyframe2 = Keyframe.ofFloat(0.74F, f3);
    arrayOfKeyframe[5] = localKeyframe2;
    float f4 = j;
    localObject2 = Keyframe.ofFloat(0.9F, f4);
    arrayOfKeyframe[6] = localObject2;
    localKeyframe1 = Keyframe.ofFloat(1.0F, 0.0F);
    arrayOfKeyframe[7] = localKeyframe1;
    localObject2 = PropertyValuesHolder.ofKeyframe((Property)localObject3, arrayOfKeyframe);
    localObject3 = new PropertyValuesHolder[i];
    localObject3[0] = localObject2;
    localObject1 = ObjectAnimator.ofPropertyValuesHolder(localObject1, (PropertyValuesHolder[])localObject3);
    long l = c;
    ((ObjectAnimator)localObject1).setDuration(l).start();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/JsInterface$9.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */