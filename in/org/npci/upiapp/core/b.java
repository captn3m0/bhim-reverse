package in.org.npci.upiapp.core;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.IntentFilter;
import android.content.res.Resources;
import in.org.npci.upiapp.HomeActivity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONObject;

public class b
{
  private static boolean a = true;
  private static b b;
  private static Map c;
  private Long d;
  private Timer e;
  private c f;
  private c g;
  private String h;
  private a i;
  private Activity j;
  private b.a k;
  
  public b()
  {
    Long localLong = Long.valueOf(0L);
    d = localLong;
    g = null;
    b = this;
  }
  
  private static Object a(Object paramObject)
  {
    Object localObject = JSONObject.NULL;
    if (paramObject == localObject) {
      paramObject = null;
    }
    for (;;)
    {
      return paramObject;
      boolean bool = paramObject instanceof JSONObject;
      if (bool)
      {
        paramObject = a((JSONObject)paramObject);
      }
      else
      {
        bool = paramObject instanceof JSONArray;
        if (bool) {
          paramObject = a((JSONArray)paramObject);
        }
      }
    }
  }
  
  private static List a(JSONArray paramJSONArray)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    int m = 0;
    for (;;)
    {
      int n = paramJSONArray.length();
      if (m >= n) {
        break;
      }
      Object localObject = a(paramJSONArray.get(m));
      localArrayList.add(localObject);
      m += 1;
    }
    return localArrayList;
  }
  
  private static Map a(JSONObject paramJSONObject)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Iterator localIterator = paramJSONObject.keys();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      String str = (String)localIterator.next();
      Object localObject = a(paramJSONObject.get(str));
      localHashMap.put(str, localObject);
    }
    return localHashMap;
  }
  
  private void a(String paramString)
  {
    h = paramString;
    c localc = f;
    g = localc;
  }
  
  public static b b()
  {
    b localb = b;
    if (localb == null)
    {
      localb = new in/org/npci/upiapp/core/b;
      localb.<init>();
    }
    return b;
  }
  
  public Map a()
  {
    try
    {
      Object localObject = i;
      localObject = ((a)localObject).getOtpRules();
      localObject = a(localObject);
      localObject = (Map)localObject;
      c = (Map)localObject;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1 = "OtpReader";
        String str2 = "Exception in getRule";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
      }
    }
    return c;
  }
  
  public void a(Activity paramActivity, a parama)
  {
    j = paramActivity;
    i = parama;
    d.a();
    int m = paramActivity.getApplicationContext().getResources().getInteger(2131427333);
    long l1 = m;
    Long localLong1 = Long.valueOf(l1);
    int n = HomeActivity.r().intValue();
    Object localObject = Long.valueOf(n);
    Long localLong2 = HomeActivity.o();
    long l2 = localLong2.longValue();
    long l3 = localLong1.longValue();
    l2 -= l3;
    l3 = System.currentTimeMillis();
    long l4 = ((Long)localObject).longValue();
    l3 -= l4;
    boolean bool3 = l2 < l3;
    if (bool3)
    {
      localObject = HomeActivity.o();
      l2 = ((Long)localObject).longValue();
      l1 = localLong1.longValue();
      l1 = l2 - l1;
    }
    for (localLong1 = Long.valueOf(l1);; localLong1 = Long.valueOf(l1))
    {
      localObject = d;
      l2 = ((Long)localObject).longValue();
      l3 = localLong1.longValue();
      boolean bool2 = l2 < l3;
      if (bool2) {
        d = localLong1;
      }
      localLong1 = d;
      localObject = HomeActivity.s();
      a(localLong1, (String)localObject);
      boolean bool1 = a;
      if (bool1) {
        e();
      }
      f();
      return;
      l2 = System.currentTimeMillis();
      l1 = ((Long)localObject).longValue();
      l1 = l2 - l1;
    }
  }
  
  public void a(c paramc)
  {
    Object localObject = paramc.c();
    a((String)localObject);
    f = paramc;
    try
    {
      localObject = i;
      ((a)localObject).onOtpReceived();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1 = "OtpReader";
        String str2 = "Exception in onOtpReceived";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
      }
    }
  }
  
  public void a(Long paramLong, String paramString)
  {
    Object localObject1 = d.b();
    Object localObject2 = j.getApplicationContext();
    Map localMap = a();
    long l = paramLong.longValue();
    String str = paramString;
    localObject1 = ((d)localObject1).a((Context)localObject2, paramString, localMap, l);
    if (localObject1 != null)
    {
      localObject2 = g;
      if (localObject2 != null)
      {
        localObject2 = g.c();
        str = ((c)localObject1).c();
        boolean bool = ((String)localObject2).equals(str);
        if (bool) {}
      }
      else
      {
        a((c)localObject1);
      }
    }
  }
  
  public void c()
  {
    d();
    g();
    Object localObject = f;
    if (localObject != null)
    {
      localObject = d.b();
      Application localApplication = j.getApplication();
      c localc = f;
      ((d)localObject).a(localApplication, localc);
    }
    b = null;
    d.b().c();
  }
  
  public void d()
  {
    Timer localTimer = e;
    if (localTimer != null)
    {
      localTimer = e;
      localTimer.cancel();
    }
  }
  
  public void e()
  {
    int m = HomeActivity.p().intValue();
    Object localObject1 = HomeActivity.q();
    int n = ((Integer)localObject1).intValue();
    Object localObject2;
    int i1;
    if (m == 0)
    {
      localObject2 = j.getApplicationContext().getResources();
      i1 = 2131427334;
      m = ((Resources)localObject2).getInteger(i1);
    }
    for (int i2 = m;; i2 = m)
    {
      if (n == 0)
      {
        localObject1 = j.getApplicationContext().getResources();
        m = 2131427335;
        n = ((Resources)localObject1).getInteger(m);
      }
      for (i1 = n;; i1 = n)
      {
        localObject1 = new java/util/Timer;
        ((Timer)localObject1).<init>();
        e = ((Timer)localObject1);
        localObject1 = e;
        localObject2 = new in/org/npci/upiapp/core/b$1;
        ((b.1)localObject2).<init>(this);
        long l1 = i1;
        long l2 = i2;
        ((Timer)localObject1).scheduleAtFixedRate((TimerTask)localObject2, l1, l2);
        return;
      }
    }
  }
  
  public void f()
  {
    Object localObject1 = j;
    if (localObject1 != null)
    {
      localObject1 = new android/content/IntentFilter;
      ((IntentFilter)localObject1).<init>();
      ((IntentFilter)localObject1).addAction("android.provider.Telephony.SMS_RECEIVED");
      int m = 999;
      ((IntentFilter)localObject1).setPriority(m);
      Object localObject2 = new in/org/npci/upiapp/core/b$a;
      ((b.a)localObject2).<init>(this, null);
      k = ((b.a)localObject2);
      localObject2 = j;
      b.a locala = k;
      ((Activity)localObject2).registerReceiver(locala, (IntentFilter)localObject1);
    }
  }
  
  public void g()
  {
    Object localObject = j;
    if (localObject != null)
    {
      localObject = k;
      if (localObject != null)
      {
        localObject = j;
        b.a locala = k;
        ((Activity)localObject).unregisterReceiver(locala);
        localObject = null;
        k = null;
      }
    }
  }
  
  public String h()
  {
    return h;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */