package in.org.npci.upiapp.core;

import android.content.Context;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class d
{
  private static final String a = d.class.getName();
  private static d b;
  private List c;
  
  private c a(String paramString, Map paramMap, Date paramDate)
  {
    boolean bool1 = false;
    Object localObject1 = (String)paramMap.get("otp");
    Object localObject2 = (String)paramMap.get("bank");
    String str = ((String)localObject2).toUpperCase();
    Matcher localMatcher = Pattern.compile((String)localObject1).matcher(paramString);
    localObject1 = "group";
    boolean bool2 = paramMap.containsKey(localObject1);
    int j;
    if (bool2)
    {
      localObject1 = (Integer)paramMap.get("group");
      j = ((Integer)localObject1).intValue();
    }
    for (;;)
    {
      localObject2 = new in/org/npci/upiapp/core/c;
      ((c)localObject2).<init>();
      ((c)localObject2).b(paramString);
      ((c)localObject2).a(paramDate);
      bool1 = localMatcher.find();
      if (bool1)
      {
        int i = localMatcher.groupCount();
        if (j <= i)
        {
          ((c)localObject2).a(str);
          localObject1 = localMatcher.group(j);
          ((c)localObject2).c((String)localObject1);
        }
      }
      for (localObject1 = localObject2;; localObject1 = null)
      {
        return (c)localObject1;
        j = 0;
      }
      j = 0;
      localObject1 = null;
    }
  }
  
  private String a(c paramc)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = paramc.a();
    localStringBuilder = localStringBuilder.append(str);
    long l = paramc.b().getTime();
    return in.org.npci.upiapp.utils.a.b(l);
  }
  
  private String a(ArrayList paramArrayList)
  {
    int i = paramArrayList.size();
    if (i <= 0)
    {
      i = 0;
      localObject1 = null;
      return (String)localObject1;
    }
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = ((StringBuilder)localObject1).append("");
    localObject1 = (String)paramArrayList.get(0);
    localObject2 = (String)localObject1;
    i = 1;
    localObject1 = localObject2;
    int j = i;
    for (;;)
    {
      int k = paramArrayList.size();
      if (j >= k) {
        break;
      }
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append((String)localObject1).append(",");
      localObject1 = (String)paramArrayList.get(j);
      localObject3 = (String)localObject1;
      i = j + 1;
      j = i;
      localObject1 = localObject3;
    }
  }
  
  private String a(Map paramMap)
  {
    Object localObject = paramMap.get("bank");
    if (localObject != null) {}
    for (localObject = paramMap.get("bank").toString().toUpperCase();; localObject = null) {
      return (String)localObject;
    }
  }
  
  public static void a()
  {
    b = null;
    b();
  }
  
  private boolean a(Context paramContext, String paramString)
  {
    Object localObject = c;
    if (localObject == null)
    {
      localObject = new in/org/npci/upiapp/utils/c;
      ((in.org.npci.upiapp.utils.c)localObject).<init>(paramContext);
      String str1 = "";
      localObject = ((in.org.npci.upiapp.utils.c)localObject).b("msgID", str1);
      String str2 = ",";
      localObject = Arrays.asList(((String)localObject).split(str2));
      c = ((List)localObject);
    }
    return c.contains(paramString);
  }
  
  private boolean a(String paramString1, String paramString2)
  {
    return Pattern.compile(paramString2, 2).matcher(paramString1).find();
  }
  
  private boolean a(String paramString, List paramList)
  {
    Iterator localIterator = paramList.iterator();
    Object localObject;
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject = (String)localIterator.next();
      int i = 2;
      localObject = Pattern.compile((String)localObject, i).matcher(paramString);
      bool = ((Matcher)localObject).find();
    } while (!bool);
    boolean bool = true;
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  private boolean a(String paramString, Map paramMap)
  {
    String str = a(paramMap);
    return paramString.equalsIgnoreCase(str);
  }
  
  public static d b()
  {
    d locald = b;
    if (locald == null)
    {
      locald = new in/org/npci/upiapp/core/d;
      locald.<init>();
      b = locald;
    }
    return b;
  }
  
  private void b(Context paramContext, String paramString)
  {
    if (paramString == null) {}
    for (;;)
    {
      return;
      in.org.npci.upiapp.utils.c localc = new in/org/npci/upiapp/utils/c;
      localc.<init>(paramContext);
      Object localObject1 = localc.b("msgID", "");
      Object localObject2 = new java/util/ArrayList;
      String str = ",";
      localObject1 = Arrays.asList(((String)localObject1).split(str));
      ((ArrayList)localObject2).<init>((Collection)localObject1);
      boolean bool = ((ArrayList)localObject2).contains(paramString);
      if (!bool)
      {
        int i = ((ArrayList)localObject2).size();
        int j = 10;
        if (i >= j)
        {
          i = 0;
          localObject1 = null;
          ((ArrayList)localObject2).remove(0);
        }
        ((ArrayList)localObject2).add(paramString);
        localObject1 = a((ArrayList)localObject2);
        localObject2 = "msgID";
        localc.a((String)localObject2, (String)localObject1);
      }
    }
  }
  
  private boolean b(Context paramContext, c paramc)
  {
    String str = paramc.d();
    boolean bool = a(paramContext, str);
    if (!bool)
    {
      str = a(paramc);
      bool = a(paramContext, str);
      if (!bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  private boolean b(String paramString, Map paramMap)
  {
    Object localObject = (Map)paramMap.get("matches");
    String str = "sender";
    localObject = (List)((Map)localObject).get(str);
    boolean bool;
    if (localObject != null) {
      bool = a(paramString, (List)localObject);
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  private boolean c(String paramString, Map paramMap)
  {
    Object localObject = (Map)paramMap.get("matches");
    String str = "message";
    localObject = (String)((Map)localObject).get(str);
    boolean bool;
    if (localObject != null) {
      bool = a(paramString, (String)localObject);
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  /* Error */
  public c a(Context paramContext, String paramString, Map paramMap, long paramLong)
  {
    // Byte code:
    //   0: iconst_2
    //   1: istore 6
    //   3: iconst_3
    //   4: istore 7
    //   6: iconst_1
    //   7: istore 8
    //   9: ldc -31
    //   11: invokestatic 231	android/net/Uri:parse	(Ljava/lang/String;)Landroid/net/Uri;
    //   14: astore 9
    //   16: iconst_4
    //   17: istore 10
    //   19: iload 10
    //   21: anewarray 32	java/lang/String
    //   24: astore 11
    //   26: aload 11
    //   28: iconst_0
    //   29: ldc -22
    //   31: aastore
    //   32: aload 11
    //   34: iload 8
    //   36: ldc -20
    //   38: aastore
    //   39: aload 11
    //   41: iload 6
    //   43: ldc -18
    //   45: aastore
    //   46: ldc -16
    //   48: astore 12
    //   50: aload 11
    //   52: iload 7
    //   54: aload 12
    //   56: aastore
    //   57: ldc -14
    //   59: astore 13
    //   61: ldc -12
    //   63: astore 14
    //   65: aload_1
    //   66: invokevirtual 250	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   69: astore 12
    //   71: iconst_1
    //   72: istore 7
    //   74: iload 7
    //   76: anewarray 32	java/lang/String
    //   79: astore 15
    //   81: iconst_0
    //   82: istore 6
    //   84: aconst_null
    //   85: astore 16
    //   87: new 86	java/lang/StringBuilder
    //   90: astore 17
    //   92: aload 17
    //   94: invokespecial 87	java/lang/StringBuilder:<init>	()V
    //   97: ldc 120
    //   99: astore 18
    //   101: aload 17
    //   103: aload 18
    //   105: invokevirtual 93	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   108: astore 17
    //   110: aload 17
    //   112: lload 4
    //   114: invokevirtual 105	java/lang/StringBuilder:append	(J)Ljava/lang/StringBuilder;
    //   117: astore 17
    //   119: aload 17
    //   121: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   124: astore 17
    //   126: aload 15
    //   128: iconst_0
    //   129: aload 17
    //   131: aastore
    //   132: aload 12
    //   134: aload 9
    //   136: aload 11
    //   138: aload 13
    //   140: aload 15
    //   142: aload 14
    //   144: invokevirtual 256	android/content/ContentResolver:query	(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   147: astore 16
    //   149: aload 16
    //   151: invokeinterface 261 1 0
    //   156: istore 10
    //   158: iload 10
    //   160: ifeq +316 -> 476
    //   163: iconst_1
    //   164: istore 10
    //   166: aload 16
    //   168: iload 10
    //   170: invokeinterface 264 2 0
    //   175: astore 9
    //   177: iconst_2
    //   178: istore 10
    //   180: aload 16
    //   182: iload 10
    //   184: invokeinterface 264 2 0
    //   189: astore 11
    //   191: new 98	java/util/Date
    //   194: astore 13
    //   196: iconst_3
    //   197: istore 10
    //   199: aload 16
    //   201: iload 10
    //   203: invokeinterface 268 2 0
    //   208: lstore 19
    //   210: aload 13
    //   212: lload 19
    //   214: invokespecial 271	java/util/Date:<init>	(J)V
    //   217: aload_0
    //   218: astore 12
    //   220: aload_2
    //   221: astore 15
    //   223: aload_3
    //   224: astore 14
    //   226: aload_0
    //   227: aload 9
    //   229: aload 11
    //   231: aload 13
    //   233: aload_2
    //   234: aload_3
    //   235: invokevirtual 274	in/org/npci/upiapp/core/d:a	(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/util/Map;)Lin/org/npci/upiapp/core/c;
    //   238: astore 12
    //   240: aload 12
    //   242: ifnonnull +52 -> 294
    //   245: new 98	java/util/Date
    //   248: astore 13
    //   250: iconst_3
    //   251: istore 10
    //   253: aload 16
    //   255: iload 10
    //   257: invokeinterface 268 2 0
    //   262: lstore 19
    //   264: aload 13
    //   266: lload 19
    //   268: invokespecial 271	java/util/Date:<init>	(J)V
    //   271: ldc_w 276
    //   274: astore 15
    //   276: aload_0
    //   277: astore 12
    //   279: aload_0
    //   280: aload 9
    //   282: aload 11
    //   284: aload 13
    //   286: aload 15
    //   288: aload_3
    //   289: invokevirtual 274	in/org/npci/upiapp/core/d:a	(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;Ljava/util/Map;)Lin/org/npci/upiapp/core/c;
    //   292: astore 12
    //   294: aload 12
    //   296: ifnull -147 -> 149
    //   299: ldc -22
    //   301: astore 9
    //   303: aload 16
    //   305: aload 9
    //   307: invokeinterface 280 2 0
    //   312: istore 21
    //   314: aload 16
    //   316: iload 21
    //   318: invokeinterface 268 2 0
    //   323: lstore 22
    //   325: lload 22
    //   327: invokestatic 286	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   330: astore 9
    //   332: aload 9
    //   334: invokestatic 289	java/lang/String:valueOf	(Ljava/lang/Object;)Ljava/lang/String;
    //   337: astore 9
    //   339: aload 12
    //   341: aload 9
    //   343: invokevirtual 291	in/org/npci/upiapp/core/c:d	(Ljava/lang/String;)V
    //   346: aload_0
    //   347: aload_1
    //   348: aload 12
    //   350: invokespecial 294	in/org/npci/upiapp/core/d:b	(Landroid/content/Context;Lin/org/npci/upiapp/core/c;)Z
    //   353: istore 21
    //   355: iload 21
    //   357: ifeq +18 -> 375
    //   360: aload 16
    //   362: ifnull +10 -> 372
    //   365: aload 16
    //   367: invokeinterface 297 1 0
    //   372: aload 12
    //   374: areturn
    //   375: getstatic 18	in/org/npci/upiapp/core/d:a	Ljava/lang/String;
    //   378: astore 9
    //   380: new 86	java/lang/StringBuilder
    //   383: astore 11
    //   385: aload 11
    //   387: invokespecial 87	java/lang/StringBuilder:<init>	()V
    //   390: ldc_w 299
    //   393: astore 13
    //   395: aload 11
    //   397: aload 13
    //   399: invokevirtual 93	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   402: astore 11
    //   404: aload 11
    //   406: aload 12
    //   408: invokevirtual 302	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   411: astore 12
    //   413: aload 12
    //   415: invokevirtual 108	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   418: astore 12
    //   420: aload 9
    //   422: aload 12
    //   424: invokestatic 305	in/juspay/tracker/f:a	(Ljava/lang/String;Ljava/lang/String;)V
    //   427: goto -278 -> 149
    //   430: astore 12
    //   432: aload 16
    //   434: astore 9
    //   436: getstatic 18	in/org/npci/upiapp/core/d:a	Ljava/lang/String;
    //   439: astore 11
    //   441: ldc_w 307
    //   444: astore 13
    //   446: aload 11
    //   448: aload 13
    //   450: aload 12
    //   452: invokestatic 312	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   455: aload 9
    //   457: ifnull +10 -> 467
    //   460: aload 9
    //   462: invokeinterface 297 1 0
    //   467: iconst_0
    //   468: istore 10
    //   470: aconst_null
    //   471: astore 12
    //   473: goto -101 -> 372
    //   476: aload 16
    //   478: ifnull -11 -> 467
    //   481: aload 16
    //   483: invokeinterface 297 1 0
    //   488: goto -21 -> 467
    //   491: astore 12
    //   493: iconst_0
    //   494: istore 6
    //   496: aconst_null
    //   497: astore 16
    //   499: aload 16
    //   501: ifnull +10 -> 511
    //   504: aload 16
    //   506: invokeinterface 297 1 0
    //   511: aload 12
    //   513: athrow
    //   514: astore 12
    //   516: goto -17 -> 499
    //   519: astore 12
    //   521: aload 9
    //   523: astore 16
    //   525: goto -26 -> 499
    //   528: astore 12
    //   530: iconst_0
    //   531: istore 21
    //   533: aconst_null
    //   534: astore 9
    //   536: goto -100 -> 436
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	539	0	this	d
    //   0	539	1	paramContext	Context
    //   0	539	2	paramString	String
    //   0	539	3	paramMap	Map
    //   0	539	4	paramLong	long
    //   1	494	6	i	int
    //   4	71	7	j	int
    //   7	28	8	k	int
    //   14	521	9	localObject1	Object
    //   17	3	10	m	int
    //   156	13	10	n	int
    //   178	291	10	i1	int
    //   24	423	11	localObject2	Object
    //   48	375	12	localObject3	Object
    //   430	21	12	localException1	Exception
    //   471	1	12	localObject4	Object
    //   491	21	12	localObject5	Object
    //   514	1	12	localObject6	Object
    //   519	1	12	localObject7	Object
    //   528	1	12	localException2	Exception
    //   59	390	13	localObject8	Object
    //   63	162	14	localObject9	Object
    //   79	208	15	localObject10	Object
    //   85	439	16	localObject11	Object
    //   90	40	17	localObject12	Object
    //   99	5	18	str	String
    //   208	59	19	l1	long
    //   312	5	21	i2	int
    //   353	179	21	bool	boolean
    //   323	3	22	l2	long
    // Exception table:
    //   from	to	target	type
    //   149	156	430	java/lang/Exception
    //   168	175	430	java/lang/Exception
    //   182	189	430	java/lang/Exception
    //   191	194	430	java/lang/Exception
    //   201	208	430	java/lang/Exception
    //   212	217	430	java/lang/Exception
    //   234	238	430	java/lang/Exception
    //   245	248	430	java/lang/Exception
    //   255	262	430	java/lang/Exception
    //   266	271	430	java/lang/Exception
    //   288	292	430	java/lang/Exception
    //   305	312	430	java/lang/Exception
    //   316	323	430	java/lang/Exception
    //   325	330	430	java/lang/Exception
    //   332	337	430	java/lang/Exception
    //   341	346	430	java/lang/Exception
    //   348	353	430	java/lang/Exception
    //   375	378	430	java/lang/Exception
    //   380	383	430	java/lang/Exception
    //   385	390	430	java/lang/Exception
    //   397	402	430	java/lang/Exception
    //   406	411	430	java/lang/Exception
    //   413	418	430	java/lang/Exception
    //   422	427	430	java/lang/Exception
    //   65	69	491	finally
    //   74	79	491	finally
    //   87	90	491	finally
    //   92	97	491	finally
    //   103	108	491	finally
    //   112	117	491	finally
    //   119	124	491	finally
    //   129	132	491	finally
    //   142	147	491	finally
    //   149	156	514	finally
    //   168	175	514	finally
    //   182	189	514	finally
    //   191	194	514	finally
    //   201	208	514	finally
    //   212	217	514	finally
    //   234	238	514	finally
    //   245	248	514	finally
    //   255	262	514	finally
    //   266	271	514	finally
    //   288	292	514	finally
    //   305	312	514	finally
    //   316	323	514	finally
    //   325	330	514	finally
    //   332	337	514	finally
    //   341	346	514	finally
    //   348	353	514	finally
    //   375	378	514	finally
    //   380	383	514	finally
    //   385	390	514	finally
    //   397	402	514	finally
    //   406	411	514	finally
    //   413	418	514	finally
    //   422	427	514	finally
    //   436	439	519	finally
    //   450	455	519	finally
    //   65	69	528	java/lang/Exception
    //   74	79	528	java/lang/Exception
    //   87	90	528	java/lang/Exception
    //   92	97	528	java/lang/Exception
    //   103	108	528	java/lang/Exception
    //   112	117	528	java/lang/Exception
    //   119	124	528	java/lang/Exception
    //   129	132	528	java/lang/Exception
    //   142	147	528	java/lang/Exception
  }
  
  public c a(String paramString1, String paramString2, Date paramDate, String paramString3, Map paramMap)
  {
    boolean bool1 = true;
    str1 = null;
    for (;;)
    {
      try
      {
        boolean bool2 = a(paramString3, paramMap);
        if (bool2)
        {
          bool3 = b(paramString1, paramMap);
          if (bool3)
          {
            bool3 = bool1;
            if (!bool3) {
              continue;
            }
            bool3 = c(paramString2, paramMap);
            if (!bool3) {
              continue;
            }
            if (!bool1) {
              continue;
            }
            localObject = a(paramString2, paramMap, paramDate);
            return (c)localObject;
          }
        }
        boolean bool3 = false;
        str2 = null;
        continue;
        bool1 = false;
        localObject = null;
        continue;
        if (!bool2) {
          continue;
        }
      }
      catch (Exception localException)
      {
        Object localObject;
        str1 = a;
        String str2 = "Exception while extracting OTP";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
        continue;
      }
      bool1 = false;
      localObject = null;
      continue;
      localObject = "unknown_bank";
      bool1 = ((String)localObject).equals(paramString3);
      if (bool1)
      {
        bool1 = b(paramString1, paramMap);
        if (bool1)
        {
          bool1 = c(paramString2, paramMap);
          if (bool1) {
            localObject = a(paramString2, paramMap, paramDate);
          }
        }
      }
    }
  }
  
  public void a(Context paramContext, c paramc)
  {
    String str = paramc.d();
    if (str != null) {}
    for (str = paramc.d();; str = a(paramc))
    {
      b(paramContext, str);
      return;
    }
  }
  
  public void c()
  {
    b = null;
    List localList = c;
    if (localList != null) {
      c = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */