package in.org.npci.upiapp.core;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import in.juspay.tracker.b;
import in.juspay.tracker.c;
import in.juspay.tracker.e;
import in.juspay.tracker.e.a;
import in.juspay.tracker.i;
import in.juspay.tracker.j;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class f
{
  private static f a;
  private String b;
  private Context c;
  private Activity d;
  private int e = 0;
  private i f;
  private j g;
  private boolean h;
  private boolean i;
  private final long j;
  private boolean k;
  
  public f(Activity paramActivity)
  {
    Object localObject = i.a();
    f = ((i)localObject);
    localObject = j.a();
    g = ((j)localObject);
    h = false;
    i = false;
    j = 10000L;
    d = paramActivity;
    localObject = paramActivity.getApplicationContext();
    c = ((Context)localObject);
    localObject = d.getResources().getString(2131165269);
    b = ((String)localObject);
    a = this;
    b();
  }
  
  public static f a()
  {
    return a;
  }
  
  private void b()
  {
    Handler localHandler = new android/os/Handler;
    localHandler.<init>();
    f.1 local1 = new in/org/npci/upiapp/core/f$1;
    local1.<init>(this);
    localHandler.postDelayed(local1, 10000L);
  }
  
  private void c()
  {
    e.a(e.a.b);
    e locale = e.a();
    Object localObject = d;
    locale.a((Context)localObject);
    locale = e.a();
    localObject = c.getResources().getString(2131165239);
    locale.a((String)localObject);
    e.a().b("NPCI");
  }
  
  private void d()
  {
    Object localObject = g;
    String str = d.getResources().getString(2131165258);
    ((j)localObject).a(str);
    localObject = g;
    str = d.getResources().getString(2131165272);
    ((j)localObject).b(str);
    localObject = g;
    str = d.getResources().getString(2131165263);
    ((j)localObject).c(str);
    localObject = g;
    str = c.getResources().getString(2131165239);
    ((j)localObject).d(str);
    localObject = f;
    int m = e;
    ((i)localObject).a(m);
    localObject = f;
    str = b;
    ((i)localObject).a(str);
  }
  
  public void fabricLogInfo(String paramString1, String paramString2)
  {
    try
    {
      Answers localAnswers = Answers.getInstance();
      Object localObject = new com/crashlytics/android/answers/CustomEvent;
      String str = paramString2.toUpperCase();
      ((CustomEvent)localObject).<init>(str);
      localObject = ((CustomEvent)localObject).putCustomAttribute(paramString1, paramString2);
      localObject = (CustomEvent)localObject;
      localAnswers.logCustom((CustomEvent)localObject);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public String getSessionInfo()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    Map localMap = e.a().f();
    localJSONObject.<init>(localMap);
    return localJSONObject.toString();
  }
  
  public void initLoggingService()
  {
    initLoggingService(true);
  }
  
  public void initLoggingService(boolean paramBoolean)
  {
    boolean bool = h;
    if (!bool)
    {
      bool = true;
      h = bool;
      d();
      c();
      k = paramBoolean;
    }
  }
  
  public void setLogLevel(String paramString)
  {
    try
    {
      int m = Integer.parseInt(paramString);
      e = m;
      i locali = f;
      int n = e;
      locali.a(n);
      return;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;)
      {
        e locale = e.a();
        c localc2 = new in/juspay/tracker/c;
        localc2.<init>();
        c localc1 = localc2.a(localNumberFormatException);
        locale.a(localc1);
      }
    }
  }
  
  public void setLogPushConfig(String paramString)
  {
    try
    {
      i locali = f;
      localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>(paramString);
      locali.a((JSONObject)localObject);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject = e.a();
        c localc2 = new in/juspay/tracker/c;
        localc2.<init>();
        c localc1 = localc2.a(localException);
        ((e)localObject).a(localc1);
      }
    }
  }
  
  public void setLoggingRules(String paramString)
  {
    try
    {
      i locali = f;
      localObject = new org/json/JSONObject;
      ((JSONObject)localObject).<init>(paramString);
      locali.b((JSONObject)localObject);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject = e.a();
        c localc2 = new in/juspay/tracker/c;
        localc2.<init>();
        c localc1 = localc2.a(localException);
        ((e)localObject).a(localc1);
      }
    }
  }
  
  public void setRemoteVersion(String paramString)
  {
    j localj = g;
    String str = d.getResources().getString(2131165272);
    localj.b(str);
  }
  
  public void setWhiteListedArray(String paramString)
  {
    try
    {
      i locali = f;
      localObject = new org/json/JSONArray;
      ((JSONArray)localObject).<init>(paramString);
      locali.a((JSONArray)localObject);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject = e.a();
        c localc2 = new in/juspay/tracker/c;
        localc2.<init>();
        c localc1 = localc2.a(localException);
        ((e)localObject).a(localc1);
      }
    }
  }
  
  public void setblackListedArray(String paramString)
  {
    try
    {
      i locali = f;
      localObject = new org/json/JSONArray;
      ((JSONArray)localObject).<init>(paramString);
      locali.b((JSONArray)localObject);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject = e.a();
        c localc2 = new in/juspay/tracker/c;
        localc2.<init>();
        c localc1 = localc2.a(localException);
        ((e)localObject).a(localc1);
      }
    }
  }
  
  public void startPushingLogs()
  {
    i = true;
    e.a().d();
  }
  
  public void trackError(String paramString)
  {
    boolean bool = i;
    if (bool)
    {
      e locale = e.a();
      Date localDate = new java/util/Date;
      localDate.<init>();
      locale.a(localDate, paramString);
    }
  }
  
  public void trackEvent(String paramString1, String paramString2)
  {
    b localb = new in/juspay/tracker/b;
    localb.<init>();
    localb.b(paramString1);
    localb.c(paramString2);
    int m = 4;
    Object localObject = new String[m];
    e locale = null;
    localObject[0] = "SCREEN";
    localObject[1] = "BUTTON_CLICKED";
    localObject[2] = "PERMISSION";
    String str = "INFO";
    localObject[3] = str;
    localObject = Arrays.asList((Object[])localObject);
    boolean bool1 = i;
    if (bool1)
    {
      locale = e.a();
      locale.a(localb);
    }
    boolean bool2 = k;
    if (bool2)
    {
      bool2 = ((List)localObject).contains(paramString1);
      if (bool2) {
        fabricLogInfo(paramString1, paramString2);
      }
    }
  }
  
  public void trackInfo(String paramString)
  {
    boolean bool = i;
    if (bool)
    {
      e locale = e.a();
      locale.c(paramString);
    }
  }
  
  public void trackSession()
  {
    boolean bool = i;
    if (bool)
    {
      e locale = e.a();
      Map localMap = e.a().f();
      locale.a(localMap);
    }
  }
  
  public void updateLoggingEndPoint(String paramString)
  {
    b = paramString;
    i locali = f;
    String str = b;
    locali.a(str);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */