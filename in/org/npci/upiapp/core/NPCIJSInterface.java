package in.org.npci.upiapp.core;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Base64;
import in.juspay.mystique.d;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.npci.upi.security.services.CLRemoteResultReceiver;
import org.npci.upi.security.services.CLServices;
import org.npci.upi.security.services.ServiceConnectionStatusNotifier;

public class NPCIJSInterface
{
  private static CLServices b;
  private Activity a;
  private d c;
  private String d;
  
  public NPCIJSInterface(Activity paramActivity, d paramd)
  {
    a = paramActivity;
    c = paramd;
  }
  
  private Object a(Object paramObject)
  {
    if (paramObject == null) {
      paramObject = JSONObject.NULL;
    }
    for (;;)
    {
      return paramObject;
      boolean bool = paramObject instanceof JSONArray;
      if (!bool)
      {
        bool = paramObject instanceof JSONObject;
        if (!bool)
        {
          Object localObject = JSONObject.NULL;
          bool = paramObject.equals(localObject);
          if (!bool)
          {
            bool = paramObject instanceof Collection;
            if (bool) {}
            try
            {
              localObject = new org/json/JSONArray;
              paramObject = (Collection)paramObject;
              ((JSONArray)localObject).<init>((Collection)paramObject);
              paramObject = localObject;
            }
            catch (Exception localException)
            {
              String str;
              paramObject = null;
            }
            localObject = paramObject.getClass();
            bool = ((Class)localObject).isArray();
            if (bool)
            {
              paramObject = b(paramObject);
            }
            else
            {
              bool = paramObject instanceof Map;
              if (bool)
              {
                localObject = new org/json/JSONObject;
                paramObject = (Map)paramObject;
                ((JSONObject)localObject).<init>((Map)paramObject);
                paramObject = localObject;
              }
              else
              {
                bool = paramObject instanceof Boolean;
                if (!bool)
                {
                  bool = paramObject instanceof Byte;
                  if (!bool)
                  {
                    bool = paramObject instanceof Character;
                    if (!bool)
                    {
                      bool = paramObject instanceof Double;
                      if (!bool)
                      {
                        bool = paramObject instanceof Float;
                        if (!bool)
                        {
                          bool = paramObject instanceof Integer;
                          if (!bool)
                          {
                            bool = paramObject instanceof Long;
                            if (!bool)
                            {
                              bool = paramObject instanceof Short;
                              if (!bool)
                              {
                                bool = paramObject instanceof String;
                                if (!bool)
                                {
                                  localObject = paramObject.getClass();
                                  localObject = ((Class)localObject).getPackage();
                                  localObject = ((Package)localObject).getName();
                                  str = "java.";
                                  bool = ((String)localObject).startsWith(str);
                                  if (bool) {
                                    paramObject = paramObject.toString();
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  private JSONArray b(Object paramObject)
  {
    Object localObject1 = new org/json/JSONArray;
    ((JSONArray)localObject1).<init>();
    Object localObject2 = paramObject.getClass();
    int i = ((Class)localObject2).isArray();
    if (i == 0)
    {
      localObject2 = new org/json/JSONException;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = ((StringBuilder)localObject1).append("Not a primitive array: ");
      Class localClass = paramObject.getClass();
      localObject1 = localClass;
      ((JSONException)localObject2).<init>((String)localObject1);
      throw ((Throwable)localObject2);
    }
    int k = Array.getLength(paramObject);
    i = 0;
    localObject2 = null;
    while (i < k)
    {
      Object localObject3 = Array.get(paramObject, i);
      localObject3 = a(localObject3);
      ((JSONArray)localObject1).put(localObject3);
      int j;
      i += 1;
    }
    return (JSONArray)localObject1;
  }
  
  public void a()
  {
    Object localObject1 = "NPCIJSInterface";
    Object localObject2 = "Lifecycle - onResume Called";
    in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2);
    for (;;)
    {
      try
      {
        localObject1 = d;
        if (localObject1 != null)
        {
          localObject2 = new org/json/JSONObject;
          ((JSONObject)localObject2).<init>();
          localObject1 = "NPCIJSInterface";
          localObject3 = "BACK_PRESSED - CALLBACK CALLED";
        }
      }
      catch (Exception localException2)
      {
        Object localObject3;
        String str;
        in.org.npci.upiapp.a.a.a(localException2);
        continue;
      }
      try
      {
        in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject3);
        localObject1 = "event";
        localObject3 = "back_pressed";
        ((JSONObject)localObject2).put((String)localObject1, localObject3);
      }
      catch (Exception localException1)
      {
        in.org.npci.upiapp.a.a.a(localException1);
      }
    }
    localObject1 = c;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    str = "window.callUICallback(\"";
    localObject3 = ((StringBuilder)localObject3).append(str);
    str = d;
    localObject3 = ((StringBuilder)localObject3).append(str);
    str = "\", ";
    localObject3 = ((StringBuilder)localObject3).append(str);
    localObject2 = ((JSONObject)localObject2).toString();
    localObject2 = ((StringBuilder)localObject3).append((String)localObject2);
    localObject3 = ");";
    localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((d)localObject1).a((String)localObject2);
    localObject1 = null;
    d = null;
  }
  
  public void b()
  {
    Activity localActivity = a;
    NPCIJSInterface.7 local7 = new in/org/npci/upiapp/core/NPCIJSInterface$7;
    local7.<init>(this);
    localActivity.runOnUiThread(local7);
  }
  
  public String decodeNPCIXmlKeys(String paramString)
  {
    String str = new java/lang/String;
    byte[] arrayOfByte = Base64.decode(paramString, 2);
    str.<init>(arrayOfByte);
    return str;
  }
  
  public String fetchData(String paramString)
  {
    return a.getSharedPreferences("NPCI", 0).getString(paramString, "NOT_FOUND");
  }
  
  public void getChallenge(String paramString1, String paramString2, String paramString3)
  {
    Activity localActivity = a;
    NPCIJSInterface.3 local3 = new in/org/npci/upiapp/core/NPCIJSInterface$3;
    local3.<init>(this, paramString3, paramString1, paramString2);
    localActivity.runOnUiThread(local3);
  }
  
  public void getCredentials(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, String paramString6, String paramString7, String paramString8, String paramString9)
  {
    d = paramString9;
    CLRemoteResultReceiver localCLRemoteResultReceiver = new org/npci/upi/security/services/CLRemoteResultReceiver;
    Object localObject1 = new in/org/npci/upiapp/core/NPCIJSInterface$5;
    Object localObject2 = new android/os/Handler;
    ((Handler)localObject2).<init>();
    ((NPCIJSInterface.5)localObject1).<init>(this, (Handler)localObject2, paramString9);
    localCLRemoteResultReceiver.<init>((ResultReceiver)localObject1);
    Activity localActivity = a;
    localObject1 = new in/org/npci/upiapp/core/NPCIJSInterface$6;
    localObject2 = this;
    ((NPCIJSInterface.6)localObject1).<init>(this, paramString9, localCLRemoteResultReceiver, paramString1, paramString2, paramString3, paramString4, paramString5, paramString6, paramString7, paramString8);
    localActivity.runOnUiThread((Runnable)localObject1);
  }
  
  public void handleInit(String paramString1, String paramString2, CLRemoteResultReceiver paramCLRemoteResultReceiver, String... paramVarArgs)
  {
    int i = 3;
    int j = 2;
    int k = 1;
    int m = 0;
    String str1 = null;
    for (;;)
    {
      try
      {
        Activity localActivity = a;
        NPCIJSInterface.2 local2 = new in/org/npci/upiapp/core/NPCIJSInterface$2;
        localObject2 = this;
        localObject3 = paramString2;
        localObject4 = paramVarArgs;
        localObject5 = paramString1;
        localObject6 = paramCLRemoteResultReceiver;
        local2.<init>(this, paramString2, paramVarArgs, paramString1, paramCLRemoteResultReceiver);
        CLServices.initService(localActivity, local2);
        return;
      }
      catch (RuntimeException localRuntimeException)
      {
        Object localObject4;
        Object localObject5;
        Object localObject6;
        int n = -1;
        int i3 = paramString2.hashCode();
        boolean bool;
        switch (i3)
        {
        default: 
          switch (n)
          {
          default: 
            break;
          case 0: 
            localObject1 = b;
            localObject2 = paramVarArgs[0];
            localObject3 = paramVarArgs[k];
            localObject1 = ((CLServices)localObject1).getChallenge((String)localObject2, (String)localObject3);
            localObject2 = c;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = ((StringBuilder)localObject3).append("window.callUICallback(\"").append(paramString1).append("\"");
            localObject4 = ",\"";
            localObject1 = ((StringBuilder)localObject3).append((String)localObject4).append((String)localObject1).append("\"");
            localObject3 = ")";
            localObject1 = (String)localObject3;
            ((d)localObject2).a((String)localObject1);
          }
          break;
        case 1393028525: 
          localObject2 = "getChallenge";
          bool = paramString2.equals(localObject2);
          if (!bool) {
            continue;
          }
          n = 0;
          localObject1 = null;
          break;
        case -2133316482: 
          localObject2 = "registerApp";
          bool = paramString2.equals(localObject2);
          if (!bool) {
            continue;
          }
          n = k;
          break;
        case -981163955: 
          localObject2 = "getCredential";
          bool = paramString2.equals(localObject2);
          if (!bool) {
            continue;
          }
          n = j;
          break;
        case -1531153537: 
          localObject2 = "unbindService";
          bool = paramString2.equals(localObject2);
          if (!bool) {
            continue;
          }
          n = i;
          continue;
          localObject1 = b;
          localObject2 = paramVarArgs[0];
          localObject3 = paramVarArgs[k];
          localObject4 = paramVarArgs[j];
          localObject5 = paramVarArgs[i];
          int i1 = ((CLServices)localObject1).registerApp((String)localObject2, (String)localObject3, (String)localObject4, (String)localObject5);
          localObject1 = Boolean.valueOf(i1);
          localObject2 = c;
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          localObject3 = ((StringBuilder)localObject3).append("window.callUICallback(\"").append(paramString1).append("\"");
          localObject4 = ",\"";
          localObject1 = ((StringBuilder)localObject3).append((String)localObject4).append(localObject1).append("\"");
          localObject3 = ")";
          localObject1 = (String)localObject3;
          ((d)localObject2).a((String)localObject1);
          continue;
          int i4 = paramVarArgs.length;
          i1 = 0;
          localObject1 = null;
          if (i1 < i4)
          {
            localObject3 = paramVarArgs[i1];
            localObject4 = "NPCIJSInterface";
            localObject5 = new java/lang/StringBuilder;
            ((StringBuilder)localObject5).<init>();
            localObject6 = "getCredentials - ";
            localObject5 = ((StringBuilder)localObject5).append((String)localObject6);
            localObject3 = (String)localObject3;
            in.org.npci.upiapp.a.a.a((String)localObject4, (String)localObject3);
            int i2;
            i1 += 1;
            continue;
          }
          localObject1 = b;
          localObject2 = paramVarArgs[0];
          localObject3 = paramVarArgs[k];
          localObject4 = paramVarArgs[j];
          localObject5 = paramVarArgs[i];
          int i5 = 4;
          localObject6 = paramVarArgs[i5];
          m = 5;
          str1 = paramVarArgs[m];
          k = 6;
          String str2 = paramVarArgs[k];
          j = 7;
          String str3 = paramVarArgs[j];
          ((CLServices)localObject1).getCredential((String)localObject2, (String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, str1, str2, str3, paramCLRemoteResultReceiver);
          localObject1 = c;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          localObject2 = ((StringBuilder)localObject2).append("window.callUICallback(\"").append(paramString1);
          localObject3 = "\")";
          localObject2 = (String)localObject3;
          ((d)localObject1).a((String)localObject2);
          continue;
        }
      }
      try
      {
        localObject1 = b;
        ((CLServices)localObject1).unbindService();
      }
      catch (Exception localException)
      {
        for (;;)
        {
          in.org.npci.upiapp.a.a.a(localException);
        }
      }
      Object localObject1 = c;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("window.callUICallback(\"").append(paramString1);
      Object localObject3 = "\")";
      localObject2 = (String)localObject3;
      ((d)localObject1).a((String)localObject2);
    }
  }
  
  public void initialiseNPCICL(String paramString)
  {
    try
    {
      Activity localActivity = a;
      localObject = new in/org/npci/upiapp/core/NPCIJSInterface$1;
      ((NPCIJSInterface.1)localObject).<init>(this, paramString);
      CLServices.initService(localActivity, (ServiceConnectionStatusNotifier)localObject);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject = "Service already initiated";
        String str = localException.getMessage();
        boolean bool = ((String)localObject).equals(str);
        d locald;
        if (bool)
        {
          locald = c;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          localObject = ((StringBuilder)localObject).append("window.callUICallback(\"").append(paramString);
          str = "\")";
          localObject = str;
          locald.a((String)localObject);
        }
        else
        {
          localObject = "NPCIJSInterface";
          str = "intialiseNPCICL";
          in.org.npci.upiapp.a.a.a((String)localObject, str, locald);
        }
      }
    }
  }
  
  public String populateHMAC(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    b();
    String str1 = null;
    try
    {
      Object localObject1 = new in/org/npci/upiapp/utils/a;
      ((in.org.npci.upiapp.utils.a)localObject1).<init>();
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = ((StringBuilder)localObject1).append(paramString1);
      localObject2 = "|";
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2);
      localObject1 = ((StringBuilder)localObject1).append(paramString2);
      localObject2 = "|";
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2);
      localObject1 = ((StringBuilder)localObject1).append(paramString4);
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject2 = "NPCIJSInterface";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      String str2 = "PSP Hmac Msg - ";
      localObject3 = ((StringBuilder)localObject3).append(str2);
      localObject3 = ((StringBuilder)localObject3).append((String)localObject1);
      localObject3 = ((StringBuilder)localObject3).toString();
      in.org.npci.upiapp.a.a.b((String)localObject2, (String)localObject3);
      int i = 2;
      localObject2 = Base64.decode(paramString3, i);
      localObject1 = in.org.npci.upiapp.utils.a.a((String)localObject1);
      localObject1 = in.org.npci.upiapp.utils.a.a((byte[])localObject1, (byte[])localObject2);
      i = 0;
      localObject2 = null;
      str1 = Base64.encodeToString((byte[])localObject1, 0);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = "NPCIJSInterface";
        Object localObject3 = "populateHMAC ";
        in.org.npci.upiapp.a.a.a((String)localObject2, (String)localObject3, localException);
      }
    }
    return str1;
  }
  
  public void registerApp(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    Activity localActivity = a;
    NPCIJSInterface.4 local4 = new in/org/npci/upiapp/core/NPCIJSInterface$4;
    local4.<init>(this, paramString5, paramString1, paramString2, paramString3, paramString4);
    localActivity.runOnUiThread(local4);
  }
  
  public void saveData(String paramString1, String paramString2)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = "Saving to local store : " + paramString1 + ", " + paramString2;
    in.org.npci.upiapp.a.a.a("NPCIJSInterface", (String)localObject);
    a.getSharedPreferences("NPCI", 0).edit().putString(paramString1, paramString2).apply();
  }
  
  public String trustCred(String paramString1, String paramString2)
  {
    String str = null;
    try
    {
      Object localObject = new in/org/npci/upiapp/utils/a;
      ((in.org.npci.upiapp.utils.a)localObject).<init>();
      localObject = in.org.npci.upiapp.utils.a.a(paramString1);
      int i = 2;
      byte[] arrayOfByte = Base64.decode(paramString2, i);
      localObject = in.org.npci.upiapp.utils.a.a((byte[])localObject, arrayOfByte);
      i = 2;
      str = Base64.encodeToString((byte[])localObject, i);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        in.org.npci.upiapp.a.a.a(localException);
      }
    }
    return str;
  }
  
  public void unbindNPCICL(String paramString)
  {
    String[] arrayOfString = new String[0];
    handleInit(paramString, "unbindService", null, arrayOfString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/NPCIJSInterface.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */