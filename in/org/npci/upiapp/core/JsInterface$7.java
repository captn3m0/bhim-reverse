package in.org.npci.upiapp.core;

import android.app.DatePickerDialog;
import android.content.Context;
import android.widget.DatePicker;
import java.util.Calendar;

class JsInterface$7
  implements Runnable
{
  JsInterface$7(JsInterface paramJsInterface, String paramString1, String paramString2, String paramString3, String paramString4) {}
  
  public void run()
  {
    Calendar localCalendar = Calendar.getInstance();
    JsInterface.7.1 local1 = new in/org/npci/upiapp/core/JsInterface$7$1;
    local1.<init>(this, localCalendar);
    long l1 = Long.valueOf(b).longValue();
    localCalendar.setTimeInMillis(l1);
    JsInterface localJsInterface = e;
    Object localObject1 = new android/app/DatePickerDialog;
    Object localObject2 = JsInterface.b(e);
    int i = localCalendar.get(1);
    int j = localCalendar.get(2);
    int k = 5;
    int m = localCalendar.get(k);
    ((DatePickerDialog)localObject1).<init>((Context)localObject2, local1, i, j, m);
    JsInterface.a(localJsInterface, (DatePickerDialog)localObject1);
    localObject1 = c;
    boolean bool;
    long l2;
    if (localObject1 != null)
    {
      localObject1 = c;
      bool = ((String)localObject1).isEmpty();
      if (!bool)
      {
        localObject1 = c;
        localObject2 = "undefined";
        bool = ((String)localObject1).equals(localObject2);
        if (!bool)
        {
          localObject1 = JsInterface.e(e).getDatePicker();
          localObject2 = Long.valueOf(c);
          l2 = ((Long)localObject2).longValue();
          ((DatePicker)localObject1).setMinDate(l2);
        }
      }
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject1 = d;
      bool = ((String)localObject1).isEmpty();
      if (!bool)
      {
        localObject1 = d;
        localObject2 = "undefined";
        bool = ((String)localObject1).equals(localObject2);
        if (!bool)
        {
          localObject1 = JsInterface.e(e).getDatePicker();
          localObject2 = Long.valueOf(d);
          l2 = ((Long)localObject2).longValue();
          ((DatePicker)localObject1).setMaxDate(l2);
        }
      }
    }
    JsInterface.e(e).show();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/JsInterface$7.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */