package in.org.npci.upiapp.core;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Parcelable;
import in.org.npci.upiapp.HomeActivity;
import java.io.File;

class JsInterface$23
  implements Runnable
{
  JsInterface$23(JsInterface paramJsInterface, String paramString1, boolean paramBoolean, String paramString2, String paramString3, String paramString4) {}
  
  public void run()
  {
    Object localObject1 = JsInterface.b(f);
    boolean bool1 = localObject1 instanceof HomeActivity;
    Object localObject2;
    String str;
    if (bool1)
    {
      localObject1 = (HomeActivity)JsInterface.b(f);
      localObject2 = f;
      str = a;
      localObject2 = ((JsInterface)localObject2).getExternalFilePath(str);
      boolean bool2 = b;
      if (!bool2)
      {
        bool2 = ((File)localObject2).exists();
        if (bool2) {
          break label217;
        }
      }
      localObject2 = a;
      str = c;
      boolean bool3 = true;
      ((HomeActivity)localObject1).a((String)localObject2, str, bool3);
      localObject1 = f;
      localObject2 = a;
      localObject1 = ((JsInterface)localObject1).getInternalFilePath((String)localObject2);
      localObject2 = f;
      str = a;
      localObject1 = ((File)localObject1).getAbsolutePath();
    }
    label217:
    for (localObject1 = ((JsInterface)localObject2).saveToExternalStorage(str, (String)localObject1);; localObject1 = localObject2)
    {
      localObject1 = Uri.fromFile((File)localObject1);
      localObject2 = new android/content/Intent;
      ((Intent)localObject2).<init>("android.intent.action.SEND");
      ((Intent)localObject2).setType("image/png");
      ((Intent)localObject2).putExtra("android.intent.extra.STREAM", (Parcelable)localObject1);
      str = d;
      ((Intent)localObject2).putExtra("android.intent.extra.SUBJECT", str);
      str = e;
      ((Intent)localObject2).putExtra("android.intent.extra.TEXT", str);
      localObject1 = JsInterface.b(f);
      str = "Share via";
      localObject2 = Intent.createChooser((Intent)localObject2, str);
      ((Activity)localObject1).startActivity((Intent)localObject2);
      return;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/JsInterface$23.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */