package in.org.npci.upiapp.core;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import in.org.npci.upiapp.a.a;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class e
{
  public static boolean a(Context paramContext)
  {
    int i = 1;
    int j = Build.VERSION.SDK_INT;
    int k = 22;
    if (j >= k)
    {
      SubscriptionManager localSubscriptionManager = SubscriptionManager.from(paramContext);
      j = localSubscriptionManager.getActiveSubscriptionInfoCount();
      if (j <= i) {}
    }
    for (;;)
    {
      return i;
      i = 0;
      continue;
      i = 0;
    }
  }
  
  public static boolean a(Context paramContext, int paramInt, String paramString1, String paramString2, String paramString3, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2)
  {
    int i = 1;
    if (paramInt == 0) {}
    for (;;)
    {
      try
      {
        localObject1 = Build.MODEL;
        localObject2 = "Philips T939";
        boolean bool1 = ((String)localObject1).equals(localObject2);
        if (!bool1) {
          continue;
        }
        localObject1 = "isms0";
        localObject2 = "android.os.ServiceManager";
        localObject2 = Class.forName((String)localObject2);
        localObject3 = "getService";
        int m = 1;
        localObject4 = new Class[m];
        int n = 0;
        localObject5 = null;
        localObject6 = String.class;
        localObject4[0] = localObject6;
        localObject2 = ((Class)localObject2).getDeclaredMethod((String)localObject3, (Class[])localObject4);
        boolean bool3 = true;
        ((Method)localObject2).setAccessible(bool3);
        bool3 = false;
        localObject3 = null;
        m = 1;
        localObject4 = new Object[m];
        n = 0;
        localObject5 = null;
        localObject4[0] = localObject1;
        localObject1 = ((Method)localObject2).invoke(null, (Object[])localObject4);
        localObject2 = "com.android.internal.telephony.ISms$Stub";
        localObject2 = Class.forName((String)localObject2);
        localObject3 = "asInterface";
        m = 1;
        localObject4 = new Class[m];
        n = 0;
        localObject5 = null;
        localObject6 = IBinder.class;
        localObject4[0] = localObject6;
        localObject2 = ((Class)localObject2).getDeclaredMethod((String)localObject3, (Class[])localObject4);
        bool3 = true;
        ((Method)localObject2).setAccessible(bool3);
        bool3 = false;
        localObject3 = null;
        m = 1;
        localObject4 = new Object[m];
        n = 0;
        localObject5 = null;
        localObject4[0] = localObject1;
        localObject1 = ((Method)localObject2).invoke(null, (Object[])localObject4);
        localObject2 = "SimUtil";
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject4 = "send msg - ";
        localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
        localObject3 = ((StringBuilder)localObject3).append(paramString3);
        localObject3 = ((StringBuilder)localObject3).toString();
        a.a((String)localObject2, (String)localObject3);
        int i2 = Build.VERSION.SDK_INT;
        i1 = 18;
        if (i2 >= i1) {
          continue;
        }
        localObject2 = localObject1.getClass();
        localObject3 = "sendText";
        m = 5;
        localObject4 = new Class[m];
        n = 0;
        localObject5 = null;
        localObject6 = String.class;
        localObject4[0] = localObject6;
        n = 1;
        localObject6 = String.class;
        localObject4[n] = localObject6;
        n = 2;
        localObject6 = String.class;
        localObject4[n] = localObject6;
        n = 3;
        localObject6 = PendingIntent.class;
        localObject4[n] = localObject6;
        n = 4;
        localObject6 = PendingIntent.class;
        localObject4[n] = localObject6;
        localObject2 = ((Class)localObject2).getMethod((String)localObject3, (Class[])localObject4);
        i1 = 5;
        localObject3 = new Object[i1];
        m = 0;
        localObject4 = null;
        localObject3[0] = paramString1;
        m = 1;
        localObject3[m] = paramString2;
        m = 2;
        localObject3[m] = paramString3;
        m = 3;
        localObject3[m] = paramPendingIntent1;
        m = 4;
        localObject3[m] = paramPendingIntent2;
        ((Method)localObject2).invoke(localObject1, (Object[])localObject3);
      }
      catch (Exception localException)
      {
        int j;
        a.a(localException);
        boolean bool2 = false;
        Object localObject1 = null;
        continue;
        bool2 = a(paramContext);
        if (!bool2) {
          continue;
        }
        Object localObject2 = new java/util/ArrayList;
        ((ArrayList)localObject2).<init>();
        localObject1 = SubscriptionManager.from(paramContext);
        localObject1 = ((SubscriptionManager)localObject1).getActiveSubscriptionInfoList();
        Object localObject3 = ((List)localObject1).iterator();
        bool2 = ((Iterator)localObject3).hasNext();
        if (!bool2) {
          continue;
        }
        localObject1 = ((Iterator)localObject3).next();
        localObject1 = (SubscriptionInfo)localObject1;
        int k = ((SubscriptionInfo)localObject1).getSubscriptionId();
        Object localObject4 = Integer.valueOf(k);
        ((ArrayList)localObject2).add(localObject4);
        localObject4 = "SimUtil";
        Object localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        Object localObject6 = "SmsManager - subscriptionId: ";
        localObject5 = ((StringBuilder)localObject5).append((String)localObject6);
        localObject1 = ((StringBuilder)localObject5).append(k);
        localObject1 = ((StringBuilder)localObject1).toString();
        a.a((String)localObject4, (String)localObject1);
        continue;
        localObject1 = ((ArrayList)localObject2).get(paramInt);
        localObject1 = (Integer)localObject1;
        k = ((Integer)localObject1).intValue();
        localObject1 = SmsManager.getSmsManagerForSubscriptionId(k);
        int i1 = 0;
        localObject3 = null;
        localObject2 = paramString1;
        localObject4 = paramString3;
        localObject5 = paramPendingIntent1;
        localObject6 = paramPendingIntent2;
        ((SmsManager)localObject1).sendTextMessage(paramString1, null, paramString3, paramPendingIntent1, paramPendingIntent2);
        continue;
        localObject1 = SmsManager.getDefault();
        i1 = 0;
        localObject3 = null;
        localObject2 = paramString1;
        localObject4 = paramString3;
        localObject5 = paramPendingIntent1;
        localObject6 = paramPendingIntent2;
        ((SmsManager)localObject1).sendTextMessage(paramString1, null, paramString3, paramPendingIntent1, paramPendingIntent2);
        continue;
      }
      j = i;
      return j;
      localObject1 = "isms";
      continue;
      if (paramInt != i) {
        continue;
      }
      localObject1 = "isms2";
    }
    localObject1 = new java/lang/Exception;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject3 = "can not get service which for sim '";
    localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).append(paramInt);
    localObject3 = "', only 0,1 accepted as values";
    localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
    localObject2 = ((StringBuilder)localObject2).toString();
    ((Exception)localObject1).<init>((String)localObject2);
    throw ((Throwable)localObject1);
  }
  
  public static boolean b(Context paramContext)
  {
    int i = 1;
    TelephonyManager localTelephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
    int j = localTelephonyManager.getSimState();
    if (j != i) {
      j = i;
    }
    for (;;)
    {
      return j;
      int k = 0;
      localTelephonyManager = null;
    }
  }
  
  public static String c(Context paramContext)
  {
    Object localObject1;
    String str;
    Object localObject3;
    try
    {
      localObject1 = new org/json/JSONArray;
      ((JSONArray)localObject1).<init>();
      int i = Build.VERSION.SDK_INT;
      int j = 22;
      if (i >= j)
      {
        Object localObject2 = SubscriptionManager.from(paramContext);
        localObject2 = ((SubscriptionManager)localObject2).getActiveSubscriptionInfoList();
        localObject4 = ((List)localObject2).iterator();
        boolean bool;
        for (;;)
        {
          bool = ((Iterator)localObject4).hasNext();
          if (!bool) {
            break;
          }
          localObject2 = ((Iterator)localObject4).next();
          localObject2 = (SubscriptionInfo)localObject2;
          try
          {
            localObject5 = new org/json/JSONObject;
            ((JSONObject)localObject5).<init>();
            str = "slotId";
            int k = ((SubscriptionInfo)localObject2).getSimSlotIndex();
            ((JSONObject)localObject5).put(str, k);
            str = "subscriptionId";
            k = ((SubscriptionInfo)localObject2).getSubscriptionId();
            ((JSONObject)localObject5).put(str, k);
            str = "displayName";
            Object localObject6 = ((SubscriptionInfo)localObject2).getDisplayName();
            ((JSONObject)localObject5).put(str, localObject6);
            str = "carrierName";
            localObject6 = ((SubscriptionInfo)localObject2).getCarrierName();
            ((JSONObject)localObject5).put(str, localObject6);
            str = "phoneNumber";
            localObject6 = ((SubscriptionInfo)localObject2).getNumber();
            ((JSONObject)localObject5).put(str, localObject6);
            str = "simId";
            localObject2 = ((SubscriptionInfo)localObject2).getIccId();
            ((JSONObject)localObject5).put(str, localObject2);
            ((JSONArray)localObject1).put(localObject5);
          }
          catch (Exception localException1)
          {
            localObject5 = "SimUtil";
            str = "Exception getting sim details for SDK >= 22";
            a.a((String)localObject5, str, localException1);
          }
        }
        return (String)localObject3;
      }
    }
    catch (Exception localException2)
    {
      localObject1 = "Not able to fetch Sim Cards";
      a.b("SimUtil", (String)localObject1);
      bool = false;
      localObject3 = null;
    }
    for (;;)
    {
      localObject3 = "phone";
      localObject3 = paramContext.getSystemService((String)localObject3);
      localObject3 = (TelephonyManager)localObject3;
      if (localObject3 != null) {}
      try
      {
        localObject4 = new org/json/JSONObject;
        ((JSONObject)localObject4).<init>();
        localObject5 = "slotId";
        int m = ((TelephonyManager)localObject3).getSimState();
        ((JSONObject)localObject4).put((String)localObject5, m);
        localObject5 = "subscriptionId";
        str = ((TelephonyManager)localObject3).getSubscriberId();
        ((JSONObject)localObject4).put((String)localObject5, str);
        localObject5 = "displayName";
        str = ((TelephonyManager)localObject3).getNetworkOperator();
        ((JSONObject)localObject4).put((String)localObject5, str);
        localObject5 = "carrierName";
        str = ((TelephonyManager)localObject3).getNetworkOperatorName();
        ((JSONObject)localObject4).put((String)localObject5, str);
        localObject5 = "phoneNumber";
        str = ((TelephonyManager)localObject3).getLine1Number();
        ((JSONObject)localObject4).put((String)localObject5, str);
        localObject5 = "simId";
        localObject3 = ((TelephonyManager)localObject3).getSimSerialNumber();
        ((JSONObject)localObject4).put((String)localObject5, localObject3);
        ((JSONArray)localObject1).put(localObject4);
      }
      catch (Exception localException3)
      {
        for (;;)
        {
          localObject4 = "SimUtil";
          localObject5 = "Exception getting sim details for SDK < 22";
          a.a((String)localObject4, (String)localObject5, localException3);
        }
      }
      localObject3 = ((JSONArray)localObject1).toString();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */