package in.org.npci.upiapp.core;

import android.app.Activity;
import android.graphics.Bitmap;
import android.text.TextUtils;
import in.juspay.widget.qrscanner.com.google.zxing.c;
import in.juspay.widget.qrscanner.com.google.zxing.f;
import in.juspay.widget.qrscanner.com.google.zxing.g;
import in.juspay.widget.qrscanner.com.google.zxing.i;
import in.juspay.widget.qrscanner.com.google.zxing.k;

public class QRScannerInterface
{
  private static final String a = QRScannerInterface.class.getSimpleName();
  private Activity b;
  private in.juspay.mystique.d c;
  private in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.d d;
  private in.juspay.widget.qrscanner.com.google.zxing.a.a.b e;
  private String f;
  
  public QRScannerInterface(Activity paramActivity, in.juspay.mystique.d paramd)
  {
    b = paramActivity;
    c = paramd;
  }
  
  private boolean b()
  {
    Activity localActivity = b;
    String str = "android.permission.CAMERA";
    int i = android.support.v4.a.a.a(localActivity, str);
    if (i == 0) {
      i = 1;
    }
    for (;;)
    {
      return i;
      int j = 0;
      localActivity = null;
    }
  }
  
  public String a(Bitmap paramBitmap)
  {
    i = paramBitmap.getWidth();
    int j = paramBitmap.getHeight();
    i *= j;
    localObject1 = new int[i];
    int k = paramBitmap.getWidth();
    int m = paramBitmap.getWidth();
    int n = paramBitmap.getHeight();
    Object localObject2 = paramBitmap;
    paramBitmap.getPixels((int[])localObject1, 0, k, 0, 0, m, n);
    localObject2 = new in/juspay/widget/qrscanner/com/google/zxing/i;
    int i1 = paramBitmap.getWidth();
    k = paramBitmap.getHeight();
    ((i)localObject2).<init>(i1, k, (int[])localObject1);
    localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/c;
    localObject4 = new in/juspay/widget/qrscanner/com/google/zxing/common/j;
    ((in.juspay.widget.qrscanner.com.google.zxing.common.j)localObject4).<init>((f)localObject2);
    ((c)localObject1).<init>((in.juspay.widget.qrscanner.com.google.zxing.b)localObject4);
    localObject2 = new in/juspay/widget/qrscanner/com/google/zxing/g;
    ((g)localObject2).<init>();
    try
    {
      localObject2 = ((in.juspay.widget.qrscanner.com.google.zxing.j)localObject2).a((c)localObject1);
      localObject2 = ((k)localObject2).a();
      localObject1 = c;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject4 = ((StringBuilder)localObject4).append("window.BarcodeResult(\"").append((String)localObject2);
      str = "\");";
      localObject4 = str;
      ((in.juspay.mystique.d)localObject1).a((String)localObject4);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localObject1 = c;
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        String str = "window.BarcodeResult(\"";
        Object localObject3 = ((StringBuilder)localObject4).append(str).append("upi://pay?pa=invalid&pn=invalid image&tn=&am=00&cu=INR");
        localObject4 = "\");";
        localObject3 = (String)localObject4;
        ((in.juspay.mystique.d)localObject1).a((String)localObject3);
        i = 0;
        localObject3 = null;
      }
    }
    return (String)localObject2;
  }
  
  public void a()
  {
    Object localObject1 = d;
    if (localObject1 != null)
    {
      localObject1 = d;
      ((in.juspay.widget.qrscanner.com.journeyapps.barcodescanner.d)localObject1).d();
    }
    localObject1 = f;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    Object localObject2;
    if (!bool)
    {
      localObject1 = f;
      int i = Integer.parseInt((String)localObject1);
      localObject2 = b;
      QRScannerInterface.3 local3 = new in/org/npci/upiapp/core/QRScannerInterface$3;
      local3.<init>(this, i);
      ((Activity)localObject2).runOnUiThread(local3);
    }
    for (;;)
    {
      return;
      localObject1 = a;
      localObject2 = "ERROR: Frame ID null!!";
      in.org.npci.upiapp.a.a.b((String)localObject1, (String)localObject2);
    }
  }
  
  public void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    Object localObject = c;
    String str;
    if (localObject == null)
    {
      localObject = a;
      str = "ERROR: Empty Dynamic UI!!";
      in.org.npci.upiapp.a.a.b((String)localObject, str);
    }
    for (;;)
    {
      return;
      switch (paramInt)
      {
      default: 
        break;
      case 101: 
        boolean bool = b();
        if (bool)
        {
          localObject = a;
          str = "Camera Access Permission Granted.";
          in.org.npci.upiapp.a.a.a((String)localObject, str);
          a();
        }
        else
        {
          localObject = a;
          str = "ERROR: Camera Access Permission Not Granted!!";
          in.org.npci.upiapp.a.a.b((String)localObject, str);
        }
        break;
      }
    }
  }
  
  public void captureManager(String paramString)
  {
    Activity localActivity = b;
    QRScannerInterface.2 local2 = new in/org/npci/upiapp/core/QRScannerInterface$2;
    local2.<init>(this, paramString);
    localActivity.runOnUiThread(local2);
  }
  
  public void closeQRScanner()
  {
    Object localObject1 = a;
    Object localObject2 = "JSInterface closeQRScanner Called";
    in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2);
    localObject1 = d;
    if (localObject1 != null)
    {
      in.org.npci.upiapp.a.a.a(a, "JSInterface closeQRScanner 2 Called");
      localObject1 = b;
      localObject2 = new in/org/npci/upiapp/core/QRScannerInterface$1;
      ((QRScannerInterface.1)localObject2).<init>(this);
      ((Activity)localObject1).runOnUiThread((Runnable)localObject2);
    }
    for (;;)
    {
      return;
      in.org.npci.upiapp.a.a.a(a, "JSInterface closeQRScanner 3 Called");
      localObject1 = a;
      localObject2 = "ERROR: CaptureManager NULL!!";
      in.org.npci.upiapp.a.a.b((String)localObject1, (String)localObject2);
    }
  }
  
  public void openQRScanner(String paramString)
  {
    String str1 = a;
    String str2 = "JSInterface openQRScanner Called";
    in.org.npci.upiapp.a.a.a(str1, str2);
    f = paramString;
    boolean bool = b();
    if (!bool)
    {
      str1 = a;
      str2 = "Requesting for Camera Access Permission.";
      in.org.npci.upiapp.a.a.a(str1, str2);
    }
    for (;;)
    {
      return;
      a();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/QRScannerInterface.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */