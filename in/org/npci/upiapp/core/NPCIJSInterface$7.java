package in.org.npci.upiapp.core;

import android.app.Activity;
import android.content.Context;
import android.os.IBinder;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

class NPCIJSInterface$7
  implements Runnable
{
  NPCIJSInterface$7(NPCIJSInterface paramNPCIJSInterface) {}
  
  public void run()
  {
    Object localObject = NPCIJSInterface.b(a).getCurrentFocus();
    if (localObject != null)
    {
      localObject = (InputMethodManager)NPCIJSInterface.b(a).getApplicationContext().getSystemService("input_method");
      IBinder localIBinder = NPCIJSInterface.b(a).getCurrentFocus().getWindowToken();
      ((InputMethodManager)localObject).hideSoftInputFromWindow(localIBinder, 0);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/NPCIJSInterface$7.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */