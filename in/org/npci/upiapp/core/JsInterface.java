package in.org.npci.upiapp.core;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.app.Activity;
import android.app.Application;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.IBinder;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.Profile;
import android.provider.MediaStore.Images.Media;
import android.text.TextUtils.TruncateAt;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import in.juspay.mystique.d;
import in.org.npci.upiapp.HomeActivity;
import in.org.npci.upiapp.utils.c;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.zip.GZIPInputStream;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONArray;
import org.json.JSONObject;

public class JsInterface
{
  private static final String c = JsInterface.class.getName();
  Cursor a;
  private c b;
  private Context d;
  private Activity e;
  private d f;
  private BroadcastReceiver g;
  private BroadcastReceiver h;
  private final int i;
  private String j;
  private DatePickerDialog k;
  private final String[] l;
  private int m;
  
  public JsInterface(Activity paramActivity, d paramd)
  {
    i = n;
    m = 88;
    a = null;
    Object localObject = paramActivity.getApplicationContext();
    d = ((Context)localObject);
    e = paramActivity;
    f = paramd;
    localObject = new in/org/npci/upiapp/utils/c;
    Context localContext = d;
    ((c)localObject).<init>(localContext);
    b = ((c)localObject);
    localObject = new String[8];
    localObject[0] = "/sbin/";
    localObject[n] = "/system/bin/";
    localObject[2] = "/system/xbin/";
    localObject[3] = "/data/local/xbin/";
    localObject[4] = "/data/local/bin/";
    localObject[5] = "/system/sd/xbin/";
    localObject[6] = "/system/bin/failsafe/";
    localObject[7] = "/data/local/";
    l = ((String[])localObject);
  }
  
  private byte[] a(byte[] paramArrayOfByte)
  {
    int n = 1024;
    byte[] arrayOfByte1 = new byte[n];
    Object localObject1;
    Object localObject2;
    GZIPInputStream localGZIPInputStream;
    byte[] arrayOfByte2;
    try
    {
      localObject1 = new java/io/ByteArrayInputStream;
      ((ByteArrayInputStream)localObject1).<init>(paramArrayOfByte);
      localObject2 = new java/io/ByteArrayOutputStream;
      ((ByteArrayOutputStream)localObject2).<init>();
      localGZIPInputStream = new java/util/zip/GZIPInputStream;
      int i1 = 1024;
      localGZIPInputStream.<init>((InputStream)localObject1, i1);
      for (;;)
      {
        i1 = localGZIPInputStream.read(arrayOfByte1);
        int i2 = -1;
        if (i1 == i2) {
          break;
        }
        i2 = 0;
        ((ByteArrayOutputStream)localObject2).write(arrayOfByte1, 0, i1);
      }
      return arrayOfByte2;
    }
    catch (IOException localIOException)
    {
      localObject1 = c;
      localObject2 = "Exception while gunzipping - ";
      in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2, localIOException);
      n = 0;
      arrayOfByte2 = null;
    }
    for (;;)
    {
      arrayOfByte2 = ((ByteArrayOutputStream)localObject2).toByteArray();
      localGZIPInputStream.close();
      ((ByteArrayInputStream)localObject1).close();
      ((ByteArrayOutputStream)localObject2).close();
    }
  }
  
  private String b(byte[] paramArrayOfByte)
  {
    int n = 9;
    int i1 = 8;
    i2 = 0;
    String str1 = null;
    int i3 = 8;
    int i4 = 8;
    try
    {
      byte[] arrayOfByte1 = new byte[i4];
      i4 = paramArrayOfByte.length - i1;
      byte[] arrayOfByte2 = new byte[i4];
      int i5 = paramArrayOfByte.length;
      i4 = 0;
      localObject2 = null;
      int i6 = 9;
      i6 = paramArrayOfByte[i6];
      arrayOfByte1[0] = i6;
      i4 = 1;
      i6 = 19;
      i6 = paramArrayOfByte[i6];
      arrayOfByte1[i4] = i6;
      i4 = 2;
      i6 = 29;
      i6 = paramArrayOfByte[i6];
      arrayOfByte1[i4] = i6;
      i4 = 3;
      i6 = 39;
      i6 = paramArrayOfByte[i6];
      arrayOfByte1[i4] = i6;
      i4 = 4;
      i6 = 49;
      i6 = paramArrayOfByte[i6];
      arrayOfByte1[i4] = i6;
      i4 = 5;
      i6 = 59;
      i6 = paramArrayOfByte[i6];
      arrayOfByte1[i4] = i6;
      i4 = 6;
      i6 = 69;
      i6 = paramArrayOfByte[i6];
      arrayOfByte1[i4] = i6;
      i4 = 7;
      i6 = 79;
      i6 = paramArrayOfByte[i6];
      arrayOfByte1[i4] = i6;
      i6 = 0;
      str2 = null;
      i4 = 0;
      localObject2 = null;
      if (i6 < i5)
      {
        int i7;
        if (i6 > 0)
        {
          i7 = i6 % 10;
          if ((i7 == n) && (i4 < i1)) {
            i4 += 1;
          }
        }
        for (;;)
        {
          i6 += 1;
          break;
          i7 = paramArrayOfByte[i6];
          int i8 = i2 % i3;
          i8 = arrayOfByte1[i8];
          i7 = (byte)(i7 ^ i8);
          arrayOfByte2[i2] = i7;
          i2 += 1;
        }
      }
      str1 = new java/lang/String;
      localObject2 = a(arrayOfByte2);
      str1.<init>((byte[])localObject2);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = c;
        String str2 = "Exception while decrypting - ";
        in.org.npci.upiapp.a.a.a((String)localObject2, str2, localException);
        i2 = 0;
        Object localObject1 = null;
      }
    }
    return str1;
  }
  
  private boolean b()
  {
    Object localObject = com.google.android.gms.common.b.a();
    Activity localActivity1 = e;
    int n = ((com.google.android.gms.common.b)localObject).a(localActivity1);
    boolean bool2;
    if (n != 0)
    {
      boolean bool1 = ((com.google.android.gms.common.b)localObject).a(n);
      if (bool1)
      {
        Activity localActivity2 = e;
        int i1 = 9000;
        localObject = ((com.google.android.gms.common.b)localObject).a(localActivity2, n, i1);
        ((Dialog)localObject).show();
      }
      bool2 = false;
      localObject = null;
    }
    for (;;)
    {
      return bool2;
      bool2 = true;
    }
  }
  
  private void c(String paramString)
  {
    if (paramString != null)
    {
      boolean bool = paramString.isEmpty();
      if (!bool)
      {
        int n = 1;
        Object localObject = new Object[n];
        localObject[0] = paramString;
        String str = String.format("window.callJSCallback('%s');", (Object[])localObject);
        localObject = f;
        ((d)localObject).a(str);
      }
    }
  }
  
  public static String decrypt(String paramString1, String paramString2)
  {
    try
    {
      str1 = in.org.npci.upiapp.utils.a.a(paramString1, paramString2);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1;
        String str2 = c;
        String str3 = "exception while decrypting token";
        in.org.npci.upiapp.a.a.a(str2, str3, localException);
        Object localObject = null;
      }
    }
    return str1;
  }
  
  public static String decryptAes(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      str1 = in.org.npci.upiapp.utils.a.b(paramString1, paramString2, paramString3);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1;
        String str2 = c;
        String str3 = "exception while decrypting AES token";
        in.org.npci.upiapp.a.a.a(str2, str3, localException);
        Object localObject = null;
      }
    }
    return str1;
  }
  
  public static String encryptAes(String paramString1, String paramString2, String paramString3)
  {
    try
    {
      str1 = in.org.npci.upiapp.utils.a.a(paramString1, paramString2, paramString3);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1;
        String str2 = c;
        String str3 = "exception while encrypting AES token";
        in.org.npci.upiapp.a.a.a(str2, str3, localException);
        Object localObject = null;
      }
    }
    return str1;
  }
  
  public int AndroidVersion()
  {
    return Build.VERSION.SDK_INT;
  }
  
  public int a(float paramFloat)
  {
    return (int)(e.getResources().getDisplayMetrics().densityDpi / 160 * paramFloat);
  }
  
  public String a(String paramString)
  {
    byte[] arrayOfByte = Base64.decode(paramString, 0);
    String str = new java/lang/String;
    str.<init>(arrayOfByte, "UTF-8");
    return str;
  }
  
  public void a(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    Object localObject1 = c;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = ((StringBuilder)localObject2).append("Permission Granted - ");
    Object localObject3 = String.valueOf(paramInt);
    localObject2 = (String)localObject3;
    in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2);
    localObject1 = f;
    if (localObject1 == null)
    {
      localObject1 = c;
      localObject2 = "DUI is null";
      in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2);
    }
    for (;;)
    {
      return;
      switch (paramInt)
      {
      default: 
        break;
      case 1: 
        for (;;)
        {
          int n;
          try
          {
            localObject2 = new org/json/JSONObject;
            ((JSONObject)localObject2).<init>();
            n = 0;
            localObject1 = null;
            int i1 = paramArrayOfInt.length;
            if (n >= i1) {
              break label511;
            }
            i1 = paramArrayOfInt[n];
            int i2 = -1;
            if (i1 == i2)
            {
              localObject3 = e;
              str1 = paramArrayOfString[n];
              boolean bool = ((Activity)localObject3).shouldShowRequestPermissionRationale(str1);
              if (bool)
              {
                localObject3 = paramArrayOfString[n];
                str1 = "DENIED";
                ((JSONObject)localObject2).put((String)localObject3, str1);
                localObject3 = f.a();
                str1 = "PERMISSION";
                localObject4 = new java/lang/StringBuilder;
                ((StringBuilder)localObject4).<init>();
                str2 = paramArrayOfString[n];
                localObject4 = ((StringBuilder)localObject4).append(str2);
                str2 = "_DENIED";
                localObject4 = ((StringBuilder)localObject4).append(str2);
                localObject4 = ((StringBuilder)localObject4).toString();
                ((f)localObject3).fabricLogInfo(str1, (String)localObject4);
                n += 1;
                continue;
              }
              localObject3 = paramArrayOfString[n];
              str1 = "EVERDENIED";
              ((JSONObject)localObject2).put((String)localObject3, str1);
              localObject3 = f.a();
              str1 = "PERMISSION";
              localObject4 = new java/lang/StringBuilder;
              ((StringBuilder)localObject4).<init>();
              str2 = paramArrayOfString[n];
              localObject4 = ((StringBuilder)localObject4).append(str2);
              str2 = "_EVERDENIED";
              localObject4 = ((StringBuilder)localObject4).append(str2);
              localObject4 = ((StringBuilder)localObject4).toString();
              ((f)localObject3).fabricLogInfo(str1, (String)localObject4);
              continue;
            }
          }
          catch (Exception localException)
          {
            locald = f;
            localObject2 = new java/lang/StringBuilder;
            ((StringBuilder)localObject2).<init>();
            localObject2 = ((StringBuilder)localObject2).append("window.callUICallback(\"");
            localObject3 = j;
            localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
            localObject3 = "\", \"ERROR\");";
            localObject2 = (String)localObject3;
            locald.a((String)localObject2);
          }
          localObject3 = paramArrayOfString[n];
          str1 = "GRANTED";
          ((JSONObject)localObject2).put((String)localObject3, str1);
          localObject3 = f.a();
          str1 = "PERMISSION";
          Object localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          String str2 = paramArrayOfString[n];
          localObject4 = ((StringBuilder)localObject4).append(str2);
          str2 = "_GRANTED";
          localObject4 = ((StringBuilder)localObject4).append(str2);
          localObject4 = ((StringBuilder)localObject4).toString();
          ((f)localObject3).fabricLogInfo(str1, (String)localObject4);
        }
        label511:
        d locald = f;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        String str1 = "window.callUICallback(\"";
        localObject3 = ((StringBuilder)localObject3).append(str1);
        str1 = j;
        localObject3 = ((StringBuilder)localObject3).append(str1);
        str1 = "\", ";
        localObject3 = ((StringBuilder)localObject3).append(str1);
        localObject2 = ((JSONObject)localObject2).toString();
        localObject2 = ((StringBuilder)localObject3).append((String)localObject2);
        localObject3 = ");";
        localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
        localObject2 = ((StringBuilder)localObject2).toString();
        locald.a((String)localObject2);
      }
    }
  }
  
  public void addItemsToListView(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4)
  {
    int n = Integer.parseInt(paramString1);
    ListView localListView = (ListView)e.findViewById(n);
    Object localObject = new org/json/JSONArray;
    ((JSONArray)localObject).<init>(paramString2);
    ArrayList localArrayList1 = in.org.npci.upiapp.utils.f.a((JSONArray)localObject, "view", "String");
    ArrayList localArrayList2 = in.org.npci.upiapp.utils.f.a((JSONArray)localObject, "value", "String");
    ArrayList localArrayList3 = in.org.npci.upiapp.utils.f.a((JSONArray)localObject, "viewType", "Int");
    Activity localActivity = e;
    localObject = new in/org/npci/upiapp/core/JsInterface$16;
    ((JsInterface.16)localObject).<init>(this, localListView, paramInt, localArrayList2, localArrayList1, localArrayList3, paramString4);
    localActivity.runOnUiThread((Runnable)localObject);
  }
  
  public String b(String paramString)
  {
    return paramString.split("\\?")[0].split("#")[0].replaceAll("[^a-zA-Z0-9.]", "_");
  }
  
  public String baseCheck(String paramString)
  {
    return Base64.encodeToString(paramString.getBytes(), 0);
  }
  
  public void calender(String paramString)
  {
    Activity localActivity = e;
    JsInterface.6 local6 = new in/org/npci/upiapp/core/JsInterface$6;
    local6.<init>(this, paramString);
    localActivity.runOnUiThread(local6);
  }
  
  public void calenderTransaction(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Activity localActivity = e;
    JsInterface.7 local7 = new in/org/npci/upiapp/core/JsInterface$7;
    local7.<init>(this, paramString1, paramString4, paramString2, paramString3);
    localActivity.runOnUiThread(local7);
  }
  
  public void callAPI(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean, String paramString5)
  {
    String str1 = a(paramString4);
    String str2 = a(paramString3);
    JsInterface.20 local20 = new in/org/npci/upiapp/core/JsInterface$20;
    Object localObject = this;
    local20.<init>(this, paramString5, paramString2, paramString1, str2, str1, paramBoolean);
    localObject = new Object[0];
    local20.execute((Object[])localObject);
  }
  
  public void checkPermission(String[] paramArrayOfString, String paramString)
  {
    j = paramString;
    int n = Build.VERSION.SDK_INT;
    int i1 = 23;
    Object localObject1;
    Object localObject2;
    if (n >= i1)
    {
      localObject1 = e;
      localObject2 = new in/org/npci/upiapp/core/JsInterface$19;
      ((JsInterface.19)localObject2).<init>(this, paramArrayOfString);
      ((Activity)localObject1).runOnUiThread((Runnable)localObject2);
    }
    for (;;)
    {
      return;
      try
      {
        localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>();
        localObject2 = "STATUS";
        localObject3 = "SUCCESS";
        ((JSONObject)localObject1).put((String)localObject2, localObject3);
        localObject2 = f;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        String str = "window.callUICallback(\"";
        localObject3 = ((StringBuilder)localObject3).append(str);
        str = j;
        localObject3 = ((StringBuilder)localObject3).append(str);
        str = "\", ";
        localObject3 = ((StringBuilder)localObject3).append(str);
        localObject1 = ((JSONObject)localObject1).toString();
        localObject1 = ((StringBuilder)localObject3).append((String)localObject1);
        localObject3 = ");";
        localObject1 = ((StringBuilder)localObject1).append((String)localObject3);
        localObject1 = ((StringBuilder)localObject1).toString();
        ((d)localObject2).a((String)localObject1);
      }
      catch (Exception localException)
      {
        d locald = f;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("window.callUICallback(\"");
        Object localObject3 = j;
        localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
        localObject3 = "\", \"ERROR\");";
        localObject2 = (String)localObject3;
        locald.a((String)localObject2);
      }
    }
  }
  
  public void clearPreferences()
  {
    c localc = b;
    Activity localActivity = e;
    localc.a(localActivity);
  }
  
  public void closeApp()
  {
    e.finish();
  }
  
  public void copyFile(String paramString1, String paramString2)
  {
    FileInputStream localFileInputStream = new java/io/FileInputStream;
    localFileInputStream.<init>(paramString2);
    FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
    localFileOutputStream.<init>(paramString1);
    int n = 1024;
    byte[] arrayOfByte = new byte[n];
    for (;;)
    {
      int i1 = localFileInputStream.read(arrayOfByte);
      int i2 = -1;
      if (i1 == i2) {
        break;
      }
      i2 = 0;
      localFileOutputStream.write(arrayOfByte, 0, i1);
    }
    localFileInputStream.close();
    localFileOutputStream.flush();
    localFileOutputStream.close();
  }
  
  public String decryptAndloadFile(String paramString)
  {
    String str1 = c;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str4 = "Processing File - ";
    localObject = str4 + paramString;
    in.org.npci.upiapp.a.a.a(str1, (String)localObject);
    str1 = null;
    for (;;)
    {
      try
      {
        localObject = e;
        str4 = "galileo";
        localObject = in.org.npci.upiapp.utils.b.a((Context)localObject, paramString, str4);
        str4 = ".jsa";
        boolean bool = paramString.endsWith(str4);
        if ((!bool) || (localObject == null)) {}
      }
      catch (Exception localException2)
      {
        String str2;
        String str3 = "";
        continue;
      }
      try
      {
        str1 = new java/lang/String;
        localObject = b((byte[])localObject);
        str1.<init>((String)localObject);
      }
      catch (Exception localException1)
      {
        str2 = "";
      }
    }
    return str1;
  }
  
  public void deleteQrCode(String paramString)
  {
    removeInternalFilePath(paramString);
  }
  
  public void downloadFile(String paramString1, boolean paramBoolean, String paramString2)
  {
    Object localObject1 = c;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = "Download File - " + paramString1;
    in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2);
    localObject1 = new in/org/npci/upiapp/core/JsInterface$12;
    ((JsInterface.12)localObject1).<init>(this, paramString1, paramBoolean, paramString2);
    localObject2 = new Object[0];
    ((JsInterface.12)localObject1).execute((Object[])localObject2);
  }
  
  public String getAppName()
  {
    return e.getString(2131165239);
  }
  
  public String getAppNameFromPackage(String paramString)
  {
    PackageManager localPackageManager = d.getPackageManager();
    Object localObject = null;
    try
    {
      localObject = localPackageManager.getApplicationInfo(paramString, 0);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        String str = null;
        continue;
        str = "(unknown)";
      }
    }
    if (localObject != null)
    {
      localObject = localPackageManager.getApplicationLabel((ApplicationInfo)localObject);
      return (String)localObject;
    }
  }
  
  public String getApplicationDetails()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str1 = "versionCode";
    int n = 11;
    try
    {
      localJSONObject.put(str1, n);
      str1 = "versionName";
      Object localObject = "1.3";
      localJSONObject.put(str1, localObject);
      str1 = "applicationId";
      localObject = "in.org.npci.upiapp";
      localJSONObject.put(str1, localObject);
      str1 = "configVersion";
      localObject = b;
      String str2 = "configVersion";
      String str3 = "";
      localObject = ((c)localObject).b(str2, str3);
      localJSONObject.put(str1, localObject);
      str1 = "bundleVersion";
      localObject = b;
      str2 = "bundleVersion";
      str3 = "";
      localObject = ((c)localObject).b(str2, str3);
      localJSONObject.put(str1, localObject);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        in.org.npci.upiapp.a.a.a(localException);
      }
    }
    return localJSONObject.toString();
  }
  
  public String getAppsWithDrawOverOthers()
  {
    PackageManager localPackageManager = d.getPackageManager();
    int n = 4096;
    Object localObject1 = localPackageManager.getInstalledPackages(n);
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    Iterator localIterator = ((List)localObject1).iterator();
    Object localObject2;
    do
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (PackageInfo)localIterator.next();
      localObject2 = requestedPermissions;
    } while (localObject2 == null);
    String[] arrayOfString = requestedPermissions;
    int i1 = arrayOfString.length;
    int i2 = 0;
    label84:
    Object localObject3;
    int i3;
    if (i2 < i1)
    {
      localObject2 = arrayOfString[i2];
      localObject3 = "android.permission.SYSTEM_ALERT_WINDOW";
      boolean bool2 = ((String)localObject2).equalsIgnoreCase((String)localObject3);
      if (bool2)
      {
        localObject3 = packageName;
        i3 = localPackageManager.checkPermission((String)localObject2, (String)localObject3);
        if (i3 != 0) {
          break label396;
        }
        i3 = 1;
      }
    }
    for (;;)
    {
      try
      {
        localObject3 = new org/json/JSONObject;
        ((JSONObject)localObject3).<init>();
        localObject4 = "package";
        Object localObject5 = packageName;
        ((JSONObject)localObject3).put((String)localObject4, localObject5);
        localObject4 = "name";
        localObject5 = packageName;
        localObject5 = getAppNameFromPackage((String)localObject5);
        ((JSONObject)localObject3).put((String)localObject4, localObject5);
        localObject4 = "targetSdkVersion";
        localObject5 = applicationInfo;
        int i5 = targetSdkVersion;
        ((JSONObject)localObject3).put((String)localObject4, i5);
        localObject4 = "available";
        ((JSONObject)localObject3).put((String)localObject4, i3);
        localJSONArray.put(localObject3);
        localObject3 = c;
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "Permission matched for package = [";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject5 = packageName;
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject5 = "], [";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject5 = applicationInfo;
        i5 = targetSdkVersion;
        localObject4 = ((StringBuilder)localObject4).append(i5);
        localObject5 = "], [";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject2 = ((StringBuilder)localObject4).append(i3);
        localObject4 = "]";
        localObject2 = ((StringBuilder)localObject2).append((String)localObject4);
        localObject2 = ((StringBuilder)localObject2).toString();
        in.org.npci.upiapp.a.a.a((String)localObject3, (String)localObject2);
      }
      catch (Exception localException)
      {
        int i4;
        label396:
        localObject3 = c;
        Object localObject4 = "Exception in function getAppsWithDrawOverOthers";
        in.org.npci.upiapp.a.a.a((String)localObject3, (String)localObject4, localException);
        continue;
      }
      i4 = i2 + 1;
      i2 = i4;
      break label84;
      break;
      i4 = 0;
      localObject2 = null;
    }
    return localJSONArray.toString();
  }
  
  public String getDeviceDetails()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    String str = "deviceId";
    try
    {
      Object localObject = e;
      localObject = in.org.npci.upiapp.utils.f.a((Context)localObject);
      localJSONObject.put(str, localObject);
      str = "os";
      localObject = "ANDROID";
      localJSONObject.put(str, localObject);
      str = "version";
      localObject = Build.VERSION.RELEASE;
      localJSONObject.put(str, localObject);
      str = "manufacturer";
      localObject = Build.MANUFACTURER;
      localJSONObject.put(str, localObject);
      str = "model";
      localObject = Build.MODEL;
      localJSONObject.put(str, localObject);
      str = "packageName";
      localObject = e;
      localObject = ((Activity)localObject).getPackageName();
      localJSONObject.put(str, localObject);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        in.org.npci.upiapp.a.a.a(localException);
      }
    }
    return localJSONObject.toString();
  }
  
  public String getDeviceId()
  {
    try
    {
      localObject1 = e;
      localObject1 = in.org.npci.upiapp.utils.f.a((Context)localObject1);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        String str1 = c;
        String str2 = "Error getting deviceId";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  public File getExternalFilePath(String paramString)
  {
    Object localObject1 = new android/content/ContextWrapper;
    Object localObject2 = e;
    ((ContextWrapper)localObject1).<init>((Context)localObject2);
    ((ContextWrapper)localObject1).getExternalFilesDir("imageDir");
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    localObject1 = ((StringBuilder)localObject1).append(localObject2);
    localObject2 = getAppName();
    localObject1 = (String)localObject2;
    localObject2 = new java/io/File;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localObject1 = (String)localObject1 + paramString;
    ((File)localObject2).<init>((String)localObject1);
    return (File)localObject2;
  }
  
  public String getFileFromInternalStorage(String paramString)
  {
    String str1 = b(paramString);
    try
    {
      localObject3 = d;
      localObject3 = in.org.npci.upiapp.utils.b.b(str1, (Context)localObject3);
      str1 = new java/lang/String;
      str1.<init>((byte[])localObject3);
    }
    catch (RuntimeException localRuntimeException)
    {
      for (;;)
      {
        localObject3 = c;
        str2 = "Exception while loading bitmap file";
        in.org.npci.upiapp.a.a.a((String)localObject3, str2, localRuntimeException);
        Object localObject1 = null;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject3 = c;
        String str2 = "Exception while loading bitmap file";
        in.org.npci.upiapp.a.a.a((String)localObject3, str2, localException);
        Object localObject2 = null;
      }
    }
    return str1;
  }
  
  public void getGCMToken()
  {
    Activity localActivity = e;
    JsInterface.1 local1 = new in/org/npci/upiapp/core/JsInterface$1;
    local1.<init>(this);
    localActivity.runOnUiThread(local1);
  }
  
  public String getHMACDigest(String paramString1, String paramString2, String paramString3)
  {
    String str1 = null;
    try
    {
      Object localObject1 = new javax/crypto/spec/SecretKeySpec;
      localObject2 = "UTF-8";
      localObject2 = paramString2.getBytes((String)localObject2);
      ((SecretKeySpec)localObject1).<init>((byte[])localObject2, paramString3);
      localObject2 = Mac.getInstance(paramString3);
      ((Mac)localObject2).init((Key)localObject1);
      localObject1 = "ASCII";
      localObject1 = paramString1.getBytes((String)localObject1);
      localObject1 = ((Mac)localObject2).doFinal((byte[])localObject1);
      int n = 2;
      str1 = Base64.encodeToString((byte[])localObject1, n);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      for (;;)
      {
        localObject2 = c;
        str2 = "Error encoding hmac ";
        in.org.npci.upiapp.a.a.a((String)localObject2, str2, localUnsupportedEncodingException);
      }
    }
    catch (InvalidKeyException localInvalidKeyException)
    {
      for (;;)
      {
        localObject2 = c;
        str2 = "Invalid HMAC key ";
        in.org.npci.upiapp.a.a.a((String)localObject2, str2, localInvalidKeyException);
      }
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      for (;;)
      {
        Object localObject2 = c;
        String str2 = "NoSuchAlgorithmException ";
        in.org.npci.upiapp.a.a.a((String)localObject2, str2, localNoSuchAlgorithmException);
      }
    }
    return str1;
  }
  
  public String getInstalledAccessiblityServices()
  {
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    int n = Build.VERSION.SDK_INT;
    int i1 = 14;
    if (n >= i1)
    {
      Object localObject1 = ((AccessibilityManager)d.getSystemService("accessibility")).getInstalledAccessibilityServiceList();
      Iterator localIterator = ((List)localObject1).iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (AccessibilityServiceInfo)localIterator.next();
        try
        {
          localObject2 = new org/json/JSONObject;
          ((JSONObject)localObject2).<init>();
          str = "package";
          String[] arrayOfString = packageNames;
          ((JSONObject)localObject2).put(str, arrayOfString);
          str = "name";
          localObject1 = ((AccessibilityServiceInfo)localObject1).getId();
          ((JSONObject)localObject2).put(str, localObject1);
          localJSONArray.put(localObject2);
        }
        catch (Exception localException)
        {
          Object localObject2 = c;
          String str = "Exception in function getInstalledAccessiblityServices";
          in.org.npci.upiapp.a.a.a((String)localObject2, str, localException);
        }
      }
    }
    return localJSONArray.toString();
  }
  
  public String getIntentData()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    localJSONObject.<init>();
    Object localObject = e;
    if (localObject != null)
    {
      localObject = e.getIntent();
      if (localObject != null)
      {
        localObject = ((HomeActivity)e).k();
        if (localObject == null) {
          break label77;
        }
        str1 = "type";
        str2 = "GCM";
      }
    }
    for (;;)
    {
      try
      {
        localJSONObject.put(str1, str2);
        str1 = "data";
        localJSONObject.put(str1, localObject);
      }
      catch (Exception localException)
      {
        label77:
        str1 = c;
        str2 = "Exception";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
        continue;
      }
      return localJSONObject.toString();
      localObject = e;
      localObject = (HomeActivity)localObject;
      localObject = ((HomeActivity)localObject).l();
      if (localObject != null)
      {
        str1 = "type";
        str2 = "MERCHANT";
        localJSONObject.put(str1, str2);
        str1 = "data";
        localJSONObject.put(str1, localObject);
      }
    }
  }
  
  public File getInternalFilePath(String paramString)
  {
    Object localObject1 = new android/content/ContextWrapper;
    Object localObject2 = e;
    ((ContextWrapper)localObject1).<init>((Context)localObject2);
    localObject1 = ((ContextWrapper)localObject1).getDir("imageDir", 0);
    localObject2 = new java/io/File;
    ((File)localObject2).<init>((File)localObject1, paramString);
    return (File)localObject2;
  }
  
  public String getKey(String paramString1, String paramString2)
  {
    return b.b(paramString1, paramString2);
  }
  
  public String getMD5(String paramString)
  {
    Object localObject1 = "MD5";
    try
    {
      localObject1 = MessageDigest.getInstance((String)localObject1);
      byte[] arrayOfByte = paramString.getBytes();
      arrayOfByte = ((MessageDigest)localObject1).digest(arrayOfByte);
      StringBuffer localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      n = 0;
      localObject1 = null;
      for (;;)
      {
        int i1 = arrayOfByte.length;
        if (n >= i1) {
          break;
        }
        i1 = arrayOfByte[n] & 0xFF | 0x100;
        String str = Integer.toHexString(i1);
        int i2 = 1;
        int i3 = 3;
        str = str.substring(i2, i3);
        localStringBuffer.append(str);
        n += 1;
      }
      localObject1 = localStringBuffer.toString();
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      for (;;)
      {
        int n = 0;
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  public String getPackageName()
  {
    try
    {
      localObject1 = e;
      localObject1 = ((Activity)localObject1).getPackageName();
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        String str1 = c;
        String str2 = "Error getting package name";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  public String getResource(String paramString)
  {
    Object localObject1 = e.getResources();
    Object localObject2 = "string";
    String str = e.getPackageName();
    int n = ((Resources)localObject1).getIdentifier(paramString, (String)localObject2, str);
    if (n != 0) {
      localObject2 = e.getResources();
    }
    for (localObject1 = ((Resources)localObject2).getString(n);; localObject1 = null)
    {
      return (String)localObject1;
      n = 0;
    }
  }
  
  public String getResource(String paramString1, String paramString2)
  {
    Object localObject1 = e.getResources();
    Object localObject2 = e.getPackageName();
    int n = ((Resources)localObject1).getIdentifier(paramString1, paramString2, (String)localObject2);
    if (n != 0) {
      localObject2 = e.getResources();
    }
    for (localObject1 = ((Resources)localObject2).getString(n);; localObject1 = null)
    {
      return (String)localObject1;
      n = 0;
    }
  }
  
  public String getSIMOperators()
  {
    return e.c(e);
  }
  
  public String getSessionId()
  {
    return in.juspay.tracker.e.a().b();
  }
  
  public String getSha256MessageDigestInHex(String paramString)
  {
    return in.org.npci.upiapp.utils.a.b(paramString);
  }
  
  public String getUUID()
  {
    return UUID.randomUUID().toString();
  }
  
  public String getWebviewUserAgentString()
  {
    return HomeActivity.m;
  }
  
  public String getsymbols(String paramString)
  {
    int n = -1;
    int i1 = paramString.hashCode();
    String str1;
    switch (i1)
    {
    default: 
      switch (n)
      {
      default: 
        str1 = "symbol";
      }
      break;
    }
    for (;;)
    {
      return str1;
      String str2 = "tick";
      boolean bool = paramString.equals(str2);
      if (!bool) {
        break;
      }
      n = 0;
      str1 = null;
      break;
      str2 = "rupee";
      bool = paramString.equals(str2);
      if (!bool) {
        break;
      }
      n = 1;
      break;
      str1 = "✓";
      continue;
      str1 = "₹";
    }
  }
  
  public void hideKeyboard()
  {
    Object localObject = e.getCurrentFocus();
    if (localObject != null)
    {
      localObject = (InputMethodManager)e.getApplicationContext().getSystemService("input_method");
      IBinder localIBinder = e.getCurrentFocus().getWindowToken();
      ((InputMethodManager)localObject).hideSoftInputFromWindow(localIBinder, 0);
    }
  }
  
  public boolean isDeviceRooted()
  {
    Object localObject1 = Build.TAGS;
    String str1;
    boolean bool;
    if (localObject1 != null)
    {
      str1 = "test-keys";
      bool = ((String)localObject1).contains(str1);
      if (bool) {
        bool = true;
      }
    }
    label172:
    for (;;)
    {
      return bool;
      Object localObject2 = l;
      int n = localObject2.length;
      int i1 = 0;
      str1 = null;
      bool = false;
      localObject1 = null;
      for (;;)
      {
        if (i1 >= n) {
          break label172;
        }
        String str2 = localObject2[i1];
        localObject1 = new java/io/File;
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = ((StringBuilder)localObject3).append(str2);
        String str3 = "/su";
        localObject3 = str3;
        ((File)localObject1).<init>((String)localObject3);
        bool = ((File)localObject1).exists();
        if (bool)
        {
          str1 = c;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          localObject2 = ((StringBuilder)localObject2).append(str2);
          String str4 = " contains su binary";
          localObject2 = str4;
          in.org.npci.upiapp.a.a.a(str1, (String)localObject2, false);
          break;
        }
        i1 += 1;
      }
    }
  }
  
  public boolean isDualSIM()
  {
    return e.a(e);
  }
  
  public boolean isExternalStorageReadable()
  {
    String str1 = Environment.getExternalStorageState();
    String str2 = "mounted";
    boolean bool1 = str2.equals(str1);
    boolean bool2;
    if (!bool1)
    {
      str2 = "mounted_ro";
      bool2 = str2.equals(str1);
      if (!bool2) {}
    }
    else
    {
      bool2 = true;
    }
    for (;;)
    {
      return bool2;
      bool2 = false;
      str1 = null;
    }
  }
  
  public boolean isExternalStorageWritable()
  {
    String str = Environment.getExternalStorageState();
    return "mounted".equals(str);
  }
  
  public boolean isNetworkAvailable()
  {
    Object localObject = e;
    String str = "connectivity";
    localObject = ((ConnectivityManager)((Activity)localObject).getSystemService(str)).getActiveNetworkInfo();
    boolean bool;
    if (localObject != null)
    {
      bool = ((NetworkInfo)localObject).isConnected();
      if (bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  public boolean isPermissionGranted(String[] paramArrayOfString)
  {
    int n = paramArrayOfString.length;
    boolean bool = true;
    int i1 = 0;
    if (i1 < n)
    {
      String str = paramArrayOfString[i1];
      Activity localActivity = e;
      int i2 = android.support.v4.a.a.a(localActivity, str);
      if (i2 != 0) {
        bool &= false;
      }
      for (;;)
      {
        i1 += 1;
        break;
        bool &= true;
      }
    }
    return bool;
  }
  
  public boolean isSimSupport()
  {
    return e.b(e);
  }
  
  public void language_change(String paramString)
  {
    Activity localActivity = e;
    JsInterface.13 local13 = new in/org/npci/upiapp/core/JsInterface$13;
    local13.<init>(this, paramString);
    localActivity.runOnUiThread(local13);
  }
  
  public void listViewAdapter(String paramString1, String paramString2, String paramString3, int paramInt, String paramString4, String paramString5)
  {
    int n = Integer.parseInt(paramString1);
    Object localObject1 = e.findViewById(n);
    Object localObject2 = localObject1;
    localObject2 = (ListView)localObject1;
    localObject1 = new org/json/JSONArray;
    ((JSONArray)localObject1).<init>(paramString2);
    Object localObject3 = in.org.npci.upiapp.utils.f.a((JSONArray)localObject1, "view", "String");
    Object localObject4 = in.org.npci.upiapp.utils.f.a((JSONArray)localObject1, "value", "String");
    Object localObject5 = in.org.npci.upiapp.utils.f.a((JSONArray)localObject1, "viewType", "Int");
    localObject1 = new in/org/npci/upiapp/a;
    Object localObject6 = d;
    Object localObject7 = f;
    Object localObject8 = paramString3;
    ((in.org.npci.upiapp.a)localObject1).<init>((Context)localObject6, paramInt, (ArrayList)localObject4, (ArrayList)localObject3, (ArrayList)localObject5, (d)localObject7, paramString3);
    localObject8 = e;
    localObject6 = new in/org/npci/upiapp/core/JsInterface$15;
    localObject4 = paramString5;
    localObject3 = localObject1;
    localObject5 = localObject2;
    localObject7 = paramString4;
    ((JsInterface.15)localObject6).<init>(this, paramString5, (in.org.npci.upiapp.a)localObject1, (ListView)localObject2, paramString4);
    ((Activity)localObject8).runOnUiThread((Runnable)localObject6);
  }
  
  public String loadFile(String paramString)
  {
    for (;;)
    {
      try
      {
        localObject = e;
        arrayOfByte = in.org.npci.upiapp.utils.b.a((Context)localObject, paramString);
        if (arrayOfByte != null) {
          continue;
        }
        localObject = "";
      }
      catch (Exception localException)
      {
        Object localObject;
        byte[] arrayOfByte;
        String str = "";
        continue;
      }
      return (String)localObject;
      localObject = new java/lang/String;
      ((String)localObject).<init>(arrayOfByte);
    }
  }
  
  public void loadImageForQr()
  {
    ((HomeActivity)e).m().closeQRScanner();
    Intent localIntent = new android/content/Intent;
    Uri localUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    localIntent.<init>("android.intent.action.PICK", localUri);
    Activity localActivity = e;
    int n = m;
    localActivity.startActivityForResult(localIntent, n);
  }
  
  public void logging(String paramString)
  {
    in.org.npci.upiapp.a.a.b("Console", paramString);
  }
  
  public void minimizeApp()
  {
    hideKeyboard();
    e.moveTaskToBack(true);
  }
  
  public String name()
  {
    String str = "";
    Object localObject1 = e.getApplication().getContentResolver();
    Object localObject2 = ContactsContract.Profile.CONTENT_URI;
    localObject2 = ((ContentResolver)localObject1).query((Uri)localObject2, null, null, null, null);
    if (localObject2 != null)
    {
      ((Cursor)localObject2).moveToFirst();
      int n = ((Cursor)localObject2).getColumnIndex("display_name");
      localObject1 = ((Cursor)localObject2).getString(n);
      ((Cursor)localObject2).close();
    }
    for (;;)
    {
      return (String)localObject1;
      localObject1 = str;
    }
  }
  
  public void nope(String paramString, int paramInt1, int paramInt2)
  {
    Activity localActivity = e;
    JsInterface.9 local9 = new in/org/npci/upiapp/core/JsInterface$9;
    local9.<init>(this, paramString, paramInt1, paramInt2);
    localActivity.runOnUiThread(local9);
  }
  
  public void openGallery(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.setAction("android.intent.action.VIEW");
    Uri localUri = Uri.parse(paramString);
    localIntent.setDataAndType(localUri, "image/*");
    e.startActivity(localIntent);
  }
  
  public void pulse(String paramString, float paramFloat, int paramInt)
  {
    Activity localActivity = e;
    JsInterface.10 local10 = new in/org/npci/upiapp/core/JsInterface$10;
    local10.<init>(this, paramString, paramFloat, paramInt);
    localActivity.runOnUiThread(local10);
  }
  
  public String readContact(int paramInt)
  {
    int n = 0;
    Object localObject1 = null;
    JSONArray localJSONArray = new org/json/JSONArray;
    localJSONArray.<init>();
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    Object localObject2 = a;
    Object localObject3;
    int i1;
    String str2;
    if (localObject2 == null)
    {
      localObject2 = d.getContentResolver();
      localObject3 = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
      String str1 = "display_name ASC";
      i1 = 0;
      str2 = null;
      localObject2 = ((ContentResolver)localObject2).query((Uri)localObject3, null, null, null, str1);
      a = ((Cursor)localObject2);
    }
    a.moveToFirst();
    int i2 = 0;
    localObject2 = null;
    while (i2 < paramInt)
    {
      localObject3 = a;
      ((Cursor)localObject3).moveToNext();
      i2 += 1;
    }
    for (;;)
    {
      localObject2 = a;
      boolean bool2 = ((Cursor)localObject2).moveToNext();
      if (bool2) {}
      try
      {
        localObject2 = a;
        localObject3 = a;
        localObject1 = "display_name";
        int i3 = ((Cursor)localObject3).getColumnIndex((String)localObject1);
        localObject2 = ((Cursor)localObject2).getString(i3);
        localObject3 = a;
        localObject1 = a;
        str2 = "data1";
        n = ((Cursor)localObject1).getColumnIndex(str2);
        localObject3 = ((Cursor)localObject3).getString(n);
        if (localObject3 == null) {
          continue;
        }
        if (localObject2 == null) {
          localObject2 = "Missing Name";
        }
        localObject3 = ((String)localObject3).replace("-", "").replace(")", "").replace("(", "").replace(" ", "");
        localObject1 = "+";
        str2 = "";
        localObject3 = ((String)localObject3).replace((CharSequence)localObject1, str2);
        n = ((String)localObject3).length();
        i1 = 10;
        if (n < i1) {
          continue;
        }
        n = ((String)localObject3).length() + -10;
        i1 = ((String)localObject3).length();
        localObject3 = ((String)localObject3).substring(n, i1);
        boolean bool1 = localArrayList.contains(localObject3);
        if (bool1) {
          continue;
        }
        localArrayList.add(localObject3);
        localObject1 = new org/json/JSONObject;
        ((JSONObject)localObject1).<init>();
        ((JSONObject)localObject1).put("nickName", localObject2);
        ((JSONObject)localObject1).put("vpa", localObject3);
        str2 = "MOBILE";
        ((JSONObject)localObject1).put("type", str2);
        localObject3 = "registeredName";
        ((JSONObject)localObject1).put((String)localObject3, localObject2);
        localJSONArray.put(localObject1);
      }
      catch (Exception localException) {}
      return localJSONArray.toString();
    }
  }
  
  public void removeExternalFilePath(String paramString)
  {
    Object localObject1 = new android/content/ContextWrapper;
    Object localObject2 = e;
    ((ContextWrapper)localObject1).<init>((Context)localObject2);
    ((ContextWrapper)localObject1).getExternalFilesDir("imageDir");
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
    localObject1 = ((StringBuilder)localObject1).append(localObject2);
    localObject2 = getAppName();
    localObject1 = (String)localObject2;
    localObject2 = new java/io/File;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localObject1 = (String)localObject1 + paramString;
    ((File)localObject2).<init>((String)localObject1);
    ((File)localObject2).delete();
  }
  
  public void removeInternalFilePath(String paramString)
  {
    Object localObject1 = new android/content/ContextWrapper;
    Object localObject2 = e;
    ((ContextWrapper)localObject1).<init>((Context)localObject2);
    localObject1 = ((ContextWrapper)localObject1).getDir("imageDir", 0);
    localObject2 = new java/io/File;
    ((File)localObject2).<init>((File)localObject1, paramString);
    ((File)localObject2).delete();
  }
  
  public void removeKey(String paramString)
  {
    b.a(paramString);
  }
  
  public void restart()
  {
    Object localObject = e.getBaseContext().getPackageManager();
    String str = e.getBaseContext().getPackageName();
    localObject = ((PackageManager)localObject).getLaunchIntentForPackage(str);
    ((Intent)localObject).addFlags(67108864);
    e.startActivity((Intent)localObject);
  }
  
  public void saveFileToInternalStorage(String paramString1, String paramString2)
  {
    Object localObject1;
    if (paramString1 != null)
    {
      int n = paramString1.length();
      if ((n != 0) && (paramString2 != null)) {
        localObject1 = b(paramString1);
      }
    }
    for (;;)
    {
      try
      {
        localObject2 = new java/io/File;
        localObject3 = d;
        String str2 = "juspay";
        localObject3 = ((Context)localObject3).getDir(str2, 0);
        ((File)localObject2).<init>((File)localObject3, (String)localObject1);
        localObject1 = new java/io/FileOutputStream;
        ((FileOutputStream)localObject1).<init>((File)localObject2);
        localObject2 = paramString2.getBytes();
        ((FileOutputStream)localObject1).write((byte[])localObject2);
        ((FileOutputStream)localObject1).flush();
        ((FileOutputStream)localObject1).close();
        return;
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        localObject2 = c;
        localObject3 = "File not found ";
        in.org.npci.upiapp.a.a.a((String)localObject2, (String)localObject3, localFileNotFoundException);
        continue;
      }
      catch (IOException localIOException)
      {
        localObject2 = c;
        Object localObject3 = "IO exception ";
        in.org.npci.upiapp.a.a.a((String)localObject2, (String)localObject3, localIOException);
        continue;
      }
      String str1 = c;
      Object localObject2 = "data missing. Not saving file";
      in.org.npci.upiapp.a.a.a(str1, (String)localObject2);
    }
  }
  
  public String saveHTMLCode(String paramString1, String paramString2)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = Environment.getExternalStorageDirectory();
    localObject1 = ((StringBuilder)localObject1).append(localObject2);
    localObject2 = File.separator;
    localObject1 = (String)localObject2 + "bhimTransactions";
    localObject2 = new java/io/File;
    ((File)localObject2).<init>((String)localObject1);
    ((File)localObject2).mkdirs();
    localObject1 = new java/io/File;
    ((File)localObject1).<init>((File)localObject2, paramString1);
    try
    {
      ((File)localObject1).createNewFile();
      localObject2 = new java/io/FileOutputStream;
      ((FileOutputStream)localObject2).<init>((File)localObject1);
      OutputStreamWriter localOutputStreamWriter = new java/io/OutputStreamWriter;
      localOutputStreamWriter.<init>((OutputStream)localObject2);
      localOutputStreamWriter.append(paramString2);
      localOutputStreamWriter.close();
      ((FileOutputStream)localObject2).flush();
      ((FileOutputStream)localObject2).close();
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
    return ((File)localObject1).getAbsolutePath();
  }
  
  public void saveToClipboard(String paramString)
  {
    int n = Build.VERSION.SDK_INT;
    int i1 = 11;
    Object localObject1;
    Object localObject2;
    if (n < i1)
    {
      localObject1 = e;
      localObject2 = "clipboard";
      localObject1 = (android.text.ClipboardManager)((Activity)localObject1).getSystemService((String)localObject2);
      ((android.text.ClipboardManager)localObject1).setText(paramString);
    }
    for (;;)
    {
      return;
      localObject1 = (android.content.ClipboardManager)e.getSystemService("clipboard");
      localObject2 = ClipData.newPlainText("Copied Text", paramString);
      ((android.content.ClipboardManager)localObject1).setPrimaryClip((ClipData)localObject2);
    }
  }
  
  public File saveToExternalStorage(String paramString1, String paramString2)
  {
    try
    {
      localObject1 = new android/content/ContextWrapper;
      localObject3 = e;
      ((ContextWrapper)localObject1).<init>((Context)localObject3);
      localObject3 = "imageDir";
      ((ContextWrapper)localObject1).getExternalFilesDir((String)localObject3);
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject3 = Environment.DIRECTORY_PICTURES;
      localObject3 = Environment.getExternalStoragePublicDirectory((String)localObject3);
      localObject1 = ((StringBuilder)localObject1).append(localObject3);
      localObject3 = getAppName();
      localObject1 = ((StringBuilder)localObject1).append((String)localObject3);
      localObject1 = ((StringBuilder)localObject1).toString();
      localObject3 = new java/io/File;
      ((File)localObject3).<init>((String)localObject1);
      bool = ((File)localObject3).exists();
      if (!bool) {
        ((File)localObject3).mkdirs();
      }
      localObject1 = new java/io/File;
      str = ".nomedia";
      ((File)localObject1).<init>((File)localObject3, str);
      ((File)localObject1).createNewFile();
      localObject1 = new java/io/File;
      ((File)localObject1).<init>((File)localObject3, paramString1);
      localObject3 = ((File)localObject1).getAbsolutePath();
      copyFile((String)localObject3, paramString2);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject3 = c;
        String str = "Exception when saving to external storage";
        in.org.npci.upiapp.a.a.a((String)localObject3, str, localException);
        boolean bool = false;
        Object localObject2 = null;
      }
    }
    return (File)localObject1;
  }
  
  public void sendMerchantResponseIntent(String paramString)
  {
    Activity localActivity = e;
    JsInterface.21 local21 = new in/org/npci/upiapp/core/JsInterface$21;
    local21.<init>(this, paramString);
    localActivity.runOnUiThread(local21);
  }
  
  public void sendSMS(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    Object localObject1 = new in/org/npci/upiapp/core/JsInterface$17;
    ((JsInterface.17)localObject1).<init>(this, paramString4);
    g = ((BroadcastReceiver)localObject1);
    localObject1 = new in/org/npci/upiapp/core/JsInterface$18;
    ((JsInterface.18)localObject1).<init>(this);
    h = ((BroadcastReceiver)localObject1);
    localObject1 = e;
    Object localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>("SMS_SENT");
    PendingIntent localPendingIntent1 = PendingIntent.getBroadcast((Context)localObject1, 0, (Intent)localObject2, 0);
    localObject1 = e;
    localObject2 = new android/content/Intent;
    ((Intent)localObject2).<init>("SMS_DELIVERED");
    PendingIntent localPendingIntent2 = PendingIntent.getBroadcast((Context)localObject1, 0, (Intent)localObject2, 0);
    localObject1 = e;
    localObject2 = g;
    Object localObject3 = new android/content/IntentFilter;
    ((IntentFilter)localObject3).<init>("SMS_SENT");
    ((Activity)localObject1).registerReceiver((BroadcastReceiver)localObject2, (IntentFilter)localObject3);
    localObject1 = e;
    localObject2 = h;
    localObject3 = new android/content/IntentFilter;
    ((IntentFilter)localObject3).<init>("SMS_DELIVERED");
    ((Activity)localObject1).registerReceiver((BroadcastReceiver)localObject2, (IntentFilter)localObject3);
    localObject1 = e;
    int n = Integer.parseInt(paramString1);
    localObject3 = paramString2;
    e.a((Context)localObject1, n, paramString2, null, paramString3, localPendingIntent1, localPendingIntent2);
  }
  
  public void sendSmsIntent(String paramString1, String paramString2, String paramString3)
  {
    Activity localActivity = e;
    JsInterface.11 local11 = new in/org/npci/upiapp/core/JsInterface$11;
    local11.<init>(this, paramString1, paramString2, paramString3);
    localActivity.runOnUiThread(local11);
  }
  
  public void setClickFeedback(String paramString)
  {
    Activity localActivity = e;
    JsInterface.4 local4 = new in/org/npci/upiapp/core/JsInterface$4;
    local4.<init>(this, paramString);
    localActivity.runOnUiThread(local4);
  }
  
  public void setEllipsize(int paramInt)
  {
    TextView localTextView = (TextView)e.findViewById(paramInt);
    TextUtils.TruncateAt localTruncateAt = TextUtils.TruncateAt.END;
    localTextView.setEllipsize(localTruncateAt);
  }
  
  public void setKey(String paramString1, String paramString2)
  {
    b.a(paramString1, paramString2);
  }
  
  public void setSwitch(String paramString1, String paramString2)
  {
    try
    {
      int n = Integer.parseInt(paramString1);
      localObject1 = e;
      Object localObject2 = ((Activity)localObject1).findViewById(n);
      localObject2 = (Switch)localObject2;
      localObject1 = new in/org/npci/upiapp/core/JsInterface$14;
      ((JsInterface.14)localObject1).<init>(this, paramString2);
      ((Switch)localObject2).setOnCheckedChangeListener((CompoundButton.OnCheckedChangeListener)localObject1);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1 = c;
        String str = "Exception in Set Switch";
        in.org.npci.upiapp.a.a.a((String)localObject1, str, localException);
      }
    }
  }
  
  public void shareQrCode(String paramString1, String paramString2, String paramString3, String paramString4, boolean paramBoolean)
  {
    Activity localActivity = e;
    JsInterface.23 local23 = new in/org/npci/upiapp/core/JsInterface$23;
    local23.<init>(this, paramString2, paramBoolean, paramString1, paramString4, paramString3);
    localActivity.runOnUiThread(local23);
  }
  
  public void shareReferalCode(String paramString1, String paramString2, boolean paramBoolean)
  {
    Activity localActivity = e;
    JsInterface.2 local2 = new in/org/npci/upiapp/core/JsInterface$2;
    local2.<init>(this, paramString2, paramString1);
    localActivity.runOnUiThread(local2);
  }
  
  public void shoutOut(String paramString)
  {
    String str = c;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = "Shoutout - " + paramString;
    in.org.npci.upiapp.a.a.a(str, (String)localObject);
  }
  
  public void showKeyboard()
  {
    Object localObject = e.getApplicationContext();
    String str = "input_method";
    localObject = (InputMethodManager)((Context)localObject).getSystemService(str);
    if (localObject != null)
    {
      int n = 1;
      ((InputMethodManager)localObject).toggleSoftInput(n, 0);
    }
  }
  
  public void showQrCode(String paramString1, String paramString2, String paramString3, boolean paramBoolean)
  {
    Activity localActivity = e;
    JsInterface.22 local22 = new in/org/npci/upiapp/core/JsInterface$22;
    local22.<init>(this, paramString3, paramString2, paramBoolean, paramString1);
    localActivity.runOnUiThread(local22);
  }
  
  public void startBrowserIntent(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.VIEW");
    Uri localUri = Uri.parse(paramString);
    localIntent.setData(localUri);
    e.startActivity(localIntent);
  }
  
  public void startCallIntent(String paramString1, String paramString2)
  {
    Activity localActivity = e;
    JsInterface.5 local5 = new in/org/npci/upiapp/core/JsInterface$5;
    local5.<init>(this, paramString1, paramString2);
    localActivity.runOnUiThread(local5);
  }
  
  public void startDialIntent(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.DIAL");
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = Uri.parse("tel:" + paramString);
    localIntent.setData((Uri)localObject);
    e.startActivity(localIntent);
  }
  
  public void startInstalledAppDetailsActivity()
  {
    Activity localActivity = e;
    Intent localIntent = new android/content/Intent;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = ((StringBuilder)localObject).append("package:");
    String str = e.getPackageName();
    localObject = Uri.parse(str);
    localIntent.<init>("android.settings.APPLICATION_DETAILS_SETTINGS", (Uri)localObject);
    localActivity.startActivity(localIntent);
  }
  
  public void tada(String paramString, float paramFloat, int paramInt)
  {
    Activity localActivity = e;
    JsInterface.8 local8 = new in/org/npci/upiapp/core/JsInterface$8;
    local8.<init>(this, paramString, paramFloat, paramInt);
    localActivity.runOnUiThread(local8);
  }
  
  public void unregisterReceiver()
  {
    Object localObject = g;
    if (localObject != null)
    {
      localObject = h;
      if (localObject != null)
      {
        localObject = e;
        BroadcastReceiver localBroadcastReceiver = g;
        ((Activity)localObject).unregisterReceiver(localBroadcastReceiver);
        localObject = e;
        localBroadcastReceiver = h;
        ((Activity)localObject).unregisterReceiver(localBroadcastReceiver);
      }
    }
  }
  
  public void viewHtml(String paramString)
  {
    int n = 1;
    int i1 = paramString.length();
    Object localObject1 = paramString.substring(n, i1);
    Object localObject2 = new java/io/File;
    ((File)localObject2).<init>((String)localObject1);
    Uri.fromFile((File)localObject2);
    localObject1 = new android/content/Intent;
    String str = "android.intent.action.VIEW";
    ((Intent)localObject1).<init>(str);
    try
    {
      localObject2 = Uri.fromFile((File)localObject2);
      str = "text/html";
      ((Intent)localObject1).setDataAndType((Uri)localObject2, str);
      localObject2 = e;
      str = "Open with";
      localObject1 = Intent.createChooser((Intent)localObject1, str);
      ((Activity)localObject2).startActivity((Intent)localObject1);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public String viewToImage(String paramString1, String paramString2, String paramString3)
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = null;
    Activity localActivity = e;
    JsInterface.3 local3 = new in/org/npci/upiapp/core/JsInterface$3;
    local3.<init>(this, paramString1, arrayOfString, paramString2, paramString3);
    localActivity.runOnUiThread(local3);
    return arrayOfString[0];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/JsInterface.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */