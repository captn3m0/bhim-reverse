package in.org.npci.upiapp.core;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import in.juspay.mystique.d;
import in.org.npci.upiapp.a.a;
import java.util.Iterator;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

class NPCIJSInterface$5
  extends ResultReceiver
{
  NPCIJSInterface$5(NPCIJSInterface paramNPCIJSInterface, Handler paramHandler, String paramString)
  {
    super(paramHandler);
  }
  
  protected void onReceiveResult(int paramInt, Bundle paramBundle)
  {
    Object localObject1 = "NPCIJSInterface";
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    Object localObject4 = "ResultCode is ";
    localObject3 = (String)localObject4 + paramInt;
    a.a((String)localObject1, (String)localObject3);
    localObject3 = new org/json/JSONObject;
    ((JSONObject)localObject3).<init>();
    Object localObject5;
    try
    {
      super.onReceiveResult(paramInt, paramBundle);
      if (paramBundle != null)
      {
        localObject1 = paramBundle.keySet();
        localObject4 = ((Set)localObject1).iterator();
        for (;;)
        {
          boolean bool = ((Iterator)localObject4).hasNext();
          if (!bool) {
            break;
          }
          localObject1 = ((Iterator)localObject4).next();
          localObject1 = (String)localObject1;
          try
          {
            localObject5 = b;
            Object localObject6 = paramBundle.get((String)localObject1);
            localObject5 = NPCIJSInterface.a((NPCIJSInterface)localObject5, localObject6);
            ((JSONObject)localObject3).put((String)localObject1, localObject5);
          }
          catch (JSONException localJSONException)
          {
            a.a(localJSONException);
          }
        }
      }
      localObject2 = NPCIJSInterface.a(b);
    }
    catch (Exception localException1)
    {
      a.a(localException1);
    }
    for (;;)
    {
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject4 = ((StringBuilder)localObject4).append("window.callUICallback(\"");
      localObject5 = a;
      localObject4 = ((StringBuilder)localObject4).append((String)localObject5).append("\", ");
      localObject3 = ((JSONObject)localObject3).toString();
      localObject3 = (String)localObject3 + ");";
      ((d)localObject2).a((String)localObject3);
      return;
      Object localObject2 = "resultCode";
      try
      {
        ((JSONObject)localObject3).put((String)localObject2, paramInt);
      }
      catch (Exception localException2)
      {
        a.a(localException2);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/NPCIJSInterface$5.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */