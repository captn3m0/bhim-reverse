package in.org.npci.upiapp.core;

import android.app.Activity;
import in.juspay.mystique.d;
import in.org.npci.upiapp.HomeActivity;
import org.json.JSONObject;

public class a
{
  private static final String a = JsInterface.class.getName();
  private final Activity b;
  private d c;
  private JSONObject d;
  
  public a(Activity paramActivity, d paramd)
  {
    b = paramActivity;
    c = paramd;
  }
  
  public JSONObject getOtpRules()
  {
    return d;
  }
  
  public void onOtpReceived()
  {
    String str = b.b().h();
    d locald = c;
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    str = "__ROOTSCREEN.onOtpReceived('" + str + "');";
    locald.a(str);
  }
  
  public void setOtpRules(String paramString)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      d = localJSONObject;
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1 = "OTPJsInterface";
        String str2 = "Exception in setOtpRules";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
      }
    }
  }
  
  public void setSessionStartForOtp(String paramString)
  {
    HomeActivity.f(paramString);
  }
  
  public void startOtpAutoRead(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    HomeActivity.b(paramString1);
    HomeActivity.c(paramString2);
    HomeActivity.d(paramString3);
    HomeActivity.e(paramString4);
    b localb = b.b();
    Activity localActivity = b;
    localb.a(localActivity, this);
  }
  
  public void stopOtpAutoRead()
  {
    b.b().c();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */