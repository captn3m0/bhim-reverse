package in.org.npci.upiapp.core;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.app.Activity;
import android.util.Property;
import android.view.View;

class JsInterface$8
  implements Runnable
{
  JsInterface$8(JsInterface paramJsInterface, String paramString, float paramFloat, int paramInt) {}
  
  public void run()
  {
    float f1 = -3.0F;
    float f2 = 1.0F;
    float f3 = 0.9F;
    float f4 = 1.1F;
    Object localObject1 = JsInterface.b(d);
    int i = Integer.parseInt(a);
    localObject1 = ((Activity)localObject1).findViewById(i);
    Object localObject2 = View.SCALE_X;
    Object localObject3 = new Keyframe[11];
    Object localObject4 = Keyframe.ofFloat(0.0F, f2);
    localObject3[0] = localObject4;
    localObject4 = Keyframe.ofFloat(0.1F, f3);
    localObject3[1] = localObject4;
    localObject4 = Keyframe.ofFloat(0.2F, f3);
    localObject3[2] = localObject4;
    localObject4 = Keyframe.ofFloat(0.3F, f4);
    localObject3[3] = localObject4;
    localObject4 = Keyframe.ofFloat(0.4F, f4);
    localObject3[4] = localObject4;
    localObject4 = Keyframe.ofFloat(0.5F, f4);
    localObject3[5] = localObject4;
    localObject4 = Keyframe.ofFloat(0.6F, f4);
    localObject3[6] = localObject4;
    localObject4 = Keyframe.ofFloat(0.7F, f4);
    localObject3[7] = localObject4;
    localObject4 = Keyframe.ofFloat(0.8F, f4);
    localObject3[8] = localObject4;
    localObject4 = Keyframe.ofFloat(f3, f4);
    localObject3[9] = localObject4;
    localObject4 = Keyframe.ofFloat(f2, f2);
    localObject3[10] = localObject4;
    localObject2 = PropertyValuesHolder.ofKeyframe((Property)localObject2, (Keyframe[])localObject3);
    localObject3 = View.SCALE_Y;
    Object localObject5 = new Keyframe[11];
    Keyframe localKeyframe1 = Keyframe.ofFloat(0.0F, f2);
    localObject5[0] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(0.1F, f3);
    localObject5[1] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(0.2F, f3);
    localObject5[2] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(0.3F, f4);
    localObject5[3] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(0.4F, f4);
    localObject5[4] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(0.5F, f4);
    localObject5[5] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(0.6F, f4);
    localObject5[6] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(0.7F, f4);
    localObject5[7] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(0.8F, f4);
    localObject5[8] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(f3, f4);
    localObject5[9] = localKeyframe1;
    localKeyframe1 = Keyframe.ofFloat(f2, f2);
    localObject5[10] = localKeyframe1;
    localObject3 = PropertyValuesHolder.ofKeyframe((Property)localObject3, (Keyframe[])localObject5);
    localObject5 = View.ROTATION;
    localObject4 = new Keyframe[11];
    Keyframe localKeyframe2 = Keyframe.ofFloat(0.0F, 0.0F);
    localObject4[0] = localKeyframe2;
    float f5 = b * f1;
    localKeyframe2 = Keyframe.ofFloat(0.1F, f5);
    localObject4[1] = localKeyframe2;
    f5 = b * f1;
    localKeyframe2 = Keyframe.ofFloat(0.2F, f5);
    localObject4[2] = localKeyframe2;
    float f6 = b;
    f5 = 3.0F * f6;
    localKeyframe2 = Keyframe.ofFloat(0.3F, f5);
    localObject4[3] = localKeyframe2;
    f5 = b * f1;
    localKeyframe2 = Keyframe.ofFloat(0.4F, f5);
    localObject4[4] = localKeyframe2;
    f6 = b;
    f5 = 3.0F * f6;
    localKeyframe2 = Keyframe.ofFloat(0.5F, f5);
    localObject4[5] = localKeyframe2;
    f5 = b * f1;
    localKeyframe2 = Keyframe.ofFloat(0.6F, f5);
    localObject4[6] = localKeyframe2;
    f6 = b;
    f5 = 3.0F * f6;
    localKeyframe2 = Keyframe.ofFloat(0.7F, f5);
    localObject4[7] = localKeyframe2;
    f5 = b * f1;
    localKeyframe2 = Keyframe.ofFloat(0.8F, f5);
    localObject4[8] = localKeyframe2;
    f5 = b;
    f4 = 3.0F * f5;
    localKeyframe2 = Keyframe.ofFloat(f3, f4);
    localObject4[9] = localKeyframe2;
    localKeyframe2 = Keyframe.ofFloat(f2, 0.0F);
    localObject4[10] = localKeyframe2;
    localObject5 = PropertyValuesHolder.ofKeyframe((Property)localObject5, (Keyframe[])localObject4);
    localObject4 = new PropertyValuesHolder[3];
    localObject4[0] = localObject2;
    localObject4[1] = localObject3;
    localObject4[2] = localObject5;
    localObject1 = ObjectAnimator.ofPropertyValuesHolder(localObject1, (PropertyValuesHolder[])localObject4);
    long l = c;
    ((ObjectAnimator)localObject1).setDuration(l).start();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/core/JsInterface$8.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */