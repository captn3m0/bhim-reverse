package in.org.npci.upiapp;

import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;

final class HomeActivity$3
  implements DialogInterface.OnClickListener
{
  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.VIEW");
    Uri localUri = Uri.parse("https://play.google.com/store/apps/details?id=in.org.npci.upiapp&hl=en");
    localIntent.setData(localUri);
    HomeActivity.t().startActivity(localIntent);
    HomeActivity.t().finish();
    HomeActivity.b(null);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/HomeActivity$3.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */