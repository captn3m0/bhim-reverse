package in.org.npci.upiapp.models;

public class ApiResponse
{
  byte[] data;
  int statusCode;
  
  public byte[] getData()
  {
    return data;
  }
  
  public int getStatusCode()
  {
    return statusCode;
  }
  
  public void setData(byte[] paramArrayOfByte)
  {
    data = paramArrayOfByte;
  }
  
  public void setStatusCode(int paramInt)
  {
    statusCode = paramInt;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/models/ApiResponse.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */