package in.org.npci.upiapp;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import in.juspay.mystique.d;
import in.juspay.mystique.h;
import in.juspay.mystique.j;
import java.util.ArrayList;
import org.json.JSONObject;

public class a
  extends ArrayAdapter
{
  private int a = 1;
  private ArrayList b;
  private final d c;
  private final j d;
  private ArrayList e;
  private ArrayList f;
  private final String g;
  
  public a(Context paramContext, int paramInt, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3, d paramd, String paramString)
  {
    super(paramContext, 2130968609, paramArrayList1);
    a = paramInt;
    b = paramArrayList1;
    c = paramd;
    j localj = paramd.b().a();
    d = localj;
    e = paramArrayList2;
    f = paramArrayList3;
    g = paramString;
  }
  
  private View a(ViewGroup paramViewGroup, int paramInt)
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    String str = (String)e.get(paramInt);
    localJSONObject.<init>(str);
    return d.a(localJSONObject);
  }
  
  public View a(String paramString)
  {
    try
    {
      localObject1 = d;
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
      localObject1 = ((j)localObject1).a(localJSONObject);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject2 = null;
      }
    }
    return (View)localObject1;
  }
  
  public void a(int paramInt, ArrayList paramArrayList1, ArrayList paramArrayList2, ArrayList paramArrayList3)
  {
    a = paramInt;
    b.addAll(paramArrayList1);
    e.addAll(paramArrayList2);
    f.addAll(paramArrayList3);
  }
  
  public int getItemViewType(int paramInt)
  {
    return ((Integer)f.get(paramInt)).intValue();
  }
  
  public View getView(int paramInt, View paramView, ViewGroup paramViewGroup)
  {
    if (paramView == null) {}
    for (;;)
    {
      try
      {
        localObject1 = a(paramViewGroup, paramInt);
        localObject2 = new in/org/npci/upiapp/a$a;
        ((a.a)localObject2).<init>();
        a = ((View)localObject1);
        ((View)localObject1).setTag(localObject2);
        localObject2 = ((View)localObject1).getTag();
        localObject2 = (a.a)localObject2;
        localObject4 = b;
        localObject4 = ((ArrayList)localObject4).get(paramInt);
        localObject4 = (String)localObject4;
        Object localObject5 = "ctx";
        String str = "parent";
        localObject4 = ((String)localObject4).replace((CharSequence)localObject5, str);
        localObject5 = c;
        localObject5 = ((d)localObject5).b();
        localObject2 = a;
        ((h)localObject5).a((View)localObject2, (String)localObject4);
        localObject2 = new in/org/npci/upiapp/a$1;
        ((a.1)localObject2).<init>(this, paramInt);
        ((View)localObject1).setOnClickListener((View.OnClickListener)localObject2);
        localObject2 = localObject1;
      }
      catch (Exception localException)
      {
        Object localObject2;
        Object localObject4 = "ListViewAdapter";
        localObject1 = "Exception in getView";
        in.org.npci.upiapp.a.a.a((String)localObject4, (String)localObject1, localException);
        Object localObject3 = null;
        continue;
      }
      return (View)localObject2;
      Object localObject1 = paramView;
    }
  }
  
  public int getViewTypeCount()
  {
    return a;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */