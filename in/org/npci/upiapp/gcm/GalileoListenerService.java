package in.org.npci.upiapp.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.a.i;
import android.support.v4.app.aa.c;
import android.support.v4.app.aa.d;
import android.support.v4.app.aa.p;
import in.org.npci.upiapp.HomeActivity;
import java.lang.reflect.Array;
import java.security.SecureRandom;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GalileoListenerService
  extends com.google.android.gms.gcm.a
{
  public static Object a(Object paramObject)
  {
    if (paramObject == null) {
      paramObject = JSONObject.NULL;
    }
    for (;;)
    {
      return paramObject;
      boolean bool = paramObject instanceof JSONArray;
      if (!bool)
      {
        bool = paramObject instanceof JSONObject;
        if (!bool)
        {
          Object localObject = JSONObject.NULL;
          bool = paramObject.equals(localObject);
          if (!bool)
          {
            bool = paramObject instanceof Collection;
            if (bool) {}
            try
            {
              localObject = new org/json/JSONArray;
              paramObject = (Collection)paramObject;
              ((JSONArray)localObject).<init>((Collection)paramObject);
              paramObject = localObject;
            }
            catch (Exception localException)
            {
              String str;
              paramObject = null;
            }
            localObject = paramObject.getClass();
            bool = ((Class)localObject).isArray();
            if (bool)
            {
              paramObject = b(paramObject);
            }
            else
            {
              bool = paramObject instanceof Map;
              if (bool)
              {
                localObject = new org/json/JSONObject;
                paramObject = (Map)paramObject;
                ((JSONObject)localObject).<init>((Map)paramObject);
                paramObject = localObject;
              }
              else
              {
                bool = paramObject instanceof Boolean;
                if (!bool)
                {
                  bool = paramObject instanceof Byte;
                  if (!bool)
                  {
                    bool = paramObject instanceof Character;
                    if (!bool)
                    {
                      bool = paramObject instanceof Double;
                      if (!bool)
                      {
                        bool = paramObject instanceof Float;
                        if (!bool)
                        {
                          bool = paramObject instanceof Integer;
                          if (!bool)
                          {
                            bool = paramObject instanceof Long;
                            if (!bool)
                            {
                              bool = paramObject instanceof Short;
                              if (!bool)
                              {
                                bool = paramObject instanceof String;
                                if (!bool)
                                {
                                  localObject = paramObject.getClass();
                                  localObject = ((Class)localObject).getPackage();
                                  localObject = ((Package)localObject).getName();
                                  str = "java.";
                                  bool = ((String)localObject).startsWith(str);
                                  if (bool) {
                                    paramObject = paramObject.toString();
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  private void a(JSONObject paramJSONObject)
  {
    Object localObject1 = "BHIMPreferences";
    Object localObject2 = null;
    for (;;)
    {
      Object localObject4;
      try
      {
        localObject2 = getSharedPreferences((String)localObject1, 0);
        Object localObject3;
        if (localObject2 != null)
        {
          localObject1 = "keyPairs";
          localObject3 = paramJSONObject.getJSONObject((String)localObject1);
          Iterator localIterator = ((JSONObject)localObject3).keys();
          boolean bool1 = localIterator.hasNext();
          if (bool1)
          {
            localObject1 = localIterator.next();
            localObject1 = (String)localObject1;
            localObject4 = ((JSONObject)localObject3).getString((String)localObject1);
            localObject5 = "null";
            boolean bool2 = ((String)localObject4).equals(localObject5);
            if (!bool2) {
              break label128;
            }
            localObject4 = ((SharedPreferences)localObject2).edit();
            localObject1 = ((SharedPreferences.Editor)localObject4).remove((String)localObject1);
            ((SharedPreferences.Editor)localObject1).apply();
            continue;
          }
        }
        return;
      }
      catch (Exception localException)
      {
        localObject2 = "GalileoListenerService";
        localObject3 = "Exception";
        in.org.npci.upiapp.a.a.a((String)localObject2, (String)localObject3, localException);
      }
      label128:
      Object localObject5 = ((SharedPreferences)localObject2).edit();
      SharedPreferences.Editor localEditor = ((SharedPreferences.Editor)localObject5).putString(localException, (String)localObject4);
      localEditor.apply();
    }
  }
  
  public static JSONArray b(Object paramObject)
  {
    Object localObject1 = new org/json/JSONArray;
    ((JSONArray)localObject1).<init>();
    Object localObject2 = paramObject.getClass();
    int i = ((Class)localObject2).isArray();
    if (i == 0)
    {
      localObject2 = new org/json/JSONException;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = ((StringBuilder)localObject1).append("Not a primitive array: ");
      Class localClass = paramObject.getClass();
      localObject1 = localClass;
      ((JSONException)localObject2).<init>((String)localObject1);
      throw ((Throwable)localObject2);
    }
    int k = Array.getLength(paramObject);
    i = 0;
    localObject2 = null;
    while (i < k)
    {
      Object localObject3 = a(Array.get(paramObject, i));
      ((JSONArray)localObject1).put(localObject3);
      int j;
      i += 1;
    }
    return (JSONArray)localObject1;
  }
  
  private void b(Bundle paramBundle)
  {
    Object localObject1 = "heading";
    try
    {
      localObject2 = paramBundle.getString((String)localObject1);
      localObject1 = "body";
      localObject1 = paramBundle.getString((String)localObject1);
      if (localObject1 == null) {
        localObject1 = "A new notification has arrived";
      }
      localObject3 = new java/security/SecureRandom;
      ((SecureRandom)localObject3).<init>();
      int i = 1000;
      int j = ((SecureRandom)localObject3).nextInt(i);
      Object localObject4 = new android/content/Intent;
      Object localObject5 = HomeActivity.class;
      ((Intent)localObject4).<init>(this, (Class)localObject5);
      int k = 402653184;
      ((Intent)localObject4).setFlags(k);
      localObject5 = "notification";
      Object localObject6 = d(paramBundle);
      ((Intent)localObject4).putExtra((String)localObject5, (String)localObject6);
      localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      long l = System.currentTimeMillis();
      localObject5 = ((StringBuilder)localObject5).append(l);
      localObject6 = "";
      localObject5 = ((StringBuilder)localObject5).append((String)localObject6);
      localObject5 = ((StringBuilder)localObject5).toString();
      ((Intent)localObject4).setAction((String)localObject5);
      k = 134217728;
      localObject4 = PendingIntent.getActivity(this, j, (Intent)localObject4, k);
      k = 2;
      localObject5 = RingtoneManager.getDefaultUri(k);
      localObject6 = new android/support/v4/app/aa$c;
      ((aa.c)localObject6).<init>();
      ((aa.c)localObject6).a((CharSequence)localObject2);
      ((aa.c)localObject6).b((CharSequence)localObject1);
      aa.d locald = new android/support/v4/app/aa$d;
      locald.<init>(this);
      int m = 2130837725;
      locald = locald.a(m);
      Object localObject7 = getResources();
      int n = 2130837800;
      localObject7 = BitmapFactory.decodeResource((Resources)localObject7, n);
      locald = locald.a((Bitmap)localObject7);
      localObject7 = "#1b3281";
      m = Color.parseColor((String)localObject7);
      locald = locald.b(m);
      localObject2 = locald.a((CharSequence)localObject2);
      localObject1 = ((aa.d)localObject2).b((CharSequence)localObject1);
      localObject1 = ((aa.d)localObject1).a((aa.p)localObject6);
      boolean bool = true;
      localObject1 = ((aa.d)localObject1).a(bool);
      localObject1 = ((aa.d)localObject1).a((Uri)localObject5);
      localObject2 = ((aa.d)localObject1).a((PendingIntent)localObject4);
      localObject1 = "notification";
      localObject1 = getSystemService((String)localObject1);
      localObject1 = (NotificationManager)localObject1;
      localObject2 = ((aa.d)localObject2).a();
      ((NotificationManager)localObject1).notify(j, (Notification)localObject2);
      localObject1 = new android/content/Intent;
      localObject2 = "in.org.npci.upiapp.uibroadcastreceiver";
      ((Intent)localObject1).<init>((String)localObject2);
      localObject2 = "onNotificationReceived";
      localObject3 = d(paramBundle);
      ((Intent)localObject1).putExtra((String)localObject2, (String)localObject3);
      localObject2 = i.a(this);
      ((i)localObject2).a((Intent)localObject1);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = "GalileoListenerService";
        Object localObject3 = "Exception while showing notification";
        in.org.npci.upiapp.a.a.a((String)localObject2, (String)localObject3, localException);
      }
    }
  }
  
  private void b(JSONObject paramJSONObject)
  {
    Object localObject1 = "request";
    try
    {
      localObject1 = paramJSONObject.getJSONObject((String)localObject1);
      str1 = "url";
      str1 = ((JSONObject)localObject1).getString(str1);
      localObject2 = "method";
      String str2 = "GET";
      ((JSONObject)localObject1).optString((String)localObject2, str2);
      localObject2 = "headers";
      ((JSONObject)localObject1).getJSONObject((String)localObject2);
      localObject2 = "isSignedJsa";
      str2 = "false";
      localObject1 = ((JSONObject)localObject1).optString((String)localObject2, str2);
      localObject1 = Boolean.valueOf((String)localObject1);
      boolean bool = ((Boolean)localObject1).booleanValue();
      if (str1 != null)
      {
        localObject2 = new in/org/npci/upiapp/gcm/GalileoListenerService$1;
        ((GalileoListenerService.1)localObject2).<init>(this, str1, bool);
        bool = false;
        localObject1 = null;
        localObject1 = new Object[0];
        ((GalileoListenerService.1)localObject2).execute((Object[])localObject1);
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1 = "GalileoListenerService";
        Object localObject2 = "Exception";
        in.org.npci.upiapp.a.a.a(str1, (String)localObject2, localException);
      }
    }
  }
  
  private void c(Bundle paramBundle)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      Object localObject1 = "data";
      localObject1 = paramBundle.getString((String)localObject1);
      localJSONObject.<init>((String)localObject1);
      localObject1 = "data";
      localJSONObject = localJSONObject.getJSONObject((String)localObject1);
      localObject1 = "tasks";
      localObject1 = localJSONObject.getJSONArray((String)localObject1);
      int i = 0;
      localJSONObject = null;
      int j = ((JSONArray)localObject1).length();
      Object localObject2;
      if (i < j)
      {
        localObject2 = ((JSONArray)localObject1).getJSONObject(i);
        String str1 = "action";
        str1 = ((JSONObject)localObject2).getString(str1);
        String str2 = "download";
        boolean bool1 = str1.equals(str2);
        if (bool1) {
          b((JSONObject)localObject2);
        }
        for (;;)
        {
          i += 1;
          break;
          str2 = "editSharedPref";
          boolean bool2 = str1.equals(str2);
          if (bool2) {
            a((JSONObject)localObject2);
          }
        }
      }
      return;
    }
    catch (Exception localException)
    {
      localObject1 = "GalileoListenerService";
      localObject2 = "Exception";
      in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2, localException);
    }
  }
  
  private String d(Bundle paramBundle)
  {
    boolean bool;
    if (paramBundle == null) {
      bool = false;
    }
    JSONObject localJSONObject;
    String str1;
    for (Object localObject1 = null;; str1 = localJSONObject.toString())
    {
      return (String)localObject1;
      localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localObject1 = paramBundle.keySet();
      Iterator localIterator = ((Set)localObject1).iterator();
      for (;;)
      {
        bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (String)localIterator.next();
        paramBundle.get((String)localObject1);
        try
        {
          localObject2 = paramBundle.get((String)localObject1);
          localObject2 = a(localObject2);
          localJSONObject.put((String)localObject1, localObject2);
        }
        catch (Exception localException)
        {
          Object localObject2 = "GalileoListenerService";
          String str2 = "Exception in getJson";
          in.org.npci.upiapp.a.a.a((String)localObject2, str2, localException);
        }
      }
    }
  }
  
  public void a(String paramString, Bundle paramBundle)
  {
    String str1 = paramBundle.getString("heading");
    String str2 = paramBundle.getString("body");
    if ((str1 == null) && (str2 == null)) {
      c(paramBundle);
    }
    for (;;)
    {
      return;
      b(paramBundle);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/gcm/GalileoListenerService.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */