package in.org.npci.upiapp.gcm;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.a.i;
import com.google.android.gms.gcm.b;

public class RegistrationIntentService
  extends IntentService
{
  private static final String[] a;
  
  static
  {
    String[] arrayOfString = new String[1];
    arrayOfString[0] = "allcustomers";
    a = arrayOfString;
  }
  
  public RegistrationIntentService()
  {
    super("RegistrationIntentService");
  }
  
  private void a(String paramString)
  {
    Object localObject1 = "RegistrationIntentService";
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    String str = "Token for my GCM Listener is : ";
    localObject2 = str + paramString;
    in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2);
    try
    {
      localObject1 = new android/content/Intent;
      localObject2 = "in.org.npci.upiapp.uibroadcastreceiver";
      ((Intent)localObject1).<init>((String)localObject2);
      localObject2 = "onTokenReceived";
      ((Intent)localObject1).putExtra((String)localObject2, paramString);
      localObject2 = i.a(this);
      ((i)localObject2).a((Intent)localObject1);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  private void b(String paramString)
  {
    try
    {
      b localb = b.a(this);
      String[] arrayOfString = a;
      int i = arrayOfString.length;
      int j = 0;
      while (j < i)
      {
        Object localObject = arrayOfString[j];
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        String str = "/topics/";
        localStringBuilder = localStringBuilder.append(str);
        localObject = localStringBuilder.append((String)localObject);
        localObject = ((StringBuilder)localObject).toString();
        localStringBuilder = null;
        localb.a(paramString, (String)localObject, null);
        j += 1;
      }
      return;
    }
    catch (Exception localException) {}
  }
  
  protected void onHandleIntent(Intent paramIntent)
  {
    try
    {
      Object localObject = com.google.android.gms.iid.a.b(this);
      int i = 2131165267;
      str1 = getString(i);
      str2 = "GCM";
      localObject = ((com.google.android.gms.iid.a)localObject).a(str1, str2, null);
      a((String)localObject);
      b((String)localObject);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1 = "RegistrationIntentService";
        String str2 = "Failed to complete token refresh";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/gcm/RegistrationIntentService.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */