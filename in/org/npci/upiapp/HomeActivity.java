package in.org.npci.upiapp;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.DetailedState;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.v4.a.i;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import com.crashlytics.android.Crashlytics;
import in.juspay.mystique.d;
import in.org.npci.upiapp.core.JsInterface;
import in.org.npci.upiapp.core.NPCIJSInterface;
import in.org.npci.upiapp.core.QRScannerInterface;
import in.org.npci.upiapp.gcm.RegistrationIntentService;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity
  extends android.support.v7.a.e
{
  private static Integer A;
  private static Integer B;
  private static Integer C;
  private static String D;
  public static String m;
  private static String p;
  private static HomeActivity y;
  private static Long z;
  private String E;
  private int F = 88;
  private HomeActivity.a o;
  private d q;
  private JsInterface r;
  private in.org.npci.upiapp.core.f s;
  private in.org.npci.upiapp.core.a t;
  private NPCIJSInterface u;
  private QRScannerInterface v;
  private String w;
  private String x;
  
  static
  {
    Class localClass = HomeActivity.class;
    boolean bool = localClass.desiredAssertionStatus();
    if (!bool) {
      bool = true;
    }
    for (;;)
    {
      n = bool;
      p = "";
      A = null;
      B = null;
      C = null;
      D = "unknown_bank";
      return;
      bool = false;
      localClass = null;
    }
  }
  
  private boolean A()
  {
    Object localObject = com.google.android.gms.common.b.a();
    int i = ((com.google.android.gms.common.b)localObject).a(this);
    boolean bool2;
    if (i != 0)
    {
      boolean bool1 = ((com.google.android.gms.common.b)localObject).a(i);
      if (bool1)
      {
        int j = 9000;
        localObject = ((com.google.android.gms.common.b)localObject).a(this, i, j);
        ((Dialog)localObject).show();
      }
      bool2 = false;
      localObject = null;
    }
    for (;;)
    {
      return bool2;
      bool2 = true;
    }
  }
  
  public static int a(BitmapFactory.Options paramOptions, int paramInt)
  {
    int i = outHeight;
    int j = 1;
    if (i > paramInt)
    {
      i /= 2;
      for (;;)
      {
        int k = i / j;
        if (k < paramInt) {
          break;
        }
        j *= 2;
      }
    }
    return j;
  }
  
  /* Error */
  private String a(String paramString, Bitmap paramBitmap)
  {
    // Byte code:
    //   0: new 97	android/content/ContextWrapper
    //   3: astore_3
    //   4: aload_0
    //   5: invokevirtual 101	in/org/npci/upiapp/HomeActivity:getApplicationContext	()Landroid/content/Context;
    //   8: astore 4
    //   10: aload_3
    //   11: aload 4
    //   13: invokespecial 104	android/content/ContextWrapper:<init>	(Landroid/content/Context;)V
    //   16: ldc 106
    //   18: astore 4
    //   20: aload_3
    //   21: aload 4
    //   23: iconst_0
    //   24: invokevirtual 110	android/content/ContextWrapper:getDir	(Ljava/lang/String;I)Ljava/io/File;
    //   27: astore_3
    //   28: new 112	java/io/File
    //   31: astore 5
    //   33: aload 5
    //   35: aload_3
    //   36: aload_1
    //   37: invokespecial 115	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   40: iconst_0
    //   41: istore 6
    //   43: aconst_null
    //   44: astore 7
    //   46: new 117	java/io/FileOutputStream
    //   49: astore 4
    //   51: aload 4
    //   53: aload 5
    //   55: invokespecial 120	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   58: getstatic 126	android/graphics/Bitmap$CompressFormat:PNG	Landroid/graphics/Bitmap$CompressFormat;
    //   61: astore_3
    //   62: bipush 100
    //   64: istore 6
    //   66: aload_2
    //   67: aload_3
    //   68: iload 6
    //   70: aload 4
    //   72: invokevirtual 133	android/graphics/Bitmap:compress	(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    //   75: pop
    //   76: aload 4
    //   78: ifnull +8 -> 86
    //   81: aload 4
    //   83: invokevirtual 136	java/io/FileOutputStream:close	()V
    //   86: aload 5
    //   88: invokevirtual 140	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   91: areturn
    //   92: astore_3
    //   93: ldc -114
    //   95: astore 4
    //   97: ldc -112
    //   99: astore 7
    //   101: aload 4
    //   103: aload 7
    //   105: aload_3
    //   106: invokestatic 149	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   109: goto -23 -> 86
    //   112: astore_3
    //   113: aconst_null
    //   114: astore 4
    //   116: ldc -114
    //   118: astore 7
    //   120: ldc -105
    //   122: astore 8
    //   124: aload 7
    //   126: aload 8
    //   128: aload_3
    //   129: invokestatic 149	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   132: aload 4
    //   134: ifnull -48 -> 86
    //   137: aload 4
    //   139: invokevirtual 136	java/io/FileOutputStream:close	()V
    //   142: goto -56 -> 86
    //   145: astore_3
    //   146: ldc -114
    //   148: astore 4
    //   150: ldc -112
    //   152: astore 7
    //   154: aload 4
    //   156: aload 7
    //   158: aload_3
    //   159: invokestatic 149	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   162: goto -76 -> 86
    //   165: astore_3
    //   166: aconst_null
    //   167: astore 4
    //   169: aload 4
    //   171: ifnull +8 -> 179
    //   174: aload 4
    //   176: invokevirtual 136	java/io/FileOutputStream:close	()V
    //   179: aload_3
    //   180: athrow
    //   181: astore 4
    //   183: ldc -114
    //   185: astore 7
    //   187: ldc -112
    //   189: astore 5
    //   191: aload 7
    //   193: aload 5
    //   195: aload 4
    //   197: invokestatic 149	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   200: goto -21 -> 179
    //   203: astore_3
    //   204: goto -35 -> 169
    //   207: astore_3
    //   208: goto -92 -> 116
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	211	0	this	HomeActivity
    //   0	211	1	paramString	String
    //   0	211	2	paramBitmap	Bitmap
    //   3	65	3	localObject1	Object
    //   92	14	3	localIOException1	java.io.IOException
    //   112	17	3	localException1	Exception
    //   145	14	3	localIOException2	java.io.IOException
    //   165	15	3	localObject2	Object
    //   203	1	3	localObject3	Object
    //   207	1	3	localException2	Exception
    //   8	167	4	localObject4	Object
    //   181	15	4	localIOException3	java.io.IOException
    //   31	163	5	localObject5	Object
    //   41	28	6	i	int
    //   44	148	7	str1	String
    //   122	5	8	str2	String
    // Exception table:
    //   from	to	target	type
    //   81	86	92	java/io/IOException
    //   46	49	112	java/lang/Exception
    //   53	58	112	java/lang/Exception
    //   137	142	145	java/io/IOException
    //   46	49	165	finally
    //   53	58	165	finally
    //   174	179	181	java/io/IOException
    //   58	61	203	finally
    //   70	76	203	finally
    //   128	132	203	finally
    //   58	61	207	java/lang/Exception
    //   70	76	207	java/lang/Exception
  }
  
  private void a(BroadcastReceiver paramBroadcastReceiver)
  {
    try
    {
      IntentFilter localIntentFilter = new android/content/IntentFilter;
      localIntentFilter.<init>();
      Object localObject = "in.org.npci.upiapp.uibroadcastreceiver";
      localIntentFilter.addAction((String)localObject);
      localObject = i.a(this);
      ((i)localObject).a(paramBroadcastReceiver, localIntentFilter);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  private void b(BroadcastReceiver paramBroadcastReceiver)
  {
    try
    {
      i locali = i.a(this);
      locali.a(paramBroadcastReceiver);
      return;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  private void b(Bundle paramBundle)
  {
    int i = 2131558501;
    Object localObject1 = (FrameLayout)findViewById(i);
    Object localObject2 = new in/juspay/mystique/d;
    HomeActivity.2 local2 = new in/org/npci/upiapp/HomeActivity$2;
    local2.<init>(this);
    ((d)localObject2).<init>(this, (FrameLayout)localObject1, null, local2);
    q = ((d)localObject2);
    localObject1 = new in/org/npci/upiapp/core/JsInterface;
    localObject2 = q;
    ((JsInterface)localObject1).<init>(this, (d)localObject2);
    r = ((JsInterface)localObject1);
    localObject1 = new in/org/npci/upiapp/core/a;
    localObject2 = q;
    ((in.org.npci.upiapp.core.a)localObject1).<init>(this, (d)localObject2);
    t = ((in.org.npci.upiapp.core.a)localObject1);
    localObject1 = q;
    localObject2 = r;
    ((d)localObject1).a(localObject2, "JBridge");
    localObject1 = q;
    localObject2 = t;
    ((d)localObject1).a(localObject2, "OtpJBridge");
    localObject1 = q;
    localObject2 = s;
    String str = "Tracker";
    ((d)localObject1).a(localObject2, str);
    if (paramBundle != null) {}
    try
    {
      localObject1 = q;
      localObject2 = "currentAppState";
      localObject2 = paramBundle.getString((String)localObject2);
      ((d)localObject1).c((String)localObject2);
      localObject1 = new in/org/npci/upiapp/core/JsInterface;
      localObject2 = q;
      ((JsInterface)localObject1).<init>(this, (d)localObject2);
      r = ((JsInterface)localObject1);
      localObject1 = q;
      localObject2 = r;
      str = "JBridge";
      ((d)localObject1).a(localObject2, str);
      localObject1 = new in/org/npci/upiapp/core/NPCIJSInterface;
      localObject2 = q;
      ((NPCIJSInterface)localObject1).<init>(this, (d)localObject2);
      u = ((NPCIJSInterface)localObject1);
      localObject1 = q;
      localObject2 = u;
      str = "NPCICL";
      ((d)localObject1).a(localObject2, str);
      localObject1 = new in/org/npci/upiapp/core/QRScannerInterface;
      localObject2 = q;
      ((QRScannerInterface)localObject1).<init>(this, (d)localObject2);
      v = ((QRScannerInterface)localObject1);
      localObject1 = q;
      localObject2 = v;
      str = "QRScanner";
      ((d)localObject1).a(localObject2, str);
      localObject1 = getResources();
      int k = 2131230728;
      boolean bool = ((Resources)localObject1).getBoolean(k);
      if (bool)
      {
        int j = Build.VERSION.SDK_INT;
        k = 19;
        if (j >= k)
        {
          j = 1;
          WebView.setWebContentsDebuggingEnabled(j);
        }
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        in.org.npci.upiapp.a.a.a(localException);
      }
    }
  }
  
  public static void b(String paramString)
  {
    A = Integer.valueOf(Integer.parseInt(paramString));
  }
  
  public static void c(String paramString)
  {
    B = Integer.valueOf(Integer.parseInt(paramString));
  }
  
  public static void d(String paramString)
  {
    C = Integer.valueOf(Integer.parseInt(paramString));
  }
  
  public static void e(String paramString)
  {
    D = paramString;
  }
  
  public static void f(String paramString)
  {
    try
    {
      long l = Long.parseLong(paramString);
      Long localLong = Long.valueOf(l);
      z = localLong;
      return;
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;)
      {
        String str1 = "HomeActivity";
        String str2 = "NumberformatExceptin in setSessionStartForOtp";
        in.org.npci.upiapp.a.a.a(str1, str2, localNumberFormatException);
      }
    }
  }
  
  private Bitmap g(String paramString)
  {
    try
    {
      localObject1 = new android/content/ContextWrapper;
      ((ContextWrapper)localObject1).<init>(this);
      Object localObject2 = "imageDir";
      FileInputStream localFileInputStream = null;
      localObject1 = ((ContextWrapper)localObject1).getDir((String)localObject2, 0);
      localObject2 = new java/io/File;
      ((File)localObject2).<init>((File)localObject1, paramString);
      localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>((File)localObject2);
      localObject1 = BitmapFactory.decodeStream(localFileInputStream);
      localFileInputStream.close();
    }
    catch (Exception localException)
    {
      for (;;)
      {
        in.org.npci.upiapp.a.a.a(localException);
        Object localObject1 = null;
      }
    }
    return (Bitmap)localObject1;
  }
  
  public static void n()
  {
    try
    {
      AlertDialog.Builder localBuilder = new android/app/AlertDialog$Builder;
      Object localObject1 = y;
      localBuilder.<init>((Context)localObject1);
      localObject1 = "App Update";
      localBuilder.setTitle((CharSequence)localObject1);
      localObject1 = "App has expired. This version will not work anymore. Please update the app from Play Store";
      localBuilder.setMessage((CharSequence)localObject1);
      localObject1 = null;
      localBuilder.setCancelable(false);
      localObject1 = "Update";
      Object localObject2 = new in/org/npci/upiapp/HomeActivity$3;
      ((HomeActivity.3)localObject2).<init>();
      localBuilder.setPositiveButton((CharSequence)localObject1, (DialogInterface.OnClickListener)localObject2);
      localObject1 = "Close";
      localObject2 = new in/org/npci/upiapp/HomeActivity$4;
      ((HomeActivity.4)localObject2).<init>();
      localBuilder.setNegativeButton((CharSequence)localObject1, (DialogInterface.OnClickListener)localObject2);
      localBuilder.create();
      localBuilder.show();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        in.org.npci.upiapp.a.a.a(localException);
      }
    }
  }
  
  public static Long o()
  {
    return z;
  }
  
  public static Integer p()
  {
    return A;
  }
  
  public static Integer q()
  {
    return B;
  }
  
  public static Integer r()
  {
    return C;
  }
  
  public static String s()
  {
    return D;
  }
  
  private boolean u()
  {
    boolean bool1 = true;
    Object localObject1 = "connectivity";
    for (;;)
    {
      try
      {
        localObject1 = getSystemService((String)localObject1);
        localObject1 = (ConnectivityManager)localObject1;
        int i = 1;
        localObject3 = ((ConnectivityManager)localObject1).getNetworkInfo(i);
        localObject4 = null;
        localObject4 = ((ConnectivityManager)localObject1).getNetworkInfo(0);
        int j = 7;
        localNetworkInfo = ((ConnectivityManager)localObject1).getNetworkInfo(j);
        int k = 9;
        localObject1 = ((ConnectivityManager)localObject1).getNetworkInfo(k);
        boolean bool3 = ((NetworkInfo)localObject3).isAvailable();
        if (!bool3) {
          continue;
        }
        localObject3 = ((NetworkInfo)localObject3).getDetailedState();
        NetworkInfo.DetailedState localDetailedState = NetworkInfo.DetailedState.CONNECTED;
        if (localObject3 != localDetailedState) {
          continue;
        }
        bool4 = bool1;
      }
      catch (Exception localException)
      {
        Object localObject3;
        Object localObject4;
        NetworkInfo localNetworkInfo;
        boolean bool2;
        boolean bool4 = false;
        Object localObject2 = null;
        continue;
      }
      return bool4;
      bool2 = ((NetworkInfo)localObject4).isAvailable();
      if (bool2)
      {
        localObject3 = ((NetworkInfo)localObject4).getDetailedState();
        localObject4 = NetworkInfo.DetailedState.CONNECTED;
        if (localObject3 == localObject4)
        {
          bool4 = bool1;
          continue;
        }
      }
      bool2 = localNetworkInfo.isAvailable();
      if (bool2)
      {
        localObject3 = localNetworkInfo.getDetailedState();
        localObject4 = NetworkInfo.DetailedState.CONNECTED;
        if (localObject3 == localObject4)
        {
          bool4 = bool1;
          continue;
        }
      }
      bool2 = ((NetworkInfo)localObject1).isAvailable();
      if (bool2)
      {
        localObject1 = ((NetworkInfo)localObject1).getDetailedState();
        localObject3 = NetworkInfo.DetailedState.CONNECTED;
        if (localObject1 == localObject3)
        {
          bool4 = bool1;
          continue;
        }
      }
      bool4 = false;
      localObject1 = null;
    }
  }
  
  private void v()
  {
    int i = -1;
    int j = 11;
    int k = 0;
    SharedPreferences.Editor localEditor1 = null;
    Object localObject1 = "NPCI";
    int i1 = 0;
    Object localObject2 = null;
    try
    {
      localObject1 = getSharedPreferences((String)localObject1, 0);
      if (localObject1 != null)
      {
        localObject2 = "LAST_APP_VERSION";
        int i2 = -1;
        i1 = ((SharedPreferences)localObject1).getInt((String)localObject2, i2);
        SharedPreferences.Editor localEditor2;
        if (i1 == i)
        {
          localEditor2 = ((SharedPreferences)localObject1).edit();
          String str = "LAST_APP_VERSION";
          int i3 = 11;
          localEditor2 = localEditor2.putInt(str, i3);
          localEditor2.apply();
        }
        if (j > i1)
        {
          localObject2 = "bhim";
          i2 = 0;
          localEditor2 = null;
          localObject2 = getDir((String)localObject2, 0);
          localObject2 = ((File)localObject2).listFiles();
          while (localObject2 != null)
          {
            i2 = localObject2.length;
            if (k >= i2) {
              break;
            }
            localEditor2 = localObject2[k];
            localEditor2.delete();
            k += 1;
          }
          localEditor1 = ((SharedPreferences)localObject1).edit();
          localObject1 = "LAST_APP_VERSION";
          i1 = 11;
          localEditor1 = localEditor1.putInt((String)localObject1, i1);
          localEditor1.apply();
        }
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        localObject1 = "HomeActivity";
        localObject2 = "Exception: resetInternalStorageIfAppUpdated";
        in.org.npci.upiapp.a.a.a((String)localObject1, (String)localObject2, localException);
      }
    }
  }
  
  private void w()
  {
    boolean bool = A();
    if (bool)
    {
      Intent localIntent = new android/content/Intent;
      Class localClass = RegistrationIntentService.class;
      localIntent.<init>(this, localClass);
      startService(localIntent);
    }
  }
  
  private void x()
  {
    Object localObject = "PRODUCTION";
    String str = "PRODUCTION";
    boolean bool = ((String)localObject).equals(str);
    if (bool)
    {
      bool = z();
      if (bool)
      {
        localObject = new java/lang/RuntimeException;
        ((RuntimeException)localObject).<init>("Debuggable mode wont work. Please download app from playstore");
        throw ((Throwable)localObject);
      }
    }
  }
  
  private void y()
  {
    Crashlytics.setUserIdentifier(getString(2131165239));
  }
  
  private boolean z()
  {
    ApplicationInfo localApplicationInfo = getApplicationInfo();
    int i = flags & 0x2;
    if (i != 0) {
      i = 1;
    }
    for (;;)
    {
      return i;
      int j = 0;
      localApplicationInfo = null;
    }
  }
  
  public int a(float paramFloat)
  {
    return (int)(getResourcesgetDisplayMetricsdensityDpi / 160 * paramFloat);
  }
  
  public Bitmap a(String paramString1, String paramString2, boolean paramBoolean)
  {
    Object localObject1 = g(paramString1);
    if (localObject1 != null) {}
    for (;;)
    {
      return (Bitmap)localObject1;
      Object localObject3 = new java/util/HashMap;
      ((HashMap)localObject3).<init>();
      localObject1 = in.juspay.widget.qrscanner.com.google.zxing.e.a;
      Object localObject4 = in.juspay.widget.qrscanner.com.google.zxing.b.a.f.d;
      ((Map)localObject3).put(localObject1, localObject4);
      localObject1 = new in/juspay/widget/qrscanner/com/google/zxing/b/b;
      ((in.juspay.widget.qrscanner.com.google.zxing.b.b)localObject1).<init>();
      try
      {
        localObject5 = in.juspay.widget.qrscanner.com.google.zxing.a.l;
        int i = 512;
        float f1 = 7.175E-43F;
        int j = 512;
        float f2 = 7.175E-43F;
        localObject4 = paramString2;
        in.juspay.widget.qrscanner.com.google.zxing.common.b localb = ((in.juspay.widget.qrscanner.com.google.zxing.b.b)localObject1).a(paramString2, (in.juspay.widget.qrscanner.com.google.zxing.a)localObject5, i, j, (Map)localObject3);
        int k = localb.c();
        int i1 = localb.d();
        localObject1 = Bitmap.Config.RGB_565;
        localObject1 = Bitmap.createBitmap(k, i1, (Bitmap.Config)localObject1);
        i = 0;
        f1 = 0.0F;
        while (i < k)
        {
          i2 = 0;
          localObject5 = null;
          f3 = 0.0F;
          if (i2 < i1)
          {
            boolean bool = localb.a(i, i2);
            if (bool) {}
            for (i3 = -16777216;; i3 = -1)
            {
              ((Bitmap)localObject1).setPixel(i, i2, i3);
              i3 = i2 + 1;
              i2 = i3;
              break;
            }
          }
          int i3 = i + 1;
          i = i3;
        }
        localObject4 = new android/graphics/BitmapFactory$Options;
        ((BitmapFactory.Options)localObject4).<init>();
        int i2 = 1;
        float f3 = Float.MIN_VALUE;
        inJustDecodeBounds = i2;
        localObject5 = getResources();
        i = 2130837812;
        f1 = 1.7280589E38F;
        BitmapFactory.decodeResource((Resources)localObject5, i, (BitmapFactory.Options)localObject4);
        i2 = 0;
        f3 = 0.0F;
        localObject5 = null;
        inJustDecodeBounds = false;
        i2 = 1101004800;
        f3 = 20.0F;
        i2 = a(f3);
        i2 = a((BitmapFactory.Options)localObject4, i2);
        inSampleSize = i2;
        localObject5 = getResources();
        i = 2130837812;
        f1 = 1.7280589E38F;
        localObject4 = BitmapFactory.decodeResource((Resources)localObject5, i, (BitmapFactory.Options)localObject4);
        localObject5 = new android/graphics/Canvas;
        ((Canvas)localObject5).<init>((Bitmap)localObject1);
        i = ((Canvas)localObject5).getWidth();
        j = ((Canvas)localObject5).getHeight();
        localObject3 = new android/graphics/Matrix;
        ((Matrix)localObject3).<init>();
        ((Canvas)localObject5).drawBitmap((Bitmap)localObject1, (Matrix)localObject3, null);
        k = ((Bitmap)localObject4).getWidth();
        i -= k;
        i /= 2;
        k = ((Bitmap)localObject4).getHeight();
        j -= k;
        j /= 2;
        f1 = i;
        f2 = j;
        k = 0;
        localObject3 = null;
        ((Canvas)localObject5).drawBitmap((Bitmap)localObject4, f1, f2, null);
        if (paramBoolean) {
          a(paramString1, (Bitmap)localObject1);
        }
      }
      catch (Exception localException)
      {
        localObject4 = "HomeActivity";
        Object localObject5 = "Exception in generateAndSaveQrBitmap";
        in.org.npci.upiapp.a.a.a((String)localObject4, (String)localObject5, localException);
        Object localObject2 = null;
      }
    }
  }
  
  protected void a(Bundle paramBundle)
  {
    int i = 8;
    x();
    v();
    w();
    Object localObject1 = new in/org/npci/upiapp/core/f;
    ((in.org.npci.upiapp.core.f)localObject1).<init>(this);
    s = ((in.org.npci.upiapp.core.f)localObject1);
    y();
    setContentView(2130968601);
    int j = 2131558502;
    localObject1 = (ImageView)findViewById(j);
    boolean bool1 = n;
    if ((!bool1) && (localObject1 == null))
    {
      localObject1 = new java/lang/AssertionError;
      ((AssertionError)localObject1).<init>();
      throw ((Throwable)localObject1);
    }
    int k = 2131558503;
    Object localObject2 = (TextView)findViewById(k);
    boolean bool2 = n;
    if ((!bool2) && (localObject2 == null))
    {
      localObject1 = new java/lang/AssertionError;
      ((AssertionError)localObject1).<init>();
      throw ((Throwable)localObject1);
    }
    int i1 = 2131558504;
    Button localButton = (Button)findViewById(i1);
    boolean bool3 = n;
    if ((!bool3) && (localButton == null))
    {
      localObject1 = new java/lang/AssertionError;
      ((AssertionError)localObject1).<init>();
      throw ((Throwable)localObject1);
    }
    HomeActivity.1 local1 = new in/org/npci/upiapp/HomeActivity$1;
    local1.<init>(this);
    localButton.setOnClickListener(local1);
    bool3 = u();
    if (!bool3)
    {
      ((TextView)localObject2).setVisibility(0);
      ((ImageView)localObject1).setVisibility(0);
      localButton.setVisibility(0);
    }
    for (;;)
    {
      y = this;
      j = 2131165262;
      p = getString(j);
      b(paramBundle);
      localObject1 = q;
      localObject2 = p;
      ((d)localObject1).b((String)localObject2);
      try
      {
        localObject1 = new android/webkit/WebView;
        ((WebView)localObject1).<init>(this);
        localObject1 = ((WebView)localObject1).getSettings();
        localObject1 = ((WebSettings)localObject1).getUserAgentString();
        m = (String)localObject1;
        return;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          String str = "";
          m = str;
        }
      }
      localButton.setVisibility(i);
      ((TextView)localObject2).setVisibility(i);
      ((ImageView)localObject1).setVisibility(0);
    }
  }
  
  public void a(String paramString)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>();
    localIntent.putExtra("response", paramString);
    setResult(-1, localIntent);
    finish();
  }
  
  public void a(String paramString1, String paramString2)
  {
    x = paramString2;
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("android.intent.action.CALL");
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = Uri.parse("tel:" + paramString1);
    localIntent.setData((Uri)localObject);
    startActivityForResult(localIntent, 12);
  }
  
  public void a(String paramString1, String paramString2, String paramString3)
  {
    w = paramString3;
    Intent localIntent = new android/content/Intent;
    Uri localUri = Uri.fromParts("sms", paramString2, null);
    localIntent.<init>("android.intent.action.VIEW", localUri);
    localIntent.putExtra("sms_body", paramString1);
    localIntent.putExtra("exit_on_sent", true);
    startActivityForResult(localIntent, 8);
  }
  
  public String k()
  {
    return E;
  }
  
  public String l()
  {
    Object localObject = getIntent();
    String str1 = ((Intent)localObject).getAction();
    localObject = ((Intent)localObject).getData();
    if (str1 != null)
    {
      String str2 = "android.intent.action.VIEW";
      boolean bool = str1.equals(str2);
      if ((!bool) || (localObject == null)) {}
    }
    for (localObject = ((Uri)localObject).toString();; localObject = null) {
      return (String)localObject;
    }
  }
  
  public QRScannerInterface m()
  {
    return v;
  }
  
  protected void onActivityResult(int paramInt1, int paramInt2, Intent paramIntent)
  {
    int i = -1;
    int j = 8;
    Object localObject1;
    if (paramInt1 == j) {
      if (paramInt2 == i)
      {
        localObject1 = q;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = ((StringBuilder)localObject3).append("window.callUICallback(\"");
        str1 = w;
        localObject3 = ((StringBuilder)localObject3).append(str1);
        str1 = "\", \"SUCCESS\")";
        localObject3 = str1;
        ((d)localObject1).a((String)localObject3);
      }
    }
    for (;;)
    {
      try
      {
        j = F;
        if ((paramInt1 == j) && (paramInt2 == i) && (paramIntent != null))
        {
          localObject1 = v;
          ((QRScannerInterface)localObject1).a();
          str2 = "";
          localObject3 = paramIntent.getData();
          if (localObject3 == null) {
            continue;
          }
          localObject1 = ((Uri)localObject3).toString();
          str1 = "file:";
          boolean bool1 = ((String)localObject1).startsWith(str1);
          if (!bool1) {
            continue;
          }
          localObject1 = ((Uri)localObject3).getPath();
        }
      }
      catch (Exception localException)
      {
        String str2;
        int k;
        boolean bool2;
        int i1;
        localObject3 = "HomeActivity";
        str1 = "Exception in onActivity Result";
        in.org.npci.upiapp.a.a.a((String)localObject3, str1, localException);
        continue;
        Object localObject2 = str2;
        continue;
      }
      try
      {
        localObject3 = new java/io/File;
        ((File)localObject3).<init>((String)localObject1);
        localObject1 = new java/io/FileInputStream;
        ((FileInputStream)localObject1).<init>((File)localObject3);
        localObject1 = BitmapFactory.decodeStream((InputStream)localObject1);
        localObject3 = v;
        ((QRScannerInterface)localObject3).a((Bitmap)localObject1);
      }
      catch (FileNotFoundException localFileNotFoundException)
      {
        localObject3 = "HomeActivity";
        str1 = "File not found exception";
        in.org.npci.upiapp.a.a.a((String)localObject3, str1, localFileNotFoundException);
        continue;
      }
      super.onActivityResult(paramInt1, paramInt2, paramIntent);
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = "Activity Result is " + paramInt1;
      in.org.npci.upiapp.a.a.a("HomeActivity", (String)localObject3);
      return;
      if (paramInt2 == 0)
      {
        localObject1 = q;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = ((StringBuilder)localObject3).append("window.callUICallback(\"");
        str1 = w;
        localObject3 = ((StringBuilder)localObject3).append(str1);
        str1 = "\", \"SUCCESS\")";
        localObject3 = str1;
        ((d)localObject1).a((String)localObject3);
        continue;
        k = 12;
        if (paramInt1 == k) {
          if (paramInt2 == i)
          {
            localObject1 = q;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = ((StringBuilder)localObject3).append("window.callUICallback(\"");
            str1 = x;
            localObject3 = ((StringBuilder)localObject3).append(str1);
            str1 = "\", \"SUCCESS\")";
            localObject3 = str1;
            ((d)localObject1).a((String)localObject3);
          }
          else if (paramInt2 == 0)
          {
            localObject1 = q;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = ((StringBuilder)localObject3).append("window.callUICallback(\"");
            str1 = x;
            localObject3 = ((StringBuilder)localObject3).append(str1);
            str1 = "\", \"FAILURE\")";
            localObject3 = str1;
            ((d)localObject1).a((String)localObject3);
            continue;
            localObject1 = getContentResolver();
            str1 = null;
            localObject1 = ((ContentResolver)localObject1).query((Uri)localObject3, null, null, null, null);
            if (localObject1 == null) {
              continue;
            }
            bool2 = ((Cursor)localObject1).moveToFirst();
            if (!bool2) {
              continue;
            }
            localObject3 = "_data";
            i1 = ((Cursor)localObject1).getColumnIndex((String)localObject3);
            if (i1 == i) {
              continue;
            }
            localObject1 = ((Cursor)localObject1).getString(i1);
          }
        }
      }
    }
  }
  
  public void onBackPressed()
  {
    q.a("window.onBackpressed()");
  }
  
  protected void onCreate(Bundle paramBundle)
  {
    super.onCreate(paramBundle);
    Object localObject = getIntent();
    String str = "notification";
    localObject = ((Intent)localObject).getStringExtra(str);
    boolean bool = TextUtils.isEmpty((CharSequence)localObject);
    if (!bool) {
      E = ((String)localObject);
    }
    a(paramBundle);
  }
  
  protected void onDestroy()
  {
    y = null;
    super.onDestroy();
    HomeActivity.a locala = o;
    b(locala);
    q.d("onDestroy");
    q.a();
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent)
  {
    boolean bool = true;
    int i = 3;
    d locald;
    String str;
    if (paramInt == i)
    {
      locald = q;
      str = "window.onHomepressed()";
      locald.a(str);
      moveTaskToBack(bool);
    }
    for (;;)
    {
      return bool;
      i = 4;
      if (paramInt == i)
      {
        locald = q;
        str = "window.onBackpressed()";
        locald.a(str);
      }
      else
      {
        i = 82;
        if (paramInt == i)
        {
          locald = q;
          str = "window.onMenupressed()";
          locald.a(str);
          moveTaskToBack(bool);
        }
        else
        {
          bool = false;
        }
      }
    }
  }
  
  protected void onNewIntent(Intent paramIntent)
  {
    super.onNewIntent(paramIntent);
    String str = paramIntent.getStringExtra("notification");
    boolean bool = TextUtils.isEmpty(str);
    if (!bool) {
      E = str;
    }
    a(null);
  }
  
  protected void onPause()
  {
    super.onPause();
    HomeActivity.a locala = o;
    b(locala);
    q.d("onPause");
  }
  
  public void onRequestPermissionsResult(int paramInt, String[] paramArrayOfString, int[] paramArrayOfInt)
  {
    r.a(paramInt, paramArrayOfString, paramArrayOfInt);
    QRScannerInterface localQRScannerInterface = v;
    if (localQRScannerInterface != null)
    {
      localQRScannerInterface = v;
      localQRScannerInterface.a(paramInt, paramArrayOfString, paramArrayOfInt);
    }
  }
  
  protected void onResume()
  {
    super.onResume();
    Object localObject = new in/org/npci/upiapp/HomeActivity$a;
    String str = null;
    ((HomeActivity.a)localObject).<init>(this, null);
    o = ((HomeActivity.a)localObject);
    localObject = o;
    a((BroadcastReceiver)localObject);
    localObject = u;
    if (localObject != null)
    {
      str = "Lifecycle - onResume Called";
      in.org.npci.upiapp.a.a.a("HomeActivity", str);
      localObject = u;
      ((NPCIJSInterface)localObject).a();
    }
    q.d("onResume");
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/HomeActivity.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */