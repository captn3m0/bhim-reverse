package in.org.npci.upiapp.utils;

import android.util.Base64;
import java.security.Key;
import java.security.MessageDigest;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class a
{
  private static final String a = a.class.getName();
  
  public static String a(String paramString1, String paramString2)
  {
    Object localObject1 = c(paramString2);
    Object localObject2 = Cipher.getInstance("AES");
    ((Cipher)localObject2).init(2, (Key)localObject1);
    localObject1 = Base64.decode(paramString1, 0);
    localObject1 = ((Cipher)localObject2).doFinal((byte[])localObject1);
    localObject2 = new java/lang/String;
    ((String)localObject2).<init>((byte[])localObject1);
    return (String)localObject2;
  }
  
  public static String a(String paramString1, String paramString2, String paramString3)
  {
    byte[] arrayOfByte = b(paramString2, paramString3);
    return Base64.encodeToString(a(paramString1.getBytes(), arrayOfByte), 0);
  }
  
  public static String a(byte[] paramArrayOfByte)
  {
    return c(b(paramArrayOfByte));
  }
  
  public static byte[] a(String paramString)
  {
    Object localObject1 = "SHA-256";
    try
    {
      localObject1 = MessageDigest.getInstance((String)localObject1);
      localObject3 = "UTF-8";
      localObject3 = paramString.getBytes((String)localObject3);
      localObject1 = ((MessageDigest)localObject1).digest((byte[])localObject3);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject3 = a;
        String str = "Error generating hash ";
        in.org.npci.upiapp.a.a.a((String)localObject3, str, localException);
        Object localObject2 = null;
      }
    }
    return (byte[])localObject1;
  }
  
  public static byte[] a(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    Object localObject = new byte[16];
    localIvParameterSpec.<init>((byte[])localObject);
    localObject = Cipher.getInstance("AES/CBC/PKCS7Padding");
    ((Cipher)localObject).init(1, localSecretKeySpec, localIvParameterSpec);
    return ((Cipher)localObject).doFinal(paramArrayOfByte1);
  }
  
  public static String b(String paramString)
  {
    return c(a(paramString));
  }
  
  public static String b(String paramString1, String paramString2, String paramString3)
  {
    byte[] arrayOfByte1 = b(paramString2, paramString3);
    byte[] arrayOfByte2 = Base64.decode(paramString1, 0);
    String str = new java/lang/String;
    arrayOfByte1 = b(arrayOfByte2, arrayOfByte1);
    str.<init>(arrayOfByte1);
    return str;
  }
  
  private static byte[] b(String paramString1, String paramString2)
  {
    PBEKeySpec localPBEKeySpec = new javax/crypto/spec/PBEKeySpec;
    char[] arrayOfChar = paramString1.toCharArray();
    byte[] arrayOfByte = paramString2.getBytes();
    localPBEKeySpec.<init>(arrayOfChar, arrayOfByte, 1, 256);
    return SecretKeyFactory.getInstance("PBEWITHSHA256AND128BITAES-CBC-BC").generateSecret(localPBEKeySpec).getEncoded();
  }
  
  private static byte[] b(byte[] paramArrayOfByte)
  {
    Object localObject1 = "SHA-256";
    try
    {
      localObject1 = MessageDigest.getInstance((String)localObject1);
      localObject1 = ((MessageDigest)localObject1).digest(paramArrayOfByte);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        String str1 = a;
        String str2 = "Error generating hash ";
        in.org.npci.upiapp.a.a.a(str1, str2, localException);
        Object localObject2 = null;
      }
    }
    return (byte[])localObject1;
  }
  
  private static byte[] b(byte[] paramArrayOfByte1, byte[] paramArrayOfByte2)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    localSecretKeySpec.<init>(paramArrayOfByte2, "AES");
    IvParameterSpec localIvParameterSpec = new javax/crypto/spec/IvParameterSpec;
    Object localObject = new byte[16];
    localIvParameterSpec.<init>((byte[])localObject);
    localObject = Cipher.getInstance("AES/CBC/PKCS7Padding");
    ((Cipher)localObject).init(2, localSecretKeySpec, localIvParameterSpec);
    return ((Cipher)localObject).doFinal(paramArrayOfByte1);
  }
  
  private static String c(byte[] paramArrayOfByte)
  {
    StringBuffer localStringBuffer;
    if (paramArrayOfByte != null)
    {
      localStringBuffer = new java/lang/StringBuffer;
      localStringBuffer.<init>();
      int i = 0;
      str1 = null;
      for (;;)
      {
        int j = paramArrayOfByte.length;
        if (i >= j) {
          break;
        }
        j = paramArrayOfByte[i] & 0xFF;
        String str2 = Integer.toHexString(j);
        int k = str2.length();
        int m = 1;
        if (k == m)
        {
          k = 48;
          localStringBuffer.append(k);
        }
        localStringBuffer.append(str2);
        i += 1;
      }
    }
    for (String str1 = localStringBuffer.toString();; str1 = "") {
      return str1;
    }
  }
  
  private static Key c(String paramString)
  {
    SecretKeySpec localSecretKeySpec = new javax/crypto/spec/SecretKeySpec;
    byte[] arrayOfByte = paramString.getBytes("UTF-8");
    localSecretKeySpec.<init>(arrayOfByte, "AES");
    return localSecretKeySpec;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/utils/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */