package in.org.npci.upiapp.utils;

import android.content.Context;
import android.content.res.AssetManager;
import in.org.npci.upiapp.a.a;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class b
{
  private static ByteArrayOutputStream a(ByteArrayOutputStream paramByteArrayOutputStream, InputStream paramInputStream)
  {
    int i = 4096;
    byte[] arrayOfByte = new byte[i];
    for (;;)
    {
      int j = paramInputStream.read(arrayOfByte);
      int k = -1;
      if (j == k) {
        break;
      }
      k = 0;
      paramByteArrayOutputStream.write(arrayOfByte, 0, j);
    }
    paramByteArrayOutputStream.close();
    paramInputStream.close();
    return paramByteArrayOutputStream;
  }
  
  public static File a(String paramString, Context paramContext)
  {
    File localFile1 = new java/io/File;
    File localFile2 = paramContext.getDir("bhim", 0);
    localFile1.<init>(localFile2, paramString);
    return localFile1;
  }
  
  /* Error */
  public static void a(Context paramContext, String paramString, byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: new 38	java/io/FileOutputStream
    //   5: astore 4
    //   7: new 24	java/io/File
    //   10: astore 5
    //   12: ldc 26
    //   14: astore 6
    //   16: aload_0
    //   17: aload 6
    //   19: iconst_0
    //   20: invokevirtual 32	android/content/Context:getDir	(Ljava/lang/String;I)Ljava/io/File;
    //   23: astore 6
    //   25: aload 5
    //   27: aload 6
    //   29: aload_1
    //   30: invokespecial 36	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   33: aload 4
    //   35: aload 5
    //   37: invokespecial 41	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   40: aload 4
    //   42: aload_2
    //   43: invokevirtual 44	java/io/FileOutputStream:write	([B)V
    //   46: aload 4
    //   48: ifnull +8 -> 56
    //   51: aload 4
    //   53: invokevirtual 45	java/io/FileOutputStream:close	()V
    //   56: return
    //   57: astore 5
    //   59: ldc 47
    //   61: astore 4
    //   63: ldc 49
    //   65: astore_3
    //   66: aload 4
    //   68: aload_3
    //   69: aload 5
    //   71: invokestatic 55	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   74: goto -18 -> 56
    //   77: astore 5
    //   79: aconst_null
    //   80: astore 4
    //   82: ldc 47
    //   84: astore_3
    //   85: ldc 57
    //   87: astore 6
    //   89: aload_3
    //   90: aload 6
    //   92: aload 5
    //   94: invokestatic 55	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   97: aload 5
    //   99: invokestatic 60	in/org/npci/upiapp/a/a:a	(Ljava/lang/Throwable;)V
    //   102: aload 4
    //   104: ifnull -48 -> 56
    //   107: aload 4
    //   109: invokevirtual 45	java/io/FileOutputStream:close	()V
    //   112: goto -56 -> 56
    //   115: astore 5
    //   117: ldc 47
    //   119: astore 4
    //   121: ldc 49
    //   123: astore_3
    //   124: aload 4
    //   126: aload_3
    //   127: aload 5
    //   129: invokestatic 55	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   132: goto -76 -> 56
    //   135: astore 5
    //   137: aload_3
    //   138: ifnull +7 -> 145
    //   141: aload_3
    //   142: invokevirtual 45	java/io/FileOutputStream:close	()V
    //   145: aload 5
    //   147: athrow
    //   148: astore 4
    //   150: ldc 47
    //   152: astore_3
    //   153: ldc 49
    //   155: astore 6
    //   157: aload_3
    //   158: aload 6
    //   160: aload 4
    //   162: invokestatic 55	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   165: goto -20 -> 145
    //   168: astore 5
    //   170: aload 4
    //   172: astore_3
    //   173: goto -36 -> 137
    //   176: astore 5
    //   178: goto -96 -> 82
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	181	0	paramContext	Context
    //   0	181	1	paramString	String
    //   0	181	2	paramArrayOfByte	byte[]
    //   1	172	3	localObject1	Object
    //   5	120	4	localObject2	Object
    //   148	23	4	localIOException1	IOException
    //   10	26	5	localFile	File
    //   57	13	5	localIOException2	IOException
    //   77	21	5	localException1	Exception
    //   115	13	5	localIOException3	IOException
    //   135	11	5	localObject3	Object
    //   168	1	5	localObject4	Object
    //   176	1	5	localException2	Exception
    //   14	145	6	localObject5	Object
    // Exception table:
    //   from	to	target	type
    //   51	56	57	java/io/IOException
    //   2	5	77	java/lang/Exception
    //   7	10	77	java/lang/Exception
    //   19	23	77	java/lang/Exception
    //   29	33	77	java/lang/Exception
    //   35	40	77	java/lang/Exception
    //   107	112	115	java/io/IOException
    //   2	5	135	finally
    //   7	10	135	finally
    //   19	23	135	finally
    //   29	33	135	finally
    //   35	40	135	finally
    //   141	145	148	java/io/IOException
    //   42	46	168	finally
    //   92	97	168	finally
    //   97	102	168	finally
    //   42	46	176	java/lang/Exception
  }
  
  public static byte[] a(Context paramContext, String paramString)
  {
    return a(paramContext, paramString, "");
  }
  
  public static byte[] a(Context paramContext, String paramString1, String paramString2)
  {
    try
    {
      localObject1 = b(paramContext, paramString1);
      if (localObject1 == null)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        localObject1 = ((StringBuilder)localObject1).append(paramString2);
        str1 = "/";
        localObject1 = ((StringBuilder)localObject1).append(str1);
        localObject1 = ((StringBuilder)localObject1).append(paramString1);
        localObject1 = ((StringBuilder)localObject1).toString();
        localObject1 = c(paramContext, (String)localObject1);
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        String str2 = "not found in internal storage.";
        a.a("FileUtil", str2, localException);
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append(paramString2);
        String str1 = "/";
        localObject2 = str1 + paramString1;
        localObject2 = c(paramContext, (String)localObject2);
      }
    }
    return (byte[])localObject1;
  }
  
  public static byte[] b(Context paramContext, String paramString)
  {
    Object localObject1 = new java/io/File;
    Object localObject2 = null;
    Object localObject3 = paramContext.getDir("bhim", 0);
    ((File)localObject1).<init>((File)localObject3, paramString);
    boolean bool = ((File)localObject1).exists();
    if (bool)
    {
      localObject3 = new java/io/FileInputStream;
      ((FileInputStream)localObject3).<init>((File)localObject1);
      localObject1 = new java/io/ByteArrayOutputStream;
      ((ByteArrayOutputStream)localObject1).<init>();
      localObject3 = a((ByteArrayOutputStream)localObject1, (InputStream)localObject3);
      localObject1 = ((ByteArrayOutputStream)localObject3).toByteArray();
      ((ByteArrayOutputStream)localObject3).flush();
      ((ByteArrayOutputStream)localObject3).close();
      localObject3 = "FileUtil";
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append(paramString);
      String str = " found in internal storage.";
      localObject2 = str;
      a.a((String)localObject3, (String)localObject2);
    }
    for (;;)
    {
      return (byte[])localObject1;
      localObject1 = null;
    }
  }
  
  public static byte[] b(String paramString, Context paramContext)
  {
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      localByteArrayOutputStream.<init>();
      localObject1 = new java/io/FileInputStream;
      localObject2 = a(paramString, paramContext);
      ((FileInputStream)localObject1).<init>((File)localObject2);
      localByteArrayOutputStream = a(localByteArrayOutputStream, (InputStream)localObject1);
      return localByteArrayOutputStream.toByteArray();
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Could not read " + paramString;
      a.a("FileUtil", (String)localObject2, localFileNotFoundException);
      localObject1 = new java/lang/RuntimeException;
      ((RuntimeException)localObject1).<init>(localFileNotFoundException);
      throw ((Throwable)localObject1);
    }
    catch (IOException localIOException)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Could not read " + paramString;
      a.a("FileUtil", (String)localObject2, localIOException);
      c(paramString, paramContext);
      localObject1 = new java/lang/RuntimeException;
      ((RuntimeException)localObject1).<init>(localIOException);
      throw ((Throwable)localObject1);
    }
    catch (Exception localException)
    {
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Could not read " + paramString;
      a.a("FileUtil", (String)localObject2, localException);
      c(paramString, paramContext);
      Object localObject1 = new java/lang/RuntimeException;
      ((RuntimeException)localObject1).<init>(localException);
      throw ((Throwable)localObject1);
    }
  }
  
  public static boolean c(String paramString, Context paramContext)
  {
    File localFile = a(paramString, paramContext);
    boolean bool1 = localFile.exists();
    Object localObject;
    String str;
    boolean bool2;
    if (bool1)
    {
      localObject = "FileUtil";
      str = "FILE CORRUPTED. DISABLING GODEL";
      a.b((String)localObject, str);
      bool2 = localFile.delete();
    }
    for (;;)
    {
      return bool2;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = ((StringBuilder)localObject).append(paramString);
      str = " not found";
      localObject = str;
      a.a("FileUtil", (String)localObject);
      bool2 = false;
      localFile = null;
    }
  }
  
  public static byte[] c(Context paramContext, String paramString)
  {
    Object localObject1 = paramContext.getAssets().open(paramString, 0);
    Object localObject2 = new java/io/ByteArrayOutputStream;
    ((ByteArrayOutputStream)localObject2).<init>();
    localObject1 = a((ByteArrayOutputStream)localObject2, (InputStream)localObject1);
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = paramString + " found in assets.";
    a.a("FileUtil", (String)localObject3);
    localObject2 = ((ByteArrayOutputStream)localObject1).toByteArray();
    ((ByteArrayOutputStream)localObject1).flush();
    ((ByteArrayOutputStream)localObject1).close();
    return (byte[])localObject2;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/utils/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */