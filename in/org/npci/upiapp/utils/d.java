package in.org.npci.upiapp.utils;

import android.content.Context;
import in.org.npci.upiapp.a.a;

public class d
{
  public static Boolean a(Context paramContext, String paramString, boolean paramBoolean)
  {
    int i = paramString.lastIndexOf("/") + 1;
    Object localObject1 = a(paramString.substring(i));
    Object localObject2 = b(paramContext, paramString, paramBoolean);
    Object localObject3;
    if (localObject2 != null)
    {
      if (paramBoolean)
      {
        localObject3 = ".zip";
        String str = ".jsa";
        localObject1 = ((String)localObject1).replace((CharSequence)localObject3, str);
      }
      b.a(paramContext, (String)localObject1, (byte[])localObject2);
      localObject2 = "RemoteAssetService";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject1 = ((StringBuilder)localObject3).append((String)localObject1);
      localObject3 = " downloaded successfully";
      localObject1 = (String)localObject3;
      a.a((String)localObject2, (String)localObject1);
      i = 1;
    }
    for (localObject1 = Boolean.valueOf(i);; localObject1 = Boolean.valueOf(false))
    {
      return (Boolean)localObject1;
      localObject2 = "RemoteAssetService";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject1 = ((StringBuilder)localObject3).append((String)localObject1);
      localObject3 = " null";
      localObject1 = (String)localObject3;
      a.b((String)localObject2, (String)localObject1);
      int j = 0;
    }
  }
  
  public static String a(String paramString)
  {
    return paramString.split("\\?")[0].split("#")[0].replaceAll("[^a-zA-Z0-9.]", "_");
  }
  
  /* Error */
  private static byte[] b(Context paramContext, String paramString, boolean paramBoolean)
  {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aconst_null
    //   3: astore 4
    //   5: aconst_null
    //   6: astore 5
    //   8: aload_1
    //   9: ldc 6
    //   11: invokevirtual 12	java/lang/String:lastIndexOf	(Ljava/lang/String;)I
    //   14: iconst_1
    //   15: iadd
    //   16: istore 6
    //   18: aload_1
    //   19: iload 6
    //   21: invokevirtual 16	java/lang/String:substring	(I)Ljava/lang/String;
    //   24: astore 7
    //   26: aload 7
    //   28: invokestatic 20	in/org/npci/upiapp/utils/d:a	(Ljava/lang/String;)Ljava/lang/String;
    //   31: astore 8
    //   33: iload_2
    //   34: ifeq +271 -> 305
    //   37: ldc 26
    //   39: astore 7
    //   41: aload_1
    //   42: aload 7
    //   44: invokevirtual 91	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   47: istore 6
    //   49: iload 6
    //   51: ifeq +254 -> 305
    //   54: aload_0
    //   55: ldc 93
    //   57: iconst_0
    //   58: invokevirtual 99	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   61: astore 7
    //   63: new 41	java/lang/StringBuilder
    //   66: astore 4
    //   68: aload 4
    //   70: invokespecial 45	java/lang/StringBuilder:<init>	()V
    //   73: aload 4
    //   75: aload 8
    //   77: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   80: astore 4
    //   82: ldc 101
    //   84: astore 9
    //   86: aload 4
    //   88: aload 9
    //   90: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   93: invokevirtual 55	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   96: astore 4
    //   98: aload 7
    //   100: aload 4
    //   102: aconst_null
    //   103: invokeinterface 106 3 0
    //   108: astore 7
    //   110: new 108	java/util/HashMap
    //   113: astore 4
    //   115: aload 4
    //   117: invokespecial 109	java/util/HashMap:<init>	()V
    //   120: invokestatic 117	java/lang/System:currentTimeMillis	()J
    //   123: lstore 10
    //   125: lload 10
    //   127: invokestatic 120	java/lang/String:valueOf	(J)Ljava/lang/String;
    //   130: astore 12
    //   132: aload 4
    //   134: ldc 111
    //   136: aload 12
    //   138: invokevirtual 124	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   141: pop
    //   142: ldc 126
    //   144: astore 9
    //   146: aload 4
    //   148: aload 9
    //   150: aload 7
    //   152: invokevirtual 124	java/util/HashMap:put	(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   155: pop
    //   156: aload_0
    //   157: aload_1
    //   158: aload 4
    //   160: invokestatic 131	in/org/npci/upiapp/utils/RestClient:a	(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Lin/org/npci/upiapp/models/ApiResponse;
    //   163: astore 13
    //   165: aload 13
    //   167: ifnull +387 -> 554
    //   170: aload 13
    //   172: invokevirtual 137	in/org/npci/upiapp/models/ApiResponse:getData	()[B
    //   175: astore 7
    //   177: aload 7
    //   179: ifnull +375 -> 554
    //   182: ldc 26
    //   184: astore 7
    //   186: aload_1
    //   187: aload 7
    //   189: invokevirtual 91	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   192: istore 6
    //   194: iload 6
    //   196: ifeq +358 -> 554
    //   199: iload_2
    //   200: ifeq +354 -> 554
    //   203: aload 13
    //   205: invokevirtual 137	in/org/npci/upiapp/models/ApiResponse:getData	()[B
    //   208: astore 7
    //   210: aload 7
    //   212: invokestatic 142	in/org/npci/upiapp/utils/a:a	([B)Ljava/lang/String;
    //   215: astore 14
    //   217: new 144	java/io/ByteArrayInputStream
    //   220: astore 4
    //   222: aload 4
    //   224: aload 7
    //   226: invokespecial 147	java/io/ByteArrayInputStream:<init>	([B)V
    //   229: new 149	java/util/zip/ZipInputStream
    //   232: astore 12
    //   234: aload 12
    //   236: aload 4
    //   238: invokespecial 152	java/util/zip/ZipInputStream:<init>	(Ljava/io/InputStream;)V
    //   241: iconst_0
    //   242: istore_3
    //   243: aconst_null
    //   244: astore 4
    //   246: aconst_null
    //   247: astore 9
    //   249: aload 12
    //   251: invokevirtual 156	java/util/zip/ZipInputStream:getNextEntry	()Ljava/util/zip/ZipEntry;
    //   254: astore 15
    //   256: aload 15
    //   258: ifnull +184 -> 442
    //   261: new 158	java/io/ByteArrayOutputStream
    //   264: astore 16
    //   266: aload 16
    //   268: invokespecial 159	java/io/ByteArrayOutputStream:<init>	()V
    //   271: aload 12
    //   273: invokevirtual 163	java/util/zip/ZipInputStream:read	()I
    //   276: istore 6
    //   278: iconst_m1
    //   279: istore 17
    //   281: iload 6
    //   283: iload 17
    //   285: if_icmpeq +43 -> 328
    //   288: aload 16
    //   290: iload 6
    //   292: invokevirtual 167	java/io/ByteArrayOutputStream:write	(I)V
    //   295: aload 12
    //   297: invokevirtual 163	java/util/zip/ZipInputStream:read	()I
    //   300: istore 6
    //   302: goto -24 -> 278
    //   305: aload_0
    //   306: aload 8
    //   308: invokestatic 170	in/org/npci/upiapp/utils/b:b	(Landroid/content/Context;Ljava/lang/String;)[B
    //   311: astore 7
    //   313: aload 7
    //   315: ifnull +477 -> 792
    //   318: aload 7
    //   320: invokestatic 142	in/org/npci/upiapp/utils/a:a	([B)Ljava/lang/String;
    //   323: astore 7
    //   325: goto -215 -> 110
    //   328: aload 12
    //   330: invokevirtual 173	java/util/zip/ZipInputStream:closeEntry	()V
    //   333: aload 16
    //   335: invokevirtual 176	java/io/ByteArrayOutputStream:close	()V
    //   338: aload 15
    //   340: invokevirtual 181	java/util/zip/ZipEntry:getName	()Ljava/lang/String;
    //   343: astore 7
    //   345: ldc -73
    //   347: astore 18
    //   349: aload 7
    //   351: aload 18
    //   353: invokevirtual 91	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   356: istore 6
    //   358: iload 6
    //   360: ifeq +35 -> 395
    //   363: aload 16
    //   365: invokevirtual 186	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   368: astore 7
    //   370: iconst_2
    //   371: istore_3
    //   372: aload 7
    //   374: iload_3
    //   375: invokestatic 193	android/util/Base64:decode	([BI)[B
    //   378: astore 7
    //   380: aload 9
    //   382: astore 4
    //   384: aload 4
    //   386: astore 9
    //   388: aload 7
    //   390: astore 4
    //   392: goto -143 -> 249
    //   395: aload 15
    //   397: invokevirtual 181	java/util/zip/ZipEntry:getName	()Ljava/lang/String;
    //   400: astore 7
    //   402: ldc 28
    //   404: astore 15
    //   406: aload 7
    //   408: aload 15
    //   410: invokevirtual 91	java/lang/String:endsWith	(Ljava/lang/String;)Z
    //   413: istore 6
    //   415: iload 6
    //   417: ifeq +364 -> 781
    //   420: aload 16
    //   422: invokevirtual 186	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   425: astore 7
    //   427: aload 4
    //   429: astore 19
    //   431: aload 7
    //   433: astore 4
    //   435: aload 19
    //   437: astore 7
    //   439: goto -55 -> 384
    //   442: aload 9
    //   444: ifnonnull +11 -> 455
    //   447: aload 4
    //   449: ifnonnull +6 -> 455
    //   452: aload 5
    //   454: areturn
    //   455: new 195	java/io/ObjectInputStream
    //   458: astore 12
    //   460: new 144	java/io/ByteArrayInputStream
    //   463: astore 7
    //   465: ldc -59
    //   467: astore 15
    //   469: aload_0
    //   470: aload 15
    //   472: invokestatic 200	in/org/npci/upiapp/utils/b:c	(Landroid/content/Context;Ljava/lang/String;)[B
    //   475: astore 15
    //   477: aload 7
    //   479: aload 15
    //   481: invokespecial 147	java/io/ByteArrayInputStream:<init>	([B)V
    //   484: aload 12
    //   486: aload 7
    //   488: invokespecial 201	java/io/ObjectInputStream:<init>	(Ljava/io/InputStream;)V
    //   491: aload 12
    //   493: invokevirtual 205	java/io/ObjectInputStream:readObject	()Ljava/lang/Object;
    //   496: astore 7
    //   498: aload 7
    //   500: checkcast 207	java/security/PublicKey
    //   503: astore 7
    //   505: ldc -47
    //   507: astore 15
    //   509: aload 15
    //   511: invokestatic 215	java/security/Signature:getInstance	(Ljava/lang/String;)Ljava/security/Signature;
    //   514: astore 15
    //   516: aload 15
    //   518: aload 7
    //   520: invokevirtual 219	java/security/Signature:initVerify	(Ljava/security/PublicKey;)V
    //   523: aload 15
    //   525: aload 9
    //   527: invokevirtual 222	java/security/Signature:update	([B)V
    //   530: aload 15
    //   532: aload 4
    //   534: invokevirtual 226	java/security/Signature:verify	([B)Z
    //   537: istore 6
    //   539: iload 6
    //   541: ifne +28 -> 569
    //   544: aload 12
    //   546: ifnull +8 -> 554
    //   549: aload 12
    //   551: invokevirtual 227	java/io/ObjectInputStream:close	()V
    //   554: aload 13
    //   556: ifnull -104 -> 452
    //   559: aload 13
    //   561: invokevirtual 137	in/org/npci/upiapp/models/ApiResponse:getData	()[B
    //   564: astore 5
    //   566: goto -114 -> 452
    //   569: ldc 93
    //   571: astore 7
    //   573: iconst_0
    //   574: istore_3
    //   575: aconst_null
    //   576: astore 4
    //   578: aload_0
    //   579: aload 7
    //   581: iconst_0
    //   582: invokevirtual 99	android/content/Context:getSharedPreferences	(Ljava/lang/String;I)Landroid/content/SharedPreferences;
    //   585: astore 7
    //   587: aload 7
    //   589: invokeinterface 231 1 0
    //   594: astore 7
    //   596: new 41	java/lang/StringBuilder
    //   599: astore 4
    //   601: aload 4
    //   603: invokespecial 45	java/lang/StringBuilder:<init>	()V
    //   606: aload 4
    //   608: aload 8
    //   610: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   613: astore 4
    //   615: ldc 101
    //   617: astore 9
    //   619: aload 4
    //   621: aload 9
    //   623: invokevirtual 49	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   626: astore 4
    //   628: aload 4
    //   630: invokevirtual 55	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   633: astore 4
    //   635: aload 7
    //   637: aload 4
    //   639: aload 14
    //   641: invokeinterface 237 3 0
    //   646: astore 7
    //   648: aload 7
    //   650: invokeinterface 241 1 0
    //   655: pop
    //   656: goto -112 -> 544
    //   659: astore 7
    //   661: aload 12
    //   663: astore 4
    //   665: ldc 39
    //   667: astore 9
    //   669: ldc -13
    //   671: astore 12
    //   673: aload 9
    //   675: aload 12
    //   677: aload 7
    //   679: invokestatic 246	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   682: aload 4
    //   684: ifnull -130 -> 554
    //   687: aload 4
    //   689: invokevirtual 227	java/io/ObjectInputStream:close	()V
    //   692: goto -138 -> 554
    //   695: astore 7
    //   697: ldc 39
    //   699: astore 4
    //   701: ldc -8
    //   703: astore 9
    //   705: aload 4
    //   707: aload 9
    //   709: aload 7
    //   711: invokestatic 246	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   714: goto -160 -> 554
    //   717: astore 7
    //   719: aconst_null
    //   720: astore 12
    //   722: aload 12
    //   724: ifnull +8 -> 732
    //   727: aload 12
    //   729: invokevirtual 227	java/io/ObjectInputStream:close	()V
    //   732: aload 7
    //   734: athrow
    //   735: astore 7
    //   737: ldc 39
    //   739: astore 4
    //   741: ldc -13
    //   743: astore 9
    //   745: aload 4
    //   747: aload 9
    //   749: aload 7
    //   751: invokestatic 246	in/org/npci/upiapp/a/a:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    //   754: goto -200 -> 554
    //   757: astore 7
    //   759: goto -37 -> 722
    //   762: astore 7
    //   764: aload 4
    //   766: astore 12
    //   768: goto -46 -> 722
    //   771: astore 7
    //   773: iconst_0
    //   774: istore_3
    //   775: aconst_null
    //   776: astore 4
    //   778: goto -113 -> 665
    //   781: aload 4
    //   783: astore 7
    //   785: aload 9
    //   787: astore 4
    //   789: goto -405 -> 384
    //   792: iconst_0
    //   793: istore 6
    //   795: aconst_null
    //   796: astore 7
    //   798: goto -688 -> 110
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	801	0	paramContext	Context
    //   0	801	1	paramString	String
    //   0	801	2	paramBoolean	boolean
    //   1	774	3	i	int
    //   3	785	4	localObject1	Object
    //   6	559	5	arrayOfByte	byte[]
    //   16	4	6	j	int
    //   47	148	6	bool1	boolean
    //   276	25	6	k	int
    //   356	438	6	bool2	boolean
    //   24	625	7	localObject2	Object
    //   659	19	7	localException1	Exception
    //   695	15	7	localOutOfMemoryError	OutOfMemoryError
    //   717	16	7	localObject3	Object
    //   735	15	7	localException2	Exception
    //   757	1	7	localObject4	Object
    //   762	1	7	localObject5	Object
    //   771	1	7	localException3	Exception
    //   783	14	7	localObject6	Object
    //   31	578	8	str1	String
    //   84	702	9	localObject7	Object
    //   123	3	10	l	long
    //   130	637	12	localObject8	Object
    //   163	397	13	localApiResponse	in.org.npci.upiapp.models.ApiResponse
    //   215	425	14	str2	String
    //   254	277	15	localObject9	Object
    //   264	157	16	localByteArrayOutputStream	java.io.ByteArrayOutputStream
    //   279	7	17	m	int
    //   347	5	18	str3	String
    //   429	7	19	localObject10	Object
    // Exception table:
    //   from	to	target	type
    //   491	496	659	java/lang/Exception
    //   498	503	659	java/lang/Exception
    //   509	514	659	java/lang/Exception
    //   518	523	659	java/lang/Exception
    //   525	530	659	java/lang/Exception
    //   532	537	659	java/lang/Exception
    //   581	585	659	java/lang/Exception
    //   587	594	659	java/lang/Exception
    //   596	599	659	java/lang/Exception
    //   601	606	659	java/lang/Exception
    //   608	613	659	java/lang/Exception
    //   621	626	659	java/lang/Exception
    //   628	633	659	java/lang/Exception
    //   639	646	659	java/lang/Exception
    //   648	656	659	java/lang/Exception
    //   249	254	695	java/lang/OutOfMemoryError
    //   261	264	695	java/lang/OutOfMemoryError
    //   266	271	695	java/lang/OutOfMemoryError
    //   271	276	695	java/lang/OutOfMemoryError
    //   290	295	695	java/lang/OutOfMemoryError
    //   295	300	695	java/lang/OutOfMemoryError
    //   328	333	695	java/lang/OutOfMemoryError
    //   333	338	695	java/lang/OutOfMemoryError
    //   338	343	695	java/lang/OutOfMemoryError
    //   351	356	695	java/lang/OutOfMemoryError
    //   363	368	695	java/lang/OutOfMemoryError
    //   374	378	695	java/lang/OutOfMemoryError
    //   395	400	695	java/lang/OutOfMemoryError
    //   408	413	695	java/lang/OutOfMemoryError
    //   420	425	695	java/lang/OutOfMemoryError
    //   549	554	695	java/lang/OutOfMemoryError
    //   687	692	695	java/lang/OutOfMemoryError
    //   727	732	695	java/lang/OutOfMemoryError
    //   732	735	695	java/lang/OutOfMemoryError
    //   455	458	717	finally
    //   460	463	717	finally
    //   470	475	717	finally
    //   479	484	717	finally
    //   486	491	717	finally
    //   249	254	735	java/lang/Exception
    //   261	264	735	java/lang/Exception
    //   266	271	735	java/lang/Exception
    //   271	276	735	java/lang/Exception
    //   290	295	735	java/lang/Exception
    //   295	300	735	java/lang/Exception
    //   328	333	735	java/lang/Exception
    //   333	338	735	java/lang/Exception
    //   338	343	735	java/lang/Exception
    //   351	356	735	java/lang/Exception
    //   363	368	735	java/lang/Exception
    //   374	378	735	java/lang/Exception
    //   395	400	735	java/lang/Exception
    //   408	413	735	java/lang/Exception
    //   420	425	735	java/lang/Exception
    //   549	554	735	java/lang/Exception
    //   687	692	735	java/lang/Exception
    //   727	732	735	java/lang/Exception
    //   732	735	735	java/lang/Exception
    //   491	496	757	finally
    //   498	503	757	finally
    //   509	514	757	finally
    //   518	523	757	finally
    //   525	530	757	finally
    //   532	537	757	finally
    //   581	585	757	finally
    //   587	594	757	finally
    //   596	599	757	finally
    //   601	606	757	finally
    //   608	613	757	finally
    //   621	626	757	finally
    //   628	633	757	finally
    //   639	646	757	finally
    //   648	656	757	finally
    //   677	682	762	finally
    //   455	458	771	java/lang/Exception
    //   460	463	771	java/lang/Exception
    //   470	475	771	java/lang/Exception
    //   479	484	771	java/lang/Exception
    //   486	491	771	java/lang/Exception
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/utils/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */