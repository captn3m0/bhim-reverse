package in.org.npci.upiapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class c
{
  private final SharedPreferences a;
  
  public c(Context paramContext)
  {
    SharedPreferences localSharedPreferences = paramContext.getSharedPreferences("BHIMPreferences", 0);
    a = localSharedPreferences;
  }
  
  public void a(Context paramContext)
  {
    paramContext.getSharedPreferences("BHIMPreferences", 0).edit().clear().commit();
  }
  
  public void a(String paramString)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.remove(paramString);
    localEditor.commit();
  }
  
  public void a(String paramString1, String paramString2)
  {
    SharedPreferences.Editor localEditor = a.edit();
    localEditor.putString(paramString1, paramString2);
    localEditor.commit();
  }
  
  public String b(String paramString1, String paramString2)
  {
    return a.getString(paramString1, paramString2);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/utils/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */