package in.org.npci.upiapp.utils;

import android.net.SSLCertificateSocketFactory;
import android.os.Build.VERSION;
import in.org.npci.upiapp.a.a;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.StrictHostnameVerifier;
import org.apache.http.params.HttpParams;

public class e
  implements LayeredSocketFactory
{
  private static final HostnameVerifier a = SSLSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER;
  private static final StrictHostnameVerifier b;
  private final boolean c = false;
  private final boolean d = false;
  private final String e = null;
  
  static
  {
    StrictHostnameVerifier localStrictHostnameVerifier = new org/apache/http/conn/ssl/StrictHostnameVerifier;
    localStrictHostnameVerifier.<init>();
    b = localStrictHostnameVerifier;
  }
  
  public Socket connectSocket(Socket paramSocket, String paramString, int paramInt1, InetAddress paramInetAddress, int paramInt2, HttpParams paramHttpParams)
  {
    return null;
  }
  
  public Socket createSocket()
  {
    return null;
  }
  
  public Socket createSocket(Socket paramSocket, String paramString, int paramInt, boolean paramBoolean)
  {
    int i = 1;
    String str = null;
    if (paramBoolean) {
      paramSocket.close();
    }
    Object localObject1 = (SSLCertificateSocketFactory)SSLCertificateSocketFactory.getDefault(0);
    Object localObject3 = InetAddress.getByName(paramString);
    localObject3 = (SSLSocket)((SSLCertificateSocketFactory)localObject1).createSocket((InetAddress)localObject3, paramInt);
    Object localObject4 = ((SSLSocket)localObject3).getSupportedProtocols();
    ((SSLSocket)localObject3).setEnabledProtocols((String[])localObject4);
    localObject4 = new String[i];
    Object localObject5 = "TLSv1.2";
    localObject4[0] = localObject5;
    ((SSLSocket)localObject3).setEnabledProtocols((String[])localObject4);
    int j = Build.VERSION.SDK_INT;
    i = 17;
    if (j >= i) {
      ((SSLCertificateSocketFactory)localObject1).setHostname((Socket)localObject3, paramString);
    }
    Object localObject2;
    for (;;)
    {
      localObject1 = ((SSLSocket)localObject3).getSession();
      bool1 = d;
      if (bool1) {
        break;
      }
      localObject4 = e;
      if (localObject4 != null) {
        break;
      }
      localObject4 = a;
      bool1 = ((HostnameVerifier)localObject4).verify(paramString, (SSLSession)localObject1);
      if (bool1) {
        break;
      }
      localObject1 = new javax/net/ssl/SSLPeerUnverifiedException;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = "Cannot verify hostname: " + paramString;
      ((SSLPeerUnverifiedException)localObject1).<init>((String)localObject3);
      throw ((Throwable)localObject1);
      try
      {
        localObject1 = localObject3.getClass();
        localObject4 = "setHostname";
        i = 1;
        localObject5 = new Class[i];
        str = null;
        Class localClass = String.class;
        localObject5[0] = localClass;
        localObject1 = ((Class)localObject1).getMethod((String)localObject4, (Class[])localObject5);
        bool1 = true;
        localObject4 = new Object[bool1];
        i = 0;
        localObject5 = null;
        localObject4[0] = paramString;
        ((Method)localObject1).invoke(localObject3, (Object[])localObject4);
      }
      catch (Exception localException)
      {
        localObject4 = e.class.getSimpleName();
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        str = "SNI not usable: ";
        localObject5 = ((StringBuilder)localObject5).append(str);
        localObject2 = localException;
        a.b((String)localObject4, (String)localObject2);
      }
    }
    boolean bool1 = d;
    if (!bool1)
    {
      localObject4 = e;
      if (localObject4 == null)
      {
        bool1 = c;
        if (bool1)
        {
          localObject4 = b;
          boolean bool2 = ((StrictHostnameVerifier)localObject4).verify(paramString, (SSLSession)localObject2);
          if (!bool2)
          {
            localObject2 = new javax/net/ssl/SSLPeerUnverifiedException;
            localObject3 = new java/lang/StringBuilder;
            ((StringBuilder)localObject3).<init>();
            localObject3 = "Cannot verify hostname strict : " + paramString;
            ((SSLPeerUnverifiedException)localObject2).<init>((String)localObject3);
            throw ((Throwable)localObject2);
          }
        }
      }
    }
    return (Socket)localObject3;
  }
  
  public boolean isSecure(Socket paramSocket)
  {
    boolean bool = paramSocket instanceof SSLSocket;
    if (bool) {}
    for (bool = ((Socket)paramSocket).isConnected();; bool = false) {
      return bool;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/utils/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */