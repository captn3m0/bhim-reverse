package in.org.npci.upiapp.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import in.org.npci.upiapp.a.a;
import in.org.npci.upiapp.models.ApiResponse;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import javax.net.ssl.SSLHandshakeException;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.scheme.SocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.conn.ssl.X509HostnameVerifier;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.util.EntityUtils;

public class RestClient
{
  private static final String a = RestClient.class.getName();
  private static DefaultHttpClient b;
  private static DefaultHttpClient c;
  
  public static ApiResponse a(Context paramContext, String paramString, Map paramMap)
  {
    int i = 304;
    Object localObject1 = new org/apache/http/client/methods/HttpGet;
    ((HttpGet)localObject1).<init>();
    Object localObject2 = paramMap.entrySet();
    Object localObject3 = ((Set)localObject2).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject3).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject3).next();
      localObject4 = (String)((Map.Entry)localObject2).getKey();
      localObject2 = (String)((Map.Entry)localObject2).getValue();
      ((HttpGet)localObject1).setHeader((String)localObject4, (String)localObject2);
    }
    localObject2 = URI.create(paramString);
    ((HttpGet)localObject1).setURI((URI)localObject2);
    localObject2 = b.execute((HttpUriRequest)localObject1);
    Object localObject4 = a;
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = ((StringBuilder)localObject3).append(paramString).append(" ");
    int k = ((HttpResponse)localObject2).getStatusLine().getStatusCode();
    localObject3 = ((StringBuilder)localObject3).append(k);
    String str = "";
    localObject3 = str;
    a.a((String)localObject4, (String)localObject3);
    localObject4 = ((HttpResponse)localObject2).getStatusLine();
    int m = ((StatusLine)localObject4).getStatusCode();
    int n = 200;
    if (m == n) {
      localObject2 = a((HttpRequestBase)localObject1, (HttpResponse)localObject2);
    }
    for (;;)
    {
      return (ApiResponse)localObject2;
      localObject4 = ((HttpResponse)localObject2).getStatusLine();
      m = ((StatusLine)localObject4).getStatusCode();
      int j;
      if (m == i)
      {
        j = paramString.lastIndexOf("/") + 1;
        localObject4 = d.a(paramString.substring(j));
        localObject2 = new in/org/npci/upiapp/models/ApiResponse;
        ((ApiResponse)localObject2).<init>();
        ((ApiResponse)localObject2).setStatusCode(i);
        localObject4 = b.b(paramContext, (String)localObject4);
        ((ApiResponse)localObject2).setData((byte[])localObject4);
      }
      else
      {
        localObject4 = a;
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        localObject1 = ((StringBuilder)localObject1).append("Error While Downloading ").append(paramString);
        localObject3 = " . Response Code:  ";
        localObject1 = ((StringBuilder)localObject1).append((String)localObject3);
        j = ((HttpResponse)localObject2).getStatusLine().getStatusCode();
        localObject2 = j;
        a.a((String)localObject4, (String)localObject2);
        j = 0;
        localObject2 = null;
      }
    }
  }
  
  public static ApiResponse a(String paramString1, String paramString2, HashMap paramHashMap, boolean paramBoolean)
  {
    HttpPost localHttpPost = new org/apache/http/client/methods/HttpPost;
    localHttpPost.<init>(paramString1);
    if (paramHashMap != null)
    {
      localObject1 = paramHashMap.entrySet();
      Iterator localIterator = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (Map.Entry)localIterator.next();
        localObject2 = (String)((Map.Entry)localObject1).getKey();
        Object localObject3 = (String)((Map.Entry)localObject1).getValue();
        localHttpPost.setHeader((String)localObject2, (String)localObject3);
        localObject2 = a;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = ((StringBuilder)localObject3).append("RestClientHeader : Header -> ");
        Object localObject4 = ((Map.Entry)localObject1).getKey();
        localObject3 = ((StringBuilder)localObject3).append(localObject4);
        localObject4 = "=>";
        localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
        localObject1 = ((Map.Entry)localObject1).getValue();
        localObject1 = localObject1;
        a.a((String)localObject2, (String)localObject1);
      }
    }
    a.a("RestClient", paramString2);
    Object localObject1 = paramString2.getBytes("UTF-8");
    Object localObject2 = new org/apache/http/entity/ByteArrayEntity;
    ((ByteArrayEntity)localObject2).<init>((byte[])localObject1);
    localHttpPost.setEntity((HttpEntity)localObject2);
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = "<URL>" + paramString1 + "<HEADER>" + paramHashMap + "<BODY>" + paramString2;
    a.a("REQUEST HTTP POST", (String)localObject2);
    return a(localHttpPost, paramBoolean);
  }
  
  public static ApiResponse a(String paramString, HashMap paramHashMap, boolean paramBoolean)
  {
    HttpGet localHttpGet = new org/apache/http/client/methods/HttpGet;
    localHttpGet.<init>(paramString);
    if (paramHashMap != null)
    {
      Object localObject1 = paramHashMap.entrySet();
      Iterator localIterator = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (Map.Entry)localIterator.next();
        String str = (String)((Map.Entry)localObject1).getKey();
        Object localObject2 = (String)((Map.Entry)localObject1).getValue();
        localHttpGet.setHeader(str, (String)localObject2);
        str = a;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("RestClientHeader : Header -> ");
        Object localObject3 = ((Map.Entry)localObject1).getKey();
        localObject2 = ((StringBuilder)localObject2).append(localObject3);
        localObject3 = "=>";
        localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
        localObject1 = ((Map.Entry)localObject1).getValue();
        localObject1 = localObject1;
        a.a(str, (String)localObject1);
      }
    }
    return a(localHttpGet, paramBoolean);
  }
  
  public static ApiResponse a(String paramString, Map paramMap, HashMap paramHashMap, boolean paramBoolean)
  {
    try
    {
      return a(paramString, paramHashMap, paramBoolean);
    }
    catch (Exception localException)
    {
      RuntimeException localRuntimeException = new java/lang/RuntimeException;
      String str = localException.getMessage();
      localRuntimeException.<init>(str);
      throw localRuntimeException;
    }
  }
  
  private static ApiResponse a(HttpRequestBase paramHttpRequestBase, HttpResponse paramHttpResponse)
  {
    for (;;)
    {
      Object localObject2;
      try
      {
        boolean bool1 = a(paramHttpResponse);
        Object localObject4;
        Object localObject5;
        if (bool1)
        {
          ApiResponse localApiResponse = new in/org/npci/upiapp/models/ApiResponse;
          localApiResponse.<init>();
          localObject3 = paramHttpResponse.getStatusLine();
          int i = ((StatusLine)localObject3).getStatusCode();
          localApiResponse.setStatusCode(i);
          localObject3 = paramHttpResponse.getEntity();
          localObject3 = ((HttpEntity)localObject3).getContentEncoding();
          if (localObject3 != null)
          {
            localObject3 = ((Header)localObject3).getValue();
            localObject4 = "gzip";
            boolean bool2 = ((String)localObject3).equals(localObject4);
            if (bool2)
            {
              localObject3 = a;
              localObject4 = "GZIP Header Present";
              a.a((String)localObject3, (String)localObject4);
              localObject3 = new java/util/zip/GZIPInputStream;
              localObject4 = paramHttpResponse.getEntity();
              localObject4 = ((HttpEntity)localObject4).getContent();
              ((GZIPInputStream)localObject3).<init>((InputStream)localObject4);
              int k = 1024;
              localObject4 = new byte[k];
              localObject5 = new java/io/ByteArrayOutputStream;
              ((ByteArrayOutputStream)localObject5).<init>();
              try
              {
                int m = ((GZIPInputStream)localObject3).read((byte[])localObject4);
                if (m >= 0)
                {
                  ((ByteArrayOutputStream)localObject5).write((byte[])localObject4, 0, m);
                  continue;
                }
              }
              finally
              {
                if (localObject3 != null)
                {
                  localObject4 = a;
                  localObject5 = "CLOSING GZIP STREAM";
                  a.a((String)localObject4, (String)localObject5);
                  ((GZIPInputStream)localObject3).close();
                }
              }
            }
          }
        }
        String str;
        localObject2 = new in/org/npci/upiapp/models/ApiResponse;
      }
      catch (UnsupportedEncodingException localUnsupportedEncodingException)
      {
        localObject3 = new java/lang/RuntimeException;
        str = localUnsupportedEncodingException.getMessage();
        ((RuntimeException)localObject3).<init>(str);
        throw ((Throwable)localObject3);
        localObject4 = ((ByteArrayOutputStream)localObject5).toByteArray();
        str.setData((byte[])localObject4);
        if (localObject3 != null)
        {
          localObject4 = a;
          localObject5 = "CLOSING GZIP STREAM";
          a.a((String)localObject4, (String)localObject5);
          ((GZIPInputStream)localObject3).close();
        }
        return str;
        localObject3 = paramHttpResponse.getEntity();
        localObject3 = EntityUtils.toByteArray((HttpEntity)localObject3);
        str.setData((byte[])localObject3);
        continue;
      }
      catch (ClientProtocolException localClientProtocolException)
      {
        localObject3 = new java/lang/RuntimeException;
        localObject2 = localClientProtocolException.getMessage();
        ((RuntimeException)localObject3).<init>((String)localObject2);
        throw ((Throwable)localObject3);
      }
      ((ApiResponse)localObject2).<init>();
      Object localObject3 = paramHttpResponse.getStatusLine();
      int j = ((StatusLine)localObject3).getStatusCode();
      ((ApiResponse)localObject2).setStatusCode(j);
      j = 0;
      localObject3 = null;
      ((ApiResponse)localObject2).setData(null);
    }
  }
  
  private static ApiResponse a(HttpRequestBase paramHttpRequestBase, boolean paramBoolean)
  {
    localObject1 = a;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = ((StringBuilder)localObject2).append("Executing ");
    localObject3 = paramHttpRequestBase.getMethod();
    localObject2 = ((StringBuilder)localObject2).append((String)localObject3).append(" ");
    localObject3 = paramHttpRequestBase.getURI();
    localObject2 = localObject3;
    a.a((String)localObject1, (String)localObject2);
    localObject1 = "Accept-Encoding";
    localObject2 = "gzip";
    for (;;)
    {
      try
      {
        paramHttpRequestBase.setHeader((String)localObject1, (String)localObject2);
        if (!paramBoolean) {
          continue;
        }
        localObject1 = c;
        localObject1 = ((DefaultHttpClient)localObject1).execute(paramHttpRequestBase);
        localObject2 = a;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        Object localObject4 = "Got response for ";
        localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
        localObject4 = paramHttpRequestBase.getURI();
        localObject3 = ((StringBuilder)localObject3).append(localObject4);
        localObject4 = ", response code ";
        localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
        localObject4 = ((HttpResponse)localObject1).getStatusLine();
        int i = ((StatusLine)localObject4).getStatusCode();
        localObject3 = ((StringBuilder)localObject3).append(i);
        localObject3 = ((StringBuilder)localObject3).toString();
        a.a((String)localObject2, (String)localObject3);
        localObject1 = a(paramHttpRequestBase, (HttpResponse)localObject1);
      }
      catch (SSLHandshakeException localSSLHandshakeException)
      {
        localObject1 = localSSLHandshakeException.getMessage();
        if (localObject1 == null) {
          continue;
        }
        a.b(a, (String)localObject1);
        localObject2 = "java.security.cert.CertPathValidatorException";
        boolean bool = ((String)localObject1).contains((CharSequence)localObject2);
        if (!bool) {
          continue;
        }
        localObject1 = new android/os/Handler;
        localObject2 = Looper.getMainLooper();
        ((Handler)localObject1).<init>((Looper)localObject2);
        localObject2 = new in/org/npci/upiapp/utils/RestClient$1;
        ((RestClient.1)localObject2).<init>();
        ((Handler)localObject1).post((Runnable)localObject2);
        bool = false;
        localObject1 = null;
        continue;
      }
      catch (Exception localException)
      {
        localObject1 = new in/org/npci/upiapp/models/ApiResponse;
        ((ApiResponse)localObject1).<init>();
        int j = -1;
        ((ApiResponse)localObject1).setStatusCode(j);
        localObject3 = localException.getMessage().getBytes();
        ((ApiResponse)localObject1).setData((byte[])localObject3);
        a.a(localException);
        continue;
      }
      return (ApiResponse)localObject1;
      localObject1 = b;
      localObject1 = ((DefaultHttpClient)localObject1).execute(paramHttpRequestBase);
    }
  }
  
  public static String a(Map paramMap)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    int i = paramMap.size();
    if (i > 0)
    {
      localObject1 = "?";
      localStringBuilder.append((String)localObject1);
    }
    Object localObject1 = paramMap.entrySet();
    Iterator localIterator = ((Set)localObject1).iterator();
    boolean bool = localIterator.hasNext();
    if (bool)
    {
      localObject1 = (Map.Entry)localIterator.next();
      Object localObject2 = (String)((Map.Entry)localObject1).getKey();
      localStringBuilder.append((String)localObject2);
      localStringBuilder.append("=");
      localObject2 = ((Map.Entry)localObject1).getValue();
      if (localObject2 == null)
      {
        localObject1 = "";
        localStringBuilder.append((String)localObject1);
      }
      for (;;)
      {
        localObject1 = "&";
        localStringBuilder.append((String)localObject1);
        break;
        localObject1 = (String)((Map.Entry)localObject1).getValue();
        localObject2 = "utf-8";
        localObject1 = URLEncoder.encode((String)localObject1, (String)localObject2);
        localStringBuilder.append((String)localObject1);
      }
    }
    return localStringBuilder.toString();
  }
  
  private static DefaultHttpClient a(Context paramContext, HttpParams paramHttpParams)
  {
    Object localObject1 = new org/apache/http/conn/scheme/SchemeRegistry;
    ((SchemeRegistry)localObject1).<init>();
    SSLSocketFactory localSSLSocketFactory = SSLSocketFactory.getSocketFactory();
    Object localObject2 = (X509HostnameVerifier)SSLSocketFactory.STRICT_HOSTNAME_VERIFIER;
    localSSLSocketFactory.setHostnameVerifier((X509HostnameVerifier)localObject2);
    localObject2 = new org/apache/http/conn/scheme/Scheme;
    Object localObject3 = b(paramContext);
    ((Scheme)localObject2).<init>("https", (SocketFactory)localObject3, 443);
    ((SchemeRegistry)localObject1).register((Scheme)localObject2);
    localObject2 = new org/apache/http/conn/scheme/Scheme;
    localObject3 = PlainSocketFactory.getSocketFactory();
    ((Scheme)localObject2).<init>("http", (SocketFactory)localObject3, 80);
    ((SchemeRegistry)localObject1).register((Scheme)localObject2);
    localObject2 = new org/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    ((ThreadSafeClientConnManager)localObject2).<init>(paramHttpParams, (SchemeRegistry)localObject1);
    localObject1 = new org/apache/http/impl/client/DefaultHttpClient;
    ((DefaultHttpClient)localObject1).<init>((ClientConnectionManager)localObject2, paramHttpParams);
    return (DefaultHttpClient)localObject1;
  }
  
  private static DefaultHttpClient a(HttpParams paramHttpParams)
  {
    Object localObject1 = new org/apache/http/conn/scheme/SchemeRegistry;
    ((SchemeRegistry)localObject1).<init>();
    Object localObject2 = new org/apache/http/conn/scheme/Scheme;
    Object localObject3 = new in/org/npci/upiapp/utils/e;
    ((e)localObject3).<init>();
    ((Scheme)localObject2).<init>("https", (SocketFactory)localObject3, 443);
    ((SchemeRegistry)localObject1).register((Scheme)localObject2);
    localObject2 = new org/apache/http/conn/scheme/Scheme;
    localObject3 = PlainSocketFactory.getSocketFactory();
    ((Scheme)localObject2).<init>("http", (SocketFactory)localObject3, 80);
    ((SchemeRegistry)localObject1).register((Scheme)localObject2);
    localObject2 = new org/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;
    ((ThreadSafeClientConnManager)localObject2).<init>(paramHttpParams, (SchemeRegistry)localObject1);
    localObject1 = new org/apache/http/impl/client/DefaultHttpClient;
    ((DefaultHttpClient)localObject1).<init>((ClientConnectionManager)localObject2, paramHttpParams);
    return (DefaultHttpClient)localObject1;
  }
  
  public static void a(Context paramContext)
  {
    int i = 30000;
    a.a(a, "Default http client");
    BasicHttpParams localBasicHttpParams = new org/apache/http/params/BasicHttpParams;
    localBasicHttpParams.<init>();
    HttpProtocolParams.setContentCharset(localBasicHttpParams, "UTF-8");
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 10000);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, i);
    b = a(localBasicHttpParams);
    localBasicHttpParams = new org/apache/http/params/BasicHttpParams;
    localBasicHttpParams.<init>();
    HttpProtocolParams.setContentCharset(localBasicHttpParams, "UTF-8");
    HttpConnectionParams.setConnectionTimeout(localBasicHttpParams, 15000);
    HttpConnectionParams.setSoTimeout(localBasicHttpParams, i);
    c = a(paramContext, localBasicHttpParams);
  }
  
  private static boolean a(HttpResponse paramHttpResponse)
  {
    StatusLine localStatusLine = paramHttpResponse.getStatusLine();
    int i = localStatusLine.getStatusCode();
    int k = 200;
    if (i >= k)
    {
      int m = 300;
      if (i < m) {
        i = 1;
      }
    }
    for (;;)
    {
      return i;
      int j = 0;
      localStatusLine = null;
    }
  }
  
  public static ApiResponse b(String paramString, HashMap paramHashMap, boolean paramBoolean)
  {
    HttpDelete localHttpDelete = new org/apache/http/client/methods/HttpDelete;
    localHttpDelete.<init>(paramString);
    if (paramHashMap != null)
    {
      Object localObject1 = paramHashMap.entrySet();
      Iterator localIterator = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (Map.Entry)localIterator.next();
        String str = (String)((Map.Entry)localObject1).getKey();
        Object localObject2 = (String)((Map.Entry)localObject1).getValue();
        localHttpDelete.setHeader(str, (String)localObject2);
        str = a;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("RestClientHeader : Header -> ");
        Object localObject3 = ((Map.Entry)localObject1).getKey();
        localObject2 = ((StringBuilder)localObject2).append(localObject3);
        localObject3 = "=>";
        localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
        localObject1 = ((Map.Entry)localObject1).getValue();
        localObject1 = localObject1;
        a.a(str, (String)localObject1);
      }
    }
    return a(localHttpDelete, paramBoolean);
  }
  
  public static ApiResponse b(String paramString, Map paramMap, HashMap paramHashMap, boolean paramBoolean)
  {
    String str1 = "URL";
    try
    {
      a.a(str1, paramString);
      str1 = "PARAMETER";
      localObject = a(paramMap);
      a.a(str1, (String)localObject);
      return b(paramString, paramHashMap, paramBoolean);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      Object localObject = new java/lang/RuntimeException;
      String str2 = localUnsupportedEncodingException.getMessage();
      ((RuntimeException)localObject).<init>(str2);
      throw ((Throwable)localObject);
    }
  }
  
  /* Error */
  private static SSLSocketFactory b(Context paramContext)
  {
    // Byte code:
    //   0: ldc_w 442
    //   3: astore_1
    //   4: aload_1
    //   5: invokestatic 448	java/security/KeyStore:getInstance	(Ljava/lang/String;)Ljava/security/KeyStore;
    //   8: astore_1
    //   9: aload_0
    //   10: invokevirtual 454	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   13: astore_2
    //   14: ldc_w 455
    //   17: istore_3
    //   18: aload_2
    //   19: iload_3
    //   20: invokevirtual 462	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
    //   23: astore_2
    //   24: ldc_w 464
    //   27: astore 4
    //   29: aload 4
    //   31: invokevirtual 468	java/lang/String:toCharArray	()[C
    //   34: astore 4
    //   36: aload_1
    //   37: aload_2
    //   38: aload 4
    //   40: invokevirtual 472	java/security/KeyStore:load	(Ljava/io/InputStream;[C)V
    //   43: aload_2
    //   44: invokevirtual 475	java/io/InputStream:close	()V
    //   47: new 348	org/apache/http/conn/ssl/SSLSocketFactory
    //   50: astore_2
    //   51: aload_2
    //   52: aload_1
    //   53: invokespecial 478	org/apache/http/conn/ssl/SSLSocketFactory:<init>	(Ljava/security/KeyStore;)V
    //   56: getstatic 356	org/apache/http/conn/ssl/SSLSocketFactory:STRICT_HOSTNAME_VERIFIER	Lorg/apache/http/conn/ssl/X509HostnameVerifier;
    //   59: astore_1
    //   60: aload_2
    //   61: aload_1
    //   62: invokevirtual 362	org/apache/http/conn/ssl/SSLSocketFactory:setHostnameVerifier	(Lorg/apache/http/conn/ssl/X509HostnameVerifier;)V
    //   65: aload_2
    //   66: areturn
    //   67: astore_1
    //   68: aload_2
    //   69: invokevirtual 475	java/io/InputStream:close	()V
    //   72: aload_1
    //   73: athrow
    //   74: astore_1
    //   75: new 480	java/lang/AssertionError
    //   78: astore_2
    //   79: aload_2
    //   80: aload_1
    //   81: invokespecial 483	java/lang/AssertionError:<init>	(Ljava/lang/Object;)V
    //   84: aload_2
    //   85: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	86	0	paramContext	Context
    //   3	59	1	localObject1	Object
    //   67	6	1	localObject2	Object
    //   74	7	1	localException	Exception
    //   13	72	2	localObject3	Object
    //   17	3	3	i	int
    //   27	12	4	localObject4	Object
    // Exception table:
    //   from	to	target	type
    //   29	34	67	finally
    //   38	43	67	finally
    //   4	8	74	java/lang/Exception
    //   9	13	74	java/lang/Exception
    //   19	23	74	java/lang/Exception
    //   43	47	74	java/lang/Exception
    //   47	50	74	java/lang/Exception
    //   52	56	74	java/lang/Exception
    //   56	59	74	java/lang/Exception
    //   61	65	74	java/lang/Exception
    //   68	72	74	java/lang/Exception
    //   72	74	74	java/lang/Exception
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/utils/RestClient.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */