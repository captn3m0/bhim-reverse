package in.org.npci.upiapp.a;

import android.util.Log;
import com.crashlytics.android.Crashlytics;
import in.org.npci.upiapp.UpiApp;

public class a
{
  public static void a(String paramString1, String paramString2)
  {
    a(paramString1, paramString2, false);
  }
  
  public static void a(String paramString1, String paramString2, Throwable paramThrowable)
  {
    boolean bool = UpiApp.a;
    if (bool)
    {
      int i = 6;
      Crashlytics.log(i, paramString1, paramString2);
    }
    a(paramThrowable);
  }
  
  public static void a(String paramString1, String paramString2, boolean paramBoolean)
  {
    boolean bool = UpiApp.a;
    if (bool)
    {
      int i = 3;
      Crashlytics.log(i, paramString1, paramString2);
    }
    for (;;)
    {
      return;
      if (paramBoolean) {
        Crashlytics.log(paramString2);
      }
    }
  }
  
  public static void a(Throwable paramThrowable)
  {
    boolean bool = UpiApp.a;
    if (bool)
    {
      String str1 = "Exception";
      String str2 = "Exception";
      Log.e(str1, str2, paramThrowable);
    }
    for (;;)
    {
      return;
      Crashlytics.logException(paramThrowable);
    }
  }
  
  public static void b(String paramString1, String paramString2)
  {
    b(paramString1, paramString2, true);
  }
  
  public static void b(String paramString1, String paramString2, boolean paramBoolean)
  {
    boolean bool = UpiApp.a;
    if (bool)
    {
      int i = 6;
      Crashlytics.log(i, paramString1, paramString2);
    }
    for (;;)
    {
      return;
      if (paramBoolean) {
        Crashlytics.log(paramString2);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/in/org/npci/upiapp/a/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */