package io.fabric.sdk.android;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import io.fabric.sdk.android.services.b.g;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.e.e;
import io.fabric.sdk.android.services.e.n;
import io.fabric.sdk.android.services.e.q;
import io.fabric.sdk.android.services.e.t;
import io.fabric.sdk.android.services.e.y;
import io.fabric.sdk.android.services.network.b;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Future;

class l
  extends h
{
  private final io.fabric.sdk.android.services.network.d a;
  private PackageManager b;
  private String c;
  private PackageInfo d;
  private String e;
  private String f;
  private String g;
  private String h;
  private String i;
  private final Future j;
  private final Collection k;
  
  public l(Future paramFuture, Collection paramCollection)
  {
    b localb = new io/fabric/sdk/android/services/network/b;
    localb.<init>();
    a = localb;
    j = paramFuture;
    k = paramCollection;
  }
  
  private io.fabric.sdk.android.services.e.d a(n paramn, Collection paramCollection)
  {
    Object localObject1 = getContext();
    Object localObject2 = new io/fabric/sdk/android/services/b/g;
    ((g)localObject2).<init>();
    localObject2 = ((g)localObject2).a((Context)localObject1);
    localObject1 = i.m((Context)localObject1);
    Object localObject3 = new String[1];
    localObject3[0] = localObject1;
    String str1 = i.a((String[])localObject3);
    int m = io.fabric.sdk.android.services.b.l.a(g).a();
    localObject3 = getIdManager().c();
    localObject1 = new io/fabric/sdk/android/services/e/d;
    String str2 = f;
    String str3 = e;
    String str4 = h;
    String str5 = i;
    ((io.fabric.sdk.android.services.e.d)localObject1).<init>((String)localObject2, (String)localObject3, str2, str3, str1, str4, m, str5, "0", paramn, paramCollection);
    return (io.fabric.sdk.android.services.e.d)localObject1;
  }
  
  private boolean a(e parame, n paramn, Collection paramCollection)
  {
    io.fabric.sdk.android.services.e.d locald = a(paramn, paramCollection);
    y localy = new io/fabric/sdk/android/services/e/y;
    String str1 = b();
    String str2 = c;
    io.fabric.sdk.android.services.network.d locald1 = a;
    localy.<init>(this, str1, str2, locald1);
    return localy.a(locald);
  }
  
  private boolean a(String paramString, e parame, Collection paramCollection)
  {
    boolean bool1 = true;
    Object localObject1 = "new";
    String str1 = b;
    boolean bool2 = ((String)localObject1).equals(str1);
    Object localObject2;
    if (bool2)
    {
      bool1 = b(paramString, parame, paramCollection);
      if (bool1)
      {
        localObject2 = q.a();
        bool1 = ((q)localObject2).d();
      }
    }
    for (;;)
    {
      return bool1;
      localObject2 = c.h();
      localObject1 = "Fabric";
      str1 = "Failed to create app with Crashlytics service.";
      String str2 = null;
      ((k)localObject2).e((String)localObject1, str1, null);
      bool1 = false;
      localObject2 = null;
      continue;
      localObject1 = "configured";
      str1 = b;
      bool2 = ((String)localObject1).equals(str1);
      if (bool2)
      {
        localObject2 = q.a();
        bool1 = ((q)localObject2).d();
      }
      else
      {
        bool2 = e;
        if (bool2)
        {
          localObject1 = c.h();
          str1 = "Fabric";
          str2 = "Server says an update is required - forcing a full App update.";
          ((k)localObject1).a(str1, str2);
          c(paramString, parame, paramCollection);
        }
      }
    }
  }
  
  private boolean b(String paramString, e parame, Collection paramCollection)
  {
    Object localObject = n.a(getContext(), paramString);
    localObject = a((n)localObject, paramCollection);
    io.fabric.sdk.android.services.e.h localh = new io/fabric/sdk/android/services/e/h;
    String str1 = b();
    String str2 = c;
    io.fabric.sdk.android.services.network.d locald = a;
    localh.<init>(this, str1, str2, locald);
    return localh.a((io.fabric.sdk.android.services.e.d)localObject);
  }
  
  private t c()
  {
    try
    {
      localObject1 = q.a();
      localObject3 = idManager;
      localObject4 = a;
      String str1 = e;
      String str2 = f;
      String str3 = b();
      localObject5 = this;
      localObject1 = ((q)localObject1).a(this, (o)localObject3, (io.fabric.sdk.android.services.network.d)localObject4, str1, str2, str3);
      ((q)localObject1).c();
      localObject1 = q.a();
      localObject1 = ((q)localObject1).b();
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject5 = c.h();
        Object localObject3 = "Fabric";
        Object localObject4 = "Error dealing with settings";
        ((k)localObject5).e((String)localObject3, (String)localObject4, localException);
        Object localObject2 = null;
      }
    }
    return (t)localObject1;
  }
  
  private boolean c(String paramString, e parame, Collection paramCollection)
  {
    n localn = n.a(getContext(), paramString);
    return a(parame, localn, paramCollection);
  }
  
  protected Boolean a()
  {
    Object localObject1 = getContext();
    Object localObject3 = i.k((Context)localObject1);
    Object localObject4 = c();
    if (localObject4 != null) {}
    for (;;)
    {
      try
      {
        localObject1 = j;
        Object localObject5;
        if (localObject1 != null)
        {
          localObject1 = j;
          localObject1 = ((Future)localObject1).get();
          localObject1 = (Map)localObject1;
          localObject5 = k;
          localObject1 = a((Map)localObject1, (Collection)localObject5);
          localObject4 = a;
          localObject1 = ((Map)localObject1).values();
          bool = a((String)localObject3, (e)localObject4, (Collection)localObject1);
          return Boolean.valueOf(bool);
        }
        localObject1 = new java/util/HashMap;
        ((HashMap)localObject1).<init>();
        continue;
        boolean bool = false;
      }
      catch (Exception localException)
      {
        localObject3 = c.h();
        localObject4 = "Fabric";
        localObject5 = "Error performing auto configuration.";
        ((k)localObject3).e((String)localObject4, (String)localObject5, localException);
      }
      Object localObject2 = null;
    }
  }
  
  Map a(Map paramMap, Collection paramCollection)
  {
    Iterator localIterator = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (h)localIterator.next();
      String str1 = ((h)localObject).getIdentifier();
      boolean bool2 = paramMap.containsKey(str1);
      if (!bool2)
      {
        str1 = ((h)localObject).getIdentifier();
        j localj = new io/fabric/sdk/android/j;
        String str2 = ((h)localObject).getIdentifier();
        localObject = ((h)localObject).getVersion();
        String str3 = "binary";
        localj.<init>(str2, (String)localObject, str3);
        paramMap.put(str1, localj);
      }
    }
    return paramMap;
  }
  
  String b()
  {
    return i.b(getContext(), "com.crashlytics.ApiEndpoint");
  }
  
  public String getIdentifier()
  {
    return "io.fabric.sdk.android:fabric";
  }
  
  public String getVersion()
  {
    return "1.3.14.143";
  }
  
  protected boolean onPreExecute()
  {
    boolean bool = false;
    for (;;)
    {
      try
      {
        localObject1 = getIdManager();
        localObject1 = ((o)localObject1).j();
        g = ((String)localObject1);
        localObject1 = getContext();
        localObject1 = ((Context)localObject1).getPackageManager();
        b = ((PackageManager)localObject1);
        localObject1 = getContext();
        localObject1 = ((Context)localObject1).getPackageName();
        c = ((String)localObject1);
        localObject1 = b;
        localObject2 = c;
        str1 = null;
        localObject1 = ((PackageManager)localObject1).getPackageInfo((String)localObject2, 0);
        d = ((PackageInfo)localObject1);
        localObject1 = d;
        int m = versionCode;
        localObject1 = Integer.toString(m);
        e = ((String)localObject1);
        localObject1 = d;
        localObject1 = versionName;
        if (localObject1 != null) {
          continue;
        }
        localObject1 = "0.0";
        f = ((String)localObject1);
        localObject1 = b;
        localObject2 = getContext();
        localObject2 = ((Context)localObject2).getApplicationInfo();
        localObject1 = ((PackageManager)localObject1).getApplicationLabel((ApplicationInfo)localObject2);
        localObject1 = ((CharSequence)localObject1).toString();
        h = ((String)localObject1);
        localObject1 = getContext();
        localObject1 = ((Context)localObject1).getApplicationInfo();
        m = targetSdkVersion;
        localObject1 = Integer.toString(m);
        i = ((String)localObject1);
        bool = true;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException)
      {
        Object localObject1;
        Object localObject2 = c.h();
        String str1 = "Fabric";
        String str2 = "Failed init";
        ((k)localObject2).e(str1, str2, localNameNotFoundException);
        continue;
      }
      return bool;
      localObject1 = d;
      localObject1 = versionName;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/l.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */