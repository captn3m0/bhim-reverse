package io.fabric.sdk.android;

public class InitializationException
  extends RuntimeException
{
  public InitializationException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/InitializationException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */