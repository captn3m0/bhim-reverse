package io.fabric.sdk.android;

import io.fabric.sdk.android.services.b.t;
import io.fabric.sdk.android.services.concurrency.e;

class g
  extends io.fabric.sdk.android.services.concurrency.f
{
  final h a;
  
  public g(h paramh)
  {
    a = paramh;
  }
  
  private t a(String paramString)
  {
    t localt = new io/fabric/sdk/android/services/b/t;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    String str = a.getIdentifier();
    localObject = str + "." + paramString;
    localt.<init>((String)localObject, "KitInitialization");
    localt.a();
    return localt;
  }
  
  protected Object a(Void... paramVarArgs)
  {
    t localt = a("doInBackground");
    Object localObject = null;
    boolean bool = d();
    if (!bool) {
      localObject = a.doInBackground();
    }
    localt.b();
    return localObject;
  }
  
  /* Error */
  protected void a()
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_1
    //   2: aload_0
    //   3: invokespecial 61	io/fabric/sdk/android/services/concurrency/f:a	()V
    //   6: ldc 63
    //   8: astore_2
    //   9: aload_0
    //   10: aload_2
    //   11: invokespecial 49	io/fabric/sdk/android/g:a	(Ljava/lang/String;)Lio/fabric/sdk/android/services/b/t;
    //   14: astore_3
    //   15: aload_0
    //   16: getfield 12	io/fabric/sdk/android/g:a	Lio/fabric/sdk/android/h;
    //   19: astore_2
    //   20: aload_2
    //   21: invokevirtual 65	io/fabric/sdk/android/h:onPreExecute	()Z
    //   24: istore 4
    //   26: aload_3
    //   27: invokevirtual 59	io/fabric/sdk/android/services/b/t:b	()V
    //   30: iload 4
    //   32: ifne +9 -> 41
    //   35: aload_0
    //   36: iload_1
    //   37: invokevirtual 68	io/fabric/sdk/android/g:a	(Z)Z
    //   40: pop
    //   41: return
    //   42: astore_2
    //   43: aload_2
    //   44: athrow
    //   45: astore_2
    //   46: aload_3
    //   47: invokevirtual 59	io/fabric/sdk/android/services/b/t:b	()V
    //   50: aload_0
    //   51: iload_1
    //   52: invokevirtual 68	io/fabric/sdk/android/g:a	(Z)Z
    //   55: pop
    //   56: aload_2
    //   57: athrow
    //   58: astore_2
    //   59: invokestatic 74	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   62: astore 5
    //   64: ldc 76
    //   66: astore 6
    //   68: ldc 78
    //   70: astore 7
    //   72: aload 5
    //   74: aload 6
    //   76: aload 7
    //   78: aload_2
    //   79: invokeinterface 84 4 0
    //   84: aload_3
    //   85: invokevirtual 59	io/fabric/sdk/android/services/b/t:b	()V
    //   88: aload_0
    //   89: iload_1
    //   90: invokevirtual 68	io/fabric/sdk/android/g:a	(Z)Z
    //   93: pop
    //   94: goto -53 -> 41
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	97	0	this	g
    //   1	89	1	bool1	boolean
    //   8	13	2	localObject1	Object
    //   42	2	2	localUnmetDependencyException	io.fabric.sdk.android.services.concurrency.UnmetDependencyException
    //   45	12	2	localObject2	Object
    //   58	21	2	localException	Exception
    //   14	71	3	localt	t
    //   24	7	4	bool2	boolean
    //   62	11	5	localk	k
    //   66	9	6	str1	String
    //   70	7	7	str2	String
    // Exception table:
    //   from	to	target	type
    //   15	19	42	io/fabric/sdk/android/services/concurrency/UnmetDependencyException
    //   20	24	42	io/fabric/sdk/android/services/concurrency/UnmetDependencyException
    //   15	19	45	finally
    //   20	24	45	finally
    //   43	45	45	finally
    //   59	62	45	finally
    //   78	84	45	finally
    //   15	19	58	java/lang/Exception
    //   20	24	58	java/lang/Exception
  }
  
  protected void a(Object paramObject)
  {
    a.onPostExecute(paramObject);
    a.initializationCallback.a(paramObject);
  }
  
  protected void b(Object paramObject)
  {
    a.onCancelled(paramObject);
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = a.getIdentifier();
    localObject1 = (String)localObject2 + " Initialization was cancelled";
    localObject2 = new io/fabric/sdk/android/InitializationException;
    ((InitializationException)localObject2).<init>((String)localObject1);
    a.initializationCallback.a((Exception)localObject2);
  }
  
  public e getPriority()
  {
    return e.c;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */