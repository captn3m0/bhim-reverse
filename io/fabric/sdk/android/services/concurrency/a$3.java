package io.fabric.sdk.android.services.concurrency;

import android.util.Log;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

class a$3
  extends FutureTask
{
  a$3(a parama, Callable paramCallable)
  {
    super(paramCallable);
  }
  
  protected void done()
  {
    try
    {
      a locala1 = a;
      localObject = get();
      a.b(locala1, localObject);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        localObject = "AsyncTask";
        Log.w((String)localObject, localInterruptedException);
      }
    }
    catch (ExecutionException localExecutionException)
    {
      localObject = new java/lang/RuntimeException;
      Throwable localThrowable = localExecutionException.getCause();
      ((RuntimeException)localObject).<init>("An error occured while executing doInBackground()", localThrowable);
      throw ((Throwable)localObject);
    }
    catch (CancellationException localCancellationException)
    {
      for (;;)
      {
        a locala2 = a;
        Object localObject = null;
        a.b(locala2, null);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/a$3.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */