package io.fabric.sdk.android.services.concurrency;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class c
  extends PriorityBlockingQueue
{
  final Queue a;
  private final ReentrantLock b;
  
  public c()
  {
    Object localObject = new java/util/LinkedList;
    ((LinkedList)localObject).<init>();
    a = ((Queue)localObject);
    localObject = new java/util/concurrent/locks/ReentrantLock;
    ((ReentrantLock)localObject).<init>();
    b = ((ReentrantLock)localObject);
  }
  
  public b a()
  {
    return b(0, null, null);
  }
  
  b a(int paramInt, Long paramLong, TimeUnit paramTimeUnit)
  {
    b localb;
    switch (paramInt)
    {
    default: 
      localb = null;
    }
    for (;;)
    {
      return localb;
      localb = (b)super.take();
      continue;
      localb = (b)super.peek();
      continue;
      localb = (b)super.poll();
      continue;
      long l = paramLong.longValue();
      localb = (b)super.poll(l, paramTimeUnit);
    }
  }
  
  public b a(long paramLong, TimeUnit paramTimeUnit)
  {
    Long localLong = Long.valueOf(paramLong);
    return b(3, localLong, paramTimeUnit);
  }
  
  boolean a(int paramInt, b paramb)
  {
    try
    {
      Object localObject1 = b;
      ((ReentrantLock)localObject1).lock();
      int i = 1;
      if (paramInt == i) {
        super.remove(paramb);
      }
      localObject1 = a;
      boolean bool = ((Queue)localObject1).offer(paramb);
      return bool;
    }
    finally
    {
      b.unlock();
    }
  }
  
  boolean a(b paramb)
  {
    return paramb.areDependenciesMet();
  }
  
  Object[] a(Object[] paramArrayOfObject1, Object[] paramArrayOfObject2)
  {
    int i = paramArrayOfObject1.length;
    int j = paramArrayOfObject2.length;
    Object localObject = paramArrayOfObject1.getClass().getComponentType();
    int k = i + j;
    localObject = (Object[])Array.newInstance((Class)localObject, k);
    System.arraycopy(paramArrayOfObject1, 0, localObject, 0, i);
    System.arraycopy(paramArrayOfObject2, 0, localObject, i, j);
    return (Object[])localObject;
  }
  
  public b b()
  {
    b localb = null;
    int i = 1;
    try
    {
      localb = b(i, null, null);
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
    return localb;
  }
  
  b b(int paramInt, Long paramLong, TimeUnit paramTimeUnit)
  {
    for (;;)
    {
      b localb = a(paramInt, paramLong, paramTimeUnit);
      if (localb != null)
      {
        boolean bool = a(localb);
        if (!bool) {}
      }
      else
      {
        return localb;
      }
      a(paramInt, localb);
    }
  }
  
  public b c()
  {
    b localb = null;
    int i = 2;
    try
    {
      localb = b(i, null, null);
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
    return localb;
  }
  
  public void clear()
  {
    try
    {
      Object localObject1 = b;
      ((ReentrantLock)localObject1).lock();
      localObject1 = a;
      ((Queue)localObject1).clear();
      super.clear();
      return;
    }
    finally
    {
      b.unlock();
    }
  }
  
  /* Error */
  public boolean contains(Object paramObject)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 22	io/fabric/sdk/android/services/concurrency/c:b	Ljava/util/concurrent/locks/ReentrantLock;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 54	java/util/concurrent/locks/ReentrantLock:lock	()V
    //   9: aload_0
    //   10: aload_1
    //   11: invokespecial 114	java/util/concurrent/PriorityBlockingQueue:contains	(Ljava/lang/Object;)Z
    //   14: istore_3
    //   15: iload_3
    //   16: ifne +20 -> 36
    //   19: aload_0
    //   20: getfield 17	io/fabric/sdk/android/services/concurrency/c:a	Ljava/util/Queue;
    //   23: astore_2
    //   24: aload_2
    //   25: aload_1
    //   26: invokeinterface 115 2 0
    //   31: istore_3
    //   32: iload_3
    //   33: ifeq +14 -> 47
    //   36: iconst_1
    //   37: istore_3
    //   38: aload_0
    //   39: getfield 22	io/fabric/sdk/android/services/concurrency/c:b	Ljava/util/concurrent/locks/ReentrantLock;
    //   42: invokevirtual 67	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   45: iload_3
    //   46: ireturn
    //   47: iconst_0
    //   48: istore_3
    //   49: aconst_null
    //   50: astore_2
    //   51: goto -13 -> 38
    //   54: astore_2
    //   55: aload_0
    //   56: getfield 22	io/fabric/sdk/android/services/concurrency/c:b	Ljava/util/concurrent/locks/ReentrantLock;
    //   59: invokevirtual 67	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   62: aload_2
    //   63: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	64	0	this	c
    //   0	64	1	paramObject	Object
    //   4	47	2	localObject1	Object
    //   54	9	2	localObject2	Object
    //   14	35	3	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   0	4	54	finally
    //   5	9	54	finally
    //   10	14	54	finally
    //   19	23	54	finally
    //   25	31	54	finally
  }
  
  public void d()
  {
    try
    {
      Object localObject1 = b;
      ((ReentrantLock)localObject1).lock();
      localObject1 = a;
      Iterator localIterator = ((Queue)localObject1).iterator();
      for (;;)
      {
        boolean bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = localIterator.next();
        localObject1 = (b)localObject1;
        boolean bool2 = a((b)localObject1);
        if (bool2)
        {
          super.offer(localObject1);
          localIterator.remove();
        }
      }
    }
    finally
    {
      b.unlock();
    }
  }
  
  public int drainTo(Collection paramCollection)
  {
    int i;
    try
    {
      ReentrantLock localReentrantLock = b;
      localReentrantLock.lock();
      i = super.drainTo(paramCollection);
      Object localObject2 = a;
      int j = ((Queue)localObject2).size();
      i += j;
      for (;;)
      {
        localObject2 = a;
        boolean bool = ((Queue)localObject2).isEmpty();
        if (bool) {
          break;
        }
        localObject2 = a;
        localObject2 = ((Queue)localObject2).poll();
        paramCollection.add(localObject2);
      }
    }
    finally
    {
      b.unlock();
    }
    return i;
  }
  
  public int drainTo(Collection paramCollection, int paramInt)
  {
    try
    {
      ReentrantLock localReentrantLock = b;
      localReentrantLock.lock();
      int i = super.drainTo(paramCollection, paramInt);
      for (;;)
      {
        Object localObject2 = a;
        boolean bool = ((Queue)localObject2).isEmpty();
        if ((bool) || (i > paramInt)) {
          break;
        }
        localObject2 = a;
        localObject2 = ((Queue)localObject2).poll();
        paramCollection.add(localObject2);
        i += 1;
      }
      return i;
    }
    finally
    {
      b.unlock();
    }
  }
  
  /* Error */
  public boolean remove(Object paramObject)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 22	io/fabric/sdk/android/services/concurrency/c:b	Ljava/util/concurrent/locks/ReentrantLock;
    //   4: astore_2
    //   5: aload_2
    //   6: invokevirtual 54	java/util/concurrent/locks/ReentrantLock:lock	()V
    //   9: aload_0
    //   10: aload_1
    //   11: invokespecial 59	java/util/concurrent/PriorityBlockingQueue:remove	(Ljava/lang/Object;)Z
    //   14: istore_3
    //   15: iload_3
    //   16: ifne +20 -> 36
    //   19: aload_0
    //   20: getfield 17	io/fabric/sdk/android/services/concurrency/c:a	Ljava/util/Queue;
    //   23: astore_2
    //   24: aload_2
    //   25: aload_1
    //   26: invokeinterface 160 2 0
    //   31: istore_3
    //   32: iload_3
    //   33: ifeq +14 -> 47
    //   36: iconst_1
    //   37: istore_3
    //   38: aload_0
    //   39: getfield 22	io/fabric/sdk/android/services/concurrency/c:b	Ljava/util/concurrent/locks/ReentrantLock;
    //   42: invokevirtual 67	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   45: iload_3
    //   46: ireturn
    //   47: iconst_0
    //   48: istore_3
    //   49: aconst_null
    //   50: astore_2
    //   51: goto -13 -> 38
    //   54: astore_2
    //   55: aload_0
    //   56: getfield 22	io/fabric/sdk/android/services/concurrency/c:b	Ljava/util/concurrent/locks/ReentrantLock;
    //   59: invokevirtual 67	java/util/concurrent/locks/ReentrantLock:unlock	()V
    //   62: aload_2
    //   63: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	64	0	this	c
    //   0	64	1	paramObject	Object
    //   4	47	2	localObject1	Object
    //   54	9	2	localObject2	Object
    //   14	35	3	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   0	4	54	finally
    //   5	9	54	finally
    //   10	14	54	finally
    //   19	23	54	finally
    //   25	31	54	finally
  }
  
  public boolean removeAll(Collection paramCollection)
  {
    try
    {
      ReentrantLock localReentrantLock = b;
      localReentrantLock.lock();
      boolean bool1 = super.removeAll(paramCollection);
      Queue localQueue = a;
      boolean bool2 = localQueue.removeAll(paramCollection);
      bool1 |= bool2;
      return bool1;
    }
    finally
    {
      b.unlock();
    }
  }
  
  public int size()
  {
    try
    {
      Object localObject1 = b;
      ((ReentrantLock)localObject1).lock();
      localObject1 = a;
      int i = ((Queue)localObject1).size();
      int j = super.size();
      i += j;
      return i;
    }
    finally
    {
      b.unlock();
    }
  }
  
  public Object[] toArray()
  {
    try
    {
      Object localObject1 = b;
      ((ReentrantLock)localObject1).lock();
      localObject1 = super.toArray();
      Object localObject3 = a;
      localObject3 = ((Queue)localObject3).toArray();
      localObject1 = a((Object[])localObject1, (Object[])localObject3);
      return (Object[])localObject1;
    }
    finally
    {
      b.unlock();
    }
  }
  
  public Object[] toArray(Object[] paramArrayOfObject)
  {
    try
    {
      Object localObject1 = b;
      ((ReentrantLock)localObject1).lock();
      localObject1 = super.toArray(paramArrayOfObject);
      Object localObject3 = a;
      localObject3 = ((Queue)localObject3).toArray(paramArrayOfObject);
      localObject1 = a((Object[])localObject1, (Object[])localObject3);
      return (Object[])localObject1;
    }
    finally
    {
      b.unlock();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */