package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;

public abstract interface b
{
  public abstract void addDependency(Object paramObject);
  
  public abstract boolean areDependenciesMet();
  
  public abstract Collection getDependencies();
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */