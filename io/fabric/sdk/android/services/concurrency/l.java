package io.fabric.sdk.android.services.concurrency;

public abstract interface l
{
  public abstract boolean isFinished();
  
  public abstract void setError(Throwable paramThrowable);
  
  public abstract void setFinished(boolean paramBoolean);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/l.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */