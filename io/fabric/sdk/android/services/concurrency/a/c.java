package io.fabric.sdk.android.services.concurrency.a;

public class c
  implements a
{
  private final long a;
  private final int b;
  
  public c(long paramLong, int paramInt)
  {
    a = paramLong;
    b = paramInt;
  }
  
  public long getDelayMillis(int paramInt)
  {
    double d1 = a;
    double d2 = b;
    double d3 = paramInt;
    d2 = Math.pow(d2, d3);
    return (d1 * d2);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/a/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */