package io.fabric.sdk.android.services.concurrency;

public enum e
{
  static
  {
    int i = 3;
    int j = 2;
    int k = 1;
    Object localObject = new io/fabric/sdk/android/services/concurrency/e;
    ((e)localObject).<init>("LOW", 0);
    a = (e)localObject;
    localObject = new io/fabric/sdk/android/services/concurrency/e;
    ((e)localObject).<init>("NORMAL", k);
    b = (e)localObject;
    localObject = new io/fabric/sdk/android/services/concurrency/e;
    ((e)localObject).<init>("HIGH", j);
    c = (e)localObject;
    localObject = new io/fabric/sdk/android/services/concurrency/e;
    ((e)localObject).<init>("IMMEDIATE", i);
    d = (e)localObject;
    localObject = new e[4];
    e locale = a;
    localObject[0] = locale;
    locale = b;
    localObject[k] = locale;
    locale = c;
    localObject[j] = locale;
    locale = d;
    localObject[i] = locale;
    e = (e[])localObject;
  }
  
  static int a(i parami, Object paramObject)
  {
    boolean bool = paramObject instanceof i;
    if (bool) {
      paramObject = (i)paramObject;
    }
    for (e locale = ((i)paramObject).getPriority();; locale = b)
    {
      int i = locale.ordinal();
      int j = parami.getPriority().ordinal();
      return i - j;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */