package io.fabric.sdk.android.services.concurrency;

import java.util.concurrent.ThreadFactory;

public final class k$a
  implements ThreadFactory
{
  private final int a;
  
  public k$a(int paramInt)
  {
    a = paramInt;
  }
  
  public Thread newThread(Runnable paramRunnable)
  {
    Thread localThread = new java/lang/Thread;
    localThread.<init>(paramRunnable);
    int i = a;
    localThread.setPriority(i);
    localThread.setName("Queue");
    return localThread;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/k$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */