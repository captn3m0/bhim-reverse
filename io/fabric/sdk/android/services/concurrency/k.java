package io.fabric.sdk.android.services.concurrency;

import java.util.concurrent.Callable;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class k
  extends ThreadPoolExecutor
{
  private static final int a = Runtime.getRuntime().availableProcessors();
  private static final int b = a + 1;
  private static final int c = a * 2 + 1;
  
  k(int paramInt1, int paramInt2, long paramLong, TimeUnit paramTimeUnit, c paramc, ThreadFactory paramThreadFactory)
  {
    super(paramInt1, paramInt2, paramLong, paramTimeUnit, paramc, paramThreadFactory);
    prestartAllCoreThreads();
  }
  
  public static k a()
  {
    int i = b;
    int j = c;
    return a(i, j);
  }
  
  public static k a(int paramInt1, int paramInt2)
  {
    k localk = new io/fabric/sdk/android/services/concurrency/k;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    c localc = new io/fabric/sdk/android/services/concurrency/c;
    localc.<init>();
    k.a locala = new io/fabric/sdk/android/services/concurrency/k$a;
    locala.<init>(10);
    localk.<init>(paramInt1, paramInt2, 1L, localTimeUnit, localc, locala);
    return localk;
  }
  
  protected void afterExecute(Runnable paramRunnable, Throwable paramThrowable)
  {
    Object localObject = paramRunnable;
    localObject = (l)paramRunnable;
    ((l)localObject).setFinished(true);
    ((l)localObject).setError(paramThrowable);
    b().d();
    super.afterExecute(paramRunnable, paramThrowable);
  }
  
  public c b()
  {
    return (c)super.getQueue();
  }
  
  public void execute(Runnable paramRunnable)
  {
    boolean bool = j.isProperDelegate(paramRunnable);
    if (bool) {
      super.execute(paramRunnable);
    }
    for (;;)
    {
      return;
      bool = false;
      RunnableFuture localRunnableFuture = newTaskFor(paramRunnable, null);
      super.execute(localRunnableFuture);
    }
  }
  
  protected RunnableFuture newTaskFor(Runnable paramRunnable, Object paramObject)
  {
    h localh = new io/fabric/sdk/android/services/concurrency/h;
    localh.<init>(paramRunnable, paramObject);
    return localh;
  }
  
  protected RunnableFuture newTaskFor(Callable paramCallable)
  {
    h localh = new io/fabric/sdk/android/services/concurrency/h;
    localh.<init>(paramCallable);
    return localh;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/k.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */