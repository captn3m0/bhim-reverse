package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;
import java.util.concurrent.ExecutorService;

public abstract class f
  extends a
  implements b, i, l
{
  private final j a;
  
  public f()
  {
    j localj = new io/fabric/sdk/android/services/concurrency/j;
    localj.<init>();
    a = localj;
  }
  
  public void a(l paraml)
  {
    Object localObject = b();
    a.d locald = a.d.a;
    if (localObject != locald)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Must not add Dependency after task is running");
      throw ((Throwable)localObject);
    }
    ((b)e()).addDependency(paraml);
  }
  
  public final void a(ExecutorService paramExecutorService, Object... paramVarArgs)
  {
    f.a locala = new io/fabric/sdk/android/services/concurrency/f$a;
    locala.<init>(paramExecutorService, this);
    super.a(locala, paramVarArgs);
  }
  
  public boolean areDependenciesMet()
  {
    return ((b)e()).areDependenciesMet();
  }
  
  public int compareTo(Object paramObject)
  {
    return e.a(this, paramObject);
  }
  
  public b e()
  {
    return a;
  }
  
  public Collection getDependencies()
  {
    return ((b)e()).getDependencies();
  }
  
  public e getPriority()
  {
    return ((i)e()).getPriority();
  }
  
  public boolean isFinished()
  {
    return ((l)e()).isFinished();
  }
  
  public void setError(Throwable paramThrowable)
  {
    ((l)e()).setError(paramThrowable);
  }
  
  public void setFinished(boolean paramBoolean)
  {
    ((l)e()).setFinished(paramBoolean);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */