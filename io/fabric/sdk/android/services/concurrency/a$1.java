package io.fabric.sdk.android.services.concurrency;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

final class a$1
  implements ThreadFactory
{
  private final AtomicInteger a;
  
  a$1()
  {
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>(1);
    a = localAtomicInteger;
  }
  
  public Thread newThread(Runnable paramRunnable)
  {
    Thread localThread = new java/lang/Thread;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = ((StringBuilder)localObject).append("AsyncTask #");
    int i = a.getAndIncrement();
    localObject = i;
    localThread.<init>(paramRunnable, (String)localObject);
    return localThread;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/a$1.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */