package io.fabric.sdk.android.services.concurrency;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class j
  implements b, i, l
{
  private final List dependencies;
  private final AtomicBoolean hasRun;
  private final AtomicReference throwable;
  
  public j()
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    dependencies = ((List)localObject);
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>(false);
    hasRun = ((AtomicBoolean)localObject);
    localObject = new java/util/concurrent/atomic/AtomicReference;
    ((AtomicReference)localObject).<init>(null);
    throwable = ((AtomicReference)localObject);
  }
  
  public static boolean isProperDelegate(Object paramObject)
  {
    Object localObject1 = paramObject;
    for (;;)
    {
      try
      {
        localb = (b)paramObject;
        localObject1 = paramObject;
        localObject1 = (l)paramObject;
        paramObject = (i)paramObject;
        if ((localb == null) || (localObject1 == null) || (paramObject == null)) {
          continue;
        }
        bool = true;
      }
      catch (ClassCastException localClassCastException)
      {
        b localb;
        boolean bool = false;
        Object localObject2 = null;
        continue;
      }
      return bool;
      bool = false;
      localb = null;
    }
  }
  
  public void addDependency(l paraml)
  {
    try
    {
      List localList = dependencies;
      localList.add(paraml);
      return;
    }
    finally {}
  }
  
  public boolean areDependenciesMet()
  {
    Object localObject = getDependencies();
    Iterator localIterator = ((Collection)localObject).iterator();
    do
    {
      bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject = (l)localIterator.next();
      bool = ((l)localObject).isFinished();
    } while (bool);
    boolean bool = false;
    localObject = null;
    for (;;)
    {
      return bool;
      bool = true;
    }
  }
  
  public int compareTo(Object paramObject)
  {
    return e.a(this, paramObject);
  }
  
  public Collection getDependencies()
  {
    try
    {
      Object localObject1 = dependencies;
      localObject1 = Collections.unmodifiableCollection((Collection)localObject1);
      return (Collection)localObject1;
    }
    finally {}
  }
  
  public Throwable getError()
  {
    return (Throwable)throwable.get();
  }
  
  public e getPriority()
  {
    return e.b;
  }
  
  public boolean isFinished()
  {
    return hasRun.get();
  }
  
  public void setError(Throwable paramThrowable)
  {
    throwable.set(paramThrowable);
  }
  
  public void setFinished(boolean paramBoolean)
  {
    try
    {
      AtomicBoolean localAtomicBoolean = hasRun;
      localAtomicBoolean.set(paramBoolean);
      return;
    }
    finally {}
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/j.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */