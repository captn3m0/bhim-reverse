package io.fabric.sdk.android.services.concurrency;

import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class h
  extends FutureTask
  implements b, i, l
{
  final Object b;
  
  public h(Runnable paramRunnable, Object paramObject)
  {
    super(paramRunnable, paramObject);
    b localb = a(paramRunnable);
    b = localb;
  }
  
  public h(Callable paramCallable)
  {
    super(paramCallable);
    b localb = a(paramCallable);
    b = localb;
  }
  
  public b a()
  {
    return (b)b;
  }
  
  protected b a(Object paramObject)
  {
    boolean bool = j.isProperDelegate(paramObject);
    if (bool) {
      paramObject = (b)paramObject;
    }
    for (;;)
    {
      return (b)paramObject;
      paramObject = new io/fabric/sdk/android/services/concurrency/j;
      ((j)paramObject).<init>();
    }
  }
  
  public void a(l paraml)
  {
    ((b)a()).addDependency(paraml);
  }
  
  public boolean areDependenciesMet()
  {
    return ((b)a()).areDependenciesMet();
  }
  
  public int compareTo(Object paramObject)
  {
    return ((i)a()).compareTo(paramObject);
  }
  
  public Collection getDependencies()
  {
    return ((b)a()).getDependencies();
  }
  
  public e getPriority()
  {
    return ((i)a()).getPriority();
  }
  
  public boolean isFinished()
  {
    return ((l)a()).isFinished();
  }
  
  public void setError(Throwable paramThrowable)
  {
    ((l)a()).setError(paramThrowable);
  }
  
  public void setFinished(boolean paramBoolean)
  {
    ((l)a()).setFinished(paramBoolean);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */