package io.fabric.sdk.android.services.concurrency;

import android.os.Message;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class a
{
  private static final int a = Runtime.getRuntime().availableProcessors();
  public static final Executor b;
  public static final Executor c;
  private static final int d = a + 1;
  private static final int e = a * 2 + 1;
  private static final ThreadFactory f;
  private static final BlockingQueue g;
  private static final a.b h;
  private static volatile Executor i = c;
  private final a.e j;
  private final FutureTask k;
  private volatile a.d l;
  private final AtomicBoolean m;
  private final AtomicBoolean n;
  
  static
  {
    Object localObject = new io/fabric/sdk/android/services/concurrency/a$1;
    ((a.1)localObject).<init>();
    f = (ThreadFactory)localObject;
    localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>(128);
    g = (BlockingQueue)localObject;
    ThreadPoolExecutor localThreadPoolExecutor = new java/util/concurrent/ThreadPoolExecutor;
    int i1 = d;
    int i2 = e;
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    BlockingQueue localBlockingQueue = g;
    ThreadFactory localThreadFactory = f;
    localThreadPoolExecutor.<init>(i1, i2, 1L, localTimeUnit, localBlockingQueue, localThreadFactory);
    b = localThreadPoolExecutor;
    localObject = new io/fabric/sdk/android/services/concurrency/a$c;
    ((a.c)localObject).<init>(null);
    c = (Executor)localObject;
    localObject = new io/fabric/sdk/android/services/concurrency/a$b;
    ((a.b)localObject).<init>();
    h = (a.b)localObject;
  }
  
  public a()
  {
    Object localObject = a.d.a;
    l = ((a.d)localObject);
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>();
    m = ((AtomicBoolean)localObject);
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>();
    n = ((AtomicBoolean)localObject);
    localObject = new io/fabric/sdk/android/services/concurrency/a$2;
    ((a.2)localObject).<init>(this);
    j = ((a.e)localObject);
    localObject = new io/fabric/sdk/android/services/concurrency/a$3;
    a.e locale = j;
    ((a.3)localObject).<init>(this, locale);
    k = ((FutureTask)localObject);
  }
  
  private void c(Object paramObject)
  {
    AtomicBoolean localAtomicBoolean = n;
    boolean bool = localAtomicBoolean.get();
    if (!bool) {
      d(paramObject);
    }
  }
  
  private Object d(Object paramObject)
  {
    int i1 = 1;
    a.b localb = h;
    a.a locala = new io/fabric/sdk/android/services/concurrency/a$a;
    Object[] arrayOfObject = new Object[i1];
    arrayOfObject[0] = paramObject;
    locala.<init>(this, arrayOfObject);
    localb.obtainMessage(i1, locala).sendToTarget();
    return paramObject;
  }
  
  private void e(Object paramObject)
  {
    boolean bool = d();
    if (bool) {
      b(paramObject);
    }
    for (;;)
    {
      a.d locald = a.d.c;
      l = locald;
      return;
      a(paramObject);
    }
  }
  
  public final a a(Executor paramExecutor, Object... paramVarArgs)
  {
    Object localObject = l;
    a.d locald = a.d.a;
    int i2;
    if (localObject != locald)
    {
      localObject = a.4.a;
      locald = l;
      int i1 = locald.ordinal();
      i2 = localObject[i1];
    }
    switch (i2)
    {
    default: 
      localObject = a.d.b;
      l = ((a.d)localObject);
      a();
      j.b = paramVarArgs;
      localObject = k;
      paramExecutor.execute((Runnable)localObject);
      return this;
    case 1: 
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Cannot execute task: the task is already running.");
      throw ((Throwable)localObject);
    }
    localObject = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject).<init>("Cannot execute task: the task has already been executed (a task can be executed only once)");
    throw ((Throwable)localObject);
  }
  
  protected abstract Object a(Object... paramVarArgs);
  
  protected void a() {}
  
  protected void a(Object paramObject) {}
  
  public final boolean a(boolean paramBoolean)
  {
    m.set(true);
    return k.cancel(paramBoolean);
  }
  
  public final a.d b()
  {
    return l;
  }
  
  protected void b(Object paramObject)
  {
    c();
  }
  
  protected void b(Object... paramVarArgs) {}
  
  protected void c() {}
  
  public final boolean d()
  {
    return m.get();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */