package io.fabric.sdk.android.services.concurrency;

import java.util.LinkedList;
import java.util.concurrent.Executor;

class a$c
  implements Executor
{
  final LinkedList a;
  Runnable b;
  
  private a$c()
  {
    LinkedList localLinkedList = new java/util/LinkedList;
    localLinkedList.<init>();
    a = localLinkedList;
  }
  
  protected void a()
  {
    try
    {
      Object localObject1 = a;
      localObject1 = ((LinkedList)localObject1).poll();
      localObject1 = (Runnable)localObject1;
      b = ((Runnable)localObject1);
      if (localObject1 != null)
      {
        localObject1 = a.b;
        Runnable localRunnable = b;
        ((Executor)localObject1).execute(localRunnable);
      }
      return;
    }
    finally {}
  }
  
  public void execute(Runnable paramRunnable)
  {
    try
    {
      Object localObject1 = a;
      a.c.1 local1 = new io/fabric/sdk/android/services/concurrency/a$c$1;
      local1.<init>(this, paramRunnable);
      ((LinkedList)localObject1).offer(local1);
      localObject1 = b;
      if (localObject1 == null) {
        a();
      }
      return;
    }
    finally {}
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/concurrency/a$c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */