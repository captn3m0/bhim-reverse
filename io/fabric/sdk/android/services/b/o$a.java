package io.fabric.sdk.android.services.b;

public enum o$a
{
  public final int h;
  
  static
  {
    int j = 4;
    int k = 3;
    int m = 2;
    int n = 1;
    Object localObject = new io/fabric/sdk/android/services/b/o$a;
    ((a)localObject).<init>("WIFI_MAC_ADDRESS", 0, n);
    a = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/o$a;
    ((a)localObject).<init>("BLUETOOTH_MAC_ADDRESS", n, m);
    b = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/o$a;
    ((a)localObject).<init>("FONT_TOKEN", m, 53);
    c = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/o$a;
    ((a)localObject).<init>("ANDROID_ID", k, 100);
    d = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/o$a;
    ((a)localObject).<init>("ANDROID_DEVICE_ID", j, 101);
    e = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/o$a;
    ((a)localObject).<init>("ANDROID_SERIAL", 5, 102);
    f = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/o$a;
    ((a)localObject).<init>("ANDROID_ADVERTISING_ID", 6, 103);
    g = (a)localObject;
    localObject = new a[7];
    a locala1 = a;
    localObject[0] = locala1;
    locala1 = b;
    localObject[n] = locala1;
    locala1 = c;
    localObject[m] = locala1;
    locala1 = d;
    localObject[k] = locala1;
    locala1 = e;
    localObject[j] = locala1;
    a locala2 = f;
    localObject[5] = locala2;
    locala2 = g;
    localObject[6] = locala2;
    i = (a[])localObject;
  }
  
  private o$a(int paramInt1)
  {
    h = paramInt1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/o$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */