package io.fabric.sdk.android.services.b;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.NoSuchElementException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class q
  implements Closeable
{
  private static final Logger b = Logger.getLogger(q.class.getName());
  int a;
  private final RandomAccessFile c;
  private int d;
  private q.a e;
  private q.a f;
  private final byte[] g;
  
  public q(File paramFile)
  {
    Object localObject = new byte[16];
    g = ((byte[])localObject);
    boolean bool = paramFile.exists();
    if (!bool) {
      a(paramFile);
    }
    localObject = b(paramFile);
    c = ((RandomAccessFile)localObject);
    e();
  }
  
  private static int a(byte[] paramArrayOfByte, int paramInt)
  {
    int i = (paramArrayOfByte[paramInt] & 0xFF) << 24;
    int j = paramInt + 1;
    j = (paramArrayOfByte[j] & 0xFF) << 16;
    i += j;
    j = paramInt + 2;
    j = (paramArrayOfByte[j] & 0xFF) << 8;
    i += j;
    j = paramInt + 3;
    j = paramArrayOfByte[j] & 0xFF;
    return i + j;
  }
  
  private q.a a(int paramInt)
  {
    Object localObject;
    if (paramInt == 0) {
      localObject = q.a.a;
    }
    for (;;)
    {
      return (q.a)localObject;
      localObject = c;
      long l = paramInt;
      ((RandomAccessFile)localObject).seek(l);
      localObject = new io/fabric/sdk/android/services/b/q$a;
      RandomAccessFile localRandomAccessFile = c;
      int i = localRandomAccessFile.readInt();
      ((q.a)localObject).<init>(paramInt, i);
    }
  }
  
  private void a(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    Object localObject1 = g;
    Object localObject2 = new int[4];
    localObject2[0] = paramInt1;
    localObject2[1] = paramInt2;
    localObject2[2] = paramInt3;
    localObject2[3] = paramInt4;
    a((byte[])localObject1, (int[])localObject2);
    c.seek(0L);
    localObject1 = c;
    localObject2 = g;
    ((RandomAccessFile)localObject1).write((byte[])localObject2);
  }
  
  private void a(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    int i = b(paramInt1);
    int j = i + paramInt3;
    int k = a;
    long l1;
    RandomAccessFile localRandomAccessFile2;
    if (j <= k)
    {
      RandomAccessFile localRandomAccessFile1 = c;
      l1 = i;
      localRandomAccessFile1.seek(l1);
      localRandomAccessFile2 = c;
      localRandomAccessFile2.write(paramArrayOfByte, paramInt2, paramInt3);
    }
    for (;;)
    {
      return;
      j = a - i;
      RandomAccessFile localRandomAccessFile3 = c;
      long l2 = i;
      localRandomAccessFile3.seek(l2);
      c.write(paramArrayOfByte, paramInt2, j);
      localRandomAccessFile2 = c;
      l1 = 16;
      localRandomAccessFile2.seek(l1);
      localRandomAccessFile2 = c;
      k = paramInt2 + j;
      j = paramInt3 - j;
      localRandomAccessFile2.write(paramArrayOfByte, k, j);
    }
  }
  
  private static void a(File paramFile)
  {
    Object localObject1 = new java/io/File;
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    Object localObject4 = paramFile.getPath();
    localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
    localObject4 = ".tmp";
    localObject3 = (String)localObject4;
    ((File)localObject1).<init>((String)localObject3);
    localObject3 = b((File)localObject1);
    long l = 4096L;
    try
    {
      ((RandomAccessFile)localObject3).setLength(l);
      l = 0L;
      ((RandomAccessFile)localObject3).seek(l);
      int i = 16;
      localObject4 = new byte[i];
      int j = 4;
      int[] arrayOfInt = new int[j];
      int[] tmp85_83 = arrayOfInt;
      int[] tmp86_85 = tmp85_83;
      int[] tmp86_85 = tmp85_83;
      tmp86_85[0] = 'က';
      tmp86_85[1] = 0;
      tmp86_85[2] = 0;
      tmp86_85[3] = 0;
      a((byte[])localObject4, arrayOfInt);
      ((RandomAccessFile)localObject3).write((byte[])localObject4);
      ((RandomAccessFile)localObject3).close();
      boolean bool = ((File)localObject1).renameTo(paramFile);
      if (!bool)
      {
        localObject1 = new java/io/IOException;
        ((IOException)localObject1).<init>("Rename failed!");
        throw ((Throwable)localObject1);
      }
    }
    finally
    {
      ((RandomAccessFile)localObject3).close();
    }
  }
  
  private static void a(byte[] paramArrayOfByte, int... paramVarArgs)
  {
    int i = 0;
    int j = paramVarArgs.length;
    int k = 0;
    while (i < j)
    {
      int m = paramVarArgs[i];
      b(paramArrayOfByte, k, m);
      k += 4;
      i += 1;
    }
  }
  
  private int b(int paramInt)
  {
    int i = a;
    if (paramInt < i) {}
    for (;;)
    {
      return paramInt;
      i = paramInt + 16;
      int j = a;
      paramInt = i - j;
    }
  }
  
  private static RandomAccessFile b(File paramFile)
  {
    RandomAccessFile localRandomAccessFile = new java/io/RandomAccessFile;
    localRandomAccessFile.<init>(paramFile, "rwd");
    return localRandomAccessFile;
  }
  
  private static Object b(Object paramObject, String paramString)
  {
    if (paramObject == null)
    {
      NullPointerException localNullPointerException = new java/lang/NullPointerException;
      localNullPointerException.<init>(paramString);
      throw localNullPointerException;
    }
    return paramObject;
  }
  
  private void b(int paramInt1, byte[] paramArrayOfByte, int paramInt2, int paramInt3)
  {
    int i = b(paramInt1);
    int j = i + paramInt3;
    int k = a;
    long l1;
    RandomAccessFile localRandomAccessFile2;
    if (j <= k)
    {
      RandomAccessFile localRandomAccessFile1 = c;
      l1 = i;
      localRandomAccessFile1.seek(l1);
      localRandomAccessFile2 = c;
      localRandomAccessFile2.readFully(paramArrayOfByte, paramInt2, paramInt3);
    }
    for (;;)
    {
      return;
      j = a - i;
      RandomAccessFile localRandomAccessFile3 = c;
      long l2 = i;
      localRandomAccessFile3.seek(l2);
      c.readFully(paramArrayOfByte, paramInt2, j);
      localRandomAccessFile2 = c;
      l1 = 16;
      localRandomAccessFile2.seek(l1);
      localRandomAccessFile2 = c;
      k = paramInt2 + j;
      j = paramInt3 - j;
      localRandomAccessFile2.readFully(paramArrayOfByte, k, j);
    }
  }
  
  private static void b(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    int i = (byte)(paramInt2 >> 24);
    paramArrayOfByte[paramInt1] = i;
    i = paramInt1 + 1;
    int j = (byte)(paramInt2 >> 16);
    paramArrayOfByte[i] = j;
    i = paramInt1 + 2;
    j = (byte)(paramInt2 >> 8);
    paramArrayOfByte[i] = j;
    i = paramInt1 + 3;
    j = (byte)paramInt2;
    paramArrayOfByte[i] = j;
  }
  
  private void c(int paramInt)
  {
    int i = paramInt + 4;
    int j = f();
    if (j >= i) {
      return;
    }
    int m = a;
    do
    {
      j += m;
      m <<= 1;
    } while (j < i);
    d(m);
    j = f.b + 4;
    q.a locala1 = f;
    i = c;
    j += i;
    i = b(j);
    Object localObject = e;
    j = b;
    int n;
    if (i < j)
    {
      localObject = c.getChannel();
      n = a;
      long l1 = n;
      ((FileChannel)localObject).position(l1);
      int i1 = i + -4;
      l1 = i1;
      long l2 = ((FileChannel)localObject).transferTo(16, l1, (WritableByteChannel)localObject);
      l1 = i1;
      boolean bool = l2 < l1;
      if (bool)
      {
        AssertionError localAssertionError = new java/lang/AssertionError;
        localAssertionError.<init>("Copied insufficient number of bytes!");
        throw localAssertionError;
      }
    }
    localObject = f;
    int k = b;
    locala1 = e;
    i = b;
    q.a locala2;
    if (k < i)
    {
      k = a;
      i = f.b;
      k = k + i + -16;
      i = d;
      n = e.b;
      a(m, i, n, k);
      locala1 = new io/fabric/sdk/android/services/b/q$a;
      locala2 = f;
      n = c;
      locala1.<init>(k, n);
      f = locala1;
    }
    for (;;)
    {
      a = m;
      break;
      k = d;
      locala1 = e;
      i = b;
      locala2 = f;
      n = b;
      a(m, k, i, n);
    }
  }
  
  private void d(int paramInt)
  {
    RandomAccessFile localRandomAccessFile = c;
    long l = paramInt;
    localRandomAccessFile.setLength(l);
    c.getChannel().force(true);
  }
  
  private void e()
  {
    c.seek(0L);
    Object localObject1 = c;
    Object localObject2 = g;
    ((RandomAccessFile)localObject1).readFully((byte[])localObject2);
    localObject1 = g;
    int i = 0;
    localObject2 = null;
    int j = a((byte[])localObject1, 0);
    a = j;
    long l1 = a;
    RandomAccessFile localRandomAccessFile = c;
    long l2 = localRandomAccessFile.length();
    boolean bool = l1 < l2;
    if (bool)
    {
      localObject1 = new java/io/IOException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("File is truncated. Expected length: ");
      int m = a;
      localObject2 = ((StringBuilder)localObject2).append(m).append(", Actual length: ");
      l2 = c.length();
      localObject2 = l2;
      ((IOException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    int k = a(g, 4);
    d = k;
    k = a(g, 8);
    i = a(g, 12);
    localObject1 = a(k);
    e = ((q.a)localObject1);
    localObject1 = a(i);
    f = ((q.a)localObject1);
  }
  
  private int f()
  {
    int i = a;
    int j = a();
    return i - j;
  }
  
  public int a()
  {
    int i = d;
    if (i == 0) {
      i = 16;
    }
    for (;;)
    {
      return i;
      q.a locala1 = f;
      i = b;
      q.a locala2 = e;
      int j = b;
      if (i >= j)
      {
        locala1 = f;
        i = b;
        j = e.b;
        i = i - j + 4;
        locala2 = f;
        j = c;
        i = i + j + 16;
      }
      else
      {
        locala1 = f;
        i = b + 4;
        j = f.c;
        i += j;
        j = a;
        i += j;
        locala2 = e;
        j = b;
        i -= j;
      }
    }
  }
  
  public void a(q.c paramc)
  {
    try
    {
      q.a locala1 = e;
      int i = b;
      int j = 0;
      locala1 = null;
      for (;;)
      {
        int k = d;
        if (j >= k) {
          break;
        }
        q.a locala2 = a(i);
        q.b localb = new io/fabric/sdk/android/services/b/q$b;
        int m = 0;
        localb.<init>(this, locala2, null);
        m = c;
        paramc.read(localb, m);
        k = b + 4;
        i = c + k;
        i = b(i);
        j += 1;
      }
      return;
    }
    finally {}
  }
  
  public void a(byte[] paramArrayOfByte)
  {
    int i = paramArrayOfByte.length;
    a(paramArrayOfByte, 0, i);
  }
  
  public void a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    Object localObject1 = "buffer";
    try
    {
      b(paramArrayOfByte, (String)localObject1);
      i = paramInt1 | paramInt2;
      if (i >= 0)
      {
        i = paramArrayOfByte.length - paramInt1;
        if (paramInt2 <= i) {}
      }
      else
      {
        localObject1 = new java/lang/IndexOutOfBoundsException;
        ((IndexOutOfBoundsException)localObject1).<init>();
        throw ((Throwable)localObject1);
      }
    }
    finally {}
    c(paramInt2);
    boolean bool = b();
    q.a locala;
    Object localObject3;
    int j;
    int k;
    int m;
    if (bool)
    {
      i = 16;
      locala = new io/fabric/sdk/android/services/b/q$a;
      locala.<init>(i, paramInt2);
      localObject3 = g;
      j = 0;
      byte[] arrayOfByte = null;
      b((byte[])localObject3, 0, paramInt2);
      i = b;
      arrayOfByte = g;
      k = 0;
      m = 4;
      a(i, arrayOfByte, 0, m);
      i = b + 4;
      a(i, paramArrayOfByte, paramInt1, paramInt2);
      if (!bool) {
        break label286;
      }
    }
    for (int i = b;; i = b)
    {
      j = a;
      k = d + 1;
      m = b;
      a(j, k, i, m);
      f = locala;
      i = d + 1;
      d = i;
      if (bool)
      {
        localObject3 = f;
        e = ((q.a)localObject3);
      }
      return;
      localObject3 = f;
      i = b + 4;
      locala = f;
      int n = c;
      i += n;
      i = b(i);
      break;
      label286:
      localObject3 = e;
    }
  }
  
  public boolean a(int paramInt1, int paramInt2)
  {
    int i = a() + 4 + paramInt1;
    if (i <= paramInt2) {}
    for (i = 1;; i = 0) {
      return i;
    }
  }
  
  /* Error */
  public boolean b()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 185	io/fabric/sdk/android/services/b/q:d	I
    //   6: istore_1
    //   7: iload_1
    //   8: ifne +9 -> 17
    //   11: iconst_1
    //   12: istore_1
    //   13: aload_0
    //   14: monitorexit
    //   15: iload_1
    //   16: ireturn
    //   17: iconst_0
    //   18: istore_1
    //   19: aconst_null
    //   20: astore_2
    //   21: goto -8 -> 13
    //   24: astore_2
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_2
    //   28: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	29	0	this	q
    //   6	10	1	i	int
    //   18	1	1	j	int
    //   20	1	2	localObject1	Object
    //   24	4	2	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   2	6	24	finally
  }
  
  public void c()
  {
    try
    {
      boolean bool = b();
      if (bool)
      {
        NoSuchElementException localNoSuchElementException = new java/util/NoSuchElementException;
        localNoSuchElementException.<init>();
        throw localNoSuchElementException;
      }
    }
    finally {}
    int i = d;
    int j = 1;
    if (i == j) {
      d();
    }
    for (;;)
    {
      return;
      q.a locala1 = e;
      i = b + 4;
      Object localObject2 = e;
      j = c;
      i += j;
      i = b(i);
      localObject2 = g;
      int k = 0;
      q.a locala2 = null;
      int m = 4;
      b(i, (byte[])localObject2, 0, m);
      localObject2 = g;
      k = 0;
      locala2 = null;
      j = a((byte[])localObject2, 0);
      k = a;
      m = d + -1;
      q.a locala3 = f;
      int n = b;
      a(k, m, i, n);
      k = d + -1;
      d = k;
      locala2 = new io/fabric/sdk/android/services/b/q$a;
      locala2.<init>(i, j);
      e = locala2;
    }
  }
  
  public void close()
  {
    try
    {
      RandomAccessFile localRandomAccessFile = c;
      localRandomAccessFile.close();
      return;
    }
    finally {}
  }
  
  public void d()
  {
    int i = 4096;
    int j = 4096;
    try
    {
      a(j, 0, 0, 0);
      j = 0;
      q.a locala = null;
      d = 0;
      locala = q.a.a;
      e = locala;
      locala = q.a.a;
      f = locala;
      j = a;
      if (j > i)
      {
        j = 4096;
        d(j);
      }
      j = 4096;
      a = j;
      return;
    }
    finally {}
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject1 = getClass().getSimpleName();
    localStringBuilder.append((String)localObject1).append('[');
    localObject1 = localStringBuilder.append("fileLength=");
    int i = a;
    ((StringBuilder)localObject1).append(i);
    localObject1 = localStringBuilder.append(", size=");
    i = d;
    ((StringBuilder)localObject1).append(i);
    localObject1 = localStringBuilder.append(", first=");
    localObject2 = e;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = localStringBuilder.append(", last=");
    localObject2 = f;
    ((StringBuilder)localObject1).append(localObject2);
    localObject1 = ", element lengths=[";
    localStringBuilder.append((String)localObject1);
    try
    {
      localObject1 = new io/fabric/sdk/android/services/b/q$1;
      ((q.1)localObject1).<init>(this, localStringBuilder);
      a((q.c)localObject1);
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        localObject2 = b;
        Level localLevel = Level.WARNING;
        String str = "read error";
        ((Logger)localObject2).log(localLevel, str, localIOException);
      }
    }
    localStringBuilder.append("]]");
    return localStringBuilder.toString();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/q.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */