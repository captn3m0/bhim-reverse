package io.fabric.sdk.android.services.b;

import java.io.InputStream;
import java.io.RandomAccessFile;

final class q$b
  extends InputStream
{
  private int b;
  private int c;
  
  private q$b(q paramq, q.a parama)
  {
    int i = b + 4;
    i = q.a(paramq, i);
    b = i;
    i = c;
    c = i;
  }
  
  public int read()
  {
    int i = c;
    if (i == 0) {
      i = -1;
    }
    for (;;)
    {
      return i;
      RandomAccessFile localRandomAccessFile = q.a(a);
      long l = b;
      localRandomAccessFile.seek(l);
      localRandomAccessFile = q.a(a);
      i = localRandomAccessFile.read();
      q localq = a;
      int j = b + 1;
      int k = q.a(localq, j);
      b = k;
      k = c + -1;
      c = k;
    }
  }
  
  public int read(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    Object localObject = "buffer";
    q.a(paramArrayOfByte, (String)localObject);
    int i = paramInt1 | paramInt2;
    if (i >= 0)
    {
      i = paramArrayOfByte.length - paramInt1;
      if (paramInt2 <= i) {}
    }
    else
    {
      localObject = new java/lang/ArrayIndexOutOfBoundsException;
      ((ArrayIndexOutOfBoundsException)localObject).<init>();
      throw ((Throwable)localObject);
    }
    i = c;
    if (i > 0)
    {
      i = c;
      if (paramInt2 > i) {
        paramInt2 = c;
      }
      localObject = a;
      int j = b;
      q.a((q)localObject, j, paramArrayOfByte, paramInt1, paramInt2);
      localObject = a;
      j = b + paramInt2;
      i = q.a((q)localObject, j);
      b = i;
      i = c - paramInt2;
      c = i;
    }
    for (;;)
    {
      return paramInt2;
      paramInt2 = -1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/q$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */