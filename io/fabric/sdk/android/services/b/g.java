package io.fabric.sdk.android.services.b;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;

public class g
{
  protected String a()
  {
    return "Fabric could not be initialized, API key missing from AndroidManifest.xml. Add the following tag to your Application element \n\t<meta-data android:name=\"io.fabric.ApiKey\" android:value=\"YOUR_API_KEY\"/>";
  }
  
  public String a(Context paramContext)
  {
    String str = b(paramContext);
    boolean bool = TextUtils.isEmpty(str);
    if (bool) {
      str = c(paramContext);
    }
    bool = TextUtils.isEmpty(str);
    if (bool) {
      d(paramContext);
    }
    return str;
  }
  
  protected String b(Context paramContext)
  {
    String str1 = null;
    try
    {
      Object localObject1 = paramContext.getPackageName();
      localObject2 = paramContext.getPackageManager();
      int i = 128;
      localObject1 = ((PackageManager)localObject2).getApplicationInfo((String)localObject1, i);
      localObject1 = metaData;
      if (localObject1 != null)
      {
        localObject2 = "io.fabric.ApiKey";
        str1 = ((Bundle)localObject1).getString((String)localObject2);
        if (str1 == null)
        {
          localObject2 = c.h();
          str3 = "Fabric";
          localObject3 = "Falling back to Crashlytics key lookup from Manifest";
          ((k)localObject2).a(str3, (String)localObject3);
          localObject2 = "com.crashlytics.ApiKey";
          str1 = ((Bundle)localObject1).getString((String)localObject2);
        }
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = c.h();
        String str3 = "Fabric";
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        String str4 = "Caught non-fatal exception while retrieving apiKey: ";
        localObject3 = ((StringBuilder)localObject3).append(str4);
        String str2 = localException;
        ((k)localObject2).a(str3, str2);
      }
    }
    return str1;
  }
  
  protected String c(Context paramContext)
  {
    String str1 = null;
    Object localObject = "io.fabric.ApiKey";
    String str2 = "string";
    int i = i.a(paramContext, (String)localObject, str2);
    if (i == 0)
    {
      localObject = c.h();
      String str3 = "Falling back to Crashlytics key lookup from Strings";
      ((k)localObject).a("Fabric", str3);
      localObject = "com.crashlytics.ApiKey";
      str2 = "string";
      i = i.a(paramContext, (String)localObject, str2);
    }
    if (i != 0) {
      str1 = paramContext.getResources().getString(i);
    }
    return str1;
  }
  
  protected void d(Context paramContext)
  {
    boolean bool = c.i();
    if (!bool)
    {
      bool = i.i(paramContext);
      if (!bool) {}
    }
    else
    {
      localObject = new java/lang/IllegalArgumentException;
      String str1 = a();
      ((IllegalArgumentException)localObject).<init>(str1);
      throw ((Throwable)localObject);
    }
    Object localObject = c.h();
    String str2 = a();
    ((k)localObject).e("Fabric", str2);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */