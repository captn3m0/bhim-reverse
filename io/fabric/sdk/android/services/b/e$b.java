package io.fabric.sdk.android.services.b;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;

final class e$b
  implements IInterface
{
  private final IBinder a;
  
  public e$b(IBinder paramIBinder)
  {
    a = paramIBinder;
  }
  
  public String a()
  {
    localParcel1 = Parcel.obtain();
    localParcel2 = Parcel.obtain();
    String str1 = null;
    Object localObject2 = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService";
    try
    {
      localParcel1.writeInterfaceToken((String)localObject2);
      localObject2 = a;
      int i = 1;
      str2 = null;
      ((IBinder)localObject2).transact(i, localParcel1, localParcel2, 0);
      localParcel2.readException();
      str1 = localParcel2.readString();
    }
    catch (Exception localException)
    {
      for (;;)
      {
        k localk = c.h();
        String str3 = "Fabric";
        String str2 = "Could not get parcel from Google Play Service to capture AdvertisingId";
        localk.a(str3, str2);
        localParcel2.recycle();
        localParcel1.recycle();
      }
    }
    finally
    {
      localParcel2.recycle();
      localParcel1.recycle();
    }
    return str1;
  }
  
  public IBinder asBinder()
  {
    return a;
  }
  
  public boolean b()
  {
    bool = true;
    localParcel1 = Parcel.obtain();
    localParcel2 = Parcel.obtain();
    localObject1 = "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService";
    for (;;)
    {
      try
      {
        localParcel1.writeInterfaceToken((String)localObject1);
        int i = 1;
        localParcel1.writeInt(i);
        localObject1 = a;
        int j = 2;
        ((IBinder)localObject1).transact(j, localParcel1, localParcel2, 0);
        localParcel2.readException();
        i = localParcel2.readInt();
        if (i == 0) {
          continue;
        }
      }
      catch (Exception localException)
      {
        Object localObject2;
        k localk = c.h();
        localObject1 = "Fabric";
        String str = "Could not get parcel from Google Play Service to capture Advertising limitAdTracking";
        localk.a((String)localObject1, str);
        localParcel2.recycle();
        localParcel1.recycle();
        bool = false;
        localk = null;
        continue;
      }
      finally
      {
        localParcel2.recycle();
        localParcel1.recycle();
      }
      return bool;
      bool = false;
      localObject2 = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/e$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */