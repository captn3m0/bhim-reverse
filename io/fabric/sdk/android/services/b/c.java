package io.fabric.sdk.android.services.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.text.TextUtils;
import io.fabric.sdk.android.k;

class c
{
  private final Context a;
  private final io.fabric.sdk.android.services.d.c b;
  
  public c(Context paramContext)
  {
    Object localObject = paramContext.getApplicationContext();
    a = ((Context)localObject);
    localObject = new io/fabric/sdk/android/services/d/d;
    ((io.fabric.sdk.android.services.d.d)localObject).<init>(paramContext, "TwitterAdvertisingInfoPreferences");
    b = ((io.fabric.sdk.android.services.d.c)localObject);
  }
  
  private void a(b paramb)
  {
    Thread localThread = new java/lang/Thread;
    c.1 local1 = new io/fabric/sdk/android/services/b/c$1;
    local1.<init>(this, paramb);
    localThread.<init>(local1);
    localThread.start();
  }
  
  private void b(b paramb)
  {
    boolean bool1 = c(paramb);
    io.fabric.sdk.android.services.d.c localc;
    SharedPreferences.Editor localEditor;
    String str2;
    if (bool1)
    {
      localc = b;
      localEditor = b.b();
      String str1 = a;
      localEditor = localEditor.putString("advertising_id", str1);
      str2 = "limit_ad_tracking_enabled";
      boolean bool2 = b;
      localEditor = localEditor.putBoolean(str2, bool2);
      localc.a(localEditor);
    }
    for (;;)
    {
      return;
      localc = b;
      localEditor = b.b().remove("advertising_id");
      str2 = "limit_ad_tracking_enabled";
      localEditor = localEditor.remove(str2);
      localc.a(localEditor);
    }
  }
  
  private boolean c(b paramb)
  {
    String str;
    boolean bool;
    if (paramb != null)
    {
      str = a;
      bool = TextUtils.isEmpty(str);
      if (!bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  private b e()
  {
    b localb = c().a();
    boolean bool = c(localb);
    k localk;
    String str1;
    String str2;
    if (!bool)
    {
      localb = d().a();
      bool = c(localb);
      if (!bool)
      {
        localk = io.fabric.sdk.android.c.h();
        str1 = "Fabric";
        str2 = "AdvertisingInfo not present";
        localk.a(str1, str2);
      }
    }
    for (;;)
    {
      return localb;
      localk = io.fabric.sdk.android.c.h();
      str1 = "Fabric";
      str2 = "Using AdvertisingInfo from Service Provider";
      localk.a(str1, str2);
      continue;
      localk = io.fabric.sdk.android.c.h();
      str1 = "Fabric";
      str2 = "Using AdvertisingInfo from Reflection Provider";
      localk.a(str1, str2);
    }
  }
  
  public b a()
  {
    b localb = b();
    boolean bool = c(localb);
    if (bool)
    {
      k localk = io.fabric.sdk.android.c.h();
      String str1 = "Fabric";
      String str2 = "Using AdvertisingInfo from Preference Store";
      localk.a(str1, str2);
      a(localb);
    }
    for (;;)
    {
      return localb;
      localb = e();
      b(localb);
    }
  }
  
  protected b b()
  {
    String str = b.a().getString("advertising_id", "");
    boolean bool = b.a().getBoolean("limit_ad_tracking_enabled", false);
    b localb = new io/fabric/sdk/android/services/b/b;
    localb.<init>(str, bool);
    return localb;
  }
  
  public f c()
  {
    d locald = new io/fabric/sdk/android/services/b/d;
    Context localContext = a;
    locald.<init>(localContext);
    return locald;
  }
  
  public f d()
  {
    e locale = new io/fabric/sdk/android/services/b/e;
    Context localContext = a;
    locale.<init>(localContext);
    return locale;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */