package io.fabric.sdk.android.services.b;

public enum l
{
  private final int e;
  
  static
  {
    int i = 4;
    int j = 3;
    int k = 2;
    int m = 1;
    Object localObject = new io/fabric/sdk/android/services/b/l;
    ((l)localObject).<init>("DEVELOPER", 0, m);
    a = (l)localObject;
    localObject = new io/fabric/sdk/android/services/b/l;
    ((l)localObject).<init>("USER_SIDELOAD", m, k);
    b = (l)localObject;
    localObject = new io/fabric/sdk/android/services/b/l;
    ((l)localObject).<init>("TEST_DISTRIBUTION", k, j);
    c = (l)localObject;
    localObject = new io/fabric/sdk/android/services/b/l;
    ((l)localObject).<init>("APP_STORE", j, i);
    d = (l)localObject;
    localObject = new l[i];
    l locall = a;
    localObject[0] = locall;
    locall = b;
    localObject[m] = locall;
    locall = c;
    localObject[k] = locall;
    locall = d;
    localObject[j] = locall;
    f = (l[])localObject;
  }
  
  private l(int paramInt1)
  {
    e = paramInt1;
  }
  
  public static l a(String paramString)
  {
    Object localObject = "io.crash.air";
    boolean bool = ((String)localObject).equals(paramString);
    if (bool) {
      localObject = c;
    }
    for (;;)
    {
      return (l)localObject;
      if (paramString != null) {
        localObject = d;
      } else {
        localObject = a;
      }
    }
  }
  
  public int a()
  {
    return e;
  }
  
  public String toString()
  {
    return Integer.toString(e);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/l.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */