package io.fabric.sdk.android.services.b;

import android.os.Process;

public abstract class h
  implements Runnable
{
  protected abstract void onRun();
  
  public final void run()
  {
    Process.setThreadPriority(10);
    onRun();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */