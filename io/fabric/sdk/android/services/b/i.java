package io.fabric.sdk.android.services.b;

import android.app.ActivityManager;
import android.app.ActivityManager.MemoryInfo;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Debug;
import android.os.StatFs;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.io.Closeable;
import java.io.File;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class i
{
  public static final Comparator a;
  private static Boolean b = null;
  private static final char[] c;
  private static long d;
  
  static
  {
    Object localObject = new char[16];
    localObject[0] = 48;
    localObject[1] = 49;
    localObject[2] = 50;
    localObject[3] = 51;
    localObject[4] = 52;
    localObject[5] = 53;
    localObject[6] = 54;
    localObject[7] = 55;
    localObject[8] = 56;
    localObject[9] = 57;
    localObject[10] = 97;
    localObject[11] = 98;
    localObject[12] = 99;
    localObject[13] = 100;
    localObject[14] = 101;
    localObject[15] = 102;
    c = (char[])localObject;
    d = -1;
    localObject = new io/fabric/sdk/android/services/b/i$1;
    ((i.1)localObject).<init>();
    a = (Comparator)localObject;
  }
  
  public static int a()
  {
    return i.a.a().ordinal();
  }
  
  public static int a(Context paramContext, String paramString1, String paramString2)
  {
    Resources localResources = paramContext.getResources();
    String str = j(paramContext);
    return localResources.getIdentifier(paramString1, paramString2, str);
  }
  
  public static int a(Context paramContext, boolean paramBoolean)
  {
    double d1 = 99.0D;
    Float localFloat = c(paramContext);
    int i;
    float f1;
    if ((!paramBoolean) || (localFloat == null))
    {
      i = 1;
      f1 = Float.MIN_VALUE;
    }
    for (;;)
    {
      return i;
      float f2 = localFloat.floatValue();
      double d2 = f2;
      boolean bool2 = d2 < d1;
      if (!bool2)
      {
        i = 3;
        f1 = 4.2E-45F;
      }
      else
      {
        f1 = localFloat.floatValue();
        double d3 = f1;
        boolean bool1 = d3 < d1;
        int j;
        if (bool1)
        {
          j = 2;
          f1 = 2.8E-45F;
        }
        else
        {
          j = 0;
          f1 = 0.0F;
          localFloat = null;
        }
      }
    }
  }
  
  static long a(String paramString1, String paramString2, int paramInt)
  {
    long l1 = Long.parseLong(paramString1.split(paramString2)[0].trim());
    long l2 = paramInt;
    return l1 * l2;
  }
  
  public static ActivityManager.RunningAppProcessInfo a(String paramString, Context paramContext)
  {
    Object localObject = ((ActivityManager)paramContext.getSystemService("activity")).getRunningAppProcesses();
    boolean bool1;
    if (localObject != null)
    {
      Iterator localIterator = ((List)localObject).iterator();
      boolean bool2;
      do
      {
        bool1 = localIterator.hasNext();
        if (!bool1) {
          break;
        }
        localObject = (ActivityManager.RunningAppProcessInfo)localIterator.next();
        String str = processName;
        bool2 = str.equals(paramString);
      } while (!bool2);
    }
    for (;;)
    {
      return (ActivityManager.RunningAppProcessInfo)localObject;
      bool1 = false;
      localObject = null;
    }
  }
  
  public static SharedPreferences a(Context paramContext)
  {
    return paramContext.getSharedPreferences("com.crashlytics.prefs", 0);
  }
  
  public static String a(int paramInt)
  {
    if (paramInt < 0)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("value must be zero or greater");
      throw ((Throwable)localObject);
    }
    Object localObject = Locale.US;
    Object[] arrayOfObject = new Object[1];
    Integer localInteger = Integer.valueOf(paramInt);
    arrayOfObject[0] = localInteger;
    return String.format((Locale)localObject, "%1$10s", arrayOfObject).replace(' ', '0');
  }
  
  /* Error */
  public static String a(File paramFile, String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: iconst_1
    //   3: istore_3
    //   4: aload_0
    //   5: invokevirtual 164	java/io/File:exists	()Z
    //   8: istore 4
    //   10: iload 4
    //   12: ifeq +126 -> 138
    //   15: new 166	java/io/BufferedReader
    //   18: astore 5
    //   20: new 168	java/io/FileReader
    //   23: astore 6
    //   25: aload 6
    //   27: aload_0
    //   28: invokespecial 171	java/io/FileReader:<init>	(Ljava/io/File;)V
    //   31: sipush 1024
    //   34: istore 7
    //   36: aload 5
    //   38: aload 6
    //   40: iload 7
    //   42: invokespecial 175	java/io/BufferedReader:<init>	(Ljava/io/Reader;I)V
    //   45: aload 5
    //   47: invokevirtual 178	java/io/BufferedReader:readLine	()Ljava/lang/String;
    //   50: astore 6
    //   52: aload 6
    //   54: ifnull +73 -> 127
    //   57: ldc -76
    //   59: astore 8
    //   61: aload 8
    //   63: invokestatic 186	java/util/regex/Pattern:compile	(Ljava/lang/String;)Ljava/util/regex/Pattern;
    //   66: astore 8
    //   68: iconst_2
    //   69: istore 9
    //   71: aload 8
    //   73: aload 6
    //   75: iload 9
    //   77: invokevirtual 189	java/util/regex/Pattern:split	(Ljava/lang/CharSequence;I)[Ljava/lang/String;
    //   80: astore 6
    //   82: aload 6
    //   84: arraylength
    //   85: istore 7
    //   87: iload 7
    //   89: iload_3
    //   90: if_icmple -45 -> 45
    //   93: iconst_0
    //   94: istore 7
    //   96: aconst_null
    //   97: astore 8
    //   99: aload 6
    //   101: iconst_0
    //   102: aaload
    //   103: astore 8
    //   105: aload 8
    //   107: aload_1
    //   108: invokevirtual 122	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   111: istore 7
    //   113: iload 7
    //   115: ifeq -70 -> 45
    //   118: iconst_1
    //   119: istore 7
    //   121: aload 6
    //   123: iload 7
    //   125: aaload
    //   126: astore_2
    //   127: ldc -65
    //   129: astore 6
    //   131: aload 5
    //   133: aload 6
    //   135: invokestatic 194	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   138: aload_2
    //   139: areturn
    //   140: astore 6
    //   142: aconst_null
    //   143: astore 5
    //   145: invokestatic 200	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   148: astore 8
    //   150: ldc -54
    //   152: astore 10
    //   154: new 204	java/lang/StringBuilder
    //   157: astore 11
    //   159: aload 11
    //   161: invokespecial 205	java/lang/StringBuilder:<init>	()V
    //   164: ldc -49
    //   166: astore 12
    //   168: aload 11
    //   170: aload 12
    //   172: invokevirtual 211	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   175: astore 11
    //   177: aload 11
    //   179: aload_0
    //   180: invokevirtual 214	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   183: astore 11
    //   185: aload 11
    //   187: invokevirtual 217	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   190: astore 11
    //   192: aload 8
    //   194: aload 10
    //   196: aload 11
    //   198: aload 6
    //   200: invokeinterface 223 4 0
    //   205: ldc -65
    //   207: astore 6
    //   209: aload 5
    //   211: aload 6
    //   213: invokestatic 194	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   216: goto -78 -> 138
    //   219: astore 6
    //   221: aconst_null
    //   222: astore 5
    //   224: aload 6
    //   226: astore_2
    //   227: aload 5
    //   229: ldc -65
    //   231: invokestatic 194	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   234: aload_2
    //   235: athrow
    //   236: astore_2
    //   237: goto -10 -> 227
    //   240: astore 6
    //   242: goto -97 -> 145
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	245	0	paramFile	File
    //   0	245	1	paramString	String
    //   1	234	2	localObject1	Object
    //   236	1	2	localObject2	Object
    //   3	88	3	i	int
    //   8	3	4	bool1	boolean
    //   18	210	5	localBufferedReader	java.io.BufferedReader
    //   23	111	6	localObject3	Object
    //   140	59	6	localException1	Exception
    //   207	5	6	str1	String
    //   219	6	6	localObject4	Object
    //   240	1	6	localException2	Exception
    //   34	61	7	j	int
    //   111	13	7	bool2	boolean
    //   59	134	8	localObject5	Object
    //   69	7	9	k	int
    //   152	43	10	str2	String
    //   157	40	11	localObject6	Object
    //   166	5	12	str3	String
    // Exception table:
    //   from	to	target	type
    //   15	18	140	java/lang/Exception
    //   20	23	140	java/lang/Exception
    //   27	31	140	java/lang/Exception
    //   40	45	140	java/lang/Exception
    //   15	18	219	finally
    //   20	23	219	finally
    //   27	31	219	finally
    //   40	45	219	finally
    //   45	50	236	finally
    //   61	66	236	finally
    //   75	80	236	finally
    //   82	85	236	finally
    //   101	103	236	finally
    //   107	111	236	finally
    //   123	126	236	finally
    //   145	148	236	finally
    //   154	157	236	finally
    //   159	164	236	finally
    //   170	175	236	finally
    //   179	183	236	finally
    //   185	190	236	finally
    //   198	205	236	finally
    //   45	50	240	java/lang/Exception
    //   61	66	240	java/lang/Exception
    //   75	80	240	java/lang/Exception
    //   82	85	240	java/lang/Exception
    //   101	103	240	java/lang/Exception
    //   107	111	240	java/lang/Exception
    //   123	126	240	java/lang/Exception
  }
  
  public static String a(InputStream paramInputStream)
  {
    Object localObject = new java/util/Scanner;
    ((Scanner)localObject).<init>(paramInputStream);
    String str = "\\A";
    localObject = ((Scanner)localObject).useDelimiter(str);
    boolean bool = ((Scanner)localObject).hasNext();
    if (bool) {}
    for (localObject = ((Scanner)localObject).next();; localObject = "") {
      return (String)localObject;
    }
  }
  
  private static String a(InputStream paramInputStream, String paramString)
  {
    Object localObject1 = "SHA-1";
    Object localObject2;
    try
    {
      localObject1 = MessageDigest.getInstance((String)localObject1);
      int i = 1024;
      Object localObject3 = new byte[i];
      String str1;
      for (;;)
      {
        int j = paramInputStream.read((byte[])localObject3);
        int k = -1;
        if (j == k) {
          break;
        }
        k = 0;
        str1 = null;
        ((MessageDigest)localObject1).update((byte[])localObject3, 0, j);
      }
      String str2;
      return (String)localObject2;
    }
    catch (Exception localException)
    {
      localObject3 = c.h();
      str2 = "Fabric";
      str1 = "Could not calculate hash for app icon.";
      ((k)localObject3).e(str2, str1, localException);
      localObject2 = "";
    }
    for (;;)
    {
      localObject2 = ((MessageDigest)localObject2).digest();
      localObject2 = a((byte[])localObject2);
    }
  }
  
  public static String a(String paramString)
  {
    return a(paramString, "SHA-1");
  }
  
  private static String a(String paramString1, String paramString2)
  {
    return a(paramString1.getBytes(), paramString2);
  }
  
  public static String a(byte[] paramArrayOfByte)
  {
    char[] arrayOfChar1 = new char[paramArrayOfByte.length * 2];
    int i = 0;
    String str = null;
    for (;;)
    {
      int j = paramArrayOfByte.length;
      if (i >= j) {
        break;
      }
      j = paramArrayOfByte[i] & 0xFF;
      int k = i * 2;
      char[] arrayOfChar2 = c;
      int m = j >>> 4;
      int n = arrayOfChar2[m];
      arrayOfChar1[k] = n;
      k = i * 2 + 1;
      arrayOfChar2 = c;
      j &= 0xF;
      j = arrayOfChar2[j];
      arrayOfChar1[k] = j;
      i += 1;
    }
    str = new java/lang/String;
    str.<init>(arrayOfChar1);
    return str;
  }
  
  private static String a(byte[] paramArrayOfByte, String paramString)
  {
    try
    {
      localObject1 = MessageDigest.getInstance(paramString);
      ((MessageDigest)localObject1).update(paramArrayOfByte);
      localObject1 = a(((MessageDigest)localObject1).digest());
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      for (;;)
      {
        Object localObject1;
        k localk = c.h();
        String str2 = "Fabric";
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("Could not create hashing algorithm: ").append(paramString);
        String str3 = ", returning empty string.";
        localObject2 = str3;
        localk.e(str2, (String)localObject2, localNoSuchAlgorithmException);
        String str1 = "";
      }
    }
    return (String)localObject1;
  }
  
  public static String a(String... paramVarArgs)
  {
    int i;
    String str1;
    if (paramVarArgs != null)
    {
      i = paramVarArgs.length;
      if (i != 0) {}
    }
    else
    {
      i = 0;
      str1 = null;
    }
    for (;;)
    {
      return str1;
      Object localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>();
      int j = paramVarArgs.length;
      i = 0;
      str1 = null;
      while (i < j)
      {
        String str2 = paramVarArgs[i];
        if (str2 != null)
        {
          String str3 = "";
          str2 = str2.replace("-", str3);
          Locale localLocale = Locale.US;
          str2 = str2.toLowerCase(localLocale);
          ((List)localObject).add(str2);
        }
        i += 1;
      }
      Collections.sort((List)localObject);
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localObject = ((List)localObject).iterator();
      boolean bool;
      for (;;)
      {
        bool = ((Iterator)localObject).hasNext();
        if (!bool) {
          break;
        }
        str1 = (String)((Iterator)localObject).next();
        localStringBuilder.append(str1);
      }
      str1 = localStringBuilder.toString();
      int k = str1.length();
      if (k > 0)
      {
        str1 = a(str1);
      }
      else
      {
        bool = false;
        str1 = null;
      }
    }
  }
  
  public static void a(Context paramContext, int paramInt, String paramString1, String paramString2)
  {
    boolean bool = e(paramContext);
    if (bool)
    {
      k localk = c.h();
      String str = "Fabric";
      localk.a(paramInt, str, paramString2);
    }
  }
  
  public static void a(Context paramContext, String paramString)
  {
    boolean bool = e(paramContext);
    if (bool)
    {
      k localk = c.h();
      String str = "Fabric";
      localk.a(str, paramString);
    }
  }
  
  public static void a(Context paramContext, String paramString, Throwable paramThrowable)
  {
    boolean bool = e(paramContext);
    if (bool)
    {
      k localk = c.h();
      String str = "Fabric";
      localk.e(str, paramString);
    }
  }
  
  public static void a(Closeable paramCloseable)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (RuntimeException localRuntimeException)
    {
      throw localRuntimeException;
    }
    catch (Exception localException)
    {
      for (;;) {}
    }
  }
  
  public static void a(Closeable paramCloseable, String paramString)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        k localk = c.h();
        String str = "Fabric";
        localk.e(str, paramString, localIOException);
      }
    }
  }
  
  public static void a(Flushable paramFlushable, String paramString)
  {
    if (paramFlushable != null) {}
    try
    {
      paramFlushable.flush();
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        k localk = c.h();
        String str = "Fabric";
        localk.e(str, paramString, localIOException);
      }
    }
  }
  
  public static void a(InputStream paramInputStream, OutputStream paramOutputStream, byte[] paramArrayOfByte)
  {
    for (;;)
    {
      int i = paramInputStream.read(paramArrayOfByte);
      int j = -1;
      if (i == j) {
        break;
      }
      j = 0;
      paramOutputStream.write(paramArrayOfByte, 0, i);
    }
  }
  
  public static boolean a(Context paramContext, String paramString, boolean paramBoolean)
  {
    Object localObject;
    if (paramContext != null)
    {
      localObject = paramContext.getResources();
      if (localObject != null)
      {
        String str = "bool";
        int i = a(paramContext, paramString, str);
        if (i <= 0) {
          break label41;
        }
        paramBoolean = ((Resources)localObject).getBoolean(i);
      }
    }
    for (;;)
    {
      return paramBoolean;
      label41:
      localObject = "string";
      int j = a(paramContext, paramString, (String)localObject);
      if (j > 0)
      {
        localObject = paramContext.getString(j);
        paramBoolean = Boolean.parseBoolean((String)localObject);
      }
    }
  }
  
  public static long b()
  {
    Object localObject2;
    synchronized (i.class)
    {
      long l1 = d;
      long l2 = -1;
      boolean bool1 = l1 < l2;
      if (!bool1)
      {
        l1 = 0L;
        localObject1 = new java/io/File;
        localObject2 = "/proc/meminfo";
        ((File)localObject1).<init>((String)localObject2);
        localObject2 = "MemTotal";
        localObject1 = a((File)localObject1, (String)localObject2);
        boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
        if (!bool2)
        {
          localObject2 = Locale.US;
          localObject2 = ((String)localObject1).toUpperCase((Locale)localObject2);
          localObject1 = "KB";
        }
      }
      for (;;)
      {
        try
        {
          bool3 = ((String)localObject2).endsWith((String)localObject1);
          if (!bool3) {
            continue;
          }
          localObject1 = "KB";
          i = 1024;
          l1 = a((String)localObject2, (String)localObject1, i);
        }
        catch (NumberFormatException localNumberFormatException)
        {
          boolean bool3;
          int i;
          Object localObject3 = c.h();
          Object localObject4 = "Fabric";
          Object localObject5 = new java/lang/StringBuilder;
          ((StringBuilder)localObject5).<init>();
          String str = "Unexpected meminfo format while computing RAM: ";
          localObject5 = ((StringBuilder)localObject5).append(str);
          localObject2 = ((StringBuilder)localObject5).append((String)localObject2);
          localObject2 = ((StringBuilder)localObject2).toString();
          ((k)localObject3).e((String)localObject4, (String)localObject2, localNumberFormatException);
          continue;
        }
        d = l1;
        l1 = d;
        return l1;
        localObject1 = "MB";
        bool3 = ((String)localObject2).endsWith((String)localObject1);
        if (bool3)
        {
          localObject1 = "MB";
          i = 1048576;
          l1 = a((String)localObject2, (String)localObject1, i);
        }
        else
        {
          localObject1 = "GB";
          bool3 = ((String)localObject2).endsWith((String)localObject1);
          if (!bool3) {
            continue;
          }
          localObject1 = "GB";
          i = 1073741824;
          l1 = a((String)localObject2, (String)localObject1, i);
        }
      }
      Object localObject1 = c.h();
      localObject3 = "Fabric";
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject5 = "Unexpected meminfo format while computing RAM: ";
      localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
      localObject4 = ((StringBuilder)localObject4).append((String)localObject2);
      localObject4 = ((StringBuilder)localObject4).toString();
      ((k)localObject1).a((String)localObject3, (String)localObject4);
    }
  }
  
  public static long b(Context paramContext)
  {
    ActivityManager.MemoryInfo localMemoryInfo = new android/app/ActivityManager$MemoryInfo;
    localMemoryInfo.<init>();
    ((ActivityManager)paramContext.getSystemService("activity")).getMemoryInfo(localMemoryInfo);
    return availMem;
  }
  
  public static long b(String paramString)
  {
    StatFs localStatFs = new android/os/StatFs;
    localStatFs.<init>(paramString);
    long l1 = localStatFs.getBlockSize();
    long l2 = localStatFs.getBlockCount() * l1;
    long l3 = localStatFs.getAvailableBlocks() * l1;
    return l2 - l3;
  }
  
  public static String b(int paramInt)
  {
    String str;
    switch (paramInt)
    {
    default: 
      str = "?";
    }
    for (;;)
    {
      return str;
      str = "A";
      continue;
      str = "D";
      continue;
      str = "E";
      continue;
      str = "I";
      continue;
      str = "V";
      continue;
      str = "W";
    }
  }
  
  public static String b(Context paramContext, String paramString)
  {
    String str = "string";
    int i = a(paramContext, paramString, str);
    if (i > 0) {}
    for (str = paramContext.getString(i);; str = "") {
      return str;
    }
  }
  
  public static String b(InputStream paramInputStream)
  {
    return a(paramInputStream, "SHA-1");
  }
  
  public static Float c(Context paramContext)
  {
    int i = 0;
    float f1 = 0.0F;
    Float localFloat = null;
    int j = -1;
    Object localObject = new android/content/IntentFilter;
    String str = "android.intent.action.BATTERY_CHANGED";
    ((IntentFilter)localObject).<init>(str);
    localObject = paramContext.registerReceiver(null, (IntentFilter)localObject);
    if (localObject == null) {}
    for (;;)
    {
      return localFloat;
      i = ((Intent)localObject).getIntExtra("level", j);
      str = "scale";
      int k = ((Intent)localObject).getIntExtra(str, j);
      f1 = i;
      float f2 = k;
      f1 /= f2;
      localFloat = Float.valueOf(f1);
    }
  }
  
  public static boolean c()
  {
    boolean bool = Debug.isDebuggerConnected();
    if (!bool)
    {
      bool = Debug.waitingForDebugger();
      if (!bool) {
        break label20;
      }
    }
    label20:
    for (bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean c(Context paramContext, String paramString)
  {
    int i = paramContext.checkCallingOrSelfPermission(paramString);
    if (i == 0) {}
    int j;
    for (i = 1;; j = 0) {
      return i;
    }
  }
  
  public static boolean c(String paramString)
  {
    if (paramString != null)
    {
      i = paramString.length();
      if (i != 0) {
        break label17;
      }
    }
    label17:
    int j;
    for (int i = 1;; j = 0) {
      return i;
    }
  }
  
  public static boolean d(Context paramContext)
  {
    boolean bool1 = false;
    boolean bool2 = f(paramContext);
    if (bool2) {
      return bool1;
    }
    Object localObject = (SensorManager)paramContext.getSystemService("sensor");
    int i = 8;
    localObject = ((SensorManager)localObject).getDefaultSensor(i);
    if (localObject != null) {
      bool2 = true;
    }
    for (;;)
    {
      bool1 = bool2;
      break;
      bool2 = false;
      localObject = null;
    }
  }
  
  public static boolean e(Context paramContext)
  {
    Boolean localBoolean = b;
    if (localBoolean == null)
    {
      boolean bool = a(paramContext, "com.crashlytics.Trace", false);
      localBoolean = Boolean.valueOf(bool);
      b = localBoolean;
    }
    return b.booleanValue();
  }
  
  public static boolean f(Context paramContext)
  {
    String str1 = Settings.Secure.getString(paramContext.getContentResolver(), "android_id");
    String str2 = "sdk";
    String str3 = Build.PRODUCT;
    boolean bool1 = str2.equals(str3);
    boolean bool2;
    if (!bool1)
    {
      str2 = "google_sdk";
      str3 = Build.PRODUCT;
      bool1 = str2.equals(str3);
      if ((!bool1) && (str1 != null)) {}
    }
    else
    {
      bool2 = true;
    }
    for (;;)
    {
      return bool2;
      bool2 = false;
      str1 = null;
    }
  }
  
  public static boolean g(Context paramContext)
  {
    boolean bool1 = true;
    boolean bool2 = f(paramContext);
    Object localObject = Build.TAGS;
    String str;
    boolean bool3;
    if ((!bool2) && (localObject != null))
    {
      str = "test-keys";
      bool3 = ((String)localObject).contains(str);
      if (!bool3) {}
    }
    for (;;)
    {
      return bool1;
      localObject = new java/io/File;
      str = "/system/app/Superuser.apk";
      ((File)localObject).<init>(str);
      bool3 = ((File)localObject).exists();
      if (!bool3)
      {
        localObject = new java/io/File;
        str = "/system/xbin/su";
        ((File)localObject).<init>(str);
        if (!bool2)
        {
          bool2 = ((File)localObject).exists();
          if (bool2) {}
        }
        else
        {
          bool1 = false;
        }
      }
    }
  }
  
  public static int h(Context paramContext)
  {
    int i = 0;
    boolean bool = f(paramContext);
    if (bool) {
      i = 1;
    }
    bool = g(paramContext);
    if (bool) {
      i |= 0x2;
    }
    bool = c();
    if (bool) {
      i |= 0x4;
    }
    return i;
  }
  
  public static boolean i(Context paramContext)
  {
    ApplicationInfo localApplicationInfo = paramContext.getApplicationInfo();
    int i = flags & 0x2;
    if (i != 0) {
      i = 1;
    }
    for (;;)
    {
      return i;
      int j = 0;
      localApplicationInfo = null;
    }
  }
  
  public static String j(Context paramContext)
  {
    Object localObject = paramContext.getApplicationContext().getApplicationInfo();
    int i = icon;
    Resources localResources;
    if (i > 0) {
      localResources = paramContext.getResources();
    }
    for (localObject = localResources.getResourcePackageName(i);; localObject = paramContext.getPackageName()) {
      return (String)localObject;
    }
  }
  
  /* Error */
  public static String k(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: invokevirtual 42	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   6: astore_2
    //   7: aload_0
    //   8: invokestatic 563	io/fabric/sdk/android/services/b/i:l	(Landroid/content/Context;)I
    //   11: istore_3
    //   12: aload_2
    //   13: iload_3
    //   14: invokevirtual 567	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
    //   17: astore 4
    //   19: aload 4
    //   21: invokestatic 570	io/fabric/sdk/android/services/b/i:b	(Ljava/io/InputStream;)Ljava/lang/String;
    //   24: astore_2
    //   25: aload_2
    //   26: invokestatic 572	io/fabric/sdk/android/services/b/i:c	(Ljava/lang/String;)Z
    //   29: istore 5
    //   31: iload 5
    //   33: ifeq +15 -> 48
    //   36: ldc_w 574
    //   39: astore_2
    //   40: aload 4
    //   42: aload_2
    //   43: invokestatic 194	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   46: aload_1
    //   47: areturn
    //   48: aload_2
    //   49: astore_1
    //   50: goto -14 -> 36
    //   53: astore_2
    //   54: iconst_0
    //   55: istore_3
    //   56: aconst_null
    //   57: astore 4
    //   59: invokestatic 200	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   62: astore 6
    //   64: ldc -54
    //   66: astore 7
    //   68: ldc_w 261
    //   71: astore 8
    //   73: aload 6
    //   75: aload 7
    //   77: aload 8
    //   79: aload_2
    //   80: invokeinterface 223 4 0
    //   85: ldc_w 574
    //   88: astore_2
    //   89: aload 4
    //   91: aload_2
    //   92: invokestatic 194	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   95: goto -49 -> 46
    //   98: astore_2
    //   99: iconst_0
    //   100: istore_3
    //   101: aconst_null
    //   102: astore 4
    //   104: aload_2
    //   105: astore_1
    //   106: aload 4
    //   108: ldc_w 574
    //   111: invokestatic 194	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   114: aload_1
    //   115: athrow
    //   116: astore_1
    //   117: goto -11 -> 106
    //   120: astore_2
    //   121: goto -62 -> 59
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	124	0	paramContext	Context
    //   1	114	1	localObject1	Object
    //   116	1	1	localObject2	Object
    //   6	43	2	localObject3	Object
    //   53	27	2	localException1	Exception
    //   88	4	2	str1	String
    //   98	7	2	localObject4	Object
    //   120	1	2	localException2	Exception
    //   11	90	3	i	int
    //   17	90	4	localInputStream	InputStream
    //   29	3	5	bool	boolean
    //   62	12	6	localk	k
    //   66	10	7	str2	String
    //   71	7	8	str3	String
    // Exception table:
    //   from	to	target	type
    //   2	6	53	java/lang/Exception
    //   7	11	53	java/lang/Exception
    //   13	17	53	java/lang/Exception
    //   2	6	98	finally
    //   7	11	98	finally
    //   13	17	98	finally
    //   19	24	116	finally
    //   25	29	116	finally
    //   59	62	116	finally
    //   79	85	116	finally
    //   19	24	120	java/lang/Exception
    //   25	29	120	java/lang/Exception
  }
  
  public static int l(Context paramContext)
  {
    return getApplicationContextgetApplicationInfoicon;
  }
  
  public static String m(Context paramContext)
  {
    String str1 = null;
    Object localObject1 = "io.fabric.android.build_id";
    String str2 = "string";
    int i = a(paramContext, (String)localObject1, str2);
    if (i == 0)
    {
      localObject1 = "com.crashlytics.android.build_id";
      str2 = "string";
      i = a(paramContext, (String)localObject1, str2);
    }
    if (i != 0)
    {
      str1 = paramContext.getResources().getString(i);
      localObject1 = c.h();
      str2 = "Fabric";
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      String str3 = "Build ID is: ";
      localObject2 = str3 + str1;
      ((k)localObject1).a(str2, (String)localObject2);
    }
    return str1;
  }
  
  public static boolean n(Context paramContext)
  {
    boolean bool1 = true;
    Object localObject = "android.permission.ACCESS_NETWORK_STATE";
    boolean bool2 = c(paramContext, (String)localObject);
    if (bool2)
    {
      localObject = ((ConnectivityManager)paramContext.getSystemService("connectivity")).getActiveNetworkInfo();
      if (localObject != null)
      {
        bool2 = ((NetworkInfo)localObject).isConnectedOrConnecting();
        if (bool2) {
          bool2 = bool1;
        }
      }
    }
    for (;;)
    {
      return bool2;
      bool2 = false;
      localObject = null;
      continue;
      bool2 = bool1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */