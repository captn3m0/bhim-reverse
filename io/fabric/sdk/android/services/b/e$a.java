package io.fabric.sdk.android.services.b;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

final class e$a
  implements ServiceConnection
{
  private boolean a = false;
  private final LinkedBlockingQueue b;
  
  private e$a()
  {
    LinkedBlockingQueue localLinkedBlockingQueue = new java/util/concurrent/LinkedBlockingQueue;
    localLinkedBlockingQueue.<init>(1);
    b = localLinkedBlockingQueue;
  }
  
  public IBinder a()
  {
    bool = a;
    Object localObject1;
    Object localObject3;
    if (bool)
    {
      localObject1 = c.h();
      localObject3 = "Fabric";
      String str = "getBinder already called";
      ((k)localObject1).e((String)localObject3, str);
    }
    bool = true;
    a = bool;
    try
    {
      localObject1 = b;
      long l = 200L;
      localObject3 = TimeUnit.MILLISECONDS;
      localObject1 = ((LinkedBlockingQueue)localObject1).poll(l, (TimeUnit)localObject3);
      localObject1 = (IBinder)localObject1;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        bool = false;
        Object localObject2 = null;
      }
    }
    return (IBinder)localObject1;
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    try
    {
      LinkedBlockingQueue localLinkedBlockingQueue = b;
      localLinkedBlockingQueue.put(paramIBinder);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    b.clear();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/e$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */