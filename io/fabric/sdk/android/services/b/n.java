package io.fabric.sdk.android.services.b;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public final class n
{
  public static ExecutorService a(String paramString)
  {
    ExecutorService localExecutorService = Executors.newSingleThreadExecutor(c(paramString));
    a(paramString, localExecutorService);
    return localExecutorService;
  }
  
  private static final void a(String paramString, ExecutorService paramExecutorService)
  {
    TimeUnit localTimeUnit = TimeUnit.SECONDS;
    a(paramString, paramExecutorService, 2, localTimeUnit);
  }
  
  public static final void a(String paramString, ExecutorService paramExecutorService, long paramLong, TimeUnit paramTimeUnit)
  {
    Runtime localRuntime = Runtime.getRuntime();
    Thread localThread = new java/lang/Thread;
    n.2 local2 = new io/fabric/sdk/android/services/b/n$2;
    Object localObject = paramString;
    local2.<init>(paramString, paramExecutorService, paramLong, paramTimeUnit);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = "Crashlytics Shutdown Hook for " + paramString;
    localThread.<init>(local2, (String)localObject);
    localRuntime.addShutdownHook(localThread);
  }
  
  public static ScheduledExecutorService b(String paramString)
  {
    ScheduledExecutorService localScheduledExecutorService = Executors.newSingleThreadScheduledExecutor(c(paramString));
    a(paramString, localScheduledExecutorService);
    return localScheduledExecutorService;
  }
  
  public static final ThreadFactory c(String paramString)
  {
    AtomicLong localAtomicLong = new java/util/concurrent/atomic/AtomicLong;
    localAtomicLong.<init>(1L);
    n.1 local1 = new io/fabric/sdk/android/services/b/n$1;
    local1.<init>(paramString, localAtomicLong);
    return local1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/n.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */