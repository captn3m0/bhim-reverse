package io.fabric.sdk.android.services.b;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

final class n$2
  extends h
{
  n$2(String paramString, ExecutorService paramExecutorService, long paramLong, TimeUnit paramTimeUnit) {}
  
  public void onRun()
  {
    try
    {
      Object localObject1 = c.h();
      localObject3 = "Fabric";
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      str1 = "Executing shutdown hook for ";
      localObject4 = ((StringBuilder)localObject4).append(str1);
      str1 = a;
      localObject4 = ((StringBuilder)localObject4).append(str1);
      localObject4 = ((StringBuilder)localObject4).toString();
      ((k)localObject1).a((String)localObject3, (String)localObject4);
      localObject1 = b;
      ((ExecutorService)localObject1).shutdown();
      localObject1 = b;
      long l = c;
      localObject3 = d;
      boolean bool = ((ExecutorService)localObject1).awaitTermination(l, (TimeUnit)localObject3);
      if (!bool)
      {
        localObject1 = c.h();
        localObject3 = "Fabric";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        str1 = a;
        localObject4 = ((StringBuilder)localObject4).append(str1);
        str1 = " did not shut down in the";
        localObject4 = ((StringBuilder)localObject4).append(str1);
        str1 = " allocated time. Requesting immediate shutdown.";
        localObject4 = ((StringBuilder)localObject4).append(str1);
        localObject4 = ((StringBuilder)localObject4).toString();
        ((k)localObject1).a((String)localObject3, (String)localObject4);
        localObject1 = b;
        ((ExecutorService)localObject1).shutdownNow();
      }
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        Object localObject2 = c.h();
        Object localObject3 = "Fabric";
        Object localObject4 = Locale.US;
        String str1 = "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.";
        int i = 1;
        Object[] arrayOfObject = new Object[i];
        String str2 = a;
        arrayOfObject[0] = str2;
        localObject4 = String.format((Locale)localObject4, str1, arrayOfObject);
        ((k)localObject2).a((String)localObject3, (String)localObject4);
        localObject2 = b;
        ((ExecutorService)localObject2).shutdownNow();
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/n$2.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */