package io.fabric.sdk.android.services.b;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.lang.reflect.Method;

class d
  implements f
{
  private final Context a;
  
  public d(Context paramContext)
  {
    Context localContext = paramContext.getApplicationContext();
    a = localContext;
  }
  
  private String b()
  {
    Object localObject1 = "com.google.android.gms.ads.identifier.AdvertisingIdClient$Info";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      localObject2 = "getId";
      localObject3 = null;
      localObject3 = new Class[0];
      localObject1 = ((Class)localObject1).getMethod((String)localObject2, (Class[])localObject3);
      localObject2 = d();
      localObject3 = null;
      localObject3 = new Object[0];
      localObject1 = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
      localObject1 = (String)localObject1;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        k localk = c.h();
        Object localObject2 = "Fabric";
        Object localObject3 = "Could not call getId on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info";
        localk.d((String)localObject2, (String)localObject3);
        localk = null;
      }
    }
    return (String)localObject1;
  }
  
  private boolean c()
  {
    Object localObject1 = "com.google.android.gms.ads.identifier.AdvertisingIdClient$Info";
    try
    {
      localObject1 = Class.forName((String)localObject1);
      localObject2 = "isLimitAdTrackingEnabled";
      localObject3 = null;
      localObject3 = new Class[0];
      localObject1 = ((Class)localObject1).getMethod((String)localObject2, (Class[])localObject3);
      localObject2 = d();
      localObject3 = null;
      localObject3 = new Object[0];
      localObject1 = ((Method)localObject1).invoke(localObject2, (Object[])localObject3);
      localObject1 = (Boolean)localObject1;
      bool = ((Boolean)localObject1).booleanValue();
    }
    catch (Exception localException)
    {
      for (;;)
      {
        k localk = c.h();
        Object localObject2 = "Fabric";
        Object localObject3 = "Could not call isLimitAdTrackingEnabled on com.google.android.gms.ads.identifier.AdvertisingIdClient$Info";
        localk.d((String)localObject2, (String)localObject3);
        boolean bool = false;
        localk = null;
      }
    }
    return bool;
  }
  
  private Object d()
  {
    Object localObject1 = null;
    Object localObject2 = "com.google.android.gms.ads.identifier.AdvertisingIdClient";
    try
    {
      localObject2 = Class.forName((String)localObject2);
      str = "getAdvertisingIdInfo";
      int i = 1;
      localObject3 = new Class[i];
      Object localObject4 = Context.class;
      localObject3[0] = localObject4;
      localObject2 = ((Class)localObject2).getMethod(str, (Class[])localObject3);
      str = null;
      i = 1;
      localObject3 = new Object[i];
      localObject4 = a;
      localObject3[0] = localObject4;
      localObject1 = ((Method)localObject2).invoke(null, (Object[])localObject3);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        k localk = c.h();
        String str = "Fabric";
        Object localObject3 = "Could not call getAdvertisingIdInfo on com.google.android.gms.ads.identifier.AdvertisingIdClient";
        localk.d(str, (String)localObject3);
      }
    }
    return localObject1;
  }
  
  public b a()
  {
    Object localObject = a;
    boolean bool1 = a((Context)localObject);
    if (bool1)
    {
      localObject = new io/fabric/sdk/android/services/b/b;
      String str = b();
      boolean bool2 = c();
      ((b)localObject).<init>(str, bool2);
    }
    for (;;)
    {
      return (b)localObject;
      bool1 = false;
      localObject = null;
    }
  }
  
  boolean a(Context paramContext)
  {
    int i = 1;
    for (Object localObject1 = "com.google.android.gms.common.GooglePlayServicesUtil";; localObject1 = null)
    {
      try
      {
        localObject1 = Class.forName((String)localObject1);
        String str = "isGooglePlayServicesAvailable";
        int j = 1;
        Object localObject3 = new Class[j];
        Class localClass = Context.class;
        localObject3[0] = localClass;
        localObject1 = ((Class)localObject1).getMethod(str, (Class[])localObject3);
        str = null;
        j = 1;
        localObject3 = new Object[j];
        localObject3[0] = paramContext;
        localObject1 = ((Method)localObject1).invoke(null, (Object[])localObject3);
        localObject1 = (Integer)localObject1;
        k = ((Integer)localObject1).intValue();
        if (k != 0) {
          break label91;
        }
        k = i;
      }
      catch (Exception localException)
      {
        for (;;)
        {
          int k;
          label91:
          int m = 0;
          Object localObject2 = null;
        }
      }
      return k;
      m = 0;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */