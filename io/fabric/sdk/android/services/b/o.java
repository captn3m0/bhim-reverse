package io.fabric.sdk.android.services.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.Secure;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class o
{
  private static final Pattern d = Pattern.compile("[^\\p{Alnum}]");
  private static final String e = Pattern.quote("/");
  c a;
  b b;
  boolean c;
  private final ReentrantLock f;
  private final p g;
  private final boolean h;
  private final boolean i;
  private final Context j;
  private final String k;
  private final String l;
  private final Collection m;
  
  public o(Context paramContext, String paramString1, String paramString2, Collection paramCollection)
  {
    Object localObject1 = new java/util/concurrent/locks/ReentrantLock;
    ((ReentrantLock)localObject1).<init>();
    f = ((ReentrantLock)localObject1);
    if (paramContext == null)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("appContext must not be null");
      throw ((Throwable)localObject1);
    }
    if (paramString1 == null)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("appIdentifier must not be null");
      throw ((Throwable)localObject1);
    }
    if (paramCollection == null)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("kits must not be null");
      throw ((Throwable)localObject1);
    }
    j = paramContext;
    k = paramString1;
    l = paramString2;
    m = paramCollection;
    localObject1 = new io/fabric/sdk/android/services/b/p;
    ((p)localObject1).<init>();
    g = ((p)localObject1);
    localObject1 = new io/fabric/sdk/android/services/b/c;
    ((c)localObject1).<init>(paramContext);
    a = ((c)localObject1);
    localObject1 = "com.crashlytics.CollectDeviceIdentifiers";
    boolean bool2 = i.a(paramContext, (String)localObject1, bool1);
    h = bool2;
    bool2 = h;
    String str1;
    Object localObject2;
    String str2;
    if (!bool2)
    {
      localObject1 = io.fabric.sdk.android.c.h();
      str1 = "Fabric";
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("Device ID collection disabled for ");
      str2 = paramContext.getPackageName();
      localObject2 = str2;
      ((k)localObject1).a(str1, (String)localObject2);
    }
    localObject1 = "com.crashlytics.CollectUserIdentifiers";
    bool2 = i.a(paramContext, (String)localObject1, bool1);
    i = bool2;
    bool2 = i;
    if (!bool2)
    {
      localObject1 = io.fabric.sdk.android.c.h();
      str1 = "Fabric";
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("User information collection disabled for ");
      str2 = paramContext.getPackageName();
      localObject2 = str2;
      ((k)localObject1).a(str1, (String)localObject2);
    }
  }
  
  private String a(SharedPreferences paramSharedPreferences)
  {
    f.lock();
    Object localObject1 = "crashlytics.installation.id";
    SharedPreferences.Editor localEditor = null;
    try
    {
      localObject1 = paramSharedPreferences.getString((String)localObject1, null);
      if (localObject1 == null)
      {
        localObject1 = UUID.randomUUID();
        localObject1 = ((UUID)localObject1).toString();
        localObject1 = a((String)localObject1);
        localEditor = paramSharedPreferences.edit();
        String str = "crashlytics.installation.id";
        localEditor = localEditor.putString(str, (String)localObject1);
        localEditor.commit();
      }
      return (String)localObject1;
    }
    finally
    {
      f.unlock();
    }
  }
  
  private String a(String paramString)
  {
    if (paramString == null) {}
    Locale localLocale;
    for (String str = null;; str = str.toLowerCase(localLocale))
    {
      return str;
      str = d.matcher(paramString).replaceAll("");
      localLocale = Locale.US;
    }
  }
  
  private void a(Map paramMap, o.a parama, String paramString)
  {
    if (paramString != null) {
      paramMap.put(parama, paramString);
    }
  }
  
  private String b(String paramString)
  {
    String str = e;
    return paramString.replaceAll(str, "");
  }
  
  public boolean a()
  {
    return i;
  }
  
  public String b()
  {
    String str = l;
    if (str == null)
    {
      SharedPreferences localSharedPreferences = i.a(j);
      str = localSharedPreferences.getString("crashlytics.installation.id", null);
      if (str == null) {
        str = a(localSharedPreferences);
      }
    }
    return str;
  }
  
  public String c()
  {
    return k;
  }
  
  public String d()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = e();
    localStringBuilder = localStringBuilder.append(str).append("/");
    str = f();
    return str;
  }
  
  public String e()
  {
    String str = Build.VERSION.RELEASE;
    return b(str);
  }
  
  public String f()
  {
    String str = Build.VERSION.INCREMENTAL;
    return b(str);
  }
  
  public String g()
  {
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[2];
    String str = Build.MANUFACTURER;
    str = b(str);
    arrayOfObject[0] = str;
    str = Build.MODEL;
    str = b(str);
    arrayOfObject[1] = str;
    return String.format(localLocale, "%s/%s", arrayOfObject);
  }
  
  public String h()
  {
    String str = "";
    boolean bool = h;
    if (bool)
    {
      str = n();
      if (str == null)
      {
        SharedPreferences localSharedPreferences = i.a(j);
        str = localSharedPreferences.getString("crashlytics.installation.id", null);
        if (str == null) {
          str = a(localSharedPreferences);
        }
      }
    }
    return str;
  }
  
  public Map i()
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    Object localObject1 = m;
    Iterator localIterator1 = ((Collection)localObject1).iterator();
    boolean bool1;
    boolean bool2;
    do
    {
      bool1 = localIterator1.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (h)localIterator1.next();
      bool2 = localObject1 instanceof m;
    } while (!bool2);
    localObject1 = ((m)localObject1).getDeviceIdentifiers().entrySet();
    Iterator localIterator2 = ((Set)localObject1).iterator();
    for (;;)
    {
      bool1 = localIterator2.hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (Map.Entry)localIterator2.next();
      localObject2 = (o.a)((Map.Entry)localObject1).getKey();
      localObject1 = (String)((Map.Entry)localObject1).getValue();
      a(localHashMap, (o.a)localObject2, (String)localObject1);
    }
    localObject1 = o.a.d;
    Object localObject2 = n();
    a(localHashMap, (o.a)localObject1, (String)localObject2);
    localObject1 = o.a.g;
    localObject2 = m();
    a(localHashMap, (o.a)localObject1, (String)localObject2);
    return Collections.unmodifiableMap(localHashMap);
  }
  
  public String j()
  {
    p localp = g;
    Context localContext = j;
    return localp.a(localContext);
  }
  
  b k()
  {
    try
    {
      boolean bool = c;
      if (!bool)
      {
        localObject1 = a;
        localObject1 = ((c)localObject1).a();
        b = ((b)localObject1);
        bool = true;
        c = bool;
      }
      Object localObject1 = b;
      return (b)localObject1;
    }
    finally {}
  }
  
  public Boolean l()
  {
    boolean bool1 = false;
    Boolean localBoolean = null;
    boolean bool2 = h;
    if (bool2)
    {
      b localb = k();
      if (localb != null)
      {
        bool1 = b;
        localBoolean = Boolean.valueOf(bool1);
      }
    }
    return localBoolean;
  }
  
  public String m()
  {
    String str = null;
    boolean bool = h;
    if (bool)
    {
      b localb = k();
      if (localb != null) {
        str = a;
      }
    }
    return str;
  }
  
  public String n()
  {
    String str1 = null;
    boolean bool1 = h;
    if (bool1)
    {
      String str2 = Settings.Secure.getString(j.getContentResolver(), "android_id");
      String str3 = "9774d56d682e549c";
      boolean bool2 = str3.equals(str2);
      if (!bool2) {
        str1 = a(str2);
      }
    }
    return str1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/o.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */