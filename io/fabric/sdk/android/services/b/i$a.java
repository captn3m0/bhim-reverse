package io.fabric.sdk.android.services.b;

import android.os.Build;
import android.text.TextUtils;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

 enum i$a
{
  private static final Map k;
  
  static
  {
    int m = 3;
    int n = 2;
    int i1 = 1;
    int i2 = 4;
    Object localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("X86_32", 0);
    a = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("X86_64", i1);
    b = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("ARM_UNKNOWN", n);
    c = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("PPC", m);
    d = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("PPC64", i2);
    e = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("ARMV6", 5);
    f = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("ARMV7", 6);
    g = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("UNKNOWN", 7);
    h = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("ARMV7S", 8);
    i = (a)localObject;
    localObject = new io/fabric/sdk/android/services/b/i$a;
    ((a)localObject).<init>("ARM64", 9);
    j = (a)localObject;
    localObject = new a[10];
    a locala1 = a;
    localObject[0] = locala1;
    locala1 = b;
    localObject[i1] = locala1;
    locala1 = c;
    localObject[n] = locala1;
    locala1 = d;
    localObject[m] = locala1;
    locala1 = e;
    localObject[i2] = locala1;
    a locala2 = f;
    localObject[5] = locala2;
    locala2 = g;
    localObject[6] = locala2;
    locala2 = h;
    localObject[7] = locala2;
    locala2 = i;
    localObject[8] = locala2;
    locala2 = j;
    localObject[9] = locala2;
    l = (a[])localObject;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>(i2);
    k = (Map)localObject;
    localObject = k;
    locala2 = g;
    ((Map)localObject).put("armeabi-v7a", locala2);
    localObject = k;
    locala2 = f;
    ((Map)localObject).put("armeabi", locala2);
    localObject = k;
    locala2 = j;
    ((Map)localObject).put("arm64-v8a", locala2);
    localObject = k;
    locala2 = a;
    ((Map)localObject).put("x86", locala2);
  }
  
  static a a()
  {
    Object localObject1 = Build.CPU_ABI;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    Object localObject2;
    if (bool)
    {
      localObject1 = c.h();
      localObject2 = "Fabric";
      String str = "Architecture#getValue()::Build.CPU_ABI returned null or empty";
      ((k)localObject1).a((String)localObject2, str);
      localObject1 = h;
    }
    for (;;)
    {
      return (a)localObject1;
      localObject2 = Locale.US;
      localObject1 = ((String)localObject1).toLowerCase((Locale)localObject2);
      localObject2 = k;
      localObject1 = (a)((Map)localObject2).get(localObject1);
      if (localObject1 == null) {
        localObject1 = h;
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/b/i$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */