package io.fabric.sdk.android.services.e;

import org.json.JSONObject;

class k
  implements v
{
  private long a(io.fabric.sdk.android.services.b.k paramk, long paramLong, JSONObject paramJSONObject)
  {
    String str = "expires_at";
    boolean bool = paramJSONObject.has(str);
    if (bool) {
      str = "expires_at";
    }
    long l2;
    for (long l1 = paramJSONObject.getLong(str);; l1 += l2)
    {
      return l1;
      l1 = paramk.a();
      l2 = 1000L * paramLong;
    }
  }
  
  private e a(JSONObject paramJSONObject)
  {
    String str1 = paramJSONObject.getString("identifier");
    String str2 = paramJSONObject.getString("status");
    String str3 = paramJSONObject.getString("url");
    String str4 = paramJSONObject.getString("reports_url");
    boolean bool1 = paramJSONObject.optBoolean("update_required", false);
    c localc = null;
    Object localObject = "icon";
    boolean bool2 = paramJSONObject.has((String)localObject);
    if (bool2)
    {
      localObject = paramJSONObject.getJSONObject("icon");
      String str5 = "hash";
      bool2 = ((JSONObject)localObject).has(str5);
      if (bool2)
      {
        localObject = paramJSONObject.getJSONObject("icon");
        localc = b((JSONObject)localObject);
      }
    }
    localObject = new io/fabric/sdk/android/services/e/e;
    ((e)localObject).<init>(str1, str2, str3, str4, bool1, localc);
    return (e)localObject;
  }
  
  private c b(JSONObject paramJSONObject)
  {
    String str = paramJSONObject.getString("hash");
    int i = paramJSONObject.getInt("width");
    int j = paramJSONObject.getInt("height");
    c localc = new io/fabric/sdk/android/services/e/c;
    localc.<init>(str, i, j);
    return localc;
  }
  
  private m c(JSONObject paramJSONObject)
  {
    boolean bool1 = true;
    boolean bool2 = paramJSONObject.optBoolean("prompt_enabled", false);
    boolean bool3 = paramJSONObject.optBoolean("collect_logged_exceptions", bool1);
    boolean bool4 = paramJSONObject.optBoolean("collect_reports", bool1);
    bool1 = paramJSONObject.optBoolean("collect_analytics", false);
    m localm = new io/fabric/sdk/android/services/e/m;
    localm.<init>(bool2, bool3, bool4, bool1);
    return localm;
  }
  
  private b d(JSONObject paramJSONObject)
  {
    int i = 1;
    String str = paramJSONObject.optString("url", "https://e.crashlytics.com/spi/v2/events");
    int j = paramJSONObject.optInt("flush_interval_secs", 600);
    int k = paramJSONObject.optInt("max_byte_size_per_file", 8000);
    int m = paramJSONObject.optInt("max_file_count_per_send", i);
    int n = paramJSONObject.optInt("max_pending_send_file_count", 100);
    boolean bool2 = paramJSONObject.optBoolean("track_custom_events", i);
    boolean bool3 = paramJSONObject.optBoolean("track_predefined_events", i);
    int i1 = paramJSONObject.optInt("sampling_rate", i);
    boolean bool1 = paramJSONObject.optBoolean("flush_on_background", i);
    b localb = new io/fabric/sdk/android/services/e/b;
    localb.<init>(str, j, k, m, n, bool2, bool3, i1, bool1);
    return localb;
  }
  
  private p e(JSONObject paramJSONObject)
  {
    int i = 64;
    int j = paramJSONObject.optInt("log_buffer_size", 64000);
    int k = paramJSONObject.optInt("max_chained_exception_depth", 8);
    int m = paramJSONObject.optInt("max_custom_exception_events", i);
    i = paramJSONObject.optInt("max_custom_key_value_pairs", i);
    int n = paramJSONObject.optInt("identifier_mask", 255);
    boolean bool = paramJSONObject.optBoolean("send_session_without_crash", false);
    p localp = new io/fabric/sdk/android/services/e/p;
    localp.<init>(j, k, m, i, n, bool);
    return localp;
  }
  
  private o f(JSONObject paramJSONObject)
  {
    boolean bool1 = true;
    String str1 = paramJSONObject.optString("title", "Send Crash Report?");
    String str2 = paramJSONObject.optString("message", "Looks like we crashed! Please help us fix the problem by sending a crash report.");
    String str3 = paramJSONObject.optString("send_button_title", "Send");
    boolean bool2 = paramJSONObject.optBoolean("show_cancel_button", bool1);
    String str4 = paramJSONObject.optString("cancel_button_title", "Don't Send");
    bool1 = paramJSONObject.optBoolean("show_always_send_button", bool1);
    String str5 = paramJSONObject.optString("always_send_button_title", "Always Send");
    o localo = new io/fabric/sdk/android/services/e/o;
    localo.<init>(str1, str2, str3, bool2, str4, bool1, str5);
    return localo;
  }
  
  private f g(JSONObject paramJSONObject)
  {
    String str1 = u.a;
    String str2 = paramJSONObject.optString("update_endpoint", str1);
    int i = paramJSONObject.optInt("update_suspend_duration", 3600);
    f localf = new io/fabric/sdk/android/services/e/f;
    localf.<init>(str2, i);
    return localf;
  }
  
  public t a(io.fabric.sdk.android.services.b.k paramk, JSONObject paramJSONObject)
  {
    int i = paramJSONObject.optInt("settings_version", 0);
    int j = paramJSONObject.optInt("cache_duration", 3600);
    JSONObject localJSONObject = paramJSONObject.getJSONObject("app");
    e locale = a(localJSONObject);
    localJSONObject = paramJSONObject.getJSONObject("session");
    p localp = e(localJSONObject);
    localJSONObject = paramJSONObject.getJSONObject("prompt");
    o localo = f(localJSONObject);
    localJSONObject = paramJSONObject.getJSONObject("features");
    m localm = c(localJSONObject);
    localJSONObject = paramJSONObject.getJSONObject("analytics");
    b localb = d(localJSONObject);
    localJSONObject = paramJSONObject.getJSONObject("beta");
    f localf = g(localJSONObject);
    long l1 = j;
    long l2 = a(paramk, l1, paramJSONObject);
    t localt = new io/fabric/sdk/android/services/e/t;
    localt.<init>(l2, locale, localp, localo, localm, localb, localf, i, j);
    return localt;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/e/k.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */