package io.fabric.sdk.android.services.e;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.NotFoundException;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.j;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.r;
import io.fabric.sdk.android.services.network.HttpRequest;
import java.io.Closeable;
import java.io.InputStream;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

abstract class a
  extends io.fabric.sdk.android.services.b.a
{
  public a(h paramh, String paramString1, String paramString2, io.fabric.sdk.android.services.network.d paramd, io.fabric.sdk.android.services.network.c paramc)
  {
    super(paramh, paramString1, paramString2, paramd, paramc);
  }
  
  private HttpRequest a(HttpRequest paramHttpRequest, d paramd)
  {
    String str1 = a;
    HttpRequest localHttpRequest = paramHttpRequest.a("X-CRASHLYTICS-API-KEY", str1).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    String str2 = kit.getVersion();
    return localHttpRequest.a("X-CRASHLYTICS-API-CLIENT-VERSION", str2);
  }
  
  private HttpRequest b(HttpRequest paramHttpRequest, d paramd)
  {
    Object localObject1 = b;
    Object localObject2 = paramHttpRequest.e("app[identifier]", (String)localObject1);
    Object localObject4 = f;
    localObject2 = ((HttpRequest)localObject2).e("app[name]", (String)localObject4);
    localObject4 = c;
    localObject2 = ((HttpRequest)localObject2).e("app[display_version]", (String)localObject4);
    localObject4 = d;
    localObject2 = ((HttpRequest)localObject2).e("app[build_version]", (String)localObject4);
    int i = g;
    localObject4 = Integer.valueOf(i);
    localObject2 = ((HttpRequest)localObject2).a("app[source]", (Number)localObject4);
    localObject4 = h;
    localObject2 = ((HttpRequest)localObject2).e("app[minimum_sdk_version]", (String)localObject4);
    localObject1 = "app[built_sdk_version]";
    localObject4 = i;
    localObject4 = ((HttpRequest)localObject2).e((String)localObject1, (String)localObject4);
    localObject2 = e;
    boolean bool = i.c((String)localObject2);
    if (!bool)
    {
      localObject2 = "app[instance_identifier]";
      localObject1 = e;
      ((HttpRequest)localObject4).e((String)localObject2, (String)localObject1);
    }
    localObject2 = j;
    if (localObject2 != null) {
      localObject1 = null;
    }
    try
    {
      localObject2 = kit;
      localObject2 = ((h)localObject2).getContext();
      localObject2 = ((Context)localObject2).getResources();
      localObject5 = j;
      int j = b;
      localObject1 = ((Resources)localObject2).openRawResource(j);
      localObject2 = "app[icon][hash]";
      localObject5 = j;
      localObject5 = a;
      localObject2 = ((HttpRequest)localObject4).e((String)localObject2, (String)localObject5);
      localObject5 = "app[icon][data]";
      localObject6 = "icon.png";
      localObject7 = "application/octet-stream";
      localObject2 = ((HttpRequest)localObject2).a((String)localObject5, (String)localObject6, (String)localObject7, (InputStream)localObject1);
      localObject5 = "app[icon][width]";
      localObject6 = j;
      int k = c;
      localObject6 = Integer.valueOf(k);
      localObject2 = ((HttpRequest)localObject2).a((String)localObject5, (Number)localObject6);
      localObject5 = "app[icon][height]";
      localObject6 = j;
      k = d;
      localObject6 = Integer.valueOf(k);
      ((HttpRequest)localObject2).a((String)localObject5, (Number)localObject6);
      localObject2 = "Failed to close app icon InputStream.";
      i.a((Closeable)localObject1, (String)localObject2);
    }
    catch (Resources.NotFoundException localNotFoundException)
    {
      for (;;)
      {
        Object localObject5 = io.fabric.sdk.android.c.h();
        Object localObject6 = "Fabric";
        Object localObject7 = new java/lang/StringBuilder;
        ((StringBuilder)localObject7).<init>();
        Object localObject8 = "Failed to find app icon with resource ID: ";
        localObject7 = ((StringBuilder)localObject7).append((String)localObject8);
        localObject8 = j;
        int m = b;
        localObject7 = ((StringBuilder)localObject7).append(m);
        localObject7 = ((StringBuilder)localObject7).toString();
        ((k)localObject5).e((String)localObject6, (String)localObject7, localNotFoundException);
        String str = "Failed to close app icon InputStream.";
        i.a((Closeable)localObject1, str);
      }
    }
    finally
    {
      i.a((Closeable)localObject1, "Failed to close app icon InputStream.");
    }
    localObject2 = k;
    if (localObject2 != null)
    {
      localObject2 = k;
      localObject1 = ((Collection)localObject2).iterator();
      for (;;)
      {
        bool = ((Iterator)localObject1).hasNext();
        if (!bool) {
          break;
        }
        localObject2 = (j)((Iterator)localObject1).next();
        localObject5 = a((j)localObject2);
        localObject6 = ((j)localObject2).b();
        ((HttpRequest)localObject4).e((String)localObject5, (String)localObject6);
        localObject5 = b((j)localObject2);
        localObject2 = ((j)localObject2).c();
        ((HttpRequest)localObject4).e((String)localObject5, (String)localObject2);
      }
    }
    return (HttpRequest)localObject4;
  }
  
  String a(j paramj)
  {
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[1];
    String str = paramj.a();
    arrayOfObject[0] = str;
    return String.format(localLocale, "app[build][libraries][%s][version]", arrayOfObject);
  }
  
  public boolean a(d paramd)
  {
    Object localObject1 = getHttpRequest();
    localObject1 = a((HttpRequest)localObject1, paramd);
    Object localObject2 = b((HttpRequest)localObject1, paramd);
    localObject1 = io.fabric.sdk.android.c.h();
    String str = "Fabric";
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = ((StringBuilder)localObject3).append("Sending app info to ");
    Object localObject4 = getUrl();
    localObject3 = (String)localObject4;
    ((k)localObject1).a(str, (String)localObject3);
    localObject1 = j;
    if (localObject1 != null)
    {
      localObject1 = io.fabric.sdk.android.c.h();
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append("App icon hash is ");
      localObject4 = j.a;
      localObject3 = (String)localObject4;
      ((k)localObject1).a("Fabric", (String)localObject3);
      localObject1 = io.fabric.sdk.android.c.h();
      str = "Fabric";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append("App icon size is ");
      int i = j.c;
      localObject3 = ((StringBuilder)localObject3).append(i).append("x");
      localObject4 = j;
      i = d;
      localObject3 = i;
      ((k)localObject1).a(str, (String)localObject3);
    }
    int j = ((HttpRequest)localObject2).b();
    localObject1 = "POST";
    localObject3 = ((HttpRequest)localObject2).p();
    boolean bool = ((String)localObject1).equals(localObject3);
    int k;
    if (bool)
    {
      localObject1 = "Create";
      localObject3 = io.fabric.sdk.android.c.h();
      Object localObject5 = new java/lang/StringBuilder;
      ((StringBuilder)localObject5).<init>();
      localObject1 = ((StringBuilder)localObject5).append((String)localObject1).append(" app request ID: ");
      localObject5 = "X-REQUEST-ID";
      localObject2 = ((HttpRequest)localObject2).b((String)localObject5);
      localObject1 = (String)localObject2;
      ((k)localObject3).a("Fabric", (String)localObject1);
      localObject1 = io.fabric.sdk.android.c.h();
      localObject2 = "Fabric";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject4 = "Result was ";
      localObject3 = (String)localObject4 + j;
      ((k)localObject1).a((String)localObject2, (String)localObject3);
      k = r.a(j);
      if (k != 0) {
        break label386;
      }
      k = 1;
    }
    for (;;)
    {
      return k;
      localObject1 = "Update";
      break;
      label386:
      int m = 0;
      localObject1 = null;
    }
  }
  
  String b(j paramj)
  {
    Locale localLocale = Locale.US;
    Object[] arrayOfObject = new Object[1];
    String str = paramj.a();
    arrayOfObject[0] = str;
    return String.format(localLocale, "app[build][libraries][%s][type]", arrayOfObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/e/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */