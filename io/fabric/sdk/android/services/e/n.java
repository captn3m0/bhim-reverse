package io.fabric.sdk.android.services.e;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;

public class n
{
  public final String a;
  public final int b;
  public final int c;
  public final int d;
  
  public n(String paramString, int paramInt1, int paramInt2, int paramInt3)
  {
    a = paramString;
    b = paramInt1;
    c = paramInt2;
    d = paramInt3;
  }
  
  public static n a(Context paramContext, String paramString)
  {
    if (paramString != null) {}
    for (;;)
    {
      try
      {
        int i = i.l(paramContext);
        Object localObject1 = c.h();
        localObject3 = "Fabric";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        String str = "App icon resource ID is ";
        localObject4 = ((StringBuilder)localObject4).append(str);
        localObject4 = ((StringBuilder)localObject4).append(i);
        localObject4 = ((StringBuilder)localObject4).toString();
        ((k)localObject1).a((String)localObject3, (String)localObject4);
        localObject3 = new android/graphics/BitmapFactory$Options;
        ((BitmapFactory.Options)localObject3).<init>();
        bool = true;
        inJustDecodeBounds = bool;
        localObject1 = paramContext.getResources();
        BitmapFactory.decodeResource((Resources)localObject1, i, (BitmapFactory.Options)localObject3);
        localObject1 = new io/fabric/sdk/android/services/e/n;
        int j = outWidth;
        int k = outHeight;
        ((n)localObject1).<init>(paramString, i, j, k);
        return (n)localObject1;
      }
      catch (Exception localException)
      {
        k localk = c.h();
        Object localObject3 = "Fabric";
        Object localObject4 = "Failed to load icon";
        localk.e((String)localObject3, (String)localObject4, localException);
      }
      boolean bool = false;
      Object localObject2 = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/e/n.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */