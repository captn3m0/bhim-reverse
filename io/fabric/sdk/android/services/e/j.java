package io.fabric.sdk.android.services.e;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.d.d;
import org.json.JSONObject;

class j
  implements s
{
  private final w a;
  private final v b;
  private final io.fabric.sdk.android.services.b.k c;
  private final g d;
  private final x e;
  private final h f;
  private final io.fabric.sdk.android.services.d.c g;
  
  public j(h paramh, w paramw, io.fabric.sdk.android.services.b.k paramk, v paramv, g paramg, x paramx)
  {
    f = paramh;
    a = paramw;
    c = paramk;
    b = paramv;
    d = paramg;
    e = paramx;
    d locald = new io/fabric/sdk/android/services/d/d;
    h localh = f;
    locald.<init>(localh);
    g = locald;
  }
  
  private void a(JSONObject paramJSONObject, String paramString)
  {
    io.fabric.sdk.android.k localk = io.fabric.sdk.android.c.h();
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = ((StringBuilder)localObject).append(paramString);
    String str = paramJSONObject.toString();
    localObject = str;
    localk.a("Fabric", (String)localObject);
  }
  
  private t b(r paramr)
  {
    localObject1 = null;
    for (;;)
    {
      try
      {
        localObject2 = r.b;
        boolean bool1 = ((r)localObject2).equals(paramr);
        if (!bool1)
        {
          localObject2 = d;
          localObject4 = ((g)localObject2).a();
          if (localObject4 == null) {
            break label232;
          }
          localObject2 = b;
          localObject5 = c;
          localObject2 = ((v)localObject2).a((io.fabric.sdk.android.services.b.k)localObject5, (JSONObject)localObject4);
          if (localObject2 == null) {
            break label200;
          }
          localObject5 = "Loaded cached settings: ";
          a((JSONObject)localObject4, (String)localObject5);
          localObject4 = c;
          long l = ((io.fabric.sdk.android.services.b.k)localObject4).a();
          localObject6 = r.c;
          bool2 = ((r)localObject6).equals(paramr);
          if (!bool2)
          {
            boolean bool3 = ((t)localObject2).a(l);
            if (bool3) {
              continue;
            }
          }
        }
      }
      catch (Exception localException1) {}
      try
      {
        localObject1 = io.fabric.sdk.android.c.h();
        localObject4 = "Fabric";
        localObject5 = "Returning cached settings.";
        ((io.fabric.sdk.android.k)localObject1).a((String)localObject4, (String)localObject5);
        localObject1 = localObject2;
        return (t)localObject1;
      }
      catch (Exception localException2)
      {
        for (;;)
        {
          localObject1 = localObject3;
          Object localObject3 = localException2;
        }
      }
      Object localObject2 = io.fabric.sdk.android.c.h();
      Object localObject4 = "Fabric";
      Object localObject5 = "Cached settings have expired.";
      ((io.fabric.sdk.android.k)localObject2).a((String)localObject4, (String)localObject5);
      continue;
      localObject4 = io.fabric.sdk.android.c.h();
      localObject5 = "Fabric";
      Object localObject6 = "Failed to get cached settings";
      ((io.fabric.sdk.android.k)localObject4).e((String)localObject5, (String)localObject6, localException1);
      continue;
      label200:
      localObject3 = io.fabric.sdk.android.c.h();
      localObject4 = "Fabric";
      localObject5 = "Failed to transform cached settings data.";
      boolean bool2 = false;
      localObject6 = null;
      ((io.fabric.sdk.android.k)localObject3).e((String)localObject4, (String)localObject5, null);
      continue;
      label232:
      localObject3 = io.fabric.sdk.android.c.h();
      localObject4 = "Fabric";
      localObject5 = "No cached settings data found.";
      ((io.fabric.sdk.android.k)localObject3).a((String)localObject4, (String)localObject5);
    }
  }
  
  public t a()
  {
    r localr = r.a;
    return a(localr);
  }
  
  /* Error */
  public t a(r paramr)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: invokestatic 130	io/fabric/sdk/android/c:i	()Z
    //   5: istore_3
    //   6: iload_3
    //   7: ifne +18 -> 25
    //   10: aload_0
    //   11: invokevirtual 132	io/fabric/sdk/android/services/e/j:d	()Z
    //   14: istore_3
    //   15: iload_3
    //   16: ifne +9 -> 25
    //   19: aload_0
    //   20: aload_1
    //   21: invokespecial 134	io/fabric/sdk/android/services/e/j:b	(Lio/fabric/sdk/android/services/e/r;)Lio/fabric/sdk/android/services/e/t;
    //   24: astore_2
    //   25: aload_2
    //   26: ifnonnull +103 -> 129
    //   29: aload_0
    //   30: getfield 36	io/fabric/sdk/android/services/e/j:e	Lio/fabric/sdk/android/services/e/x;
    //   33: astore 4
    //   35: aload_0
    //   36: getfield 28	io/fabric/sdk/android/services/e/j:a	Lio/fabric/sdk/android/services/e/w;
    //   39: astore 5
    //   41: aload 4
    //   43: aload 5
    //   45: invokeinterface 139 2 0
    //   50: astore 4
    //   52: aload 4
    //   54: ifnull +75 -> 129
    //   57: aload_0
    //   58: getfield 32	io/fabric/sdk/android/services/e/j:b	Lio/fabric/sdk/android/services/e/v;
    //   61: astore 5
    //   63: aload_0
    //   64: getfield 30	io/fabric/sdk/android/services/e/j:c	Lio/fabric/sdk/android/services/b/k;
    //   67: astore 6
    //   69: aload 5
    //   71: aload 6
    //   73: aload 4
    //   75: invokeinterface 89 3 0
    //   80: astore_2
    //   81: aload_0
    //   82: getfield 34	io/fabric/sdk/android/services/e/j:d	Lio/fabric/sdk/android/services/e/g;
    //   85: astore 5
    //   87: aload_2
    //   88: getfield 142	io/fabric/sdk/android/services/e/t:g	J
    //   91: lstore 7
    //   93: aload 5
    //   95: lload 7
    //   97: aload 4
    //   99: invokeinterface 145 4 0
    //   104: ldc -109
    //   106: astore 5
    //   108: aload_0
    //   109: aload 4
    //   111: aload 5
    //   113: invokespecial 94	io/fabric/sdk/android/services/e/j:a	(Lorg/json/JSONObject;Ljava/lang/String;)V
    //   116: aload_0
    //   117: invokevirtual 149	io/fabric/sdk/android/services/e/j:b	()Ljava/lang/String;
    //   120: astore 4
    //   122: aload_0
    //   123: aload 4
    //   125: invokevirtual 152	io/fabric/sdk/android/services/e/j:a	(Ljava/lang/String;)Z
    //   128: pop
    //   129: aload_2
    //   130: astore 4
    //   132: aload_2
    //   133: ifnonnull +14 -> 147
    //   136: getstatic 101	io/fabric/sdk/android/services/e/r:c	Lio/fabric/sdk/android/services/e/r;
    //   139: astore_2
    //   140: aload_0
    //   141: aload_2
    //   142: invokespecial 134	io/fabric/sdk/android/services/e/j:b	(Lio/fabric/sdk/android/services/e/r;)Lio/fabric/sdk/android/services/e/t;
    //   145: astore 4
    //   147: aload 4
    //   149: areturn
    //   150: astore 9
    //   152: iconst_0
    //   153: istore_3
    //   154: aconst_null
    //   155: astore 4
    //   157: aload 9
    //   159: astore_2
    //   160: invokestatic 49	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   163: astore 5
    //   165: ldc 51
    //   167: astore 6
    //   169: ldc -102
    //   171: astore 10
    //   173: aload 5
    //   175: aload 6
    //   177: aload 10
    //   179: aload_2
    //   180: invokeinterface 115 4 0
    //   185: goto -38 -> 147
    //   188: astore 9
    //   190: aload_2
    //   191: astore 4
    //   193: aload 9
    //   195: astore_2
    //   196: goto -36 -> 160
    //   199: astore_2
    //   200: goto -40 -> 160
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	203	0	this	j
    //   0	203	1	paramr	r
    //   1	195	2	localObject1	Object
    //   199	1	2	localException1	Exception
    //   5	149	3	bool	boolean
    //   33	159	4	localObject2	Object
    //   39	135	5	localObject3	Object
    //   67	109	6	localObject4	Object
    //   91	5	7	l	long
    //   150	8	9	localException2	Exception
    //   188	6	9	localException3	Exception
    //   171	7	10	str	String
    // Exception table:
    //   from	to	target	type
    //   2	5	150	java/lang/Exception
    //   10	14	150	java/lang/Exception
    //   20	24	150	java/lang/Exception
    //   29	33	188	java/lang/Exception
    //   35	39	188	java/lang/Exception
    //   43	50	188	java/lang/Exception
    //   57	61	188	java/lang/Exception
    //   63	67	188	java/lang/Exception
    //   73	80	188	java/lang/Exception
    //   81	85	188	java/lang/Exception
    //   87	91	188	java/lang/Exception
    //   97	104	188	java/lang/Exception
    //   111	116	188	java/lang/Exception
    //   116	120	188	java/lang/Exception
    //   123	129	188	java/lang/Exception
    //   136	139	199	java/lang/Exception
    //   141	145	199	java/lang/Exception
  }
  
  boolean a(String paramString)
  {
    SharedPreferences.Editor localEditor = g.b();
    localEditor.putString("existing_instance_identifier", paramString);
    return g.a(localEditor);
  }
  
  String b()
  {
    String[] arrayOfString = new String[1];
    String str = i.m(f.getContext());
    arrayOfString[0] = str;
    return i.a(arrayOfString);
  }
  
  String c()
  {
    return g.a().getString("existing_instance_identifier", "");
  }
  
  boolean d()
  {
    String str1 = c();
    String str2 = b();
    boolean bool = str1.equals(str2);
    if (!bool) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      str1 = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/e/j.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */