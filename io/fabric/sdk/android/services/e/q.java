package io.fabric.sdk.android.services.e;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class q
{
  private final AtomicReference a;
  private final CountDownLatch b;
  private s c;
  private boolean d;
  
  private q()
  {
    Object localObject = new java/util/concurrent/atomic/AtomicReference;
    ((AtomicReference)localObject).<init>();
    a = ((AtomicReference)localObject);
    localObject = new java/util/concurrent/CountDownLatch;
    ((CountDownLatch)localObject).<init>(1);
    b = ((CountDownLatch)localObject);
    d = false;
  }
  
  public static q a()
  {
    return q.a.a();
  }
  
  private void a(t paramt)
  {
    a.set(paramt);
    b.countDown();
  }
  
  /* Error */
  public q a(io.fabric.sdk.android.h paramh, io.fabric.sdk.android.services.b.o paramo, io.fabric.sdk.android.services.network.d paramd, String paramString1, String paramString2, String paramString3)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 31	io/fabric/sdk/android/services/e/q:d	Z
    //   6: istore 7
    //   8: iload 7
    //   10: ifeq +11 -> 21
    //   13: aload_0
    //   14: astore 8
    //   16: aload_0
    //   17: monitorexit
    //   18: aload 8
    //   20: areturn
    //   21: aload_0
    //   22: getfield 46	io/fabric/sdk/android/services/e/q:c	Lio/fabric/sdk/android/services/e/s;
    //   25: astore 8
    //   27: aload 8
    //   29: ifnonnull +301 -> 330
    //   32: aload_1
    //   33: invokevirtual 52	io/fabric/sdk/android/h:getContext	()Landroid/content/Context;
    //   36: astore 8
    //   38: aload_2
    //   39: invokevirtual 57	io/fabric/sdk/android/services/b/o:c	()Ljava/lang/String;
    //   42: astore 9
    //   44: new 59	io/fabric/sdk/android/services/b/g
    //   47: astore 10
    //   49: aload 10
    //   51: invokespecial 60	io/fabric/sdk/android/services/b/g:<init>	()V
    //   54: aload 10
    //   56: aload 8
    //   58: invokevirtual 63	io/fabric/sdk/android/services/b/g:a	(Landroid/content/Context;)Ljava/lang/String;
    //   61: astore 10
    //   63: aload_2
    //   64: invokevirtual 66	io/fabric/sdk/android/services/b/o:j	()Ljava/lang/String;
    //   67: astore 11
    //   69: new 68	io/fabric/sdk/android/services/b/s
    //   72: astore 12
    //   74: aload 12
    //   76: invokespecial 69	io/fabric/sdk/android/services/b/s:<init>	()V
    //   79: new 71	io/fabric/sdk/android/services/e/k
    //   82: astore 13
    //   84: aload 13
    //   86: invokespecial 72	io/fabric/sdk/android/services/e/k:<init>	()V
    //   89: new 74	io/fabric/sdk/android/services/e/i
    //   92: astore 14
    //   94: aload 14
    //   96: aload_1
    //   97: invokespecial 77	io/fabric/sdk/android/services/e/i:<init>	(Lio/fabric/sdk/android/h;)V
    //   100: aload 8
    //   102: invokestatic 82	io/fabric/sdk/android/services/b/i:k	(Landroid/content/Context;)Ljava/lang/String;
    //   105: astore 15
    //   107: getstatic 88	java/util/Locale:US	Ljava/util/Locale;
    //   110: astore 16
    //   112: ldc 90
    //   114: astore 17
    //   116: iconst_1
    //   117: istore 18
    //   119: iload 18
    //   121: anewarray 4	java/lang/Object
    //   124: astore 19
    //   126: aconst_null
    //   127: astore 20
    //   129: aload 19
    //   131: iconst_0
    //   132: aload 9
    //   134: aastore
    //   135: aload 16
    //   137: aload 17
    //   139: aload 19
    //   141: invokestatic 96	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   144: astore 9
    //   146: new 98	io/fabric/sdk/android/services/e/l
    //   149: astore 21
    //   151: aload 21
    //   153: aload_1
    //   154: aload 6
    //   156: aload 9
    //   158: aload_3
    //   159: invokespecial 101	io/fabric/sdk/android/services/e/l:<init>	(Lio/fabric/sdk/android/h;Ljava/lang/String;Ljava/lang/String;Lio/fabric/sdk/android/services/network/d;)V
    //   162: aload_2
    //   163: invokevirtual 104	io/fabric/sdk/android/services/b/o:g	()Ljava/lang/String;
    //   166: astore 9
    //   168: aload_2
    //   169: invokevirtual 107	io/fabric/sdk/android/services/b/o:f	()Ljava/lang/String;
    //   172: astore 16
    //   174: aload_2
    //   175: invokevirtual 110	io/fabric/sdk/android/services/b/o:e	()Ljava/lang/String;
    //   178: astore 17
    //   180: aload_2
    //   181: invokevirtual 113	io/fabric/sdk/android/services/b/o:m	()Ljava/lang/String;
    //   184: astore 19
    //   186: aload_2
    //   187: invokevirtual 115	io/fabric/sdk/android/services/b/o:b	()Ljava/lang/String;
    //   190: astore 20
    //   192: aload_2
    //   193: invokevirtual 118	io/fabric/sdk/android/services/b/o:n	()Ljava/lang/String;
    //   196: astore 22
    //   198: iconst_1
    //   199: istore 23
    //   201: iload 23
    //   203: anewarray 92	java/lang/String
    //   206: astore 24
    //   208: aload 8
    //   210: invokestatic 120	io/fabric/sdk/android/services/b/i:m	(Landroid/content/Context;)Ljava/lang/String;
    //   213: astore 8
    //   215: aload 24
    //   217: iconst_0
    //   218: aload 8
    //   220: aastore
    //   221: aload 24
    //   223: invokestatic 123	io/fabric/sdk/android/services/b/i:a	([Ljava/lang/String;)Ljava/lang/String;
    //   226: astore 24
    //   228: aload 11
    //   230: invokestatic 128	io/fabric/sdk/android/services/b/l:a	(Ljava/lang/String;)Lio/fabric/sdk/android/services/b/l;
    //   233: astore 8
    //   235: aload 8
    //   237: invokevirtual 131	io/fabric/sdk/android/services/b/l:a	()I
    //   240: istore 25
    //   242: new 133	io/fabric/sdk/android/services/e/w
    //   245: astore 8
    //   247: aload 5
    //   249: astore 11
    //   251: aload 8
    //   253: aload 10
    //   255: aload 9
    //   257: aload 16
    //   259: aload 17
    //   261: aload 19
    //   263: aload 20
    //   265: aload 22
    //   267: aload 24
    //   269: aload 5
    //   271: aload 4
    //   273: iload 25
    //   275: aload 15
    //   277: invokespecial 136	io/fabric/sdk/android/services/e/w:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    //   280: new 138	io/fabric/sdk/android/services/e/j
    //   283: astore 10
    //   285: aload_1
    //   286: astore 9
    //   288: aload 8
    //   290: astore 16
    //   292: aload 12
    //   294: astore 17
    //   296: aload 13
    //   298: astore 19
    //   300: aload 14
    //   302: astore 20
    //   304: aload 21
    //   306: astore 22
    //   308: aload 10
    //   310: aload_1
    //   311: aload 8
    //   313: aload 12
    //   315: aload 13
    //   317: aload 14
    //   319: aload 21
    //   321: invokespecial 141	io/fabric/sdk/android/services/e/j:<init>	(Lio/fabric/sdk/android/h;Lio/fabric/sdk/android/services/e/w;Lio/fabric/sdk/android/services/b/k;Lio/fabric/sdk/android/services/e/v;Lio/fabric/sdk/android/services/e/g;Lio/fabric/sdk/android/services/e/x;)V
    //   324: aload_0
    //   325: aload 10
    //   327: putfield 46	io/fabric/sdk/android/services/e/q:c	Lio/fabric/sdk/android/services/e/s;
    //   330: iconst_1
    //   331: istore 7
    //   333: aload_0
    //   334: iload 7
    //   336: putfield 31	io/fabric/sdk/android/services/e/q:d	Z
    //   339: aload_0
    //   340: astore 8
    //   342: goto -326 -> 16
    //   345: astore 8
    //   347: aload_0
    //   348: monitorexit
    //   349: aload 8
    //   351: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	352	0	this	q
    //   0	352	1	paramh	io.fabric.sdk.android.h
    //   0	352	2	paramo	io.fabric.sdk.android.services.b.o
    //   0	352	3	paramd	io.fabric.sdk.android.services.network.d
    //   0	352	4	paramString1	String
    //   0	352	5	paramString2	String
    //   0	352	6	paramString3	String
    //   6	329	7	bool	boolean
    //   14	327	8	localObject1	Object
    //   345	5	8	localObject2	Object
    //   42	245	9	localObject3	Object
    //   47	279	10	localObject4	Object
    //   67	183	11	str1	String
    //   72	242	12	locals	io.fabric.sdk.android.services.b.s
    //   82	234	13	localk	k
    //   92	226	14	locali	i
    //   105	171	15	str2	String
    //   110	181	16	localObject5	Object
    //   114	181	17	localObject6	Object
    //   117	3	18	i	int
    //   124	175	19	localObject7	Object
    //   127	176	20	localObject8	Object
    //   149	171	21	locall	l
    //   196	111	22	localObject9	Object
    //   199	3	23	j	int
    //   206	62	24	localObject10	Object
    //   240	34	25	k	int
    // Exception table:
    //   from	to	target	type
    //   2	6	345	finally
    //   21	25	345	finally
    //   32	36	345	finally
    //   38	42	345	finally
    //   44	47	345	finally
    //   49	54	345	finally
    //   56	61	345	finally
    //   63	67	345	finally
    //   69	72	345	finally
    //   74	79	345	finally
    //   79	82	345	finally
    //   84	89	345	finally
    //   89	92	345	finally
    //   96	100	345	finally
    //   100	105	345	finally
    //   107	110	345	finally
    //   119	124	345	finally
    //   132	135	345	finally
    //   139	144	345	finally
    //   146	149	345	finally
    //   158	162	345	finally
    //   162	166	345	finally
    //   168	172	345	finally
    //   174	178	345	finally
    //   180	184	345	finally
    //   186	190	345	finally
    //   192	196	345	finally
    //   201	206	345	finally
    //   208	213	345	finally
    //   218	221	345	finally
    //   221	226	345	finally
    //   228	233	345	finally
    //   235	240	345	finally
    //   242	245	345	finally
    //   275	280	345	finally
    //   280	283	345	finally
    //   319	324	345	finally
    //   325	330	345	finally
    //   334	339	345	finally
  }
  
  public Object a(q.b paramb, Object paramObject)
  {
    t localt = (t)a.get();
    if (localt == null) {}
    for (;;)
    {
      return paramObject;
      paramObject = paramb.usingSettings(localt);
    }
  }
  
  public t b()
  {
    try
    {
      localObject = b;
      ((CountDownLatch)localObject).await();
      localObject = a;
      localObject = ((AtomicReference)localObject).get();
      localObject = (t)localObject;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        Object localObject;
        k localk = c.h();
        String str1 = "Fabric";
        String str2 = "Interrupted while waiting for settings data.";
        localk.e(str1, str2);
        localk = null;
      }
    }
    return (t)localObject;
  }
  
  /* Error */
  public boolean c()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 46	io/fabric/sdk/android/services/e/q:c	Lio/fabric/sdk/android/services/e/s;
    //   6: astore_1
    //   7: aload_1
    //   8: invokeinterface 178 1 0
    //   13: astore_1
    //   14: aload_0
    //   15: aload_1
    //   16: invokespecial 181	io/fabric/sdk/android/services/e/q:a	(Lio/fabric/sdk/android/services/e/t;)V
    //   19: aload_1
    //   20: ifnull +9 -> 29
    //   23: iconst_1
    //   24: istore_2
    //   25: aload_0
    //   26: monitorexit
    //   27: iload_2
    //   28: ireturn
    //   29: iconst_0
    //   30: istore_2
    //   31: aconst_null
    //   32: astore_1
    //   33: goto -8 -> 25
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	41	0	this	q
    //   6	27	1	localObject1	Object
    //   36	4	1	localObject2	Object
    //   24	7	2	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   2	6	36	finally
    //   7	13	36	finally
    //   15	19	36	finally
  }
  
  /* Error */
  public boolean d()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 46	io/fabric/sdk/android/services/e/q:c	Lio/fabric/sdk/android/services/e/s;
    //   6: astore_1
    //   7: getstatic 186	io/fabric/sdk/android/services/e/r:b	Lio/fabric/sdk/android/services/e/r;
    //   10: astore_2
    //   11: aload_1
    //   12: aload_2
    //   13: invokeinterface 189 2 0
    //   18: astore_1
    //   19: aload_0
    //   20: aload_1
    //   21: invokespecial 181	io/fabric/sdk/android/services/e/q:a	(Lio/fabric/sdk/android/services/e/t;)V
    //   24: aload_1
    //   25: ifnonnull +24 -> 49
    //   28: invokestatic 162	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   31: astore_2
    //   32: ldc -92
    //   34: astore_3
    //   35: ldc -65
    //   37: astore 4
    //   39: aload_2
    //   40: aload_3
    //   41: aload 4
    //   43: aconst_null
    //   44: invokeinterface 194 4 0
    //   49: aload_1
    //   50: ifnull +11 -> 61
    //   53: iconst_1
    //   54: istore 5
    //   56: aload_0
    //   57: monitorexit
    //   58: iload 5
    //   60: ireturn
    //   61: iconst_0
    //   62: istore 5
    //   64: aconst_null
    //   65: astore_1
    //   66: goto -10 -> 56
    //   69: astore_1
    //   70: aload_0
    //   71: monitorexit
    //   72: aload_1
    //   73: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	74	0	this	q
    //   6	60	1	localObject1	Object
    //   69	4	1	localObject2	Object
    //   10	30	2	localObject3	Object
    //   34	7	3	str1	String
    //   37	5	4	str2	String
    //   54	9	5	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   2	6	69	finally
    //   7	10	69	finally
    //   12	18	69	finally
    //   20	24	69	finally
    //   28	31	69	finally
    //   43	49	69	finally
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/e/q.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */