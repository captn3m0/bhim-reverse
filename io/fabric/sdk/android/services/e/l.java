package io.fabric.sdk.android.services.e;

import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.a;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.d;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class l
  extends a
  implements x
{
  public l(h paramh, String paramString1, String paramString2, d paramd)
  {
    this(paramh, paramString1, paramString2, paramd, localc);
  }
  
  l(h paramh, String paramString1, String paramString2, d paramd, io.fabric.sdk.android.services.network.c paramc)
  {
    super(paramh, paramString1, paramString2, paramd, paramc);
  }
  
  private HttpRequest a(HttpRequest paramHttpRequest, w paramw)
  {
    String str = a;
    a(paramHttpRequest, "X-CRASHLYTICS-API-KEY", str);
    a(paramHttpRequest, "X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    str = kit.getVersion();
    a(paramHttpRequest, "X-CRASHLYTICS-API-CLIENT-VERSION", str);
    a(paramHttpRequest, "Accept", "application/json");
    str = b;
    a(paramHttpRequest, "X-CRASHLYTICS-DEVICE-MODEL", str);
    str = c;
    a(paramHttpRequest, "X-CRASHLYTICS-OS-BUILD-VERSION", str);
    str = d;
    a(paramHttpRequest, "X-CRASHLYTICS-OS-DISPLAY-VERSION", str);
    str = e;
    a(paramHttpRequest, "X-CRASHLYTICS-ADVERTISING-TOKEN", str);
    str = f;
    a(paramHttpRequest, "X-CRASHLYTICS-INSTALLATION-ID", str);
    str = g;
    a(paramHttpRequest, "X-CRASHLYTICS-ANDROID-ID", str);
    return paramHttpRequest;
  }
  
  private JSONObject a(String paramString)
  {
    try
    {
      localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>(paramString);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        JSONObject localJSONObject;
        Object localObject1 = io.fabric.sdk.android.c.h();
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("Failed to parse settings JSON from ");
        String str = getUrl();
        localObject2 = str;
        ((k)localObject1).a("Fabric", (String)localObject2, localException);
        k localk = io.fabric.sdk.android.c.h();
        localObject1 = "Fabric";
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject2 = "Settings response ";
        localObject3 = (String)localObject2 + paramString;
        localk.a((String)localObject1, (String)localObject3);
        localk = null;
      }
    }
    return localJSONObject;
  }
  
  private void a(HttpRequest paramHttpRequest, String paramString1, String paramString2)
  {
    if (paramString2 != null) {
      paramHttpRequest.a(paramString1, paramString2);
    }
  }
  
  private Map b(w paramw)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str1 = j;
    localHashMap.put("build_version", str1);
    str1 = i;
    localHashMap.put("display_version", str1);
    int i = k;
    str1 = Integer.toString(i);
    localHashMap.put("source", str1);
    String str2 = l;
    if (str2 != null)
    {
      str2 = "icon_hash";
      str1 = l;
      localHashMap.put(str2, str1);
    }
    str2 = h;
    boolean bool = i.c(str2);
    if (!bool)
    {
      str1 = "instance";
      localHashMap.put(str1, str2);
    }
    return localHashMap;
  }
  
  public JSONObject a(w paramw)
  {
    Object localObject1 = null;
    try
    {
      Object localObject2 = b(paramw);
      localObject1 = getHttpRequest((Map)localObject2);
      localObject1 = a((HttpRequest)localObject1, paramw);
      localk = io.fabric.sdk.android.c.h();
      str1 = "Fabric";
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      str2 = "Requesting settings from ";
      localObject4 = ((StringBuilder)localObject4).append(str2);
      str2 = getUrl();
      localObject4 = ((StringBuilder)localObject4).append(str2);
      localObject4 = ((StringBuilder)localObject4).toString();
      localk.a(str1, (String)localObject4);
      localk = io.fabric.sdk.android.c.h();
      str1 = "Fabric";
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      str2 = "Settings query params were: ";
      localObject4 = ((StringBuilder)localObject4).append(str2);
      localObject2 = ((StringBuilder)localObject4).append(localObject2);
      localObject2 = ((StringBuilder)localObject2).toString();
      localk.a(str1, (String)localObject2);
      localObject2 = a((HttpRequest)localObject1);
      return (JSONObject)localObject2;
    }
    finally
    {
      k localk;
      String str1;
      Object localObject4;
      String str2;
      if (localObject1 != null)
      {
        localk = io.fabric.sdk.android.c.h();
        str1 = "Fabric";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject4 = ((StringBuilder)localObject4).append("Settings request ID: ");
        str2 = "X-REQUEST-ID";
        localObject1 = ((HttpRequest)localObject1).b(str2);
        localObject1 = (String)localObject1;
        localk.a(str1, (String)localObject1);
      }
    }
  }
  
  JSONObject a(HttpRequest paramHttpRequest)
  {
    int i = paramHttpRequest.b();
    Object localObject1 = io.fabric.sdk.android.c.h();
    Object localObject2 = "Fabric";
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    String str = "Settings result was: ";
    localObject3 = str + i;
    ((k)localObject1).a((String)localObject2, (String)localObject3);
    boolean bool = a(i);
    if (bool) {
      localObject4 = paramHttpRequest.e();
    }
    for (Object localObject4 = a((String)localObject4);; localObject4 = null)
    {
      return (JSONObject)localObject4;
      localObject4 = io.fabric.sdk.android.c.h();
      localObject1 = "Fabric";
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("Failed to retrieve settings from ");
      localObject3 = getUrl();
      localObject2 = (String)localObject3;
      ((k)localObject4).e((String)localObject1, (String)localObject2);
      bool = false;
    }
  }
  
  boolean a(int paramInt)
  {
    int i = 200;
    if (paramInt != i)
    {
      i = 201;
      if (paramInt != i)
      {
        i = 202;
        if (paramInt != i)
        {
          i = 203;
          if (paramInt != i) {
            break label40;
          }
        }
      }
    }
    label40:
    for (i = 1;; i = 0) {
      return i;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/e/l.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */