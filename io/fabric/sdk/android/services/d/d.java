package io.fabric.sdk.android.services.d;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import io.fabric.sdk.android.h;

public class d
  implements c
{
  private final SharedPreferences a;
  private final String b;
  private final Context c;
  
  public d(Context paramContext, String paramString)
  {
    if (paramContext == null)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Cannot get directory before context has been set. Call Fabric.with() first");
      throw ((Throwable)localObject);
    }
    c = paramContext;
    b = paramString;
    Object localObject = c;
    String str = b;
    localObject = ((Context)localObject).getSharedPreferences(str, 0);
    a = ((SharedPreferences)localObject);
  }
  
  public d(h paramh)
  {
    this(localContext, str);
  }
  
  public SharedPreferences a()
  {
    return a;
  }
  
  public boolean a(SharedPreferences.Editor paramEditor)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 9;
    if (i >= j) {
      paramEditor.apply();
    }
    boolean bool;
    for (i = 1;; bool = paramEditor.commit()) {
      return i;
    }
  }
  
  public SharedPreferences.Editor b()
  {
    return a.edit();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/d/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */