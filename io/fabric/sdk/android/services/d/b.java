package io.fabric.sdk.android.services.d;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import java.io.File;

public class b
  implements a
{
  private final Context a;
  private final String b;
  private final String c;
  
  public b(h paramh)
  {
    Object localObject = paramh.getContext();
    if (localObject == null)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Cannot get directory before context has been set. Call Fabric.with() first");
      throw ((Throwable)localObject);
    }
    localObject = paramh.getContext();
    a = ((Context)localObject);
    localObject = paramh.getPath();
    b = ((String)localObject);
    localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = ((StringBuilder)localObject).append("Android/");
    String str = a.getPackageName();
    localObject = str;
    c = ((String)localObject);
  }
  
  public File a()
  {
    File localFile = a.getFilesDir();
    return a(localFile);
  }
  
  File a(File paramFile)
  {
    k localk;
    String str1;
    String str2;
    if (paramFile != null)
    {
      boolean bool = paramFile.exists();
      if (!bool)
      {
        bool = paramFile.mkdirs();
        if (!bool) {}
      }
      else
      {
        return paramFile;
      }
      localk = c.h();
      str1 = "Fabric";
      str2 = "Couldn't create file";
      localk.d(str1, str2);
    }
    for (;;)
    {
      paramFile = null;
      break;
      localk = c.h();
      str1 = "Fabric";
      str2 = "Null File";
      localk.a(str1, str2);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/d/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */