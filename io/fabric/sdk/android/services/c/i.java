package io.fabric.sdk.android.services.c;

import android.content.Context;

public class i
  implements Runnable
{
  private final Context a;
  private final e b;
  
  public i(Context paramContext, e parame)
  {
    a = paramContext;
    b = parame;
  }
  
  public void run()
  {
    try
    {
      Object localObject1 = a;
      localObject2 = "Performing time based file roll over.";
      io.fabric.sdk.android.services.b.i.a((Context)localObject1, (String)localObject2);
      localObject1 = b;
      boolean bool = ((e)localObject1).rollFileOver();
      if (!bool)
      {
        localObject1 = b;
        ((e)localObject1).cancelTimeBasedFileRollOver();
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = a;
        String str = "Failed to roll over file";
        io.fabric.sdk.android.services.b.i.a((Context)localObject2, str, localException);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/c/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */