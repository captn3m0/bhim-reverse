package io.fabric.sdk.android.services.c;

import android.content.Context;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.q;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class h
  implements c
{
  private final Context a;
  private final File b;
  private final String c;
  private final File d;
  private q e;
  private File f;
  
  public h(Context paramContext, File paramFile, String paramString1, String paramString2)
  {
    a = paramContext;
    b = paramFile;
    c = paramString2;
    Object localObject = new java/io/File;
    File localFile = b;
    ((File)localObject).<init>(localFile, paramString1);
    d = ((File)localObject);
    localObject = new io/fabric/sdk/android/services/b/q;
    localFile = d;
    ((q)localObject).<init>(localFile);
    e = ((q)localObject);
    e();
  }
  
  private void a(File paramFile1, File paramFile2)
  {
    OutputStream localOutputStream = null;
    try
    {
      FileInputStream localFileInputStream = new java/io/FileInputStream;
      localFileInputStream.<init>(paramFile1);
      int i;
      byte[] arrayOfByte;
      i.a(localFileInputStream, "Failed to close file input stream");
    }
    finally
    {
      try
      {
        localOutputStream = a(paramFile2);
        i = 1024;
        arrayOfByte = new byte[i];
        i.a(localFileInputStream, localOutputStream, arrayOfByte);
        i.a(localFileInputStream, "Failed to close file input stream");
        i.a(localOutputStream, "Failed to close output stream");
        paramFile1.delete();
        return;
      }
      finally
      {
        for (;;) {}
      }
      localObject1 = finally;
      localFileInputStream = null;
    }
    i.a(localOutputStream, "Failed to close output stream");
    paramFile1.delete();
    throw ((Throwable)localObject1);
  }
  
  private void e()
  {
    File localFile1 = new java/io/File;
    File localFile2 = b;
    String str = c;
    localFile1.<init>(localFile2, str);
    f = localFile1;
    localFile1 = f;
    boolean bool = localFile1.exists();
    if (!bool)
    {
      localFile1 = f;
      localFile1.mkdirs();
    }
  }
  
  public int a()
  {
    return e.a();
  }
  
  public OutputStream a(File paramFile)
  {
    FileOutputStream localFileOutputStream = new java/io/FileOutputStream;
    localFileOutputStream.<init>(paramFile);
    return localFileOutputStream;
  }
  
  public List a(int paramInt)
  {
    ArrayList localArrayList = new java/util/ArrayList;
    localArrayList.<init>();
    File[] arrayOfFile = f.listFiles();
    int i = arrayOfFile.length;
    int j = 0;
    for (;;)
    {
      if (j < i)
      {
        File localFile = arrayOfFile[j];
        localArrayList.add(localFile);
        int k = localArrayList.size();
        if (k < paramInt) {}
      }
      else
      {
        return localArrayList;
      }
      j += 1;
    }
  }
  
  public void a(String paramString)
  {
    e.close();
    Object localObject = d;
    File localFile1 = new java/io/File;
    File localFile2 = f;
    localFile1.<init>(localFile2, paramString);
    a((File)localObject, localFile1);
    localObject = new io/fabric/sdk/android/services/b/q;
    localFile1 = d;
    ((q)localObject).<init>(localFile1);
    e = ((q)localObject);
  }
  
  public void a(List paramList)
  {
    Iterator localIterator = paramList.iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      File localFile = (File)localIterator.next();
      Context localContext = a;
      int i = 1;
      Object[] arrayOfObject = new Object[i];
      String str1 = localFile.getName();
      arrayOfObject[0] = str1;
      String str2 = String.format("deleting sent analytics file %s", arrayOfObject);
      i.a(localContext, str2);
      localFile.delete();
    }
  }
  
  public void a(byte[] paramArrayOfByte)
  {
    e.a(paramArrayOfByte);
  }
  
  public boolean a(int paramInt1, int paramInt2)
  {
    return e.a(paramInt1, paramInt2);
  }
  
  public boolean b()
  {
    return e.b();
  }
  
  public List c()
  {
    return Arrays.asList(f.listFiles());
  }
  
  public void d()
  {
    try
    {
      q localq = e;
      localq.close();
    }
    catch (IOException localIOException)
    {
      for (;;) {}
    }
    d.delete();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/c/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */