package io.fabric.sdk.android.services.c;

import android.content.Context;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.k;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.TreeSet;
import java.util.concurrent.CopyOnWriteArrayList;

public abstract class b
{
  public static final int MAX_BYTE_SIZE_PER_FILE = 8000;
  public static final int MAX_FILES_IN_BATCH = 1;
  public static final int MAX_FILES_TO_KEEP = 100;
  public static final String ROLL_OVER_FILE_NAME_SEPARATOR = "_";
  protected final Context context;
  protected final k currentTimeProvider;
  private final int defaultMaxFilesToKeep;
  protected final c eventStorage;
  protected volatile long lastRollOverTime;
  protected final List rollOverListeners;
  protected final a transform;
  
  public b(Context paramContext, a parama, k paramk, c paramc, int paramInt)
  {
    Object localObject = new java/util/concurrent/CopyOnWriteArrayList;
    ((CopyOnWriteArrayList)localObject).<init>();
    rollOverListeners = ((List)localObject);
    localObject = paramContext.getApplicationContext();
    context = ((Context)localObject);
    transform = parama;
    eventStorage = paramc;
    currentTimeProvider = paramk;
    long l = currentTimeProvider.a();
    lastRollOverTime = l;
    defaultMaxFilesToKeep = paramInt;
  }
  
  private void rollFileOverIfNeeded(int paramInt)
  {
    Object localObject = eventStorage;
    int i = getMaxByteSizePerFile();
    boolean bool = ((c)localObject).a(paramInt, i);
    if (!bool)
    {
      localObject = Locale.US;
      Object[] arrayOfObject = new Object[3];
      Integer localInteger = Integer.valueOf(eventStorage.a());
      arrayOfObject[0] = localInteger;
      localInteger = Integer.valueOf(paramInt);
      arrayOfObject[1] = localInteger;
      int j = 2;
      int k = getMaxByteSizePerFile();
      localInteger = Integer.valueOf(k);
      arrayOfObject[j] = localInteger;
      localObject = String.format((Locale)localObject, "session analytics events file is %d bytes, new event is %d bytes, this is over flush limit of %d, rolling it over", arrayOfObject);
      Context localContext = context;
      int m = 4;
      String str = "Fabric";
      i.a(localContext, m, str, (String)localObject);
      rollFileOver();
    }
  }
  
  private void triggerRollOverOnListeners(String paramString)
  {
    Object localObject = rollOverListeners;
    Iterator localIterator = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject = (d)localIterator.next();
      try
      {
        ((d)localObject).onRollOver(paramString);
      }
      catch (Exception localException)
      {
        Context localContext = context;
        String str = "One of the roll over listeners threw an exception";
        i.a(localContext, str, localException);
      }
    }
  }
  
  public void deleteAllEventsFiles()
  {
    c localc = eventStorage;
    List localList = eventStorage.c();
    localc.a(localList);
    eventStorage.d();
  }
  
  public void deleteOldestInRollOverIfOverMax()
  {
    Object localObject1 = eventStorage.c();
    int i = getMaxFilesToKeep();
    int j = ((List)localObject1).size();
    if (j <= i) {}
    for (;;)
    {
      return;
      j = ((List)localObject1).size() - i;
      Object localObject2 = context;
      Object localObject3 = Locale.US;
      String str = "Found %d files in  roll over directory, this is greater than %d, deleting %d oldest files";
      int k = 3;
      Object localObject4 = new Object[k];
      int m = ((List)localObject1).size();
      Integer localInteger1 = Integer.valueOf(m);
      localObject4[0] = localInteger1;
      int n = 1;
      Object localObject5 = Integer.valueOf(i);
      localObject4[n] = localObject5;
      i = 2;
      Integer localInteger2 = Integer.valueOf(j);
      localObject4[i] = localInteger2;
      localObject5 = String.format((Locale)localObject3, str, (Object[])localObject4);
      i.a((Context)localObject2, (String)localObject5);
      localObject5 = new java/util/TreeSet;
      localObject2 = new io/fabric/sdk/android/services/c/b$1;
      ((b.1)localObject2).<init>(this);
      ((TreeSet)localObject5).<init>((Comparator)localObject2);
      localObject2 = ((List)localObject1).iterator();
      boolean bool;
      for (;;)
      {
        bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (File)((Iterator)localObject2).next();
        localObject3 = ((File)localObject1).getName();
        long l = parseCreationTimestampFromFileName((String)localObject3);
        localObject4 = new io/fabric/sdk/android/services/c/b$a;
        ((b.a)localObject4).<init>((File)localObject1, l);
        ((TreeSet)localObject5).add(localObject4);
      }
      localObject2 = new java/util/ArrayList;
      ((ArrayList)localObject2).<init>();
      localObject5 = ((TreeSet)localObject5).iterator();
      int i1;
      do
      {
        bool = ((Iterator)localObject5).hasNext();
        if (!bool) {
          break;
        }
        localObject1 = nexta;
        ((ArrayList)localObject2).add(localObject1);
        i1 = ((ArrayList)localObject2).size();
      } while (i1 != j);
      localObject1 = eventStorage;
      ((c)localObject1).a((List)localObject2);
    }
  }
  
  public void deleteSentFiles(List paramList)
  {
    eventStorage.a(paramList);
  }
  
  protected abstract String generateUniqueRollOverFileName();
  
  public List getBatchOfFilesToSend()
  {
    return eventStorage.a(1);
  }
  
  public long getLastRollOverTime()
  {
    return lastRollOverTime;
  }
  
  protected int getMaxByteSizePerFile()
  {
    return 8000;
  }
  
  protected int getMaxFilesToKeep()
  {
    return defaultMaxFilesToKeep;
  }
  
  public long parseCreationTimestampFromFileName(String paramString)
  {
    long l = 0L;
    Object localObject = paramString.split("_");
    int i = localObject.length;
    int j = 3;
    if (i != j) {}
    for (;;)
    {
      return l;
      i = 2;
      try
      {
        localObject = localObject[i];
        localObject = Long.valueOf((String)localObject);
        l = ((Long)localObject).longValue();
      }
      catch (NumberFormatException localNumberFormatException) {}
    }
  }
  
  public void registerRollOverListener(d paramd)
  {
    if (paramd != null)
    {
      List localList = rollOverListeners;
      localList.add(paramd);
    }
  }
  
  public boolean rollFileOver()
  {
    boolean bool1 = true;
    Object localObject1 = null;
    String str1 = null;
    Object localObject2 = eventStorage;
    boolean bool2 = ((c)localObject2).b();
    if (!bool2)
    {
      str1 = generateUniqueRollOverFileName();
      eventStorage.a(str1);
      localObject2 = context;
      int i = 4;
      String str2 = "Fabric";
      Locale localLocale = Locale.US;
      String str3 = "generated new file %s";
      Object[] arrayOfObject = new Object[bool1];
      arrayOfObject[0] = str1;
      localObject1 = String.format(localLocale, str3, arrayOfObject);
      i.a((Context)localObject2, i, str2, (String)localObject1);
      localObject1 = currentTimeProvider;
      long l = ((k)localObject1).a();
      lastRollOverTime = l;
    }
    for (;;)
    {
      triggerRollOverOnListeners(str1);
      return bool1;
      bool1 = false;
    }
  }
  
  public void writeEvent(Object paramObject)
  {
    byte[] arrayOfByte = transform.toBytes(paramObject);
    int i = arrayOfByte.length;
    rollFileOverIfNeeded(i);
    eventStorage.a(arrayOfByte);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/c/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */