package io.fabric.sdk.android.services.network;

import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

public abstract interface HttpRequest$b
{
  public static final b a;
  
  static
  {
    HttpRequest.b.1 local1 = new io/fabric/sdk/android/services/network/HttpRequest$b$1;
    local1.<init>();
    a = local1;
  }
  
  public abstract HttpURLConnection a(URL paramURL);
  
  public abstract HttpURLConnection a(URL paramURL, Proxy paramProxy);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/HttpRequest$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */