package io.fabric.sdk.android.services.network;

import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;

public abstract class HttpRequest$a
  extends HttpRequest.c
{
  private final Closeable a;
  private final boolean b;
  
  protected HttpRequest$a(Closeable paramCloseable, boolean paramBoolean)
  {
    a = paramCloseable;
    b = paramBoolean;
  }
  
  protected void c()
  {
    Object localObject = a;
    boolean bool = localObject instanceof Flushable;
    if (bool)
    {
      localObject = (Flushable)a;
      ((Flushable)localObject).flush();
    }
    bool = b;
    if (bool) {}
    for (;;)
    {
      try
      {
        localObject = a;
        ((Closeable)localObject).close();
        return;
      }
      catch (IOException localIOException)
      {
        continue;
      }
      localObject = a;
      ((Closeable)localObject).close();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/HttpRequest$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */