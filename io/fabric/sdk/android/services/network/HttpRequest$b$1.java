package io.fabric.sdk.android.services.network;

import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;

final class HttpRequest$b$1
  implements HttpRequest.b
{
  public HttpURLConnection a(URL paramURL)
  {
    return (HttpURLConnection)paramURL.openConnection();
  }
  
  public HttpURLConnection a(URL paramURL, Proxy paramProxy)
  {
    return (HttpURLConnection)paramURL.openConnection(paramProxy);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/HttpRequest$b$1.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */