package io.fabric.sdk.android.services.network;

import java.io.InputStream;

public abstract interface f
{
  public abstract String getKeyStorePassword();
  
  public abstract InputStream getKeyStoreStream();
  
  public abstract long getPinCreationTimeInMillis();
  
  public abstract String[] getPins();
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */