package io.fabric.sdk.android.services.network;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.zip.GZIPInputStream;

public class HttpRequest
{
  private static final String[] b = new String[0];
  private static HttpRequest.b c = HttpRequest.b.a;
  public final URL a;
  private HttpURLConnection d = null;
  private final String e;
  private HttpRequest.d f;
  private boolean g;
  private boolean h = true;
  private boolean i;
  private int j;
  private String k;
  private int l;
  
  public HttpRequest(CharSequence paramCharSequence, String paramString)
  {
    URL localURL = null;
    i = false;
    int m = 8192;
    j = m;
    try
    {
      localURL = new java/net/URL;
      localObject = paramCharSequence.toString();
      localURL.<init>((String)localObject);
      a = localURL;
      e = paramString;
      return;
    }
    catch (MalformedURLException localMalformedURLException)
    {
      Object localObject = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
      ((HttpRequest.HttpRequestException)localObject).<init>(localMalformedURLException);
      throw ((Throwable)localObject);
    }
  }
  
  public static HttpRequest a(CharSequence paramCharSequence, Map paramMap, boolean paramBoolean)
  {
    String str = a(paramCharSequence, paramMap);
    if (paramBoolean) {
      str = a(str);
    }
    return b(str);
  }
  
  /* Error */
  public static String a(CharSequence paramCharSequence)
  {
    // Byte code:
    //   0: new 50	java/net/URL
    //   3: astore_1
    //   4: aload_0
    //   5: invokeinterface 56 1 0
    //   10: astore_2
    //   11: aload_1
    //   12: aload_2
    //   13: invokespecial 59	java/net/URL:<init>	(Ljava/lang/String;)V
    //   16: aload_1
    //   17: invokevirtual 82	java/net/URL:getHost	()Ljava/lang/String;
    //   20: astore_3
    //   21: aload_1
    //   22: invokevirtual 86	java/net/URL:getPort	()I
    //   25: istore 4
    //   27: iconst_m1
    //   28: istore 5
    //   30: iload 4
    //   32: iload 5
    //   34: if_icmpeq +52 -> 86
    //   37: new 88	java/lang/StringBuilder
    //   40: astore 6
    //   42: aload 6
    //   44: invokespecial 89	java/lang/StringBuilder:<init>	()V
    //   47: aload 6
    //   49: aload_3
    //   50: invokevirtual 93	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   53: astore 6
    //   55: bipush 58
    //   57: istore 7
    //   59: aload 6
    //   61: iload 7
    //   63: invokevirtual 97	java/lang/StringBuilder:append	(C)Ljava/lang/StringBuilder;
    //   66: astore 6
    //   68: iload 4
    //   70: invokestatic 102	java/lang/Integer:toString	(I)Ljava/lang/String;
    //   73: astore_2
    //   74: aload 6
    //   76: aload_2
    //   77: invokevirtual 93	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   80: astore_2
    //   81: aload_2
    //   82: invokevirtual 103	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   85: astore_3
    //   86: new 105	java/net/URI
    //   89: astore_2
    //   90: aload_1
    //   91: invokevirtual 108	java/net/URL:getProtocol	()Ljava/lang/String;
    //   94: astore 6
    //   96: aload_1
    //   97: invokevirtual 111	java/net/URL:getPath	()Ljava/lang/String;
    //   100: astore 8
    //   102: aload_1
    //   103: invokevirtual 114	java/net/URL:getQuery	()Ljava/lang/String;
    //   106: astore_1
    //   107: aload_2
    //   108: aload 6
    //   110: aload_3
    //   111: aload 8
    //   113: aload_1
    //   114: aconst_null
    //   115: invokespecial 117	java/net/URI:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   118: aload_2
    //   119: invokevirtual 120	java/net/URI:toASCIIString	()Ljava/lang/String;
    //   122: astore_2
    //   123: bipush 63
    //   125: istore 5
    //   127: aload_2
    //   128: iload 5
    //   130: invokevirtual 125	java/lang/String:indexOf	(I)I
    //   133: istore 5
    //   135: iload 5
    //   137: ifle +99 -> 236
    //   140: iload 5
    //   142: iconst_1
    //   143: iadd
    //   144: istore 7
    //   146: aload_2
    //   147: invokevirtual 128	java/lang/String:length	()I
    //   150: istore 9
    //   152: iload 7
    //   154: iload 9
    //   156: if_icmpge +80 -> 236
    //   159: new 88	java/lang/StringBuilder
    //   162: astore_3
    //   163: aload_3
    //   164: invokespecial 89	java/lang/StringBuilder:<init>	()V
    //   167: iconst_0
    //   168: istore 9
    //   170: aconst_null
    //   171: astore 8
    //   173: iload 5
    //   175: iconst_1
    //   176: iadd
    //   177: istore 10
    //   179: aload_2
    //   180: iconst_0
    //   181: iload 10
    //   183: invokevirtual 132	java/lang/String:substring	(II)Ljava/lang/String;
    //   186: astore 8
    //   188: aload_3
    //   189: aload 8
    //   191: invokevirtual 93	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: astore_3
    //   195: iload 5
    //   197: iconst_1
    //   198: iadd
    //   199: istore 5
    //   201: aload_2
    //   202: iload 5
    //   204: invokevirtual 134	java/lang/String:substring	(I)Ljava/lang/String;
    //   207: astore_2
    //   208: ldc -120
    //   210: astore 6
    //   212: ldc -118
    //   214: astore 8
    //   216: aload_2
    //   217: aload 6
    //   219: aload 8
    //   221: invokevirtual 142	java/lang/String:replace	(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   224: astore_2
    //   225: aload_3
    //   226: aload_2
    //   227: invokevirtual 93	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   230: astore_2
    //   231: aload_2
    //   232: invokevirtual 103	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   235: astore_2
    //   236: aload_2
    //   237: areturn
    //   238: astore_2
    //   239: new 65	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
    //   242: astore 6
    //   244: aload 6
    //   246: aload_2
    //   247: invokespecial 68	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
    //   250: aload 6
    //   252: athrow
    //   253: astore_2
    //   254: new 144	java/io/IOException
    //   257: astore 6
    //   259: aload 6
    //   261: ldc -110
    //   263: invokespecial 147	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   266: aload 6
    //   268: aload_2
    //   269: invokevirtual 151	java/io/IOException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   272: pop
    //   273: new 65	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
    //   276: astore_2
    //   277: aload_2
    //   278: aload 6
    //   280: invokespecial 68	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
    //   283: aload_2
    //   284: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	285	0	paramCharSequence	CharSequence
    //   3	111	1	localObject1	Object
    //   10	227	2	localObject2	Object
    //   238	9	2	localIOException	IOException
    //   253	16	2	localURISyntaxException	java.net.URISyntaxException
    //   276	8	2	localHttpRequestException	HttpRequest.HttpRequestException
    //   20	206	3	localObject3	Object
    //   25	44	4	m	int
    //   28	175	5	n	int
    //   40	239	6	localObject4	Object
    //   57	5	7	c1	char
    //   144	13	7	i1	int
    //   100	120	8	str	String
    //   150	19	9	i2	int
    //   177	5	10	i3	int
    // Exception table:
    //   from	to	target	type
    //   0	3	238	java/io/IOException
    //   4	10	238	java/io/IOException
    //   12	16	238	java/io/IOException
    //   86	89	253	java/net/URISyntaxException
    //   90	94	253	java/net/URISyntaxException
    //   96	100	253	java/net/URISyntaxException
    //   102	106	253	java/net/URISyntaxException
    //   114	118	253	java/net/URISyntaxException
    //   118	122	253	java/net/URISyntaxException
    //   128	133	253	java/net/URISyntaxException
    //   146	150	253	java/net/URISyntaxException
    //   159	162	253	java/net/URISyntaxException
    //   163	167	253	java/net/URISyntaxException
    //   181	186	253	java/net/URISyntaxException
    //   189	194	253	java/net/URISyntaxException
    //   202	207	253	java/net/URISyntaxException
    //   219	224	253	java/net/URISyntaxException
    //   226	230	253	java/net/URISyntaxException
    //   231	235	253	java/net/URISyntaxException
  }
  
  public static String a(CharSequence paramCharSequence, Map paramMap)
  {
    char c1 = '=';
    Object localObject = paramCharSequence.toString();
    if (paramMap != null)
    {
      boolean bool1 = paramMap.isEmpty();
      if (!bool1) {
        break label29;
      }
    }
    for (;;)
    {
      return (String)localObject;
      label29:
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>((String)localObject);
      a((String)localObject, localStringBuilder);
      b((String)localObject, localStringBuilder);
      Iterator localIterator = paramMap.entrySet().iterator();
      localObject = (Map.Entry)localIterator.next();
      String str = ((Map.Entry)localObject).getKey().toString();
      localStringBuilder.append(str);
      localStringBuilder.append(c1);
      localObject = ((Map.Entry)localObject).getValue();
      if (localObject != null) {
        localStringBuilder.append(localObject);
      }
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        char c2 = '&';
        localStringBuilder.append(c2);
        localObject = (Map.Entry)localIterator.next();
        str = ((Map.Entry)localObject).getKey().toString();
        localStringBuilder.append(str);
        localStringBuilder.append(c1);
        localObject = ((Map.Entry)localObject).getValue();
        if (localObject != null) {
          localStringBuilder.append(localObject);
        }
      }
      localObject = localStringBuilder.toString();
    }
  }
  
  private static StringBuilder a(String paramString, StringBuilder paramStringBuilder)
  {
    char c1 = '/';
    int m = paramString.indexOf(':') + 2;
    int n = paramString.lastIndexOf(c1);
    if (m == n) {
      paramStringBuilder.append(c1);
    }
    return paramStringBuilder;
  }
  
  public static HttpRequest b(CharSequence paramCharSequence)
  {
    HttpRequest localHttpRequest = new io/fabric/sdk/android/services/network/HttpRequest;
    localHttpRequest.<init>(paramCharSequence, "GET");
    return localHttpRequest;
  }
  
  public static HttpRequest b(CharSequence paramCharSequence, Map paramMap, boolean paramBoolean)
  {
    String str = a(paramCharSequence, paramMap);
    if (paramBoolean) {
      str = a(str);
    }
    return c(str);
  }
  
  private static StringBuilder b(String paramString, StringBuilder paramStringBuilder)
  {
    char c1 = '?';
    int m = 38;
    int n = paramString.indexOf(c1);
    int i1 = paramStringBuilder.length() + -1;
    int i2 = -1;
    if (n == i2) {
      paramStringBuilder.append(c1);
    }
    for (;;)
    {
      return paramStringBuilder;
      if (n < i1)
      {
        n = paramString.charAt(i1);
        if (n != m) {
          paramStringBuilder.append(m);
        }
      }
    }
  }
  
  public static HttpRequest c(CharSequence paramCharSequence)
  {
    HttpRequest localHttpRequest = new io/fabric/sdk/android/services/network/HttpRequest;
    localHttpRequest.<init>(paramCharSequence, "POST");
    return localHttpRequest;
  }
  
  public static HttpRequest d(CharSequence paramCharSequence)
  {
    HttpRequest localHttpRequest = new io/fabric/sdk/android/services/network/HttpRequest;
    localHttpRequest.<init>(paramCharSequence, "PUT");
    return localHttpRequest;
  }
  
  public static HttpRequest e(CharSequence paramCharSequence)
  {
    HttpRequest localHttpRequest = new io/fabric/sdk/android/services/network/HttpRequest;
    localHttpRequest.<init>(paramCharSequence, "DELETE");
    return localHttpRequest;
  }
  
  private static String f(String paramString)
  {
    if (paramString != null)
    {
      int m = paramString.length();
      if (m <= 0) {}
    }
    for (;;)
    {
      return paramString;
      paramString = "UTF-8";
    }
  }
  
  private Proxy q()
  {
    Proxy localProxy = new java/net/Proxy;
    Proxy.Type localType = Proxy.Type.HTTP;
    InetSocketAddress localInetSocketAddress = new java/net/InetSocketAddress;
    String str = k;
    int m = l;
    localInetSocketAddress.<init>(str, m);
    localProxy.<init>(localType, localInetSocketAddress);
    return localProxy;
  }
  
  /* Error */
  private HttpURLConnection r()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 237	io/fabric/sdk/android/services/network/HttpRequest:k	Ljava/lang/String;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnull +38 -> 44
    //   9: getstatic 34	io/fabric/sdk/android/services/network/HttpRequest:c	Lio/fabric/sdk/android/services/network/HttpRequest$b;
    //   12: astore_1
    //   13: aload_0
    //   14: getfield 61	io/fabric/sdk/android/services/network/HttpRequest:a	Ljava/net/URL;
    //   17: astore_2
    //   18: aload_0
    //   19: invokespecial 249	io/fabric/sdk/android/services/network/HttpRequest:q	()Ljava/net/Proxy;
    //   22: astore_3
    //   23: aload_1
    //   24: aload_2
    //   25: aload_3
    //   26: invokeinterface 252 3 0
    //   31: astore_1
    //   32: aload_0
    //   33: getfield 63	io/fabric/sdk/android/services/network/HttpRequest:e	Ljava/lang/String;
    //   36: astore_2
    //   37: aload_1
    //   38: aload_2
    //   39: invokevirtual 257	java/net/HttpURLConnection:setRequestMethod	(Ljava/lang/String;)V
    //   42: aload_1
    //   43: areturn
    //   44: getstatic 34	io/fabric/sdk/android/services/network/HttpRequest:c	Lio/fabric/sdk/android/services/network/HttpRequest$b;
    //   47: astore_1
    //   48: aload_0
    //   49: getfield 61	io/fabric/sdk/android/services/network/HttpRequest:a	Ljava/net/URL;
    //   52: astore_2
    //   53: aload_1
    //   54: aload_2
    //   55: invokeinterface 260 2 0
    //   60: astore_1
    //   61: goto -29 -> 32
    //   64: astore_1
    //   65: new 65	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
    //   68: astore_2
    //   69: aload_2
    //   70: aload_1
    //   71: invokespecial 68	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
    //   74: aload_2
    //   75: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	76	0	this	HttpRequest
    //   4	57	1	localObject1	Object
    //   64	7	1	localIOException	IOException
    //   17	58	2	localObject2	Object
    //   22	4	3	localProxy	Proxy
    // Exception table:
    //   from	to	target	type
    //   0	4	64	java/io/IOException
    //   9	12	64	java/io/IOException
    //   13	17	64	java/io/IOException
    //   18	22	64	java/io/IOException
    //   25	31	64	java/io/IOException
    //   32	36	64	java/io/IOException
    //   38	42	64	java/io/IOException
    //   44	47	64	java/io/IOException
    //   48	52	64	java/io/IOException
    //   54	60	64	java/io/IOException
  }
  
  public int a(String paramString, int paramInt)
  {
    l();
    return a().getHeaderFieldInt(paramString, paramInt);
  }
  
  public HttpRequest a(int paramInt)
  {
    a().setConnectTimeout(paramInt);
    return this;
  }
  
  protected HttpRequest a(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    HttpRequest.1 local1 = new io/fabric/sdk/android/services/network/HttpRequest$1;
    boolean bool = h;
    local1.<init>(this, paramInputStream, bool, paramInputStream, paramOutputStream);
    return (HttpRequest)local1.call();
  }
  
  public HttpRequest a(String paramString, Number paramNumber)
  {
    return a(paramString, null, paramNumber);
  }
  
  public HttpRequest a(String paramString1, String paramString2)
  {
    a().setRequestProperty(paramString1, paramString2);
    return this;
  }
  
  public HttpRequest a(String paramString1, String paramString2, Number paramNumber)
  {
    if (paramNumber != null) {}
    for (String str = paramNumber.toString();; str = null) {
      return b(paramString1, paramString2, str);
    }
  }
  
  protected HttpRequest a(String paramString1, String paramString2, String paramString3)
  {
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = ((StringBuilder)localObject1).append("form-data; name=\"");
    ((StringBuilder)localObject2).append(paramString1);
    if (paramString2 != null)
    {
      localObject2 = ((StringBuilder)localObject1).append("\"; filename=\"");
      ((StringBuilder)localObject2).append(paramString2);
    }
    char c1 = '"';
    ((StringBuilder)localObject1).append(c1);
    localObject2 = "Content-Disposition";
    localObject1 = ((StringBuilder)localObject1).toString();
    f((String)localObject2, (String)localObject1);
    if (paramString3 != null)
    {
      localObject1 = "Content-Type";
      f((String)localObject1, paramString3);
    }
    return f("\r\n");
  }
  
  /* Error */
  public HttpRequest a(String paramString1, String paramString2, String paramString3, java.io.File paramFile)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: new 310	java/io/BufferedInputStream
    //   6: astore 6
    //   8: new 312	java/io/FileInputStream
    //   11: astore 7
    //   13: aload 7
    //   15: aload 4
    //   17: invokespecial 315	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   20: aload 6
    //   22: aload 7
    //   24: invokespecial 318	java/io/BufferedInputStream:<init>	(Ljava/io/InputStream;)V
    //   27: aload_0
    //   28: aload_1
    //   29: aload_2
    //   30: aload_3
    //   31: aload 6
    //   33: invokevirtual 321	io/fabric/sdk/android/services/network/HttpRequest:a	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)Lio/fabric/sdk/android/services/network/HttpRequest;
    //   36: astore 7
    //   38: aload 6
    //   40: ifnull +8 -> 48
    //   43: aload 6
    //   45: invokevirtual 326	java/io/InputStream:close	()V
    //   48: aload 7
    //   50: areturn
    //   51: astore 7
    //   53: aconst_null
    //   54: astore 6
    //   56: new 65	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException
    //   59: astore 5
    //   61: aload 5
    //   63: aload 7
    //   65: invokespecial 68	io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException:<init>	(Ljava/io/IOException;)V
    //   68: aload 5
    //   70: athrow
    //   71: astore 7
    //   73: aload 6
    //   75: ifnull +8 -> 83
    //   78: aload 6
    //   80: invokevirtual 326	java/io/InputStream:close	()V
    //   83: aload 7
    //   85: athrow
    //   86: astore 6
    //   88: goto -40 -> 48
    //   91: astore 6
    //   93: goto -10 -> 83
    //   96: astore 7
    //   98: aconst_null
    //   99: astore 6
    //   101: goto -28 -> 73
    //   104: astore 7
    //   106: goto -50 -> 56
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	109	0	this	HttpRequest
    //   0	109	1	paramString1	String
    //   0	109	2	paramString2	String
    //   0	109	3	paramString3	String
    //   0	109	4	paramFile	java.io.File
    //   1	68	5	localHttpRequestException	HttpRequest.HttpRequestException
    //   6	73	6	localBufferedInputStream	BufferedInputStream
    //   86	1	6	localIOException1	IOException
    //   91	1	6	localIOException2	IOException
    //   99	1	6	localObject1	Object
    //   11	38	7	localObject2	Object
    //   51	13	7	localIOException3	IOException
    //   71	13	7	localObject3	Object
    //   96	1	7	localObject4	Object
    //   104	1	7	localIOException4	IOException
    // Exception table:
    //   from	to	target	type
    //   3	6	51	java/io/IOException
    //   8	11	51	java/io/IOException
    //   15	20	51	java/io/IOException
    //   22	27	51	java/io/IOException
    //   31	36	71	finally
    //   56	59	71	finally
    //   63	68	71	finally
    //   68	71	71	finally
    //   43	48	86	java/io/IOException
    //   78	83	91	java/io/IOException
    //   3	6	96	finally
    //   8	11	96	finally
    //   15	20	96	finally
    //   22	27	96	finally
    //   31	36	104	java/io/IOException
  }
  
  public HttpRequest a(String paramString1, String paramString2, String paramString3, InputStream paramInputStream)
  {
    try
    {
      n();
      a(paramString1, paramString2, paramString3);
      HttpRequest.d locald = f;
      a(paramInputStream, locald);
      return this;
    }
    catch (IOException localIOException)
    {
      HttpRequest.HttpRequestException localHttpRequestException = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
      localHttpRequestException.<init>(localIOException);
      throw localHttpRequestException;
    }
  }
  
  public HttpRequest a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    try
    {
      n();
      a(paramString1, paramString2, paramString3);
      HttpRequest.d locald = f;
      locald.a(paramString4);
      return this;
    }
    catch (IOException localIOException)
    {
      HttpRequest.HttpRequestException localHttpRequestException = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
      localHttpRequestException.<init>(localIOException);
      throw localHttpRequestException;
    }
  }
  
  public HttpRequest a(Map.Entry paramEntry)
  {
    String str1 = (String)paramEntry.getKey();
    String str2 = (String)paramEntry.getValue();
    return a(str1, str2);
  }
  
  public HttpRequest a(boolean paramBoolean)
  {
    a().setUseCaches(paramBoolean);
    return this;
  }
  
  public String a(String paramString)
  {
    ByteArrayOutputStream localByteArrayOutputStream = d();
    try
    {
      localObject = f();
      a((InputStream)localObject, localByteArrayOutputStream);
      localObject = f(paramString);
      return localByteArrayOutputStream.toString((String)localObject);
    }
    catch (IOException localIOException)
    {
      Object localObject = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
      ((HttpRequest.HttpRequestException)localObject).<init>(localIOException);
      throw ((Throwable)localObject);
    }
  }
  
  public HttpURLConnection a()
  {
    HttpURLConnection localHttpURLConnection = d;
    if (localHttpURLConnection == null)
    {
      localHttpURLConnection = r();
      d = localHttpURLConnection;
    }
    return d;
  }
  
  public int b()
  {
    try
    {
      k();
      HttpURLConnection localHttpURLConnection = a();
      return localHttpURLConnection.getResponseCode();
    }
    catch (IOException localIOException)
    {
      HttpRequest.HttpRequestException localHttpRequestException = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
      localHttpRequestException.<init>(localIOException);
      throw localHttpRequestException;
    }
  }
  
  public HttpRequest b(String paramString1, String paramString2, String paramString3)
  {
    return a(paramString1, paramString2, null, paramString3);
  }
  
  public String b(String paramString)
  {
    l();
    return a().getHeaderField(paramString);
  }
  
  public String b(String paramString1, String paramString2)
  {
    String str = b(paramString1);
    return c(str, paramString2);
  }
  
  public int c(String paramString)
  {
    return a(paramString, -1);
  }
  
  protected String c(String paramString1, String paramString2)
  {
    int m = 34;
    int n = 0;
    int i1 = 59;
    int i2 = -1;
    if (paramString1 != null)
    {
      i3 = paramString1.length();
      if (i3 != 0) {}
    }
    else
    {
      i3 = 0;
    }
    int i5;
    int i6;
    for (String str = null;; str = null)
    {
      return str;
      i5 = paramString1.length();
      i3 = paramString1.indexOf(i1);
      i6 = i3 + 1;
      if ((i6 != 0) && (i6 != i5)) {
        break;
      }
      i3 = 0;
    }
    int i3 = paramString1.indexOf(i1, i6);
    if (i3 == i2)
    {
      i3 = i6;
      i6 = i5;
    }
    for (;;)
    {
      if (i3 < i6)
      {
        int i7 = paramString1.indexOf('=', i3);
        if ((i7 != i2) && (i7 < i6))
        {
          str = paramString1.substring(i3, i7).trim();
          boolean bool = paramString2.equals(str);
          if (bool)
          {
            i4 = i7 + 1;
            str = paramString1.substring(i4, i6).trim();
            i7 = str.length();
            if (i7 != 0)
            {
              i5 = 2;
              if (i7 <= i5) {
                break;
              }
              i5 = str.charAt(0);
              if (m != i5) {
                break;
              }
              i5 = i7 + -1;
              i5 = str.charAt(i5);
              if (m != i5) {
                break;
              }
              i5 = 1;
              n = i7 + -1;
              str = str.substring(i5, n);
              break;
            }
          }
        }
        i6 += 1;
        i4 = paramString1.indexOf(i1, i6);
        if (i4 == i2) {
          i4 = i5;
        }
        i8 = i4;
        i4 = i6;
        i6 = i8;
        continue;
      }
      int i4 = 0;
      str = null;
      break;
      int i8 = i4;
      i4 = i6;
      i6 = i8;
    }
  }
  
  public boolean c()
  {
    int m = 200;
    int i1 = b();
    if (m == i1) {}
    int n;
    for (m = 1;; n = 0) {
      return m;
    }
  }
  
  public HttpRequest d(String paramString)
  {
    return d(paramString, null);
  }
  
  public HttpRequest d(String paramString1, String paramString2)
  {
    Object localObject;
    if (paramString2 != null)
    {
      int m = paramString2.length();
      if (m > 0)
      {
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        localObject = ((StringBuilder)localObject).append(paramString1);
        String str = "; charset=";
        localObject = str + paramString2;
      }
    }
    for (HttpRequest localHttpRequest = a("Content-Type", (String)localObject);; localHttpRequest = a("Content-Type", paramString1)) {
      return localHttpRequest;
    }
  }
  
  protected ByteArrayOutputStream d()
  {
    int m = j();
    ByteArrayOutputStream localByteArrayOutputStream;
    if (m > 0)
    {
      localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      localByteArrayOutputStream.<init>(m);
    }
    for (;;)
    {
      return localByteArrayOutputStream;
      localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      localByteArrayOutputStream.<init>();
    }
  }
  
  public HttpRequest e(String paramString1, String paramString2)
  {
    return b(paramString1, null, paramString2);
  }
  
  public String e()
  {
    String str = h();
    return a(str);
  }
  
  public HttpRequest f(CharSequence paramCharSequence)
  {
    try
    {
      m();
      HttpRequest.d locald = f;
      localObject = paramCharSequence.toString();
      locald.a((String)localObject);
      return this;
    }
    catch (IOException localIOException)
    {
      Object localObject = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
      ((HttpRequest.HttpRequestException)localObject).<init>(localIOException);
      throw ((Throwable)localObject);
    }
  }
  
  public HttpRequest f(String paramString1, String paramString2)
  {
    return f(paramString1).f(": ").f(paramString2).f("\r\n");
  }
  
  public BufferedInputStream f()
  {
    BufferedInputStream localBufferedInputStream = new java/io/BufferedInputStream;
    InputStream localInputStream = g();
    int m = j;
    localBufferedInputStream.<init>(localInputStream, m);
    return localBufferedInputStream;
  }
  
  public InputStream g()
  {
    int m = b();
    int n = 400;
    if (m < n) {}
    for (;;)
    {
      Object localObject4;
      try
      {
        Object localObject1 = a();
        localObject1 = ((HttpURLConnection)localObject1).getInputStream();
        boolean bool = i;
        if (bool)
        {
          localObject4 = "gzip";
          String str = i();
          bool = ((String)localObject4).equals(str);
          if (bool) {
            break label113;
          }
        }
        return (InputStream)localObject1;
      }
      catch (IOException localIOException1)
      {
        localObject4 = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
        ((HttpRequest.HttpRequestException)localObject4).<init>(localIOException1);
        throw ((Throwable)localObject4);
      }
      Object localObject2 = a().getErrorStream();
      if (localObject2 != null) {
        continue;
      }
      try
      {
        localObject2 = a();
        localObject2 = ((HttpURLConnection)localObject2).getInputStream();
      }
      catch (IOException localIOException2)
      {
        localObject4 = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
        ((HttpRequest.HttpRequestException)localObject4).<init>(localIOException2);
        throw ((Throwable)localObject4);
      }
      try
      {
        label113:
        localObject4 = new java/util/zip/GZIPInputStream;
        ((GZIPInputStream)localObject4).<init>(localIOException2);
        Object localObject3 = localObject4;
      }
      catch (IOException localIOException3)
      {
        localObject4 = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
        ((HttpRequest.HttpRequestException)localObject4).<init>(localIOException3);
        throw ((Throwable)localObject4);
      }
    }
  }
  
  public String h()
  {
    return b("Content-Type", "charset");
  }
  
  public String i()
  {
    return b("Content-Encoding");
  }
  
  public int j()
  {
    return c("Content-Length");
  }
  
  protected HttpRequest k()
  {
    HttpRequest.d locald = f;
    if (locald == null) {
      return this;
    }
    boolean bool = g;
    if (bool)
    {
      locald = f;
      String str = "\r\n--00content0boundary00--\r\n";
      locald.a(str);
    }
    bool = h;
    if (bool) {}
    for (;;)
    {
      try
      {
        locald = f;
        locald.close();
      }
      catch (IOException localIOException)
      {
        continue;
      }
      bool = false;
      locald = null;
      f = null;
      break;
      locald = f;
      locald.close();
    }
  }
  
  protected HttpRequest l()
  {
    try
    {
      return k();
    }
    catch (IOException localIOException)
    {
      HttpRequest.HttpRequestException localHttpRequestException = new io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException;
      localHttpRequestException.<init>(localIOException);
      throw localHttpRequestException;
    }
  }
  
  protected HttpRequest m()
  {
    Object localObject = f;
    if (localObject != null) {}
    for (;;)
    {
      return this;
      localObject = a();
      boolean bool = true;
      ((HttpURLConnection)localObject).setDoOutput(bool);
      localObject = a().getRequestProperty("Content-Type");
      localObject = c((String)localObject, "charset");
      HttpRequest.d locald = new io/fabric/sdk/android/services/network/HttpRequest$d;
      OutputStream localOutputStream = a().getOutputStream();
      int m = j;
      locald.<init>(localOutputStream, (String)localObject, m);
      f = locald;
    }
  }
  
  protected HttpRequest n()
  {
    boolean bool = g;
    HttpRequest.d locald;
    String str;
    if (!bool)
    {
      bool = true;
      g = bool;
      d("multipart/form-data; boundary=00content0boundary00").m();
      locald = f;
      str = "--00content0boundary00\r\n";
      locald.a(str);
    }
    for (;;)
    {
      return this;
      locald = f;
      str = "\r\n--00content0boundary00\r\n";
      locald.a(str);
    }
  }
  
  public URL o()
  {
    return a().getURL();
  }
  
  public String p()
  {
    return a().getRequestMethod();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    Object localObject = p();
    localStringBuilder = localStringBuilder.append((String)localObject).append(' ');
    localObject = o();
    return (String)localObject;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/HttpRequest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */