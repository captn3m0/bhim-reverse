package io.fabric.sdk.android.services.network;

import io.fabric.sdk.android.k;
import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;

public class b
  implements d
{
  private final k a;
  private f b;
  private SSLSocketFactory c;
  private boolean d;
  
  public b()
  {
    this(localb);
  }
  
  public b(k paramk)
  {
    a = paramk;
  }
  
  private void a()
  {
    Object localObject1 = null;
    try
    {
      d = false;
      localObject1 = null;
      c = null;
      return;
    }
    finally {}
  }
  
  private boolean a(String paramString)
  {
    Object localObject;
    boolean bool;
    if (paramString != null)
    {
      localObject = Locale.US;
      localObject = paramString.toLowerCase((Locale)localObject);
      String str = "https";
      bool = ((String)localObject).startsWith(str);
      if (bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  private SSLSocketFactory b()
  {
    try
    {
      SSLSocketFactory localSSLSocketFactory = c;
      if (localSSLSocketFactory == null)
      {
        boolean bool = d;
        if (!bool)
        {
          localSSLSocketFactory = c();
          c = localSSLSocketFactory;
        }
      }
      localSSLSocketFactory = c;
      return localSSLSocketFactory;
    }
    finally {}
  }
  
  private SSLSocketFactory c()
  {
    boolean bool = true;
    try
    {
      d = bool;
      try
      {
        localObject1 = b;
        localObject1 = e.a((f)localObject1);
        localk = a;
        str1 = "Fabric";
        str2 = "Custom SSL pinning enabled";
        localk.a(str1, str2);
      }
      catch (Exception localException)
      {
        for (;;)
        {
          Object localObject1;
          k localk = a;
          String str1 = "Fabric";
          String str2 = "Exception while validating pinned certs";
          localk.e(str1, str2, localException);
          bool = false;
          Object localObject2 = null;
        }
      }
      return (SSLSocketFactory)localObject1;
    }
    finally {}
  }
  
  public HttpRequest a(c paramc, String paramString)
  {
    Map localMap = Collections.emptyMap();
    return a(paramc, paramString, localMap);
  }
  
  public HttpRequest a(c paramc, String paramString, Map paramMap)
  {
    boolean bool1 = true;
    Object localObject1 = b.1.a;
    int i = paramc.ordinal();
    int j = localObject1[i];
    Object localObject2;
    switch (j)
    {
    default: 
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("Unsupported HTTP method!");
      throw ((Throwable)localObject1);
    case 1: 
      localObject1 = HttpRequest.a(paramString, paramMap, bool1);
      localObject2 = localObject1;
    }
    for (;;)
    {
      boolean bool2 = a(paramString);
      if (bool2)
      {
        localObject1 = b;
        if (localObject1 != null)
        {
          SSLSocketFactory localSSLSocketFactory = b();
          if (localSSLSocketFactory != null)
          {
            localObject1 = (HttpsURLConnection)((HttpRequest)localObject2).a();
            ((HttpsURLConnection)localObject1).setSSLSocketFactory(localSSLSocketFactory);
          }
        }
      }
      return (HttpRequest)localObject2;
      localObject1 = HttpRequest.b(paramString, paramMap, bool1);
      localObject2 = localObject1;
      continue;
      localObject1 = HttpRequest.d(paramString);
      localObject2 = localObject1;
      continue;
      localObject1 = HttpRequest.e(paramString);
      localObject2 = localObject1;
    }
  }
  
  public void a(f paramf)
  {
    f localf = b;
    if (localf != paramf)
    {
      b = paramf;
      a();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */