package io.fabric.sdk.android.services.network;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

class g
  implements X509TrustManager
{
  private static final X509Certificate[] a = new X509Certificate[0];
  private final TrustManager[] b;
  private final h c;
  private final long d;
  private final List e;
  private final Set f;
  
  public g(h paramh, f paramf)
  {
    Object localObject1 = new java/util/LinkedList;
    ((LinkedList)localObject1).<init>();
    e = ((List)localObject1);
    localObject1 = new java/util/HashSet;
    ((HashSet)localObject1).<init>();
    localObject1 = Collections.synchronizedSet((Set)localObject1);
    f = ((Set)localObject1);
    localObject1 = a(paramh);
    b = ((TrustManager[])localObject1);
    c = paramh;
    long l = paramf.getPinCreationTimeInMillis();
    d = l;
    String[] arrayOfString = paramf.getPins();
    int i = arrayOfString.length;
    int j = 0;
    localObject1 = null;
    while (j < i)
    {
      Object localObject2 = arrayOfString[j];
      List localList = e;
      localObject2 = a((String)localObject2);
      localList.add(localObject2);
      j += 1;
    }
  }
  
  private void a(X509Certificate[] paramArrayOfX509Certificate)
  {
    long l1 = 15552000000L;
    long l2 = d;
    long l3 = -1;
    int i = l2 < l3;
    String str;
    if (i != 0)
    {
      l2 = System.currentTimeMillis();
      l3 = d;
      l2 -= l3;
      i = l2 < l1;
      if (i > 0)
      {
        localObject1 = c.h();
        localObject2 = "Fabric";
        Object localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = ((StringBuilder)localObject3).append("Certificate pins are stale, (");
        long l4 = System.currentTimeMillis();
        long l5 = d;
        l4 -= l5;
        localObject3 = ((StringBuilder)localObject3).append(l4).append(" millis vs ").append(l1).append(" millis) ");
        str = "falling back to system trust.";
        localObject3 = str;
        ((k)localObject1).d((String)localObject2, (String)localObject3);
        return;
      }
    }
    Object localObject1 = c;
    Object localObject2 = a.a(paramArrayOfX509Certificate, (h)localObject1);
    int k = localObject2.length;
    i = 0;
    localObject1 = null;
    for (;;)
    {
      if (i >= k) {
        break label213;
      }
      str = localObject2[i];
      boolean bool = a(str);
      if (bool) {
        break;
      }
      int j;
      i += 1;
    }
    label213:
    localObject1 = new java/security/cert/CertificateException;
    ((CertificateException)localObject1).<init>("No valid pins found in chain!");
    throw ((Throwable)localObject1);
  }
  
  private void a(X509Certificate[] paramArrayOfX509Certificate, String paramString)
  {
    TrustManager[] arrayOfTrustManager = b;
    int i = arrayOfTrustManager.length;
    int j = 0;
    X509TrustManager localX509TrustManager = null;
    for (int k = 0; k < i; k = j)
    {
      localX509TrustManager = (X509TrustManager)arrayOfTrustManager[k];
      localX509TrustManager.checkServerTrusted(paramArrayOfX509Certificate, paramString);
      j = k + 1;
    }
  }
  
  /* Error */
  private boolean a(X509Certificate paramX509Certificate)
  {
    // Byte code:
    //   0: ldc -121
    //   2: astore_2
    //   3: aload_2
    //   4: invokestatic 141	java/security/MessageDigest:getInstance	(Ljava/lang/String;)Ljava/security/MessageDigest;
    //   7: astore_2
    //   8: aload_1
    //   9: invokevirtual 145	java/security/cert/X509Certificate:getPublicKey	()Ljava/security/PublicKey;
    //   12: astore_3
    //   13: aload_3
    //   14: invokeinterface 151 1 0
    //   19: astore_3
    //   20: aload_2
    //   21: aload_3
    //   22: invokevirtual 155	java/security/MessageDigest:digest	([B)[B
    //   25: astore_3
    //   26: aload_0
    //   27: getfield 31	io/fabric/sdk/android/services/network/g:e	Ljava/util/List;
    //   30: astore_2
    //   31: aload_2
    //   32: invokeinterface 159 1 0
    //   37: astore 4
    //   39: aload 4
    //   41: invokeinterface 165 1 0
    //   46: istore 5
    //   48: iload 5
    //   50: ifeq +34 -> 84
    //   53: aload 4
    //   55: invokeinterface 169 1 0
    //   60: astore_2
    //   61: aload_2
    //   62: checkcast 171	[B
    //   65: astore_2
    //   66: aload_2
    //   67: aload_3
    //   68: invokestatic 177	java/util/Arrays:equals	([B[B)Z
    //   71: istore 5
    //   73: iload 5
    //   75: ifeq -36 -> 39
    //   78: iconst_1
    //   79: istore 5
    //   81: iload 5
    //   83: ireturn
    //   84: iconst_0
    //   85: istore 5
    //   87: aconst_null
    //   88: astore_2
    //   89: goto -8 -> 81
    //   92: astore_2
    //   93: new 124	java/security/cert/CertificateException
    //   96: astore_3
    //   97: aload_3
    //   98: aload_2
    //   99: invokespecial 181	java/security/cert/CertificateException:<init>	(Ljava/lang/Throwable;)V
    //   102: aload_3
    //   103: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	104	0	this	g
    //   0	104	1	paramX509Certificate	X509Certificate
    //   2	87	2	localObject1	Object
    //   92	7	2	localNoSuchAlgorithmException	NoSuchAlgorithmException
    //   12	91	3	localObject2	Object
    //   37	17	4	localIterator	java.util.Iterator
    //   46	40	5	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   3	7	92	java/security/NoSuchAlgorithmException
    //   8	12	92	java/security/NoSuchAlgorithmException
    //   13	19	92	java/security/NoSuchAlgorithmException
    //   21	25	92	java/security/NoSuchAlgorithmException
    //   26	30	92	java/security/NoSuchAlgorithmException
    //   31	37	92	java/security/NoSuchAlgorithmException
    //   39	46	92	java/security/NoSuchAlgorithmException
    //   53	60	92	java/security/NoSuchAlgorithmException
    //   61	65	92	java/security/NoSuchAlgorithmException
    //   67	71	92	java/security/NoSuchAlgorithmException
  }
  
  private byte[] a(String paramString)
  {
    int i = 16;
    int j = paramString.length();
    byte[] arrayOfByte = new byte[j / 2];
    int k = 0;
    while (k < j)
    {
      int m = k / 2;
      int n = Character.digit(paramString.charAt(k), i) << 4;
      int i1 = k + 1;
      i1 = Character.digit(paramString.charAt(i1), i);
      n = (byte)(n + i1);
      arrayOfByte[m] = n;
      k += 2;
    }
    return arrayOfByte;
  }
  
  private TrustManager[] a(h paramh)
  {
    Object localObject1 = "X509";
    try
    {
      localObject1 = TrustManagerFactory.getInstance((String)localObject1);
      localObject2 = a;
      ((TrustManagerFactory)localObject1).init((KeyStore)localObject2);
      return ((TrustManagerFactory)localObject1).getTrustManagers();
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      localObject2 = new java/lang/AssertionError;
      ((AssertionError)localObject2).<init>(localNoSuchAlgorithmException);
      throw ((Throwable)localObject2);
    }
    catch (KeyStoreException localKeyStoreException)
    {
      Object localObject2 = new java/lang/AssertionError;
      ((AssertionError)localObject2).<init>(localKeyStoreException);
      throw ((Throwable)localObject2);
    }
  }
  
  public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
  {
    CertificateException localCertificateException = new java/security/cert/CertificateException;
    localCertificateException.<init>("Client certificates not supported!");
    throw localCertificateException;
  }
  
  public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString)
  {
    Set localSet = f;
    X509Certificate localX509Certificate = paramArrayOfX509Certificate[0];
    boolean bool = localSet.contains(localX509Certificate);
    if (bool) {}
    for (;;)
    {
      return;
      a(paramArrayOfX509Certificate, paramString);
      a(paramArrayOfX509Certificate);
      localSet = f;
      localX509Certificate = paramArrayOfX509Certificate[0];
      localSet.add(localX509Certificate);
    }
  }
  
  public X509Certificate[] getAcceptedIssuers()
  {
    return a;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */