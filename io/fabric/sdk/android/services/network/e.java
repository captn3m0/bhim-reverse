package io.fabric.sdk.android.services.network;

import java.io.InputStream;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

public final class e
{
  public static final SSLSocketFactory a(f paramf)
  {
    SSLContext localSSLContext = SSLContext.getInstance("TLS");
    Object localObject1 = new io/fabric/sdk/android/services/network/h;
    Object localObject2 = paramf.getKeyStoreStream();
    String str = paramf.getKeyStorePassword();
    ((h)localObject1).<init>((InputStream)localObject2, str);
    localObject2 = new io/fabric/sdk/android/services/network/g;
    ((g)localObject2).<init>((h)localObject1, paramf);
    localObject1 = new TrustManager[1];
    localObject1[0] = localObject2;
    localSSLContext.init(null, (TrustManager[])localObject1, null);
    return localSSLContext.getSocketFactory();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */