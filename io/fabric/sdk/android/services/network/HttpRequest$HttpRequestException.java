package io.fabric.sdk.android.services.network;

import java.io.IOException;

public class HttpRequest$HttpRequestException
  extends RuntimeException
{
  protected HttpRequest$HttpRequestException(IOException paramIOException)
  {
    super(paramIOException);
  }
  
  public IOException a()
  {
    return (IOException)super.getCause();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/HttpRequest$HttpRequestException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */