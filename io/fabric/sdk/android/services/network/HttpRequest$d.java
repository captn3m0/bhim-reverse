package io.fabric.sdk.android.services.network;

import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;

public class HttpRequest$d
  extends BufferedOutputStream
{
  private final CharsetEncoder a;
  
  public HttpRequest$d(OutputStream paramOutputStream, String paramString, int paramInt)
  {
    super(paramOutputStream, paramInt);
    CharsetEncoder localCharsetEncoder = Charset.forName(HttpRequest.e(paramString)).newEncoder();
    a = localCharsetEncoder;
  }
  
  public d a(String paramString)
  {
    Object localObject1 = a;
    Object localObject2 = CharBuffer.wrap(paramString);
    localObject1 = ((CharsetEncoder)localObject1).encode((CharBuffer)localObject2);
    localObject2 = ((ByteBuffer)localObject1).array();
    int i = ((ByteBuffer)localObject1).limit();
    super.write((byte[])localObject2, 0, i);
    return this;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/services/network/HttpRequest$d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */