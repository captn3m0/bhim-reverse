package io.fabric.sdk.android;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.concurrency.d;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;

public class c
{
  static volatile c a;
  static final k b;
  final k c;
  final boolean d;
  private final Context e;
  private final Map f;
  private final ExecutorService g;
  private final Handler h;
  private final f i;
  private final f j;
  private final o k;
  private a l;
  private WeakReference m;
  private AtomicBoolean n;
  
  static
  {
    b localb = new io/fabric/sdk/android/b;
    localb.<init>();
    b = localb;
  }
  
  c(Context paramContext, Map paramMap, io.fabric.sdk.android.services.concurrency.k paramk, Handler paramHandler, k paramk1, boolean paramBoolean, f paramf, o paramo)
  {
    Object localObject = paramContext.getApplicationContext();
    e = ((Context)localObject);
    f = paramMap;
    g = paramk;
    h = paramHandler;
    c = paramk1;
    d = paramBoolean;
    i = paramf;
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>(false);
    n = ((AtomicBoolean)localObject);
    int i1 = paramMap.size();
    localObject = a(i1);
    j = ((f)localObject);
    k = paramo;
    localObject = c(paramContext);
    a((Activity)localObject);
  }
  
  static c a()
  {
    Object localObject = a;
    if (localObject == null)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Must Initialize Fabric before using singleton()");
      throw ((Throwable)localObject);
    }
    return a;
  }
  
  public static c a(Context paramContext, h... paramVarArgs)
  {
    Object localObject1 = a;
    if (localObject1 == null) {}
    synchronized (c.class)
    {
      localObject1 = a;
      if (localObject1 == null)
      {
        localObject1 = new io/fabric/sdk/android/c$a;
        ((c.a)localObject1).<init>(paramContext);
        localObject1 = ((c.a)localObject1).a(paramVarArgs);
        localObject1 = ((c.a)localObject1).a();
        c((c)localObject1);
      }
      return a;
    }
  }
  
  public static h a(Class paramClass)
  {
    return (h)af.get(paramClass);
  }
  
  private static void a(Map paramMap, Collection paramCollection)
  {
    Iterator localIterator = paramCollection.iterator();
    for (;;)
    {
      boolean bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      Object localObject = (h)localIterator.next();
      Class localClass = localObject.getClass();
      paramMap.put(localClass, localObject);
      boolean bool2 = localObject instanceof i;
      if (bool2)
      {
        localObject = ((i)localObject).getKits();
        a(paramMap, (Collection)localObject);
      }
    }
  }
  
  private static Map b(Collection paramCollection)
  {
    HashMap localHashMap = new java/util/HashMap;
    int i1 = paramCollection.size();
    localHashMap.<init>(i1);
    a(localHashMap, paramCollection);
    return localHashMap;
  }
  
  private Activity c(Context paramContext)
  {
    boolean bool = paramContext instanceof Activity;
    if (bool) {}
    for (paramContext = (Activity)paramContext;; paramContext = null) {
      return paramContext;
    }
  }
  
  private static void c(c paramc)
  {
    a = paramc;
    paramc.j();
  }
  
  public static k h()
  {
    Object localObject = a;
    if (localObject == null) {}
    for (localObject = b;; localObject = ac) {
      return (k)localObject;
    }
  }
  
  public static boolean i()
  {
    c localc = a;
    boolean bool;
    if (localc == null)
    {
      bool = false;
      localc = null;
    }
    for (;;)
    {
      return bool;
      localc = a;
      bool = d;
    }
  }
  
  private void j()
  {
    Object localObject1 = new io/fabric/sdk/android/a;
    Object localObject2 = e;
    ((a)localObject1).<init>((Context)localObject2);
    l = ((a)localObject1);
    localObject1 = l;
    localObject2 = new io/fabric/sdk/android/c$1;
    ((c.1)localObject2).<init>(this);
    ((a)localObject1).a((a.b)localObject2);
    localObject1 = e;
    a((Context)localObject1);
  }
  
  public c a(Activity paramActivity)
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(paramActivity);
    m = localWeakReference;
    return this;
  }
  
  f a(int paramInt)
  {
    c.2 local2 = new io/fabric/sdk/android/c$2;
    local2.<init>(this, paramInt);
    return local2;
  }
  
  void a(Context paramContext)
  {
    Object localObject1 = b(paramContext);
    Object localObject2 = g();
    Object localObject3 = new io/fabric/sdk/android/l;
    ((l)localObject3).<init>((Future)localObject1, (Collection)localObject2);
    Object localObject4 = new java/util/ArrayList;
    ((ArrayList)localObject4).<init>((Collection)localObject2);
    Collections.sort((List)localObject4);
    localObject1 = f.d;
    localObject2 = k;
    ((l)localObject3).injectParameters(paramContext, this, (f)localObject1, (o)localObject2);
    localObject2 = ((List)localObject4).iterator();
    Object localObject5;
    Object localObject6;
    for (;;)
    {
      bool = ((Iterator)localObject2).hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (h)((Iterator)localObject2).next();
      localObject5 = j;
      localObject6 = k;
      ((h)localObject1).injectParameters(paramContext, this, (f)localObject5, (o)localObject6);
    }
    ((l)localObject3).initialize();
    localObject1 = h();
    localObject2 = "Fabric";
    int i1 = 3;
    boolean bool = ((k)localObject1).a((String)localObject2, i1);
    if (bool)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>("Initializing ");
      localObject2 = d();
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(" [Version: ");
      localObject2 = c();
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append("], with the following kits:\n");
    }
    for (localObject2 = localObject1;; localObject2 = null)
    {
      localObject4 = ((List)localObject4).iterator();
      for (;;)
      {
        bool = ((Iterator)localObject4).hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (h)((Iterator)localObject4).next();
        localObject5 = initializationTask;
        localObject6 = initializationTask;
        ((g)localObject5).a((io.fabric.sdk.android.services.concurrency.l)localObject6);
        localObject5 = f;
        a((Map)localObject5, (h)localObject1);
        ((h)localObject1).initialize();
        if (localObject2 != null)
        {
          localObject5 = ((h)localObject1).getIdentifier();
          localObject5 = ((StringBuilder)localObject2).append((String)localObject5);
          localObject6 = " [Version: ";
          localObject5 = ((StringBuilder)localObject5).append((String)localObject6);
          localObject1 = ((h)localObject1).getVersion();
          localObject1 = ((StringBuilder)localObject5).append((String)localObject1);
          localObject5 = "]\n";
          ((StringBuilder)localObject1).append((String)localObject5);
        }
      }
      bool = false;
      localObject1 = null;
    }
    if (localObject2 != null)
    {
      localObject1 = h();
      localObject3 = "Fabric";
      localObject2 = ((StringBuilder)localObject2).toString();
      ((k)localObject1).a((String)localObject3, (String)localObject2);
    }
  }
  
  void a(Map paramMap, h paramh)
  {
    Object localObject1 = dependsOnAnnotation;
    if (localObject1 != null)
    {
      Class[] arrayOfClass = ((d)localObject1).a();
      int i1 = arrayOfClass.length;
      boolean bool1 = false;
      localObject1 = null;
      int i2;
      for (int i3 = 0; i3 < i1; i3 = i2)
      {
        Class localClass = arrayOfClass[i3];
        bool1 = localClass.isInterface();
        if (bool1)
        {
          localObject1 = paramMap.values();
          localObject2 = ((Collection)localObject1).iterator();
          for (;;)
          {
            bool1 = ((Iterator)localObject2).hasNext();
            if (!bool1) {
              break;
            }
            localObject1 = (h)((Iterator)localObject2).next();
            Object localObject3 = localObject1.getClass();
            boolean bool2 = localClass.isAssignableFrom((Class)localObject3);
            if (bool2)
            {
              localObject3 = initializationTask;
              localObject1 = initializationTask;
              ((g)localObject3).a((io.fabric.sdk.android.services.concurrency.l)localObject1);
            }
          }
        }
        localObject1 = (h)paramMap.get(localClass);
        if (localObject1 == null)
        {
          localObject1 = new io/fabric/sdk/android/services/concurrency/UnmetDependencyException;
          ((UnmetDependencyException)localObject1).<init>("Referenced Kit was null, does the kit exist?");
          throw ((Throwable)localObject1);
        }
        Object localObject2 = initializationTask;
        localObject1 = getinitializationTask;
        ((g)localObject2).a((io.fabric.sdk.android.services.concurrency.l)localObject1);
        i2 = i3 + 1;
      }
    }
  }
  
  public Activity b()
  {
    Object localObject = m;
    if (localObject != null) {}
    for (localObject = (Activity)m.get();; localObject = null) {
      return (Activity)localObject;
    }
  }
  
  Future b(Context paramContext)
  {
    e locale = new io/fabric/sdk/android/e;
    String str = paramContext.getPackageCodePath();
    locale.<init>(str);
    return f().submit(locale);
  }
  
  public String c()
  {
    return "1.3.14.143";
  }
  
  public String d()
  {
    return "io.fabric.sdk.android:fabric";
  }
  
  public a e()
  {
    return l;
  }
  
  public ExecutorService f()
  {
    return g;
  }
  
  public Collection g()
  {
    return f.values();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */