package io.fabric.sdk.android;

import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

class a$a
{
  private final Set a;
  private final Application b;
  
  a$a(Application paramApplication)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    a = localHashSet;
    b = paramApplication;
  }
  
  private void a()
  {
    Object localObject = a;
    Iterator localIterator = ((Set)localObject).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject = (Application.ActivityLifecycleCallbacks)localIterator.next();
      Application localApplication = b;
      localApplication.unregisterActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject);
    }
  }
  
  private boolean a(a.b paramb)
  {
    Object localObject = b;
    boolean bool;
    if (localObject != null)
    {
      localObject = new io/fabric/sdk/android/a$a$1;
      ((a.a.1)localObject).<init>(this, paramb);
      b.registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject);
      Set localSet = a;
      localSet.add(localObject);
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/a$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */