package io.fabric.sdk.android;

import android.content.Context;
import io.fabric.sdk.android.services.b.o;
import java.io.File;
import java.util.Collection;
import java.util.concurrent.ExecutorService;

public abstract class h
  implements Comparable
{
  Context context;
  final io.fabric.sdk.android.services.concurrency.d dependsOnAnnotation;
  c fabric;
  o idManager;
  f initializationCallback;
  g initializationTask;
  
  public h()
  {
    Object localObject = new io/fabric/sdk/android/g;
    ((g)localObject).<init>(this);
    initializationTask = ((g)localObject);
    localObject = (io.fabric.sdk.android.services.concurrency.d)getClass().getAnnotation(io.fabric.sdk.android.services.concurrency.d.class);
    dependsOnAnnotation = ((io.fabric.sdk.android.services.concurrency.d)localObject);
  }
  
  public int compareTo(h paramh)
  {
    int i = 1;
    int j = -1;
    boolean bool2 = containsAnnotatedDependency(paramh);
    if (bool2) {}
    for (;;)
    {
      return i;
      bool2 = paramh.containsAnnotatedDependency(this);
      if (bool2)
      {
        i = j;
      }
      else
      {
        bool2 = hasAnnotatedDependency();
        if (bool2)
        {
          bool2 = paramh.hasAnnotatedDependency();
          if (!bool2) {}
        }
        else
        {
          boolean bool1 = hasAnnotatedDependency();
          if (!bool1)
          {
            bool1 = paramh.hasAnnotatedDependency();
            if (bool1)
            {
              bool1 = j;
              continue;
            }
          }
          bool1 = false;
        }
      }
    }
  }
  
  boolean containsAnnotatedDependency(h paramh)
  {
    boolean bool1 = false;
    int i = hasAnnotatedDependency();
    Class[] arrayOfClass;
    int k;
    if (i != 0)
    {
      arrayOfClass = dependsOnAnnotation.a();
      k = arrayOfClass.length;
      i = 0;
    }
    for (;;)
    {
      if (i < k)
      {
        Class localClass1 = arrayOfClass[i];
        Class localClass2 = paramh.getClass();
        boolean bool2 = localClass1.isAssignableFrom(localClass2);
        if (bool2) {
          bool1 = true;
        }
      }
      else
      {
        return bool1;
      }
      int j;
      i += 1;
    }
  }
  
  protected abstract Object doInBackground();
  
  public Context getContext()
  {
    return context;
  }
  
  protected Collection getDependencies()
  {
    return initializationTask.getDependencies();
  }
  
  public c getFabric()
  {
    return fabric;
  }
  
  protected o getIdManager()
  {
    return idManager;
  }
  
  public abstract String getIdentifier();
  
  public String getPath()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append(".Fabric");
    String str = File.separator;
    localStringBuilder = localStringBuilder.append(str);
    str = getIdentifier();
    return str;
  }
  
  public abstract String getVersion();
  
  boolean hasAnnotatedDependency()
  {
    io.fabric.sdk.android.services.concurrency.d locald = dependsOnAnnotation;
    boolean bool;
    if (locald != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      locald = null;
    }
  }
  
  final void initialize()
  {
    g localg = initializationTask;
    ExecutorService localExecutorService = fabric.f();
    Void[] arrayOfVoid = new Void[1];
    ((Void)null);
    arrayOfVoid[0] = null;
    localg.a(localExecutorService, arrayOfVoid);
  }
  
  void injectParameters(Context paramContext, c paramc, f paramf, o paramo)
  {
    fabric = paramc;
    d locald = new io/fabric/sdk/android/d;
    String str1 = getIdentifier();
    String str2 = getPath();
    locald.<init>(paramContext, str1, str2);
    context = locald;
    initializationCallback = paramf;
    idManager = paramo;
  }
  
  protected void onCancelled(Object paramObject) {}
  
  protected void onPostExecute(Object paramObject) {}
  
  protected boolean onPreExecute()
  {
    return true;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */