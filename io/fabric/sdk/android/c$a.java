package io.fabric.sdk.android;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import io.fabric.sdk.android.services.b.o;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class c$a
{
  private final Context a;
  private h[] b;
  private io.fabric.sdk.android.services.concurrency.k c;
  private Handler d;
  private k e;
  private boolean f;
  private String g;
  private String h;
  private f i;
  
  public c$a(Context paramContext)
  {
    if (paramContext == null)
    {
      IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localIllegalArgumentException.<init>("Context must not be null.");
      throw localIllegalArgumentException;
    }
    a = paramContext;
  }
  
  public a a(h... paramVarArgs)
  {
    Object localObject = b;
    if (localObject != null)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Kits already set.");
      throw ((Throwable)localObject);
    }
    b = paramVarArgs;
    return this;
  }
  
  public c a()
  {
    Object localObject1 = c;
    if (localObject1 == null)
    {
      localObject1 = io.fabric.sdk.android.services.concurrency.k.a();
      c = ((io.fabric.sdk.android.services.concurrency.k)localObject1);
    }
    localObject1 = d;
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = new android/os/Handler;
      localObject2 = Looper.getMainLooper();
      ((Handler)localObject1).<init>((Looper)localObject2);
      d = ((Handler)localObject1);
    }
    localObject1 = e;
    Object localObject3;
    if (localObject1 == null)
    {
      boolean bool1 = f;
      if (bool1)
      {
        localObject1 = new io/fabric/sdk/android/b;
        int j = 3;
        ((b)localObject1).<init>(j);
        e = ((k)localObject1);
      }
    }
    else
    {
      localObject1 = h;
      if (localObject1 == null)
      {
        localObject1 = a.getPackageName();
        h = ((String)localObject1);
      }
      localObject1 = i;
      if (localObject1 == null)
      {
        localObject1 = f.d;
        i = ((f)localObject1);
      }
      localObject1 = b;
      if (localObject1 != null) {
        break label257;
      }
      localObject3 = new java/util/HashMap;
      ((HashMap)localObject3).<init>();
    }
    for (;;)
    {
      o localo = new io/fabric/sdk/android/services/b/o;
      localObject1 = a;
      localObject2 = h;
      Object localObject4 = g;
      Object localObject5 = ((Map)localObject3).values();
      localo.<init>((Context)localObject1, (String)localObject2, (String)localObject4, (Collection)localObject5);
      localObject1 = new io/fabric/sdk/android/c;
      localObject2 = a;
      localObject4 = c;
      localObject5 = d;
      k localk = e;
      boolean bool2 = f;
      f localf = i;
      ((c)localObject1).<init>((Context)localObject2, (Map)localObject3, (io.fabric.sdk.android.services.concurrency.k)localObject4, (Handler)localObject5, localk, bool2, localf, localo);
      return (c)localObject1;
      localObject1 = new io/fabric/sdk/android/b;
      ((b)localObject1).<init>();
      e = ((k)localObject1);
      break;
      label257:
      localObject1 = Arrays.asList(b);
      localObject3 = c.a((Collection)localObject1);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/c$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */