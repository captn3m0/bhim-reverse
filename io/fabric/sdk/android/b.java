package io.fabric.sdk.android;

import android.util.Log;

public class b
  implements k
{
  private int a;
  
  public b()
  {
    a = 4;
  }
  
  public b(int paramInt)
  {
    a = paramInt;
  }
  
  public void a(int paramInt, String paramString1, String paramString2)
  {
    a(paramInt, paramString1, paramString2, false);
  }
  
  public void a(int paramInt, String paramString1, String paramString2, boolean paramBoolean)
  {
    if (!paramBoolean)
    {
      boolean bool = a(paramString1, paramInt);
      if (!bool) {}
    }
    else
    {
      Log.println(paramInt, paramString1, paramString2);
    }
  }
  
  public void a(String paramString1, String paramString2)
  {
    a(paramString1, paramString2, null);
  }
  
  public void a(String paramString1, String paramString2, Throwable paramThrowable)
  {
    boolean bool = a(paramString1, 3);
    if (bool) {
      Log.d(paramString1, paramString2, paramThrowable);
    }
  }
  
  public boolean a(String paramString, int paramInt)
  {
    int i = a;
    if (i <= paramInt) {}
    for (i = 1;; i = 0) {
      return i;
    }
  }
  
  public void b(String paramString1, String paramString2)
  {
    b(paramString1, paramString2, null);
  }
  
  public void b(String paramString1, String paramString2, Throwable paramThrowable)
  {
    boolean bool = a(paramString1, 2);
    if (bool) {
      Log.v(paramString1, paramString2, paramThrowable);
    }
  }
  
  public void c(String paramString1, String paramString2)
  {
    c(paramString1, paramString2, null);
  }
  
  public void c(String paramString1, String paramString2, Throwable paramThrowable)
  {
    boolean bool = a(paramString1, 4);
    if (bool) {
      Log.i(paramString1, paramString2, paramThrowable);
    }
  }
  
  public void d(String paramString1, String paramString2)
  {
    d(paramString1, paramString2, null);
  }
  
  public void d(String paramString1, String paramString2, Throwable paramThrowable)
  {
    boolean bool = a(paramString1, 5);
    if (bool) {
      Log.w(paramString1, paramString2, paramThrowable);
    }
  }
  
  public void e(String paramString1, String paramString2)
  {
    e(paramString1, paramString2, null);
  }
  
  public void e(String paramString1, String paramString2, Throwable paramThrowable)
  {
    boolean bool = a(paramString1, 6);
    if (bool) {
      Log.e(paramString1, paramString2, paramThrowable);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/io/fabric/sdk/android/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */