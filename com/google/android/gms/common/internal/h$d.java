package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;

public final class h$d
  extends m.a
{
  private h a;
  private final int b;
  
  public h$d(h paramh, int paramInt)
  {
    a = paramh;
    b = paramInt;
  }
  
  private void a()
  {
    a = null;
  }
  
  public void a(int paramInt, Bundle paramBundle)
  {
    p.a(a, "onAccountValidationComplete can be called only once per call to validateAccount");
    h localh = a;
    int i = b;
    localh.a(paramInt, paramBundle, i);
    a();
  }
  
  public void a(int paramInt, IBinder paramIBinder, Bundle paramBundle)
  {
    p.a(a, "onPostInitComplete can be called only once per call to getRemoteService");
    h localh = a;
    int i = b;
    localh.a(paramInt, paramIBinder, paramBundle, i);
    a();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h$d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */