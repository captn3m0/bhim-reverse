package com.google.android.gms.common.internal;

import android.app.PendingIntent;
import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

abstract class h$a
  extends h.c
{
  public final int a;
  public final Bundle b;
  
  protected h$a(h paramh, int paramInt, Bundle paramBundle)
  {
    super(paramh, localBoolean);
    a = paramInt;
    b = paramBundle;
  }
  
  protected abstract void a(ConnectionResult paramConnectionResult);
  
  protected void a(Boolean paramBoolean)
  {
    int i = 1;
    Object localObject1 = null;
    Object localObject2;
    if (paramBoolean == null)
    {
      localObject2 = c;
      h.a((h)localObject2, i, null);
    }
    for (;;)
    {
      return;
      int j = a;
      switch (j)
      {
      default: 
        h.a(c, i, null);
        localObject2 = b;
        if (localObject2 != null)
        {
          localObject1 = b;
          localObject2 = "pendingIntent";
          localObject1 = (PendingIntent)((Bundle)localObject1).getParcelable((String)localObject2);
        }
        localObject2 = new com/google/android/gms/common/ConnectionResult;
        i = a;
        ((ConnectionResult)localObject2).<init>(i, (PendingIntent)localObject1);
        a((ConnectionResult)localObject2);
        break;
      case 0: 
        boolean bool = a();
        if (!bool)
        {
          h.a(c, i, null);
          localObject2 = new com/google/android/gms/common/ConnectionResult;
          i = 8;
          ((ConnectionResult)localObject2).<init>(i, null);
          a((ConnectionResult)localObject2);
        }
        break;
      }
    }
    h.a(c, i, null);
    localObject1 = new java/lang/IllegalStateException;
    ((IllegalStateException)localObject1).<init>("A fatal developer error has occurred. Check the logs for further information.");
    throw ((Throwable)localObject1);
  }
  
  protected abstract boolean a();
  
  protected void b() {}
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */