package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.c.b;
import com.google.android.gms.common.api.c.c;

public final class h$g
  extends h.a
{
  public final IBinder e;
  
  public h$g(h paramh, int paramInt, IBinder paramIBinder, Bundle paramBundle)
  {
    super(paramh, paramInt, paramBundle);
    e = paramIBinder;
  }
  
  protected void a(ConnectionResult paramConnectionResult)
  {
    c.c localc = h.e(f);
    if (localc != null)
    {
      localc = h.e(f);
      localc.a(paramConnectionResult);
    }
    f.a(paramConnectionResult);
  }
  
  protected boolean a()
  {
    bool1 = false;
    localBundle = null;
    try
    {
      Object localObject1 = e;
      localObject1 = ((IBinder)localObject1).getInterfaceDescriptor();
      localObject3 = f.b();
      boolean bool2 = ((String)localObject3).equals(localObject1);
      if (bool2) {
        break label129;
      }
      localObject3 = "GmsClient";
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder = localStringBuilder.append("service descriptor mismatch: ");
      String str = f.b();
      localStringBuilder = localStringBuilder.append(str);
      str = " vs. ";
      localStringBuilder = localStringBuilder.append(str);
      localObject1 = (String)localObject1;
      Log.e((String)localObject3, (String)localObject1);
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        Object localObject2 = "GmsClient";
        Object localObject3 = "service probably died";
        Log.w((String)localObject2, (String)localObject3);
        continue;
        localObject2 = f;
        localObject3 = e;
        localObject2 = ((h)localObject2).a((IBinder)localObject3);
        if (localObject2 != null)
        {
          localObject3 = f;
          int i = 2;
          int j = 3;
          boolean bool3 = h.a((h)localObject3, i, j, (IInterface)localObject2);
          if (bool3)
          {
            localBundle = f.k();
            localObject2 = h.b(f);
            if (localObject2 != null)
            {
              localObject2 = h.b(f);
              ((c.b)localObject2).a(localBundle);
            }
            bool1 = true;
          }
        }
      }
    }
    return bool1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h$g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */