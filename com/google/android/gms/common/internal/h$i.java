package com.google.android.gms.common.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.c.e;

public final class h$i
  extends h.a
{
  public h$i(h paramh, int paramInt, Bundle paramBundle)
  {
    super(paramh, paramInt, paramBundle);
  }
  
  protected void a(ConnectionResult paramConnectionResult)
  {
    h.a(e).b(paramConnectionResult);
    e.a(paramConnectionResult);
  }
  
  protected boolean a()
  {
    c.e locale = h.a(e);
    ConnectionResult localConnectionResult = ConnectionResult.a;
    locale.b(localConnectionResult);
    return true;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h$i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */