package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class g
  implements Parcelable.Creator
{
  static void a(GetServiceRequest paramGetServiceRequest, Parcel paramParcel, int paramInt)
  {
    int i = a.a(paramParcel);
    int j = a;
    a.a(paramParcel, 1, j);
    j = b;
    a.a(paramParcel, 2, j);
    j = c;
    a.a(paramParcel, 3, j);
    Object localObject = d;
    a.a(paramParcel, 4, (String)localObject, false);
    localObject = e;
    a.a(paramParcel, 5, (IBinder)localObject, false);
    localObject = f;
    a.a(paramParcel, 6, (Parcelable[])localObject, paramInt, false);
    localObject = g;
    a.a(paramParcel, 7, (Bundle)localObject, false);
    localObject = h;
    a.a(paramParcel, 8, (Parcelable)localObject, paramInt, false);
    a.a(paramParcel, i);
  }
  
  public GetServiceRequest a(Parcel paramParcel)
  {
    int i = 0;
    Object localObject1 = null;
    int j = zza.b(paramParcel);
    Bundle localBundle = null;
    Object localObject2 = null;
    IBinder localIBinder = null;
    String str = null;
    int k = 0;
    int m = 0;
    Object localObject3 = null;
    for (;;)
    {
      n = paramParcel.dataPosition();
      if (n >= j) {
        break;
      }
      n = zza.a(paramParcel);
      int i1 = zza.a(n);
      switch (i1)
      {
      default: 
        zza.b(paramParcel, n);
        break;
      case 1: 
        m = zza.d(paramParcel, n);
        break;
      case 2: 
        k = zza.d(paramParcel, n);
        break;
      case 3: 
        i = zza.d(paramParcel, n);
        break;
      case 4: 
        str = zza.h(paramParcel, n);
        break;
      case 5: 
        localIBinder = zza.i(paramParcel, n);
        break;
      case 6: 
        localObject2 = Scope.CREATOR;
        localObject4 = (Scope[])zza.b(paramParcel, n, (Parcelable.Creator)localObject2);
        localObject2 = localObject4;
        break;
      case 7: 
        localBundle = zza.j(paramParcel, n);
        break;
      case 8: 
        localObject1 = Account.CREATOR;
        localObject4 = (Account)zza.a(paramParcel, n, (Parcelable.Creator)localObject1);
        localObject1 = localObject4;
      }
    }
    int n = paramParcel.dataPosition();
    if (n != j)
    {
      localObject4 = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = "Overread allowed size end=" + j;
      ((zza.zza)localObject4).<init>((String)localObject3, paramParcel);
      throw ((Throwable)localObject4);
    }
    Object localObject4 = new com/google/android/gms/common/internal/GetServiceRequest;
    ((GetServiceRequest)localObject4).<init>(m, k, i, str, localIBinder, (Scope[])localObject2, localBundle, (Account)localObject1);
    return (GetServiceRequest)localObject4;
  }
  
  public GetServiceRequest[] a(int paramInt)
  {
    return new GetServiceRequest[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */