package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.c.e;

public final class h$h
  extends h.a
{
  public h$h(h paramh)
  {
    super(paramh, 0, null);
  }
  
  protected void a(ConnectionResult paramConnectionResult)
  {
    h.a(e).a(paramConnectionResult);
    e.a(paramConnectionResult);
  }
  
  protected boolean a()
  {
    c.e locale = h.a(e);
    ConnectionResult localConnectionResult = ConnectionResult.a;
    locale.a(localConnectionResult);
    return true;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h$h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */