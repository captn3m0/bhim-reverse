package com.google.android.gms.common.internal.safeparcel;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public class a
{
  public static int a(Parcel paramParcel)
  {
    return b(paramParcel, 20293);
  }
  
  public static void a(Parcel paramParcel, int paramInt)
  {
    c(paramParcel, paramInt);
  }
  
  public static void a(Parcel paramParcel, int paramInt1, int paramInt2)
  {
    b(paramParcel, paramInt1, 4);
    paramParcel.writeInt(paramInt2);
  }
  
  public static void a(Parcel paramParcel, int paramInt, long paramLong)
  {
    b(paramParcel, paramInt, 8);
    paramParcel.writeLong(paramLong);
  }
  
  public static void a(Parcel paramParcel, int paramInt, Bundle paramBundle, boolean paramBoolean)
  {
    int i;
    if (paramBundle == null) {
      if (paramBoolean)
      {
        i = 0;
        b(paramParcel, paramInt, 0);
      }
    }
    for (;;)
    {
      return;
      i = b(paramParcel, paramInt);
      paramParcel.writeBundle(paramBundle);
      c(paramParcel, i);
    }
  }
  
  public static void a(Parcel paramParcel, int paramInt, IBinder paramIBinder, boolean paramBoolean)
  {
    int i;
    if (paramIBinder == null) {
      if (paramBoolean)
      {
        i = 0;
        b(paramParcel, paramInt, 0);
      }
    }
    for (;;)
    {
      return;
      i = b(paramParcel, paramInt);
      paramParcel.writeStrongBinder(paramIBinder);
      c(paramParcel, i);
    }
  }
  
  public static void a(Parcel paramParcel, int paramInt1, Parcelable paramParcelable, int paramInt2, boolean paramBoolean)
  {
    int i;
    if (paramParcelable == null) {
      if (paramBoolean)
      {
        i = 0;
        b(paramParcel, paramInt1, 0);
      }
    }
    for (;;)
    {
      return;
      i = b(paramParcel, paramInt1);
      paramParcelable.writeToParcel(paramParcel, paramInt2);
      c(paramParcel, i);
    }
  }
  
  public static void a(Parcel paramParcel, int paramInt, Float paramFloat, boolean paramBoolean)
  {
    int i;
    float f;
    if (paramFloat == null) {
      if (paramBoolean)
      {
        i = 0;
        f = 0.0F;
        b(paramParcel, paramInt, 0);
      }
    }
    for (;;)
    {
      return;
      i = 4;
      b(paramParcel, paramInt, i);
      f = paramFloat.floatValue();
      paramParcel.writeFloat(f);
    }
  }
  
  public static void a(Parcel paramParcel, int paramInt, Long paramLong, boolean paramBoolean)
  {
    int i;
    if (paramLong == null) {
      if (paramBoolean)
      {
        i = 0;
        b(paramParcel, paramInt, 0);
      }
    }
    for (;;)
    {
      return;
      i = 8;
      b(paramParcel, paramInt, i);
      long l = paramLong.longValue();
      paramParcel.writeLong(l);
    }
  }
  
  public static void a(Parcel paramParcel, int paramInt, String paramString, boolean paramBoolean)
  {
    int i;
    if (paramString == null) {
      if (paramBoolean)
      {
        i = 0;
        b(paramParcel, paramInt, 0);
      }
    }
    for (;;)
    {
      return;
      i = b(paramParcel, paramInt);
      paramParcel.writeString(paramString);
      c(paramParcel, i);
    }
  }
  
  public static void a(Parcel paramParcel, int paramInt, boolean paramBoolean)
  {
    int i = 4;
    b(paramParcel, paramInt, i);
    if (paramBoolean) {}
    for (i = 1;; i = 0)
    {
      paramParcel.writeInt(i);
      return;
    }
  }
  
  public static void a(Parcel paramParcel, int paramInt1, Parcelable[] paramArrayOfParcelable, int paramInt2, boolean paramBoolean)
  {
    if (paramArrayOfParcelable == null) {
      if (paramBoolean) {
        b(paramParcel, paramInt1, 0);
      }
    }
    for (;;)
    {
      return;
      int i = b(paramParcel, paramInt1);
      int j = paramArrayOfParcelable.length;
      paramParcel.writeInt(j);
      int k = 0;
      if (k < j)
      {
        Parcelable localParcelable = paramArrayOfParcelable[k];
        if (localParcelable == null) {
          paramParcel.writeInt(0);
        }
        for (;;)
        {
          k += 1;
          break;
          a(paramParcel, localParcelable, paramInt2);
        }
      }
      c(paramParcel, i);
    }
  }
  
  public static void a(Parcel paramParcel, int paramInt, String[] paramArrayOfString, boolean paramBoolean)
  {
    int i;
    if (paramArrayOfString == null) {
      if (paramBoolean)
      {
        i = 0;
        b(paramParcel, paramInt, 0);
      }
    }
    for (;;)
    {
      return;
      i = b(paramParcel, paramInt);
      paramParcel.writeStringArray(paramArrayOfString);
      c(paramParcel, i);
    }
  }
  
  private static void a(Parcel paramParcel, Parcelable paramParcelable, int paramInt)
  {
    int i = paramParcel.dataPosition();
    paramParcel.writeInt(1);
    int j = paramParcel.dataPosition();
    paramParcelable.writeToParcel(paramParcel, paramInt);
    int k = paramParcel.dataPosition();
    paramParcel.setDataPosition(i);
    i = k - j;
    paramParcel.writeInt(i);
    paramParcel.setDataPosition(k);
  }
  
  private static int b(Parcel paramParcel, int paramInt)
  {
    int i = 0xFFFF0000 | paramInt;
    paramParcel.writeInt(i);
    paramParcel.writeInt(0);
    return paramParcel.dataPosition();
  }
  
  private static void b(Parcel paramParcel, int paramInt1, int paramInt2)
  {
    int i = (char)-1;
    if (paramInt2 >= i)
    {
      i = 0xFFFF0000 | paramInt1;
      paramParcel.writeInt(i);
      paramParcel.writeInt(paramInt2);
    }
    for (;;)
    {
      return;
      i = paramInt2 << 16 | paramInt1;
      paramParcel.writeInt(i);
    }
  }
  
  private static void c(Parcel paramParcel, int paramInt)
  {
    int i = paramParcel.dataPosition();
    int j = i - paramInt;
    int k = paramInt + -4;
    paramParcel.setDataPosition(k);
    paramParcel.writeInt(j);
    paramParcel.setDataPosition(i);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/safeparcel/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */