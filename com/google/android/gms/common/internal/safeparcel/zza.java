package com.google.android.gms.common.internal.safeparcel;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

public class zza
{
  public static int a(int paramInt)
  {
    return (char)-1 & paramInt;
  }
  
  public static int a(Parcel paramParcel)
  {
    return paramParcel.readInt();
  }
  
  public static int a(Parcel paramParcel, int paramInt)
  {
    int i = -65536;
    int j = paramInt & i;
    if (j != i)
    {
      j = paramInt >> 16;
      i = (char)-1;
      j &= i;
    }
    for (;;)
    {
      return j;
      j = paramParcel.readInt();
    }
  }
  
  public static Parcelable a(Parcel paramParcel, int paramInt, Parcelable.Creator paramCreator)
  {
    int i = a(paramParcel, paramInt);
    int j = paramParcel.dataPosition();
    Parcelable localParcelable;
    if (i == 0) {
      localParcelable = null;
    }
    for (;;)
    {
      return localParcelable;
      localParcelable = (Parcelable)paramCreator.createFromParcel(paramParcel);
      i += j;
      paramParcel.setDataPosition(i);
    }
  }
  
  private static void a(Parcel paramParcel, int paramInt1, int paramInt2)
  {
    int i = a(paramParcel, paramInt1);
    if (i != paramInt2)
    {
      zza.zza localzza = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      StringBuilder localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localStringBuilder = localStringBuilder.append("Expected size ").append(paramInt2).append(" got ").append(i).append(" (0x");
      String str = Integer.toHexString(i);
      str = str + ")";
      localzza.<init>(str, paramParcel);
      throw localzza;
    }
  }
  
  private static void a(Parcel paramParcel, int paramInt1, int paramInt2, int paramInt3)
  {
    if (paramInt2 != paramInt3)
    {
      zza.zza localzza = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = ((StringBuilder)localObject).append("Expected size ").append(paramInt3).append(" got ").append(paramInt2).append(" (0x");
      String str = Integer.toHexString(paramInt2);
      localObject = str + ")";
      localzza.<init>((String)localObject, paramParcel);
      throw localzza;
    }
  }
  
  public static int b(Parcel paramParcel)
  {
    int i = a(paramParcel);
    int j = a(paramParcel, i);
    int k = paramParcel.dataPosition();
    int m = a(i);
    int n = 20293;
    zza.zza localzza;
    String str;
    if (m != n)
    {
      localzza = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      StringBuilder localStringBuilder1 = new java/lang/StringBuilder;
      localStringBuilder1.<init>();
      localStringBuilder1 = localStringBuilder1.append("Expected object header. Got 0x");
      str = Integer.toHexString(i);
      str = str;
      localzza.<init>(str, paramParcel);
      throw localzza;
    }
    i = k + j;
    if (i >= k)
    {
      j = paramParcel.dataSize();
      if (i <= j) {}
    }
    else
    {
      localzza = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      StringBuilder localStringBuilder2 = new java/lang/StringBuilder;
      localStringBuilder2.<init>();
      str = "Size read is invalid start=" + k + " end=" + i;
      localzza.<init>(str, paramParcel);
      throw localzza;
    }
    return i;
  }
  
  public static void b(Parcel paramParcel, int paramInt)
  {
    int i = a(paramParcel, paramInt);
    int j = paramParcel.dataPosition();
    i += j;
    paramParcel.setDataPosition(i);
  }
  
  public static Object[] b(Parcel paramParcel, int paramInt, Parcelable.Creator paramCreator)
  {
    int i = a(paramParcel, paramInt);
    int j = paramParcel.dataPosition();
    Object[] arrayOfObject;
    if (i == 0) {
      arrayOfObject = null;
    }
    for (;;)
    {
      return arrayOfObject;
      arrayOfObject = paramParcel.createTypedArray(paramCreator);
      i += j;
      paramParcel.setDataPosition(i);
    }
  }
  
  public static boolean c(Parcel paramParcel, int paramInt)
  {
    a(paramParcel, paramInt, 4);
    int i = paramParcel.readInt();
    if (i != 0) {}
    int j;
    for (i = 1;; j = 0) {
      return i;
    }
  }
  
  public static int d(Parcel paramParcel, int paramInt)
  {
    a(paramParcel, paramInt, 4);
    return paramParcel.readInt();
  }
  
  public static long e(Parcel paramParcel, int paramInt)
  {
    a(paramParcel, paramInt, 8);
    return paramParcel.readLong();
  }
  
  public static Long f(Parcel paramParcel, int paramInt)
  {
    int i = a(paramParcel, paramInt);
    if (i == 0) {
      i = 0;
    }
    long l;
    for (Long localLong = null;; localLong = Long.valueOf(l))
    {
      return localLong;
      int j = 8;
      a(paramParcel, paramInt, i, j);
      l = paramParcel.readLong();
    }
  }
  
  public static Float g(Parcel paramParcel, int paramInt)
  {
    int i = a(paramParcel, paramInt);
    float f;
    if (i == 0)
    {
      i = 0;
      f = 0.0F;
    }
    for (Float localFloat = null;; localFloat = Float.valueOf(f))
    {
      return localFloat;
      int j = 4;
      a(paramParcel, paramInt, i, j);
      f = paramParcel.readFloat();
    }
  }
  
  public static String h(Parcel paramParcel, int paramInt)
  {
    int i = a(paramParcel, paramInt);
    int j = paramParcel.dataPosition();
    String str;
    if (i == 0) {
      str = null;
    }
    for (;;)
    {
      return str;
      str = paramParcel.readString();
      i += j;
      paramParcel.setDataPosition(i);
    }
  }
  
  public static IBinder i(Parcel paramParcel, int paramInt)
  {
    int i = a(paramParcel, paramInt);
    int j = paramParcel.dataPosition();
    IBinder localIBinder;
    if (i == 0) {
      localIBinder = null;
    }
    for (;;)
    {
      return localIBinder;
      localIBinder = paramParcel.readStrongBinder();
      i += j;
      paramParcel.setDataPosition(i);
    }
  }
  
  public static Bundle j(Parcel paramParcel, int paramInt)
  {
    int i = a(paramParcel, paramInt);
    int j = paramParcel.dataPosition();
    Bundle localBundle;
    if (i == 0) {
      localBundle = null;
    }
    for (;;)
    {
      return localBundle;
      localBundle = paramParcel.readBundle();
      i += j;
      paramParcel.setDataPosition(i);
    }
  }
  
  public static String[] k(Parcel paramParcel, int paramInt)
  {
    int i = a(paramParcel, paramInt);
    int j = paramParcel.dataPosition();
    String[] arrayOfString;
    if (i == 0) {
      arrayOfString = null;
    }
    for (;;)
    {
      return arrayOfString;
      arrayOfString = paramParcel.createStringArray();
      i += j;
      paramParcel.setDataPosition(i);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/safeparcel/zza.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */