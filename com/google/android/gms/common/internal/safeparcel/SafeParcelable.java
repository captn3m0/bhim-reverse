package com.google.android.gms.common.internal.safeparcel;

import android.os.Parcelable;

public abstract interface SafeParcelable
  extends Parcelable
{
  public static final String NULL = "SAFE_PARCELABLE_NULL_STRING";
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/safeparcel/SafeParcelable.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */