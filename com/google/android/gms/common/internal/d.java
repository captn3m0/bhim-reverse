package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.view.View;
import com.google.android.gms.common.api.c.a;
import com.google.android.gms.internal.m;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public final class d
{
  private final Account a;
  private final Set b;
  private final Set c;
  private final Map d;
  private final int e;
  private final View f;
  private final String g;
  private final String h;
  private final m i;
  
  public d(Account paramAccount, Set paramSet, Map paramMap, int paramInt, View paramView, String paramString1, String paramString2, m paramm)
  {
    a = paramAccount;
    if (paramSet == null) {}
    HashSet localHashSet;
    for (Object localObject = Collections.EMPTY_SET;; localObject = Collections.unmodifiableSet(paramSet))
    {
      b = ((Set)localObject);
      if (paramMap == null) {
        paramMap = Collections.EMPTY_MAP;
      }
      d = paramMap;
      f = paramView;
      e = paramInt;
      g = paramString1;
      h = paramString2;
      i = paramm;
      localHashSet = new java/util/HashSet;
      localObject = b;
      localHashSet.<init>((Collection)localObject);
      localObject = d.values();
      Iterator localIterator = ((Collection)localObject).iterator();
      for (;;)
      {
        boolean bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject = nexta;
        localHashSet.addAll((Collection)localObject);
      }
    }
    localObject = Collections.unmodifiableSet(localHashSet);
    c = ((Set)localObject);
  }
  
  public static d a(Context paramContext)
  {
    c.a locala = new com/google/android/gms/common/api/c$a;
    locala.<init>(paramContext);
    return locala.a();
  }
  
  public Account a()
  {
    return a;
  }
  
  public Set b()
  {
    return c;
  }
  
  public String c()
  {
    return h;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */