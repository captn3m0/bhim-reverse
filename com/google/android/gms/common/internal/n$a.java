package com.google.android.gms.common.internal;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;

public abstract class n$a
  extends Binder
  implements n
{
  public static n a(IBinder paramIBinder)
  {
    Object localObject;
    if (paramIBinder == null) {
      localObject = null;
    }
    for (;;)
    {
      return (n)localObject;
      localObject = paramIBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      if (localObject != null)
      {
        boolean bool = localObject instanceof n;
        if (bool)
        {
          localObject = (n)localObject;
          continue;
        }
      }
      localObject = new com/google/android/gms/common/internal/n$a$a;
      ((n.a.a)localObject).<init>(paramIBinder);
    }
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
  {
    boolean bool1 = false;
    Object localObject1 = null;
    boolean bool2 = true;
    switch (paramInt1)
    {
    default: 
      bool1 = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
    }
    for (;;)
    {
      return bool1;
      localObject1 = "com.google.android.gms.common.internal.IGmsServiceBroker";
      paramParcel2.writeString((String)localObject1);
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      m localm = m.a.a(paramParcel1.readStrongBinder());
      int i = paramParcel1.readInt();
      String str1 = paramParcel1.readString();
      Object localObject2 = paramParcel1.readString();
      Object localObject3 = paramParcel1.createStringArray();
      Object localObject4 = paramParcel1.readString();
      int j = paramParcel1.readInt();
      if (j != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      for (Object localObject5 = localObject1;; localObject5 = null)
      {
        localObject1 = this;
        a(localm, i, str1, (String)localObject2, (String[])localObject3, (String)localObject4, (Bundle)localObject5);
        paramParcel2.writeNoException();
        bool1 = bool2;
        break;
        j = 0;
      }
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      int k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      a(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      int m = paramParcel1.readInt();
      String str2 = paramParcel1.readString();
      a((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      a((m)localObject1, m);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      b(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      c(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      d(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      e(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      localObject2 = paramParcel1.readString();
      localObject3 = paramParcel1.createStringArray();
      localObject4 = paramParcel1.readString();
      localObject5 = paramParcel1.readStrongBinder();
      String str3 = paramParcel1.readString();
      int n = paramParcel1.readInt();
      if (n != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      for (Object localObject6 = localObject1;; localObject6 = null)
      {
        localObject1 = this;
        a(localm, i, str1, (String)localObject2, (String[])localObject3, (String)localObject4, (IBinder)localObject5, str3, (Bundle)localObject6);
        paramParcel2.writeNoException();
        bool1 = bool2;
        break;
        n = 0;
      }
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      localObject2 = paramParcel1.readString();
      localObject3 = paramParcel1.createStringArray();
      localObject1 = this;
      a(localm, i, str1, (String)localObject2, (String[])localObject3);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      f(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      g(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      h(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      i(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      j(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      k(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      l(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      m(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      localObject2 = paramParcel1.readStrongBinder();
      int i1 = paramParcel1.readInt();
      if (i1 != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      for (localObject3 = localObject1;; localObject3 = null)
      {
        localObject1 = this;
        a(localm, i, str1, (IBinder)localObject2, (Bundle)localObject3);
        paramParcel2.writeNoException();
        bool1 = bool2;
        break;
        i1 = 0;
      }
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      localObject2 = paramParcel1.createStringArray();
      localObject3 = paramParcel1.readString();
      int i2 = paramParcel1.readInt();
      if (i2 != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      for (localObject4 = localObject1;; localObject4 = null)
      {
        localObject1 = this;
        a(localm, i, str1, (String[])localObject2, (String)localObject3, (Bundle)localObject4);
        paramParcel2.writeNoException();
        bool1 = bool2;
        break;
        i2 = 0;
      }
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      b((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      c((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      n(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      d((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      o(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      e((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      p(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      localObject1 = "com.google.android.gms.common.internal.IGmsServiceBroker";
      paramParcel1.enforceInterface((String)localObject1);
      a();
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      localObject2 = paramParcel1.readString();
      localObject3 = paramParcel1.createStringArray();
      i2 = paramParcel1.readInt();
      if (i2 != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      for (localObject4 = localObject1;; localObject4 = null)
      {
        localObject1 = this;
        a(localm, i, str1, (String)localObject2, (String[])localObject3, (Bundle)localObject4);
        paramParcel2.writeNoException();
        bool1 = bool2;
        break;
        i2 = 0;
      }
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      f((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      g((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      localObject2 = paramParcel1.readString();
      localObject3 = paramParcel1.readString();
      localObject4 = paramParcel1.createStringArray();
      localObject1 = this;
      a(localm, i, str1, (String)localObject2, (String)localObject3, (String[])localObject4);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      str1 = paramParcel1.readString();
      a((m)localObject1, m, str2, str1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      h((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      i((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      q(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      r(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      j((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      s(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      k((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      str1 = paramParcel1.readString();
      k = paramParcel1.readInt();
      if (k != 0) {
        localObject1 = (Bundle)Bundle.CREATOR.createFromParcel(paramParcel1);
      }
      t(localm, i, str1, (Bundle)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      l((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localObject1 = m.a.a(paramParcel1.readStrongBinder());
      m = paramParcel1.readInt();
      str2 = paramParcel1.readString();
      m((m)localObject1, m, str2);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      if (i != 0) {
        localObject1 = (GetServiceRequest)GetServiceRequest.CREATOR.createFromParcel(paramParcel1);
      }
      a(localm, (GetServiceRequest)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
      continue;
      paramParcel1.enforceInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
      localm = m.a.a(paramParcel1.readStrongBinder());
      i = paramParcel1.readInt();
      if (i != 0) {
        localObject1 = (ValidateAccountRequest)ValidateAccountRequest.CREATOR.createFromParcel(paramParcel1);
      }
      a(localm, (ValidateAccountRequest)localObject1);
      paramParcel2.writeNoException();
      bool1 = bool2;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/n$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */