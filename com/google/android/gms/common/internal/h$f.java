package com.google.android.gms.common.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.c.c;
import com.google.android.gms.common.api.c.e;
import java.util.Set;

public class h$f
  implements c.e
{
  public h$f(h paramh) {}
  
  public void a(ConnectionResult paramConnectionResult)
  {
    boolean bool = paramConnectionResult.a();
    Object localObject;
    if (bool)
    {
      localObject = a;
      Set localSet = h.d(a);
      ((h)localObject).a(null, localSet);
    }
    for (;;)
    {
      return;
      localObject = h.e(a);
      if (localObject != null)
      {
        localObject = h.e(a);
        ((c.c)localObject).a(paramConnectionResult);
      }
    }
  }
  
  public void b(ConnectionResult paramConnectionResult)
  {
    IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
    localIllegalStateException.<init>("Legacy GmsClient received onReportAccountValidation callback.");
    throw localIllegalStateException;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h$f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */