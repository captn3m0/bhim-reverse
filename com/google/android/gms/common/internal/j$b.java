package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.common.stats.b;
import java.util.HashSet;
import java.util.Set;

final class j$b
{
  private final j.b.a b;
  private final Set c;
  private int d;
  private boolean e;
  private IBinder f;
  private final j.a g;
  private ComponentName h;
  
  public j$b(j paramj, j.a parama)
  {
    g = parama;
    Object localObject = new com/google/android/gms/common/internal/j$b$a;
    ((j.b.a)localObject).<init>(this);
    b = ((j.b.a)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    c = ((Set)localObject);
    d = 2;
  }
  
  public void a(ServiceConnection paramServiceConnection, String paramString)
  {
    b localb = j.c(a);
    Context localContext = j.b(a);
    Intent localIntent = g.a();
    localb.a(localContext, paramServiceConnection, paramString, localIntent);
    c.add(paramServiceConnection);
  }
  
  public void a(String paramString)
  {
    d = 3;
    Object localObject1 = j.c(a);
    Object localObject2 = j.b(a);
    Intent localIntent = g.a();
    j.b.a locala = b;
    int i = 129;
    Object localObject3 = paramString;
    boolean bool = ((b)localObject1).a((Context)localObject2, paramString, localIntent, locala, i);
    e = bool;
    bool = e;
    if (!bool)
    {
      int j = 2;
      d = j;
    }
    try
    {
      localObject1 = a;
      localObject1 = j.c((j)localObject1);
      localObject2 = a;
      localObject2 = j.b((j)localObject2);
      localObject3 = b;
      ((b)localObject1).a((Context)localObject2, (ServiceConnection)localObject3);
      return;
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
  }
  
  public boolean a()
  {
    return e;
  }
  
  public boolean a(ServiceConnection paramServiceConnection)
  {
    return c.contains(paramServiceConnection);
  }
  
  public int b()
  {
    return d;
  }
  
  public void b(ServiceConnection paramServiceConnection, String paramString)
  {
    b localb = j.c(a);
    Context localContext = j.b(a);
    localb.b(localContext, paramServiceConnection);
    c.remove(paramServiceConnection);
  }
  
  public void b(String paramString)
  {
    b localb = j.c(a);
    Context localContext = j.b(a);
    j.b.a locala = b;
    localb.a(localContext, locala);
    e = false;
    d = 2;
  }
  
  public boolean c()
  {
    return c.isEmpty();
  }
  
  public IBinder d()
  {
    return f;
  }
  
  public ComponentName e()
  {
    return h;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/j$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */