package com.google.android.gms.common.internal;

import java.util.ArrayList;
import java.util.List;

public final class o$a
{
  private final List a;
  private final Object b;
  
  private o$a(Object paramObject)
  {
    Object localObject = p.a(paramObject);
    b = localObject;
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    a = ((List)localObject);
  }
  
  public a a(String paramString, Object paramObject)
  {
    List localList = a;
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    Object localObject2 = (String)p.a(paramString);
    localObject2 = ((StringBuilder)localObject1).append((String)localObject2).append("=");
    localObject1 = String.valueOf(paramObject);
    localObject2 = (String)localObject1;
    localList.add(localObject2);
    return this;
  }
  
  public String toString()
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>(100);
    String str = b.getClass().getSimpleName();
    StringBuilder localStringBuilder = ((StringBuilder)localObject).append(str).append('{');
    int i = a.size();
    int j = 0;
    localObject = null;
    int k = 0;
    str = null;
    while (k < i)
    {
      localObject = (String)a.get(k);
      localStringBuilder.append((String)localObject);
      j = i + -1;
      if (k < j)
      {
        localObject = ", ";
        localStringBuilder.append((String)localObject);
      }
      j = k + 1;
      k = j;
    }
    return '}';
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/o$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */