package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;

public final class h$e
  implements ServiceConnection
{
  private final int b;
  
  public h$e(h paramh, int paramInt)
  {
    b = paramInt;
  }
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    p.a(paramIBinder, "Expecting a valid IBinder");
    h localh = a;
    n localn = n.a.a(paramIBinder);
    h.a(localh, localn);
    localh = a;
    int i = b;
    localh.c(i);
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    Handler localHandler = a.a;
    Object localObject = a.a;
    int i = b;
    localObject = ((Handler)localObject).obtainMessage(4, i, 1);
    localHandler.sendMessage((Message)localObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h$e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */