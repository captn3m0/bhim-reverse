package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class b
  implements Parcelable.Creator
{
  static void a(ValidateAccountRequest paramValidateAccountRequest, Parcel paramParcel, int paramInt)
  {
    int i = a.a(paramParcel);
    int j = a;
    a.a(paramParcel, 1, j);
    j = paramValidateAccountRequest.a();
    a.a(paramParcel, 2, j);
    Object localObject = b;
    a.a(paramParcel, 3, (IBinder)localObject, false);
    localObject = paramValidateAccountRequest.b();
    a.a(paramParcel, 4, (Parcelable[])localObject, paramInt, false);
    localObject = paramValidateAccountRequest.d();
    a.a(paramParcel, 5, (Bundle)localObject, false);
    localObject = paramValidateAccountRequest.c();
    a.a(paramParcel, 6, (String)localObject, false);
    a.a(paramParcel, i);
  }
  
  public ValidateAccountRequest a(Parcel paramParcel)
  {
    int i = 0;
    String str = null;
    int j = zza.b(paramParcel);
    Bundle localBundle = null;
    Object localObject1 = null;
    IBinder localIBinder = null;
    int k = 0;
    Object localObject2 = null;
    for (;;)
    {
      m = paramParcel.dataPosition();
      if (m >= j) {
        break;
      }
      m = zza.a(paramParcel);
      int n = zza.a(m);
      switch (n)
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        k = zza.d(paramParcel, m);
        break;
      case 2: 
        i = zza.d(paramParcel, m);
        break;
      case 3: 
        localIBinder = zza.i(paramParcel, m);
        break;
      case 4: 
        localObject1 = Scope.CREATOR;
        localObject3 = (Scope[])zza.b(paramParcel, m, (Parcelable.Creator)localObject1);
        localObject1 = localObject3;
        break;
      case 5: 
        localBundle = zza.j(paramParcel, m);
        break;
      case 6: 
        str = zza.h(paramParcel, m);
      }
    }
    int m = paramParcel.dataPosition();
    if (m != j)
    {
      localObject3 = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Overread allowed size end=" + j;
      ((zza.zza)localObject3).<init>((String)localObject2, paramParcel);
      throw ((Throwable)localObject3);
    }
    Object localObject3 = new com/google/android/gms/common/internal/ValidateAccountRequest;
    ((ValidateAccountRequest)localObject3).<init>(k, i, localIBinder, (Scope[])localObject1, localBundle, str);
    return (ValidateAccountRequest)localObject3;
  }
  
  public ValidateAccountRequest[] a(int paramInt)
  {
    return new ValidateAccountRequest[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */