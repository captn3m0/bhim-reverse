package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.util.Log;

public class f
  implements DialogInterface.OnClickListener
{
  private final Activity a;
  private final Fragment b;
  private final Intent c;
  private final int d;
  
  public f(Activity paramActivity, Intent paramIntent, int paramInt)
  {
    a = paramActivity;
    b = null;
    c = paramIntent;
    d = paramInt;
  }
  
  public f(Fragment paramFragment, Intent paramIntent, int paramInt)
  {
    a = null;
    b = paramFragment;
    c = paramIntent;
    d = paramInt;
  }
  
  public void onClick(DialogInterface paramDialogInterface, int paramInt)
  {
    for (;;)
    {
      try
      {
        localObject1 = c;
        if (localObject1 != null)
        {
          localObject1 = b;
          if (localObject1 != null)
          {
            localObject1 = b;
            localObject2 = c;
            i = d;
            ((Fragment)localObject1).a((Intent)localObject2, i);
            paramDialogInterface.dismiss();
            return;
          }
        }
      }
      catch (ActivityNotFoundException localActivityNotFoundException)
      {
        Object localObject1;
        int i;
        String str = "SettingsRedirect";
        Object localObject2 = "Can't redirect to app settings for Google Play services";
        Log.e(str, (String)localObject2);
        continue;
      }
      localObject1 = c;
      if (localObject1 != null)
      {
        localObject1 = a;
        localObject2 = c;
        i = d;
        ((Activity)localObject1).startActivityForResult((Intent)localObject2, i);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */