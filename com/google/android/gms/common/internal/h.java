package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.c.b;
import com.google.android.gms.common.api.c.c;
import com.google.android.gms.common.api.c.e;
import com.google.android.gms.common.b;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public abstract class h
{
  public static final String[] c;
  final Handler a;
  protected AtomicInteger b;
  private final Context d;
  private final d e;
  private final Looper f;
  private final i g;
  private final b h;
  private final Object i;
  private n j;
  private c.e k;
  private IInterface l;
  private final ArrayList m;
  private h.e n;
  private int o;
  private final Set p;
  private final Account q;
  private final c.b r;
  private final c.c s;
  private final int t;
  
  static
  {
    String[] arrayOfString = new String[2];
    arrayOfString[0] = "service_esmobile";
    arrayOfString[1] = "service_googleme";
    c = arrayOfString;
  }
  
  protected h(Context paramContext, Looper paramLooper, int paramInt, d paramd, c.b paramb, c.c paramc)
  {
    this(paramContext, paramLooper, locali, localb, paramInt, paramd, localb1, localc);
  }
  
  protected h(Context paramContext, Looper paramLooper, i parami, b paramb, int paramInt, d paramd, c.b paramb1, c.c paramc)
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    i = localObject;
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    m = ((ArrayList)localObject);
    o = 1;
    localObject = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject).<init>(0);
    b = ((AtomicInteger)localObject);
    localObject = (Context)p.a(paramContext, "Context must not be null");
    d = ((Context)localObject);
    localObject = (Looper)p.a(paramLooper, "Looper must not be null");
    f = ((Looper)localObject);
    localObject = (i)p.a(parami, "Supervisor must not be null");
    g = ((i)localObject);
    localObject = (b)p.a(paramb, "API availability must not be null");
    h = ((b)localObject);
    localObject = new com/google/android/gms/common/internal/h$b;
    ((h.b)localObject).<init>(this, paramLooper);
    a = ((Handler)localObject);
    t = paramInt;
    localObject = (d)p.a(paramd);
    e = ((d)localObject);
    localObject = paramd.a();
    q = ((Account)localObject);
    localObject = paramd.b();
    localObject = b((Set)localObject);
    p = ((Set)localObject);
    r = paramb1;
    s = paramc;
  }
  
  private boolean a(int paramInt1, int paramInt2, IInterface paramIInterface)
  {
    synchronized (i)
    {
      int i1 = o;
      if (i1 != paramInt1)
      {
        i1 = 0;
        Object localObject2 = null;
        return i1;
      }
      b(paramInt2, paramIInterface);
      i1 = 1;
    }
  }
  
  private Set b(Set paramSet)
  {
    Set localSet = a(paramSet);
    if (localSet == null) {}
    for (Object localObject = localSet;; localObject = localSet)
    {
      return (Set)localObject;
      Iterator localIterator = localSet.iterator();
      boolean bool;
      do
      {
        bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject = (Scope)localIterator.next();
        bool = paramSet.contains(localObject);
      } while (bool);
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Expanding scopes is not permitted, use implied scopes instead");
      throw ((Throwable)localObject);
    }
  }
  
  private void b(int paramInt, IInterface paramIInterface)
  {
    int i1 = 1;
    ??? = null;
    int i2 = 3;
    int i3;
    if (paramInt == i2)
    {
      i3 = i1;
      if (paramIInterface == null) {
        break label98;
      }
      i2 = i1;
      label24:
      if (i3 != i2) {
        break label104;
      }
      p.b(i1);
    }
    for (;;)
    {
      synchronized (i)
      {
        o = paramInt;
        l = paramIInterface;
        a(paramInt, paramIInterface);
        switch (paramInt)
        {
        default: 
          return;
          i3 = 0;
          break;
          i2 = 0;
          break label24;
          i1 = 0;
          Object localObject2 = null;
          break;
        case 2: 
          label98:
          label104:
          o();
        }
      }
      d();
      continue;
      p();
    }
  }
  
  private void o()
  {
    Object localObject1 = n;
    if (localObject1 != null)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("Calling connect() while still connected, missing disconnect() for ");
      localObject3 = a();
      localObject2 = (String)localObject3;
      Log.e("GmsClient", (String)localObject2);
      localObject1 = g;
      localObject2 = a();
      localObject3 = n;
      localObject4 = c();
      ((i)localObject1).b((String)localObject2, (ServiceConnection)localObject3, (String)localObject4);
      localObject1 = b;
      ((AtomicInteger)localObject1).incrementAndGet();
    }
    localObject1 = new com/google/android/gms/common/internal/h$e;
    int i1 = b.get();
    ((h.e)localObject1).<init>(this, i1);
    n = ((h.e)localObject1);
    localObject1 = g;
    Object localObject2 = a();
    Object localObject3 = n;
    Object localObject4 = c();
    boolean bool = ((i)localObject1).a((String)localObject2, (ServiceConnection)localObject3, (String)localObject4);
    if (!bool)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("unable to connect to service: ");
      localObject3 = a();
      localObject2 = (String)localObject3;
      Log.e("GmsClient", (String)localObject2);
      localObject1 = a;
      localObject2 = a;
      int i2 = 3;
      localObject4 = b;
      int i3 = ((AtomicInteger)localObject4).get();
      int i4 = 9;
      localObject2 = ((Handler)localObject2).obtainMessage(i2, i3, i4);
      ((Handler)localObject1).sendMessage((Message)localObject2);
    }
  }
  
  private void p()
  {
    Object localObject = n;
    if (localObject != null)
    {
      localObject = g;
      String str1 = a();
      h.e locale = n;
      String str2 = c();
      ((i)localObject).b(str1, locale, str2);
      localObject = null;
      n = null;
    }
  }
  
  protected abstract IInterface a(IBinder paramIBinder);
  
  protected abstract String a();
  
  protected Set a(Set paramSet)
  {
    return paramSet;
  }
  
  protected void a(int paramInt) {}
  
  protected void a(int paramInt1, Bundle paramBundle, int paramInt2)
  {
    Handler localHandler = a;
    Object localObject = a;
    h.i locali = new com/google/android/gms/common/internal/h$i;
    locali.<init>(this, paramInt1, paramBundle);
    localObject = ((Handler)localObject).obtainMessage(5, paramInt2, -1, locali);
    localHandler.sendMessage((Message)localObject);
  }
  
  protected void a(int paramInt1, IBinder paramIBinder, Bundle paramBundle, int paramInt2)
  {
    Handler localHandler = a;
    Object localObject = a;
    h.g localg = new com/google/android/gms/common/internal/h$g;
    localg.<init>(this, paramInt1, paramIBinder, paramBundle);
    localObject = ((Handler)localObject).obtainMessage(1, paramInt2, -1, localg);
    localHandler.sendMessage((Message)localObject);
  }
  
  protected void a(int paramInt, IInterface paramIInterface) {}
  
  protected void a(ConnectionResult paramConnectionResult) {}
  
  public void a(c.e parame)
  {
    c.e locale = (c.e)p.a(parame, "Connection progress callbacks cannot be null.");
    k = locale;
    b(2, null);
  }
  
  public void a(l paraml, Set paramSet)
  {
    for (;;)
    {
      try
      {
        localObject1 = i();
        localObject2 = new com/google/android/gms/common/internal/GetServiceRequest;
        int i1 = t;
        ((GetServiceRequest)localObject2).<init>(i1);
        localObject3 = d;
        localObject3 = ((Context)localObject3).getPackageName();
        localObject2 = ((GetServiceRequest)localObject2).a((String)localObject3);
        localObject1 = ((GetServiceRequest)localObject2).a((Bundle)localObject1);
        if (paramSet != null) {
          ((GetServiceRequest)localObject1).a(paramSet);
        }
        bool = m();
        if (bool)
        {
          localObject2 = h();
          localObject2 = ((GetServiceRequest)localObject1).a((Account)localObject2);
          ((GetServiceRequest)localObject2).a(paraml);
          localObject2 = j;
          localObject3 = new com/google/android/gms/common/internal/h$d;
          AtomicInteger localAtomicInteger = b;
          int i2 = localAtomicInteger.get();
          ((h.d)localObject3).<init>(this, i2);
          ((n)localObject2).a((m)localObject3, (GetServiceRequest)localObject1);
          return;
        }
      }
      catch (DeadObjectException localDeadObjectException)
      {
        Object localObject1;
        boolean bool;
        String str = "GmsClient";
        localObject2 = "service died";
        Log.w(str, (String)localObject2);
        int i3 = 1;
        b(i3);
        continue;
      }
      catch (RemoteException localRemoteException)
      {
        Object localObject2 = "GmsClient";
        Object localObject3 = "Remote exception occurred";
        Log.w((String)localObject2, (String)localObject3, localRemoteException);
        continue;
      }
      bool = n();
      if (bool)
      {
        localObject2 = q;
        ((GetServiceRequest)localObject1).a((Account)localObject2);
      }
    }
  }
  
  protected abstract String b();
  
  public void b(int paramInt)
  {
    Handler localHandler = a;
    Object localObject = a;
    int i1 = b.get();
    localObject = ((Handler)localObject).obtainMessage(4, i1, paramInt);
    localHandler.sendMessage((Message)localObject);
  }
  
  protected final String c()
  {
    return e.c();
  }
  
  protected void c(int paramInt)
  {
    Handler localHandler = a;
    Object localObject = a;
    h.h localh = new com/google/android/gms/common/internal/h$h;
    localh.<init>(this);
    localObject = ((Handler)localObject).obtainMessage(6, paramInt, -1, localh);
    localHandler.sendMessage((Message)localObject);
  }
  
  protected void d() {}
  
  public void e()
  {
    Object localObject1 = h;
    Object localObject2 = d;
    int i1 = ((b)localObject1).a((Context)localObject2);
    if (i1 != 0)
    {
      int i2 = 1;
      b(i2, null);
      localObject2 = new com/google/android/gms/common/internal/h$f;
      ((h.f)localObject2).<init>(this);
      k = ((c.e)localObject2);
      localObject2 = a;
      Handler localHandler = a;
      int i3 = 3;
      AtomicInteger localAtomicInteger = b;
      int i4 = localAtomicInteger.get();
      localObject1 = localHandler.obtainMessage(i3, i4, i1);
      ((Handler)localObject2).sendMessage((Message)localObject1);
    }
    for (;;)
    {
      return;
      localObject1 = new com/google/android/gms/common/internal/h$f;
      ((h.f)localObject1).<init>(this);
      a((c.e)localObject1);
    }
  }
  
  public boolean f()
  {
    synchronized (i)
    {
      int i1 = o;
      int i3 = 3;
      if (i1 == i3)
      {
        i1 = 1;
        return i1;
      }
      int i2 = 0;
      Object localObject2 = null;
    }
  }
  
  public boolean g()
  {
    synchronized (i)
    {
      int i1 = o;
      int i3 = 2;
      if (i1 == i3)
      {
        i1 = 1;
        return i1;
      }
      int i2 = 0;
      Object localObject2 = null;
    }
  }
  
  public final Account h()
  {
    Account localAccount = q;
    if (localAccount != null) {
      localAccount = q;
    }
    for (;;)
    {
      return localAccount;
      localAccount = new android/accounts/Account;
      String str1 = "<<default account>>";
      String str2 = "com.google";
      localAccount.<init>(str1, str2);
    }
  }
  
  protected Bundle i()
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    return localBundle;
  }
  
  protected final void j()
  {
    boolean bool = f();
    if (!bool)
    {
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Not connected. Call connect() and wait for onConnected() to be called.");
      throw localIllegalStateException;
    }
  }
  
  public Bundle k()
  {
    return null;
  }
  
  public final IInterface l()
  {
    int i1;
    synchronized (i)
    {
      i1 = o;
      int i3 = 4;
      if (i1 == i3)
      {
        DeadObjectException localDeadObjectException = new android/os/DeadObjectException;
        localDeadObjectException.<init>();
        throw localDeadObjectException;
      }
    }
    j();
    IInterface localIInterface = l;
    if (localIInterface != null) {
      i1 = 1;
    }
    for (;;)
    {
      String str = "Client is connected but service is null";
      p.a(i1, str);
      localIInterface = l;
      return localIInterface;
      int i2 = 0;
      localIInterface = null;
    }
  }
  
  public boolean m()
  {
    return false;
  }
  
  public boolean n()
  {
    return false;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */