package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Collection;

public class GetServiceRequest
  implements SafeParcelable
{
  public static final Parcelable.Creator CREATOR;
  final int a;
  final int b;
  int c;
  String d;
  IBinder e;
  Scope[] f;
  Bundle g;
  Account h;
  
  static
  {
    g localg = new com/google/android/gms/common/internal/g;
    localg.<init>();
    CREATOR = localg;
  }
  
  public GetServiceRequest(int paramInt)
  {
    a = 2;
    int i = b.a;
    c = i;
    b = paramInt;
  }
  
  GetServiceRequest(int paramInt1, int paramInt2, int paramInt3, String paramString, IBinder paramIBinder, Scope[] paramArrayOfScope, Bundle paramBundle, Account paramAccount)
  {
    a = paramInt1;
    b = paramInt2;
    c = paramInt3;
    d = paramString;
    int i = 2;
    Account localAccount;
    if (paramInt1 < i) {
      localAccount = a(paramIBinder);
    }
    for (h = localAccount;; h = paramAccount)
    {
      f = paramArrayOfScope;
      g = paramBundle;
      return;
      e = paramIBinder;
    }
  }
  
  private Account a(IBinder paramIBinder)
  {
    Account localAccount = null;
    if (paramIBinder != null) {
      localAccount = a.a(l.a.a(paramIBinder));
    }
    return localAccount;
  }
  
  public GetServiceRequest a(Account paramAccount)
  {
    h = paramAccount;
    return this;
  }
  
  public GetServiceRequest a(Bundle paramBundle)
  {
    g = paramBundle;
    return this;
  }
  
  public GetServiceRequest a(l paraml)
  {
    if (paraml != null)
    {
      IBinder localIBinder = paraml.asBinder();
      e = localIBinder;
    }
    return this;
  }
  
  public GetServiceRequest a(String paramString)
  {
    d = paramString;
    return this;
  }
  
  public GetServiceRequest a(Collection paramCollection)
  {
    Scope[] arrayOfScope = new Scope[paramCollection.size()];
    arrayOfScope = (Scope[])paramCollection.toArray(arrayOfScope);
    f = arrayOfScope;
    return this;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    g.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/GetServiceRequest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */