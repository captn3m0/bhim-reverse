package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Intent;

final class j$a
{
  private final String a;
  private final ComponentName b;
  
  public j$a(String paramString)
  {
    String str = p.a(paramString);
    a = str;
    b = null;
  }
  
  public Intent a()
  {
    Object localObject1 = a;
    Object localObject2;
    if (localObject1 != null)
    {
      localObject1 = new android/content/Intent;
      localObject2 = a;
      ((Intent)localObject1).<init>((String)localObject2);
      localObject2 = "com.google.android.gms";
    }
    for (localObject1 = ((Intent)localObject1).setPackage((String)localObject2);; localObject1 = ((Intent)localObject1).setComponent((ComponentName)localObject2))
    {
      return (Intent)localObject1;
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>();
      localObject2 = b;
    }
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (this == paramObject) {}
    for (;;)
    {
      return bool1;
      boolean bool2 = paramObject instanceof a;
      if (!bool2)
      {
        bool1 = false;
      }
      else
      {
        paramObject = (a)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = o.a(localObject1, localObject2);
        if (bool2)
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = o.a(localObject1, localObject2);
          if (bool2) {}
        }
        else
        {
          bool1 = false;
        }
      }
    }
  }
  
  public int hashCode()
  {
    Object[] arrayOfObject = new Object[2];
    Object localObject = a;
    arrayOfObject[0] = localObject;
    localObject = b;
    arrayOfObject[1] = localObject;
    return o.a(arrayOfObject);
  }
  
  public String toString()
  {
    String str = a;
    if (str == null) {}
    for (str = b.flattenToString();; str = a) {
      return str;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/j$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */