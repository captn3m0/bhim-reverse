package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Handler.Callback;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import com.google.android.gms.common.stats.b;
import java.util.HashMap;

final class j
  extends i
  implements Handler.Callback
{
  private final HashMap a;
  private final Context b;
  private final Handler c;
  private final b d;
  private final long e;
  
  j(Context paramContext)
  {
    Object localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    a = ((HashMap)localObject);
    localObject = paramContext.getApplicationContext();
    b = ((Context)localObject);
    localObject = new android/os/Handler;
    Looper localLooper = paramContext.getMainLooper();
    ((Handler)localObject).<init>(localLooper, this);
    c = ((Handler)localObject);
    localObject = b.a();
    d = ((b)localObject);
    e = 5000L;
  }
  
  private boolean a(j.a parama, ServiceConnection paramServiceConnection, String paramString)
  {
    Object localObject1 = "ServiceConnection must not be null";
    p.a(paramServiceConnection, localObject1);
    for (;;)
    {
      Object localObject3;
      Object localObject4;
      synchronized (a)
      {
        localObject1 = a;
        localObject1 = ((HashMap)localObject1).get(parama);
        localObject1 = (j.b)localObject1;
        if (localObject1 == null)
        {
          localObject1 = new com/google/android/gms/common/internal/j$b;
          ((j.b)localObject1).<init>(this, parama);
          ((j.b)localObject1).a(paramServiceConnection, paramString);
          ((j.b)localObject1).a(paramString);
          localObject3 = a;
          ((HashMap)localObject3).put(parama, localObject1);
          boolean bool1 = ((j.b)localObject1).a();
          return bool1;
        }
        localObject3 = c;
        localObject4 = null;
        ((Handler)localObject3).removeMessages(0, localObject1);
        boolean bool2 = ((j.b)localObject1).a(paramServiceConnection);
        if (bool2)
        {
          localObject1 = new java/lang/IllegalStateException;
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          localObject4 = "Trying to bind a GmsServiceConnection that was already connected before.  config=";
          localObject3 = ((StringBuilder)localObject3).append((String)localObject4);
          localObject3 = ((StringBuilder)localObject3).append(parama);
          localObject3 = ((StringBuilder)localObject3).toString();
          ((IllegalStateException)localObject1).<init>((String)localObject3);
          throw ((Throwable)localObject1);
        }
      }
      ((j.b)localObject2).a(paramServiceConnection, paramString);
      int i = ((j.b)localObject2).b();
      switch (i)
      {
      default: 
        break;
      case 1: 
        localObject3 = ((j.b)localObject2).e();
        localObject4 = ((j.b)localObject2).d();
        paramServiceConnection.onServiceConnected((ComponentName)localObject3, (IBinder)localObject4);
        break;
      case 2: 
        ((j.b)localObject2).a(paramString);
      }
    }
  }
  
  private void b(j.a parama, ServiceConnection paramServiceConnection, String paramString)
  {
    Object localObject1 = "ServiceConnection must not be null";
    p.a(paramServiceConnection, localObject1);
    Object localObject4;
    String str;
    synchronized (a)
    {
      localObject1 = a;
      localObject1 = ((HashMap)localObject1).get(parama);
      localObject1 = (j.b)localObject1;
      if (localObject1 == null)
      {
        localObject1 = new java/lang/IllegalStateException;
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        str = "Nonexistent connection status for service config: ";
        localObject4 = ((StringBuilder)localObject4).append(str);
        localObject4 = ((StringBuilder)localObject4).append(parama);
        localObject4 = ((StringBuilder)localObject4).toString();
        ((IllegalStateException)localObject1).<init>((String)localObject4);
        throw ((Throwable)localObject1);
      }
    }
    boolean bool = ((j.b)localObject2).a(paramServiceConnection);
    Object localObject3;
    if (!bool)
    {
      localObject3 = new java/lang/IllegalStateException;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      str = "Trying to unbind a GmsServiceConnection  that was not bound before.  config=";
      localObject4 = ((StringBuilder)localObject4).append(str);
      localObject4 = ((StringBuilder)localObject4).append(parama);
      localObject4 = ((StringBuilder)localObject4).toString();
      ((IllegalStateException)localObject3).<init>((String)localObject4);
      throw ((Throwable)localObject3);
    }
    ((j.b)localObject3).b(paramServiceConnection, paramString);
    bool = ((j.b)localObject3).c();
    if (bool)
    {
      localObject4 = c;
      str = null;
      localObject3 = ((Handler)localObject4).obtainMessage(0, localObject3);
      localObject4 = c;
      long l = e;
      ((Handler)localObject4).sendMessageDelayed((Message)localObject3, l);
    }
  }
  
  public boolean a(String paramString1, ServiceConnection paramServiceConnection, String paramString2)
  {
    j.a locala = new com/google/android/gms/common/internal/j$a;
    locala.<init>(paramString1);
    return a(locala, paramServiceConnection, paramString2);
  }
  
  public void b(String paramString1, ServiceConnection paramServiceConnection, String paramString2)
  {
    j.a locala = new com/google/android/gms/common/internal/j$a;
    locala.<init>(paramString1);
    b(locala, paramServiceConnection, paramString2);
  }
  
  public boolean handleMessage(Message paramMessage)
  {
    int i = what;
    Object localObject1;
    switch (i)
    {
    default: 
      i = 0;
      localObject1 = null;
    }
    for (;;)
    {
      return i;
      localObject1 = (j.b)obj;
      synchronized (a)
      {
        boolean bool = ((j.b)localObject1).c();
        if (bool)
        {
          bool = ((j.b)localObject1).a();
          if (bool)
          {
            localObject3 = "GmsClientSupervisor";
            ((j.b)localObject1).b((String)localObject3);
          }
          Object localObject3 = a;
          localObject1 = j.b.a((j.b)localObject1);
          ((HashMap)localObject3).remove(localObject1);
        }
        int j = 1;
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/j.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */