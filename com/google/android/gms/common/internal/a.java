package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesUtil;

public class a
  extends l.a
{
  int a;
  private Account b;
  private Context c;
  
  public static Account a(l paraml)
  {
    Account localAccount = null;
    if (paraml != null) {
      l = Binder.clearCallingIdentity();
    }
    try
    {
      localAccount = paraml.a();
    }
    catch (RemoteException localRemoteException)
    {
      for (;;)
      {
        String str1 = "AccountAccessor";
        String str2 = "Remote account accessor probably died";
        Log.w(str1, str2);
        Binder.restoreCallingIdentity(l);
      }
    }
    finally
    {
      Binder.restoreCallingIdentity(l);
    }
    return localAccount;
  }
  
  public Account a()
  {
    int i = Binder.getCallingUid();
    int j = a;
    if (i == j) {}
    for (Object localObject = b;; localObject = b)
    {
      return (Account)localObject;
      Context localContext = c;
      boolean bool = GooglePlayServicesUtil.zze(localContext, i);
      if (!bool) {
        break;
      }
      a = i;
    }
    localObject = new java/lang/SecurityException;
    ((SecurityException)localObject).<init>("Caller is not GooglePlayServices");
    throw ((Throwable)localObject);
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (this == paramObject) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = paramObject instanceof a;
      Account localAccount1;
      if (!bool)
      {
        bool = false;
        localAccount1 = null;
      }
      else
      {
        localAccount1 = b;
        paramObject = (a)paramObject;
        Account localAccount2 = b;
        bool = localAccount1.equals(localAccount2);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */