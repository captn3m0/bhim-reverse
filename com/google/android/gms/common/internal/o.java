package com.google.android.gms.common.internal;

import java.util.Arrays;

public final class o
{
  public static int a(Object... paramVarArgs)
  {
    return Arrays.hashCode(paramVarArgs);
  }
  
  public static o.a a(Object paramObject)
  {
    o.a locala = new com/google/android/gms/common/internal/o$a;
    locala.<init>(paramObject, null);
    return locala;
  }
  
  public static boolean a(Object paramObject1, Object paramObject2)
  {
    if (paramObject1 != paramObject2)
    {
      if (paramObject1 == null) {
        break label23;
      }
      bool = paramObject1.equals(paramObject2);
      if (!bool) {
        break label23;
      }
    }
    label23:
    for (boolean bool = true;; bool = false) {
      return bool;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/o.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */