package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.ServiceConnection;

public abstract class i
{
  private static final Object a;
  private static i b;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
  }
  
  public static i a(Context paramContext)
  {
    synchronized (a)
    {
      Object localObject2 = b;
      if (localObject2 == null)
      {
        localObject2 = new com/google/android/gms/common/internal/j;
        Context localContext = paramContext.getApplicationContext();
        ((j)localObject2).<init>(localContext);
        b = (i)localObject2;
      }
      return b;
    }
  }
  
  public abstract boolean a(String paramString1, ServiceConnection paramServiceConnection, String paramString2);
  
  public abstract void b(String paramString1, ServiceConnection paramServiceConnection, String paramString2);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */