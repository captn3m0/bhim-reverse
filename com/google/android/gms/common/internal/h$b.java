package com.google.android.gms.common.internal;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.c.b;
import com.google.android.gms.common.api.c.e;
import java.util.concurrent.atomic.AtomicInteger;

final class h$b
  extends Handler
{
  public h$b(h paramh, Looper paramLooper)
  {
    super(paramLooper);
  }
  
  private void a(Message paramMessage)
  {
    h.c localc = (h.c)obj;
    localc.b();
    localc.d();
  }
  
  private boolean b(Message paramMessage)
  {
    int i = 1;
    int j = what;
    int m = 2;
    if (j != m)
    {
      j = what;
      if (j != i)
      {
        int k = what;
        int n = 5;
        if (k != n)
        {
          k = what;
          n = 6;
          if (k != n) {
            break label57;
          }
        }
      }
    }
    for (;;)
    {
      return i;
      label57:
      i = 0;
    }
  }
  
  public void handleMessage(Message paramMessage)
  {
    int i = 1;
    int j = 4;
    Object localObject1 = a.b;
    int k = ((AtomicInteger)localObject1).get();
    int i1 = arg1;
    if (k != i1)
    {
      boolean bool1 = b(paramMessage);
      if (bool1) {
        a(paramMessage);
      }
    }
    for (;;)
    {
      return;
      int m = what;
      if (m != i)
      {
        m = what;
        i1 = 5;
        if (m != i1)
        {
          m = what;
          i1 = 6;
          if (m != i1) {
            break label122;
          }
        }
      }
      localObject1 = a;
      boolean bool2 = ((h)localObject1).g();
      if (!bool2)
      {
        a(paramMessage);
      }
      else
      {
        label122:
        int n = what;
        i1 = 3;
        Object localObject2;
        if (n == i1)
        {
          localObject1 = new com/google/android/gms/common/ConnectionResult;
          i1 = arg2;
          ((ConnectionResult)localObject1).<init>(i1, null);
          h.a(a).a((ConnectionResult)localObject1);
          localObject2 = a;
          ((h)localObject2).a((ConnectionResult)localObject1);
        }
        else
        {
          n = what;
          if (n == j)
          {
            h.a(a, j, null);
            localObject1 = h.b(a);
            if (localObject1 != null)
            {
              localObject1 = h.b(a);
              i1 = arg2;
              ((c.b)localObject1).a(i1);
            }
            localObject1 = a;
            i1 = arg2;
            ((h)localObject1).a(i1);
            localObject1 = a;
            h.a((h)localObject1, j, i, null);
          }
          else
          {
            n = what;
            i1 = 2;
            if (n == i1)
            {
              localObject1 = a;
              bool3 = ((h)localObject1).f();
              if (!bool3)
              {
                a(paramMessage);
                continue;
              }
            }
            boolean bool3 = b(paramMessage);
            if (bool3)
            {
              localObject1 = (h.c)obj;
              ((h.c)localObject1).c();
            }
            else
            {
              localObject1 = "GmsClient";
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              localObject2 = ((StringBuilder)localObject2).append("Don't know how to handle message: ");
              j = what;
              localObject2 = j;
              Exception localException = new java/lang/Exception;
              localException.<init>();
              Log.wtf((String)localObject1, (String)localObject2, localException);
            }
          }
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/h$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */