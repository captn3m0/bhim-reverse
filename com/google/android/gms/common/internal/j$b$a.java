package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.Iterator;
import java.util.Set;

public class j$b$a
  implements ServiceConnection
{
  public j$b$a(j.b paramb) {}
  
  public void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder)
  {
    Object localObject1 = a.a;
    synchronized (j.a((j)localObject1))
    {
      localObject1 = a;
      j.b.a((j.b)localObject1, paramIBinder);
      localObject1 = a;
      j.b.a((j.b)localObject1, paramComponentName);
      localObject1 = a;
      localObject1 = j.b.b((j.b)localObject1);
      Iterator localIterator = ((Set)localObject1).iterator();
      boolean bool = localIterator.hasNext();
      if (bool)
      {
        localObject1 = localIterator.next();
        localObject1 = (ServiceConnection)localObject1;
        ((ServiceConnection)localObject1).onServiceConnected(paramComponentName, paramIBinder);
      }
    }
    j.b localb = a;
    int i = 1;
    j.b.a(localb, i);
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    Object localObject1 = a.a;
    synchronized (j.a((j)localObject1))
    {
      localObject1 = a;
      i = 0;
      Iterator localIterator = null;
      j.b.a((j.b)localObject1, null);
      localObject1 = a;
      j.b.a((j.b)localObject1, paramComponentName);
      localObject1 = a;
      localObject1 = j.b.b((j.b)localObject1);
      localIterator = ((Set)localObject1).iterator();
      boolean bool = localIterator.hasNext();
      if (bool)
      {
        localObject1 = localIterator.next();
        localObject1 = (ServiceConnection)localObject1;
        ((ServiceConnection)localObject1).onServiceDisconnected(paramComponentName);
      }
    }
    j.b localb = a;
    int i = 2;
    j.b.a(localb, i);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/internal/j$b$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */