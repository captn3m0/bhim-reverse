package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class a
  implements Parcelable.Creator
{
  static void a(DataHolder paramDataHolder, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.a.a(paramParcel);
    Object localObject = paramDataHolder.c();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 1, (String[])localObject, false);
    int j = paramDataHolder.b();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 1000, j);
    localObject = paramDataHolder.d();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 2, (Parcelable[])localObject, paramInt, false);
    j = paramDataHolder.e();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 3, j);
    localObject = paramDataHolder.f();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 4, (Bundle)localObject, false);
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, i);
  }
  
  public DataHolder a(Parcel paramParcel)
  {
    int i = 0;
    Bundle localBundle = null;
    int j = zza.b(paramParcel);
    Object localObject1 = null;
    String[] arrayOfString = null;
    int k = 0;
    Object localObject2 = null;
    for (;;)
    {
      m = paramParcel.dataPosition();
      if (m >= j) {
        break;
      }
      m = zza.a(paramParcel);
      int n = zza.a(m);
      switch (n)
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        arrayOfString = zza.k(paramParcel, m);
        break;
      case 1000: 
        k = zza.d(paramParcel, m);
        break;
      case 2: 
        localObject1 = CursorWindow.CREATOR;
        localObject3 = (CursorWindow[])zza.b(paramParcel, m, (Parcelable.Creator)localObject1);
        localObject1 = localObject3;
        break;
      case 3: 
        i = zza.d(paramParcel, m);
        break;
      case 4: 
        localBundle = zza.j(paramParcel, m);
      }
    }
    int m = paramParcel.dataPosition();
    if (m != j)
    {
      localObject3 = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Overread allowed size end=" + j;
      ((zza.zza)localObject3).<init>((String)localObject2, paramParcel);
      throw ((Throwable)localObject3);
    }
    Object localObject3 = new com/google/android/gms/common/data/DataHolder;
    ((DataHolder)localObject3).<init>(k, arrayOfString, (CursorWindow[])localObject1, i, localBundle);
    ((DataHolder)localObject3).a();
    return (DataHolder)localObject3;
  }
  
  public DataHolder[] a(int paramInt)
  {
    return new DataHolder[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/data/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */