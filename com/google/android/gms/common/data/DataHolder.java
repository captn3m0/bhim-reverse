package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class DataHolder
  implements SafeParcelable
{
  public static final a CREATOR;
  private static final DataHolder.a l;
  Bundle a;
  int[] b;
  int c;
  boolean d = false;
  private final int e;
  private final String[] f;
  private final CursorWindow[] g;
  private final int h;
  private final Bundle i;
  private Object j;
  private boolean k = true;
  
  static
  {
    Object localObject = new com/google/android/gms/common/data/a;
    ((a)localObject).<init>();
    CREATOR = (a)localObject;
    localObject = new com/google/android/gms/common/data/DataHolder$1;
    String[] arrayOfString = new String[0];
    ((DataHolder.1)localObject).<init>(arrayOfString, null);
    l = (DataHolder.a)localObject;
  }
  
  DataHolder(int paramInt1, String[] paramArrayOfString, CursorWindow[] paramArrayOfCursorWindow, int paramInt2, Bundle paramBundle)
  {
    e = paramInt1;
    f = paramArrayOfString;
    g = paramArrayOfCursorWindow;
    h = paramInt2;
    i = paramBundle;
  }
  
  public void a()
  {
    int m = 0;
    Object localObject1 = new android/os/Bundle;
    ((Bundle)localObject1).<init>();
    a = ((Bundle)localObject1);
    int n = 0;
    localObject1 = null;
    Object localObject2;
    int i1;
    Object localObject3;
    for (;;)
    {
      localObject2 = f;
      i1 = localObject2.length;
      if (n >= i1) {
        break;
      }
      localObject2 = a;
      localObject3 = f[n];
      ((Bundle)localObject2).putInt((String)localObject3, n);
      n += 1;
    }
    localObject1 = new int[g.length];
    b = ((int[])localObject1);
    n = 0;
    localObject1 = null;
    for (;;)
    {
      localObject2 = g;
      i1 = localObject2.length;
      if (m >= i1) {
        break;
      }
      b[m] = n;
      localObject2 = g[m];
      i1 = ((CursorWindow)localObject2).getStartPosition();
      i1 = n - i1;
      localObject3 = g[m];
      int i2 = ((CursorWindow)localObject3).getNumRows();
      i1 = i2 - i1;
      n += i1;
      m += 1;
    }
    c = n;
  }
  
  int b()
  {
    return e;
  }
  
  String[] c()
  {
    return f;
  }
  
  CursorWindow[] d()
  {
    return g;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return h;
  }
  
  public Bundle f()
  {
    return i;
  }
  
  /* Error */
  protected void finalize()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 51	com/google/android/gms/common/data/DataHolder:k	Z
    //   4: istore_1
    //   5: iload_1
    //   6: ifeq +125 -> 131
    //   9: aload_0
    //   10: getfield 57	com/google/android/gms/common/data/DataHolder:g	[Landroid/database/CursorWindow;
    //   13: astore_2
    //   14: aload_2
    //   15: arraylength
    //   16: istore_1
    //   17: iload_1
    //   18: ifle +113 -> 131
    //   21: aload_0
    //   22: invokevirtual 86	com/google/android/gms/common/data/DataHolder:g	()Z
    //   25: istore_1
    //   26: iload_1
    //   27: ifne +104 -> 131
    //   30: aload_0
    //   31: getfield 88	com/google/android/gms/common/data/DataHolder:j	Ljava/lang/Object;
    //   34: astore_2
    //   35: aload_2
    //   36: ifnonnull +100 -> 136
    //   39: new 90	java/lang/StringBuilder
    //   42: astore_2
    //   43: aload_2
    //   44: invokespecial 91	java/lang/StringBuilder:<init>	()V
    //   47: ldc 93
    //   49: astore_3
    //   50: aload_2
    //   51: aload_3
    //   52: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: astore_2
    //   56: aload_0
    //   57: invokevirtual 101	java/lang/Object:toString	()Ljava/lang/String;
    //   60: astore_3
    //   61: aload_2
    //   62: aload_3
    //   63: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: astore_2
    //   67: aload_2
    //   68: invokevirtual 102	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   71: astore_2
    //   72: ldc 104
    //   74: astore_3
    //   75: new 90	java/lang/StringBuilder
    //   78: astore 4
    //   80: aload 4
    //   82: invokespecial 91	java/lang/StringBuilder:<init>	()V
    //   85: ldc 106
    //   87: astore 5
    //   89: aload 4
    //   91: aload 5
    //   93: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: astore 4
    //   98: aload 4
    //   100: aload_2
    //   101: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: astore_2
    //   105: ldc 108
    //   107: astore 4
    //   109: aload_2
    //   110: aload 4
    //   112: invokevirtual 97	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: astore_2
    //   116: aload_2
    //   117: invokevirtual 102	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   120: astore_2
    //   121: aload_3
    //   122: aload_2
    //   123: invokestatic 113	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   126: pop
    //   127: aload_0
    //   128: invokevirtual 115	com/google/android/gms/common/data/DataHolder:h	()V
    //   131: aload_0
    //   132: invokespecial 118	java/lang/Object:finalize	()V
    //   135: return
    //   136: aload_0
    //   137: getfield 88	com/google/android/gms/common/data/DataHolder:j	Ljava/lang/Object;
    //   140: astore_2
    //   141: aload_2
    //   142: invokevirtual 101	java/lang/Object:toString	()Ljava/lang/String;
    //   145: astore_2
    //   146: goto -74 -> 72
    //   149: astore_2
    //   150: aload_0
    //   151: invokespecial 118	java/lang/Object:finalize	()V
    //   154: aload_2
    //   155: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	156	0	this	DataHolder
    //   4	2	1	bool1	boolean
    //   16	2	1	m	int
    //   25	2	1	bool2	boolean
    //   13	133	2	localObject1	Object
    //   149	6	2	localObject2	Object
    //   49	73	3	str1	String
    //   78	33	4	localObject3	Object
    //   87	5	5	str2	String
    // Exception table:
    //   from	to	target	type
    //   0	4	149	finally
    //   9	13	149	finally
    //   14	16	149	finally
    //   21	25	149	finally
    //   30	34	149	finally
    //   39	42	149	finally
    //   43	47	149	finally
    //   51	55	149	finally
    //   56	60	149	finally
    //   62	66	149	finally
    //   67	71	149	finally
    //   75	78	149	finally
    //   80	85	149	finally
    //   91	96	149	finally
    //   100	104	149	finally
    //   110	115	149	finally
    //   116	120	149	finally
    //   122	127	149	finally
    //   127	131	149	finally
    //   136	140	149	finally
    //   141	145	149	finally
  }
  
  public boolean g()
  {
    try
    {
      boolean bool = d;
      return bool;
    }
    finally {}
  }
  
  public void h()
  {
    try
    {
      int m = d;
      if (m == 0)
      {
        m = 1;
        d = m;
        m = 0;
        Object localObject1 = null;
        for (;;)
        {
          Object localObject3 = g;
          int i1 = localObject3.length;
          if (m >= i1) {
            break;
          }
          localObject3 = g;
          localObject3 = localObject3[m];
          ((CursorWindow)localObject3).close();
          int n;
          m += 1;
        }
      }
      return;
    }
    finally {}
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    a.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/data/DataHolder.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */