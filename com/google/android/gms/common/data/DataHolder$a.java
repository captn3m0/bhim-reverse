package com.google.android.gms.common.data;

import com.google.android.gms.common.internal.p;
import java.util.ArrayList;
import java.util.HashMap;

public class DataHolder$a
{
  private final String[] a;
  private final ArrayList b;
  private final String c;
  private final HashMap d;
  private boolean e;
  private String f;
  
  private DataHolder$a(String[] paramArrayOfString, String paramString)
  {
    Object localObject = (String[])p.a(paramArrayOfString);
    a = ((String[])localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    b = ((ArrayList)localObject);
    c = paramString;
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    d = ((HashMap)localObject);
    e = false;
    f = null;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/data/DataHolder$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */