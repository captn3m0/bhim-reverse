package com.google.android.gms.common;

import java.lang.ref.WeakReference;

abstract class f$bc
  extends f.a
{
  private static final WeakReference b;
  private WeakReference a;
  
  static
  {
    WeakReference localWeakReference = new java/lang/ref/WeakReference;
    localWeakReference.<init>(null);
    b = localWeakReference;
  }
  
  f$bc(byte[] paramArrayOfByte)
  {
    super(paramArrayOfByte);
    WeakReference localWeakReference = b;
    a = localWeakReference;
  }
  
  byte[] a()
  {
    try
    {
      Object localObject1 = a;
      localObject1 = ((WeakReference)localObject1).get();
      localObject1 = (byte[])localObject1;
      if (localObject1 == null)
      {
        localObject1 = b();
        WeakReference localWeakReference = new java/lang/ref/WeakReference;
        localWeakReference.<init>(localObject1);
        a = localWeakReference;
      }
      return (byte[])localObject1;
    }
    finally {}
  }
  
  protected abstract byte[] b();
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/f$bc.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */