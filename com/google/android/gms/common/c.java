package com.google.android.gms.common;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.Bundle;
import android.support.v4.app.k;

public class c
  extends k
{
  private Dialog ai = null;
  private DialogInterface.OnCancelListener aj = null;
  
  public static c a(Dialog paramDialog, DialogInterface.OnCancelListener paramOnCancelListener)
  {
    c localc = new com/google/android/gms/common/c;
    localc.<init>();
    Dialog localDialog = (Dialog)com.google.android.gms.common.internal.p.a(paramDialog, "Cannot display null dialog");
    localDialog.setOnCancelListener(null);
    localDialog.setOnDismissListener(null);
    ai = localDialog;
    if (paramOnCancelListener != null) {
      aj = paramOnCancelListener;
    }
    return localc;
  }
  
  public void a(android.support.v4.app.p paramp, String paramString)
  {
    super.a(paramp, paramString);
  }
  
  public Dialog c(Bundle paramBundle)
  {
    Dialog localDialog = ai;
    if (localDialog == null)
    {
      localDialog = null;
      b(false);
    }
    return ai;
  }
  
  public void onCancel(DialogInterface paramDialogInterface)
  {
    DialogInterface.OnCancelListener localOnCancelListener = aj;
    if (localOnCancelListener != null)
    {
      localOnCancelListener = aj;
      localOnCancelListener.onCancel(paramDialogInterface);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */