package com.google.android.gms.common;

import com.google.android.gms.common.internal.p;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

abstract class f$a
{
  private int a;
  
  protected f$a(byte[] paramArrayOfByte)
  {
    int i = paramArrayOfByte.length;
    int k = 25;
    if (i == k) {}
    int j;
    for (i = 1;; j = 0)
    {
      p.b(i, "cert hash data has incorrect length");
      j = Arrays.hashCode(paramArrayOfByte);
      a = j;
      return;
    }
  }
  
  protected static byte[] a(String paramString)
  {
    String str = "ISO-8859-1";
    try
    {
      return paramString.getBytes(str);
    }
    catch (UnsupportedEncodingException localUnsupportedEncodingException)
    {
      AssertionError localAssertionError = new java/lang/AssertionError;
      localAssertionError.<init>(localUnsupportedEncodingException);
      throw localAssertionError;
    }
  }
  
  abstract byte[] a();
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    byte[] arrayOfByte1;
    if (paramObject != null)
    {
      bool = paramObject instanceof a;
      if (bool) {}
    }
    else
    {
      bool = false;
      arrayOfByte1 = null;
    }
    for (;;)
    {
      return bool;
      paramObject = (a)paramObject;
      arrayOfByte1 = a();
      byte[] arrayOfByte2 = ((a)paramObject).a();
      bool = Arrays.equals(arrayOfByte1, arrayOfByte2);
    }
  }
  
  public int hashCode()
  {
    return a;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/f$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */