package com.google.android.gms.common;

import android.content.Intent;

public class UserRecoverableException
  extends Exception
{
  private final Intent a;
  
  public UserRecoverableException(String paramString, Intent paramIntent)
  {
    super(paramString);
    a = paramIntent;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/UserRecoverableException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */