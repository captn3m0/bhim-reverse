package com.google.android.gms.common;

public final class GooglePlayServicesNotAvailableException
  extends Exception
{
  public final int a;
  
  public GooglePlayServicesNotAvailableException(int paramInt)
  {
    a = paramInt;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/GooglePlayServicesNotAvailableException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */