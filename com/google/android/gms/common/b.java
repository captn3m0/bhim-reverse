package com.google.android.gms.common;

import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.TextUtils;
import com.google.android.gms.common.internal.k;

public class b
{
  public static final int a = GooglePlayServicesUtil.GOOGLE_PLAY_SERVICES_VERSION_CODE;
  private static final b b;
  
  static
  {
    b localb = new com/google/android/gms/common/b;
    localb.<init>();
    b = localb;
  }
  
  public static b a()
  {
    return b;
  }
  
  private String a(Context paramContext, String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder.append("gcore_");
    int i = a;
    localStringBuilder.append(i);
    Object localObject = "-";
    localStringBuilder.append((String)localObject);
    boolean bool = TextUtils.isEmpty(paramString);
    if (!bool) {
      localStringBuilder.append(paramString);
    }
    localObject = "-";
    localStringBuilder.append((String)localObject);
    if (paramContext != null)
    {
      localObject = paramContext.getPackageName();
      localStringBuilder.append((String)localObject);
    }
    localObject = "-";
    localStringBuilder.append((String)localObject);
    if (paramContext != null) {}
    try
    {
      localObject = paramContext.getPackageManager();
      String str = paramContext.getPackageName();
      localObject = ((PackageManager)localObject).getPackageInfo(str, 0);
      int j = versionCode;
      localStringBuilder.append(j);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;) {}
    }
    return localStringBuilder.toString();
  }
  
  public int a(Context paramContext)
  {
    int i = GooglePlayServicesUtil.isGooglePlayServicesAvailable(paramContext);
    boolean bool = GooglePlayServicesUtil.zzd(paramContext, i);
    if (bool) {
      i = 18;
    }
    return i;
  }
  
  public Dialog a(Activity paramActivity, int paramInt1, int paramInt2)
  {
    return GooglePlayServicesUtil.getErrorDialog(paramInt1, paramActivity, paramInt2);
  }
  
  public PendingIntent a(Context paramContext, int paramInt1, int paramInt2)
  {
    return a(paramContext, paramInt1, paramInt2, null);
  }
  
  public PendingIntent a(Context paramContext, int paramInt1, int paramInt2, String paramString)
  {
    Object localObject = a(paramContext, paramInt1, paramString);
    if (localObject == null) {}
    int i;
    for (localObject = null;; localObject = PendingIntent.getActivity(paramContext, paramInt2, (Intent)localObject, i))
    {
      return (PendingIntent)localObject;
      i = 268435456;
    }
  }
  
  public Intent a(Context paramContext, int paramInt, String paramString)
  {
    Intent localIntent;
    switch (paramInt)
    {
    default: 
      localIntent = null;
    }
    for (;;)
    {
      return localIntent;
      String str = a(paramContext, paramString);
      localIntent = k.a("com.google.android.gms", str);
      continue;
      localIntent = k.a();
      continue;
      localIntent = k.a("com.google.android.gms");
    }
  }
  
  public final boolean a(int paramInt)
  {
    return GooglePlayServicesUtil.isUserRecoverableError(paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */