package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class e
  implements Parcelable.Creator
{
  static void a(ConnectionResult paramConnectionResult, Parcel paramParcel, int paramInt)
  {
    int i = a.a(paramParcel);
    int j = b;
    a.a(paramParcel, 1, j);
    j = paramConnectionResult.b();
    a.a(paramParcel, 2, j);
    Object localObject = paramConnectionResult.c();
    a.a(paramParcel, 3, (Parcelable)localObject, paramInt, false);
    localObject = paramConnectionResult.d();
    a.a(paramParcel, 4, (String)localObject, false);
    a.a(paramParcel, i);
  }
  
  public ConnectionResult a(Parcel paramParcel)
  {
    Object localObject1 = null;
    int i = 0;
    int j = zza.b(paramParcel);
    int k = 0;
    Object localObject2 = null;
    int m = 0;
    int n = paramParcel.dataPosition();
    if (n < j)
    {
      n = zza.a(paramParcel);
      int i1 = zza.a(n);
      switch (i1)
      {
      default: 
        zza.b(paramParcel, n);
        localObject3 = localObject1;
        localObject1 = localObject2;
        k = i;
        i = m;
      }
      for (;;)
      {
        m = i;
        i = k;
        localObject2 = localObject1;
        localObject1 = localObject3;
        break;
        n = zza.d(paramParcel, n);
        Object localObject4 = localObject1;
        localObject1 = localObject2;
        k = i;
        i = n;
        localObject3 = localObject4;
        continue;
        n = zza.d(paramParcel, n);
        i = m;
        localObject4 = localObject2;
        k = n;
        localObject3 = localObject1;
        localObject1 = localObject2;
        continue;
        localObject2 = PendingIntent.CREATOR;
        localObject3 = (PendingIntent)zza.a(paramParcel, n, (Parcelable.Creator)localObject2);
        k = i;
        i = m;
        localObject4 = localObject1;
        localObject1 = localObject3;
        localObject3 = localObject4;
        continue;
        localObject3 = zza.h(paramParcel, n);
        localObject1 = localObject2;
        k = i;
        i = m;
      }
    }
    n = paramParcel.dataPosition();
    if (n != j)
    {
      localObject3 = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = "Overread allowed size end=" + j;
      ((zza.zza)localObject3).<init>((String)localObject1, paramParcel);
      throw ((Throwable)localObject3);
    }
    Object localObject3 = new com/google/android/gms/common/ConnectionResult;
    ((ConnectionResult)localObject3).<init>(m, i, (PendingIntent)localObject2, (String)localObject1);
    return (ConnectionResult)localObject3;
  }
  
  public ConnectionResult[] a(int paramInt)
  {
    return new ConnectionResult[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */