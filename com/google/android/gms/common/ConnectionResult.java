package com.google.android.gms.common;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.o;
import com.google.android.gms.common.internal.o.a;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class ConnectionResult
  implements SafeParcelable
{
  public static final Parcelable.Creator CREATOR;
  public static final ConnectionResult a;
  final int b;
  private final int c;
  private final PendingIntent d;
  private final String e;
  
  static
  {
    Object localObject = new com/google/android/gms/common/ConnectionResult;
    ((ConnectionResult)localObject).<init>(0);
    a = (ConnectionResult)localObject;
    localObject = new com/google/android/gms/common/e;
    ((e)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public ConnectionResult(int paramInt)
  {
    this(paramInt, null, null);
  }
  
  ConnectionResult(int paramInt1, int paramInt2, PendingIntent paramPendingIntent, String paramString)
  {
    b = paramInt1;
    c = paramInt2;
    d = paramPendingIntent;
    e = paramString;
  }
  
  public ConnectionResult(int paramInt, PendingIntent paramPendingIntent)
  {
    this(paramInt, paramPendingIntent, null);
  }
  
  public ConnectionResult(int paramInt, PendingIntent paramPendingIntent, String paramString)
  {
    this(1, paramInt, paramPendingIntent, paramString);
  }
  
  static String a(int paramInt)
  {
    Object localObject;
    switch (paramInt)
    {
    case 12: 
    default: 
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = ((StringBuilder)localObject).append("UNKNOWN_ERROR_CODE(").append(paramInt);
      String str = ")";
      localObject = str;
    }
    for (;;)
    {
      return (String)localObject;
      localObject = "SUCCESS";
      continue;
      localObject = "SERVICE_MISSING";
      continue;
      localObject = "SERVICE_VERSION_UPDATE_REQUIRED";
      continue;
      localObject = "SERVICE_DISABLED";
      continue;
      localObject = "SIGN_IN_REQUIRED";
      continue;
      localObject = "INVALID_ACCOUNT";
      continue;
      localObject = "RESOLUTION_REQUIRED";
      continue;
      localObject = "NETWORK_ERROR";
      continue;
      localObject = "INTERNAL_ERROR";
      continue;
      localObject = "SERVICE_INVALID";
      continue;
      localObject = "DEVELOPER_ERROR";
      continue;
      localObject = "LICENSE_CHECK_FAILED";
      continue;
      localObject = "CANCELED";
      continue;
      localObject = "TIMEOUT";
      continue;
      localObject = "INTERRUPTED";
      continue;
      localObject = "API_UNAVAILABLE";
      continue;
      localObject = "SIGN_IN_FAILED";
      continue;
      localObject = "SERVICE_UPDATING";
      continue;
      localObject = "SERVICE_MISSING_PERMISSION";
    }
  }
  
  public boolean a()
  {
    int i = c;
    if (i == 0) {}
    int j;
    for (i = 1;; j = 0) {
      return i;
    }
  }
  
  public int b()
  {
    return c;
  }
  
  public PendingIntent c()
  {
    return d;
  }
  
  public String d()
  {
    return e;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {}
    for (;;)
    {
      return bool1;
      boolean bool2 = paramObject instanceof ConnectionResult;
      if (!bool2)
      {
        bool1 = false;
      }
      else
      {
        paramObject = (ConnectionResult)paramObject;
        int i = c;
        int j = c;
        if (i == j)
        {
          Object localObject1 = d;
          Object localObject2 = d;
          boolean bool3 = o.a(localObject1, localObject2);
          if (bool3)
          {
            localObject1 = e;
            localObject2 = e;
            bool3 = o.a(localObject1, localObject2);
            if (bool3) {
              continue;
            }
          }
        }
        bool1 = false;
      }
    }
  }
  
  public int hashCode()
  {
    Object[] arrayOfObject = new Object[3];
    Object localObject = Integer.valueOf(c);
    arrayOfObject[0] = localObject;
    localObject = d;
    arrayOfObject[1] = localObject;
    localObject = e;
    arrayOfObject[2] = localObject;
    return o.a(arrayOfObject);
  }
  
  public String toString()
  {
    o.a locala = o.a(this);
    Object localObject = a(c);
    locala = locala.a("statusCode", localObject);
    localObject = d;
    locala = locala.a("resolution", localObject);
    localObject = e;
    return locala.a("message", localObject).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    e.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/ConnectionResult.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */