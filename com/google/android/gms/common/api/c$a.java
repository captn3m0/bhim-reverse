package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Looper;
import android.support.v4.e.a;
import android.view.View;
import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.d;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.k;
import com.google.android.gms.internal.m;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class c$a
{
  private Account a;
  private final Set b;
  private final Set c;
  private int d;
  private View e;
  private String f;
  private String g;
  private final Map h;
  private final Context i;
  private final Map j;
  private int k;
  private Looper l;
  private b m;
  private a.a n;
  private final ArrayList o;
  private final ArrayList p;
  private m q;
  
  public c$a(Context paramContext)
  {
    Object localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    b = ((Set)localObject);
    localObject = new java/util/HashSet;
    ((HashSet)localObject).<init>();
    c = ((Set)localObject);
    localObject = new android/support/v4/e/a;
    ((a)localObject).<init>();
    h = ((Map)localObject);
    localObject = new android/support/v4/e/a;
    ((a)localObject).<init>();
    j = ((Map)localObject);
    k = -1;
    localObject = b.a();
    m = ((b)localObject);
    localObject = k.c;
    n = ((a.a)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    o = ((ArrayList)localObject);
    localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    p = ((ArrayList)localObject);
    i = paramContext;
    localObject = paramContext.getMainLooper();
    l = ((Looper)localObject);
    localObject = paramContext.getPackageName();
    f = ((String)localObject);
    localObject = paramContext.getClass().getName();
    g = ((String)localObject);
  }
  
  public d a()
  {
    Object localObject1 = j;
    Object localObject2 = k.g;
    boolean bool = ((Map)localObject1).containsKey(localObject2);
    Set localSet;
    Map localMap;
    int i1;
    View localView;
    String str1;
    String str2;
    if (bool)
    {
      localObject1 = q;
      if (localObject1 == null)
      {
        bool = true;
        p.a(bool, "SignIn.API can't be used in conjunction with requestServerAuthCode.");
        localObject1 = j;
        localObject2 = k.g;
        localObject1 = (m)((Map)localObject1).get(localObject2);
        q = ((m)localObject1);
      }
    }
    else
    {
      localObject1 = new com/google/android/gms/common/internal/d;
      localObject2 = a;
      localSet = b;
      localMap = h;
      i1 = d;
      localView = e;
      str1 = f;
      str2 = g;
      localm = q;
      if (localm == null) {
        break label153;
      }
    }
    label153:
    for (m localm = q;; localm = m.a)
    {
      ((d)localObject1).<init>((Account)localObject2, localSet, localMap, i1, localView, str1, str2, localm);
      return (d)localObject1;
      bool = false;
      localObject1 = null;
      break;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/api/c$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */