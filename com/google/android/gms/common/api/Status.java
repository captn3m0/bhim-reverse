package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.o;
import com.google.android.gms.common.internal.o.a;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class Status
  implements SafeParcelable
{
  public static final Parcelable.Creator CREATOR;
  public static final Status a;
  public static final Status b;
  public static final Status c;
  public static final Status d;
  public static final Status e;
  private final int f;
  private final int g;
  private final String h;
  private final PendingIntent i;
  
  static
  {
    Object localObject = new com/google/android/gms/common/api/Status;
    ((Status)localObject).<init>(0);
    a = (Status)localObject;
    localObject = new com/google/android/gms/common/api/Status;
    ((Status)localObject).<init>(14);
    b = (Status)localObject;
    localObject = new com/google/android/gms/common/api/Status;
    ((Status)localObject).<init>(8);
    c = (Status)localObject;
    localObject = new com/google/android/gms/common/api/Status;
    ((Status)localObject).<init>(15);
    d = (Status)localObject;
    localObject = new com/google/android/gms/common/api/Status;
    ((Status)localObject).<init>(16);
    e = (Status)localObject;
    localObject = new com/google/android/gms/common/api/e;
    ((e)localObject).<init>();
    CREATOR = (Parcelable.Creator)localObject;
  }
  
  public Status(int paramInt)
  {
    this(paramInt, null);
  }
  
  Status(int paramInt1, int paramInt2, String paramString, PendingIntent paramPendingIntent)
  {
    f = paramInt1;
    g = paramInt2;
    h = paramString;
    i = paramPendingIntent;
  }
  
  public Status(int paramInt, String paramString)
  {
    this(1, paramInt, paramString, null);
  }
  
  private String f()
  {
    String str = h;
    if (str != null) {}
    int j;
    for (str = h;; str = b.a(j))
    {
      return str;
      j = g;
    }
  }
  
  PendingIntent a()
  {
    return i;
  }
  
  public String b()
  {
    return h;
  }
  
  int c()
  {
    return f;
  }
  
  public boolean d()
  {
    int j = g;
    if (j <= 0) {}
    int k;
    for (j = 1;; k = 0) {
      return j;
    }
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public int e()
  {
    return g;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = false;
    boolean bool2 = paramObject instanceof Status;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      paramObject = (Status)paramObject;
      int j = f;
      int k = f;
      if (j == k)
      {
        j = g;
        k = g;
        if (j == k)
        {
          Object localObject1 = h;
          Object localObject2 = h;
          boolean bool3 = o.a(localObject1, localObject2);
          if (bool3)
          {
            localObject1 = i;
            localObject2 = i;
            bool3 = o.a(localObject1, localObject2);
            if (bool3) {
              bool1 = true;
            }
          }
        }
      }
    }
  }
  
  public int hashCode()
  {
    Object[] arrayOfObject = new Object[4];
    Object localObject = Integer.valueOf(f);
    arrayOfObject[0] = localObject;
    localObject = Integer.valueOf(g);
    arrayOfObject[1] = localObject;
    localObject = h;
    arrayOfObject[2] = localObject;
    localObject = i;
    arrayOfObject[3] = localObject;
    return o.a(arrayOfObject);
  }
  
  public String toString()
  {
    o.a locala = o.a(this);
    Object localObject = f();
    locala = locala.a("statusCode", localObject);
    localObject = i;
    return locala.a("resolution", localObject).toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    e.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/api/Status.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */