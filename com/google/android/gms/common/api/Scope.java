package com.google.android.gms.common.api;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class Scope
  implements SafeParcelable
{
  public static final Parcelable.Creator CREATOR;
  final int a;
  private final String b;
  
  static
  {
    d locald = new com/google/android/gms/common/api/d;
    locald.<init>();
    CREATOR = locald;
  }
  
  Scope(int paramInt, String paramString)
  {
    p.a(paramString, "scopeUri must not be null or empty");
    a = paramInt;
    b = paramString;
  }
  
  public Scope(String paramString)
  {
    this(1, paramString);
  }
  
  public String a()
  {
    return b;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool;
    if (this == paramObject) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = paramObject instanceof Scope;
      String str1;
      if (!bool)
      {
        bool = false;
        str1 = null;
      }
      else
      {
        str1 = b;
        paramObject = (Scope)paramObject;
        String str2 = b;
        bool = str1.equals(str2);
      }
    }
  }
  
  public int hashCode()
  {
    return b.hashCode();
  }
  
  public String toString()
  {
    return b;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    d.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/api/Scope.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */