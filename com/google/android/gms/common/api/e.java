package com.google.android.gms.common.api;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class e
  implements Parcelable.Creator
{
  static void a(Status paramStatus, Parcel paramParcel, int paramInt)
  {
    int i = a.a(paramParcel);
    int j = paramStatus.e();
    a.a(paramParcel, 1, j);
    j = paramStatus.c();
    a.a(paramParcel, 1000, j);
    Object localObject = paramStatus.b();
    a.a(paramParcel, 2, (String)localObject, false);
    localObject = paramStatus.a();
    a.a(paramParcel, 3, (Parcelable)localObject, paramInt, false);
    a.a(paramParcel, i);
  }
  
  public Status a(Parcel paramParcel)
  {
    Object localObject1 = null;
    int i = 0;
    int j = zza.b(paramParcel);
    Object localObject2 = null;
    int k = 0;
    for (;;)
    {
      m = paramParcel.dataPosition();
      if (m >= j) {
        break;
      }
      m = zza.a(paramParcel);
      int n = zza.a(m);
      switch (n)
      {
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        i = zza.d(paramParcel, m);
        break;
      case 1000: 
        k = zza.d(paramParcel, m);
        break;
      case 2: 
        localObject2 = zza.h(paramParcel, m);
        break;
      case 3: 
        localObject1 = PendingIntent.CREATOR;
        localObject1 = (PendingIntent)zza.a(paramParcel, m, (Parcelable.Creator)localObject1);
      }
    }
    int m = paramParcel.dataPosition();
    if (m != j)
    {
      localObject1 = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Overread allowed size end=" + j;
      ((zza.zza)localObject1).<init>((String)localObject2, paramParcel);
      throw ((Throwable)localObject1);
    }
    Status localStatus = new com/google/android/gms/common/api/Status;
    localStatus.<init>(k, i, (String)localObject2, (PendingIntent)localObject1);
    return localStatus;
  }
  
  public Status[] a(int paramInt)
  {
    return new Status[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/api/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */