package com.google.android.gms.common.api;

public class b
{
  public static String a(int paramInt)
  {
    Object localObject;
    switch (paramInt)
    {
    default: 
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      String str = "unknown status code: ";
      localObject = str + paramInt;
    }
    for (;;)
    {
      return (String)localObject;
      localObject = "SUCCESS_CACHE";
      continue;
      localObject = "SUCCESS";
      continue;
      localObject = "SERVICE_MISSING";
      continue;
      localObject = "SERVICE_VERSION_UPDATE_REQUIRED";
      continue;
      localObject = "SERVICE_DISABLED";
      continue;
      localObject = "SIGN_IN_REQUIRED";
      continue;
      localObject = "INVALID_ACCOUNT";
      continue;
      localObject = "RESOLUTION_REQUIRED";
      continue;
      localObject = "NETWORK_ERROR";
      continue;
      localObject = "INTERNAL_ERROR";
      continue;
      localObject = "SERVICE_INVALID";
      continue;
      localObject = "DEVELOPER_ERROR";
      continue;
      localObject = "LICENSE_CHECK_FAILED";
      continue;
      localObject = "ERROR";
      continue;
      localObject = "INTERRUPTED";
      continue;
      localObject = "TIMEOUT";
      continue;
      localObject = "CANCELED";
      continue;
      localObject = "API_NOT_CONNECTED";
      continue;
      localObject = "AUTH_API_INVALID_CREDENTIALS";
      continue;
      localObject = "AUTH_API_ACCESS_FORBIDDEN";
      continue;
      localObject = "AUTH_API_CLIENT_ERROR";
      continue;
      localObject = "AUTH_API_SERVER_ERROR";
      continue;
      localObject = "AUTH_TOKEN_ERROR";
      continue;
      localObject = "AUTH_URL_RESOLUTION";
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/api/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */