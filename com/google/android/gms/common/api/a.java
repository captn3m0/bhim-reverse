package com.google.android.gms.common.api;

import com.google.android.gms.common.internal.p;

public final class a
{
  private final a.a a;
  private final a.c b;
  private final a.b c;
  private final a.d d;
  private final String e;
  
  public a(String paramString, a.a parama, a.b paramb)
  {
    p.a(parama, "Cannot construct an Api with a null ClientBuilder");
    p.a(paramb, "Cannot construct an Api with a null ClientKey");
    e = paramString;
    a = parama;
    b = null;
    c = paramb;
    d = null;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/api/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */