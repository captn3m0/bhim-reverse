package com.google.android.gms.common.stats;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Debug;
import android.os.Parcelable;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import com.google.android.gms.common.internal.c;
import com.google.android.gms.internal.i;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class b
{
  private static final Object a;
  private static b b;
  private static Integer h;
  private final List c;
  private final List d;
  private final List e;
  private final List f;
  private e g;
  private e i;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
  }
  
  private b()
  {
    int k = c();
    int m = d.b;
    if (k == m)
    {
      localObject1 = Collections.EMPTY_LIST;
      c = ((List)localObject1);
      localObject1 = Collections.EMPTY_LIST;
      d = ((List)localObject1);
      localObject1 = Collections.EMPTY_LIST;
      e = ((List)localObject1);
      localObject1 = Collections.EMPTY_LIST;
      f = ((List)localObject1);
      return;
    }
    Object localObject1 = (String)c.a.b.c();
    if (localObject1 == null)
    {
      localObject1 = Collections.EMPTY_LIST;
      label87:
      c = ((List)localObject1);
      localObject1 = (String)c.a.c.c();
      if (localObject1 != null) {
        break label266;
      }
      localObject1 = Collections.EMPTY_LIST;
      label114:
      d = ((List)localObject1);
      localObject1 = (String)c.a.d.c();
      if (localObject1 != null) {
        break label285;
      }
      localObject1 = Collections.EMPTY_LIST;
      label141:
      e = ((List)localObject1);
      localObject1 = (String)c.a.e.c();
      if (localObject1 != null) {
        break label304;
      }
    }
    Object localObject2;
    for (localObject1 = Collections.EMPTY_LIST;; localObject1 = Arrays.asList(((String)localObject1).split((String)localObject2)))
    {
      f = ((List)localObject1);
      localObject2 = new com/google/android/gms/common/stats/e;
      long l = ((Long)c.a.f.c()).longValue();
      ((e)localObject2).<init>(j, l);
      g = ((e)localObject2);
      localObject2 = new com/google/android/gms/common/stats/e;
      localObject1 = (Long)c.a.f.c();
      l = ((Long)localObject1).longValue();
      ((e)localObject2).<init>(j, l);
      i = ((e)localObject2);
      break;
      localObject2 = ",";
      localObject1 = Arrays.asList(((String)localObject1).split((String)localObject2));
      break label87;
      label266:
      localObject2 = ",";
      localObject1 = Arrays.asList(((String)localObject1).split((String)localObject2));
      break label114;
      label285:
      localObject2 = ",";
      localObject1 = Arrays.asList(((String)localObject1).split((String)localObject2));
      break label141;
      label304:
      localObject2 = ",";
    }
  }
  
  public static b a()
  {
    synchronized (a)
    {
      b localb = b;
      if (localb == null)
      {
        localb = new com/google/android/gms/common/stats/b;
        localb.<init>();
        b = localb;
      }
      return b;
    }
  }
  
  private String a(ServiceConnection paramServiceConnection)
  {
    long l1 = Process.myPid() << 32;
    long l2 = System.identityHashCode(paramServiceConnection);
    return String.valueOf(l1 | l2);
  }
  
  private void a(Context paramContext, String paramString1, int paramInt, String paramString2, String paramString3, String paramString4, String paramString5)
  {
    long l1 = System.currentTimeMillis();
    String str = null;
    int j = c();
    int k = d.f;
    j &= k;
    if (j != 0)
    {
      j = 13;
      if (paramInt != j)
      {
        j = 3;
        k = 5;
        str = i.a(j, k);
      }
    }
    long l2 = 0L;
    j = c();
    k = d.h;
    j &= k;
    if (j != 0) {
      l2 = Debug.getNativeHeapAllocatedSize();
    }
    j = 1;
    if (paramInt != j)
    {
      j = 4;
      if (paramInt != j)
      {
        j = 14;
        if (paramInt != j) {
          break label184;
        }
      }
    }
    Object localObject = new com/google/android/gms/common/stats/ConnectionEvent;
    long l3 = SystemClock.elapsedRealtime();
    k = paramInt;
    ((ConnectionEvent)localObject).<init>(l1, paramInt, null, null, null, null, str, paramString1, l3, l2);
    for (;;)
    {
      Intent localIntent = new android/content/Intent;
      localIntent.<init>();
      ComponentName localComponentName = d.a;
      localObject = localIntent.setComponent(localComponentName).putExtra("com.google.android.gms.common.stats.EXTRA_LOG_EVENT", (Parcelable)localObject);
      paramContext.startService((Intent)localObject);
      return;
      label184:
      localObject = new com/google/android/gms/common/stats/ConnectionEvent;
      l3 = SystemClock.elapsedRealtime();
      k = paramInt;
      ((ConnectionEvent)localObject).<init>(l1, paramInt, paramString2, paramString3, paramString4, paramString5, str, paramString1, l3, l2);
    }
  }
  
  private void a(Context paramContext, String paramString1, String paramString2, Intent paramIntent, int paramInt)
  {
    int j = 1;
    String str1 = null;
    String str2 = null;
    boolean bool1 = b();
    Object localObject1;
    if (bool1)
    {
      localObject1 = g;
      if (localObject1 != null) {
        break label32;
      }
    }
    label32:
    boolean bool2;
    do
    {
      return;
      int k = 4;
      if ((paramInt != k) && (paramInt != j)) {
        break;
      }
      localObject1 = g;
      bool2 = ((e)localObject1).b(paramString1);
    } while (!bool2);
    String str3 = null;
    String str4 = null;
    for (;;)
    {
      localObject1 = this;
      Object localObject2 = paramContext;
      Object localObject3 = paramString1;
      a(paramContext, paramString1, paramInt, str2, paramString2, str4, str3);
      break;
      localObject1 = b(paramContext, paramIntent);
      if (localObject1 == null)
      {
        localObject1 = "ConnectionTracker";
        int m = 2;
        localObject3 = new Object[m];
        localObject3[0] = paramString2;
        str1 = paramIntent.toUri(0);
        localObject3[j] = str1;
        localObject2 = String.format("Client %s made an invalid request %s", (Object[])localObject3);
        Log.w((String)localObject1, (String)localObject2);
        break;
      }
      str4 = processName;
      str3 = name;
      str2 = i.a(paramContext);
      bool2 = a(str2, paramString2, str4, str3);
      if (!bool2) {
        break;
      }
      localObject1 = g;
      ((e)localObject1).a(paramString1);
    }
  }
  
  private boolean a(Context paramContext, Intent paramIntent)
  {
    Object localObject = paramIntent.getComponent();
    boolean bool2;
    if (localObject != null)
    {
      boolean bool1 = c.a;
      if (bool1)
      {
        String str1 = "com.google.android.gms";
        String str2 = ((ComponentName)localObject).getPackageName();
        bool1 = str1.equals(str2);
        if (!bool1) {}
      }
    }
    else
    {
      bool2 = false;
      localObject = null;
    }
    for (;;)
    {
      return bool2;
      localObject = ((ComponentName)localObject).getPackageName();
      bool2 = com.google.android.gms.internal.d.a(paramContext, (String)localObject);
    }
  }
  
  private boolean a(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    int j = c();
    List localList = c;
    boolean bool = localList.contains(paramString1);
    if (!bool)
    {
      localList = d;
      bool = localList.contains(paramString2);
      if (!bool)
      {
        localList = e;
        bool = localList.contains(paramString3);
        if (!bool)
        {
          localList = f;
          bool = localList.contains(paramString4);
          if (!bool)
          {
            bool = paramString3.equals(paramString1);
            if (!bool) {
              break label125;
            }
            int m = d.g;
            j &= m;
            if (j == 0) {
              break label125;
            }
          }
        }
      }
    }
    label125:
    int k;
    for (j = 0;; k = 1) {
      return j;
    }
  }
  
  private static ServiceInfo b(Context paramContext, Intent paramIntent)
  {
    int j = 20;
    int k = 3;
    int m = 2;
    int n = 1;
    Object localObject1 = paramContext.getPackageManager();
    int i1 = 128;
    localObject1 = ((PackageManager)localObject1).queryIntentServices(paramIntent, i1);
    Object localObject2;
    Object localObject3;
    Object localObject4;
    if (localObject1 != null)
    {
      i1 = ((List)localObject1).size();
      if (i1 != 0) {}
    }
    else
    {
      localObject2 = new Object[m];
      localObject3 = paramIntent.toUri(0);
      localObject2[0] = localObject3;
      localObject3 = i.a(k, j);
      localObject2[n] = localObject3;
      localObject4 = String.format("There are no handler of this intent: %s\n Stack trace: %s", (Object[])localObject2);
      Log.w("ConnectionTracker", (String)localObject4);
      localObject1 = null;
    }
    for (;;)
    {
      return (ServiceInfo)localObject1;
      i1 = ((List)localObject1).size();
      if (i1 > n)
      {
        localObject3 = new Object[m];
        String str = paramIntent.toUri(0);
        localObject3[0] = str;
        str = i.a(k, j);
        localObject3[n] = str;
        localObject2 = String.format("Multiple handlers found for this intent: %s\n Stack trace: %s", (Object[])localObject3);
        Log.w("ConnectionTracker", (String)localObject2);
        localObject4 = ((List)localObject1).iterator();
        boolean bool = ((Iterator)localObject4).hasNext();
        if (bool)
        {
          localObject1 = (ResolveInfo)((Iterator)localObject4).next();
          localObject4 = "ConnectionTracker";
          localObject1 = serviceInfo.name;
          Log.w((String)localObject4, (String)localObject1);
          localObject1 = null;
          continue;
        }
      }
      localObject1 = get0serviceInfo;
    }
  }
  
  private boolean b()
  {
    boolean bool1 = false;
    boolean bool2 = c.a;
    if (!bool2) {}
    for (;;)
    {
      return bool1;
      int j = c();
      int k = d.b;
      if (j != k) {
        bool1 = true;
      }
    }
  }
  
  private static int c()
  {
    Object localObject = h;
    if (localObject == null) {}
    for (;;)
    {
      try
      {
        boolean bool = com.google.android.gms.internal.d.a();
        if (!bool) {
          continue;
        }
        localObject = c.a.a;
        localObject = ((com.google.android.gms.internal.b)localObject).c();
        localObject = (Integer)localObject;
        j = ((Integer)localObject).intValue();
        localObject = Integer.valueOf(j);
        h = (Integer)localObject;
      }
      catch (SecurityException localSecurityException)
      {
        int j = d.b;
        Integer localInteger = Integer.valueOf(j);
        h = localInteger;
        continue;
      }
      return h.intValue();
      j = d.b;
    }
  }
  
  public void a(Context paramContext, ServiceConnection paramServiceConnection)
  {
    paramContext.unbindService(paramServiceConnection);
    String str = a(paramServiceConnection);
    a(paramContext, str, null, null, 1);
  }
  
  public void a(Context paramContext, ServiceConnection paramServiceConnection, String paramString, Intent paramIntent)
  {
    String str = a(paramServiceConnection);
    a(paramContext, str, paramString, paramIntent, 3);
  }
  
  public boolean a(Context paramContext, Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt)
  {
    String str = paramContext.getClass().getName();
    return a(paramContext, str, paramIntent, paramServiceConnection, paramInt);
  }
  
  public boolean a(Context paramContext, String paramString, Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt)
  {
    boolean bool1 = a(paramContext, paramIntent);
    Object localObject;
    if (bool1)
    {
      localObject = "Attempted to bind to a service in a STOPPED package.";
      Log.w("ConnectionTracker", (String)localObject);
    }
    boolean bool2;
    for (bool1 = false;; bool1 = bool2)
    {
      return bool1;
      bool2 = paramContext.bindService(paramIntent, paramServiceConnection, paramInt);
      if (bool2)
      {
        String str = a(paramServiceConnection);
        int j = 2;
        localObject = paramContext;
        a(paramContext, str, paramString, paramIntent, j);
      }
    }
  }
  
  public void b(Context paramContext, ServiceConnection paramServiceConnection)
  {
    String str = a(paramServiceConnection);
    a(paramContext, str, null, null, 4);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/stats/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */