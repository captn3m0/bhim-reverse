package com.google.android.gms.common.stats;

import android.content.ComponentName;

public final class d
{
  public static final ComponentName a;
  public static int b;
  public static int c;
  public static int d;
  public static int e;
  public static int f;
  public static int g;
  public static int h;
  public static int i;
  
  static
  {
    int j = 1;
    ComponentName localComponentName = new android/content/ComponentName;
    localComponentName.<init>("com.google.android.gms", "com.google.android.gms.common.stats.GmsCoreStatsService");
    a = localComponentName;
    b = 0;
    c = j;
    d = 2;
    e = 4;
    f = 8;
    g = 16;
    h = 32;
    i = j;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/stats/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */