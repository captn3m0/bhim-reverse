package com.google.android.gms.common.stats;

import com.google.android.gms.internal.b;

public final class c
{
  public static b a;
  public static b b;
  
  static
  {
    int i = 100;
    Integer localInteger = Integer.valueOf(i);
    a = b.a("gms:common:stats:max_num_of_events", localInteger);
    localInteger = Integer.valueOf(i);
    b = b.a("gms:common:stats:max_chunk_size", localInteger);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/stats/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */