package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class a
  implements Parcelable.Creator
{
  static void a(ConnectionEvent paramConnectionEvent, Parcel paramParcel, int paramInt)
  {
    int i = com.google.android.gms.common.internal.safeparcel.a.a(paramParcel);
    int j = a;
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 1, j);
    long l = paramConnectionEvent.a();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 2, l);
    String str = paramConnectionEvent.c();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 4, str, false);
    str = paramConnectionEvent.d();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 5, str, false);
    str = paramConnectionEvent.e();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 6, str, false);
    str = paramConnectionEvent.f();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 7, str, false);
    str = paramConnectionEvent.g();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 8, str, false);
    l = paramConnectionEvent.k();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 10, l);
    l = paramConnectionEvent.j();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 11, l);
    j = paramConnectionEvent.b();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 12, j);
    str = paramConnectionEvent.h();
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, 13, str, false);
    com.google.android.gms.common.internal.safeparcel.a.a(paramParcel, i);
  }
  
  public ConnectionEvent a(Parcel paramParcel)
  {
    int i = zza.b(paramParcel);
    int j = 0;
    StringBuilder localStringBuilder = null;
    long l1 = 0L;
    int k = 0;
    String str1 = null;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    String str5 = null;
    String str6 = null;
    long l2 = 0L;
    long l3 = 0L;
    for (;;)
    {
      m = paramParcel.dataPosition();
      if (m >= i) {
        break;
      }
      m = zza.a(paramParcel);
      int n = zza.a(m);
      switch (n)
      {
      case 3: 
      case 9: 
      default: 
        zza.b(paramParcel, m);
        break;
      case 1: 
        j = zza.d(paramParcel, m);
        break;
      case 2: 
        l1 = zza.e(paramParcel, m);
        break;
      case 4: 
        str1 = zza.h(paramParcel, m);
        break;
      case 5: 
        str2 = zza.h(paramParcel, m);
        break;
      case 6: 
        str3 = zza.h(paramParcel, m);
        break;
      case 7: 
        str4 = zza.h(paramParcel, m);
        break;
      case 8: 
        str5 = zza.h(paramParcel, m);
        break;
      case 10: 
        l2 = zza.e(paramParcel, m);
        break;
      case 11: 
        l3 = zza.e(paramParcel, m);
        break;
      case 12: 
        k = zza.d(paramParcel, m);
        break;
      case 13: 
        str6 = zza.h(paramParcel, m);
      }
    }
    int m = paramParcel.dataPosition();
    if (m != i)
    {
      localObject = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      String str7 = "Overread allowed size end=" + i;
      ((zza.zza)localObject).<init>(str7, paramParcel);
      throw ((Throwable)localObject);
    }
    Object localObject = new com/google/android/gms/common/stats/ConnectionEvent;
    ((ConnectionEvent)localObject).<init>(j, l1, k, str1, str2, str3, str4, str5, str6, l2, l3);
    return (ConnectionEvent)localObject;
  }
  
  public ConnectionEvent[] a(int paramInt)
  {
    return new ConnectionEvent[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/stats/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */