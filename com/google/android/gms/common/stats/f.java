package com.google.android.gms.common.stats;

public abstract class f
{
  public abstract long a();
  
  public abstract int b();
  
  public abstract long i();
  
  public abstract String l();
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    long l = a();
    localStringBuilder = localStringBuilder.append(l).append("\t");
    int i = b();
    localStringBuilder = localStringBuilder.append(i).append("\t");
    l = i();
    localStringBuilder = localStringBuilder.append(l);
    String str = l();
    return str;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/stats/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */