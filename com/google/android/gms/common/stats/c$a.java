package com.google.android.gms.common.stats;

import com.google.android.gms.internal.b;

public final class c$a
{
  public static b a;
  public static b b;
  public static b c;
  public static b d;
  public static b e;
  public static b f;
  
  static
  {
    Object localObject = Integer.valueOf(d.b);
    a = b.a("gms:common:stats:connections:level", (Integer)localObject);
    b = b.a("gms:common:stats:connections:ignored_calling_processes", "");
    c = b.a("gms:common:stats:connections:ignored_calling_services", "");
    d = b.a("gms:common:stats:connections:ignored_target_processes", "");
    e = b.a("gms:common:stats:connections:ignored_target_services", "com.google.android.gms.auth.GetToken");
    localObject = Long.valueOf(600000L);
    f = b.a("gms:common:stats:connections:time_out_duration", (Long)localObject);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/stats/c$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */