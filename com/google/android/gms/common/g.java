package com.google.android.gms.common;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.util.Base64;
import android.util.Log;
import java.util.Set;

public class g
{
  private static final g a;
  
  static
  {
    g localg = new com/google/android/gms/common/g;
    localg.<init>();
    a = localg;
  }
  
  public static g a()
  {
    return a;
  }
  
  private boolean a(PackageInfo paramPackageInfo, boolean paramBoolean)
  {
    int i = 1;
    Object localObject1 = signatures;
    int j = localObject1.length;
    Object localObject2;
    if (j != i)
    {
      localObject2 = "Package has more than one signature.";
      Log.w("GoogleSignatureVerifier", (String)localObject2);
      j = 0;
      localObject1 = null;
    }
    for (;;)
    {
      return j;
      Object localObject3 = new com/google/android/gms/common/f$ab;
      localObject1 = signatures[0].toByteArray();
      ((f.ab)localObject3).<init>((byte[])localObject1);
      if (paramBoolean) {}
      for (localObject1 = f.a();; localObject1 = f.b())
      {
        bool = ((Set)localObject1).contains(localObject3);
        if (!bool) {
          break label102;
        }
        bool = i;
        break;
      }
      label102:
      localObject1 = "GoogleSignatureVerifier";
      i = 2;
      boolean bool = Log.isLoggable((String)localObject1, i);
      if (bool)
      {
        localObject1 = "GoogleSignatureVerifier";
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        String str = "Signature not valid.  Found: \n";
        localObject2 = ((StringBuilder)localObject2).append(str);
        localObject3 = Base64.encodeToString(((f.a)localObject3).a(), 0);
        localObject2 = (String)localObject3;
        Log.v((String)localObject1, (String)localObject2);
      }
      bool = false;
      localObject1 = null;
    }
  }
  
  f.a a(PackageInfo paramPackageInfo, f.a... paramVarArgs)
  {
    String str1 = null;
    Object localObject1 = signatures;
    int i = localObject1.length;
    int j = 1;
    if (i != j)
    {
      str1 = "Package has more than one signature.";
      Log.w("GoogleSignatureVerifier", str1);
      i = 0;
      localObject1 = null;
    }
    for (;;)
    {
      return (f.a)localObject1;
      Object localObject2 = new com/google/android/gms/common/f$ab;
      localObject1 = signatures[0].toByteArray();
      ((f.ab)localObject2).<init>((byte[])localObject1);
      i = 0;
      localObject1 = null;
      Object localObject3;
      for (;;)
      {
        int k = paramVarArgs.length;
        if (i >= k) {
          break label120;
        }
        localObject3 = paramVarArgs[i];
        boolean bool2 = ((f.a)localObject3).equals(localObject2);
        if (bool2)
        {
          localObject1 = paramVarArgs[i];
          break;
        }
        i += 1;
      }
      label120:
      localObject1 = "GoogleSignatureVerifier";
      int m = 2;
      boolean bool1 = Log.isLoggable((String)localObject1, m);
      if (bool1)
      {
        localObject1 = "GoogleSignatureVerifier";
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        String str2 = "Signature not valid.  Found: \n";
        localObject3 = ((StringBuilder)localObject3).append(str2);
        localObject2 = ((f.a)localObject2).a();
        str1 = Base64.encodeToString((byte[])localObject2, 0);
        str1 = str1;
        Log.v((String)localObject1, str1);
      }
      bool1 = false;
      localObject1 = null;
    }
  }
  
  public boolean a(PackageManager paramPackageManager, PackageInfo paramPackageInfo)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    if (paramPackageInfo == null) {}
    for (;;)
    {
      return bool2;
      boolean bool3 = GooglePlayServicesUtil.zzc(paramPackageManager);
      if (bool3)
      {
        bool2 = a(paramPackageInfo, bool1);
      }
      else
      {
        bool2 = a(paramPackageInfo, false);
        if (!bool2)
        {
          bool3 = a(paramPackageInfo, bool1);
          if (bool3)
          {
            String str1 = "GoogleSignatureVerifier";
            String str2 = "Test-keys aren't accepted on this build.";
            Log.w(str1, str2);
          }
        }
      }
    }
  }
  
  public boolean a(PackageManager paramPackageManager, String paramString)
  {
    int i = 64;
    try
    {
      PackageInfo localPackageInfo = paramPackageManager.getPackageInfo(paramString, i);
      bool = a(paramPackageManager, localPackageInfo);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        String str1 = "GoogleSignatureVerifier";
        int j = 3;
        boolean bool = Log.isLoggable(str1, j);
        if (bool)
        {
          str1 = "GoogleSignatureVerifier";
          Object localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          localObject = ((StringBuilder)localObject).append("Package manager can't find package ").append(paramString);
          String str2 = ", defaulting to false";
          localObject = str2;
          Log.d(str1, (String)localObject);
        }
        bool = false;
        str1 = null;
      }
    }
    return bool;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */