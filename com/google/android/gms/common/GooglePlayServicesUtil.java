package com.google.android.gms.common;

import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.Notification.BigTextStyle;
import android.app.Notification.Builder;
import android.app.Notification.Style;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageInstaller.SessionInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.net.Uri;
import android.net.Uri.Builder;
import android.os.Build;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.UserManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.aa.d;
import android.support.v4.app.l;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import com.google.android.gms.a.a;
import com.google.android.gms.a.b;
import com.google.android.gms.common.internal.e;
import com.google.android.gms.internal.h;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public final class GooglePlayServicesUtil
{
  public static final String GMS_ERROR_DIALOG = "GooglePlayServicesErrorDialog";
  public static final String GOOGLE_PLAY_SERVICES_PACKAGE = "com.google.android.gms";
  public static final int GOOGLE_PLAY_SERVICES_VERSION_CODE = ;
  public static final String GOOGLE_PLAY_STORE_PACKAGE = "com.android.vending";
  public static boolean zzaee = false;
  public static boolean zzaef = false;
  private static int zzaeg = -1;
  private static String zzaeh;
  private static Integer zzaei;
  static final AtomicBoolean zzaej;
  private static final AtomicBoolean zzaek;
  private static final Object zzqf;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    zzqf = localObject;
    zzaeh = null;
    zzaei = null;
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>();
    zzaej = (AtomicBoolean)localObject;
    localObject = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject).<init>();
    zzaek = (AtomicBoolean)localObject;
  }
  
  public static Dialog getErrorDialog(int paramInt1, Activity paramActivity, int paramInt2)
  {
    return getErrorDialog(paramInt1, paramActivity, paramInt2, null);
  }
  
  public static Dialog getErrorDialog(int paramInt1, Activity paramActivity, int paramInt2, DialogInterface.OnCancelListener paramOnCancelListener)
  {
    return zza(paramInt1, paramActivity, null, paramInt2, paramOnCancelListener);
  }
  
  public static PendingIntent getErrorPendingIntent(int paramInt1, Context paramContext, int paramInt2)
  {
    return b.a().a(paramContext, paramInt1, paramInt2);
  }
  
  public static String getErrorString(int paramInt)
  {
    return ConnectionResult.a(paramInt);
  }
  
  public static String getOpenSourceSoftwareLicenseInfo(Context paramContext)
  {
    Object localObject1 = new android/net/Uri$Builder;
    ((Uri.Builder)localObject1).<init>();
    localObject1 = ((Uri.Builder)localObject1).scheme("android.resource").authority("com.google.android.gms").appendPath("raw");
    localObject5 = "oss_notice";
    localObject1 = ((Uri.Builder)localObject1).appendPath((String)localObject5).build();
    for (;;)
    {
      try
      {
        localObject5 = paramContext.getContentResolver();
        localObject5 = ((ContentResolver)localObject5).openInputStream((Uri)localObject1);
      }
      catch (Exception localException)
      {
        String str;
        Object localObject2;
        Object localObject4 = null;
        continue;
      }
      try
      {
        localObject1 = new java/util/Scanner;
        ((Scanner)localObject1).<init>((InputStream)localObject5);
        str = "\\A";
        localObject1 = ((Scanner)localObject1).useDelimiter(str);
        localObject1 = ((Scanner)localObject1).next();
      }
      catch (NoSuchElementException localNoSuchElementException)
      {
        if (localObject5 == null) {
          continue;
        }
        ((InputStream)localObject5).close();
        localObject2 = null;
      }
      finally
      {
        if (localObject5 == null) {
          continue;
        }
        ((InputStream)localObject5).close();
      }
    }
    return (String)localObject1;
  }
  
  public static Context getRemoteContext(Context paramContext)
  {
    Object localObject1 = "com.google.android.gms";
    int i = 3;
    try
    {
      localObject1 = paramContext.createPackageContext((String)localObject1, i);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        Object localObject2 = null;
      }
    }
    return (Context)localObject1;
  }
  
  public static Resources getRemoteResource(Context paramContext)
  {
    try
    {
      localObject1 = paramContext.getPackageManager();
      String str = "com.google.android.gms";
      localObject1 = ((PackageManager)localObject1).getResourcesForApplication(str);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        Object localObject1;
        Object localObject2 = null;
      }
    }
    return (Resources)localObject1;
  }
  
  public static int isGooglePlayServicesAvailable(Context paramContext)
  {
    int i = 1;
    int j = 9;
    String str1 = null;
    boolean bool2 = com.google.android.gms.common.internal.c.a;
    if (bool2)
    {
      j = 0;
      Object localObject2 = null;
    }
    for (;;)
    {
      return j;
      Object localObject5 = paramContext.getPackageManager();
      int m;
      boolean bool3;
      String str3;
      String str2;
      try
      {
        localObject6 = paramContext.getResources();
        m = a.b.common_google_play_services_unknown_issue;
        ((Resources)localObject6).getString(m);
        localObject6 = "com.google.android.gms";
        localObject8 = paramContext.getPackageName();
        bool3 = ((String)localObject6).equals(localObject8);
        if (!bool3) {
          zzak(paramContext);
        }
        localObject6 = "com.google.android.gms";
        m = 64;
      }
      finally
      {
        for (;;)
        {
          try
          {
            Object localObject6 = ((PackageManager)localObject5).getPackageInfo((String)localObject6, m);
            localObject8 = g.a();
            boolean bool4 = com.google.android.gms.internal.c.a(paramContext);
            if (!bool4) {
              break label192;
            }
            localObject9 = f.bj.a;
            localObject8 = ((g)localObject8).a((PackageInfo)localObject6, (f.a[])localObject9);
            if (localObject8 != null) {
              break label317;
            }
            str1 = "GooglePlayServicesUtil";
            str3 = "Google Play services signature invalid.";
            Log.w(str1, str3);
          }
          catch (PackageManager.NameNotFoundException localNameNotFoundException2)
          {
            localObject3 = "GooglePlayServicesUtil";
            str1 = "Google Play services is missing.";
            Log.w((String)localObject3, str1);
            j = i;
          }
          localObject7 = finally;
          str2 = "GooglePlayServicesUtil";
          localObject8 = "The Google Play services resources were not found. Check your project configuration to ensure that the resources are included.";
          Log.e(str2, (String)localObject8);
        }
      }
      Object localObject3;
      continue;
      label192:
      Object localObject9 = "com.android.vending";
      int n = 8256;
      Object localObject1;
      try
      {
        localObject9 = ((PackageManager)localObject5).getPackageInfo((String)localObject9, n);
        arrayOfa = f.bj.a;
        localObject9 = ((g)localObject8).a((PackageInfo)localObject9, arrayOfa);
        if (localObject9 != null) {
          break label268;
        }
        str1 = "GooglePlayServicesUtil";
        str3 = "Google Play Store signature invalid.";
        Log.w(str1, str3);
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException1)
      {
        localObject1 = "GooglePlayServicesUtil";
        str3 = "Google Play Store is neither installed nor updating.";
        Log.w((String)localObject1, str3);
      }
      continue;
      label268:
      n = 1;
      f.a[] arrayOfa = new f.a[n];
      arrayOfa[0] = localObject9;
      Object localObject8 = ((g)localObject8).a(str2, arrayOfa);
      if (localObject8 == null)
      {
        localObject1 = "GooglePlayServicesUtil";
        str3 = "Google Play services signature invalid.";
        Log.w((String)localObject1, str3);
      }
      else
      {
        label317:
        j = com.google.android.gms.internal.c.a(GOOGLE_PLAY_SERVICES_VERSION_CODE);
        m = com.google.android.gms.internal.c.a(versionCode);
        if (m < j)
        {
          localObject3 = "GooglePlayServicesUtil";
          localObject1 = new java/lang/StringBuilder;
          ((StringBuilder)localObject1).<init>();
          localObject1 = ((StringBuilder)localObject1).append("Google Play services out of date.  Requires ");
          i = GOOGLE_PLAY_SERVICES_VERSION_CODE;
          localObject1 = ((StringBuilder)localObject1).append(i);
          str3 = " but found ";
          localObject1 = ((StringBuilder)localObject1).append(str3);
          i = versionCode;
          localObject1 = i;
          Log.w((String)localObject3, (String)localObject1);
          j = 2;
        }
        else
        {
          localObject3 = applicationInfo;
          if (localObject3 == null)
          {
            localObject3 = "com.google.android.gms";
            bool3 = false;
            str2 = null;
          }
          try
          {
            localObject3 = ((PackageManager)localObject5).getApplicationInfo((String)localObject3, 0);
            boolean bool1 = enabled;
            if (bool1) {
              break label479;
            }
            k = 3;
          }
          catch (PackageManager.NameNotFoundException localNameNotFoundException3)
          {
            localObject1 = "GooglePlayServicesUtil";
            localObject5 = "Google Play services missing when getting application info.";
            Log.wtf((String)localObject1, (String)localObject5, localNameNotFoundException3);
            k = i;
          }
          continue;
          label479:
          int k = 0;
          Object localObject4 = null;
        }
      }
    }
  }
  
  public static boolean isUserRecoverableError(int paramInt)
  {
    switch (paramInt)
    {
    }
    for (boolean bool = false;; bool = true) {
      return bool;
    }
  }
  
  public static boolean showErrorDialogFragment(int paramInt1, Activity paramActivity, int paramInt2)
  {
    return showErrorDialogFragment(paramInt1, paramActivity, paramInt2, null);
  }
  
  public static boolean showErrorDialogFragment(int paramInt1, Activity paramActivity, int paramInt2, DialogInterface.OnCancelListener paramOnCancelListener)
  {
    return showErrorDialogFragment(paramInt1, paramActivity, null, paramInt2, paramOnCancelListener);
  }
  
  public static boolean showErrorDialogFragment(int paramInt1, Activity paramActivity, Fragment paramFragment, int paramInt2, DialogInterface.OnCancelListener paramOnCancelListener)
  {
    Dialog localDialog = zza(paramInt1, paramActivity, paramFragment, paramInt2, paramOnCancelListener);
    boolean bool;
    if (localDialog == null)
    {
      bool = false;
      localDialog = null;
    }
    for (;;)
    {
      return bool;
      String str = "GooglePlayServicesErrorDialog";
      zza(paramActivity, paramOnCancelListener, str, localDialog);
      bool = true;
    }
  }
  
  public static void showErrorNotification(int paramInt, Context paramContext)
  {
    boolean bool1 = com.google.android.gms.internal.c.a(paramContext);
    if (bool1)
    {
      int i = 2;
      if (paramInt == i) {
        paramInt = 42;
      }
    }
    boolean bool2 = zzd(paramContext, paramInt);
    if (!bool2)
    {
      bool2 = zzf(paramContext, paramInt);
      if (!bool2) {}
    }
    else
    {
      zzal(paramContext);
    }
    for (;;)
    {
      return;
      zza(paramInt, paramContext);
    }
  }
  
  private static Dialog zza(int paramInt1, Activity paramActivity, Fragment paramFragment, int paramInt2, DialogInterface.OnCancelListener paramOnCancelListener)
  {
    Object localObject1 = null;
    if (paramInt1 == 0) {
      return (Dialog)localObject1;
    }
    boolean bool1 = com.google.android.gms.internal.c.a(paramActivity);
    if (bool1)
    {
      int i = 2;
      if (paramInt1 == i) {
        paramInt1 = 42;
      }
    }
    boolean bool2 = h.c();
    if (bool2)
    {
      localObject2 = new android/util/TypedValue;
      ((TypedValue)localObject2).<init>();
      localObject3 = paramActivity.getTheme();
      int m = 16843529;
      boolean bool4 = true;
      ((Resources.Theme)localObject3).resolveAttribute(m, (TypedValue)localObject2, bool4);
      localObject3 = paramActivity.getResources();
      int j = resourceId;
      localObject2 = ((Resources)localObject3).getResourceEntryName(j);
      localObject3 = "Theme.Dialog.Alert";
      boolean bool3 = ((String)localObject3).equals(localObject2);
      if (bool3)
      {
        localObject1 = new android/app/AlertDialog$Builder;
        int k = 5;
        ((AlertDialog.Builder)localObject1).<init>(paramActivity, k);
      }
    }
    if (localObject1 == null)
    {
      localObject1 = new android/app/AlertDialog$Builder;
      ((AlertDialog.Builder)localObject1).<init>(paramActivity);
    }
    Object localObject2 = zzam(paramActivity);
    localObject2 = e.a(paramActivity, paramInt1, (String)localObject2);
    ((AlertDialog.Builder)localObject1).setMessage((CharSequence)localObject2);
    if (paramOnCancelListener != null) {
      ((AlertDialog.Builder)localObject1).setOnCancelListener(paramOnCancelListener);
    }
    localObject2 = b.a();
    Object localObject3 = ((b)localObject2).a(paramActivity, paramInt1, "d");
    if (paramFragment == null)
    {
      localObject2 = new com/google/android/gms/common/internal/f;
      ((com.google.android.gms.common.internal.f)localObject2).<init>(paramActivity, (Intent)localObject3, paramInt2);
    }
    for (;;)
    {
      localObject3 = e.b(paramActivity, paramInt1);
      if (localObject3 != null) {
        ((AlertDialog.Builder)localObject1).setPositiveButton((CharSequence)localObject3, (DialogInterface.OnClickListener)localObject2);
      }
      localObject2 = e.a(paramActivity, paramInt1);
      if (localObject2 != null) {
        ((AlertDialog.Builder)localObject1).setTitle((CharSequence)localObject2);
      }
      localObject1 = ((AlertDialog.Builder)localObject1).create();
      break;
      localObject2 = new com/google/android/gms/common/internal/f;
      ((com.google.android.gms.common.internal.f)localObject2).<init>(paramFragment, (Intent)localObject3, paramInt2);
    }
  }
  
  private static void zza(int paramInt, Context paramContext)
  {
    zza(paramInt, paramContext, null);
  }
  
  private static void zza(int paramInt, Context paramContext, String paramString)
  {
    int i = 17301642;
    boolean bool1 = true;
    Object localObject1 = paramContext.getResources();
    Object localObject2 = zzam(paramContext);
    Object localObject3 = e.c(paramContext, paramInt);
    if (localObject3 == null)
    {
      int j = a.b.common_google_play_services_notification_ticker;
      localObject3 = ((Resources)localObject1).getString(j);
    }
    localObject2 = e.b(paramContext, paramInt, (String)localObject2);
    Object localObject4 = b.a();
    Object localObject5 = "n";
    localObject4 = ((b)localObject4).a(paramContext, paramInt, 0, (String)localObject5);
    boolean bool3 = com.google.android.gms.internal.c.a(paramContext);
    int n;
    int m;
    int k;
    if (bool3)
    {
      com.google.android.gms.common.internal.p.a(h.d());
      localObject5 = new android/app/Notification$Builder;
      ((Notification.Builder)localObject5).<init>(paramContext);
      i = a.a.common_ic_googleplayservices;
      localObject5 = ((Notification.Builder)localObject5).setSmallIcon(i);
      i = 2;
      localObject5 = ((Notification.Builder)localObject5).setPriority(i).setAutoCancel(bool1);
      Notification.BigTextStyle localBigTextStyle = new android/app/Notification$BigTextStyle;
      localBigTextStyle.<init>();
      Object localObject6 = new java/lang/StringBuilder;
      ((StringBuilder)localObject6).<init>();
      localObject3 = ((StringBuilder)localObject6).append((String)localObject3);
      localObject6 = " ";
      localObject3 = (String)localObject6 + (String)localObject2;
      localObject3 = localBigTextStyle.bigText((CharSequence)localObject3);
      localObject3 = ((Notification.Builder)localObject5).setStyle((Notification.Style)localObject3);
      n = a.a.common_full_open_on_phone;
      m = a.b.common_open_on_phone;
      localObject1 = ((Resources)localObject1).getString(m);
      localObject3 = ((Notification.Builder)localObject3).addAction(n, (CharSequence)localObject1, (PendingIntent)localObject4).build();
      localObject1 = localObject3;
      boolean bool2 = zzbw(paramInt);
      if (!bool2) {
        break label557;
      }
      k = 10436;
      localObject2 = zzaej;
      ((AtomicBoolean)localObject2).set(false);
      n = k;
      label270:
      localObject3 = (NotificationManager)paramContext.getSystemService("notification");
      if (paramString == null) {
        break label569;
      }
      ((NotificationManager)localObject3).notify(paramString, n, (Notification)localObject1);
    }
    for (;;)
    {
      return;
      m = a.b.common_google_play_services_notification_ticker;
      localObject1 = ((Resources)localObject1).getString(m);
      boolean bool4 = h.a();
      if (bool4)
      {
        localObject5 = new android/app/Notification$Builder;
        ((Notification.Builder)localObject5).<init>(paramContext);
        localObject5 = ((Notification.Builder)localObject5).setSmallIcon(i);
        localObject3 = ((Notification.Builder)localObject5).setContentTitle((CharSequence)localObject3).setContentText((CharSequence)localObject2).setContentIntent((PendingIntent)localObject4).setTicker((CharSequence)localObject1).setAutoCancel(bool1);
        boolean bool5 = h.g();
        if (bool5) {
          ((Notification.Builder)localObject3).setLocalOnly(bool1);
        }
        bool5 = h.d();
        if (bool5)
        {
          localObject1 = new android/app/Notification$BigTextStyle;
          ((Notification.BigTextStyle)localObject1).<init>();
          localObject1 = ((Notification.BigTextStyle)localObject1).bigText((CharSequence)localObject2);
          ((Notification.Builder)localObject3).setStyle((Notification.Style)localObject1);
        }
        for (localObject3 = ((Notification.Builder)localObject3).build();; localObject3 = ((Notification.Builder)localObject3).getNotification())
        {
          int i1 = Build.VERSION.SDK_INT;
          n = 19;
          if (i1 == n)
          {
            localObject1 = extras;
            localObject2 = "android.support.localOnly";
            ((Bundle)localObject1).putBoolean((String)localObject2, bool1);
          }
          localObject1 = localObject3;
          break;
        }
      }
      localObject5 = new android/support/v4/app/aa$d;
      ((aa.d)localObject5).<init>(paramContext);
      localObject5 = ((aa.d)localObject5).a(i);
      localObject1 = ((aa.d)localObject5).c((CharSequence)localObject1);
      long l = System.currentTimeMillis();
      localObject3 = ((aa.d)localObject1).a(l).a(bool1).a((PendingIntent)localObject4).a((CharSequence)localObject3).b((CharSequence)localObject2).a();
      localObject1 = localObject3;
      break;
      label557:
      k = 39789;
      n = k;
      break label270;
      label569:
      ((NotificationManager)localObject3).notify(n, (Notification)localObject1);
    }
  }
  
  public static void zza(Activity paramActivity, DialogInterface.OnCancelListener paramOnCancelListener, String paramString, Dialog paramDialog)
  {
    boolean bool = paramActivity instanceof l;
    Object localObject2;
    if (bool)
    {
      paramActivity = (l)paramActivity;
      localObject1 = paramActivity.f();
      localObject2 = c.a(paramDialog, paramOnCancelListener);
      ((c)localObject2).a((android.support.v4.app.p)localObject1, paramString);
    }
    for (;;)
    {
      return;
      bool = h.a();
      if (!bool) {
        break;
      }
      localObject1 = paramActivity.getFragmentManager();
      localObject2 = a.a(paramDialog, paramOnCancelListener);
      ((a)localObject2).show((FragmentManager)localObject1, paramString);
    }
    Object localObject1 = new java/lang/RuntimeException;
    ((RuntimeException)localObject1).<init>("This Activity does not support Fragments.");
    throw ((Throwable)localObject1);
  }
  
  public static void zzac(Context paramContext)
  {
    b localb = b.a();
    int i = localb.a(paramContext);
    if (i != 0)
    {
      Object localObject1 = b.a().a(paramContext, i, "e");
      Object localObject2 = "GooglePlayServicesUtil";
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      String str = "GooglePlayServices not available due to error ";
      localObject3 = str + i;
      Log.e((String)localObject2, (String)localObject3);
      if (localObject1 == null)
      {
        localObject1 = new com/google/android/gms/common/GooglePlayServicesNotAvailableException;
        ((GooglePlayServicesNotAvailableException)localObject1).<init>(i);
        throw ((Throwable)localObject1);
      }
      localObject2 = new com/google/android/gms/common/GooglePlayServicesRepairableException;
      ((GooglePlayServicesRepairableException)localObject2).<init>(i, "Google Play Services not available", (Intent)localObject1);
      throw ((Throwable)localObject2);
    }
  }
  
  public static void zzaj(Context paramContext)
  {
    Object localObject = zzaej;
    boolean bool1 = true;
    boolean bool2 = ((AtomicBoolean)localObject).getAndSet(bool1);
    if (bool2) {}
    for (;;)
    {
      return;
      localObject = "notification";
      try
      {
        localObject = paramContext.getSystemService((String)localObject);
        localObject = (NotificationManager)localObject;
        int i = 10436;
        ((NotificationManager)localObject).cancel(i);
      }
      catch (SecurityException localSecurityException) {}
    }
  }
  
  private static void zzak(Context paramContext)
  {
    Object localObject1 = zzaek;
    boolean bool1 = ((AtomicBoolean)localObject1).get();
    if (bool1) {}
    int k;
    int m;
    do
    {
      return;
      for (;;)
      {
        String str;
        synchronized (zzqf)
        {
          localObject1 = zzaeh;
          if (localObject1 == null)
          {
            localObject1 = paramContext.getPackageName();
            zzaeh = (String)localObject1;
            try
            {
              localObject1 = paramContext.getPackageManager();
              localObject5 = paramContext.getPackageName();
              j = 128;
              localObject1 = ((PackageManager)localObject1).getApplicationInfo((String)localObject5, j);
              localObject1 = metaData;
              if (localObject1 == null) {
                continue;
              }
              localObject5 = "com.google.android.gms.version";
              i = ((Bundle)localObject1).getInt((String)localObject5);
              localObject1 = Integer.valueOf(i);
              zzaei = (Integer)localObject1;
            }
            catch (PackageManager.NameNotFoundException localNameNotFoundException)
            {
              int i;
              localObject5 = "GooglePlayServicesUtil";
              str = "This should never happen.";
              Log.wtf((String)localObject5, str, localNameNotFoundException);
              continue;
            }
            localObject1 = zzaei;
            if (localObject1 != null) {
              break;
            }
            localObject1 = new java/lang/IllegalStateException;
            ((IllegalStateException)localObject1).<init>("A required meta-data tag in your app's AndroidManifest.xml does not exist.  You must have the following declaration within the <application> element:     <meta-data android:name=\"com.google.android.gms.version\" android:value=\"@integer/google_play_services_version\" />");
            throw ((Throwable)localObject1);
            i = 0;
            localObject1 = null;
            zzaei = null;
          }
        }
        localObject3 = zzaeh;
        localObject5 = paramContext.getPackageName();
        boolean bool2 = ((String)localObject3).equals(localObject5);
        if (!bool2)
        {
          localObject3 = new java/lang/IllegalArgumentException;
          localObject5 = new java/lang/StringBuilder;
          ((StringBuilder)localObject5).<init>();
          str = "isGooglePlayServicesAvailable should only be called with Context from your application's package. A previous call used package '";
          localObject5 = ((StringBuilder)localObject5).append(str);
          str = zzaeh;
          localObject5 = ((StringBuilder)localObject5).append(str);
          str = "' and this call used package '";
          localObject5 = ((StringBuilder)localObject5).append(str);
          str = paramContext.getPackageName();
          localObject5 = ((StringBuilder)localObject5).append(str);
          str = "'.";
          localObject5 = ((StringBuilder)localObject5).append(str);
          localObject5 = ((StringBuilder)localObject5).toString();
          ((IllegalArgumentException)localObject3).<init>((String)localObject5);
          throw ((Throwable)localObject3);
        }
      }
      k = ((Integer)localObject3).intValue();
      m = GOOGLE_PLAY_SERVICES_VERSION_CODE;
    } while (k == m);
    ??? = new java/lang/IllegalStateException;
    Object localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>();
    localObject5 = ((StringBuilder)localObject5).append("The meta-data tag in your app's AndroidManifest.xml does not have the right value.  Expected ");
    int j = GOOGLE_PLAY_SERVICES_VERSION_CODE;
    Object localObject3 = j + " but" + " found " + localObject3 + ".  You must have the" + " following declaration within the <application> element: " + "    <meta-data android:name=\"" + "com.google.android.gms.version" + "\" android:value=\"@integer/google_play_services_version\" />";
    ((IllegalStateException)???).<init>((String)localObject3);
    throw ((Throwable)???);
  }
  
  private static void zzal(Context paramContext)
  {
    GooglePlayServicesUtil.a locala = new com/google/android/gms/common/GooglePlayServicesUtil$a;
    locala.<init>(paramContext);
    Message localMessage = locala.obtainMessage(1);
    locala.sendMessageDelayed(localMessage, 120000L);
  }
  
  public static String zzam(Context paramContext)
  {
    String str = getApplicationInfoname;
    bool = TextUtils.isEmpty(str);
    Object localObject1;
    PackageManager localPackageManager;
    if (bool)
    {
      str = paramContext.getPackageName();
      localObject1 = paramContext.getApplicationContext();
      localPackageManager = ((Context)localObject1).getPackageManager();
    }
    try
    {
      localObject1 = paramContext.getPackageName();
      localObject1 = localPackageManager.getApplicationInfo((String)localObject1, 0);
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        bool = false;
        Object localObject2 = null;
      }
    }
    if (localObject1 != null) {
      str = localPackageManager.getApplicationLabel((ApplicationInfo)localObject1).toString();
    }
    return str;
  }
  
  public static int zzan(Context paramContext)
  {
    int i = 0;
    PackageInfo localPackageInfo = null;
    try
    {
      PackageManager localPackageManager = paramContext.getPackageManager();
      str2 = "com.google.android.gms";
      localPackageInfo = localPackageManager.getPackageInfo(str2, 0);
      i = versionCode;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        String str1 = "GooglePlayServicesUtil";
        String str2 = "Google Play services is missing.";
        Log.w(str1, str2);
      }
    }
    return i;
  }
  
  public static boolean zzao(Context paramContext)
  {
    PackageManager localPackageManager = paramContext.getPackageManager();
    boolean bool1 = h.h();
    boolean bool2;
    if (bool1)
    {
      String str = "cn.google";
      bool2 = localPackageManager.hasSystemFeature(str);
      if (bool2) {
        bool2 = true;
      }
    }
    for (;;)
    {
      return bool2;
      bool2 = false;
      localPackageManager = null;
    }
  }
  
  public static boolean zzap(Context paramContext)
  {
    boolean bool = h.e();
    Object localObject;
    if (bool)
    {
      localObject = (UserManager)paramContext.getSystemService("user");
      String str1 = paramContext.getPackageName();
      localObject = ((UserManager)localObject).getApplicationRestrictions(str1);
      if (localObject != null)
      {
        str1 = "true";
        String str2 = "restricted_profile";
        localObject = ((Bundle)localObject).getString(str2);
        bool = str1.equals(localObject);
        if (bool) {
          bool = true;
        }
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  public static boolean zzb(Context paramContext, int paramInt, String paramString)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    int i = h.f();
    if (i != 0) {
      localObject = (AppOpsManager)paramContext.getSystemService("appops");
    }
    do
    {
      try
      {
        ((AppOpsManager)localObject).checkPackage(paramInt, paramString);
        bool2 = bool1;
      }
      catch (SecurityException localSecurityException)
      {
        String[] arrayOfString;
        for (;;) {}
      }
      return bool2;
      localObject = paramContext.getPackageManager();
      arrayOfString = ((PackageManager)localObject).getPackagesForUid(paramInt);
    } while ((paramString == null) || (arrayOfString == null));
    i = 0;
    Object localObject = null;
    for (;;)
    {
      int k = arrayOfString.length;
      if (i >= k) {
        break;
      }
      String str = arrayOfString[i];
      boolean bool3 = paramString.equals(str);
      if (bool3)
      {
        bool2 = bool1;
        break;
      }
      int j;
      i += 1;
    }
  }
  
  public static boolean zzb(PackageManager paramPackageManager)
  {
    boolean bool = true;
    for (;;)
    {
      int i;
      synchronized (zzqf)
      {
        i = zzaeg;
        int j = -1;
        Object localObject2;
        if (i == j)
        {
          localObject2 = "com.google.android.gms";
          j = 64;
        }
        try
        {
          localObject2 = paramPackageManager.getPackageInfo((String)localObject2, j);
          g localg = g.a();
          int k = 1;
          f.a[] arrayOfa = new f.a[k];
          Object localObject4 = f.b;
          int m = 1;
          localObject4 = localObject4[m];
          arrayOfa[0] = localObject4;
          localObject2 = localg.a((PackageInfo)localObject2, arrayOfa);
          if (localObject2 == null) {
            continue;
          }
          i = 1;
          zzaeg = i;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
          i = 0;
          Object localObject3 = null;
          zzaeg = 0;
          continue;
        }
        i = zzaeg;
        if (i != 0)
        {
          return bool;
          i = 0;
          localObject2 = null;
          zzaeg = 0;
        }
      }
      bool = false;
      Object localObject6 = null;
    }
  }
  
  public static boolean zzb(PackageManager paramPackageManager, String paramString)
  {
    return g.a().a(paramPackageManager, paramString);
  }
  
  public static Intent zzbv(int paramInt)
  {
    return b.a().a(null, paramInt, null);
  }
  
  private static boolean zzbw(int paramInt)
  {
    switch (paramInt)
    {
    }
    for (boolean bool = false;; bool = true) {
      return bool;
    }
  }
  
  public static boolean zzc(PackageManager paramPackageManager)
  {
    boolean bool = zzb(paramPackageManager);
    if (!bool)
    {
      bool = zzow();
      if (bool) {
        break label21;
      }
    }
    label21:
    for (bool = true;; bool = false) {
      return bool;
    }
  }
  
  public static boolean zzd(Context paramContext, int paramInt)
  {
    int i = 1;
    int j = 18;
    if (paramInt == j) {}
    for (;;)
    {
      return i;
      String str;
      boolean bool;
      if (paramInt == i)
      {
        str = "com.google.android.gms";
        bool = zzh(paramContext, str);
      }
      else
      {
        bool = false;
        str = null;
      }
    }
  }
  
  public static boolean zze(Context paramContext, int paramInt)
  {
    Object localObject = "com.google.android.gms";
    boolean bool = zzb(paramContext, paramInt, (String)localObject);
    if (bool)
    {
      localObject = paramContext.getPackageManager();
      String str = "com.google.android.gms";
      bool = zzb((PackageManager)localObject, str);
      if (bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
  
  public static boolean zzf(Context paramContext, int paramInt)
  {
    int i = 9;
    String str;
    boolean bool;
    if (paramInt == i)
    {
      str = "com.android.vending";
      bool = zzh(paramContext, str);
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  static boolean zzh(Context paramContext, String paramString)
  {
    boolean bool = h.h();
    Object localObject1;
    if (bool)
    {
      localObject1 = paramContext.getPackageManager().getPackageInstaller().getAllSessions();
      Iterator localIterator = ((List)localObject1).iterator();
      do
      {
        bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject1 = ((PackageInstaller.SessionInfo)localIterator.next()).getAppPackageName();
        bool = paramString.equals(localObject1);
      } while (!bool);
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = zzap(paramContext);
      if (bool)
      {
        bool = false;
        localObject1 = null;
      }
      else
      {
        localObject1 = paramContext.getPackageManager();
        int i = 8192;
        try
        {
          localObject1 = ((PackageManager)localObject1).getApplicationInfo(paramString, i);
          bool = enabled;
        }
        catch (PackageManager.NameNotFoundException localNameNotFoundException)
        {
          bool = false;
          Object localObject2 = null;
        }
      }
    }
  }
  
  private static int zzov()
  {
    return 8298000;
  }
  
  public static boolean zzow()
  {
    boolean bool = zzaee;
    if (bool) {}
    String str1;
    String str2;
    for (bool = zzaef;; bool = str1.equals(str2))
    {
      return bool;
      str1 = "user";
      str2 = Build.TYPE;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/common/GooglePlayServicesUtil.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */