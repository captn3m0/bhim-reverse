package com.google.android.gms.gcm;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.measurement.a;

class d
{
  static a a;
  
  public static void a(Context paramContext, Intent paramIntent)
  {
    a(paramContext, "_nr", paramIntent);
  }
  
  private static void a(Context paramContext, String paramString, Intent paramIntent)
  {
    Object localObject1 = paramIntent.getStringExtra("gcm.a.campaign");
    localObject2 = "GcmAnalytics";
    int i = 3;
    boolean bool2 = Log.isLoggable((String)localObject2, i);
    Object localObject4;
    if (bool2)
    {
      localObject2 = "GcmAnalytics";
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append("Sending event=").append(paramString);
      localObject4 = " campaign=";
      localObject3 = (String)localObject4 + (String)localObject1;
      Log.d((String)localObject2, (String)localObject3);
    }
    localObject2 = new android/os/Bundle;
    ((Bundle)localObject2).<init>();
    Object localObject3 = "nc";
    ((Bundle)localObject2).putString((String)localObject3, (String)localObject1);
    localObject1 = paramIntent.getStringExtra("from");
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool1)
    {
      localObject3 = "/topics/";
      bool1 = ((String)localObject1).startsWith((String)localObject3);
      if (!bool1) {
        break label179;
      }
      localObject3 = "nt";
      ((Bundle)localObject2).putString((String)localObject3, (String)localObject1);
    }
    for (;;)
    {
      try
      {
        localObject1 = a;
        if (localObject1 != null) {
          continue;
        }
        localObject1 = a.a(paramContext);
        localObject3 = "gcm";
        ((a)localObject1).a((String)localObject3, paramString, (Bundle)localObject2);
        return;
      }
      catch (NoClassDefFoundError localNoClassDefFoundError)
      {
        label179:
        String str2;
        String str3;
        String str1 = "GcmAnalytics";
        localObject2 = "Unable to log event, missing GMS measurement library";
        Log.e(str1, (String)localObject2);
        continue;
      }
      try
      {
        Long.parseLong((String)localObject1);
        localObject3 = "nsid";
        ((Bundle)localObject2).putString((String)localObject3, (String)localObject1);
      }
      catch (NumberFormatException localNumberFormatException)
      {
        str2 = "GcmAnalytics";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        str3 = "Unrecognised from address: ";
        localObject4 = ((StringBuilder)localObject4).append(str3);
        localObject1 = (String)localObject1;
        Log.d(str2, (String)localObject1);
      }
      continue;
      localObject1 = a;
    }
  }
  
  public static void b(Context paramContext, Intent paramIntent)
  {
    a(paramContext, "_no", paramIntent);
  }
  
  public static void c(Context paramContext, Intent paramIntent)
  {
    a(paramContext, "_nd", paramIntent);
  }
  
  public static void d(Context paramContext, Intent paramIntent)
  {
    a(paramContext, "_nf", paramIntent);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/gcm/d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */