package com.google.android.gms.gcm;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import com.google.android.gms.iid.a;
import com.google.android.gms.iid.e;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class c
{
  public static int a = 5000000;
  public static int b = 6500000;
  public static int c = 7000000;
  static c d;
  private static final AtomicInteger i;
  final Messenger e;
  private Context f;
  private PendingIntent g;
  private Map h;
  private final BlockingQueue j;
  
  static
  {
    AtomicInteger localAtomicInteger = new java/util/concurrent/atomic/AtomicInteger;
    localAtomicInteger.<init>(1);
    i = localAtomicInteger;
  }
  
  public c()
  {
    Object localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>();
    j = ((BlockingQueue)localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    localObject = Collections.synchronizedMap((Map)localObject);
    h = ((Map)localObject);
    localObject = new android/os/Messenger;
    c.1 local1 = new com/google/android/gms/gcm/c$1;
    Looper localLooper = Looper.getMainLooper();
    local1.<init>(this, localLooper);
    ((Messenger)localObject).<init>(local1);
    e = ((Messenger)localObject);
  }
  
  public static c a(Context paramContext)
  {
    synchronized (c.class)
    {
      c localc = d;
      if (localc == null)
      {
        localc = new com/google/android/gms/gcm/c;
        localc.<init>();
        d = localc;
        localc = d;
        Context localContext = paramContext.getApplicationContext();
        f = localContext;
      }
      localc = d;
      return localc;
    }
  }
  
  private void a(String paramString1, String paramString2, long paramLong, int paramInt, Bundle paramBundle)
  {
    if (paramString1 == null)
    {
      localObject1 = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject1).<init>("Missing 'to'");
      throw ((Throwable)localObject1);
    }
    Object localObject1 = new android/content/Intent;
    Object localObject2 = "com.google.android.gcm.intent.SEND";
    ((Intent)localObject1).<init>((String)localObject2);
    if (paramBundle != null) {
      ((Intent)localObject1).putExtras(paramBundle);
    }
    a((Intent)localObject1);
    localObject2 = b(f);
    ((Intent)localObject1).setPackage((String)localObject2);
    ((Intent)localObject1).putExtra("google.to", paramString1);
    ((Intent)localObject1).putExtra("google.message_id", paramString2);
    Object localObject3 = Long.toString(paramLong);
    ((Intent)localObject1).putExtra("google.ttl", (String)localObject3);
    localObject3 = Integer.toString(paramInt);
    ((Intent)localObject1).putExtra("google.delay", (String)localObject3);
    localObject2 = b(f);
    localObject3 = ".gsf";
    boolean bool1 = ((String)localObject2).contains((CharSequence)localObject3);
    if (bool1)
    {
      localObject3 = new android/os/Bundle;
      ((Bundle)localObject3).<init>();
      localObject1 = paramBundle.keySet();
      Object localObject4 = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject4).hasNext();
        if (!bool2) {
          break;
        }
        localObject1 = (String)((Iterator)localObject4).next();
        localObject2 = paramBundle.get((String)localObject1);
        boolean bool3 = localObject2 instanceof String;
        if (bool3)
        {
          Object localObject5 = new java/lang/StringBuilder;
          ((StringBuilder)localObject5).<init>();
          String str = "gcm.";
          localObject5 = str + (String)localObject1;
          localObject1 = localObject2;
          localObject1 = (String)localObject2;
          ((Bundle)localObject3).putString((String)localObject5, (String)localObject1);
        }
      }
      ((Bundle)localObject3).putString("google.to", paramString1);
      ((Bundle)localObject3).putString("google.message_id", paramString2);
      localObject1 = a.b(f);
      localObject2 = "GCM";
      localObject4 = "upstream";
      ((a)localObject1).b((String)localObject2, (String)localObject4, (Bundle)localObject3);
    }
    for (;;)
    {
      return;
      localObject2 = f;
      localObject3 = "com.google.android.gtalkservice.permission.GTALK_SERVICE";
      ((Context)localObject2).sendOrderedBroadcast((Intent)localObject1, (String)localObject3);
    }
  }
  
  public static String b(Context paramContext)
  {
    return e.a(paramContext);
  }
  
  private boolean b(Intent paramIntent)
  {
    Object localObject1 = paramIntent.getStringExtra("In-Reply-To");
    Object localObject2;
    if (localObject1 == null)
    {
      localObject2 = "error";
      boolean bool1 = paramIntent.hasExtra((String)localObject2);
      if (bool1) {
        localObject1 = paramIntent.getStringExtra("google.message_id");
      }
    }
    boolean bool2;
    if (localObject1 != null)
    {
      localObject2 = h;
      localObject1 = (Handler)((Map)localObject2).remove(localObject1);
      if (localObject1 != null)
      {
        localObject2 = Message.obtain();
        obj = paramIntent;
        bool2 = ((Handler)localObject1).sendMessage((Message)localObject2);
      }
    }
    for (;;)
    {
      return bool2;
      bool2 = false;
      localObject1 = null;
    }
  }
  
  public static int c(Context paramContext)
  {
    Object localObject = paramContext.getPackageManager();
    try
    {
      String str = b(paramContext);
      localObject = ((PackageManager)localObject).getPackageInfo(str, 0);
      k = versionCode;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        int k = -1;
      }
    }
    return k;
  }
  
  void a(Intent paramIntent)
  {
    try
    {
      Object localObject1 = g;
      if (localObject1 == null)
      {
        localObject1 = new android/content/Intent;
        ((Intent)localObject1).<init>();
        localObject3 = "com.google.example.invalidpackage";
        ((Intent)localObject1).setPackage((String)localObject3);
        localObject3 = f;
        localObject1 = PendingIntent.getBroadcast((Context)localObject3, 0, (Intent)localObject1, 0);
        g = ((PendingIntent)localObject1);
      }
      localObject1 = "app";
      Object localObject3 = g;
      paramIntent.putExtra((String)localObject1, (Parcelable)localObject3);
      return;
    }
    finally {}
  }
  
  public void a(String paramString1, String paramString2, long paramLong, Bundle paramBundle)
  {
    a(paramString1, paramString2, paramLong, -1, paramBundle);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/gcm/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */