package com.google.android.gms.gcm;

import android.content.Context;
import android.content.Intent;
import android.os.Build.VERSION;
import android.support.v4.a.k;
import android.util.Base64;

public class GcmReceiver
  extends k
{
  private static String a = "gcm.googleapis.com/refresh";
  
  public void b(Context paramContext, Intent paramIntent)
  {
    a(paramContext, paramIntent);
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    paramIntent.setComponent(null);
    Object localObject = paramContext.getPackageName();
    paramIntent.setPackage((String)localObject);
    int i = Build.VERSION.SDK_INT;
    int k = 18;
    if (i <= k)
    {
      localObject = paramContext.getPackageName();
      paramIntent.removeCategory((String)localObject);
    }
    localObject = paramIntent.getStringExtra("from");
    String str1 = "com.google.android.c2dm.intent.REGISTRATION";
    String str2 = paramIntent.getAction();
    boolean bool2 = str1.equals(str2);
    if (!bool2)
    {
      str1 = "google.com/iid";
      bool2 = str1.equals(localObject);
      if (!bool2)
      {
        str1 = a;
        bool1 = str1.equals(localObject);
        if (!bool1) {
          break label118;
        }
      }
    }
    localObject = "com.google.android.gms.iid.InstanceID";
    paramIntent.setAction((String)localObject);
    label118:
    localObject = paramIntent.getStringExtra("gcm.rawData64");
    if (localObject != null)
    {
      str1 = "rawData";
      str2 = null;
      localObject = Base64.decode((String)localObject, 0);
      paramIntent.putExtra(str1, (byte[])localObject);
      localObject = "gcm.rawData64";
      paramIntent.removeExtra((String)localObject);
    }
    localObject = "com.google.android.c2dm.intent.RECEIVE";
    str1 = paramIntent.getAction();
    boolean bool1 = ((String)localObject).equals(str1);
    if (bool1) {
      b(paramContext, paramIntent);
    }
    for (;;)
    {
      bool1 = isOrderedBroadcast();
      if (bool1)
      {
        int j = -1;
        setResultCode(j);
      }
      return;
      a(paramContext, paramIntent);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/gcm/GcmReceiver.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */