package com.google.android.gms.gcm;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.iid.a;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class b
{
  private static b a;
  private static final Pattern c = Pattern.compile("/topics/[a-zA-Z0-9-_.~%]{1,900}");
  private a b;
  
  private b(Context paramContext)
  {
    a locala = a.b(paramContext);
    b = locala;
  }
  
  public static b a(Context paramContext)
  {
    synchronized (b.class)
    {
      b localb = a;
      if (localb == null)
      {
        localb = new com/google/android/gms/gcm/b;
        localb.<init>(paramContext);
        a = localb;
      }
      localb = a;
      return localb;
    }
  }
  
  public void a(String paramString1, String paramString2, Bundle paramBundle)
  {
    boolean bool;
    Object localObject1;
    Object localObject2;
    if (paramString1 != null)
    {
      bool = paramString1.isEmpty();
      if (!bool) {}
    }
    else
    {
      localObject1 = new java/lang/IllegalArgumentException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Invalid appInstanceToken: " + paramString1;
      ((IllegalArgumentException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    if (paramString2 != null)
    {
      localObject1 = c.matcher(paramString2);
      bool = ((Matcher)localObject1).matches();
      if (bool) {}
    }
    else
    {
      localObject1 = new java/lang/IllegalArgumentException;
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Invalid topic name: " + paramString2;
      ((IllegalArgumentException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    if (paramBundle == null)
    {
      paramBundle = new android/os/Bundle;
      paramBundle.<init>();
    }
    paramBundle.putString("gcm.topic", paramString2);
    b.a(paramString1, paramString2, paramBundle);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/gcm/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */