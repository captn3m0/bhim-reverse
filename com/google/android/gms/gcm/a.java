package com.google.android.gms.gcm;

import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import java.util.concurrent.Executor;

public abstract class a
  extends Service
{
  private static boolean d = false;
  private final Object a;
  private int b;
  private int c;
  
  public a()
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
    c = 0;
  }
  
  static boolean a(Intent paramIntent)
  {
    boolean bool = d;
    String str;
    if (bool)
    {
      str = paramIntent.getStringExtra("gcm.a.campaign");
      bool = TextUtils.isEmpty(str);
      if (!bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  static boolean a(Bundle paramBundle)
  {
    boolean bool = d;
    String str;
    if (bool)
    {
      str = paramBundle.getString("gcm.a.campaign");
      bool = TextUtils.isEmpty(str);
      if (!bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  private void b()
  {
    synchronized (a)
    {
      int i = c + -1;
      c = i;
      i = c;
      if (i == 0)
      {
        i = b;
        a(i);
      }
      return;
    }
  }
  
  private void b(Intent paramIntent)
  {
    PendingIntent localPendingIntent = (PendingIntent)paramIntent.getParcelableExtra("com.google.android.gms.gcm.PENDING_INTENT");
    if (localPendingIntent != null) {}
    try
    {
      localPendingIntent.send();
      d.b(this, paramIntent);
      return;
    }
    catch (PendingIntent.CanceledException localCanceledException)
    {
      for (;;)
      {
        String str1 = "GcmListenerService";
        String str2 = "Notification pending intent canceled";
        Log.e(str1, str2);
      }
    }
  }
  
  private void c(Intent paramIntent)
  {
    int i = Build.VERSION.SDK_INT;
    int j = 11;
    Object localObject1;
    Object localObject2;
    if (i >= j)
    {
      localObject1 = AsyncTask.THREAD_POOL_EXECUTOR;
      localObject2 = new com/google/android/gms/gcm/a$1;
      ((a.1)localObject2).<init>(this, paramIntent);
      ((Executor)localObject1).execute((Runnable)localObject2);
    }
    for (;;)
    {
      return;
      localObject1 = new com/google/android/gms/gcm/a$2;
      ((a.2)localObject1).<init>(this, paramIntent);
      j = 0;
      localObject2 = new Void[0];
      ((a.2)localObject1).execute((Object[])localObject2);
    }
  }
  
  private void d(Intent paramIntent)
  {
    for (;;)
    {
      try
      {
        Object localObject1 = paramIntent.getAction();
        int i = -1;
        int j = ((String)localObject1).hashCode();
        String str1;
        switch (j)
        {
        default: 
          switch (i)
          {
          default: 
            str1 = "GcmListenerService";
            localObject1 = new java/lang/StringBuilder;
            ((StringBuilder)localObject1).<init>();
            str2 = "Unknown intent action: ";
            localObject1 = ((StringBuilder)localObject1).append(str2);
            str2 = paramIntent.getAction();
            localObject1 = ((StringBuilder)localObject1).append(str2);
            localObject1 = ((StringBuilder)localObject1).toString();
            Log.d(str1, (String)localObject1);
            b();
            return;
          }
        case 366519424: 
          str2 = "com.google.android.c2dm.intent.RECEIVE";
          bool = ((String)localObject1).equals(str2);
          if (!bool) {
            continue;
          }
          i = 0;
          str1 = null;
          break;
        }
        String str2 = "com.google.android.gms.gcm.NOTIFICATION_DISMISS";
        boolean bool = ((String)localObject1).equals(str2);
        if (bool)
        {
          i = 1;
          continue;
          e(paramIntent);
          continue;
          d.c(this, paramIntent);
        }
      }
      finally
      {
        GcmReceiver.a(paramIntent);
      }
    }
  }
  
  private void e(Intent paramIntent)
  {
    String str1 = paramIntent.getStringExtra("message_type");
    if (str1 == null) {
      str1 = "gcm";
    }
    int i = -1;
    int j = str1.hashCode();
    String str2;
    Object localObject;
    switch (j)
    {
    default: 
      switch (i)
      {
      default: 
        str2 = "GcmListenerService";
        localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        String str3 = "Received message with unknown type: ";
        localObject = ((StringBuilder)localObject).append(str3);
        str1 = str1;
        Log.w(str2, str1);
      }
      break;
    }
    for (;;)
    {
      return;
      localObject = "gcm";
      boolean bool1 = str1.equals(localObject);
      if (!bool1) {
        break;
      }
      i = 0;
      str2 = null;
      break;
      localObject = "deleted_messages";
      bool1 = str1.equals(localObject);
      if (!bool1) {
        break;
      }
      i = 1;
      break;
      localObject = "send_event";
      bool1 = str1.equals(localObject);
      if (!bool1) {
        break;
      }
      i = 2;
      break;
      localObject = "send_error";
      bool1 = str1.equals(localObject);
      if (!bool1) {
        break;
      }
      i = 3;
      break;
      boolean bool2 = a(paramIntent);
      if (bool2) {
        d.a(this, paramIntent);
      }
      f(paramIntent);
      continue;
      a();
      continue;
      str1 = paramIntent.getStringExtra("google.message_id");
      a(str1);
      continue;
      str1 = paramIntent.getStringExtra("google.message_id");
      str2 = paramIntent.getStringExtra("error");
      a(str1, str2);
    }
  }
  
  private void f(Intent paramIntent)
  {
    Bundle localBundle = paramIntent.getExtras();
    localBundle.remove("message_type");
    Object localObject = "android.support.content.wakelockid";
    localBundle.remove((String)localObject);
    boolean bool = e.a(localBundle);
    if (bool)
    {
      bool = e.a(this);
      if (!bool)
      {
        localObject = getClass();
        localObject = e.a(this, (Class)localObject);
        ((e)localObject).c(localBundle);
      }
    }
    for (;;)
    {
      return;
      e.b(localBundle);
      bool = a(paramIntent);
      if (bool) {
        d.d(this, paramIntent);
      }
      localObject = localBundle.getString("from");
      String str = "from";
      localBundle.remove(str);
      a((String)localObject, localBundle);
    }
  }
  
  public void a() {}
  
  public void a(String paramString) {}
  
  public void a(String paramString, Bundle paramBundle) {}
  
  public void a(String paramString1, String paramString2) {}
  
  boolean a(int paramInt)
  {
    return stopSelfResult(paramInt);
  }
  
  public final IBinder onBind(Intent paramIntent)
  {
    return null;
  }
  
  public final int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    for (;;)
    {
      synchronized (a)
      {
        b = paramInt2;
        int i = c + 1;
        c = i;
        String str = "com.google.android.gms.gcm.NOTIFICATION_OPEN";
        ??? = paramIntent.getAction();
        boolean bool = str.equals(???);
        if (bool)
        {
          b(paramIntent);
          b();
          GcmReceiver.a(paramIntent);
          return 3;
        }
      }
      c(paramIntent);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/gcm/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */