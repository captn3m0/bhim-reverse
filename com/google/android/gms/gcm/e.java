package com.google.android.gms.gcm;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.app.KeyguardManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Process;
import android.os.SystemClock;
import android.support.v4.app.aa.d;
import android.text.TextUtils;
import android.util.Log;
import java.util.Iterator;
import java.util.List;
import java.util.MissingFormatArgumentException;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;

class e
{
  static e a;
  private final Context b;
  private final Class c;
  
  private e(Context paramContext, Class paramClass)
  {
    Context localContext = paramContext.getApplicationContext();
    b = localContext;
    c = paramClass;
  }
  
  private int a()
  {
    return (int)SystemClock.uptimeMillis();
  }
  
  private PendingIntent a(Bundle paramBundle, PendingIntent paramPendingIntent)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.google.android.gms.gcm.NOTIFICATION_OPEN");
    a(localIntent, paramBundle);
    localIntent.putExtra("com.google.android.gms.gcm.PENDING_INTENT", paramPendingIntent);
    Context localContext = b;
    int i = a();
    return PendingIntent.getService(localContext, i, localIntent, 1073741824);
  }
  
  static e a(Context paramContext, Class paramClass)
  {
    synchronized (e.class)
    {
      e locale = a;
      if (locale == null)
      {
        locale = new com/google/android/gms/gcm/e;
        locale.<init>(paramContext, paramClass);
        a = locale;
      }
      locale = a;
      return locale;
    }
  }
  
  static String a(Bundle paramBundle, String paramString)
  {
    String str1 = paramBundle.getString(paramString);
    if (str1 == null)
    {
      String str2 = "gcm.notification.";
      str1 = paramString.replace("gcm.n.", str2);
      str1 = paramBundle.getString(str1);
    }
    return str1;
  }
  
  private String a(String paramString)
  {
    int i = "gcm.n.".length();
    return paramString.substring(i);
  }
  
  private void a(Intent paramIntent, Bundle paramBundle)
  {
    Object localObject1 = b;
    Object localObject2 = c;
    paramIntent.setClass((Context)localObject1, (Class)localObject2);
    localObject1 = paramBundle.keySet();
    localObject2 = ((Set)localObject1).iterator();
    for (;;)
    {
      boolean bool1 = ((Iterator)localObject2).hasNext();
      if (!bool1) {
        break;
      }
      localObject1 = (String)((Iterator)localObject2).next();
      String str = "gcm.a.";
      boolean bool2 = ((String)localObject1).startsWith(str);
      if (!bool2)
      {
        str = "from";
        bool2 = ((String)localObject1).equals(str);
        if (!bool2) {}
      }
      else
      {
        str = paramBundle.getString((String)localObject1);
        paramIntent.putExtra((String)localObject1, str);
      }
    }
  }
  
  private void a(String paramString, Notification paramNotification)
  {
    Object localObject1 = "GcmNotification";
    int i = 3;
    boolean bool2 = Log.isLoggable((String)localObject1, i);
    if (bool2)
    {
      localObject1 = "GcmNotification";
      localObject2 = "Showing notification";
      Log.d((String)localObject1, (String)localObject2);
    }
    localObject1 = b;
    Object localObject2 = "notification";
    localObject1 = (NotificationManager)((Context)localObject1).getSystemService((String)localObject2);
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (bool1)
    {
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      String str = "GCM-Notification:";
      localObject2 = ((StringBuilder)localObject2).append(str);
      long l = SystemClock.uptimeMillis();
      localObject2 = ((StringBuilder)localObject2).append(l);
      paramString = ((StringBuilder)localObject2).toString();
    }
    ((NotificationManager)localObject1).notify(paramString, 0, paramNotification);
  }
  
  static boolean a(Context paramContext)
  {
    boolean bool1 = false;
    Object localObject = (KeyguardManager)paramContext.getSystemService("keyguard");
    boolean bool2 = ((KeyguardManager)localObject).inKeyguardRestrictedInputMode();
    if (bool2) {}
    do
    {
      return bool1;
      j = Process.myPid();
      localObject = ((ActivityManager)paramContext.getSystemService("activity")).getRunningAppProcesses();
    } while (localObject == null);
    Iterator localIterator = ((List)localObject).iterator();
    int k;
    do
    {
      bool2 = localIterator.hasNext();
      if (!bool2) {
        break;
      }
      localObject = (ActivityManager.RunningAppProcessInfo)localIterator.next();
      k = pid;
    } while (k != j);
    int i = importance;
    int j = 100;
    if (i == j) {
      i = 1;
    }
    for (;;)
    {
      bool1 = i;
      break;
      i = 0;
      localObject = null;
    }
  }
  
  static boolean a(Bundle paramBundle)
  {
    String str = a(paramBundle, "gcm.n.icon");
    boolean bool;
    if (str != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  private int b(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      localObject1 = new com/google/android/gms/gcm/e$a;
      ((e.a)localObject1).<init>(this, "Missing icon", null);
      throw ((Throwable)localObject1);
    }
    Object localObject2 = b.getResources();
    Object localObject1 = "drawable";
    String str = b.getPackageName();
    int i = ((Resources)localObject2).getIdentifier(paramString, (String)localObject1, str);
    if (i != 0) {}
    do
    {
      return i;
      localObject1 = "mipmap";
      str = b.getPackageName();
      i = ((Resources)localObject2).getIdentifier(paramString, (String)localObject1, str);
    } while (i != 0);
    localObject1 = new com/google/android/gms/gcm/e$a;
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = "Icon resource not found: " + paramString;
    ((e.a)localObject1).<init>(this, (String)localObject2, null);
    throw ((Throwable)localObject1);
  }
  
  private String b(Bundle paramBundle, String paramString)
  {
    localObject1 = a(paramBundle, paramString);
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject1);
    if (!bool1) {}
    for (;;)
    {
      return (String)localObject1;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = paramString + "_loc_key";
      String str2 = a(paramBundle, (String)localObject1);
      int i = TextUtils.isEmpty(str2);
      if (i != 0)
      {
        i = 0;
        localObject1 = null;
        continue;
      }
      localObject2 = b.getResources();
      localObject1 = "string";
      localObject3 = b.getPackageName();
      k = ((Resources)localObject2).getIdentifier(str2, (String)localObject1, (String)localObject3);
      if (k == 0)
      {
        localObject1 = new com/google/android/gms/gcm/e$a;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = paramString + "_loc_key";
        localObject3 = a((String)localObject3);
        str2 = (String)localObject3 + " resource not found: " + str2;
        ((e.a)localObject1).<init>(this, str2, null);
        throw ((Throwable)localObject1);
      }
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = ((StringBuilder)localObject1).append(paramString);
      Object localObject4 = "_loc_args";
      localObject1 = (String)localObject4;
      localObject1 = a(paramBundle, (String)localObject1);
      boolean bool2 = TextUtils.isEmpty((CharSequence)localObject1);
      if (bool2)
      {
        localObject1 = ((Resources)localObject2).getString(k);
        continue;
      }
      try
      {
        localObject4 = new org/json/JSONArray;
        ((JSONArray)localObject4).<init>((String)localObject1);
        String[] arrayOfString = new String[((JSONArray)localObject4).length()];
        i = 0;
        localObject1 = null;
        for (;;)
        {
          int m = arrayOfString.length;
          if (i >= m) {
            break;
          }
          Object localObject5 = ((JSONArray)localObject4).opt(i);
          arrayOfString[i] = localObject5;
          int j;
          i += 1;
        }
        try
        {
          localObject1 = ((Resources)localObject2).getString(k, arrayOfString);
        }
        catch (MissingFormatArgumentException localMissingFormatArgumentException)
        {
          e.a locala;
          localObject2 = new com/google/android/gms/gcm/e$a;
          localObject3 = new java/lang/StringBuilder;
          ((StringBuilder)localObject3).<init>();
          String str1 = "Missing format argument for " + locala + ": " + localMissingFormatArgumentException;
          ((e.a)localObject2).<init>(this, str1, null);
          throw ((Throwable)localObject2);
        }
      }
      catch (JSONException localJSONException)
      {
        locala = new com/google/android/gms/gcm/e$a;
        localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("Malformed ");
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = paramString + "_loc_args";
        localObject3 = a((String)localObject3);
        localObject1 = (String)localObject3 + ": " + (String)localObject1;
        locala.<init>(this, (String)localObject1, null);
        throw locala;
      }
    }
  }
  
  static void b(Bundle paramBundle)
  {
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    Object localObject = paramBundle.keySet();
    Iterator localIterator = ((Set)localObject).iterator();
    for (;;)
    {
      bool1 = localIterator.hasNext();
      if (!bool1) {
        break;
      }
      localObject = (String)localIterator.next();
      String str = "gcm.n.";
      boolean bool2 = ((String)localObject).startsWith(str);
      if (bool2)
      {
        int i = "gcm.n.".length();
        str = ((String)localObject).substring(i);
        localObject = paramBundle.getString((String)localObject);
        localBundle.putString(str, (String)localObject);
        localIterator.remove();
      }
      else
      {
        str = "gcm.notification.";
        boolean bool3 = ((String)localObject).startsWith(str);
        if (bool3)
        {
          int j = "gcm.notification.".length();
          str = ((String)localObject).substring(j);
          localObject = paramBundle.getString((String)localObject);
          localBundle.putString(str, (String)localObject);
          localIterator.remove();
        }
      }
    }
    boolean bool1 = localBundle.isEmpty();
    if (!bool1)
    {
      localObject = "notification";
      paramBundle.putBundle((String)localObject, localBundle);
    }
  }
  
  private Uri c(String paramString)
  {
    int i = 0;
    Uri localUri = null;
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {}
    for (;;)
    {
      return localUri;
      localObject1 = "default";
      bool = ((String)localObject1).equals(paramString);
      if (!bool) {
        break;
      }
      i = 2;
      localUri = RingtoneManager.getDefaultUri(i);
    }
    Object localObject1 = new com/google/android/gms/gcm/e$a;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = "Invalid sound: " + paramString;
    ((e.a)localObject1).<init>(this, (String)localObject2, null);
    throw ((Throwable)localObject1);
  }
  
  private Notification d(Bundle paramBundle)
  {
    PendingIntent localPendingIntent = null;
    Object localObject1 = "gcm.n.title";
    Object localObject2 = b(paramBundle, (String)localObject1);
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject2);
    if (bool1)
    {
      localObject1 = new com/google/android/gms/gcm/e$a;
      ((e.a)localObject1).<init>(this, "Missing title", null);
      throw ((Throwable)localObject1);
    }
    String str1 = b(paramBundle, "gcm.n.body");
    localObject1 = a(paramBundle, "gcm.n.icon");
    int i = b((String)localObject1);
    String str2 = a(paramBundle, "gcm.n.color");
    localObject1 = a(paramBundle, "gcm.n.sound");
    Uri localUri = c((String)localObject1);
    localObject1 = e(paramBundle);
    boolean bool2 = a.a(paramBundle);
    if (bool2)
    {
      localObject1 = a(paramBundle, (PendingIntent)localObject1);
      localPendingIntent = f(paramBundle);
    }
    aa.d locald1 = new android/support/v4/app/aa$d;
    Context localContext = b;
    locald1.<init>(localContext);
    boolean bool3 = true;
    locald1 = locald1.a(bool3);
    aa.d locald2 = locald1.a(i);
    localObject2 = locald2.a((CharSequence)localObject2).b(str1);
    boolean bool4 = TextUtils.isEmpty(str2);
    if (!bool4)
    {
      int j = Color.parseColor(str2);
      ((aa.d)localObject2).b(j);
    }
    if (localUri != null) {
      ((aa.d)localObject2).a(localUri);
    }
    if (localObject1 != null) {
      ((aa.d)localObject2).a((PendingIntent)localObject1);
    }
    if (localPendingIntent != null) {
      ((aa.d)localObject2).b(localPendingIntent);
    }
    return ((aa.d)localObject2).a();
  }
  
  private PendingIntent e(Bundle paramBundle)
  {
    Object localObject = a(paramBundle, "gcm.n.click_action");
    boolean bool1 = TextUtils.isEmpty((CharSequence)localObject);
    int i;
    if (bool1) {
      i = 0;
    }
    Intent localIntent;
    int k;
    int j;
    for (localObject = null;; localObject = PendingIntent.getActivity((Context)localObject, k, localIntent, j))
    {
      return (PendingIntent)localObject;
      localIntent = new android/content/Intent;
      localIntent.<init>((String)localObject);
      localObject = b.getPackageName();
      localIntent.setPackage((String)localObject);
      i = 268435456;
      localIntent.setFlags(i);
      localIntent.putExtras(paramBundle);
      localObject = paramBundle.keySet();
      Iterator localIterator = ((Set)localObject).iterator();
      for (;;)
      {
        boolean bool2 = localIterator.hasNext();
        if (!bool2) {
          break;
        }
        localObject = (String)localIterator.next();
        String str = "gcm.n.";
        boolean bool3 = ((String)localObject).startsWith(str);
        if (!bool3)
        {
          str = "gcm.notification.";
          bool3 = ((String)localObject).startsWith(str);
          if (!bool3) {}
        }
        else
        {
          localIntent.removeExtra((String)localObject);
        }
      }
      localObject = b;
      k = a();
      j = 1073741824;
    }
  }
  
  private PendingIntent f(Bundle paramBundle)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.google.android.gms.gcm.NOTIFICATION_DISMISS");
    a(localIntent, paramBundle);
    Context localContext = b;
    int i = a();
    return PendingIntent.getService(localContext, i, localIntent, 1073741824);
  }
  
  boolean c(Bundle paramBundle)
  {
    try
    {
      Notification localNotification = d(paramBundle);
      str2 = "gcm.n.tag";
      str2 = a(paramBundle, str2);
      a(str2, localNotification);
      bool = true;
    }
    catch (e.a locala)
    {
      for (;;)
      {
        String str2 = "GcmNotification";
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        String str3 = "Failed to show notification: ";
        localStringBuilder = localStringBuilder.append(str3);
        String str1 = locala.getMessage();
        str1 = str1;
        Log.w(str2, str1);
        boolean bool = false;
        str1 = null;
      }
    }
    return bool;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/gcm/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */