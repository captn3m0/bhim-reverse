package com.google.android.gms.internal;

import android.os.Binder;

public abstract class b
{
  private static final Object c;
  private static b.a d = null;
  private static int e = 0;
  private static String f = "com.google.android.providers.gsf.permission.READ_GSERVICES";
  protected final String a;
  protected final Object b;
  private Object g = null;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    c = localObject;
  }
  
  protected b(String paramString, Object paramObject)
  {
    a = paramString;
    b = paramObject;
  }
  
  public static int a()
  {
    return e;
  }
  
  public static b a(String paramString, Integer paramInteger)
  {
    b.3 local3 = new com/google/android/gms/internal/b$3;
    local3.<init>(paramString, paramInteger);
    return local3;
  }
  
  public static b a(String paramString, Long paramLong)
  {
    b.2 local2 = new com/google/android/gms/internal/b$2;
    local2.<init>(paramString, paramLong);
    return local2;
  }
  
  public static b a(String paramString1, String paramString2)
  {
    b.4 local4 = new com/google/android/gms/internal/b$4;
    local4.<init>(paramString1, paramString2);
    return local4;
  }
  
  public static b a(String paramString, boolean paramBoolean)
  {
    b.1 local1 = new com/google/android/gms/internal/b$1;
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    local1.<init>(paramString, localBoolean);
    return local1;
  }
  
  public static boolean b()
  {
    b.a locala = d;
    boolean bool;
    if (locala != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      locala = null;
    }
  }
  
  protected abstract Object a(String paramString);
  
  public final Object c()
  {
    Object localObject = g;
    if (localObject != null) {}
    for (localObject = g;; localObject = a((String)localObject))
    {
      return localObject;
      localObject = a;
    }
  }
  
  public final Object d()
  {
    long l = Binder.clearCallingIdentity();
    try
    {
      Object localObject1 = c();
      return localObject1;
    }
    finally
    {
      Binder.restoreCallingIdentity(l);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */