package com.google.android.gms.internal;

public final class j$d
  extends p
{
  private static volatile d[] B;
  public Integer a;
  public j.a[] b;
  public j.e[] c;
  public Long d;
  public Long e;
  public Long f;
  public Long g;
  public Long h;
  public String i;
  public String j;
  public String k;
  public String l;
  public Integer m;
  public String n;
  public String o;
  public String p;
  public Long q;
  public Long r;
  public String s;
  public Boolean t;
  public String u;
  public Long v;
  public Integer w;
  public String x;
  public String y;
  public Boolean z;
  
  public j$d()
  {
    b();
  }
  
  public static d[] a()
  {
    d[] arrayOfd = B;
    if (arrayOfd == null) {}
    synchronized (o.a)
    {
      arrayOfd = B;
      if (arrayOfd == null)
      {
        arrayOfd = null;
        arrayOfd = new d[0];
        B = arrayOfd;
      }
      return B;
    }
  }
  
  public d a(n paramn)
  {
    for (;;)
    {
      int i1 = paramn.a();
      int i2;
      Object localObject1;
      int i4;
      Object localObject2;
      Object localObject3;
      int i5;
      long l1;
      switch (i1)
      {
      default: 
        boolean bool1 = r.a(paramn, i1);
        if (bool1) {}
        break;
      case 0: 
        return this;
      case 8: 
        i2 = paramn.e();
        localObject1 = Integer.valueOf(i2);
        a = ((Integer)localObject1);
        break;
      case 18: 
        i2 = 18;
        i4 = r.b(paramn, i2);
        localObject1 = b;
        if (localObject1 == null)
        {
          i2 = 0;
          localObject1 = null;
        }
        for (;;)
        {
          i4 += i2;
          localObject2 = new j.a[i4];
          if (i2 != 0)
          {
            localObject3 = b;
            System.arraycopy(localObject3, 0, localObject2, 0, i2);
          }
          for (;;)
          {
            i5 = localObject2.length + -1;
            if (i2 >= i5) {
              break;
            }
            localObject3 = new com/google/android/gms/internal/j$a;
            ((j.a)localObject3).<init>();
            localObject2[i2] = localObject3;
            localObject3 = localObject2[i2];
            paramn.a((p)localObject3);
            paramn.a();
            i2 += 1;
          }
          localObject1 = b;
          i2 = localObject1.length;
        }
        localObject3 = new com/google/android/gms/internal/j$a;
        ((j.a)localObject3).<init>();
        localObject2[i2] = localObject3;
        localObject1 = localObject2[i2];
        paramn.a((p)localObject1);
        b = ((j.a[])localObject2);
        break;
      case 26: 
        i2 = 26;
        i4 = r.b(paramn, i2);
        localObject1 = c;
        if (localObject1 == null)
        {
          i2 = 0;
          localObject1 = null;
        }
        for (;;)
        {
          i4 += i2;
          localObject2 = new j.e[i4];
          if (i2 != 0)
          {
            localObject3 = c;
            System.arraycopy(localObject3, 0, localObject2, 0, i2);
          }
          for (;;)
          {
            i5 = localObject2.length + -1;
            if (i2 >= i5) {
              break;
            }
            localObject3 = new com/google/android/gms/internal/j$e;
            ((j.e)localObject3).<init>();
            localObject2[i2] = localObject3;
            localObject3 = localObject2[i2];
            paramn.a((p)localObject3);
            paramn.a();
            i2 += 1;
          }
          localObject1 = c;
          i2 = localObject1.length;
        }
        localObject3 = new com/google/android/gms/internal/j$e;
        ((j.e)localObject3).<init>();
        localObject2[i2] = localObject3;
        localObject1 = localObject2[i2];
        paramn.a((p)localObject1);
        c = ((j.e[])localObject2);
        break;
      case 32: 
        l1 = paramn.d();
        localObject1 = Long.valueOf(l1);
        d = ((Long)localObject1);
        break;
      case 40: 
        l1 = paramn.d();
        localObject1 = Long.valueOf(l1);
        e = ((Long)localObject1);
        break;
      case 48: 
        l1 = paramn.d();
        localObject1 = Long.valueOf(l1);
        f = ((Long)localObject1);
        break;
      case 56: 
        l1 = paramn.d();
        localObject1 = Long.valueOf(l1);
        h = ((Long)localObject1);
        break;
      case 66: 
        localObject1 = paramn.g();
        i = ((String)localObject1);
        break;
      case 74: 
        localObject1 = paramn.g();
        j = ((String)localObject1);
        break;
      case 82: 
        localObject1 = paramn.g();
        k = ((String)localObject1);
        break;
      case 90: 
        localObject1 = paramn.g();
        l = ((String)localObject1);
        break;
      case 96: 
        i2 = paramn.e();
        localObject1 = Integer.valueOf(i2);
        m = ((Integer)localObject1);
        break;
      case 106: 
        localObject1 = paramn.g();
        n = ((String)localObject1);
        break;
      case 114: 
        localObject1 = paramn.g();
        o = ((String)localObject1);
        break;
      case 130: 
        localObject1 = paramn.g();
        p = ((String)localObject1);
        break;
      case 136: 
        l1 = paramn.d();
        localObject1 = Long.valueOf(l1);
        q = ((Long)localObject1);
        break;
      case 144: 
        l1 = paramn.d();
        localObject1 = Long.valueOf(l1);
        r = ((Long)localObject1);
        break;
      case 154: 
        localObject1 = paramn.g();
        s = ((String)localObject1);
        break;
      case 160: 
        boolean bool2 = paramn.f();
        localObject1 = Boolean.valueOf(bool2);
        t = ((Boolean)localObject1);
        break;
      case 170: 
        localObject1 = paramn.g();
        u = ((String)localObject1);
        break;
      case 176: 
        l1 = paramn.d();
        localObject1 = Long.valueOf(l1);
        v = ((Long)localObject1);
        break;
      case 184: 
        int i3 = paramn.e();
        localObject1 = Integer.valueOf(i3);
        w = ((Integer)localObject1);
        break;
      case 194: 
        localObject1 = paramn.g();
        x = ((String)localObject1);
        break;
      case 202: 
        localObject1 = paramn.g();
        y = ((String)localObject1);
        break;
      case 208: 
        l1 = paramn.d();
        localObject1 = Long.valueOf(l1);
        g = ((Long)localObject1);
        break;
      case 224: 
        boolean bool3 = paramn.f();
        localObject1 = Boolean.valueOf(bool3);
        z = ((Boolean)localObject1);
      }
    }
  }
  
  public void a(zztd paramzztd)
  {
    int i1 = 0;
    Object localObject1 = null;
    Object localObject2 = a;
    int i3;
    Object localObject3;
    int i4;
    if (localObject2 != null)
    {
      i3 = 1;
      localObject3 = a;
      i4 = ((Integer)localObject3).intValue();
      paramzztd.a(i3, i4);
    }
    localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = b;
      i3 = localObject2.length;
      if (i3 > 0)
      {
        i3 = 0;
        localObject2 = null;
        for (;;)
        {
          localObject3 = b;
          i4 = localObject3.length;
          if (i3 >= i4) {
            break;
          }
          localObject3 = b[i3];
          if (localObject3 != null)
          {
            int i5 = 2;
            paramzztd.a(i5, (p)localObject3);
          }
          i3 += 1;
        }
      }
    }
    localObject2 = c;
    if (localObject2 != null)
    {
      localObject2 = c;
      i3 = localObject2.length;
      if (i3 > 0) {
        for (;;)
        {
          localObject2 = c;
          i3 = localObject2.length;
          if (i1 >= i3) {
            break;
          }
          localObject2 = c[i1];
          if (localObject2 != null)
          {
            i4 = 3;
            paramzztd.a(i4, (p)localObject2);
          }
          i1 += 1;
        }
      }
    }
    localObject2 = d;
    long l1;
    if (localObject2 != null)
    {
      i3 = 4;
      localObject1 = d;
      l1 = ((Long)localObject1).longValue();
      paramzztd.a(i3, l1);
    }
    localObject2 = e;
    if (localObject2 != null)
    {
      i3 = 5;
      localObject1 = e;
      l1 = ((Long)localObject1).longValue();
      paramzztd.a(i3, l1);
    }
    localObject2 = f;
    if (localObject2 != null)
    {
      i3 = 6;
      localObject1 = f;
      l1 = ((Long)localObject1).longValue();
      paramzztd.a(i3, l1);
    }
    localObject2 = h;
    if (localObject2 != null)
    {
      i3 = 7;
      localObject1 = h;
      l1 = ((Long)localObject1).longValue();
      paramzztd.a(i3, l1);
    }
    localObject2 = i;
    if (localObject2 != null)
    {
      i3 = 8;
      localObject1 = i;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = j;
    if (localObject2 != null)
    {
      i3 = 9;
      localObject1 = j;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = k;
    if (localObject2 != null)
    {
      i3 = 10;
      localObject1 = k;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = l;
    if (localObject2 != null)
    {
      i3 = 11;
      localObject1 = l;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = m;
    if (localObject2 != null)
    {
      i3 = 12;
      localObject1 = m;
      i1 = ((Integer)localObject1).intValue();
      paramzztd.a(i3, i1);
    }
    localObject2 = n;
    if (localObject2 != null)
    {
      i3 = 13;
      localObject1 = n;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = o;
    if (localObject2 != null)
    {
      i3 = 14;
      localObject1 = o;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = p;
    if (localObject2 != null)
    {
      i3 = 16;
      localObject1 = p;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = q;
    if (localObject2 != null)
    {
      i3 = 17;
      localObject1 = q;
      l1 = ((Long)localObject1).longValue();
      paramzztd.a(i3, l1);
    }
    localObject2 = r;
    if (localObject2 != null)
    {
      i3 = 18;
      localObject1 = r;
      l1 = ((Long)localObject1).longValue();
      paramzztd.a(i3, l1);
    }
    localObject2 = s;
    if (localObject2 != null)
    {
      i3 = 19;
      localObject1 = s;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = t;
    if (localObject2 != null)
    {
      i3 = 20;
      localObject1 = t;
      boolean bool1 = ((Boolean)localObject1).booleanValue();
      paramzztd.a(i3, bool1);
    }
    localObject2 = u;
    if (localObject2 != null)
    {
      i3 = 21;
      localObject1 = u;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = v;
    if (localObject2 != null)
    {
      i3 = 22;
      localObject1 = v;
      l1 = ((Long)localObject1).longValue();
      paramzztd.a(i3, l1);
    }
    localObject2 = w;
    if (localObject2 != null)
    {
      i3 = 23;
      localObject1 = w;
      int i2 = ((Integer)localObject1).intValue();
      paramzztd.a(i3, i2);
    }
    localObject2 = x;
    if (localObject2 != null)
    {
      i3 = 24;
      localObject1 = x;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = y;
    if (localObject2 != null)
    {
      i3 = 25;
      localObject1 = y;
      paramzztd.a(i3, (String)localObject1);
    }
    localObject2 = g;
    if (localObject2 != null)
    {
      i3 = 26;
      localObject1 = g;
      l1 = ((Long)localObject1).longValue();
      paramzztd.a(i3, l1);
    }
    localObject2 = z;
    if (localObject2 != null)
    {
      i3 = 28;
      localObject1 = z;
      boolean bool2 = ((Boolean)localObject1).booleanValue();
      paramzztd.a(i3, bool2);
    }
    super.a(paramzztd);
  }
  
  public d b()
  {
    a = null;
    Object localObject = j.a.a();
    b = ((j.a[])localObject);
    localObject = j.e.a();
    c = ((j.e[])localObject);
    d = null;
    e = null;
    f = null;
    g = null;
    h = null;
    i = null;
    j = null;
    k = null;
    l = null;
    m = null;
    n = null;
    o = null;
    p = null;
    q = null;
    r = null;
    s = null;
    t = null;
    u = null;
    v = null;
    w = null;
    x = null;
    y = null;
    z = null;
    A = -1;
    return this;
  }
  
  protected int c()
  {
    int i1 = 0;
    Object localObject1 = null;
    int i2 = super.c();
    Object localObject2 = a;
    Object localObject3;
    int i3;
    int i4;
    if (localObject2 != null)
    {
      localObject3 = a;
      i3 = ((Integer)localObject3).intValue();
      i4 = zztd.b(1, i3);
      i2 += i4;
    }
    localObject2 = b;
    if (localObject2 != null)
    {
      localObject2 = b;
      i4 = localObject2.length;
      if (i4 > 0)
      {
        i4 = i2;
        i2 = 0;
        for (;;)
        {
          localObject3 = b;
          i3 = localObject3.length;
          if (i2 >= i3) {
            break;
          }
          localObject3 = b[i2];
          if (localObject3 != null)
          {
            int i6 = 2;
            i3 = zztd.b(i6, (p)localObject3);
            i4 += i3;
          }
          i2 += 1;
        }
        i2 = i4;
      }
    }
    localObject2 = c;
    if (localObject2 != null)
    {
      localObject2 = c;
      i4 = localObject2.length;
      if (i4 > 0) {
        for (;;)
        {
          localObject2 = c;
          i4 = localObject2.length;
          if (i1 >= i4) {
            break;
          }
          localObject2 = c[i1];
          if (localObject2 != null)
          {
            i3 = 3;
            i4 = zztd.b(i3, (p)localObject2);
            i2 += i4;
          }
          i1 += 1;
        }
      }
    }
    localObject1 = d;
    long l1;
    if (localObject1 != null)
    {
      localObject2 = d;
      l1 = ((Long)localObject2).longValue();
      i1 = zztd.b(4, l1);
      i2 += i1;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = e;
      l1 = ((Long)localObject2).longValue();
      i1 = zztd.b(5, l1);
      i2 += i1;
    }
    localObject1 = f;
    if (localObject1 != null)
    {
      localObject2 = f;
      l1 = ((Long)localObject2).longValue();
      i1 = zztd.b(6, l1);
      i2 += i1;
    }
    localObject1 = h;
    if (localObject1 != null)
    {
      localObject2 = h;
      l1 = ((Long)localObject2).longValue();
      i1 = zztd.b(7, l1);
      i2 += i1;
    }
    localObject1 = i;
    if (localObject1 != null)
    {
      localObject2 = i;
      i1 = zztd.b(8, (String)localObject2);
      i2 += i1;
    }
    localObject1 = j;
    if (localObject1 != null)
    {
      localObject2 = j;
      i1 = zztd.b(9, (String)localObject2);
      i2 += i1;
    }
    localObject1 = k;
    if (localObject1 != null)
    {
      localObject2 = k;
      i1 = zztd.b(10, (String)localObject2);
      i2 += i1;
    }
    localObject1 = l;
    if (localObject1 != null)
    {
      localObject2 = l;
      i1 = zztd.b(11, (String)localObject2);
      i2 += i1;
    }
    localObject1 = m;
    if (localObject1 != null)
    {
      localObject2 = m;
      i4 = ((Integer)localObject2).intValue();
      i1 = zztd.b(12, i4);
      i2 += i1;
    }
    localObject1 = n;
    if (localObject1 != null)
    {
      localObject2 = n;
      i1 = zztd.b(13, (String)localObject2);
      i2 += i1;
    }
    localObject1 = o;
    if (localObject1 != null)
    {
      localObject2 = o;
      i1 = zztd.b(14, (String)localObject2);
      i2 += i1;
    }
    localObject1 = p;
    if (localObject1 != null)
    {
      localObject2 = p;
      i1 = zztd.b(16, (String)localObject2);
      i2 += i1;
    }
    localObject1 = q;
    if (localObject1 != null)
    {
      localObject2 = q;
      l1 = ((Long)localObject2).longValue();
      i1 = zztd.b(17, l1);
      i2 += i1;
    }
    localObject1 = r;
    if (localObject1 != null)
    {
      localObject2 = r;
      l1 = ((Long)localObject2).longValue();
      i1 = zztd.b(18, l1);
      i2 += i1;
    }
    localObject1 = s;
    if (localObject1 != null)
    {
      localObject2 = s;
      i1 = zztd.b(19, (String)localObject2);
      i2 += i1;
    }
    localObject1 = t;
    if (localObject1 != null)
    {
      localObject2 = t;
      boolean bool1 = ((Boolean)localObject2).booleanValue();
      i1 = zztd.b(20, bool1);
      i2 += i1;
    }
    localObject1 = u;
    if (localObject1 != null)
    {
      localObject2 = u;
      i1 = zztd.b(21, (String)localObject2);
      i2 += i1;
    }
    localObject1 = v;
    if (localObject1 != null)
    {
      localObject2 = v;
      l1 = ((Long)localObject2).longValue();
      i1 = zztd.b(22, l1);
      i2 += i1;
    }
    localObject1 = w;
    if (localObject1 != null)
    {
      localObject2 = w;
      int i5 = ((Integer)localObject2).intValue();
      i1 = zztd.b(23, i5);
      i2 += i1;
    }
    localObject1 = x;
    if (localObject1 != null)
    {
      localObject2 = x;
      i1 = zztd.b(24, (String)localObject2);
      i2 += i1;
    }
    localObject1 = y;
    if (localObject1 != null)
    {
      localObject2 = y;
      i1 = zztd.b(25, (String)localObject2);
      i2 += i1;
    }
    localObject1 = g;
    if (localObject1 != null)
    {
      localObject2 = g;
      l1 = ((Long)localObject2).longValue();
      i1 = zztd.b(26, l1);
      i2 += i1;
    }
    localObject1 = z;
    if (localObject1 != null)
    {
      localObject2 = z;
      boolean bool2 = ((Boolean)localObject2).booleanValue();
      i1 = zztd.b(28, bool2);
      i2 += i1;
    }
    return i2;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {}
    for (;;)
    {
      return bool1;
      boolean bool2 = paramObject instanceof d;
      if (!bool2)
      {
        bool1 = false;
      }
      else
      {
        paramObject = (d)paramObject;
        Object localObject1 = a;
        if (localObject1 == null)
        {
          localObject1 = a;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = a;
          localObject2 = a;
          bool2 = ((Integer)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool1 = false;
            continue;
          }
        }
        localObject1 = b;
        Object localObject2 = b;
        bool2 = o.a((Object[])localObject1, (Object[])localObject2);
        if (!bool2)
        {
          bool1 = false;
        }
        else
        {
          localObject1 = c;
          localObject2 = c;
          bool2 = o.a((Object[])localObject1, (Object[])localObject2);
          if (!bool2)
          {
            bool1 = false;
          }
          else
          {
            localObject1 = d;
            if (localObject1 == null)
            {
              localObject1 = d;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = d;
              localObject2 = d;
              bool2 = ((Long)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = e;
            if (localObject1 == null)
            {
              localObject1 = e;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = e;
              localObject2 = e;
              bool2 = ((Long)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = f;
            if (localObject1 == null)
            {
              localObject1 = f;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = f;
              localObject2 = f;
              bool2 = ((Long)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = g;
            if (localObject1 == null)
            {
              localObject1 = g;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = g;
              localObject2 = g;
              bool2 = ((Long)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = h;
            if (localObject1 == null)
            {
              localObject1 = h;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = h;
              localObject2 = h;
              bool2 = ((Long)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = i;
            if (localObject1 == null)
            {
              localObject1 = i;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = i;
              localObject2 = i;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = j;
            if (localObject1 == null)
            {
              localObject1 = j;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = j;
              localObject2 = j;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = k;
            if (localObject1 == null)
            {
              localObject1 = k;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = k;
              localObject2 = k;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = l;
            if (localObject1 == null)
            {
              localObject1 = l;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = l;
              localObject2 = l;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = m;
            if (localObject1 == null)
            {
              localObject1 = m;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = m;
              localObject2 = m;
              bool2 = ((Integer)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = n;
            if (localObject1 == null)
            {
              localObject1 = n;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = n;
              localObject2 = n;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = o;
            if (localObject1 == null)
            {
              localObject1 = o;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = o;
              localObject2 = o;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = p;
            if (localObject1 == null)
            {
              localObject1 = p;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = p;
              localObject2 = p;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = q;
            if (localObject1 == null)
            {
              localObject1 = q;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = q;
              localObject2 = q;
              bool2 = ((Long)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = r;
            if (localObject1 == null)
            {
              localObject1 = r;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = r;
              localObject2 = r;
              bool2 = ((Long)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = s;
            if (localObject1 == null)
            {
              localObject1 = s;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = s;
              localObject2 = s;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = t;
            if (localObject1 == null)
            {
              localObject1 = t;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = t;
              localObject2 = t;
              bool2 = ((Boolean)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = u;
            if (localObject1 == null)
            {
              localObject1 = u;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = u;
              localObject2 = u;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = v;
            if (localObject1 == null)
            {
              localObject1 = v;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = v;
              localObject2 = v;
              bool2 = ((Long)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = w;
            if (localObject1 == null)
            {
              localObject1 = w;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = w;
              localObject2 = w;
              bool2 = ((Integer)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = x;
            if (localObject1 == null)
            {
              localObject1 = x;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = x;
              localObject2 = x;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = y;
            if (localObject1 == null)
            {
              localObject1 = y;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = y;
              localObject2 = y;
              bool2 = ((String)localObject1).equals(localObject2);
              if (!bool2)
              {
                bool1 = false;
                continue;
              }
            }
            localObject1 = z;
            if (localObject1 == null)
            {
              localObject1 = z;
              if (localObject1 != null) {
                bool1 = false;
              }
            }
            else
            {
              localObject1 = z;
              localObject2 = z;
              bool2 = ((Boolean)localObject1).equals(localObject2);
              if (!bool2) {
                bool1 = false;
              }
            }
          }
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i1 = 0;
    Boolean localBoolean = null;
    int i2 = getClass().getName().hashCode() + 527;
    int i3 = i2 * 31;
    Object localObject1 = a;
    if (localObject1 == null)
    {
      i2 = 0;
      localObject1 = null;
      i2 = (i2 + i3) * 31;
      i3 = o.a(b);
      i2 = (i2 + i3) * 31;
      Object localObject2 = c;
      i3 = o.a((Object[])localObject2);
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = d;
      if (localObject1 != null) {
        break label711;
      }
      i2 = 0;
      localObject1 = null;
      label106:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = e;
      if (localObject1 != null) {
        break label726;
      }
      i2 = 0;
      localObject1 = null;
      label133:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = f;
      if (localObject1 != null) {
        break label741;
      }
      i2 = 0;
      localObject1 = null;
      label160:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = g;
      if (localObject1 != null) {
        break label756;
      }
      i2 = 0;
      localObject1 = null;
      label187:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = h;
      if (localObject1 != null) {
        break label771;
      }
      i2 = 0;
      localObject1 = null;
      label214:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = i;
      if (localObject1 != null) {
        break label786;
      }
      i2 = 0;
      localObject1 = null;
      label241:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = j;
      if (localObject1 != null) {
        break label801;
      }
      i2 = 0;
      localObject1 = null;
      label268:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = k;
      if (localObject1 != null) {
        break label816;
      }
      i2 = 0;
      localObject1 = null;
      label295:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = l;
      if (localObject1 != null) {
        break label831;
      }
      i2 = 0;
      localObject1 = null;
      label322:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = m;
      if (localObject1 != null) {
        break label846;
      }
      i2 = 0;
      localObject1 = null;
      label349:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = n;
      if (localObject1 != null) {
        break label861;
      }
      i2 = 0;
      localObject1 = null;
      label376:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = o;
      if (localObject1 != null) {
        break label876;
      }
      i2 = 0;
      localObject1 = null;
      label403:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = p;
      if (localObject1 != null) {
        break label891;
      }
      i2 = 0;
      localObject1 = null;
      label430:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = q;
      if (localObject1 != null) {
        break label906;
      }
      i2 = 0;
      localObject1 = null;
      label457:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = r;
      if (localObject1 != null) {
        break label921;
      }
      i2 = 0;
      localObject1 = null;
      label484:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = s;
      if (localObject1 != null) {
        break label936;
      }
      i2 = 0;
      localObject1 = null;
      label511:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = t;
      if (localObject1 != null) {
        break label951;
      }
      i2 = 0;
      localObject1 = null;
      label538:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = u;
      if (localObject1 != null) {
        break label966;
      }
      i2 = 0;
      localObject1 = null;
      label565:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = v;
      if (localObject1 != null) {
        break label981;
      }
      i2 = 0;
      localObject1 = null;
      label592:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = w;
      if (localObject1 != null) {
        break label996;
      }
      i2 = 0;
      localObject1 = null;
      label619:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = x;
      if (localObject1 != null) {
        break label1011;
      }
      i2 = 0;
      localObject1 = null;
      label646:
      i2 += i3;
      i3 = i2 * 31;
      localObject1 = y;
      if (localObject1 != null) {
        break label1026;
      }
      i2 = 0;
      localObject1 = null;
      label673:
      i2 = (i2 + i3) * 31;
      localObject2 = z;
      if (localObject2 != null) {
        break label1041;
      }
    }
    for (;;)
    {
      return i2 + i1;
      localObject1 = a;
      i2 = ((Integer)localObject1).hashCode();
      break;
      label711:
      localObject1 = d;
      i2 = ((Long)localObject1).hashCode();
      break label106;
      label726:
      localObject1 = e;
      i2 = ((Long)localObject1).hashCode();
      break label133;
      label741:
      localObject1 = f;
      i2 = ((Long)localObject1).hashCode();
      break label160;
      label756:
      localObject1 = g;
      i2 = ((Long)localObject1).hashCode();
      break label187;
      label771:
      localObject1 = h;
      i2 = ((Long)localObject1).hashCode();
      break label214;
      label786:
      localObject1 = i;
      i2 = ((String)localObject1).hashCode();
      break label241;
      label801:
      localObject1 = j;
      i2 = ((String)localObject1).hashCode();
      break label268;
      label816:
      localObject1 = k;
      i2 = ((String)localObject1).hashCode();
      break label295;
      label831:
      localObject1 = l;
      i2 = ((String)localObject1).hashCode();
      break label322;
      label846:
      localObject1 = m;
      i2 = ((Integer)localObject1).hashCode();
      break label349;
      label861:
      localObject1 = n;
      i2 = ((String)localObject1).hashCode();
      break label376;
      label876:
      localObject1 = o;
      i2 = ((String)localObject1).hashCode();
      break label403;
      label891:
      localObject1 = p;
      i2 = ((String)localObject1).hashCode();
      break label430;
      label906:
      localObject1 = q;
      i2 = ((Long)localObject1).hashCode();
      break label457;
      label921:
      localObject1 = r;
      i2 = ((Long)localObject1).hashCode();
      break label484;
      label936:
      localObject1 = s;
      i2 = ((String)localObject1).hashCode();
      break label511;
      label951:
      localObject1 = t;
      i2 = ((Boolean)localObject1).hashCode();
      break label538;
      label966:
      localObject1 = u;
      i2 = ((String)localObject1).hashCode();
      break label565;
      label981:
      localObject1 = v;
      i2 = ((Long)localObject1).hashCode();
      break label592;
      label996:
      localObject1 = w;
      i2 = ((Integer)localObject1).hashCode();
      break label619;
      label1011:
      localObject1 = x;
      i2 = ((String)localObject1).hashCode();
      break label646;
      label1026:
      localObject1 = y;
      i2 = ((String)localObject1).hashCode();
      break label673;
      label1041:
      localBoolean = z;
      i1 = localBoolean.hashCode();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/j$d.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */