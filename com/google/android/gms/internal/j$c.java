package com.google.android.gms.internal;

public final class j$c
  extends p
{
  public j.d[] a;
  
  public j$c()
  {
    a();
  }
  
  public c a()
  {
    j.d[] arrayOfd = j.d.a();
    a = arrayOfd;
    A = -1;
    return this;
  }
  
  public c a(n paramn)
  {
    for (;;)
    {
      int i = paramn.a();
      switch (i)
      {
      default: 
        boolean bool = r.a(paramn, i);
        if (bool) {}
        break;
      case 0: 
        return this;
      case 10: 
        int j = 10;
        int k = r.b(paramn, j);
        j.d[] arrayOfd1 = a;
        if (arrayOfd1 == null)
        {
          j = 0;
          arrayOfd1 = null;
        }
        j.d[] arrayOfd2;
        for (;;)
        {
          k += j;
          arrayOfd2 = new j.d[k];
          if (j != 0)
          {
            localObject = a;
            System.arraycopy(localObject, 0, arrayOfd2, 0, j);
          }
          for (;;)
          {
            int m = arrayOfd2.length + -1;
            if (j >= m) {
              break;
            }
            localObject = new com/google/android/gms/internal/j$d;
            ((j.d)localObject).<init>();
            arrayOfd2[j] = localObject;
            localObject = arrayOfd2[j];
            paramn.a((p)localObject);
            paramn.a();
            j += 1;
          }
          arrayOfd1 = a;
          j = arrayOfd1.length;
        }
        Object localObject = new com/google/android/gms/internal/j$d;
        ((j.d)localObject).<init>();
        arrayOfd2[j] = localObject;
        arrayOfd1 = arrayOfd2[j];
        paramn.a(arrayOfd1);
        a = arrayOfd2;
      }
    }
  }
  
  public void a(zztd paramzztd)
  {
    j.d[] arrayOfd = a;
    if (arrayOfd != null)
    {
      arrayOfd = a;
      int i = arrayOfd.length;
      if (i > 0)
      {
        i = 0;
        arrayOfd = null;
        for (;;)
        {
          Object localObject = a;
          int j = localObject.length;
          if (i >= j) {
            break;
          }
          localObject = a[i];
          if (localObject != null)
          {
            int k = 1;
            paramzztd.a(k, (p)localObject);
          }
          i += 1;
        }
      }
    }
    super.a(paramzztd);
  }
  
  protected int c()
  {
    int i = super.c();
    j.d[] arrayOfd = a;
    if (arrayOfd != null)
    {
      arrayOfd = a;
      int j = arrayOfd.length;
      if (j > 0)
      {
        j = 0;
        arrayOfd = null;
        for (;;)
        {
          Object localObject = a;
          int k = localObject.length;
          if (j >= k) {
            break;
          }
          localObject = a[j];
          if (localObject != null)
          {
            int m = 1;
            k = zztd.b(m, (p)localObject);
            i += k;
          }
          j += 1;
        }
      }
    }
    return i;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {}
    for (;;)
    {
      return bool1;
      boolean bool2 = paramObject instanceof c;
      if (!bool2)
      {
        bool1 = false;
      }
      else
      {
        paramObject = (c)paramObject;
        j.d[] arrayOfd1 = a;
        j.d[] arrayOfd2 = a;
        bool2 = o.a(arrayOfd1, arrayOfd2);
        if (!bool2) {
          bool1 = false;
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = (getClass().getName().hashCode() + 527) * 31;
    int j = o.a(a);
    return i + j;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/j$c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */