package com.google.android.gms.internal;

public final class j$a
  extends p
{
  private static volatile a[] f;
  public j.b[] a;
  public String b;
  public Long c;
  public Long d;
  public Integer e;
  
  public j$a()
  {
    b();
  }
  
  public static a[] a()
  {
    a[] arrayOfa = f;
    if (arrayOfa == null) {}
    synchronized (o.a)
    {
      arrayOfa = f;
      if (arrayOfa == null)
      {
        arrayOfa = null;
        arrayOfa = new a[0];
        f = arrayOfa;
      }
      return f;
    }
  }
  
  public a a(n paramn)
  {
    for (;;)
    {
      int i = paramn.a();
      int j;
      Object localObject1;
      long l;
      switch (i)
      {
      default: 
        boolean bool = r.a(paramn, i);
        if (bool) {}
        break;
      case 0: 
        return this;
      case 10: 
        j = 10;
        int k = r.b(paramn, j);
        localObject1 = a;
        if (localObject1 == null)
        {
          j = 0;
          localObject1 = null;
        }
        j.b[] arrayOfb;
        for (;;)
        {
          k += j;
          arrayOfb = new j.b[k];
          if (j != 0)
          {
            localObject2 = a;
            System.arraycopy(localObject2, 0, arrayOfb, 0, j);
          }
          for (;;)
          {
            int m = arrayOfb.length + -1;
            if (j >= m) {
              break;
            }
            localObject2 = new com/google/android/gms/internal/j$b;
            ((j.b)localObject2).<init>();
            arrayOfb[j] = localObject2;
            localObject2 = arrayOfb[j];
            paramn.a((p)localObject2);
            paramn.a();
            j += 1;
          }
          localObject1 = a;
          j = localObject1.length;
        }
        Object localObject2 = new com/google/android/gms/internal/j$b;
        ((j.b)localObject2).<init>();
        arrayOfb[j] = localObject2;
        localObject1 = arrayOfb[j];
        paramn.a((p)localObject1);
        a = arrayOfb;
        break;
      case 18: 
        localObject1 = paramn.g();
        b = ((String)localObject1);
        break;
      case 24: 
        l = paramn.d();
        localObject1 = Long.valueOf(l);
        c = ((Long)localObject1);
        break;
      case 32: 
        l = paramn.d();
        localObject1 = Long.valueOf(l);
        d = ((Long)localObject1);
        break;
      case 40: 
        j = paramn.e();
        localObject1 = Integer.valueOf(j);
        e = ((Integer)localObject1);
      }
    }
  }
  
  public void a(zztd paramzztd)
  {
    Object localObject1 = a;
    int i;
    Object localObject2;
    int j;
    if (localObject1 != null)
    {
      localObject1 = a;
      i = localObject1.length;
      if (i > 0)
      {
        i = 0;
        localObject1 = null;
        for (;;)
        {
          localObject2 = a;
          j = localObject2.length;
          if (i >= j) {
            break;
          }
          localObject2 = a[i];
          if (localObject2 != null)
          {
            int k = 1;
            paramzztd.a(k, (p)localObject2);
          }
          i += 1;
        }
      }
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      i = 2;
      localObject2 = b;
      paramzztd.a(i, (String)localObject2);
    }
    localObject1 = c;
    long l;
    if (localObject1 != null)
    {
      i = 3;
      localObject2 = c;
      l = ((Long)localObject2).longValue();
      paramzztd.a(i, l);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      i = 4;
      localObject2 = d;
      l = ((Long)localObject2).longValue();
      paramzztd.a(i, l);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      i = 5;
      localObject2 = e;
      j = ((Integer)localObject2).intValue();
      paramzztd.a(i, j);
    }
    super.a(paramzztd);
  }
  
  public a b()
  {
    j.b[] arrayOfb = j.b.a();
    a = arrayOfb;
    b = null;
    c = null;
    d = null;
    e = null;
    A = -1;
    return this;
  }
  
  protected int c()
  {
    int i = super.c();
    Object localObject1 = a;
    int j;
    Object localObject2;
    int k;
    if (localObject1 != null)
    {
      localObject1 = a;
      j = localObject1.length;
      if (j > 0)
      {
        j = 0;
        localObject1 = null;
        for (;;)
        {
          localObject2 = a;
          k = localObject2.length;
          if (j >= k) {
            break;
          }
          localObject2 = a[j];
          if (localObject2 != null)
          {
            int m = 1;
            k = zztd.b(m, (p)localObject2);
            i += k;
          }
          j += 1;
        }
      }
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = b;
      j = zztd.b(2, (String)localObject2);
      i += j;
    }
    localObject1 = c;
    long l;
    if (localObject1 != null)
    {
      localObject2 = c;
      l = ((Long)localObject2).longValue();
      j = zztd.b(3, l);
      i += j;
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = d;
      l = ((Long)localObject2).longValue();
      j = zztd.b(4, l);
      i += j;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = e;
      k = ((Integer)localObject2).intValue();
      j = zztd.b(5, k);
      i += j;
    }
    return i;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {}
    for (;;)
    {
      return bool1;
      boolean bool2 = paramObject instanceof a;
      if (!bool2)
      {
        bool1 = false;
      }
      else
      {
        paramObject = (a)paramObject;
        Object localObject1 = a;
        Object localObject2 = a;
        bool2 = o.a((Object[])localObject1, (Object[])localObject2);
        if (!bool2)
        {
          bool1 = false;
        }
        else
        {
          localObject1 = b;
          if (localObject1 == null)
          {
            localObject1 = b;
            if (localObject1 != null) {
              bool1 = false;
            }
          }
          else
          {
            localObject1 = b;
            localObject2 = b;
            bool2 = ((String)localObject1).equals(localObject2);
            if (!bool2)
            {
              bool1 = false;
              continue;
            }
          }
          localObject1 = c;
          if (localObject1 == null)
          {
            localObject1 = c;
            if (localObject1 != null) {
              bool1 = false;
            }
          }
          else
          {
            localObject1 = c;
            localObject2 = c;
            bool2 = ((Long)localObject1).equals(localObject2);
            if (!bool2)
            {
              bool1 = false;
              continue;
            }
          }
          localObject1 = d;
          if (localObject1 == null)
          {
            localObject1 = d;
            if (localObject1 != null) {
              bool1 = false;
            }
          }
          else
          {
            localObject1 = d;
            localObject2 = d;
            bool2 = ((Long)localObject1).equals(localObject2);
            if (!bool2)
            {
              bool1 = false;
              continue;
            }
          }
          localObject1 = e;
          if (localObject1 == null)
          {
            localObject1 = e;
            if (localObject1 != null) {
              bool1 = false;
            }
          }
          else
          {
            localObject1 = e;
            localObject2 = e;
            bool2 = ((Integer)localObject1).equals(localObject2);
            if (!bool2) {
              bool1 = false;
            }
          }
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = 0;
    Integer localInteger = null;
    int j = (getClass().getName().hashCode() + 527) * 31;
    Object localObject1 = a;
    int k = o.a((Object[])localObject1);
    j += k;
    k = j * 31;
    Object localObject2 = b;
    if (localObject2 == null)
    {
      j = 0;
      localObject2 = null;
      j += k;
      k = j * 31;
      localObject2 = c;
      if (localObject2 != null) {
        break label154;
      }
      j = 0;
      localObject2 = null;
      label89:
      j += k;
      k = j * 31;
      localObject2 = d;
      if (localObject2 != null) {
        break label169;
      }
      j = 0;
      localObject2 = null;
      label116:
      j = (j + k) * 31;
      localObject1 = e;
      if (localObject1 != null) {
        break label184;
      }
    }
    for (;;)
    {
      return j + i;
      localObject2 = b;
      j = ((String)localObject2).hashCode();
      break;
      label154:
      localObject2 = c;
      j = ((Long)localObject2).hashCode();
      break label89;
      label169:
      localObject2 = d;
      j = ((Long)localObject2).hashCode();
      break label116;
      label184:
      localInteger = e;
      i = localInteger.hashCode();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/j$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */