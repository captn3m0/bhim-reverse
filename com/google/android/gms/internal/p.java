package com.google.android.gms.internal;

public abstract class p
{
  protected volatile int A = -1;
  
  public void a(zztd paramzztd) {}
  
  public abstract p b(n paramn);
  
  protected int c()
  {
    return 0;
  }
  
  public int d()
  {
    int i = A;
    if (i < 0) {
      e();
    }
    return A;
  }
  
  public int e()
  {
    int i = c();
    A = i;
    return i;
  }
  
  public p f()
  {
    return (p)super.clone();
  }
  
  public String toString()
  {
    return q.a(this);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/p.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */