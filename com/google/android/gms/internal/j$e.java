package com.google.android.gms.internal;

public final class j$e
  extends p
{
  private static volatile e[] f;
  public Long a;
  public String b;
  public String c;
  public Long d;
  public Float e;
  
  public j$e()
  {
    b();
  }
  
  public static e[] a()
  {
    e[] arrayOfe = f;
    if (arrayOfe == null) {}
    synchronized (o.a)
    {
      arrayOfe = f;
      if (arrayOfe == null)
      {
        arrayOfe = null;
        arrayOfe = new e[0];
        f = arrayOfe;
      }
      return f;
    }
  }
  
  public e a(n paramn)
  {
    for (;;)
    {
      int i = paramn.a();
      long l;
      Object localObject;
      switch (i)
      {
      default: 
        boolean bool = r.a(paramn, i);
        if (bool) {}
        break;
      case 0: 
        return this;
      case 8: 
        l = paramn.d();
        localObject = Long.valueOf(l);
        a = ((Long)localObject);
        break;
      case 18: 
        localObject = paramn.g();
        b = ((String)localObject);
        break;
      case 26: 
        localObject = paramn.g();
        c = ((String)localObject);
        break;
      case 32: 
        l = paramn.d();
        localObject = Long.valueOf(l);
        d = ((Long)localObject);
        break;
      case 45: 
        float f1 = paramn.c();
        localObject = Float.valueOf(f1);
        e = ((Float)localObject);
      }
    }
  }
  
  public void a(zztd paramzztd)
  {
    Object localObject1 = a;
    int i;
    Object localObject2;
    long l;
    if (localObject1 != null)
    {
      i = 1;
      localObject2 = a;
      l = ((Long)localObject2).longValue();
      paramzztd.a(i, l);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      i = 2;
      localObject2 = b;
      paramzztd.a(i, (String)localObject2);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      i = 3;
      localObject2 = c;
      paramzztd.a(i, (String)localObject2);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      i = 4;
      localObject2 = d;
      l = ((Long)localObject2).longValue();
      paramzztd.a(i, l);
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      i = 5;
      localObject2 = e;
      float f1 = ((Float)localObject2).floatValue();
      paramzztd.a(i, f1);
    }
    super.a(paramzztd);
  }
  
  public e b()
  {
    a = null;
    b = null;
    c = null;
    d = null;
    e = null;
    A = -1;
    return this;
  }
  
  protected int c()
  {
    int i = super.c();
    Object localObject1 = a;
    Object localObject2;
    long l;
    int j;
    if (localObject1 != null)
    {
      localObject2 = a;
      l = ((Long)localObject2).longValue();
      j = zztd.b(1, l);
      i += j;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = b;
      j = zztd.b(2, (String)localObject2);
      i += j;
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = c;
      j = zztd.b(3, (String)localObject2);
      i += j;
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = d;
      l = ((Long)localObject2).longValue();
      j = zztd.b(4, l);
      i += j;
    }
    localObject1 = e;
    if (localObject1 != null)
    {
      localObject2 = e;
      float f1 = ((Float)localObject2).floatValue();
      j = zztd.b(5, f1);
      i += j;
    }
    return i;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {}
    for (;;)
    {
      return bool1;
      boolean bool2 = paramObject instanceof e;
      if (!bool2)
      {
        bool1 = false;
      }
      else
      {
        paramObject = (e)paramObject;
        Object localObject1 = a;
        Object localObject2;
        if (localObject1 == null)
        {
          localObject1 = a;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = a;
          localObject2 = a;
          bool2 = ((Long)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool1 = false;
            continue;
          }
        }
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject1 = b;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = ((String)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool1 = false;
            continue;
          }
        }
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject1 = c;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = c;
          localObject2 = c;
          bool2 = ((String)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool1 = false;
            continue;
          }
        }
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject1 = d;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = d;
          localObject2 = d;
          bool2 = ((Long)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool1 = false;
            continue;
          }
        }
        localObject1 = e;
        if (localObject1 == null)
        {
          localObject1 = e;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = e;
          localObject2 = e;
          bool2 = ((Float)localObject1).equals(localObject2);
          if (!bool2) {
            bool1 = false;
          }
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = 0;
    Float localFloat1 = null;
    int j = getClass().getName().hashCode() + 527;
    int k = j * 31;
    Object localObject = a;
    if (localObject == null)
    {
      j = 0;
      localObject = null;
      j += k;
      k = j * 31;
      localObject = b;
      if (localObject != null) {
        break label160;
      }
      j = 0;
      localObject = null;
      label68:
      j += k;
      k = j * 31;
      localObject = c;
      if (localObject != null) {
        break label175;
      }
      j = 0;
      localObject = null;
      label95:
      j += k;
      k = j * 31;
      localObject = d;
      if (localObject != null) {
        break label190;
      }
      j = 0;
      localObject = null;
      label122:
      j = (j + k) * 31;
      Float localFloat2 = e;
      if (localFloat2 != null) {
        break label205;
      }
    }
    for (;;)
    {
      return j + i;
      localObject = a;
      j = ((Long)localObject).hashCode();
      break;
      label160:
      localObject = b;
      j = ((String)localObject).hashCode();
      break label68;
      label175:
      localObject = c;
      j = ((String)localObject).hashCode();
      break label95;
      label190:
      localObject = d;
      j = ((Long)localObject).hashCode();
      break label122;
      label205:
      localFloat1 = e;
      i = localFloat1.hashCode();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/j$e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */