package com.google.android.gms.internal;

import java.io.IOException;

public class zztj
  extends IOException
{
  public zztj(String paramString)
  {
    super(paramString);
  }
  
  static zztj a()
  {
    zztj localzztj = new com/google/android/gms/internal/zztj;
    localzztj.<init>("While parsing a protocol message, the input ended unexpectedly in the middle of a field.  This could mean either than the input has been truncated or that an embedded message misreported its own length.");
    return localzztj;
  }
  
  static zztj b()
  {
    zztj localzztj = new com/google/android/gms/internal/zztj;
    localzztj.<init>("CodedInputStream encountered an embedded string or message which claimed to have negative size.");
    return localzztj;
  }
  
  static zztj c()
  {
    zztj localzztj = new com/google/android/gms/internal/zztj;
    localzztj.<init>("CodedInputStream encountered a malformed varint.");
    return localzztj;
  }
  
  static zztj d()
  {
    zztj localzztj = new com/google/android/gms/internal/zztj;
    localzztj.<init>("Protocol message contained an invalid tag (zero).");
    return localzztj;
  }
  
  static zztj e()
  {
    zztj localzztj = new com/google/android/gms/internal/zztj;
    localzztj.<init>("Protocol message end-group tag did not match expected tag.");
    return localzztj;
  }
  
  static zztj f()
  {
    zztj localzztj = new com/google/android/gms/internal/zztj;
    localzztj.<init>("Protocol message tag had invalid wire type.");
    return localzztj;
  }
  
  static zztj g()
  {
    zztj localzztj = new com/google/android/gms/internal/zztj;
    localzztj.<init>("Protocol message had too many levels of nesting.  May be malicious.  Use CodedInputStream.setRecursionLimit() to increase the depth limit.");
    return localzztj;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/zztj.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */