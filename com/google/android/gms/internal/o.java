package com.google.android.gms.internal;

public final class o
{
  public static final Object a;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
  }
  
  public static int a(Object[] paramArrayOfObject)
  {
    int i = 0;
    if (paramArrayOfObject == null) {}
    for (int j = 0;; j = paramArrayOfObject.length)
    {
      int k = 0;
      while (k < j)
      {
        Object localObject = paramArrayOfObject[k];
        if (localObject != null)
        {
          i *= 31;
          int m = localObject.hashCode();
          i += m;
        }
        k += 1;
      }
    }
    return i;
  }
  
  public static boolean a(Object[] paramArrayOfObject1, Object[] paramArrayOfObject2)
  {
    int i = 1;
    boolean bool1 = false;
    int j;
    if (paramArrayOfObject1 == null)
    {
      j = 0;
      if (paramArrayOfObject2 != null) {
        break label69;
      }
    }
    int m;
    Object localObject1;
    int i1;
    Object localObject2;
    int i2;
    label69:
    for (int k = 0;; k = paramArrayOfObject2.length)
    {
      m = 0;
      localObject1 = null;
      for (i1 = 0; i1 < j; i1 = i2)
      {
        localObject2 = paramArrayOfObject1[i1];
        if (localObject2 != null) {
          break;
        }
        i2 = i1 + 1;
      }
      k = paramArrayOfObject1.length;
      j = k;
      break;
    }
    for (;;)
    {
      if (i3 < k)
      {
        localObject1 = paramArrayOfObject2[i3];
        if (localObject1 == null)
        {
          m = i3 + 1;
          i3 = m;
          continue;
        }
      }
      if (i1 >= j)
      {
        i2 = i;
        if (i3 < k) {
          break label150;
        }
        m = i;
        label127:
        if ((i2 == 0) || (m == 0)) {
          break label159;
        }
        bool1 = i;
      }
      label150:
      label159:
      boolean bool2;
      do
      {
        do
        {
          return bool1;
          i2 = 0;
          localObject2 = null;
          break;
          m = 0;
          localObject1 = null;
          break label127;
        } while (i2 != m);
        localObject1 = paramArrayOfObject1[i1];
        localObject2 = paramArrayOfObject2[i3];
        bool2 = localObject1.equals(localObject2);
      } while (!bool2);
      i2 = i1 + 1;
      int n = i3 + 1;
      i1 = i2;
      break;
      int i3 = n;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/o.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */