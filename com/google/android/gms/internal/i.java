package com.google.android.gms.internal;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.os.Binder;
import java.util.Iterator;
import java.util.List;

public class i
{
  public static String a(int paramInt1, int paramInt2)
  {
    StackTraceElement[] arrayOfStackTraceElement = Thread.currentThread().getStackTrace();
    StringBuffer localStringBuffer = new java/lang/StringBuffer;
    localStringBuffer.<init>();
    int i = paramInt2 + paramInt1;
    while (paramInt1 < i)
    {
      Object localObject = a(arrayOfStackTraceElement, paramInt1);
      localObject = localStringBuffer.append((String)localObject);
      String str = " ";
      ((StringBuffer)localObject).append(str);
      paramInt1 += 1;
    }
    return localStringBuffer.toString();
  }
  
  public static String a(Context paramContext)
  {
    int i = Binder.getCallingPid();
    return a(paramContext, i);
  }
  
  public static String a(Context paramContext, int paramInt)
  {
    Object localObject = ((ActivityManager)paramContext.getSystemService("activity")).getRunningAppProcesses();
    boolean bool;
    if (localObject != null)
    {
      Iterator localIterator = ((List)localObject).iterator();
      int i;
      do
      {
        bool = localIterator.hasNext();
        if (!bool) {
          break;
        }
        localObject = (ActivityManager.RunningAppProcessInfo)localIterator.next();
        i = pid;
      } while (i != paramInt);
    }
    for (localObject = processName;; localObject = null)
    {
      return (String)localObject;
      bool = false;
    }
  }
  
  private static String a(StackTraceElement[] paramArrayOfStackTraceElement, int paramInt)
  {
    int i = paramInt + 4;
    int j = paramArrayOfStackTraceElement.length;
    if (i >= j) {}
    StringBuilder localStringBuilder;
    for (Object localObject = "<bottom of call stack>";; localObject = i)
    {
      return (String)localObject;
      i = paramInt + 4;
      localObject = paramArrayOfStackTraceElement[i];
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      String str = ((StackTraceElement)localObject).getClassName();
      localStringBuilder = localStringBuilder.append(str).append(".");
      str = ((StackTraceElement)localObject).getMethodName();
      localStringBuilder = localStringBuilder.append(str);
      str = ":";
      localStringBuilder = localStringBuilder.append(str);
      i = ((StackTraceElement)localObject).getLineNumber();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */