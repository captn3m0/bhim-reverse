package com.google.android.gms.internal;

import com.google.android.gms.common.api.c.d;

public final class m
{
  public static final m a;
  private final boolean b;
  private final boolean c;
  private final String d;
  private final c.d e;
  private final boolean f;
  private final boolean g;
  private final boolean h;
  
  static
  {
    m.a locala = new com/google/android/gms/internal/m$a;
    locala.<init>();
    a = locala.a();
  }
  
  private m(boolean paramBoolean1, boolean paramBoolean2, String paramString, c.d paramd, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5)
  {
    b = paramBoolean1;
    c = paramBoolean2;
    d = paramString;
    e = paramd;
    f = paramBoolean3;
    g = paramBoolean4;
    h = paramBoolean5;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/m.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */