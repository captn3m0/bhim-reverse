package com.google.android.gms.internal;

import android.os.SystemClock;

public final class f
  implements e
{
  private static f a;
  
  public static e c()
  {
    synchronized (f.class)
    {
      f localf = a;
      if (localf == null)
      {
        localf = new com/google/android/gms/internal/f;
        localf.<init>();
        a = localf;
      }
      localf = a;
      return localf;
    }
  }
  
  public long a()
  {
    return System.currentTimeMillis();
  }
  
  public long b()
  {
    return SystemClock.elapsedRealtime();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */