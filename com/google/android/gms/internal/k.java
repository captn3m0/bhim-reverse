package com.google.android.gms.internal;

import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.a.a;
import com.google.android.gms.common.api.a.b;

public final class k
{
  public static final a.b a;
  public static final a.b b;
  public static final a.a c;
  static final a.a d;
  public static final Scope e;
  public static final Scope f;
  public static final com.google.android.gms.common.api.a g;
  public static final com.google.android.gms.common.api.a h;
  public static final l i;
  
  static
  {
    Object localObject = new com/google/android/gms/common/api/a$b;
    ((a.b)localObject).<init>();
    a = (a.b)localObject;
    localObject = new com/google/android/gms/common/api/a$b;
    ((a.b)localObject).<init>();
    b = (a.b)localObject;
    localObject = new com/google/android/gms/internal/k$1;
    ((k.1)localObject).<init>();
    c = (a.a)localObject;
    localObject = new com/google/android/gms/internal/k$2;
    ((k.2)localObject).<init>();
    d = (a.a)localObject;
    localObject = new com/google/android/gms/common/api/Scope;
    ((Scope)localObject).<init>("profile");
    e = (Scope)localObject;
    localObject = new com/google/android/gms/common/api/Scope;
    ((Scope)localObject).<init>("email");
    f = (Scope)localObject;
    localObject = new com/google/android/gms/common/api/a;
    a.a locala = c;
    a.b localb = a;
    ((com.google.android.gms.common.api.a)localObject).<init>("SignIn.API", locala, localb);
    g = (com.google.android.gms.common.api.a)localObject;
    localObject = new com/google/android/gms/common/api/a;
    locala = d;
    localb = b;
    ((com.google.android.gms.common.api.a)localObject).<init>("SignIn.INTERNAL_API", locala, localb);
    h = (com.google.android.gms.common.api.a)localObject;
    localObject = new com/google/android/gms/a/a/a;
    ((com.google.android.gms.a.a.a)localObject).<init>();
    i = (l)localObject;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/k.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */