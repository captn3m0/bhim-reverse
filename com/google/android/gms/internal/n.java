package com.google.android.gms.internal;

public final class n
{
  private final byte[] a;
  private int b;
  private int c;
  private int d;
  private int e;
  private int f;
  private int g = -1 >>> 1;
  private int h;
  private int i = 64;
  private int j = 67108864;
  
  private n(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    a = paramArrayOfByte;
    b = paramInt1;
    int k = paramInt1 + paramInt2;
    c = k;
    e = paramInt1;
  }
  
  public static n a(byte[] paramArrayOfByte)
  {
    int k = paramArrayOfByte.length;
    return a(paramArrayOfByte, 0, k);
  }
  
  public static n a(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    n localn = new com/google/android/gms/internal/n;
    localn.<init>(paramArrayOfByte, paramInt1, paramInt2);
    return localn;
  }
  
  private void o()
  {
    int k = c;
    int m = d;
    k += m;
    c = k;
    k = c;
    m = g;
    if (k > m)
    {
      m = g;
      k -= m;
      d = k;
      k = c;
      m = d;
      k -= m;
      c = k;
    }
    for (;;)
    {
      return;
      k = 0;
      d = 0;
    }
  }
  
  public int a()
  {
    int k = 0;
    boolean bool = l();
    if (bool) {
      f = 0;
    }
    for (;;)
    {
      return k;
      k = h();
      f = k;
      k = f;
      if (k == 0) {
        throw zztj.d();
      }
      k = f;
    }
  }
  
  public void a(int paramInt)
  {
    int k = f;
    if (k != paramInt) {
      throw zztj.e();
    }
  }
  
  public void a(p paramp)
  {
    int k = h();
    int m = h;
    int n = i;
    if (m >= n) {
      throw zztj.g();
    }
    k = c(k);
    m = h + 1;
    h = m;
    paramp.b(this);
    a(0);
    m = h + -1;
    h = m;
    d(k);
  }
  
  public void b()
  {
    boolean bool;
    do
    {
      int k = a();
      if (k == 0) {
        break;
      }
      bool = b(k);
    } while (bool);
  }
  
  public boolean b(int paramInt)
  {
    boolean bool = true;
    int k = r.a(paramInt);
    switch (k)
    {
    default: 
      throw zztj.f();
    case 0: 
      e();
    }
    for (;;)
    {
      return bool;
      k();
      continue;
      k = h();
      g(k);
      continue;
      b();
      k = r.b(paramInt);
      int m = 4;
      k = r.a(k, m);
      a(k);
      continue;
      bool = false;
      continue;
      j();
    }
  }
  
  public float c()
  {
    return Float.intBitsToFloat(j());
  }
  
  public int c(int paramInt)
  {
    if (paramInt < 0) {
      throw zztj.b();
    }
    int k = e + paramInt;
    int m = g;
    if (k > m) {
      throw zztj.a();
    }
    g = k;
    o();
    return m;
  }
  
  public long d()
  {
    return i();
  }
  
  public void d(int paramInt)
  {
    g = paramInt;
    o();
  }
  
  public int e()
  {
    return h();
  }
  
  public void e(int paramInt)
  {
    int k = e;
    int m = b;
    k -= m;
    IllegalArgumentException localIllegalArgumentException;
    Object localObject;
    if (paramInt > k)
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = ((StringBuilder)localObject).append("Position ").append(paramInt).append(" is beyond current ");
      int n = e;
      int i1 = b;
      n -= i1;
      localObject = n;
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    }
    if (paramInt < 0)
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = "Bad position " + paramInt;
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    }
    k = b + paramInt;
    e = k;
  }
  
  public boolean f()
  {
    int k = h();
    if (k != 0) {}
    int m;
    for (k = 1;; m = 0) {
      return k;
    }
  }
  
  public byte[] f(int paramInt)
  {
    if (paramInt < 0) {
      throw zztj.b();
    }
    int k = e + paramInt;
    int m = g;
    if (k > m)
    {
      k = g;
      m = e;
      k -= m;
      g(k);
      throw zztj.a();
    }
    k = c;
    m = e;
    k -= m;
    if (paramInt <= k)
    {
      byte[] arrayOfByte1 = new byte[paramInt];
      byte[] arrayOfByte2 = a;
      int n = e;
      System.arraycopy(arrayOfByte2, n, arrayOfByte1, 0, paramInt);
      m = e + paramInt;
      e = m;
      return arrayOfByte1;
    }
    throw zztj.a();
  }
  
  public String g()
  {
    int k = h();
    int m = c;
    int n = e;
    m -= n;
    String str1;
    Object localObject;
    if ((k <= m) && (k > 0))
    {
      str1 = new java/lang/String;
      localObject = a;
      int i1 = e;
      String str2 = "UTF-8";
      str1.<init>((byte[])localObject, i1, k, str2);
      n = e;
      k += n;
      e = k;
    }
    for (;;)
    {
      return str1;
      str1 = new java/lang/String;
      byte[] arrayOfByte = f(k);
      localObject = "UTF-8";
      str1.<init>(arrayOfByte, (String)localObject);
    }
  }
  
  public void g(int paramInt)
  {
    if (paramInt < 0) {
      throw zztj.b();
    }
    int k = e + paramInt;
    int m = g;
    if (k > m)
    {
      k = g;
      m = e;
      k -= m;
      g(k);
      throw zztj.a();
    }
    k = c;
    m = e;
    k -= m;
    if (paramInt <= k)
    {
      k = e + paramInt;
      e = k;
      return;
    }
    throw zztj.a();
  }
  
  public int h()
  {
    int k = n();
    if (k >= 0) {}
    int n;
    do
    {
      for (;;)
      {
        return k;
        k &= 0x7F;
        m = n();
        if (m >= 0)
        {
          m <<= 7;
          k |= m;
        }
        else
        {
          m = (m & 0x7F) << 7;
          k |= m;
          m = n();
          if (m >= 0)
          {
            m <<= 14;
            k |= m;
          }
          else
          {
            m = (m & 0x7F) << 14;
            k |= m;
            m = n();
            if (m < 0) {
              break;
            }
            m <<= 21;
            k |= m;
          }
        }
      }
      m = (m & 0x7F) << 21;
      k |= m;
      m = n();
      n = m << 28;
      k |= n;
    } while (m >= 0);
    int m = 0;
    for (;;)
    {
      n = 5;
      if (m >= n) {
        break label158;
      }
      n = n();
      if (n >= 0) {
        break;
      }
      m += 1;
    }
    label158:
    throw zztj.c();
  }
  
  public long i()
  {
    int k = 0;
    long l1 = 0L;
    for (;;)
    {
      int m = 64;
      if (k >= m) {
        break;
      }
      m = n();
      int n = m & 0x7F;
      long l2 = n << k;
      l1 |= l2;
      m &= 0x80;
      if (m == 0) {
        return l1;
      }
      k += 7;
    }
    throw zztj.c();
  }
  
  public int j()
  {
    int k = n();
    int m = n();
    int n = n();
    int i1 = n();
    k &= 0xFF;
    m = (m & 0xFF) << 8;
    k |= m;
    m = (n & 0xFF) << 16;
    k |= m;
    m = (i1 & 0xFF) << 24;
    return k | m;
  }
  
  public long k()
  {
    long l1 = 255L;
    int k = n();
    int m = n();
    int n = n();
    int i1 = n();
    int i2 = n();
    int i3 = n();
    int i4 = n();
    int i5 = n();
    long l2 = k & l1;
    long l3 = (m & l1) << 8 | l2;
    l2 = (n & l1) << 16;
    l3 |= l2;
    long l4 = (i1 & l1) << 24;
    l3 |= l4;
    l4 = (i2 & l1) << 32;
    l3 |= l4;
    l4 = (i3 & l1) << 40;
    l3 |= l4;
    l4 = (i4 & l1) << 48;
    l3 |= l4;
    l4 = (i5 & l1) << 56;
    return l3 | l4;
  }
  
  public boolean l()
  {
    int k = e;
    int n = c;
    if (k == n) {}
    int m;
    for (k = 1;; m = 0) {
      return k;
    }
  }
  
  public int m()
  {
    int k = e;
    int m = b;
    return k - m;
  }
  
  public byte n()
  {
    int k = e;
    int m = c;
    if (k == m) {
      throw zztj.a();
    }
    byte[] arrayOfByte = a;
    m = e;
    int n = m + 1;
    e = n;
    return arrayOfByte[m];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/n.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */