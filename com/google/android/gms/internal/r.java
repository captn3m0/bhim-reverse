package com.google.android.gms.internal;

public final class r
{
  public static final int[] a = new int[0];
  public static final long[] b = new long[0];
  public static final float[] c = new float[0];
  public static final double[] d = new double[0];
  public static final boolean[] e = new boolean[0];
  public static final String[] f = new String[0];
  public static final byte[][] g = new byte[0][];
  public static final byte[] h = new byte[0];
  
  static int a(int paramInt)
  {
    return paramInt & 0x7;
  }
  
  static int a(int paramInt1, int paramInt2)
  {
    return paramInt1 << 3 | paramInt2;
  }
  
  public static boolean a(n paramn, int paramInt)
  {
    return paramn.b(paramInt);
  }
  
  public static int b(int paramInt)
  {
    return paramInt >>> 3;
  }
  
  public static final int b(n paramn, int paramInt)
  {
    int i = 1;
    int j = paramn.m();
    paramn.b(paramInt);
    for (;;)
    {
      int k = paramn.a();
      if (k != paramInt) {
        break;
      }
      paramn.b(paramInt);
      i += 1;
    }
    paramn.e(j);
    return i;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/r.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */