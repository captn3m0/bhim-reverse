package com.google.android.gms.internal;

public final class j$b
  extends p
{
  private static volatile b[] e;
  public String a;
  public String b;
  public Long c;
  public Float d;
  
  public j$b()
  {
    b();
  }
  
  public static b[] a()
  {
    b[] arrayOfb = e;
    if (arrayOfb == null) {}
    synchronized (o.a)
    {
      arrayOfb = e;
      if (arrayOfb == null)
      {
        arrayOfb = null;
        arrayOfb = new b[0];
        e = arrayOfb;
      }
      return e;
    }
  }
  
  public b a(n paramn)
  {
    for (;;)
    {
      int i = paramn.a();
      Object localObject;
      switch (i)
      {
      default: 
        boolean bool = r.a(paramn, i);
        if (bool) {}
        break;
      case 0: 
        return this;
      case 10: 
        localObject = paramn.g();
        a = ((String)localObject);
        break;
      case 18: 
        localObject = paramn.g();
        b = ((String)localObject);
        break;
      case 24: 
        long l = paramn.d();
        localObject = Long.valueOf(l);
        c = ((Long)localObject);
        break;
      case 37: 
        float f = paramn.c();
        localObject = Float.valueOf(f);
        d = ((Float)localObject);
      }
    }
  }
  
  public void a(zztd paramzztd)
  {
    Object localObject1 = a;
    int i;
    Object localObject2;
    if (localObject1 != null)
    {
      i = 1;
      localObject2 = a;
      paramzztd.a(i, (String)localObject2);
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      i = 2;
      localObject2 = b;
      paramzztd.a(i, (String)localObject2);
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      i = 3;
      localObject2 = c;
      long l = ((Long)localObject2).longValue();
      paramzztd.a(i, l);
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      i = 4;
      localObject2 = d;
      float f = ((Float)localObject2).floatValue();
      paramzztd.a(i, f);
    }
    super.a(paramzztd);
  }
  
  public b b()
  {
    a = null;
    b = null;
    c = null;
    d = null;
    A = -1;
    return this;
  }
  
  protected int c()
  {
    int i = super.c();
    Object localObject1 = a;
    Object localObject2;
    int j;
    if (localObject1 != null)
    {
      localObject2 = a;
      j = zztd.b(1, (String)localObject2);
      i += j;
    }
    localObject1 = b;
    if (localObject1 != null)
    {
      localObject2 = b;
      j = zztd.b(2, (String)localObject2);
      i += j;
    }
    localObject1 = c;
    if (localObject1 != null)
    {
      localObject2 = c;
      long l = ((Long)localObject2).longValue();
      j = zztd.b(3, l);
      i += j;
    }
    localObject1 = d;
    if (localObject1 != null)
    {
      localObject2 = d;
      float f = ((Float)localObject2).floatValue();
      j = zztd.b(4, f);
      i += j;
    }
    return i;
  }
  
  public boolean equals(Object paramObject)
  {
    boolean bool1 = true;
    if (paramObject == this) {}
    for (;;)
    {
      return bool1;
      boolean bool2 = paramObject instanceof b;
      if (!bool2)
      {
        bool1 = false;
      }
      else
      {
        paramObject = (b)paramObject;
        Object localObject1 = a;
        Object localObject2;
        if (localObject1 == null)
        {
          localObject1 = a;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = a;
          localObject2 = a;
          bool2 = ((String)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool1 = false;
            continue;
          }
        }
        localObject1 = b;
        if (localObject1 == null)
        {
          localObject1 = b;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = b;
          localObject2 = b;
          bool2 = ((String)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool1 = false;
            continue;
          }
        }
        localObject1 = c;
        if (localObject1 == null)
        {
          localObject1 = c;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = c;
          localObject2 = c;
          bool2 = ((Long)localObject1).equals(localObject2);
          if (!bool2)
          {
            bool1 = false;
            continue;
          }
        }
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject1 = d;
          if (localObject1 != null) {
            bool1 = false;
          }
        }
        else
        {
          localObject1 = d;
          localObject2 = d;
          bool2 = ((Float)localObject1).equals(localObject2);
          if (!bool2) {
            bool1 = false;
          }
        }
      }
    }
  }
  
  public int hashCode()
  {
    int i = 0;
    Float localFloat1 = null;
    int j = getClass().getName().hashCode() + 527;
    int k = j * 31;
    Object localObject = a;
    if (localObject == null)
    {
      j = 0;
      localObject = null;
      j += k;
      k = j * 31;
      localObject = b;
      if (localObject != null) {
        break label133;
      }
      j = 0;
      localObject = null;
      label68:
      j += k;
      k = j * 31;
      localObject = c;
      if (localObject != null) {
        break label148;
      }
      j = 0;
      localObject = null;
      label95:
      j = (j + k) * 31;
      Float localFloat2 = d;
      if (localFloat2 != null) {
        break label163;
      }
    }
    for (;;)
    {
      return j + i;
      localObject = a;
      j = ((String)localObject).hashCode();
      break;
      label133:
      localObject = b;
      j = ((String)localObject).hashCode();
      break label68;
      label148:
      localObject = c;
      j = ((Long)localObject).hashCode();
      break label95;
      label163:
      localFloat1 = d;
      i = localFloat1.hashCode();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/j$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */