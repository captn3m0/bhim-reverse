package com.google.android.gms.internal;

import android.content.res.Configuration;
import android.content.res.Resources;

public final class g
{
  public static boolean a(Resources paramResources)
  {
    int i = 1;
    boolean bool1 = false;
    if (paramResources == null) {
      return bool1;
    }
    Configuration localConfiguration = paramResources.getConfiguration();
    int j = screenLayout & 0xF;
    int k = 3;
    if (j > k) {
      j = i;
    }
    for (;;)
    {
      boolean bool3 = h.a();
      if ((!bool3) || (j == 0))
      {
        bool2 = b(paramResources);
        if (!bool2) {
          break;
        }
      }
      bool1 = i;
      break;
      boolean bool2 = false;
      localConfiguration = null;
    }
  }
  
  private static boolean b(Resources paramResources)
  {
    boolean bool1 = false;
    Configuration localConfiguration = paramResources.getConfiguration();
    boolean bool2 = h.b();
    if (bool2)
    {
      int i = screenLayout & 0xF;
      int j = 3;
      if (i <= j)
      {
        int k = smallestScreenWidthDp;
        i = 600;
        if (k >= i) {
          bool1 = true;
        }
      }
    }
    return bool1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/internal/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */