package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.measurement.internal.f;
import com.google.android.gms.measurement.internal.h;
import com.google.android.gms.measurement.internal.t;
import com.google.android.gms.measurement.internal.t.a;
import com.google.android.gms.measurement.internal.y;

public final class AppMeasurementReceiver
  extends BroadcastReceiver
{
  static final Object a;
  static PowerManager.WakeLock b;
  static Boolean c;
  
  static
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    a = localObject;
  }
  
  public static boolean a(Context paramContext)
  {
    p.a(paramContext);
    Object localObject = c;
    boolean bool;
    if (localObject != null)
    {
      localObject = c;
      bool = ((Boolean)localObject).booleanValue();
    }
    for (;;)
    {
      return bool;
      localObject = AppMeasurementReceiver.class;
      bool = f.a(paramContext, (Class)localObject, false);
      Boolean localBoolean = Boolean.valueOf(bool);
      c = localBoolean;
    }
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    Object localObject1 = y.a(paramContext);
    localObject3 = ((y)localObject1).f();
    Object localObject4 = paramIntent.getAction();
    localObject1 = ((y)localObject1).d();
    boolean bool = ((h)localObject1).C();
    if (bool)
    {
      localObject1 = ((t)localObject3).t();
      ??? = "Device AppMeasurementReceiver got";
      ((t.a)localObject1).a((String)???, localObject4);
    }
    for (;;)
    {
      localObject1 = "com.google.android.gms.measurement.UPLOAD";
      bool = ((String)localObject1).equals(localObject4);
      if (bool)
      {
        bool = AppMeasurementService.a(paramContext);
        localObject4 = new android/content/Intent;
        ((Intent)localObject4).<init>(paramContext, AppMeasurementService.class);
        ((Intent)localObject4).setAction("com.google.android.gms.measurement.UPLOAD");
      }
      synchronized (a)
      {
        paramContext.startService((Intent)localObject4);
        if (!bool)
        {
          return;
          localObject1 = ((t)localObject3).t();
          ??? = "Local AppMeasurementReceiver got";
          ((t.a)localObject1).a((String)???, localObject4);
          continue;
        }
        localObject1 = "power";
        try
        {
          localObject1 = paramContext.getSystemService((String)localObject1);
          localObject1 = (PowerManager)localObject1;
          localObject4 = b;
          if (localObject4 == null)
          {
            int i = 1;
            String str = "AppMeasurement WakeLock";
            localObject1 = ((PowerManager)localObject1).newWakeLock(i, str);
            b = (PowerManager.WakeLock)localObject1;
            localObject1 = b;
            i = 0;
            localObject4 = null;
            ((PowerManager.WakeLock)localObject1).setReferenceCounted(false);
          }
          localObject1 = b;
          long l = 1000L;
          ((PowerManager.WakeLock)localObject1).acquire(l);
        }
        catch (SecurityException localSecurityException)
        {
          for (;;)
          {
            t.a locala = ((t)localObject3).o();
            localObject3 = "AppMeasurementService at risk of not starting. For more reliable app measurements, add the WAKE_LOCK permission to your manifest.";
            locala.a((String)localObject3);
          }
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/AppMeasurementReceiver.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */