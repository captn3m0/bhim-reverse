package com.google.android.gms.measurement;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import com.google.android.gms.a.b;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.p;

public final class b
{
  private static volatile b d;
  private final String a;
  private final Status b;
  private final boolean c;
  
  b(Context paramContext)
  {
    Object localObject1 = paramContext.getResources();
    int m = a.b.common_google_play_services_unknown_issue;
    Object localObject2 = ((Resources)localObject1).getResourcePackageName(m);
    String str1 = "google_app_measurement_enable";
    String str2 = "integer";
    int n = ((Resources)localObject1).getIdentifier(str1, str2, (String)localObject2);
    Object localObject3;
    int k;
    if (n != 0)
    {
      n = ((Resources)localObject1).getInteger(n);
      if (n == 0) {}
    }
    else
    {
      c = j;
      localObject3 = "google_app_id";
      str1 = "string";
      j = ((Resources)localObject1).getIdentifier((String)localObject3, str1, (String)localObject2);
      if (j != 0) {
        break label158;
      }
      k = c;
      if (k == 0) {
        break label144;
      }
      localObject3 = new com/google/android/gms/common/api/Status;
      localObject1 = "Missing an expected resource: 'R.string.google_app_id' for initializing Google services.  Possible causes are missing google-services.json or com.google.gms.google-services gradle plugin.";
      ((Status)localObject3).<init>(i, (String)localObject1);
      b = ((Status)localObject3);
      label130:
      a = null;
    }
    for (;;)
    {
      return;
      k = 0;
      localObject3 = null;
      break;
      label144:
      localObject3 = Status.a;
      b = ((Status)localObject3);
      break label130;
      label158:
      localObject3 = ((Resources)localObject1).getString(k);
      boolean bool = TextUtils.isEmpty((CharSequence)localObject3);
      if (bool)
      {
        bool = c;
        if (bool)
        {
          localObject1 = new com/google/android/gms/common/api/Status;
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          str1 = "The resource 'R.string.google_app_id' is invalid, expected an app  identifier and found: '";
          localObject3 = ((StringBuilder)localObject2).append(str1).append((String)localObject3);
          localObject2 = "'.";
          localObject3 = (String)localObject2;
          ((Status)localObject1).<init>(i, (String)localObject3);
        }
        for (b = ((Status)localObject1);; b = ((Status)localObject3))
        {
          a = null;
          break;
          localObject3 = Status.a;
        }
      }
      a = ((String)localObject3);
      localObject3 = Status.a;
      b = ((Status)localObject3);
    }
  }
  
  public static Status a(Context paramContext)
  {
    p.a(paramContext, "Context must not be null.");
    b localb = d;
    if (localb == null) {}
    synchronized (b.class)
    {
      localb = d;
      if (localb == null)
      {
        localb = new com/google/android/gms/measurement/b;
        localb.<init>(paramContext);
        d = localb;
      }
      return db;
    }
  }
  
  public static String a()
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      synchronized (b.class)
      {
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject1 = new java/lang/IllegalStateException;
          String str = "Initialize must be called before getGoogleAppId.";
          ((IllegalStateException)localObject1).<init>(str);
          throw ((Throwable)localObject1);
        }
      }
    }
    return d.b();
  }
  
  public static boolean c()
  {
    Object localObject1 = d;
    if (localObject1 == null) {
      synchronized (b.class)
      {
        localObject1 = d;
        if (localObject1 == null)
        {
          localObject1 = new java/lang/IllegalStateException;
          String str = "Initialize must be called before isMeasurementEnabled.";
          ((IllegalStateException)localObject1).<init>(str);
          throw ((Throwable)localObject1);
        }
      }
    }
    return d.d();
  }
  
  String b()
  {
    Object localObject1 = b;
    boolean bool = ((Status)localObject1).d();
    if (!bool)
    {
      localObject1 = new java/lang/IllegalStateException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("Initialize must be successful before calling getGoogleAppId.  The result of the previous call to initialize was: '");
      Status localStatus = b;
      localObject2 = localStatus + "'.";
      ((IllegalStateException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    return a;
  }
  
  boolean d()
  {
    Object localObject1 = b;
    boolean bool = ((Status)localObject1).d();
    if (!bool)
    {
      localObject1 = new java/lang/IllegalStateException;
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("Initialize must be successful before calling isMeasurementEnabledInternal.  The result of the previous call to initialize was: '");
      Status localStatus = b;
      localObject2 = localStatus + "'.";
      ((IllegalStateException)localObject1).<init>((String)localObject2);
      throw ((Throwable)localObject1);
    }
    return c;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */