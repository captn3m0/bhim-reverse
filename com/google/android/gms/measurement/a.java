package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.measurement.internal.ad;
import com.google.android.gms.measurement.internal.y;

public class a
{
  private final y a;
  
  public a(y paramy)
  {
    p.a(paramy);
    a = paramy;
  }
  
  public static a a(Context paramContext)
  {
    return y.a(paramContext).j();
  }
  
  public void a(String paramString1, String paramString2, Bundle paramBundle)
  {
    if (paramBundle == null)
    {
      paramBundle = new android/os/Bundle;
      paramBundle.<init>();
    }
    a.i().a(paramString1, paramString2, paramBundle);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */