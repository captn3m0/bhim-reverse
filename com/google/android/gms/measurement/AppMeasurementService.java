package com.google.android.gms.measurement;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.measurement.internal.f;
import com.google.android.gms.measurement.internal.h;
import com.google.android.gms.measurement.internal.t;
import com.google.android.gms.measurement.internal.t.a;
import com.google.android.gms.measurement.internal.x;
import com.google.android.gms.measurement.internal.y;
import com.google.android.gms.measurement.internal.z;

public final class AppMeasurementService
  extends Service
{
  private static Boolean b;
  private final Handler a;
  
  public AppMeasurementService()
  {
    Handler localHandler = new android/os/Handler;
    localHandler.<init>();
    a = localHandler;
  }
  
  /* Error */
  private void a()
  {
    // Byte code:
    //   0: getstatic 22	com/google/android/gms/measurement/AppMeasurementReceiver:a	Ljava/lang/Object;
    //   3: astore_1
    //   4: aload_1
    //   5: monitorenter
    //   6: getstatic 25	com/google/android/gms/measurement/AppMeasurementReceiver:b	Landroid/os/PowerManager$WakeLock;
    //   9: astore_2
    //   10: aload_2
    //   11: ifnull +16 -> 27
    //   14: aload_2
    //   15: invokevirtual 31	android/os/PowerManager$WakeLock:isHeld	()Z
    //   18: istore_3
    //   19: iload_3
    //   20: ifeq +7 -> 27
    //   23: aload_2
    //   24: invokevirtual 34	android/os/PowerManager$WakeLock:release	()V
    //   27: aload_1
    //   28: monitorexit
    //   29: return
    //   30: astore_2
    //   31: aload_1
    //   32: monitorexit
    //   33: aload_2
    //   34: athrow
    //   35: astore_2
    //   36: goto -7 -> 29
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	39	0	this	AppMeasurementService
    //   9	15	2	localWakeLock	android.os.PowerManager.WakeLock
    //   30	4	2	localObject2	Object
    //   35	1	2	localSecurityException	SecurityException
    //   18	2	3	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   6	9	30	finally
    //   14	18	30	finally
    //   23	27	30	finally
    //   27	29	30	finally
    //   31	33	30	finally
    //   0	3	35	java/lang/SecurityException
    //   4	6	35	java/lang/SecurityException
    //   33	35	35	java/lang/SecurityException
  }
  
  public static boolean a(Context paramContext)
  {
    p.a(paramContext);
    Object localObject = b;
    boolean bool;
    if (localObject != null)
    {
      localObject = b;
      bool = ((Boolean)localObject).booleanValue();
    }
    for (;;)
    {
      return bool;
      localObject = AppMeasurementService.class;
      bool = f.a(paramContext, (Class)localObject);
      Boolean localBoolean = Boolean.valueOf(bool);
      b = localBoolean;
    }
  }
  
  private t b()
  {
    return y.a(this).f();
  }
  
  public IBinder onBind(Intent paramIntent)
  {
    Object localObject1 = null;
    Object localObject2;
    Object localObject3;
    if (paramIntent == null)
    {
      localObject2 = b().b();
      localObject3 = "onBind called with null intent";
      ((t.a)localObject2).a((String)localObject3);
    }
    for (;;)
    {
      return (IBinder)localObject1;
      localObject2 = paramIntent.getAction();
      localObject3 = "com.google.android.gms.measurement.START";
      boolean bool = ((String)localObject3).equals(localObject2);
      if (bool)
      {
        localObject1 = new com/google/android/gms/measurement/internal/z;
        localObject2 = y.a(this);
        ((z)localObject1).<init>((y)localObject2);
      }
      else
      {
        localObject3 = b().o();
        String str = "onBind received unknown action";
        ((t.a)localObject3).a(str, localObject2);
      }
    }
  }
  
  public void onCreate()
  {
    super.onCreate();
    Object localObject1 = y.a(this);
    Object localObject2 = ((y)localObject1).f();
    localObject1 = ((y)localObject1).d();
    boolean bool = ((h)localObject1).C();
    if (bool)
    {
      localObject1 = ((t)localObject2).t();
      localObject2 = "Device AppMeasurementService is starting up";
      ((t.a)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      return;
      localObject1 = ((t)localObject2).t();
      localObject2 = "Local AppMeasurementService is starting up";
      ((t.a)localObject1).a((String)localObject2);
    }
  }
  
  public void onDestroy()
  {
    Object localObject1 = y.a(this);
    Object localObject2 = ((y)localObject1).f();
    localObject1 = ((y)localObject1).d();
    boolean bool = ((h)localObject1).C();
    if (bool)
    {
      localObject1 = ((t)localObject2).t();
      localObject2 = "Device AppMeasurementService is shutting down";
      ((t.a)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      super.onDestroy();
      return;
      localObject1 = ((t)localObject2).t();
      localObject2 = "Local AppMeasurementService is shutting down";
      ((t.a)localObject1).a((String)localObject2);
    }
  }
  
  public void onRebind(Intent paramIntent)
  {
    Object localObject1;
    Object localObject2;
    if (paramIntent == null)
    {
      localObject1 = b().b();
      localObject2 = "onRebind called with null intent";
      ((t.a)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      return;
      localObject1 = paramIntent.getAction();
      localObject2 = b().t();
      String str = "onRebind called. action";
      ((t.a)localObject2).a(str, localObject1);
    }
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    int i = 2;
    a();
    y localy = y.a(this);
    t localt = localy.f();
    Object localObject1 = paramIntent.getAction();
    Object localObject2 = localy.d();
    boolean bool1 = ((h)localObject2).C();
    String str;
    Integer localInteger;
    if (bool1)
    {
      localObject2 = localt.t();
      str = "Device AppMeasurementService called. startId, action";
      localInteger = Integer.valueOf(paramInt2);
      ((t.a)localObject2).a(str, localInteger, localObject1);
    }
    for (;;)
    {
      localObject2 = "com.google.android.gms.measurement.UPLOAD";
      boolean bool2 = ((String)localObject2).equals(localObject1);
      if (bool2)
      {
        localObject1 = localy.g();
        localObject2 = new com/google/android/gms/measurement/AppMeasurementService$1;
        ((AppMeasurementService.1)localObject2).<init>(this, localy, paramInt2, localt);
        ((x)localObject1).a((Runnable)localObject2);
      }
      return i;
      localObject2 = localt.t();
      str = "Local AppMeasurementService called. startId, action";
      localInteger = Integer.valueOf(paramInt2);
      ((t.a)localObject2).a(str, localInteger, localObject1);
    }
  }
  
  public boolean onUnbind(Intent paramIntent)
  {
    boolean bool = true;
    Object localObject1;
    Object localObject2;
    if (paramIntent == null)
    {
      localObject1 = b().b();
      localObject2 = "onUnbind called with null intent";
      ((t.a)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      return bool;
      localObject1 = paramIntent.getAction();
      localObject2 = b().t();
      String str = "onUnbind called for intent. action";
      ((t.a)localObject2).a(str, localObject1);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/AppMeasurementService.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */