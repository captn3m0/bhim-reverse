package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.p;

final class x$b
  implements Thread.UncaughtExceptionHandler
{
  private final String b;
  
  public x$b(x paramx, String paramString)
  {
    p.a(paramString);
    b = paramString;
  }
  
  public void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    try
    {
      Object localObject1 = a;
      localObject1 = ((x)localObject1).l();
      localObject1 = ((t)localObject1).b();
      String str = b;
      ((t.a)localObject1).a(str, paramThrowable);
      return;
    }
    finally {}
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/x$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */