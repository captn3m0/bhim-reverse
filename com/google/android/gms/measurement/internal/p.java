package com.google.android.gms.measurement.internal;

public final class p
{
  public static p.a a;
  public static p.a b;
  public static p.a c;
  public static p.a d;
  public static p.a e;
  public static p.a f;
  public static p.a g;
  public static p.a h;
  public static p.a i;
  public static p.a j;
  public static p.a k;
  public static p.a l;
  public static p.a m = p.a.a("measurement.upload.initial_upload_delay_time", 15000L);
  public static p.a n = p.a.a("measurement.upload.retry_time", 1800000L);
  public static p.a o = p.a.a("measurement.upload.retry_count", 6);
  public static p.a p = p.a.a("measurement.upload.max_queue_time", 2419200000L);
  public static p.a q = p.a.a("measurement.service_client.idle_disconnect_millis", 5000L);
  
  static
  {
    long l1 = 86400000L;
    long l2 = 3600000L;
    boolean bool = true;
    a = p.a.a("measurement.service_enabled", bool);
    b = p.a.a("measurement.service_client_enabled", bool);
    c = p.a.a("measurement.log_tag", "GMPM", "GMPM-SVC");
    d = p.a.a("measurement.ad_id_cache_time", 10000L);
    e = p.a.a("measurement.monitoring.sample_period_millis", l1);
    f = p.a.a("measurement.upload.max_bundles", 100);
    g = p.a.a("measurement.upload.max_batch_size", 65536);
    h = p.a.a("measurement.upload.url", "https://app-measurement.com/a");
    i = p.a.a("measurement.upload.backoff_period", 43200000L);
    j = p.a.a("measurement.upload.window_interval", l2);
    k = p.a.a("measurement.upload.interval", l2);
    l = p.a.a("measurement.upload.stale_data_deletion_interval", l1);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/p.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */