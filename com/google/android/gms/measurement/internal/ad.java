package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.e;
import com.google.android.gms.measurement.a.a;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Set;

public class ad
  extends ab
{
  private ad.a a;
  private a.a b;
  private boolean c;
  
  protected ad(y paramy)
  {
    super(paramy);
  }
  
  private void a(String paramString1, String paramString2, long paramLong, Bundle paramBundle, boolean paramBoolean, String paramString3)
  {
    p.a(paramString1);
    p.a(paramString2);
    p.a(paramBundle);
    e();
    y();
    Object localObject1 = m();
    boolean bool = ((w)localObject1).r();
    Object localObject2;
    if (!bool)
    {
      localObject1 = l().r();
      localObject2 = "Event not sent since app measurement is disabled";
      ((t.a)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      return;
      bool = c;
      if (!bool)
      {
        bool = true;
        c = bool;
        p();
      }
      if (paramBoolean)
      {
        localObject1 = b;
        if (localObject1 != null)
        {
          localObject1 = l().s();
          localObject2 = "Passing event to registered event handler (FE)";
          ((t.a)localObject1).a((String)localObject2, paramString2, paramBundle);
          localObject1 = b;
          ((a.a)localObject1).a(paramString1, paramString2, paramBundle);
          continue;
        }
      }
      localObject1 = g;
      bool = ((y)localObject1).b();
      if (bool)
      {
        l().s().a("Logging event (FE)", paramString2, paramBundle);
        localObject1 = new com/google/android/gms/measurement/internal/EventParcel;
        EventParams localEventParams = new com/google/android/gms/measurement/internal/EventParams;
        localEventParams.<init>(paramBundle);
        localObject2 = paramString2;
        ((EventParcel)localObject1).<init>(paramString2, localEventParams, paramString1, paramLong);
        localObject2 = g();
        ((ae)localObject2).a((EventParcel)localObject1, paramString3);
      }
    }
  }
  
  private void a(String paramString1, String paramString2, Bundle paramBundle, boolean paramBoolean, String paramString3)
  {
    p.a(paramString1);
    long l = h().a();
    Object localObject1 = j();
    ((f)localObject1).a(paramString2);
    Bundle localBundle = new android/os/Bundle;
    localBundle.<init>();
    if (paramBundle != null)
    {
      localObject1 = paramBundle.keySet();
      localObject2 = ((Set)localObject1).iterator();
      for (;;)
      {
        boolean bool = ((Iterator)localObject2).hasNext();
        if (!bool) {
          break;
        }
        localObject1 = (String)((Iterator)localObject2).next();
        j().c((String)localObject1);
        localObject3 = j();
        localObject4 = paramBundle.get((String)localObject1);
        localObject3 = ((f)localObject3).a((String)localObject1, localObject4);
        if (localObject3 != null)
        {
          localObject4 = j();
          ((f)localObject4).a(localBundle, (String)localObject1, localObject3);
        }
      }
    }
    x localx = k();
    localObject1 = new com/google/android/gms/measurement/internal/ad$2;
    Object localObject2 = this;
    Object localObject3 = paramString1;
    Object localObject4 = paramString2;
    ((ad.2)localObject1).<init>(this, paramString1, paramString2, l, localBundle, paramBoolean, paramString3);
    localx.a((Runnable)localObject1);
  }
  
  private void a(String paramString1, String paramString2, Object paramObject, long paramLong)
  {
    p.a(paramString1);
    p.a(paramString2);
    e();
    c();
    y();
    Object localObject1 = m();
    boolean bool = ((w)localObject1).r();
    Object localObject2;
    if (!bool)
    {
      localObject1 = l().r();
      localObject2 = "User attribute not set since app measurement is disabled";
      ((t.a)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      return;
      localObject1 = g;
      bool = ((y)localObject1).b();
      if (bool)
      {
        l().s().a("Setting user attribute (FE)", paramString2, paramObject);
        localObject1 = new com/google/android/gms/measurement/internal/UserAttributeParcel;
        localObject2 = paramString2;
        ((UserAttributeParcel)localObject1).<init>(paramString2, paramLong, paramObject, paramString1);
        localObject2 = g();
        ((ae)localObject2).a((UserAttributeParcel)localObject1);
      }
    }
  }
  
  private void a(boolean paramBoolean)
  {
    e();
    c();
    y();
    t.a locala = l().s();
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    locala.a("Setting app measurement enabled (FE)", localBoolean);
    m().b(paramBoolean);
    g().o();
  }
  
  private void p()
  {
    try
    {
      Object localObject = q();
      localObject = Class.forName((String)localObject);
      a((Class)localObject);
      return;
    }
    catch (ClassNotFoundException localClassNotFoundException)
    {
      for (;;)
      {
        t.a locala = l().r();
        String str = "Tag Manager is not found and thus will not be used";
        locala.a(str);
      }
    }
  }
  
  private String q()
  {
    return "com.google.android.gms.tagmanager.TagManagerService";
  }
  
  protected void a() {}
  
  public void a(Class paramClass)
  {
    Object localObject1 = "initialize";
    int i = 1;
    try
    {
      localObject2 = new Class[i];
      int j = 0;
      localObject3 = null;
      Class localClass = Context.class;
      localObject2[0] = localClass;
      localObject1 = paramClass.getDeclaredMethod((String)localObject1, (Class[])localObject2);
      i = 0;
      localObject2 = null;
      j = 1;
      localObject3 = new Object[j];
      localClass = null;
      Context localContext = i();
      localObject3[0] = localContext;
      ((Method)localObject1).invoke(null, (Object[])localObject3);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = l().o();
        Object localObject3 = "Failed to invoke Tag Manager's initialize() method";
        ((t.a)localObject2).a((String)localObject3, localException);
      }
    }
  }
  
  public void a(String paramString1, String paramString2, Bundle paramBundle)
  {
    c();
    a(paramString1, paramString2, paramBundle, true, null);
  }
  
  public void a(String paramString1, String paramString2, Object paramObject)
  {
    p.a(paramString1);
    long l = h().a();
    Object localObject1 = j();
    ((f)localObject1).b(paramString2);
    Object localObject2;
    Object localObject3;
    if (paramObject != null)
    {
      j().b(paramString2, paramObject);
      localObject1 = j();
      localObject2 = ((f)localObject1).c(paramString2, paramObject);
      if (localObject2 != null)
      {
        localObject1 = k();
        ad.3 local3 = new com/google/android/gms/measurement/internal/ad$3;
        localObject3 = this;
        local3.<init>(this, paramString1, paramString2, localObject2, l);
        ((x)localObject1).a(local3);
      }
    }
    for (;;)
    {
      return;
      localObject1 = k();
      localObject3 = new com/google/android/gms/measurement/internal/ad$4;
      localObject2 = paramString2;
      ((ad.4)localObject3).<init>(this, paramString1, paramString2, l);
      ((x)localObject1).a((Runnable)localObject3);
    }
  }
  
  public void b()
  {
    Object localObject1 = i().getApplicationContext();
    boolean bool = localObject1 instanceof Application;
    if (bool)
    {
      localObject1 = (Application)i().getApplicationContext();
      Object localObject2 = a;
      if (localObject2 == null)
      {
        localObject2 = new com/google/android/gms/measurement/internal/ad$a;
        ((ad.a)localObject2).<init>(this, null);
        a = ((ad.a)localObject2);
      }
      localObject2 = a;
      ((Application)localObject1).unregisterActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject2);
      localObject2 = a;
      ((Application)localObject1).registerActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks)localObject2);
      localObject1 = l().t();
      localObject2 = "Registered activity lifecycle callback";
      ((t.a)localObject1).a((String)localObject2);
    }
  }
  
  public void o()
  {
    e();
    c();
    y();
    Object localObject = g;
    boolean bool = ((y)localObject).b();
    if (!bool) {}
    for (;;)
    {
      return;
      localObject = g();
      ((ae)localObject).p();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/ad.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */