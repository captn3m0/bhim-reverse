package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.b;
import com.google.android.gms.common.internal.c;

public class h
  extends aa
{
  static final String a = String.valueOf(b.a / 1000).replaceAll("(\\d+)(\\d)(\\d\\d)", "$1.$2.$3");
  private Boolean b;
  
  h(y paramy)
  {
    super(paramy);
  }
  
  public String A()
  {
    return "google_app_measurement2.db";
  }
  
  public long B()
  {
    return b.a / 1000;
  }
  
  public boolean C()
  {
    return c.a;
  }
  
  /* Error */
  public boolean D()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 42	com/google/android/gms/measurement/internal/h:b	Ljava/lang/Boolean;
    //   4: astore_1
    //   5: aload_1
    //   6: ifnonnull +116 -> 122
    //   9: aload_0
    //   10: monitorenter
    //   11: aload_0
    //   12: getfield 42	com/google/android/gms/measurement/internal/h:b	Ljava/lang/Boolean;
    //   15: astore_1
    //   16: aload_1
    //   17: ifnonnull +103 -> 120
    //   20: aload_0
    //   21: invokevirtual 46	com/google/android/gms/measurement/internal/h:i	()Landroid/content/Context;
    //   24: astore_1
    //   25: aload_1
    //   26: invokevirtual 52	android/content/Context:getApplicationInfo	()Landroid/content/pm/ApplicationInfo;
    //   29: astore_1
    //   30: aload_0
    //   31: invokevirtual 46	com/google/android/gms/measurement/internal/h:i	()Landroid/content/Context;
    //   34: astore_2
    //   35: invokestatic 58	android/os/Process:myPid	()I
    //   38: istore_3
    //   39: aload_2
    //   40: iload_3
    //   41: invokestatic 63	com/google/android/gms/internal/i:a	(Landroid/content/Context;I)Ljava/lang/String;
    //   44: astore_2
    //   45: aload_1
    //   46: ifnull +38 -> 84
    //   49: aload_1
    //   50: getfield 68	android/content/pm/ApplicationInfo:processName	Ljava/lang/String;
    //   53: astore_1
    //   54: aload_1
    //   55: ifnull +75 -> 130
    //   58: aload_1
    //   59: aload_2
    //   60: invokevirtual 72	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   63: istore 4
    //   65: iload 4
    //   67: ifeq +63 -> 130
    //   70: iconst_1
    //   71: istore 4
    //   73: iload 4
    //   75: invokestatic 78	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
    //   78: astore_1
    //   79: aload_0
    //   80: aload_1
    //   81: putfield 42	com/google/android/gms/measurement/internal/h:b	Ljava/lang/Boolean;
    //   84: aload_0
    //   85: getfield 42	com/google/android/gms/measurement/internal/h:b	Ljava/lang/Boolean;
    //   88: astore_1
    //   89: aload_1
    //   90: ifnonnull +30 -> 120
    //   93: getstatic 81	java/lang/Boolean:TRUE	Ljava/lang/Boolean;
    //   96: astore_1
    //   97: aload_0
    //   98: aload_1
    //   99: putfield 42	com/google/android/gms/measurement/internal/h:b	Ljava/lang/Boolean;
    //   102: aload_0
    //   103: invokevirtual 85	com/google/android/gms/measurement/internal/h:l	()Lcom/google/android/gms/measurement/internal/t;
    //   106: astore_1
    //   107: aload_1
    //   108: invokevirtual 90	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   111: astore_1
    //   112: ldc 92
    //   114: astore_2
    //   115: aload_1
    //   116: aload_2
    //   117: invokevirtual 97	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   120: aload_0
    //   121: monitorexit
    //   122: aload_0
    //   123: getfield 42	com/google/android/gms/measurement/internal/h:b	Ljava/lang/Boolean;
    //   126: invokevirtual 101	java/lang/Boolean:booleanValue	()Z
    //   129: ireturn
    //   130: iconst_0
    //   131: istore 4
    //   133: aconst_null
    //   134: astore_1
    //   135: goto -62 -> 73
    //   138: astore_1
    //   139: aload_0
    //   140: monitorexit
    //   141: aload_1
    //   142: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	143	0	this	h
    //   4	131	1	localObject1	Object
    //   138	4	1	localObject2	Object
    //   34	83	2	localObject3	Object
    //   38	3	3	i	int
    //   63	69	4	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   11	15	138	finally
    //   20	24	138	finally
    //   25	29	138	finally
    //   30	34	138	finally
    //   35	38	138	finally
    //   40	44	138	finally
    //   49	53	138	finally
    //   59	63	138	finally
    //   73	78	138	finally
    //   80	84	138	finally
    //   84	88	138	finally
    //   93	96	138	finally
    //   98	102	138	finally
    //   102	106	138	finally
    //   107	111	138	finally
    //   116	120	138	finally
    //   120	122	138	finally
    //   139	141	138	finally
  }
  
  public long E()
  {
    return ((Long)p.p.a()).longValue();
  }
  
  public long F()
  {
    return ((Long)p.l.a()).longValue();
  }
  
  public long G()
  {
    return 20;
  }
  
  public long H()
  {
    long l = ((Long)p.e.a()).longValue();
    return Math.max(0L, l);
  }
  
  public int I()
  {
    return ((Integer)p.f.a()).intValue();
  }
  
  public int J()
  {
    int i = ((Integer)p.g.a()).intValue();
    return Math.max(0, i);
  }
  
  public String K()
  {
    return (String)p.h.a();
  }
  
  public long L()
  {
    long l = ((Long)p.i.a()).longValue();
    return Math.max(0L, l);
  }
  
  public long M()
  {
    long l = ((Long)p.k.a()).longValue();
    return Math.max(0L, l);
  }
  
  public long N()
  {
    return ((Long)p.j.a()).longValue();
  }
  
  public long O()
  {
    long l = ((Long)p.m.a()).longValue();
    return Math.max(0L, l);
  }
  
  public long P()
  {
    long l = ((Long)p.n.a()).longValue();
    return Math.max(0L, l);
  }
  
  public int Q()
  {
    int i = ((Integer)p.o.a()).intValue();
    i = Math.max(0, i);
    return Math.min(20, i);
  }
  
  String a()
  {
    return (String)p.c.a();
  }
  
  int b()
  {
    return 32;
  }
  
  public int o()
  {
    return 24;
  }
  
  int p()
  {
    return 36;
  }
  
  int q()
  {
    return 256;
  }
  
  int r()
  {
    return 36;
  }
  
  int s()
  {
    return 2048;
  }
  
  int t()
  {
    return 20;
  }
  
  long u()
  {
    return 3600000L;
  }
  
  long v()
  {
    return 60000L;
  }
  
  long w()
  {
    return 61000L;
  }
  
  long x()
  {
    return ((Long)p.d.a()).longValue();
  }
  
  long y()
  {
    return ((Long)p.q.a()).longValue();
  }
  
  public String z()
  {
    return "google_app_measurement.db";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/h.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */