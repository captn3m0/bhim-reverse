package com.google.android.gms.measurement.internal;

class u$b
  implements Runnable
{
  private final u.a a;
  private final int b;
  private final Throwable c;
  private final byte[] d;
  
  private u$b(u.a parama, int paramInt, Throwable paramThrowable, byte[] paramArrayOfByte)
  {
    a = parama;
    b = paramInt;
    c = paramThrowable;
    d = paramArrayOfByte;
  }
  
  public void run()
  {
    u.a locala = a;
    int i = b;
    Throwable localThrowable = c;
    byte[] arrayOfByte = d;
    locala.a(i, localThrowable, arrayOfByte);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/u$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */