package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.measurement.a;

public class t
  extends ab
{
  private final String a;
  private final char b;
  private final long c;
  private final t.a d;
  private final t.a e;
  private final t.a f;
  private final t.a h;
  private final t.a i;
  private final t.a j;
  private final t.a k;
  private final t.a l;
  private final t.a m;
  
  t(y paramy)
  {
    super(paramy);
    Object localObject = n().a();
    a = ((String)localObject);
    long l1 = n().B();
    c = l1;
    localObject = n();
    boolean bool2 = ((h)localObject).D();
    if (bool2)
    {
      localObject = n();
      bool2 = ((h)localObject).C();
      if (bool2) {}
      for (char c1 = 'P';; c1 = 'C')
      {
        b = c1;
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, n, false, false);
        d = ((t.a)localObject);
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, n, bool1, false);
        e = ((t.a)localObject);
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, n, false, bool1);
        f = ((t.a)localObject);
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, i1, false, false);
        h = ((t.a)localObject);
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, i1, bool1, false);
        i = ((t.a)localObject);
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, i1, false, bool1);
        j = ((t.a)localObject);
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, 4, false, false);
        k = ((t.a)localObject);
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, 3, false, false);
        l = ((t.a)localObject);
        localObject = new com/google/android/gms/measurement/internal/t$a;
        ((t.a)localObject).<init>(this, 2, false, false);
        m = ((t.a)localObject);
        return;
      }
    }
    localObject = n();
    boolean bool3 = ((h)localObject).C();
    if (bool3) {}
    for (char c2 = 'p';; c2 = 'c')
    {
      b = c2;
      break;
    }
  }
  
  private static String a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool) {
      paramString = "";
    }
    for (;;)
    {
      return paramString;
      int n = paramString.lastIndexOf('.');
      int i1 = -1;
      if (n != i1)
      {
        i1 = 0;
        paramString = paramString.substring(0, n);
      }
    }
  }
  
  static String a(boolean paramBoolean, Object paramObject)
  {
    int n = 0;
    StringBuilder localStringBuilder = null;
    double d1 = 10.0D;
    Object localObject1;
    if (paramObject == null)
    {
      localObject1 = "";
      return (String)localObject1;
    }
    boolean bool1 = paramObject instanceof Integer;
    long l1;
    if (bool1)
    {
      paramObject = (Integer)paramObject;
      int i1 = ((Integer)paramObject).intValue();
      l1 = i1;
    }
    for (Object localObject2 = Long.valueOf(l1);; localObject2 = paramObject)
    {
      boolean bool2 = localObject2 instanceof Long;
      Object localObject3;
      if (bool2)
      {
        if (!paramBoolean)
        {
          localObject1 = String.valueOf(localObject2);
          break;
        }
        localObject1 = localObject2;
        localObject1 = (Long)localObject2;
        long l2 = Math.abs(((Long)localObject1).longValue());
        long l3 = 100;
        bool2 = l2 < l3;
        if (bool2)
        {
          localObject1 = String.valueOf(localObject2);
          break;
        }
        localObject1 = String.valueOf(localObject2);
        int i2 = ((String)localObject1).charAt(0);
        n = 45;
        if (i2 == n) {}
        for (localObject1 = "-";; localObject1 = "")
        {
          localObject2 = String.valueOf(Math.abs(((Long)localObject2).longValue()));
          localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder = localStringBuilder.append((String)localObject1);
          int i5 = ((String)localObject2).length() + -1;
          double d2 = i5;
          l2 = Math.round(Math.pow(d1, d2));
          localStringBuilder = localStringBuilder.append(l2);
          localObject3 = "...";
          localStringBuilder = localStringBuilder.append((String)localObject3);
          localObject1 = localStringBuilder.append((String)localObject1);
          int i6 = ((String)localObject2).length();
          double d3 = i6;
          d3 = Math.pow(d1, d3);
          l2 = 4607182418800017408L;
          d2 = 1.0D;
          d3 -= d2;
          long l4 = Math.round(d3);
          localObject1 = l4;
          break;
        }
      }
      int i3 = localObject2 instanceof Boolean;
      if (i3 != 0)
      {
        localObject1 = String.valueOf(localObject2);
        break;
      }
      i3 = localObject2 instanceof Throwable;
      if (i3 != 0)
      {
        localObject2 = (Throwable)localObject2;
        localObject3 = new java/lang/StringBuilder;
        localObject1 = ((Throwable)localObject2).toString();
        ((StringBuilder)localObject3).<init>((String)localObject1);
        String str1 = a(a.class.getCanonicalName());
        String str2 = a(y.class.getCanonicalName());
        localObject2 = ((Throwable)localObject2).getStackTrace();
        int i7 = localObject2.length;
        i3 = 0;
        localObject1 = null;
        if (i3 < i7)
        {
          localStringBuilder = localObject2[i3];
          boolean bool3 = localStringBuilder.isNativeMethod();
          if (bool3) {}
          do
          {
            do
            {
              int i4;
              i3 += 1;
              break;
              str3 = localStringBuilder.getClassName();
            } while (str3 == null);
            String str3 = a(str3);
            boolean bool4 = str3.equals(str1);
            if (bool4) {
              break label483;
            }
            bool3 = str3.equals(str2);
          } while (!bool3);
          label483:
          localObject1 = ": ";
          ((StringBuilder)localObject3).append((String)localObject1);
          ((StringBuilder)localObject3).append(localStringBuilder);
        }
        localObject1 = ((StringBuilder)localObject3).toString();
        break;
      }
      if (paramBoolean)
      {
        localObject1 = "-";
        break;
      }
      localObject1 = String.valueOf(localObject2);
      break;
    }
  }
  
  static String a(boolean paramBoolean, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    if (paramString == null) {
      paramString = "";
    }
    String str1 = a(paramBoolean, paramObject1);
    String str2 = a(paramBoolean, paramObject2);
    String str3 = a(paramBoolean, paramObject3);
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str4 = "";
    boolean bool1 = TextUtils.isEmpty(paramString);
    if (!bool1)
    {
      localStringBuilder.append(paramString);
      str4 = ": ";
    }
    bool1 = TextUtils.isEmpty(str1);
    if (!bool1)
    {
      localStringBuilder.append(str4);
      localStringBuilder.append(str1);
      str4 = ", ";
    }
    boolean bool2 = TextUtils.isEmpty(str2);
    if (!bool2)
    {
      localStringBuilder.append(str4);
      localStringBuilder.append(str2);
      str4 = ", ";
    }
    bool2 = TextUtils.isEmpty(str3);
    if (!bool2)
    {
      localStringBuilder.append(str4);
      localStringBuilder.append(str3);
    }
    return localStringBuilder.toString();
  }
  
  protected void a() {}
  
  protected void a(int paramInt, String paramString)
  {
    String str = a;
    Log.println(paramInt, str, paramString);
  }
  
  public void a(int paramInt, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    int n = 1024;
    int i1 = 0;
    Object localObject1 = null;
    p.a(paramString);
    Object localObject2 = g;
    x localx = ((y)localObject2).h();
    if (localx != null)
    {
      boolean bool = localx.w();
      if (bool)
      {
        bool = localx.x();
        if (!bool) {
          break label75;
        }
      }
    }
    i1 = 6;
    localObject2 = "Scheduler not initialized or shutdown. Not logging error/warn.";
    a(i1, (String)localObject2);
    return;
    label75:
    if (paramInt < 0) {
      paramInt = 0;
    }
    localObject2 = "01VDIWEA?";
    int i2 = ((String)localObject2).length();
    if (paramInt >= i2)
    {
      localObject2 = "01VDIWEA?";
      i2 = ((String)localObject2).length();
      paramInt = i2 + -1;
    }
    localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = ((StringBuilder)localObject2).append("1");
    char c1 = "01VDIWEA?".charAt(paramInt);
    localObject2 = ((StringBuilder)localObject2).append(c1);
    c1 = b;
    localObject2 = ((StringBuilder)localObject2).append(c1);
    long l1 = c;
    localObject2 = ((StringBuilder)localObject2).append(l1).append(":");
    String str = a(true, paramString, paramObject1, paramObject2, paramObject3);
    localObject2 = str;
    int i3 = ((String)localObject2).length();
    if (i3 > n) {}
    for (localObject1 = paramString.substring(0, n);; localObject1 = localObject2)
    {
      localObject2 = new com/google/android/gms/measurement/internal/t$1;
      ((t.1)localObject2).<init>(this, (String)localObject1);
      localx.a((Runnable)localObject2);
      break;
    }
  }
  
  protected void a(int paramInt, boolean paramBoolean1, boolean paramBoolean2, String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    Object localObject;
    if (!paramBoolean1)
    {
      boolean bool = a(paramInt);
      if (bool)
      {
        bool = false;
        localObject = a(false, paramString, paramObject1, paramObject2, paramObject3);
        a(paramInt, (String)localObject);
      }
    }
    if (!paramBoolean2)
    {
      int n = 5;
      if (paramInt >= n)
      {
        localObject = this;
        a(paramInt, paramString, paramObject1, paramObject2, paramObject3);
      }
    }
  }
  
  protected boolean a(int paramInt)
  {
    return Log.isLoggable(a, paramInt);
  }
  
  public t.a b()
  {
    return d;
  }
  
  public t.a o()
  {
    return h;
  }
  
  public t.a p()
  {
    return i;
  }
  
  public t.a q()
  {
    return j;
  }
  
  public t.a r()
  {
    return k;
  }
  
  public t.a s()
  {
    return l;
  }
  
  public t.a t()
  {
    return m;
  }
  
  public String u()
  {
    Object localObject = mb.a();
    if (localObject == null) {}
    StringBuilder localStringBuilder;
    for (localObject = null;; localObject = (String)localObject)
    {
      return (String)localObject;
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      String str = String.valueOf(second);
      localStringBuilder = localStringBuilder.append(str);
      str = ":";
      localStringBuilder = localStringBuilder.append(str);
      localObject = (String)first;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/t.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */