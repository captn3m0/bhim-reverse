package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Binder;
import android.os.Process;
import android.text.TextUtils;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.internal.p;

public class z
  extends q.a
{
  private final y a;
  private final boolean b;
  
  public z(y paramy)
  {
    p.a(paramy);
    a = paramy;
    b = false;
  }
  
  public z(y paramy, boolean paramBoolean)
  {
    p.a(paramy);
    a = paramy;
    b = paramBoolean;
  }
  
  private void b(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    if (bool)
    {
      a.f().b().a("Measurement Service called without app package");
      SecurityException localSecurityException1 = new java/lang/SecurityException;
      localSecurityException1.<init>("Measurement Service called without app package");
      throw localSecurityException1;
    }
    try
    {
      c(paramString);
      return;
    }
    catch (SecurityException localSecurityException2)
    {
      a.f().b().a("Measurement Service called with invalid calling package", paramString);
      throw localSecurityException2;
    }
  }
  
  private void c(String paramString)
  {
    boolean bool1 = b;
    int i;
    if (bool1)
    {
      i = Process.myUid();
      localObject1 = a.n();
      boolean bool3 = GooglePlayServicesUtil.zzb((Context)localObject1, i, paramString);
      if (!bool3) {
        break label42;
      }
    }
    label42:
    boolean bool2;
    do
    {
      return;
      i = Binder.getCallingUid();
      break;
      localObject1 = a.n();
      bool2 = GooglePlayServicesUtil.zze((Context)localObject1, i);
      if (!bool2) {
        break label76;
      }
      localObject2 = a;
      bool2 = ((y)localObject2).v();
    } while (!bool2);
    label76:
    Object localObject2 = new java/lang/SecurityException;
    Object[] arrayOfObject = new Object[1];
    arrayOfObject[0] = paramString;
    Object localObject1 = String.format("Unknown calling package name '%s'.", arrayOfObject);
    ((SecurityException)localObject2).<init>((String)localObject1);
    throw ((Throwable)localObject2);
  }
  
  public void a(AppMetadata paramAppMetadata)
  {
    p.a(paramAppMetadata);
    Object localObject = b;
    b((String)localObject);
    localObject = a.g();
    z.6 local6 = new com/google/android/gms/measurement/internal/z$6;
    local6.<init>(this, paramAppMetadata);
    ((x)localObject).a(local6);
  }
  
  public void a(EventParcel paramEventParcel, AppMetadata paramAppMetadata)
  {
    p.a(paramEventParcel);
    p.a(paramAppMetadata);
    Object localObject = b;
    b((String)localObject);
    localObject = a.g();
    z.2 local2 = new com/google/android/gms/measurement/internal/z$2;
    local2.<init>(this, paramAppMetadata, paramEventParcel);
    ((x)localObject).a(local2);
  }
  
  public void a(EventParcel paramEventParcel, String paramString1, String paramString2)
  {
    p.a(paramEventParcel);
    p.a(paramString1);
    b(paramString1);
    x localx = a.g();
    z.3 local3 = new com/google/android/gms/measurement/internal/z$3;
    local3.<init>(this, paramString2, paramEventParcel, paramString1);
    localx.a(local3);
  }
  
  public void a(UserAttributeParcel paramUserAttributeParcel, AppMetadata paramAppMetadata)
  {
    p.a(paramUserAttributeParcel);
    p.a(paramAppMetadata);
    Object localObject1 = b;
    b((String)localObject1);
    localObject1 = paramUserAttributeParcel.a();
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = a.g();
      localObject2 = new com/google/android/gms/measurement/internal/z$4;
      ((z.4)localObject2).<init>(this, paramAppMetadata, paramUserAttributeParcel);
      ((x)localObject1).a((Runnable)localObject2);
    }
    for (;;)
    {
      return;
      localObject1 = a.g();
      localObject2 = new com/google/android/gms/measurement/internal/z$5;
      ((z.5)localObject2).<init>(this, paramAppMetadata, paramUserAttributeParcel);
      ((x)localObject1).a((Runnable)localObject2);
    }
  }
  
  void a(String paramString)
  {
    int i = 2;
    boolean bool1 = TextUtils.isEmpty(paramString);
    Object localObject1;
    Object localObject2;
    if (!bool1)
    {
      localObject1 = paramString.split(":", i);
      int j = localObject1.length;
      if (j == i)
      {
        j = 0;
        localObject2 = null;
      }
    }
    try
    {
      localObject2 = localObject1[0];
      localObject2 = Long.valueOf((String)localObject2);
      l1 = ((Long)localObject2).longValue();
      long l2 = 0L;
      boolean bool2 = l1 < l2;
      if (bool2)
      {
        localObject2 = a.e().b;
        int k = 1;
        localObject1 = localObject1[k];
        ((w.b)localObject2).a((String)localObject1, l1);
        return;
      }
    }
    catch (NumberFormatException localNumberFormatException)
    {
      for (;;)
      {
        long l1;
        Object localObject3 = a.f().o();
        Object localObject4 = "Combining sample with a non-number weight";
        localObject1 = localObject1[0];
        ((t.a)localObject3).a((String)localObject4, localObject1);
        continue;
        localObject1 = a.f().o();
        localObject3 = "Combining sample with a non-positive weight";
        localObject4 = Long.valueOf(l1);
        ((t.a)localObject1).a((String)localObject3, localObject4);
      }
    }
  }
  
  public void b(AppMetadata paramAppMetadata)
  {
    p.a(paramAppMetadata);
    Object localObject = b;
    b((String)localObject);
    localObject = a.g();
    z.1 local1 = new com/google/android/gms/measurement/internal/z$1;
    local1.<init>(this, paramAppMetadata);
    ((x)localObject).a(local1);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/z.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */