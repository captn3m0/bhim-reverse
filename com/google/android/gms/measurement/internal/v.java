package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.google.android.gms.common.internal.p;

class v
  extends BroadcastReceiver
{
  static final String a = v.class.getName();
  private final y b;
  private boolean c;
  private boolean d;
  
  v(y paramy)
  {
    p.a(paramy);
    b = paramy;
  }
  
  private Context d()
  {
    return b.n();
  }
  
  private t e()
  {
    return b.f();
  }
  
  public void a()
  {
    b.a();
    Object localObject1 = b;
    ((y)localObject1).u();
    boolean bool1 = c;
    if (bool1) {}
    for (;;)
    {
      return;
      localObject1 = d();
      Object localObject2 = new android/content/IntentFilter;
      ((IntentFilter)localObject2).<init>("android.net.conn.CONNECTIVITY_CHANGE");
      ((Context)localObject1).registerReceiver(this, (IntentFilter)localObject2);
      bool1 = b.m().b();
      d = bool1;
      localObject1 = e().t();
      localObject2 = "Registering connectivity change receiver. Network connected";
      boolean bool2 = d;
      Boolean localBoolean = Boolean.valueOf(bool2);
      ((t.a)localObject1).a((String)localObject2, localBoolean);
      bool1 = true;
      c = bool1;
    }
  }
  
  public void b()
  {
    String str = null;
    b.a();
    Object localObject1 = b;
    ((y)localObject1).u();
    boolean bool = c();
    if (!bool) {}
    for (;;)
    {
      return;
      localObject1 = e().t();
      Object localObject2 = "Unregistering connectivity change receiver";
      ((t.a)localObject1).a((String)localObject2);
      c = false;
      d = false;
      localObject1 = d();
      try
      {
        ((Context)localObject1).unregisterReceiver(this);
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        localObject2 = e().b();
        str = "Failed to unregister the network broadcast receiver";
        ((t.a)localObject2).a(str, localIllegalArgumentException);
      }
    }
  }
  
  public boolean c()
  {
    b.u();
    return c;
  }
  
  public void onReceive(Context paramContext, Intent paramIntent)
  {
    b.a();
    Object localObject1 = paramIntent.getAction();
    Object localObject2 = e().t();
    Object localObject3 = "NetworkBroadcastReceiver received action";
    ((t.a)localObject2).a((String)localObject3, localObject1);
    localObject2 = "android.net.conn.CONNECTIVITY_CHANGE";
    boolean bool1 = ((String)localObject2).equals(localObject1);
    if (bool1)
    {
      localObject1 = b.m();
      boolean bool2 = ((u)localObject1).b();
      bool1 = d;
      if (bool1 != bool2)
      {
        d = bool2;
        localObject2 = b.g();
        localObject3 = new com/google/android/gms/measurement/internal/v$1;
        ((v.1)localObject3).<init>(this, bool2);
        ((x)localObject2).a((Runnable)localObject3);
      }
    }
    for (;;)
    {
      return;
      localObject2 = e().o();
      localObject3 = "NetworkBroadcastReceiver received unknown action";
      ((t.a)localObject2).a((String)localObject3, localObject1);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/v.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */