package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.Looper;
import com.google.android.gms.common.api.c.b;
import com.google.android.gms.common.api.c.c;
import com.google.android.gms.common.internal.d;
import com.google.android.gms.common.internal.h;

public class s
  extends h
{
  public s(Context paramContext, Looper paramLooper, d paramd, c.b paramb, c.c paramc)
  {
    super(paramContext, paramLooper, 93, paramd, paramb, paramc);
  }
  
  protected String a()
  {
    return "com.google.android.gms.measurement.START";
  }
  
  public q b(IBinder paramIBinder)
  {
    return q.a.a(paramIBinder);
  }
  
  protected String b()
  {
    return "com.google.android.gms.measurement.internal.IMeasurementService";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/s.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */