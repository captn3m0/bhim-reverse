package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Pair;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.e;
import java.security.SecureRandom;

public final class w$b
{
  final String a;
  private final String c;
  private final String d;
  private final long e;
  
  private w$b(w paramw, String paramString, long paramLong)
  {
    p.a(paramString);
    long l = 0L;
    boolean bool = paramLong < l;
    if (bool) {
      bool = true;
    }
    for (;;)
    {
      p.b(bool);
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = paramString + ":start";
      a = ((String)localObject);
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = paramString + ":count";
      c = ((String)localObject);
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = paramString + ":value";
      d = ((String)localObject);
      e = paramLong;
      return;
      bool = false;
      localObject = null;
    }
  }
  
  private void b()
  {
    b.e();
    long l = b.h().a();
    SharedPreferences.Editor localEditor = w.a(b).edit();
    String str = c;
    localEditor.remove(str);
    str = d;
    localEditor.remove(str);
    str = a;
    localEditor.putLong(str, l);
    localEditor.apply();
  }
  
  private long c()
  {
    long l1 = 0L;
    w localw = b;
    localw.e();
    long l2 = d();
    boolean bool = l2 < l1;
    if (!bool) {
      b();
    }
    for (;;)
    {
      return l1;
      e locale = b.h();
      l1 = locale.a();
      l1 = Math.abs(l2 - l1);
    }
  }
  
  private long d()
  {
    SharedPreferences localSharedPreferences = w.c(b);
    String str = a;
    return localSharedPreferences.getLong(str, 0L);
  }
  
  public Pair a()
  {
    long l1 = 0L;
    boolean bool1 = false;
    Object localObject1 = null;
    Object localObject2 = b;
    ((w)localObject2).e();
    long l2 = c();
    long l3 = e;
    boolean bool2 = l2 < l3;
    if (bool2) {}
    for (;;)
    {
      return (Pair)localObject1;
      l3 = e;
      long l4 = 2;
      l3 *= l4;
      bool2 = l2 < l3;
      if (bool2)
      {
        b();
      }
      else
      {
        localObject2 = w.c(b);
        Object localObject3 = d;
        localObject2 = ((SharedPreferences)localObject2).getString((String)localObject3, null);
        localObject1 = w.c(b);
        localObject3 = c;
        l2 = ((SharedPreferences)localObject1).getLong((String)localObject3, l1);
        b();
        if (localObject2 != null)
        {
          bool1 = l2 < l1;
          if (bool1) {}
        }
        else
        {
          localObject1 = w.a;
          continue;
        }
        localObject1 = new android/util/Pair;
        localObject3 = Long.valueOf(l2);
        ((Pair)localObject1).<init>(localObject2, localObject3);
      }
    }
  }
  
  public void a(String paramString)
  {
    a(paramString, 1L);
  }
  
  public void a(String paramString, long paramLong)
  {
    long l1 = Long.MAX_VALUE;
    long l2 = 0L;
    Object localObject1 = b;
    ((w)localObject1).e();
    long l3 = d();
    boolean bool = l3 < l2;
    if (!bool) {
      b();
    }
    if (paramString == null) {
      paramString = "";
    }
    localObject1 = w.a(b);
    Object localObject2 = c;
    long l4 = ((SharedPreferences)localObject1).getLong((String)localObject2, l2);
    bool = l4 < l2;
    if (!bool)
    {
      localObject1 = w.a(b).edit();
      localObject2 = d;
      ((SharedPreferences.Editor)localObject1).putString((String)localObject2, paramString);
      localObject2 = c;
      ((SharedPreferences.Editor)localObject1).putLong((String)localObject2, paramLong);
      ((SharedPreferences.Editor)localObject1).apply();
      return;
    }
    localObject1 = w.b(b);
    l3 = ((SecureRandom)localObject1).nextLong() & l1;
    l2 = l4 + paramLong;
    l2 = l1 / l2 * paramLong;
    bool = l3 < l2;
    if (bool) {
      bool = true;
    }
    for (;;)
    {
      localObject2 = w.a(b).edit();
      if (bool)
      {
        localObject1 = d;
        ((SharedPreferences.Editor)localObject2).putString((String)localObject1, paramString);
      }
      localObject1 = c;
      l4 += paramLong;
      ((SharedPreferences.Editor)localObject2).putLong((String)localObject1, l4);
      ((SharedPreferences.Editor)localObject2).apply();
      break;
      bool = false;
      localObject1 = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/w$b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */