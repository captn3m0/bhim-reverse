package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.CursorWindow;
import android.database.sqlite.SQLiteCursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build.VERSION;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.e;
import java.io.File;
import java.util.Map;

class i
  extends ab
{
  private static final Map a;
  private final i.a b;
  private final b c;
  
  static
  {
    android.support.v4.e.a locala = new android/support/v4/e/a;
    locala.<init>(5);
    a = locala;
    a.put("app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;");
    a.put("app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;");
    a.put("gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;");
    a.put("dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;");
    a.put("measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;");
  }
  
  i(y paramy)
  {
    super(paramy);
    Object localObject1 = new com/google/android/gms/measurement/internal/b;
    Object localObject2 = h();
    ((b)localObject1).<init>((e)localObject2);
    c = ((b)localObject1);
    localObject1 = A();
    localObject2 = new com/google/android/gms/measurement/internal/i$a;
    Context localContext = i();
    ((i.a)localObject2).<init>(this, localContext, (String)localObject1);
    b = ((i.a)localObject2);
  }
  
  private String A()
  {
    Object localObject = n();
    boolean bool = ((h)localObject).C();
    if (!bool) {
      localObject = n().z();
    }
    for (;;)
    {
      return (String)localObject;
      localObject = n();
      bool = ((h)localObject).D();
      if (bool)
      {
        localObject = n().z();
      }
      else
      {
        localObject = l().p();
        String str = "Using secondary database";
        ((t.a)localObject).a(str);
        localObject = n().A();
      }
    }
  }
  
  private boolean B()
  {
    Context localContext = i();
    String str = A();
    return localContext.getDatabasePath(str).exists();
  }
  
  static int a(Cursor paramCursor, int paramInt)
  {
    int i = Build.VERSION.SDK_INT;
    int k = 11;
    if (i >= k) {
      i = paramCursor.getType(paramInt);
    }
    for (;;)
    {
      return i;
      Object localObject = paramCursor;
      localObject = ((SQLiteCursor)paramCursor).getWindow();
      k = paramCursor.getPosition();
      boolean bool2 = ((CursorWindow)localObject).isNull(k, paramInt);
      if (bool2)
      {
        i = 0;
        localObject = null;
      }
      else
      {
        bool2 = ((CursorWindow)localObject).isLong(k, paramInt);
        if (bool2)
        {
          i = 1;
        }
        else
        {
          bool2 = ((CursorWindow)localObject).isFloat(k, paramInt);
          if (bool2)
          {
            i = 2;
          }
          else
          {
            bool2 = ((CursorWindow)localObject).isString(k, paramInt);
            if (bool2)
            {
              i = 3;
            }
            else
            {
              boolean bool1 = ((CursorWindow)localObject).isBlob(k, paramInt);
              int j;
              if (bool1) {
                j = 4;
              } else {
                j = -1;
              }
            }
          }
        }
      }
    }
  }
  
  /* Error */
  private long a(String paramString, String[] paramArrayOfString, long paramLong)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 168	com/google/android/gms/measurement/internal/i:q	()Landroid/database/sqlite/SQLiteDatabase;
    //   4: astore 5
    //   6: aconst_null
    //   7: astore 6
    //   9: aload 5
    //   11: aload_1
    //   12: aload_2
    //   13: invokevirtual 174	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   16: astore 6
    //   18: aload 6
    //   20: invokeinterface 177 1 0
    //   25: istore 7
    //   27: iload 7
    //   29: ifeq +32 -> 61
    //   32: iconst_0
    //   33: istore 7
    //   35: aconst_null
    //   36: astore 5
    //   38: aload 6
    //   40: iconst_0
    //   41: invokeinterface 181 2 0
    //   46: lstore_3
    //   47: aload 6
    //   49: ifnull +10 -> 59
    //   52: aload 6
    //   54: invokeinterface 185 1 0
    //   59: lload_3
    //   60: lreturn
    //   61: aload 6
    //   63: ifnull -4 -> 59
    //   66: aload 6
    //   68: invokeinterface 185 1 0
    //   73: goto -14 -> 59
    //   76: astore 5
    //   78: aload_0
    //   79: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   82: astore 8
    //   84: aload 8
    //   86: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   89: astore 8
    //   91: ldc -67
    //   93: astore 9
    //   95: aload 8
    //   97: aload 9
    //   99: aload_1
    //   100: aload 5
    //   102: invokevirtual 192	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   105: aload 5
    //   107: athrow
    //   108: astore 5
    //   110: aload 6
    //   112: ifnull +10 -> 122
    //   115: aload 6
    //   117: invokeinterface 185 1 0
    //   122: aload 5
    //   124: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	125	0	this	i
    //   0	125	1	paramString	String
    //   0	125	2	paramArrayOfString	String[]
    //   0	125	3	paramLong	long
    //   4	33	5	localSQLiteDatabase	SQLiteDatabase
    //   76	30	5	localSQLiteException	SQLiteException
    //   108	15	5	localObject1	Object
    //   7	109	6	localCursor	Cursor
    //   25	9	7	bool	boolean
    //   82	14	8	localObject2	Object
    //   93	5	9	str	String
    // Exception table:
    //   from	to	target	type
    //   12	16	76	android/database/sqlite/SQLiteException
    //   18	25	76	android/database/sqlite/SQLiteException
    //   40	46	76	android/database/sqlite/SQLiteException
    //   12	16	108	finally
    //   18	25	108	finally
    //   40	46	108	finally
    //   78	82	108	finally
    //   84	89	108	finally
    //   100	105	108	finally
    //   105	108	108	finally
  }
  
  /* Error */
  public m a(String paramString1, String paramString2)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: aload_1
    //   3: invokestatic 199	com/google/android/gms/common/internal/p:a	(Ljava/lang/String;)Ljava/lang/String;
    //   6: pop
    //   7: aload_2
    //   8: invokestatic 199	com/google/android/gms/common/internal/p:a	(Ljava/lang/String;)Ljava/lang/String;
    //   11: pop
    //   12: aload_0
    //   13: invokevirtual 202	com/google/android/gms/measurement/internal/i:e	()V
    //   16: aload_0
    //   17: invokevirtual 205	com/google/android/gms/measurement/internal/i:y	()V
    //   20: aload_0
    //   21: invokevirtual 168	com/google/android/gms/measurement/internal/i:q	()Landroid/database/sqlite/SQLiteDatabase;
    //   24: astore 4
    //   26: ldc -49
    //   28: astore 5
    //   30: iconst_3
    //   31: istore 6
    //   33: iload 6
    //   35: anewarray 209	java/lang/String
    //   38: astore 7
    //   40: iconst_0
    //   41: istore 8
    //   43: aconst_null
    //   44: astore 9
    //   46: ldc -45
    //   48: astore 10
    //   50: aload 7
    //   52: iconst_0
    //   53: aload 10
    //   55: aastore
    //   56: iconst_1
    //   57: istore 8
    //   59: ldc -43
    //   61: astore 10
    //   63: aload 7
    //   65: iload 8
    //   67: aload 10
    //   69: aastore
    //   70: iconst_2
    //   71: istore 8
    //   73: ldc -41
    //   75: astore 10
    //   77: aload 7
    //   79: iload 8
    //   81: aload 10
    //   83: aastore
    //   84: ldc -39
    //   86: astore 9
    //   88: iconst_2
    //   89: istore 11
    //   91: iload 11
    //   93: anewarray 209	java/lang/String
    //   96: astore 10
    //   98: iconst_0
    //   99: istore 12
    //   101: aload 10
    //   103: iconst_0
    //   104: aload_1
    //   105: aastore
    //   106: iconst_1
    //   107: istore 12
    //   109: aload 10
    //   111: iload 12
    //   113: aload_2
    //   114: aastore
    //   115: iconst_0
    //   116: istore 12
    //   118: aload 4
    //   120: aload 5
    //   122: aload 7
    //   124: aload 9
    //   126: aload 10
    //   128: aconst_null
    //   129: aconst_null
    //   130: aconst_null
    //   131: invokevirtual 221	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   134: astore 13
    //   136: aload 13
    //   138: invokeinterface 177 1 0
    //   143: istore 14
    //   145: iload 14
    //   147: ifne +21 -> 168
    //   150: aload 13
    //   152: ifnull +10 -> 162
    //   155: aload 13
    //   157: invokeinterface 185 1 0
    //   162: aconst_null
    //   163: astore 5
    //   165: aload 5
    //   167: areturn
    //   168: iconst_0
    //   169: istore 14
    //   171: aconst_null
    //   172: astore 4
    //   174: aload 13
    //   176: iconst_0
    //   177: invokeinterface 181 2 0
    //   182: lstore 15
    //   184: iconst_1
    //   185: istore 14
    //   187: aload 13
    //   189: iload 14
    //   191: invokeinterface 181 2 0
    //   196: lstore 17
    //   198: iconst_2
    //   199: istore 14
    //   201: aload 13
    //   203: iload 14
    //   205: invokeinterface 181 2 0
    //   210: lstore 19
    //   212: new 223	com/google/android/gms/measurement/internal/m
    //   215: astore 5
    //   217: aload_1
    //   218: astore 7
    //   220: aload_2
    //   221: astore 9
    //   223: aload 5
    //   225: aload_1
    //   226: aload_2
    //   227: lload 15
    //   229: lload 17
    //   231: lload 19
    //   233: invokespecial 226	com/google/android/gms/measurement/internal/m:<init>	(Ljava/lang/String;Ljava/lang/String;JJJ)V
    //   236: aload 13
    //   238: invokeinterface 229 1 0
    //   243: istore 14
    //   245: iload 14
    //   247: ifeq +27 -> 274
    //   250: aload_0
    //   251: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   254: astore 4
    //   256: aload 4
    //   258: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   261: astore 4
    //   263: ldc -25
    //   265: astore 7
    //   267: aload 4
    //   269: aload 7
    //   271: invokevirtual 107	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   274: aload 13
    //   276: ifnull -111 -> 165
    //   279: aload 13
    //   281: invokeinterface 185 1 0
    //   286: goto -121 -> 165
    //   289: astore 4
    //   291: aconst_null
    //   292: astore 5
    //   294: aload_0
    //   295: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   298: astore 7
    //   300: aload 7
    //   302: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   305: astore 7
    //   307: ldc -23
    //   309: astore 9
    //   311: aload 7
    //   313: aload 9
    //   315: aload_1
    //   316: aload_2
    //   317: aload 4
    //   319: invokevirtual 236	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    //   322: aload 5
    //   324: ifnull +10 -> 334
    //   327: aload 5
    //   329: invokeinterface 185 1 0
    //   334: aconst_null
    //   335: astore 5
    //   337: goto -172 -> 165
    //   340: astore 4
    //   342: aload_3
    //   343: ifnull +9 -> 352
    //   346: aload_3
    //   347: invokeinterface 185 1 0
    //   352: aload 4
    //   354: athrow
    //   355: astore 4
    //   357: aload 13
    //   359: astore_3
    //   360: goto -18 -> 342
    //   363: astore 4
    //   365: aload 5
    //   367: astore_3
    //   368: goto -26 -> 342
    //   371: astore 4
    //   373: aload 13
    //   375: astore 5
    //   377: goto -83 -> 294
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	380	0	this	i
    //   0	380	1	paramString1	String
    //   0	380	2	paramString2	String
    //   1	367	3	localObject1	Object
    //   24	244	4	localObject2	Object
    //   289	29	4	localSQLiteException1	SQLiteException
    //   340	13	4	localObject3	Object
    //   355	1	4	localObject4	Object
    //   363	1	4	localObject5	Object
    //   371	1	4	localSQLiteException2	SQLiteException
    //   28	348	5	localObject6	Object
    //   31	3	6	i	int
    //   38	274	7	localObject7	Object
    //   41	39	8	j	int
    //   44	270	9	str	String
    //   48	79	10	localObject8	Object
    //   89	3	11	k	int
    //   99	18	12	m	int
    //   134	240	13	localCursor	Cursor
    //   143	47	14	n	int
    //   199	5	14	i1	int
    //   243	3	14	bool	boolean
    //   182	46	15	l1	long
    //   196	34	17	l2	long
    //   210	22	19	l3	long
    // Exception table:
    //   from	to	target	type
    //   20	24	289	android/database/sqlite/SQLiteException
    //   33	38	289	android/database/sqlite/SQLiteException
    //   53	56	289	android/database/sqlite/SQLiteException
    //   67	70	289	android/database/sqlite/SQLiteException
    //   81	84	289	android/database/sqlite/SQLiteException
    //   91	96	289	android/database/sqlite/SQLiteException
    //   104	106	289	android/database/sqlite/SQLiteException
    //   113	115	289	android/database/sqlite/SQLiteException
    //   130	134	289	android/database/sqlite/SQLiteException
    //   20	24	340	finally
    //   33	38	340	finally
    //   53	56	340	finally
    //   67	70	340	finally
    //   81	84	340	finally
    //   91	96	340	finally
    //   104	106	340	finally
    //   113	115	340	finally
    //   130	134	340	finally
    //   136	143	355	finally
    //   176	182	355	finally
    //   189	196	355	finally
    //   203	210	355	finally
    //   212	215	355	finally
    //   231	236	355	finally
    //   236	243	355	finally
    //   250	254	355	finally
    //   256	261	355	finally
    //   269	274	355	finally
    //   294	298	363	finally
    //   300	305	363	finally
    //   317	322	363	finally
    //   136	143	371	android/database/sqlite/SQLiteException
    //   176	182	371	android/database/sqlite/SQLiteException
    //   189	196	371	android/database/sqlite/SQLiteException
    //   203	210	371	android/database/sqlite/SQLiteException
    //   212	215	371	android/database/sqlite/SQLiteException
    //   231	236	371	android/database/sqlite/SQLiteException
    //   236	243	371	android/database/sqlite/SQLiteException
    //   250	254	371	android/database/sqlite/SQLiteException
    //   256	261	371	android/database/sqlite/SQLiteException
    //   269	274	371	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  public java.util.List a(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 199	com/google/android/gms/common/internal/p:a	(Ljava/lang/String;)Ljava/lang/String;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual 202	com/google/android/gms/measurement/internal/i:e	()V
    //   9: aload_0
    //   10: invokevirtual 205	com/google/android/gms/measurement/internal/i:y	()V
    //   13: new 238	java/util/ArrayList
    //   16: astore_2
    //   17: aload_2
    //   18: invokespecial 240	java/util/ArrayList:<init>	()V
    //   21: aload_0
    //   22: invokevirtual 168	com/google/android/gms/measurement/internal/i:q	()Landroid/database/sqlite/SQLiteDatabase;
    //   25: astore_3
    //   26: ldc -14
    //   28: astore 4
    //   30: iconst_3
    //   31: istore 5
    //   33: iload 5
    //   35: anewarray 209	java/lang/String
    //   38: astore 6
    //   40: iconst_0
    //   41: istore 7
    //   43: aconst_null
    //   44: astore 8
    //   46: ldc -12
    //   48: astore 9
    //   50: aload 6
    //   52: iconst_0
    //   53: aload 9
    //   55: aastore
    //   56: iconst_1
    //   57: istore 7
    //   59: ldc -10
    //   61: astore 9
    //   63: aload 6
    //   65: iload 7
    //   67: aload 9
    //   69: aastore
    //   70: iconst_2
    //   71: istore 7
    //   73: ldc -8
    //   75: astore 9
    //   77: aload 6
    //   79: iload 7
    //   81: aload 9
    //   83: aastore
    //   84: ldc -6
    //   86: astore 8
    //   88: iconst_1
    //   89: istore 10
    //   91: iload 10
    //   93: anewarray 209	java/lang/String
    //   96: astore 9
    //   98: aload 9
    //   100: iconst_0
    //   101: aload_1
    //   102: aastore
    //   103: aconst_null
    //   104: astore 11
    //   106: ldc -4
    //   108: astore 12
    //   110: aload_0
    //   111: invokevirtual 78	com/google/android/gms/measurement/internal/i:n	()Lcom/google/android/gms/measurement/internal/h;
    //   114: astore 13
    //   116: aload 13
    //   118: invokevirtual 255	com/google/android/gms/measurement/internal/h:t	()I
    //   121: iconst_1
    //   122: iadd
    //   123: istore 14
    //   125: iload 14
    //   127: invokestatic 259	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   130: astore 13
    //   132: aload_3
    //   133: aload 4
    //   135: aload 6
    //   137: aload 8
    //   139: aload 9
    //   141: aconst_null
    //   142: aconst_null
    //   143: aload 12
    //   145: aload 13
    //   147: invokevirtual 262	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   150: astore 12
    //   152: aload 12
    //   154: invokeinterface 177 1 0
    //   159: istore 15
    //   161: iload 15
    //   163: ifne +19 -> 182
    //   166: aload 12
    //   168: ifnull +10 -> 178
    //   171: aload 12
    //   173: invokeinterface 185 1 0
    //   178: aload_2
    //   179: astore_3
    //   180: aload_3
    //   181: areturn
    //   182: iconst_0
    //   183: istore 15
    //   185: aconst_null
    //   186: astore_3
    //   187: aload 12
    //   189: iconst_0
    //   190: invokeinterface 265 2 0
    //   195: astore 8
    //   197: iconst_1
    //   198: istore 15
    //   200: aload 12
    //   202: iload 15
    //   204: invokeinterface 181 2 0
    //   209: lstore 16
    //   211: iconst_2
    //   212: istore 15
    //   214: aload_0
    //   215: aload 12
    //   217: iload 15
    //   219: invokevirtual 268	com/google/android/gms/measurement/internal/i:b	(Landroid/database/Cursor;I)Ljava/lang/Object;
    //   222: astore 11
    //   224: aload 11
    //   226: ifnonnull +124 -> 350
    //   229: aload_0
    //   230: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   233: astore_3
    //   234: aload_3
    //   235: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   238: astore_3
    //   239: ldc_w 270
    //   242: astore 4
    //   244: aload_3
    //   245: aload 4
    //   247: invokevirtual 107	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   250: aload 12
    //   252: invokeinterface 229 1 0
    //   257: istore 15
    //   259: iload 15
    //   261: ifne -79 -> 182
    //   264: aload_2
    //   265: invokeinterface 275 1 0
    //   270: istore 15
    //   272: aload_0
    //   273: invokevirtual 78	com/google/android/gms/measurement/internal/i:n	()Lcom/google/android/gms/measurement/internal/h;
    //   276: astore 4
    //   278: aload 4
    //   280: invokevirtual 255	com/google/android/gms/measurement/internal/h:t	()I
    //   283: istore 18
    //   285: iload 15
    //   287: iload 18
    //   289: if_icmple +44 -> 333
    //   292: aload_0
    //   293: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   296: astore_3
    //   297: aload_3
    //   298: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   301: astore_3
    //   302: ldc_w 277
    //   305: astore 4
    //   307: aload_3
    //   308: aload 4
    //   310: invokevirtual 107	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   313: aload_0
    //   314: invokevirtual 78	com/google/android/gms/measurement/internal/i:n	()Lcom/google/android/gms/measurement/internal/h;
    //   317: astore_3
    //   318: aload_3
    //   319: invokevirtual 255	com/google/android/gms/measurement/internal/h:t	()I
    //   322: istore 15
    //   324: aload_2
    //   325: iload 15
    //   327: invokeinterface 281 2 0
    //   332: pop
    //   333: aload 12
    //   335: ifnull +10 -> 345
    //   338: aload 12
    //   340: invokeinterface 185 1 0
    //   345: aload_2
    //   346: astore_3
    //   347: goto -167 -> 180
    //   350: new 283	com/google/android/gms/measurement/internal/d
    //   353: astore 4
    //   355: aload_1
    //   356: astore 6
    //   358: aload 4
    //   360: aload_1
    //   361: aload 8
    //   363: lload 16
    //   365: aload 11
    //   367: invokespecial 286	com/google/android/gms/measurement/internal/d:<init>	(Ljava/lang/String;Ljava/lang/String;JLjava/lang/Object;)V
    //   370: aload_2
    //   371: aload 4
    //   373: invokeinterface 290 2 0
    //   378: pop
    //   379: goto -129 -> 250
    //   382: astore_3
    //   383: aload 12
    //   385: astore 4
    //   387: aload_0
    //   388: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   391: astore 6
    //   393: aload 6
    //   395: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   398: astore 6
    //   400: ldc_w 292
    //   403: astore 8
    //   405: aload 6
    //   407: aload 8
    //   409: aload_1
    //   410: aload_3
    //   411: invokevirtual 192	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   414: aload 4
    //   416: ifnull +10 -> 426
    //   419: aload 4
    //   421: invokeinterface 185 1 0
    //   426: iconst_0
    //   427: istore 15
    //   429: aconst_null
    //   430: astore_3
    //   431: goto -251 -> 180
    //   434: astore_3
    //   435: aconst_null
    //   436: astore 12
    //   438: aload 12
    //   440: ifnull +10 -> 450
    //   443: aload 12
    //   445: invokeinterface 185 1 0
    //   450: aload_3
    //   451: athrow
    //   452: astore_3
    //   453: goto -15 -> 438
    //   456: astore_3
    //   457: aload 4
    //   459: astore 12
    //   461: goto -23 -> 438
    //   464: astore_3
    //   465: iconst_0
    //   466: istore 18
    //   468: aconst_null
    //   469: astore 4
    //   471: goto -84 -> 387
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	474	0	this	i
    //   0	474	1	paramString	String
    //   16	355	2	localArrayList	java.util.ArrayList
    //   25	322	3	localObject1	Object
    //   382	29	3	localSQLiteException1	SQLiteException
    //   430	1	3	localObject2	Object
    //   434	17	3	localObject3	Object
    //   452	1	3	localObject4	Object
    //   456	1	3	localObject5	Object
    //   464	1	3	localSQLiteException2	SQLiteException
    //   28	442	4	localObject6	Object
    //   31	3	5	i	int
    //   38	368	6	localObject7	Object
    //   41	39	7	j	int
    //   44	364	8	str	String
    //   48	92	9	localObject8	Object
    //   89	3	10	k	int
    //   104	262	11	localObject9	Object
    //   108	352	12	localObject10	Object
    //   114	32	13	localObject11	Object
    //   123	3	14	m	int
    //   159	44	15	n	int
    //   212	6	15	i1	int
    //   257	3	15	bool	boolean
    //   270	158	15	i2	int
    //   209	155	16	l	long
    //   283	184	18	i3	int
    // Exception table:
    //   from	to	target	type
    //   152	159	382	android/database/sqlite/SQLiteException
    //   189	195	382	android/database/sqlite/SQLiteException
    //   202	209	382	android/database/sqlite/SQLiteException
    //   217	222	382	android/database/sqlite/SQLiteException
    //   229	233	382	android/database/sqlite/SQLiteException
    //   234	238	382	android/database/sqlite/SQLiteException
    //   245	250	382	android/database/sqlite/SQLiteException
    //   250	257	382	android/database/sqlite/SQLiteException
    //   264	270	382	android/database/sqlite/SQLiteException
    //   272	276	382	android/database/sqlite/SQLiteException
    //   278	283	382	android/database/sqlite/SQLiteException
    //   292	296	382	android/database/sqlite/SQLiteException
    //   297	301	382	android/database/sqlite/SQLiteException
    //   308	313	382	android/database/sqlite/SQLiteException
    //   313	317	382	android/database/sqlite/SQLiteException
    //   318	322	382	android/database/sqlite/SQLiteException
    //   325	333	382	android/database/sqlite/SQLiteException
    //   350	353	382	android/database/sqlite/SQLiteException
    //   365	370	382	android/database/sqlite/SQLiteException
    //   371	379	382	android/database/sqlite/SQLiteException
    //   21	25	434	finally
    //   33	38	434	finally
    //   53	56	434	finally
    //   67	70	434	finally
    //   81	84	434	finally
    //   91	96	434	finally
    //   101	103	434	finally
    //   110	114	434	finally
    //   116	121	434	finally
    //   125	130	434	finally
    //   145	150	434	finally
    //   152	159	452	finally
    //   189	195	452	finally
    //   202	209	452	finally
    //   217	222	452	finally
    //   229	233	452	finally
    //   234	238	452	finally
    //   245	250	452	finally
    //   250	257	452	finally
    //   264	270	452	finally
    //   272	276	452	finally
    //   278	283	452	finally
    //   292	296	452	finally
    //   297	301	452	finally
    //   308	313	452	finally
    //   313	317	452	finally
    //   318	322	452	finally
    //   325	333	452	finally
    //   350	353	452	finally
    //   365	370	452	finally
    //   371	379	452	finally
    //   387	391	456	finally
    //   393	398	456	finally
    //   410	414	456	finally
    //   21	25	464	android/database/sqlite/SQLiteException
    //   33	38	464	android/database/sqlite/SQLiteException
    //   53	56	464	android/database/sqlite/SQLiteException
    //   67	70	464	android/database/sqlite/SQLiteException
    //   81	84	464	android/database/sqlite/SQLiteException
    //   91	96	464	android/database/sqlite/SQLiteException
    //   101	103	464	android/database/sqlite/SQLiteException
    //   110	114	464	android/database/sqlite/SQLiteException
    //   116	121	464	android/database/sqlite/SQLiteException
    //   125	130	464	android/database/sqlite/SQLiteException
    //   145	150	464	android/database/sqlite/SQLiteException
  }
  
  /* Error */
  public java.util.List a(String paramString, int paramInt1, int paramInt2)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore 4
    //   3: aload_0
    //   4: invokevirtual 202	com/google/android/gms/measurement/internal/i:e	()V
    //   7: aload_0
    //   8: invokevirtual 205	com/google/android/gms/measurement/internal/i:y	()V
    //   11: iload_2
    //   12: ifle +174 -> 186
    //   15: iload 4
    //   17: istore 5
    //   19: iload 5
    //   21: invokestatic 295	com/google/android/gms/common/internal/p:b	(Z)V
    //   24: iload_3
    //   25: ifle +170 -> 195
    //   28: iload 4
    //   30: invokestatic 295	com/google/android/gms/common/internal/p:b	(Z)V
    //   33: aload_1
    //   34: invokestatic 199	com/google/android/gms/common/internal/p:a	(Ljava/lang/String;)Ljava/lang/String;
    //   37: pop
    //   38: aload_0
    //   39: invokevirtual 168	com/google/android/gms/measurement/internal/i:q	()Landroid/database/sqlite/SQLiteDatabase;
    //   42: astore 6
    //   44: ldc_w 297
    //   47: astore 7
    //   49: iconst_2
    //   50: istore 8
    //   52: iload 8
    //   54: anewarray 209	java/lang/String
    //   57: astore 9
    //   59: iconst_0
    //   60: istore 10
    //   62: aconst_null
    //   63: astore 11
    //   65: ldc -4
    //   67: astore 12
    //   69: aload 9
    //   71: iconst_0
    //   72: aload 12
    //   74: aastore
    //   75: iconst_1
    //   76: istore 10
    //   78: ldc_w 299
    //   81: astore 12
    //   83: aload 9
    //   85: iload 10
    //   87: aload 12
    //   89: aastore
    //   90: ldc -6
    //   92: astore 11
    //   94: iconst_1
    //   95: istore 13
    //   97: iload 13
    //   99: anewarray 209	java/lang/String
    //   102: astore 12
    //   104: aconst_null
    //   105: astore 14
    //   107: aload 12
    //   109: iconst_0
    //   110: aload_1
    //   111: aastore
    //   112: aconst_null
    //   113: astore 14
    //   115: iconst_0
    //   116: istore 15
    //   118: aconst_null
    //   119: astore 16
    //   121: ldc -4
    //   123: astore 17
    //   125: iload_2
    //   126: invokestatic 259	java/lang/String:valueOf	(I)Ljava/lang/String;
    //   129: astore 18
    //   131: aload 6
    //   133: aload 7
    //   135: aload 9
    //   137: aload 11
    //   139: aload 12
    //   141: aconst_null
    //   142: aconst_null
    //   143: aload 17
    //   145: aload 18
    //   147: invokevirtual 262	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   150: astore 9
    //   152: aload 9
    //   154: invokeinterface 177 1 0
    //   159: istore 5
    //   161: iload 5
    //   163: ifne +41 -> 204
    //   166: invokestatic 305	java/util/Collections:emptyList	()Ljava/util/List;
    //   169: astore 6
    //   171: aload 9
    //   173: ifnull +10 -> 183
    //   176: aload 9
    //   178: invokeinterface 185 1 0
    //   183: aload 6
    //   185: areturn
    //   186: iconst_0
    //   187: istore 5
    //   189: aconst_null
    //   190: astore 6
    //   192: goto -173 -> 19
    //   195: iconst_0
    //   196: istore 4
    //   198: aconst_null
    //   199: astore 7
    //   201: goto -173 -> 28
    //   204: new 238	java/util/ArrayList
    //   207: astore 6
    //   209: aload 6
    //   211: invokespecial 240	java/util/ArrayList:<init>	()V
    //   214: iconst_0
    //   215: istore 10
    //   217: aconst_null
    //   218: astore 11
    //   220: iconst_0
    //   221: istore 4
    //   223: aconst_null
    //   224: astore 7
    //   226: aload 9
    //   228: iconst_0
    //   229: invokeinterface 181 2 0
    //   234: lstore 19
    //   236: iconst_1
    //   237: istore 4
    //   239: aload 9
    //   241: iload 4
    //   243: invokeinterface 309 2 0
    //   248: astore 7
    //   250: aload_0
    //   251: invokevirtual 313	com/google/android/gms/measurement/internal/i:j	()Lcom/google/android/gms/measurement/internal/f;
    //   254: astore 16
    //   256: aload 16
    //   258: aload 7
    //   260: invokevirtual 318	com/google/android/gms/measurement/internal/f:b	([B)[B
    //   263: astore 7
    //   265: aload 6
    //   267: invokeinterface 321 1 0
    //   272: istore 15
    //   274: iload 15
    //   276: ifne +93 -> 369
    //   279: aload 7
    //   281: arraylength
    //   282: iload 10
    //   284: iadd
    //   285: istore 15
    //   287: iload 15
    //   289: iload_3
    //   290: if_icmple +79 -> 369
    //   293: aload 9
    //   295: ifnull -112 -> 183
    //   298: aload 9
    //   300: invokeinterface 185 1 0
    //   305: goto -122 -> 183
    //   308: astore 7
    //   310: aload_0
    //   311: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   314: astore 12
    //   316: aload 12
    //   318: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   321: astore 12
    //   323: ldc_w 323
    //   326: astore 14
    //   328: aload 12
    //   330: aload 14
    //   332: aload_1
    //   333: aload 7
    //   335: invokevirtual 192	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   338: iload 10
    //   340: istore 4
    //   342: aload 9
    //   344: invokeinterface 229 1 0
    //   349: istore 10
    //   351: iload 10
    //   353: ifeq -60 -> 293
    //   356: iload 4
    //   358: iload_3
    //   359: if_icmpgt -66 -> 293
    //   362: iload 4
    //   364: istore 10
    //   366: goto -146 -> 220
    //   369: aload 7
    //   371: invokestatic 328	com/google/android/gms/internal/n:a	([B)Lcom/google/android/gms/internal/n;
    //   374: astore 16
    //   376: new 330	com/google/android/gms/internal/j$d
    //   379: astore 17
    //   381: aload 17
    //   383: invokespecial 331	com/google/android/gms/internal/j$d:<init>	()V
    //   386: aload 17
    //   388: aload 16
    //   390: invokevirtual 334	com/google/android/gms/internal/j$d:a	(Lcom/google/android/gms/internal/n;)Lcom/google/android/gms/internal/j$d;
    //   393: pop
    //   394: aload 7
    //   396: arraylength
    //   397: iload 10
    //   399: iadd
    //   400: istore 4
    //   402: lload 19
    //   404: invokestatic 339	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   407: astore 11
    //   409: aload 17
    //   411: aload 11
    //   413: invokestatic 345	android/util/Pair:create	(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    //   416: astore 11
    //   418: aload 6
    //   420: aload 11
    //   422: invokeinterface 290 2 0
    //   427: pop
    //   428: goto -86 -> 342
    //   431: astore 6
    //   433: aload 9
    //   435: astore 7
    //   437: aload_0
    //   438: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   441: astore 9
    //   443: aload 9
    //   445: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   448: astore 9
    //   450: ldc_w 347
    //   453: astore 11
    //   455: aload 9
    //   457: aload 11
    //   459: aload_1
    //   460: aload 6
    //   462: invokevirtual 192	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   465: invokestatic 305	java/util/Collections:emptyList	()Ljava/util/List;
    //   468: astore 6
    //   470: aload 7
    //   472: ifnull -289 -> 183
    //   475: aload 7
    //   477: invokeinterface 185 1 0
    //   482: goto -299 -> 183
    //   485: astore 7
    //   487: aload_0
    //   488: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   491: astore 12
    //   493: aload 12
    //   495: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   498: astore 12
    //   500: ldc_w 349
    //   503: astore 14
    //   505: aload 12
    //   507: aload 14
    //   509: aload_1
    //   510: aload 7
    //   512: invokevirtual 192	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   515: iload 10
    //   517: istore 4
    //   519: goto -177 -> 342
    //   522: astore 6
    //   524: iconst_0
    //   525: istore 8
    //   527: aconst_null
    //   528: astore 9
    //   530: aload 9
    //   532: ifnull +10 -> 542
    //   535: aload 9
    //   537: invokeinterface 185 1 0
    //   542: aload 6
    //   544: athrow
    //   545: astore 6
    //   547: goto -17 -> 530
    //   550: astore 6
    //   552: aload 7
    //   554: astore 9
    //   556: goto -26 -> 530
    //   559: astore 6
    //   561: iconst_0
    //   562: istore 4
    //   564: aconst_null
    //   565: astore 7
    //   567: goto -130 -> 437
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	570	0	this	i
    //   0	570	1	paramString	String
    //   0	570	2	paramInt1	int
    //   0	570	3	paramInt2	int
    //   1	562	4	i	int
    //   17	3	5	j	int
    //   159	29	5	bool1	boolean
    //   42	377	6	localObject1	Object
    //   431	30	6	localSQLiteException1	SQLiteException
    //   468	1	6	localList	java.util.List
    //   522	21	6	localObject2	Object
    //   545	1	6	localObject3	Object
    //   550	1	6	localObject4	Object
    //   559	1	6	localSQLiteException2	SQLiteException
    //   47	233	7	localObject5	Object
    //   308	87	7	localIOException1	java.io.IOException
    //   435	41	7	localObject6	Object
    //   485	68	7	localIOException2	java.io.IOException
    //   565	1	7	localObject7	Object
    //   50	476	8	k	int
    //   57	498	9	localObject8	Object
    //   60	279	10	m	int
    //   349	3	10	bool2	boolean
    //   364	152	10	n	int
    //   63	395	11	localObject9	Object
    //   67	439	12	localObject10	Object
    //   95	3	13	i1	int
    //   105	403	14	str1	String
    //   116	159	15	bool3	boolean
    //   285	6	15	i2	int
    //   119	270	16	localObject11	Object
    //   123	287	17	localObject12	Object
    //   129	17	18	str2	String
    //   234	169	19	l	long
    // Exception table:
    //   from	to	target	type
    //   241	248	308	java/io/IOException
    //   250	254	308	java/io/IOException
    //   258	263	308	java/io/IOException
    //   152	159	431	android/database/sqlite/SQLiteException
    //   166	169	431	android/database/sqlite/SQLiteException
    //   204	207	431	android/database/sqlite/SQLiteException
    //   209	214	431	android/database/sqlite/SQLiteException
    //   228	234	431	android/database/sqlite/SQLiteException
    //   241	248	431	android/database/sqlite/SQLiteException
    //   250	254	431	android/database/sqlite/SQLiteException
    //   258	263	431	android/database/sqlite/SQLiteException
    //   265	272	431	android/database/sqlite/SQLiteException
    //   279	282	431	android/database/sqlite/SQLiteException
    //   310	314	431	android/database/sqlite/SQLiteException
    //   316	321	431	android/database/sqlite/SQLiteException
    //   333	338	431	android/database/sqlite/SQLiteException
    //   342	349	431	android/database/sqlite/SQLiteException
    //   369	374	431	android/database/sqlite/SQLiteException
    //   376	379	431	android/database/sqlite/SQLiteException
    //   381	386	431	android/database/sqlite/SQLiteException
    //   388	394	431	android/database/sqlite/SQLiteException
    //   394	397	431	android/database/sqlite/SQLiteException
    //   402	407	431	android/database/sqlite/SQLiteException
    //   411	416	431	android/database/sqlite/SQLiteException
    //   420	428	431	android/database/sqlite/SQLiteException
    //   487	491	431	android/database/sqlite/SQLiteException
    //   493	498	431	android/database/sqlite/SQLiteException
    //   510	515	431	android/database/sqlite/SQLiteException
    //   388	394	485	java/io/IOException
    //   38	42	522	finally
    //   52	57	522	finally
    //   72	75	522	finally
    //   87	90	522	finally
    //   97	102	522	finally
    //   110	112	522	finally
    //   125	129	522	finally
    //   145	150	522	finally
    //   152	159	545	finally
    //   166	169	545	finally
    //   204	207	545	finally
    //   209	214	545	finally
    //   228	234	545	finally
    //   241	248	545	finally
    //   250	254	545	finally
    //   258	263	545	finally
    //   265	272	545	finally
    //   279	282	545	finally
    //   310	314	545	finally
    //   316	321	545	finally
    //   333	338	545	finally
    //   342	349	545	finally
    //   369	374	545	finally
    //   376	379	545	finally
    //   381	386	545	finally
    //   388	394	545	finally
    //   394	397	545	finally
    //   402	407	545	finally
    //   411	416	545	finally
    //   420	428	545	finally
    //   487	491	545	finally
    //   493	498	545	finally
    //   510	515	545	finally
    //   437	441	550	finally
    //   443	448	550	finally
    //   460	465	550	finally
    //   465	468	550	finally
    //   38	42	559	android/database/sqlite/SQLiteException
    //   52	57	559	android/database/sqlite/SQLiteException
    //   72	75	559	android/database/sqlite/SQLiteException
    //   87	90	559	android/database/sqlite/SQLiteException
    //   97	102	559	android/database/sqlite/SQLiteException
    //   110	112	559	android/database/sqlite/SQLiteException
    //   125	129	559	android/database/sqlite/SQLiteException
    //   145	150	559	android/database/sqlite/SQLiteException
  }
  
  protected void a() {}
  
  public void a(long paramLong)
  {
    int i = 1;
    e();
    y();
    Object localObject1 = q();
    Object localObject2 = new String[i];
    String str1 = String.valueOf(paramLong);
    localObject2[0] = str1;
    String str2 = "queue";
    str1 = "rowid=?";
    int j = ((SQLiteDatabase)localObject1).delete(str2, str1, (String[])localObject2);
    if (j != i)
    {
      localObject1 = l().b();
      localObject2 = "Deleted fewer rows from queue than expected";
      ((t.a)localObject1).a((String)localObject2);
    }
  }
  
  void a(ContentValues paramContentValues, String paramString, Object paramObject)
  {
    p.a(paramString);
    p.a(paramObject);
    boolean bool = paramObject instanceof String;
    if (bool)
    {
      paramObject = (String)paramObject;
      paramContentValues.put(paramString, (String)paramObject);
    }
    for (;;)
    {
      return;
      bool = paramObject instanceof Long;
      if (bool)
      {
        paramObject = (Long)paramObject;
        paramContentValues.put(paramString, (Long)paramObject);
      }
      else
      {
        bool = paramObject instanceof Float;
        if (!bool) {
          break;
        }
        paramObject = (Float)paramObject;
        paramContentValues.put(paramString, (Float)paramObject);
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localIllegalArgumentException.<init>("Invalid value type");
    throw localIllegalArgumentException;
  }
  
  /* Error */
  public void a(com.google.android.gms.internal.j.d paramd)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 202	com/google/android/gms/measurement/internal/i:e	()V
    //   4: aload_0
    //   5: invokevirtual 205	com/google/android/gms/measurement/internal/i:y	()V
    //   8: aload_1
    //   9: invokestatic 365	com/google/android/gms/common/internal/p:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   12: pop
    //   13: aload_1
    //   14: getfield 388	com/google/android/gms/internal/j$d:o	Ljava/lang/String;
    //   17: invokestatic 199	com/google/android/gms/common/internal/p:a	(Ljava/lang/String;)Ljava/lang/String;
    //   20: pop
    //   21: aload_1
    //   22: getfield 392	com/google/android/gms/internal/j$d:f	Ljava/lang/Long;
    //   25: invokestatic 365	com/google/android/gms/common/internal/p:a	(Ljava/lang/Object;)Ljava/lang/Object;
    //   28: pop
    //   29: aload_0
    //   30: invokevirtual 395	com/google/android/gms/measurement/internal/i:s	()V
    //   33: aload_0
    //   34: invokevirtual 54	com/google/android/gms/measurement/internal/i:h	()Lcom/google/android/gms/internal/e;
    //   37: astore_2
    //   38: aload_2
    //   39: invokeinterface 400 1 0
    //   44: lstore_3
    //   45: aload_1
    //   46: getfield 392	com/google/android/gms/internal/j$d:f	Ljava/lang/Long;
    //   49: astore 5
    //   51: aload 5
    //   53: invokevirtual 403	java/lang/Long:longValue	()J
    //   56: lstore 6
    //   58: aload_0
    //   59: invokevirtual 78	com/google/android/gms/measurement/internal/i:n	()Lcom/google/android/gms/measurement/internal/h;
    //   62: astore 8
    //   64: aload 8
    //   66: invokevirtual 406	com/google/android/gms/measurement/internal/h:E	()J
    //   69: lstore 9
    //   71: lload_3
    //   72: lload 9
    //   74: lsub
    //   75: lstore 9
    //   77: lload 6
    //   79: lload 9
    //   81: lcmp
    //   82: istore 11
    //   84: iload 11
    //   86: iflt +43 -> 129
    //   89: aload_1
    //   90: getfield 392	com/google/android/gms/internal/j$d:f	Ljava/lang/Long;
    //   93: astore 5
    //   95: aload 5
    //   97: invokevirtual 403	java/lang/Long:longValue	()J
    //   100: lstore 6
    //   102: aload_0
    //   103: invokevirtual 78	com/google/android/gms/measurement/internal/i:n	()Lcom/google/android/gms/measurement/internal/h;
    //   106: astore 8
    //   108: aload 8
    //   110: invokevirtual 406	com/google/android/gms/measurement/internal/h:E	()J
    //   113: lload_3
    //   114: ladd
    //   115: lstore 9
    //   117: lload 6
    //   119: lload 9
    //   121: lcmp
    //   122: istore 11
    //   124: iload 11
    //   126: ifle +38 -> 164
    //   129: aload_0
    //   130: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   133: invokevirtual 408	com/google/android/gms/measurement/internal/t:o	()Lcom/google/android/gms/measurement/internal/t$a;
    //   136: astore 5
    //   138: ldc_w 410
    //   141: astore 12
    //   143: lload_3
    //   144: invokestatic 339	java/lang/Long:valueOf	(J)Ljava/lang/Long;
    //   147: astore_2
    //   148: aload_1
    //   149: getfield 392	com/google/android/gms/internal/j$d:f	Ljava/lang/Long;
    //   152: astore 13
    //   154: aload 5
    //   156: aload 12
    //   158: aload_2
    //   159: aload 13
    //   161: invokevirtual 192	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   164: aload_1
    //   165: invokevirtual 412	com/google/android/gms/internal/j$d:e	()I
    //   168: istore 14
    //   170: iload 14
    //   172: newarray <illegal type>
    //   174: astore_2
    //   175: aload_2
    //   176: invokestatic 417	com/google/android/gms/internal/zztd:a	([B)Lcom/google/android/gms/internal/zztd;
    //   179: astore 13
    //   181: aload_1
    //   182: aload 13
    //   184: invokevirtual 420	com/google/android/gms/internal/j$d:a	(Lcom/google/android/gms/internal/zztd;)V
    //   187: aload 13
    //   189: invokevirtual 422	com/google/android/gms/internal/zztd:b	()V
    //   192: aload_0
    //   193: invokevirtual 313	com/google/android/gms/measurement/internal/i:j	()Lcom/google/android/gms/measurement/internal/f;
    //   196: astore 13
    //   198: aload 13
    //   200: aload_2
    //   201: invokevirtual 424	com/google/android/gms/measurement/internal/f:a	([B)[B
    //   204: astore_2
    //   205: aload_0
    //   206: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   209: invokevirtual 426	com/google/android/gms/measurement/internal/t:t	()Lcom/google/android/gms/measurement/internal/t$a;
    //   212: astore 13
    //   214: aload_2
    //   215: arraylength
    //   216: istore 15
    //   218: iload 15
    //   220: invokestatic 433	java/lang/Integer:valueOf	(I)Ljava/lang/Integer;
    //   223: astore 12
    //   225: aload 13
    //   227: ldc_w 428
    //   230: aload 12
    //   232: invokevirtual 436	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   235: new 367	android/content/ContentValues
    //   238: astore 13
    //   240: aload 13
    //   242: invokespecial 437	android/content/ContentValues:<init>	()V
    //   245: aload_1
    //   246: getfield 388	com/google/android/gms/internal/j$d:o	Ljava/lang/String;
    //   249: astore 12
    //   251: aload 13
    //   253: ldc_w 439
    //   256: aload 12
    //   258: invokevirtual 370	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/String;)V
    //   261: aload_1
    //   262: getfield 392	com/google/android/gms/internal/j$d:f	Ljava/lang/Long;
    //   265: astore 12
    //   267: aload 13
    //   269: ldc_w 441
    //   272: aload 12
    //   274: invokevirtual 373	android/content/ContentValues:put	(Ljava/lang/String;Ljava/lang/Long;)V
    //   277: ldc_w 299
    //   280: astore 5
    //   282: aload 13
    //   284: aload 5
    //   286: aload_2
    //   287: invokevirtual 444	android/content/ContentValues:put	(Ljava/lang/String;[B)V
    //   290: aload_0
    //   291: invokevirtual 168	com/google/android/gms/measurement/internal/i:q	()Landroid/database/sqlite/SQLiteDatabase;
    //   294: astore_2
    //   295: ldc_w 297
    //   298: astore 5
    //   300: iconst_0
    //   301: istore 15
    //   303: aconst_null
    //   304: astore 12
    //   306: aload_2
    //   307: aload 5
    //   309: aconst_null
    //   310: aload 13
    //   312: invokevirtual 448	android/database/sqlite/SQLiteDatabase:insert	(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    //   315: lstore_3
    //   316: iconst_m1
    //   317: i2l
    //   318: lstore 6
    //   320: lload_3
    //   321: lload 6
    //   323: lcmp
    //   324: istore 14
    //   326: iload 14
    //   328: ifne +24 -> 352
    //   331: aload_0
    //   332: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   335: astore_2
    //   336: aload_2
    //   337: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   340: astore_2
    //   341: ldc_w 450
    //   344: astore 13
    //   346: aload_2
    //   347: aload 13
    //   349: invokevirtual 107	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   352: return
    //   353: astore_2
    //   354: aload_0
    //   355: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   358: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   361: astore 13
    //   363: ldc_w 452
    //   366: astore 5
    //   368: aload 13
    //   370: aload 5
    //   372: aload_2
    //   373: invokevirtual 436	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   376: goto -24 -> 352
    //   379: astore_2
    //   380: aload_0
    //   381: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   384: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   387: astore 13
    //   389: ldc_w 454
    //   392: astore 5
    //   394: aload 13
    //   396: aload 5
    //   398: aload_2
    //   399: invokevirtual 436	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   402: goto -50 -> 352
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	405	0	this	i
    //   0	405	1	paramd	com.google.android.gms.internal.j.d
    //   37	310	2	localObject1	Object
    //   353	20	2	localIOException	java.io.IOException
    //   379	20	2	localSQLiteException	SQLiteException
    //   44	277	3	l1	long
    //   49	348	5	localObject2	Object
    //   56	266	6	l2	long
    //   62	47	8	localh	h
    //   69	51	9	l3	long
    //   82	43	11	bool1	boolean
    //   141	164	12	localObject3	Object
    //   152	243	13	localObject4	Object
    //   168	3	14	i	int
    //   324	3	14	bool2	boolean
    //   216	86	15	j	int
    // Exception table:
    //   from	to	target	type
    //   164	168	353	java/io/IOException
    //   170	174	353	java/io/IOException
    //   175	179	353	java/io/IOException
    //   182	187	353	java/io/IOException
    //   187	192	353	java/io/IOException
    //   192	196	353	java/io/IOException
    //   200	204	353	java/io/IOException
    //   290	294	379	android/database/sqlite/SQLiteException
    //   310	315	379	android/database/sqlite/SQLiteException
    //   331	335	379	android/database/sqlite/SQLiteException
    //   336	340	379	android/database/sqlite/SQLiteException
    //   347	352	379	android/database/sqlite/SQLiteException
  }
  
  public void a(a parama)
  {
    p.a(parama);
    e();
    y();
    Object localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    Object localObject2 = a;
    ((ContentValues)localObject1).put("app_id", (String)localObject2);
    localObject2 = b;
    ((ContentValues)localObject1).put("app_instance_id", (String)localObject2);
    localObject2 = c;
    ((ContentValues)localObject1).put("gmp_app_id", (String)localObject2);
    localObject2 = d;
    ((ContentValues)localObject1).put("resettable_device_id_hash", (String)localObject2);
    localObject2 = Long.valueOf(e);
    ((ContentValues)localObject1).put("last_bundle_index", (Long)localObject2);
    localObject2 = Long.valueOf(f);
    ((ContentValues)localObject1).put("last_bundle_end_timestamp", (Long)localObject2);
    localObject2 = g;
    ((ContentValues)localObject1).put("app_version", (String)localObject2);
    localObject2 = h;
    ((ContentValues)localObject1).put("app_store", (String)localObject2);
    localObject2 = Long.valueOf(i);
    ((ContentValues)localObject1).put("gmp_version", (Long)localObject2);
    long l1 = j;
    localObject2 = Long.valueOf(l1);
    ((ContentValues)localObject1).put("dev_cert_hash", (Long)localObject2);
    Object localObject3 = "measurement_enabled";
    boolean bool1 = k;
    localObject2 = Boolean.valueOf(bool1);
    ((ContentValues)localObject1).put((String)localObject3, (Boolean)localObject2);
    try
    {
      localObject3 = q();
      localObject2 = "apps";
      int i = 5;
      long l2 = ((SQLiteDatabase)localObject3).insertWithOnConflict((String)localObject2, null, (ContentValues)localObject1, i);
      l1 = -1;
      boolean bool2 = l2 < l1;
      if (!bool2)
      {
        localObject1 = l();
        localObject1 = ((t)localObject1).b();
        localObject3 = "Failed to insert/update app (got -1)";
        ((t.a)localObject1).a((String)localObject3);
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        localObject3 = l().b();
        localObject2 = "Error storing app";
        ((t.a)localObject3).a((String)localObject2, localSQLiteException);
      }
    }
  }
  
  public void a(d paramd)
  {
    p.a(paramd);
    e();
    y();
    Object localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    Object localObject2 = a;
    ((ContentValues)localObject1).put("app_id", (String)localObject2);
    localObject2 = b;
    ((ContentValues)localObject1).put("name", (String)localObject2);
    long l1 = c;
    localObject2 = Long.valueOf(l1);
    ((ContentValues)localObject1).put("set_timestamp", (Long)localObject2);
    Object localObject3 = "value";
    localObject2 = d;
    a((ContentValues)localObject1, (String)localObject3, localObject2);
    try
    {
      localObject3 = q();
      localObject2 = "user_attributes";
      int i = 5;
      long l2 = ((SQLiteDatabase)localObject3).insertWithOnConflict((String)localObject2, null, (ContentValues)localObject1, i);
      l1 = -1;
      boolean bool = l2 < l1;
      if (!bool)
      {
        localObject1 = l();
        localObject1 = ((t)localObject1).b();
        localObject3 = "Failed to insert/update user attribute (got -1)";
        ((t.a)localObject1).a((String)localObject3);
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        localObject3 = l().b();
        localObject2 = "Error storing user attribute";
        ((t.a)localObject3).a((String)localObject2, localSQLiteException);
      }
    }
  }
  
  public void a(m paramm)
  {
    p.a(paramm);
    e();
    y();
    Object localObject1 = new android/content/ContentValues;
    ((ContentValues)localObject1).<init>();
    Object localObject2 = a;
    ((ContentValues)localObject1).put("app_id", (String)localObject2);
    localObject2 = b;
    ((ContentValues)localObject1).put("name", (String)localObject2);
    localObject2 = Long.valueOf(c);
    ((ContentValues)localObject1).put("lifetime_count", (Long)localObject2);
    localObject2 = Long.valueOf(d);
    ((ContentValues)localObject1).put("current_bundle_count", (Long)localObject2);
    Object localObject3 = "last_fire_timestamp";
    long l1 = e;
    localObject2 = Long.valueOf(l1);
    ((ContentValues)localObject1).put((String)localObject3, (Long)localObject2);
    try
    {
      localObject3 = q();
      localObject2 = "events";
      int i = 5;
      long l2 = ((SQLiteDatabase)localObject3).insertWithOnConflict((String)localObject2, null, (ContentValues)localObject1, i);
      l1 = -1;
      boolean bool = l2 < l1;
      if (!bool)
      {
        localObject1 = l();
        localObject1 = ((t)localObject1).b();
        localObject3 = "Failed to insert/update event aggregates (got -1)";
        ((t.a)localObject1).a((String)localObject3);
      }
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        localObject3 = l().b();
        localObject2 = "Error storing event aggregates";
        ((t.a)localObject3).a((String)localObject2, localSQLiteException);
      }
    }
  }
  
  /* Error */
  public a b(String paramString)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokestatic 199	com/google/android/gms/common/internal/p:a	(Ljava/lang/String;)Ljava/lang/String;
    //   4: pop
    //   5: aload_0
    //   6: invokevirtual 202	com/google/android/gms/measurement/internal/i:e	()V
    //   9: aload_0
    //   10: invokevirtual 205	com/google/android/gms/measurement/internal/i:y	()V
    //   13: aconst_null
    //   14: astore_2
    //   15: aload_0
    //   16: invokevirtual 168	com/google/android/gms/measurement/internal/i:q	()Landroid/database/sqlite/SQLiteDatabase;
    //   19: astore_3
    //   20: ldc_w 503
    //   23: astore 4
    //   25: bipush 10
    //   27: istore 5
    //   29: iload 5
    //   31: anewarray 209	java/lang/String
    //   34: astore 6
    //   36: iconst_0
    //   37: istore 7
    //   39: aconst_null
    //   40: astore 8
    //   42: ldc_w 460
    //   45: astore 9
    //   47: aload 6
    //   49: iconst_0
    //   50: aload 9
    //   52: aastore
    //   53: iconst_1
    //   54: istore 7
    //   56: ldc_w 464
    //   59: astore 9
    //   61: aload 6
    //   63: iload 7
    //   65: aload 9
    //   67: aastore
    //   68: iconst_2
    //   69: istore 7
    //   71: ldc_w 468
    //   74: astore 9
    //   76: aload 6
    //   78: iload 7
    //   80: aload 9
    //   82: aastore
    //   83: iconst_3
    //   84: istore 7
    //   86: ldc_w 473
    //   89: astore 9
    //   91: aload 6
    //   93: iload 7
    //   95: aload 9
    //   97: aastore
    //   98: iconst_4
    //   99: istore 7
    //   101: ldc_w 478
    //   104: astore 9
    //   106: aload 6
    //   108: iload 7
    //   110: aload 9
    //   112: aastore
    //   113: iconst_5
    //   114: istore 7
    //   116: ldc 21
    //   118: astore 9
    //   120: aload 6
    //   122: iload 7
    //   124: aload 9
    //   126: aastore
    //   127: bipush 6
    //   129: istore 7
    //   131: ldc 31
    //   133: astore 9
    //   135: aload 6
    //   137: iload 7
    //   139: aload 9
    //   141: aastore
    //   142: bipush 7
    //   144: istore 7
    //   146: ldc 35
    //   148: astore 9
    //   150: aload 6
    //   152: iload 7
    //   154: aload 9
    //   156: aastore
    //   157: bipush 8
    //   159: istore 7
    //   161: ldc 39
    //   163: astore 9
    //   165: aload 6
    //   167: iload 7
    //   169: aload 9
    //   171: aastore
    //   172: bipush 9
    //   174: istore 7
    //   176: ldc 43
    //   178: astore 9
    //   180: aload 6
    //   182: iload 7
    //   184: aload 9
    //   186: aastore
    //   187: ldc -6
    //   189: astore 8
    //   191: iconst_1
    //   192: istore 10
    //   194: iload 10
    //   196: anewarray 209	java/lang/String
    //   199: astore 9
    //   201: aconst_null
    //   202: astore 11
    //   204: aload 9
    //   206: iconst_0
    //   207: aload_1
    //   208: aastore
    //   209: aconst_null
    //   210: astore 11
    //   212: aload_3
    //   213: aload 4
    //   215: aload 6
    //   217: aload 8
    //   219: aload 9
    //   221: aconst_null
    //   222: aconst_null
    //   223: aconst_null
    //   224: invokevirtual 221	android/database/sqlite/SQLiteDatabase:query	(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   227: astore 12
    //   229: aload 12
    //   231: invokeinterface 177 1 0
    //   236: istore 13
    //   238: iload 13
    //   240: ifne +21 -> 261
    //   243: aconst_null
    //   244: astore 4
    //   246: aload 12
    //   248: ifnull +10 -> 258
    //   251: aload 12
    //   253: invokeinterface 185 1 0
    //   258: aload 4
    //   260: areturn
    //   261: iconst_0
    //   262: istore 13
    //   264: aconst_null
    //   265: astore_3
    //   266: aload 12
    //   268: iconst_0
    //   269: invokeinterface 265 2 0
    //   274: astore 8
    //   276: iconst_1
    //   277: istore 13
    //   279: aload 12
    //   281: iload 13
    //   283: invokeinterface 265 2 0
    //   288: astore 9
    //   290: iconst_2
    //   291: istore 13
    //   293: aload 12
    //   295: iload 13
    //   297: invokeinterface 265 2 0
    //   302: astore 11
    //   304: iconst_3
    //   305: istore 13
    //   307: aload 12
    //   309: iload 13
    //   311: invokeinterface 181 2 0
    //   316: lstore 14
    //   318: iconst_4
    //   319: istore 13
    //   321: aload 12
    //   323: iload 13
    //   325: invokeinterface 181 2 0
    //   330: lstore 16
    //   332: iconst_5
    //   333: istore 13
    //   335: aload 12
    //   337: iload 13
    //   339: invokeinterface 265 2 0
    //   344: astore 18
    //   346: bipush 6
    //   348: istore 13
    //   350: aload 12
    //   352: iload 13
    //   354: invokeinterface 265 2 0
    //   359: astore 19
    //   361: bipush 7
    //   363: istore 13
    //   365: aload 12
    //   367: iload 13
    //   369: invokeinterface 181 2 0
    //   374: lstore 20
    //   376: bipush 8
    //   378: istore 13
    //   380: aload 12
    //   382: iload 13
    //   384: invokeinterface 181 2 0
    //   389: lstore 22
    //   391: bipush 9
    //   393: istore 13
    //   395: aload 12
    //   397: iload 13
    //   399: invokeinterface 543 2 0
    //   404: istore 13
    //   406: iload 13
    //   408: ifeq +98 -> 506
    //   411: iconst_1
    //   412: istore 13
    //   414: new 456	com/google/android/gms/measurement/internal/a
    //   417: astore 4
    //   419: iload 13
    //   421: ifeq +103 -> 524
    //   424: iconst_1
    //   425: istore 24
    //   427: aload_1
    //   428: astore 6
    //   430: aload 4
    //   432: aload_1
    //   433: aload 8
    //   435: aload 9
    //   437: aload 11
    //   439: lload 14
    //   441: lload 16
    //   443: aload 18
    //   445: aload 19
    //   447: lload 20
    //   449: lload 22
    //   451: iload 24
    //   453: invokespecial 546	com/google/android/gms/measurement/internal/a:<init>	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Ljava/lang/String;JJZ)V
    //   456: aload 12
    //   458: invokeinterface 229 1 0
    //   463: istore 13
    //   465: iload 13
    //   467: ifeq +24 -> 491
    //   470: aload_0
    //   471: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   474: astore_3
    //   475: aload_3
    //   476: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   479: astore_3
    //   480: ldc_w 548
    //   483: astore 6
    //   485: aload_3
    //   486: aload 6
    //   488: invokevirtual 107	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   491: aload 12
    //   493: ifnull -235 -> 258
    //   496: aload 12
    //   498: invokeinterface 185 1 0
    //   503: goto -245 -> 258
    //   506: bipush 9
    //   508: istore 13
    //   510: aload 12
    //   512: iload 13
    //   514: invokeinterface 551 2 0
    //   519: istore 13
    //   521: goto -107 -> 414
    //   524: iconst_0
    //   525: istore 24
    //   527: goto -100 -> 427
    //   530: astore_3
    //   531: iconst_0
    //   532: istore 5
    //   534: aconst_null
    //   535: astore 6
    //   537: aload_0
    //   538: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   541: astore 4
    //   543: aload 4
    //   545: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   548: astore 4
    //   550: ldc_w 553
    //   553: astore 8
    //   555: aload 4
    //   557: aload 8
    //   559: aload_1
    //   560: aload_3
    //   561: invokevirtual 192	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    //   564: aconst_null
    //   565: astore 4
    //   567: aload 6
    //   569: ifnull -311 -> 258
    //   572: aload 6
    //   574: invokeinterface 185 1 0
    //   579: goto -321 -> 258
    //   582: astore_3
    //   583: aload_2
    //   584: ifnull +9 -> 593
    //   587: aload_2
    //   588: invokeinterface 185 1 0
    //   593: aload_3
    //   594: athrow
    //   595: astore_3
    //   596: aload 12
    //   598: astore_2
    //   599: goto -16 -> 583
    //   602: astore_3
    //   603: aload 6
    //   605: astore_2
    //   606: goto -23 -> 583
    //   609: astore_3
    //   610: aload 12
    //   612: astore 6
    //   614: goto -77 -> 537
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	617	0	this	i
    //   0	617	1	paramString	String
    //   14	592	2	localObject1	Object
    //   19	467	3	localObject2	Object
    //   530	31	3	localSQLiteException1	SQLiteException
    //   582	12	3	localObject3	Object
    //   595	1	3	localObject4	Object
    //   602	1	3	localObject5	Object
    //   609	1	3	localSQLiteException2	SQLiteException
    //   23	543	4	localObject6	Object
    //   27	506	5	i	int
    //   34	579	6	localObject7	Object
    //   37	146	7	j	int
    //   40	518	8	str1	String
    //   45	391	9	localObject8	Object
    //   192	3	10	k	int
    //   202	236	11	str2	String
    //   227	384	12	localCursor	Cursor
    //   236	46	13	m	int
    //   291	107	13	n	int
    //   404	62	13	bool1	boolean
    //   508	12	13	i1	int
    //   316	124	14	l1	long
    //   330	112	16	l2	long
    //   344	100	18	str3	String
    //   359	87	19	str4	String
    //   374	74	20	l3	long
    //   389	61	22	l4	long
    //   425	101	24	bool2	boolean
    // Exception table:
    //   from	to	target	type
    //   15	19	530	android/database/sqlite/SQLiteException
    //   29	34	530	android/database/sqlite/SQLiteException
    //   50	53	530	android/database/sqlite/SQLiteException
    //   65	68	530	android/database/sqlite/SQLiteException
    //   80	83	530	android/database/sqlite/SQLiteException
    //   95	98	530	android/database/sqlite/SQLiteException
    //   110	113	530	android/database/sqlite/SQLiteException
    //   124	127	530	android/database/sqlite/SQLiteException
    //   139	142	530	android/database/sqlite/SQLiteException
    //   154	157	530	android/database/sqlite/SQLiteException
    //   169	172	530	android/database/sqlite/SQLiteException
    //   184	187	530	android/database/sqlite/SQLiteException
    //   194	199	530	android/database/sqlite/SQLiteException
    //   207	209	530	android/database/sqlite/SQLiteException
    //   223	227	530	android/database/sqlite/SQLiteException
    //   15	19	582	finally
    //   29	34	582	finally
    //   50	53	582	finally
    //   65	68	582	finally
    //   80	83	582	finally
    //   95	98	582	finally
    //   110	113	582	finally
    //   124	127	582	finally
    //   139	142	582	finally
    //   154	157	582	finally
    //   169	172	582	finally
    //   184	187	582	finally
    //   194	199	582	finally
    //   207	209	582	finally
    //   223	227	582	finally
    //   229	236	595	finally
    //   268	274	595	finally
    //   281	288	595	finally
    //   295	302	595	finally
    //   309	316	595	finally
    //   323	330	595	finally
    //   337	344	595	finally
    //   352	359	595	finally
    //   367	374	595	finally
    //   382	389	595	finally
    //   397	404	595	finally
    //   414	417	595	finally
    //   451	456	595	finally
    //   456	463	595	finally
    //   470	474	595	finally
    //   475	479	595	finally
    //   486	491	595	finally
    //   512	519	595	finally
    //   537	541	602	finally
    //   543	548	602	finally
    //   560	564	602	finally
    //   229	236	609	android/database/sqlite/SQLiteException
    //   268	274	609	android/database/sqlite/SQLiteException
    //   281	288	609	android/database/sqlite/SQLiteException
    //   295	302	609	android/database/sqlite/SQLiteException
    //   309	316	609	android/database/sqlite/SQLiteException
    //   323	330	609	android/database/sqlite/SQLiteException
    //   337	344	609	android/database/sqlite/SQLiteException
    //   352	359	609	android/database/sqlite/SQLiteException
    //   367	374	609	android/database/sqlite/SQLiteException
    //   382	389	609	android/database/sqlite/SQLiteException
    //   397	404	609	android/database/sqlite/SQLiteException
    //   414	417	609	android/database/sqlite/SQLiteException
    //   451	456	609	android/database/sqlite/SQLiteException
    //   456	463	609	android/database/sqlite/SQLiteException
    //   470	474	609	android/database/sqlite/SQLiteException
    //   475	479	609	android/database/sqlite/SQLiteException
    //   486	491	609	android/database/sqlite/SQLiteException
    //   512	519	609	android/database/sqlite/SQLiteException
  }
  
  Object b(Cursor paramCursor, int paramInt)
  {
    float f = 0.0F;
    Object localObject1 = null;
    int i = a(paramCursor, paramInt);
    Object localObject2;
    Object localObject3;
    switch (i)
    {
    default: 
      localObject2 = l().b();
      String str = "Loaded invalid unknown value type, ignoring it";
      localObject3 = Integer.valueOf(i);
      ((t.a)localObject2).a(str, localObject3);
    }
    for (;;)
    {
      return localObject1;
      localObject3 = l().b();
      localObject2 = "Loaded invalid null value from database";
      ((t.a)localObject3).a((String)localObject2);
      continue;
      long l = paramCursor.getLong(paramInt);
      localObject1 = Long.valueOf(l);
      continue;
      f = paramCursor.getFloat(paramInt);
      localObject1 = Float.valueOf(f);
      continue;
      localObject1 = paramCursor.getString(paramInt);
      continue;
      localObject3 = l().b();
      localObject2 = "Loaded invalid blob type value, ignoring it";
      ((t.a)localObject3).a((String)localObject2);
    }
  }
  
  public void b()
  {
    y();
    q().beginTransaction();
  }
  
  public void b(String paramString1, String paramString2)
  {
    p.a(paramString1);
    p.a(paramString2);
    e();
    y();
    try
    {
      Object localObject1 = q();
      localObject2 = "user_attributes";
      str = "app_id=? and name=?";
      int i = 2;
      String[] arrayOfString = new String[i];
      int j = 0;
      arrayOfString[0] = paramString1;
      j = 1;
      arrayOfString[j] = paramString2;
      int k = ((SQLiteDatabase)localObject1).delete((String)localObject2, str, arrayOfString);
      localObject2 = l();
      localObject2 = ((t)localObject2).t();
      str = "Deleted user attribute rows:";
      localObject1 = Integer.valueOf(k);
      ((t.a)localObject2).a(str, localObject1);
      return;
    }
    catch (SQLiteException localSQLiteException)
    {
      for (;;)
      {
        Object localObject2 = l().b();
        String str = "Error deleting user attribute";
        ((t.a)localObject2).a(str, paramString1, paramString2, localSQLiteException);
      }
    }
  }
  
  public void o()
  {
    y();
    q().setTransactionSuccessful();
  }
  
  public void p()
  {
    y();
    q().endTransaction();
  }
  
  SQLiteDatabase q()
  {
    e();
    try
    {
      i.a locala = b;
      return locala.getWritableDatabase();
    }
    catch (SQLiteException localSQLiteException)
    {
      l().o().a("Error opening database", localSQLiteException);
      throw localSQLiteException;
    }
  }
  
  /* Error */
  public String r()
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: aload_0
    //   3: invokevirtual 168	com/google/android/gms/measurement/internal/i:q	()Landroid/database/sqlite/SQLiteDatabase;
    //   6: astore_2
    //   7: ldc_w 589
    //   10: astore_3
    //   11: aconst_null
    //   12: astore 4
    //   14: aload_2
    //   15: aload_3
    //   16: aconst_null
    //   17: invokevirtual 174	android/database/sqlite/SQLiteDatabase:rawQuery	(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    //   20: astore_3
    //   21: aload_3
    //   22: invokeinterface 177 1 0
    //   27: istore 5
    //   29: iload 5
    //   31: ifeq +28 -> 59
    //   34: iconst_0
    //   35: istore 5
    //   37: aconst_null
    //   38: astore_2
    //   39: aload_3
    //   40: iconst_0
    //   41: invokeinterface 265 2 0
    //   46: astore_1
    //   47: aload_3
    //   48: ifnull +9 -> 57
    //   51: aload_3
    //   52: invokeinterface 185 1 0
    //   57: aload_1
    //   58: areturn
    //   59: aload_3
    //   60: ifnull -3 -> 57
    //   63: aload_3
    //   64: invokeinterface 185 1 0
    //   69: goto -12 -> 57
    //   72: astore_2
    //   73: aconst_null
    //   74: astore_3
    //   75: aload_0
    //   76: invokevirtual 94	com/google/android/gms/measurement/internal/i:l	()Lcom/google/android/gms/measurement/internal/t;
    //   79: astore 4
    //   81: aload 4
    //   83: invokevirtual 187	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   86: astore 4
    //   88: ldc_w 591
    //   91: astore 6
    //   93: aload 4
    //   95: aload 6
    //   97: aload_2
    //   98: invokevirtual 436	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   101: aload_3
    //   102: ifnull -45 -> 57
    //   105: aload_3
    //   106: invokeinterface 185 1 0
    //   111: goto -54 -> 57
    //   114: astore_2
    //   115: aconst_null
    //   116: astore_3
    //   117: aload_2
    //   118: astore_1
    //   119: aload_3
    //   120: ifnull +9 -> 129
    //   123: aload_3
    //   124: invokeinterface 185 1 0
    //   129: aload_1
    //   130: athrow
    //   131: astore_1
    //   132: goto -13 -> 119
    //   135: astore_2
    //   136: goto -61 -> 75
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	139	0	this	i
    //   1	129	1	localObject1	Object
    //   131	1	1	localObject2	Object
    //   6	33	2	localSQLiteDatabase	SQLiteDatabase
    //   72	26	2	localSQLiteException1	SQLiteException
    //   114	4	2	localObject3	Object
    //   135	1	2	localSQLiteException2	SQLiteException
    //   10	114	3	localObject4	Object
    //   12	82	4	localObject5	Object
    //   27	9	5	bool	boolean
    //   91	5	6	str	String
    // Exception table:
    //   from	to	target	type
    //   16	20	72	android/database/sqlite/SQLiteException
    //   16	20	114	finally
    //   21	27	131	finally
    //   40	46	131	finally
    //   75	79	131	finally
    //   81	86	131	finally
    //   97	101	131	finally
    //   21	27	135	android/database/sqlite/SQLiteException
    //   40	46	135	android/database/sqlite/SQLiteException
  }
  
  void s()
  {
    e();
    y();
    boolean bool = B();
    if (!bool) {}
    for (;;)
    {
      return;
      w.a locala = mf;
      long l1 = locala.a();
      e locale = h();
      long l2 = locale.b();
      l1 = Math.abs(l2 - l1);
      h localh = n();
      long l3 = localh.F();
      bool = l1 < l3;
      if (bool)
      {
        locala = mf;
        locala.a(l2);
        t();
      }
    }
  }
  
  void t()
  {
    e();
    y();
    boolean bool = B();
    if (!bool) {}
    for (;;)
    {
      return;
      Object localObject1 = q();
      int j = 2;
      Object localObject2 = new String[j];
      String str1 = String.valueOf(h().a());
      localObject2[0] = str1;
      int k = 1;
      long l = n().E();
      str1 = String.valueOf(l);
      localObject2[k] = str1;
      String str2 = "queue";
      str1 = "abs(bundle_end_timestamp - ?) > cast(? as integer)";
      int i = ((SQLiteDatabase)localObject1).delete(str2, str1, (String[])localObject2);
      if (i > 0)
      {
        localObject2 = l().t();
        str2 = "Deleted stale rows. rowsDeleted";
        localObject1 = Integer.valueOf(i);
        ((t.a)localObject2).a(str2, localObject1);
      }
    }
  }
  
  public long u()
  {
    return a("select max(bundle_end_timestamp) from queue", null, 0L);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/i.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */