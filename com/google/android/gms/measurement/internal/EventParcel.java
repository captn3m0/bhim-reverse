package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public final class EventParcel
  implements SafeParcelable
{
  public static final o CREATOR;
  public final int a;
  public final String b;
  public final EventParams c;
  public final String d;
  public final long e;
  
  static
  {
    o localo = new com/google/android/gms/measurement/internal/o;
    localo.<init>();
    CREATOR = localo;
  }
  
  EventParcel(int paramInt, String paramString1, EventParams paramEventParams, String paramString2, long paramLong)
  {
    a = paramInt;
    b = paramString1;
    c = paramEventParams;
    d = paramString2;
    e = paramLong;
  }
  
  public EventParcel(String paramString1, EventParams paramEventParams, String paramString2, long paramLong)
  {
    a = 1;
    b = paramString1;
    c = paramEventParams;
    d = paramString2;
    e = paramLong;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append("origin=");
    Object localObject = d;
    localStringBuilder = localStringBuilder.append((String)localObject).append(",name=");
    localObject = b;
    localStringBuilder = localStringBuilder.append((String)localObject).append(",params=");
    localObject = c;
    return (String)localObject;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    o.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/EventParcel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */