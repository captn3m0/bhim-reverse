package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

class ae$3
  implements Runnable
{
  ae$3(ae paramae) {}
  
  public void run()
  {
    Object localObject1 = ae.c(a);
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = a.l().b();
      localObject2 = "Failed to send measurementEnabled to service";
      ((t.a)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      return;
      try
      {
        localObject2 = a;
        localObject2 = ((ae)localObject2).f();
        localObject3 = a;
        localObject3 = ((ae)localObject3).l();
        localObject3 = ((t)localObject3).u();
        localObject2 = ((r)localObject2).a((String)localObject3);
        ((q)localObject1).b((AppMetadata)localObject2);
        localObject1 = a;
        ae.d((ae)localObject1);
      }
      catch (RemoteException localRemoteException)
      {
        localObject2 = a.l().b();
        Object localObject3 = "Failed to send measurementEnabled to AppMeasurementService";
        ((t.a)localObject2).a((String)localObject3, localRemoteException);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/ae$3.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */