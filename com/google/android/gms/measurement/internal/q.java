package com.google.android.gms.measurement.internal;

import android.os.IInterface;

public abstract interface q
  extends IInterface
{
  public abstract void a(AppMetadata paramAppMetadata);
  
  public abstract void a(EventParcel paramEventParcel, AppMetadata paramAppMetadata);
  
  public abstract void a(EventParcel paramEventParcel, String paramString1, String paramString2);
  
  public abstract void a(UserAttributeParcel paramUserAttributeParcel, AppMetadata paramAppMetadata);
  
  public abstract void b(AppMetadata paramAppMetadata);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/q.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */