package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.text.TextUtils;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.measurement.b;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.security.auth.x500.X500Principal;

public class r
  extends ab
{
  private static final X500Principal a;
  private String b;
  private String c;
  private String d;
  private String e;
  private long f;
  private String h;
  
  static
  {
    X500Principal localX500Principal = new javax/security/auth/x500/X500Principal;
    localX500Principal.<init>("CN=Android Debug,O=Android,C=US");
    a = localX500Principal;
  }
  
  r(y paramy)
  {
    super(paramy);
  }
  
  static long a(byte[] paramArrayOfByte)
  {
    int i = 0;
    p.a(paramArrayOfByte);
    int j = paramArrayOfByte.length;
    if (j > 0) {}
    long l1;
    int k;
    for (j = 1;; k = 0)
    {
      p.a(j);
      l1 = 0L;
      k = paramArrayOfByte.length + -1;
      while (k >= 0)
      {
        int m = paramArrayOfByte.length + -8;
        if (k < m) {
          break;
        }
        m = paramArrayOfByte[k];
        long l2 = m;
        long l3 = 255L;
        l2 = (l2 & l3) << i;
        l1 += l2;
        i += 8;
        k += -1;
      }
    }
    return l1;
  }
  
  AppMetadata a(String paramString)
  {
    AppMetadata localAppMetadata = new com/google/android/gms/measurement/internal/AppMetadata;
    String str1 = b;
    String str2 = b();
    String str3 = c;
    String str4 = d;
    long l1 = n().B();
    long l2 = o();
    boolean bool = m().r();
    localAppMetadata.<init>(str1, str2, str3, str4, l1, l2, paramString, bool);
    return localAppMetadata;
  }
  
  protected void a()
  {
    String str1 = "Unknown";
    str2 = "Unknown";
    localObject1 = i().getPackageManager();
    localObject2 = i().getPackageName();
    Object localObject3 = ((PackageManager)localObject1).getInstallerPackageName((String)localObject2);
    if (localObject3 == null) {
      localObject3 = "manual_install";
    }
    for (;;)
    {
      try
      {
        localObject4 = i();
        localObject4 = ((Context)localObject4).getPackageName();
        localObject5 = null;
        localObject4 = ((PackageManager)localObject1).getPackageInfo((String)localObject4, 0);
        if (localObject4 != null)
        {
          localObject5 = applicationInfo;
          localObject5 = ((PackageManager)localObject1).getApplicationLabel((ApplicationInfo)localObject5);
          boolean bool1 = TextUtils.isEmpty((CharSequence)localObject5);
          if (!bool1) {
            str2 = ((CharSequence)localObject5).toString();
          }
          str1 = versionName;
        }
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException2)
      {
        Object localObject4;
        long l;
        locala = l().b();
        Object localObject5 = "Error retrieving package info: appName";
        locala.a((String)localObject5, str2);
        continue;
      }
      b = ((String)localObject2);
      d = ((String)localObject3);
      c = str1;
      e = str2;
      l = 0L;
      localObject3 = "MD5";
      try
      {
        localObject3 = MessageDigest.getInstance((String)localObject3);
        boolean bool2 = p();
        if (!bool2)
        {
          localObject2 = i();
          localObject2 = ((Context)localObject2).getPackageName();
          int j = 64;
          localObject1 = ((PackageManager)localObject1).getPackageInfo((String)localObject2, j);
          if (localObject3 != null)
          {
            localObject2 = signatures;
            if (localObject2 != null)
            {
              localObject2 = signatures;
              int i = localObject2.length;
              if (i > 0)
              {
                localObject1 = signatures;
                i = 0;
                localObject2 = null;
                localObject1 = localObject1[0];
                localObject1 = ((Signature)localObject1).toByteArray();
                localObject3 = ((MessageDigest)localObject3).digest((byte[])localObject1);
                l = a((byte[])localObject3);
              }
            }
          }
        }
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
      {
        boolean bool3;
        t.a locala;
        localObject1 = l().b();
        localObject2 = "Could not get MD5 instance";
        ((t.a)localObject1).a((String)localObject2, localNoSuchAlgorithmException);
        continue;
      }
      catch (PackageManager.NameNotFoundException localNameNotFoundException1)
      {
        localObject1 = l().b();
        localObject2 = "Package name not found";
        ((t.a)localObject1).a((String)localObject2, localNameNotFoundException1);
        continue;
      }
      f = l;
      return;
      localObject4 = "com.android.vending";
      bool3 = ((String)localObject4).equals(localObject3);
      if (bool3) {
        localObject3 = "";
      }
    }
  }
  
  String b()
  {
    y();
    Object localObject1 = n();
    int i = ((h)localObject1).C();
    if (i != 0)
    {
      localObject1 = "";
      return (String)localObject1;
    }
    localObject1 = h;
    Object localObject3;
    if (localObject1 == null)
    {
      localObject1 = i();
      localObject3 = b.a((Context)localObject1);
      if (localObject3 != null)
      {
        i = ((Status)localObject3).d();
        if (i == 0) {}
      }
    }
    else
    {
      for (;;)
      {
        try
        {
          i = b.c();
          if (i == 0) {
            continue;
          }
          localObject1 = b.a();
          boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
          if (bool) {
            localObject1 = "";
          }
          h = ((String)localObject1);
        }
        catch (IllegalStateException localIllegalStateException)
        {
          h = "";
          localObject3 = l().b();
          localObject4 = "getGoogleAppId or isMeasurementEnabled failed with exception";
          ((t.a)localObject3).a((String)localObject4, localIllegalStateException);
          continue;
        }
        localObject1 = h;
        break;
        localObject1 = "";
        h = ((String)localObject1);
      }
    }
    h = "";
    Object localObject2 = l();
    Object localObject4 = ((t)localObject2).b();
    String str = "getGoogleAppId failed with status";
    if (localObject3 == null)
    {
      i = 0;
      localObject2 = null;
    }
    for (;;)
    {
      localObject2 = Integer.valueOf(i);
      ((t.a)localObject4).a(str, localObject2);
      if (localObject3 == null) {
        break;
      }
      localObject2 = ((Status)localObject3).b();
      if (localObject2 == null) {
        break;
      }
      localObject2 = l().s();
      localObject3 = ((Status)localObject3).b();
      ((t.a)localObject2).a((String)localObject3);
      break;
      int j = ((Status)localObject3).e();
    }
  }
  
  long o()
  {
    y();
    return f;
  }
  
  boolean p()
  {
    try
    {
      Object localObject1 = i();
      localObject1 = ((Context)localObject1).getPackageManager();
      localObject2 = i();
      localObject2 = ((Context)localObject2).getPackageName();
      int i = 64;
      localObject1 = ((PackageManager)localObject1).getPackageInfo((String)localObject2, i);
      if (localObject1 == null) {
        break label148;
      }
      localObject2 = signatures;
      if (localObject2 == null) {
        break label148;
      }
      localObject2 = signatures;
      int j = localObject2.length;
      if (j <= 0) {
        break label148;
      }
      localObject1 = signatures;
      j = 0;
      localObject2 = null;
      localObject1 = localObject1[0];
      localObject2 = "X.509";
      localObject2 = CertificateFactory.getInstance((String)localObject2);
      localObject3 = new java/io/ByteArrayInputStream;
      localObject1 = ((Signature)localObject1).toByteArray();
      ((ByteArrayInputStream)localObject3).<init>((byte[])localObject1);
      localObject1 = ((CertificateFactory)localObject2).generateCertificate((InputStream)localObject3);
      localObject1 = (X509Certificate)localObject1;
      localObject1 = ((X509Certificate)localObject1).getSubjectX500Principal();
      localObject2 = a;
      bool = ((X500Principal)localObject1).equals(localObject2);
    }
    catch (CertificateException localCertificateException)
    {
      for (;;)
      {
        localObject2 = l().b();
        localObject3 = "Error obtaining certificate";
        ((t.a)localObject2).a((String)localObject3, localCertificateException);
        boolean bool = true;
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        label148:
        Object localObject2 = l().b();
        Object localObject3 = "Package name not found";
        ((t.a)localObject2).a((String)localObject3, localNameNotFoundException);
      }
    }
    return bool;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/r.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */