package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.e;
import com.google.android.gms.internal.j.a;
import com.google.android.gms.internal.j.b;
import com.google.android.gms.internal.j.c;
import com.google.android.gms.internal.j.d;
import com.google.android.gms.internal.j.e;
import com.google.android.gms.measurement.AppMeasurementReceiver;
import com.google.android.gms.measurement.AppMeasurementService;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class y
{
  private static ac a;
  private static volatile y b;
  private final Context c;
  private final h d;
  private final w e;
  private final t f;
  private final x g;
  private final com.google.android.gms.measurement.a h;
  private final f i;
  private final i j;
  private final u k;
  private final e l;
  private final ae m;
  private final k n;
  private final ad o;
  private final r p;
  private final v q;
  private final c r;
  private final boolean s;
  private Boolean t;
  private List u;
  private int v;
  private int w;
  
  y(ac paramac)
  {
    p.a(paramac);
    Object localObject1 = a;
    c = ((Context)localObject1);
    localObject1 = paramac.j(this);
    l = ((e)localObject1);
    localObject1 = paramac.a(this);
    d = ((h)localObject1);
    localObject1 = paramac.b(this);
    ((w)localObject1).z();
    e = ((w)localObject1);
    localObject1 = paramac.c(this);
    ((t)localObject1).z();
    f = ((t)localObject1);
    localObject1 = paramac.g(this);
    i = ((f)localObject1);
    localObject1 = paramac.l(this);
    ((k)localObject1).z();
    n = ((k)localObject1);
    localObject1 = paramac.m(this);
    ((r)localObject1).z();
    p = ((r)localObject1);
    localObject1 = paramac.h(this);
    ((i)localObject1).z();
    j = ((i)localObject1);
    localObject1 = paramac.i(this);
    ((u)localObject1).z();
    k = ((u)localObject1);
    localObject1 = paramac.k(this);
    ((ae)localObject1).z();
    m = ((ae)localObject1);
    localObject1 = paramac.f(this);
    ((ad)localObject1).z();
    o = ((ad)localObject1);
    localObject1 = paramac.o(this);
    ((c)localObject1).z();
    r = ((c)localObject1);
    localObject1 = paramac.n(this);
    q = ((v)localObject1);
    localObject1 = paramac.e(this);
    h = ((com.google.android.gms.measurement.a)localObject1);
    localObject1 = paramac.d(this);
    ((x)localObject1).z();
    g = ((x)localObject1);
    int i1 = v;
    int i3 = w;
    Object localObject2;
    if (i1 != i3)
    {
      localObject1 = f().b();
      localObject2 = "Not all components initialized";
      int i4 = v;
      Integer localInteger1 = Integer.valueOf(i4);
      int i5 = w;
      Integer localInteger2 = Integer.valueOf(i5);
      ((t.a)localObject1).a((String)localObject2, localInteger1, localInteger2);
    }
    s = true;
    localObject1 = d;
    boolean bool = ((h)localObject1).C();
    if (!bool)
    {
      bool = v();
      if (!bool)
      {
        localObject1 = c.getApplicationContext();
        bool = localObject1 instanceof Application;
        if (!bool) {
          break label401;
        }
        int i2 = Build.VERSION.SDK_INT;
        i3 = 14;
        if (i2 < i3) {
          break label380;
        }
        localObject1 = i();
        ((ad)localObject1).b();
      }
    }
    for (;;)
    {
      localObject1 = g;
      localObject2 = new com/google/android/gms/measurement/internal/y$1;
      ((y.1)localObject2).<init>(this);
      ((x)localObject1).a((Runnable)localObject2);
      return;
      label380:
      localObject1 = f().s();
      localObject2 = "Not tracking deep linking pre-ICS";
      ((t.a)localObject1).a((String)localObject2);
      continue;
      label401:
      localObject1 = f().o();
      localObject2 = "Application context is not an Application";
      ((t.a)localObject1).a((String)localObject2);
    }
  }
  
  private boolean A()
  {
    String str = l().r();
    boolean bool = TextUtils.isEmpty(str);
    if (!bool) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  private void B()
  {
    long l1 = 0L;
    u();
    a();
    boolean bool1 = b();
    c localc;
    if (bool1)
    {
      bool1 = A();
      if (bool1) {}
    }
    else
    {
      s().b();
      localc = t();
      localc.b();
    }
    for (;;)
    {
      return;
      long l2 = C();
      boolean bool2 = l2 < l1;
      if (!bool2)
      {
        s().b();
        localc = t();
        localc.b();
      }
      else
      {
        Object localObject1 = m();
        bool2 = ((u)localObject1).b();
        if (!bool2)
        {
          s().a();
          localc = t();
          localc.b();
        }
        else
        {
          localObject1 = ee;
          long l3 = ((w.a)localObject1).a();
          Object localObject2 = d();
          long l4 = ((h)localObject2).L();
          f localf = k();
          boolean bool3 = localf.a(l3, l4);
          if (!bool3)
          {
            l3 += l4;
            l2 = Math.max(l2, l3);
          }
          s().b();
          localObject1 = o();
          l3 = ((e)localObject1).a();
          l2 -= l3;
          bool2 = l2 < l1;
          if (!bool2)
          {
            localc = t();
            l3 = 1L;
            localc.a(l3);
          }
          else
          {
            localObject1 = f().t();
            String str = "Upload scheduled in approximately ms";
            localObject2 = Long.valueOf(l2);
            ((t.a)localObject1).a(str, localObject2);
            localObject1 = t();
            ((c)localObject1).a(l2);
          }
        }
      }
    }
  }
  
  private long C()
  {
    long l1 = 0L;
    e locale = o();
    long l2 = locale.a();
    h localh1 = d();
    long l3 = localh1.O();
    h localh2 = d();
    long l4 = localh2.M();
    w.a locala1 = ec;
    long l5 = locala1.a();
    w.a locala2 = ed;
    long l6 = locala2.a();
    Object localObject = l();
    long l7 = ((i)localObject).u();
    boolean bool1 = l7 < l1;
    if (!bool1) {
      l3 = l1;
    }
    for (;;)
    {
      return l3;
      l7 = Math.abs(l7 - l2);
      l2 -= l7;
      l3 += l2;
      localObject = k();
      boolean bool2 = ((f)localObject).a(l5, l4);
      if (!bool2) {
        l3 = l5 + l4;
      }
      boolean bool3 = l6 < l1;
      if (bool3)
      {
        int i1 = l6 < l2;
        if (i1 >= 0)
        {
          i1 = 0;
          locale = null;
          for (;;)
          {
            h localh3 = d();
            int i3 = localh3.Q();
            if (i1 >= i3) {
              break label259;
            }
            l4 = 1 << i1;
            localh3 = d();
            l5 = localh3.P();
            l4 *= l5;
            l3 += l4;
            boolean bool4 = l3 < l6;
            if (bool4) {
              break;
            }
            int i2;
            i1 += 1;
          }
          label259:
          l3 = l1;
        }
      }
    }
  }
  
  public static y a(Context paramContext)
  {
    p.a(paramContext);
    p.a(paramContext.getApplicationContext());
    Object localObject1 = b;
    if (localObject1 == null) {}
    synchronized (y.class)
    {
      localObject1 = b;
      if (localObject1 == null)
      {
        localObject1 = a;
        if (localObject1 != null)
        {
          localObject1 = a;
          localObject1 = ((ac)localObject1).a();
          b = (y)localObject1;
        }
      }
      else
      {
        return b;
      }
      localObject1 = new com/google/android/gms/measurement/internal/ac;
      ((ac)localObject1).<init>(paramContext);
    }
  }
  
  private void a(int paramInt, Throwable paramThrowable, byte[] paramArrayOfByte)
  {
    boolean bool = false;
    Object localObject1 = null;
    u();
    a();
    if (paramArrayOfByte == null) {
      paramArrayOfByte = new byte[0];
    }
    Object localObject4 = u;
    Object localObject5 = null;
    u = null;
    int i1 = 200;
    long l1;
    Integer localInteger1;
    Object localObject3;
    if (paramInt != i1)
    {
      i1 = 204;
      if (paramInt != i1) {}
    }
    else if (paramThrowable == null)
    {
      localObject1 = ec;
      l1 = o().a();
      ((w.a)localObject1).a(l1);
      localObject1 = ed;
      l1 = 0L;
      ((w.a)localObject1).a(l1);
      B();
      localObject1 = f().t();
      localObject5 = "Successful upload. Got network response. code, size";
      localInteger1 = Integer.valueOf(paramInt);
      int i2 = paramArrayOfByte.length;
      Integer localInteger2 = Integer.valueOf(i2);
      ((t.a)localObject1).a((String)localObject5, localInteger1, localInteger2);
      localObject1 = l();
      ((i)localObject1).b();
      try
      {
        localObject4 = ((List)localObject4).iterator();
        for (;;)
        {
          bool = ((Iterator)localObject4).hasNext();
          if (!bool) {
            break;
          }
          localObject1 = ((Iterator)localObject4).next();
          localObject1 = (Long)localObject1;
          localObject5 = l();
          long l2 = ((Long)localObject1).longValue();
          ((i)localObject5).a(l2);
        }
        localObject3 = l();
      }
      finally
      {
        l().p();
      }
      ((i)localObject3).o();
      l().p();
      localObject3 = m();
      bool = ((u)localObject3).b();
      if (bool)
      {
        bool = A();
        if (bool) {
          x();
        }
      }
    }
    for (;;)
    {
      return;
      B();
      continue;
      localObject4 = f().t();
      localInteger1 = Integer.valueOf(paramInt);
      ((t.a)localObject4).a("Network upload failed. Will retry later. code, error", localInteger1, paramThrowable);
      localObject4 = ed;
      localObject5 = o();
      l1 = ((e)localObject5).a();
      ((w.a)localObject4).a(l1);
      int i3 = 503;
      if (paramInt != i3)
      {
        i3 = 429;
        if (paramInt != i3) {}
      }
      else
      {
        bool = true;
      }
      if (bool)
      {
        localObject3 = ee;
        localObject4 = o();
        l1 = ((e)localObject4).a();
        ((w.a)localObject3).a(l1);
      }
      B();
    }
  }
  
  private void a(aa paramaa)
  {
    if (paramaa == null)
    {
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Component not created");
      throw localIllegalStateException;
    }
  }
  
  private void a(List paramList)
  {
    boolean bool = paramList.isEmpty();
    Object localObject;
    if (!bool)
    {
      bool = true;
      p.b(bool);
      localObject = u;
      if (localObject == null) {
        break label53;
      }
      localObject = f().b();
      String str = "Set uploading progress before finishing the previous upload";
      ((t.a)localObject).a(str);
    }
    for (;;)
    {
      return;
      bool = false;
      localObject = null;
      break;
      label53:
      localObject = new java/util/ArrayList;
      ((ArrayList)localObject).<init>(paramList);
      u = ((List)localObject);
    }
  }
  
  private void b(ab paramab)
  {
    IllegalStateException localIllegalStateException;
    if (paramab == null)
    {
      localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Component not created");
      throw localIllegalStateException;
    }
    boolean bool = paramab.w();
    if (!bool)
    {
      localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Component not initialized");
      throw localIllegalStateException;
    }
  }
  
  private void c(AppMetadata paramAppMetadata)
  {
    u();
    a();
    p.a(paramAppMetadata);
    p.a(b);
    Object localObject1 = l();
    Object localObject2 = b;
    localObject2 = ((i)localObject1).b((String)localObject2);
    String str1 = e().o();
    boolean bool1 = false;
    localObject1 = null;
    String str2;
    String str3;
    if (localObject2 == null)
    {
      localObject2 = new com/google/android/gms/measurement/internal/a;
      str2 = b;
      localObject1 = e();
      str3 = ((w)localObject1).p();
      String str4 = c;
      long l1 = 0L;
      long l2 = 0L;
      String str5 = d;
      String str6 = e;
      long l3 = f;
      long l4 = g;
      boolean bool2 = i;
      ((a)localObject2).<init>(str2, str3, str4, str1, l1, l2, str5, str6, l3, l4, bool2);
      bool1 = true;
    }
    for (;;)
    {
      str2 = c;
      boolean bool3 = TextUtils.isEmpty(str2);
      if (!bool3)
      {
        str2 = c;
        str3 = c;
        bool3 = str2.equals(str3);
        if (bool3)
        {
          l5 = f;
          l6 = i;
          bool3 = l5 < l6;
          if (!bool3) {}
        }
        else
        {
          localObject1 = c;
          l5 = f;
          localObject2 = ((a)localObject2).a((String)localObject1, l5);
          bool1 = true;
        }
      }
      str2 = d;
      bool3 = TextUtils.isEmpty(str2);
      if (!bool3)
      {
        str2 = d;
        str3 = g;
        bool3 = str2.equals(str3);
        if (bool3)
        {
          str2 = e;
          str3 = h;
          bool3 = str2.equals(str3);
          if (bool3) {}
        }
        else
        {
          localObject1 = d;
          str2 = e;
          localObject2 = ((a)localObject2).b((String)localObject1, str2);
          bool1 = true;
        }
      }
      long l5 = g;
      long l6 = j;
      bool3 = l5 < l6;
      if (bool3)
      {
        l5 = g;
        localObject2 = ((a)localObject2).a(l5);
        bool1 = true;
      }
      bool3 = i;
      boolean bool4 = k;
      if (bool3 != bool4)
      {
        bool1 = i;
        localObject2 = ((a)localObject2).a(bool1);
        bool1 = true;
      }
      if (bool1)
      {
        localObject1 = l();
        ((i)localObject1).a((a)localObject2);
      }
      return;
      str2 = d;
      bool3 = str1.equals(str2);
      if (!bool3)
      {
        localObject1 = e().p();
        localObject2 = ((a)localObject2).a((String)localObject1, str1);
        bool1 = true;
      }
    }
  }
  
  private boolean z()
  {
    u();
    List localList = u;
    boolean bool;
    if (localList != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localList = null;
    }
  }
  
  j.d a(l[] paramArrayOfl, AppMetadata paramAppMetadata)
  {
    p.a(paramAppMetadata);
    p.a(paramArrayOfl);
    u();
    j.d locald = new com/google/android/gms/internal/j$d;
    locald.<init>();
    Object localObject1 = Integer.valueOf(1);
    a = ((Integer)localObject1);
    i = "android";
    localObject1 = b;
    o = ((String)localObject1);
    localObject1 = e;
    n = ((String)localObject1);
    localObject1 = d;
    p = ((String)localObject1);
    localObject1 = Long.valueOf(f);
    q = ((Long)localObject1);
    localObject1 = c;
    y = ((String)localObject1);
    long l1 = g;
    long l2 = 0L;
    int i1 = l1 < l2;
    if (i1 == 0) {
      i1 = 0;
    }
    for (localObject1 = null;; localObject1 = Long.valueOf(l1))
    {
      v = ((Long)localObject1);
      localObject1 = e();
      localObject2 = ((w)localObject1).b();
      if (localObject2 != null)
      {
        localObject1 = first;
        if (localObject1 != null)
        {
          localObject1 = second;
          if (localObject1 != null)
          {
            localObject1 = (String)first;
            s = ((String)localObject1);
            localObject1 = (Boolean)second;
            t = ((Boolean)localObject1);
          }
        }
      }
      localObject1 = q().b();
      k = ((String)localObject1);
      localObject1 = q().o();
      j = ((String)localObject1);
      localObject1 = Integer.valueOf((int)q().p());
      m = ((Integer)localObject1);
      localObject1 = q().q();
      l = ((String)localObject1);
      r = null;
      d = null;
      localObject1 = Long.valueOf(0d);
      e = ((Long)localObject1);
      l1 = 0d;
      localObject1 = Long.valueOf(l1);
      f = ((Long)localObject1);
      i1 = 1;
      for (;;)
      {
        int i5 = paramArrayOfl.length;
        if (i1 >= i5) {
          break;
        }
        l2 = e.longValue();
        l3 = d;
        localObject2 = Long.valueOf(Math.min(l2, l3));
        e = ((Long)localObject2);
        l2 = f.longValue();
        l3 = d;
        l2 = Math.max(l2, l3);
        localObject2 = Long.valueOf(l2);
        f = ((Long)localObject2);
        int i2;
        i1 += 1;
      }
      l1 = g;
    }
    localObject1 = l();
    Object localObject2 = b;
    localObject2 = ((i)localObject1).b((String)localObject2);
    Object localObject4;
    Object localObject5;
    Object localObject6;
    if (localObject2 == null)
    {
      localObject2 = new com/google/android/gms/measurement/internal/a;
      localObject3 = b;
      localObject4 = e().p();
      localObject5 = c;
      localObject1 = e();
      localObject6 = ((w)localObject1).o();
      long l4 = 0L;
      long l5 = 0L;
      String str1 = d;
      String str2 = e;
      long l6 = f;
      long l7 = g;
      boolean bool3 = i;
      ((a)localObject2).<init>((String)localObject3, (String)localObject4, (String)localObject5, (String)localObject6, l4, l5, str1, str2, l6, l7, bool3);
    }
    localObject1 = f();
    l2 = f.longValue();
    localObject1 = ((a)localObject2).a((t)localObject1, l2);
    l().a((a)localObject1);
    Object localObject3 = b;
    u = ((String)localObject3);
    localObject1 = Integer.valueOf((int)e);
    w = ((Integer)localObject1);
    l2 = f;
    long l3 = 0L;
    boolean bool1 = l2 < l3;
    if (!bool1) {
      bool1 = false;
    }
    for (localObject1 = null;; localObject1 = Long.valueOf(l1))
    {
      h = ((Long)localObject1);
      localObject1 = h;
      g = ((Long)localObject1);
      localObject1 = l();
      localObject2 = b;
      localObject3 = ((i)localObject1).a((String)localObject2);
      localObject1 = new j.e[((List)localObject3).size()];
      c = ((j.e[])localObject1);
      bool1 = false;
      localObject1 = null;
      i6 = 0;
      localObject2 = null;
      for (;;)
      {
        i3 = ((List)localObject3).size();
        if (i6 >= i3) {
          break;
        }
        localObject4 = new com/google/android/gms/internal/j$e;
        ((j.e)localObject4).<init>();
        c[i6] = localObject4;
        localObject1 = getb;
        b = ((String)localObject1);
        l3 = getc;
        localObject1 = Long.valueOf(l3);
        a = ((Long)localObject1);
        localObject5 = k();
        localObject1 = getd;
        ((f)localObject5).a((j.e)localObject4, localObject1);
        i3 = i6 + 1;
        i6 = i3;
      }
      l1 = f;
    }
    localObject1 = new j.a[paramArrayOfl.length];
    b = ((j.a[])localObject1);
    int i3 = 0;
    localObject1 = null;
    int i6 = 0;
    localObject2 = null;
    for (;;)
    {
      i3 = paramArrayOfl.length;
      if (i6 >= i3) {
        break;
      }
      localObject5 = new com/google/android/gms/internal/j$a;
      ((j.a)localObject5).<init>();
      b[i6] = localObject5;
      localObject1 = b;
      b = ((String)localObject1);
      l2 = d;
      localObject1 = Long.valueOf(l2);
      c = ((Long)localObject1);
      localObject1 = new j.b[f.a()];
      a = ((j.b[])localObject1);
      i3 = 0;
      localObject1 = null;
      localObject6 = f.iterator();
      int i7 = 0;
      localObject3 = null;
      for (;;)
      {
        boolean bool2 = ((Iterator)localObject6).hasNext();
        if (!bool2) {
          break;
        }
        localObject1 = (String)((Iterator)localObject6).next();
        j.b localb = new com/google/android/gms/internal/j$b;
        localb.<init>();
        j.b[] arrayOfb = a;
        int i8 = i7 + 1;
        arrayOfb[i7] = localb;
        a = ((String)localObject1);
        localObject1 = f.a((String)localObject1);
        localObject3 = k();
        ((f)localObject3).a(localb, localObject1);
        i7 = i8;
      }
      int i4 = i6 + 1;
      i6 = i4;
    }
    localObject1 = f().u();
    x = ((String)localObject1);
    return locald;
  }
  
  void a()
  {
    boolean bool = s;
    if (!bool)
    {
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("AppMeasurement is not initialized");
      throw localIllegalStateException;
    }
  }
  
  void a(AppMetadata paramAppMetadata)
  {
    u();
    a();
    p.a(b);
    c(paramAppMetadata);
  }
  
  /* Error */
  void a(EventParcel paramEventParcel, AppMetadata paramAppMetadata)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 257	com/google/android/gms/measurement/internal/y:u	()V
    //   4: aload_0
    //   5: invokevirtual 259	com/google/android/gms/measurement/internal/y:a	()V
    //   8: aload_2
    //   9: getfield 441	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   12: invokestatic 444	com/google/android/gms/common/internal/p:a	(Ljava/lang/String;)Ljava/lang/String;
    //   15: pop
    //   16: aload_2
    //   17: getfield 455	com/google/android/gms/measurement/internal/AppMetadata:c	Ljava/lang/String;
    //   20: astore_3
    //   21: aload_3
    //   22: invokestatic 255	android/text/TextUtils:isEmpty	(Ljava/lang/CharSequence;)Z
    //   25: istore 4
    //   27: iload 4
    //   29: ifeq +4 -> 33
    //   32: return
    //   33: aload_0
    //   34: invokevirtual 178	com/google/android/gms/measurement/internal/y:f	()Lcom/google/android/gms/measurement/internal/t;
    //   37: invokevirtual 325	com/google/android/gms/measurement/internal/t:t	()Lcom/google/android/gms/measurement/internal/t$a;
    //   40: ldc_w 666
    //   43: aload_1
    //   44: invokevirtual 335	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   47: new 570	com/google/android/gms/measurement/internal/l
    //   50: astore 5
    //   52: aload_1
    //   53: getfield 669	com/google/android/gms/measurement/internal/EventParcel:d	Ljava/lang/String;
    //   56: astore 6
    //   58: aload_2
    //   59: getfield 441	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   62: astore 7
    //   64: aload_1
    //   65: getfield 670	com/google/android/gms/measurement/internal/EventParcel:b	Ljava/lang/String;
    //   68: astore 8
    //   70: aload_1
    //   71: getfield 671	com/google/android/gms/measurement/internal/EventParcel:e	J
    //   74: lstore 9
    //   76: lconst_0
    //   77: lstore 11
    //   79: aload_1
    //   80: getfield 673	com/google/android/gms/measurement/internal/EventParcel:c	Lcom/google/android/gms/measurement/internal/EventParams;
    //   83: invokevirtual 676	com/google/android/gms/measurement/internal/EventParams:b	()Landroid/os/Bundle;
    //   86: astore 13
    //   88: aload_0
    //   89: astore 14
    //   91: aload 5
    //   93: aload_0
    //   94: aload 6
    //   96: aload 7
    //   98: aload 8
    //   100: lload 9
    //   102: lload 11
    //   104: aload 13
    //   106: invokespecial 679	com/google/android/gms/measurement/internal/l:<init>	(Lcom/google/android/gms/measurement/internal/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJLandroid/os/Bundle;)V
    //   109: aload_0
    //   110: invokevirtual 246	com/google/android/gms/measurement/internal/y:l	()Lcom/google/android/gms/measurement/internal/i;
    //   113: astore_3
    //   114: aload_3
    //   115: invokevirtual 379	com/google/android/gms/measurement/internal/i:b	()V
    //   118: aload_0
    //   119: aload_2
    //   120: invokespecial 664	com/google/android/gms/measurement/internal/y:c	(Lcom/google/android/gms/measurement/internal/AppMetadata;)V
    //   123: aload_0
    //   124: invokevirtual 246	com/google/android/gms/measurement/internal/y:l	()Lcom/google/android/gms/measurement/internal/i;
    //   127: astore_3
    //   128: aload_2
    //   129: getfield 441	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   132: astore 14
    //   134: aload 5
    //   136: getfield 630	com/google/android/gms/measurement/internal/l:b	Ljava/lang/String;
    //   139: astore 6
    //   141: aload_3
    //   142: aload 14
    //   144: aload 6
    //   146: invokevirtual 682	com/google/android/gms/measurement/internal/i:a	(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/measurement/internal/m;
    //   149: astore_3
    //   150: aload_3
    //   151: ifnonnull +144 -> 295
    //   154: new 684	com/google/android/gms/measurement/internal/m
    //   157: astore 6
    //   159: aload_2
    //   160: getfield 441	com/google/android/gms/measurement/internal/AppMetadata:b	Ljava/lang/String;
    //   163: astore 7
    //   165: aload 5
    //   167: getfield 630	com/google/android/gms/measurement/internal/l:b	Ljava/lang/String;
    //   170: astore 8
    //   172: lconst_1
    //   173: lstore 9
    //   175: lconst_1
    //   176: lstore 11
    //   178: aload 5
    //   180: getfield 572	com/google/android/gms/measurement/internal/l:d	J
    //   183: lstore 15
    //   185: aload 6
    //   187: aload 7
    //   189: aload 8
    //   191: lload 9
    //   193: lload 11
    //   195: lload 15
    //   197: invokespecial 687	com/google/android/gms/measurement/internal/m:<init>	(Ljava/lang/String;Ljava/lang/String;JJJ)V
    //   200: aload_0
    //   201: invokevirtual 246	com/google/android/gms/measurement/internal/y:l	()Lcom/google/android/gms/measurement/internal/i;
    //   204: astore_3
    //   205: aload_3
    //   206: aload 6
    //   208: invokevirtual 690	com/google/android/gms/measurement/internal/i:a	(Lcom/google/android/gms/measurement/internal/m;)V
    //   211: iconst_1
    //   212: istore 4
    //   214: iload 4
    //   216: anewarray 570	com/google/android/gms/measurement/internal/l
    //   219: astore_3
    //   220: aconst_null
    //   221: astore 14
    //   223: aload_3
    //   224: iconst_0
    //   225: aload 5
    //   227: aastore
    //   228: aload_0
    //   229: aload_3
    //   230: aload_2
    //   231: invokevirtual 693	com/google/android/gms/measurement/internal/y:a	([Lcom/google/android/gms/measurement/internal/l;Lcom/google/android/gms/measurement/internal/AppMetadata;)Lcom/google/android/gms/internal/j$d;
    //   234: astore_3
    //   235: aload_0
    //   236: invokevirtual 246	com/google/android/gms/measurement/internal/y:l	()Lcom/google/android/gms/measurement/internal/i;
    //   239: astore 14
    //   241: aload 14
    //   243: aload_3
    //   244: invokevirtual 696	com/google/android/gms/measurement/internal/i:a	(Lcom/google/android/gms/internal/j$d;)V
    //   247: aload_0
    //   248: invokevirtual 246	com/google/android/gms/measurement/internal/y:l	()Lcom/google/android/gms/measurement/internal/i;
    //   251: astore_3
    //   252: aload_3
    //   253: invokevirtual 402	com/google/android/gms/measurement/internal/i:o	()V
    //   256: aload_0
    //   257: invokevirtual 178	com/google/android/gms/measurement/internal/y:f	()Lcom/google/android/gms/measurement/internal/t;
    //   260: astore_3
    //   261: aload_3
    //   262: invokevirtual 234	com/google/android/gms/measurement/internal/t:s	()Lcom/google/android/gms/measurement/internal/t$a;
    //   265: astore_3
    //   266: ldc_w 698
    //   269: astore 14
    //   271: aload_3
    //   272: aload 14
    //   274: aload 5
    //   276: invokevirtual 335	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   279: aload_0
    //   280: invokevirtual 246	com/google/android/gms/measurement/internal/y:l	()Lcom/google/android/gms/measurement/internal/i;
    //   283: astore_3
    //   284: aload_3
    //   285: invokevirtual 400	com/google/android/gms/measurement/internal/i:p	()V
    //   288: aload_0
    //   289: invokespecial 376	com/google/android/gms/measurement/internal/y:B	()V
    //   292: goto -260 -> 32
    //   295: aload_3
    //   296: getfield 699	com/google/android/gms/measurement/internal/m:e	J
    //   299: lstore 17
    //   301: aload 5
    //   303: aload_0
    //   304: lload 17
    //   306: invokevirtual 702	com/google/android/gms/measurement/internal/l:a	(Lcom/google/android/gms/measurement/internal/y;J)Lcom/google/android/gms/measurement/internal/l;
    //   309: astore 5
    //   311: aload 5
    //   313: getfield 572	com/google/android/gms/measurement/internal/l:d	J
    //   316: lstore 17
    //   318: aload_3
    //   319: lload 17
    //   321: invokevirtual 705	com/google/android/gms/measurement/internal/m:a	(J)Lcom/google/android/gms/measurement/internal/m;
    //   324: astore 6
    //   326: goto -126 -> 200
    //   329: astore_3
    //   330: aload_0
    //   331: invokevirtual 246	com/google/android/gms/measurement/internal/y:l	()Lcom/google/android/gms/measurement/internal/i;
    //   334: invokevirtual 400	com/google/android/gms/measurement/internal/i:p	()V
    //   337: aload_3
    //   338: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	339	0	this	y
    //   0	339	1	paramEventParcel	EventParcel
    //   0	339	2	paramAppMetadata	AppMetadata
    //   20	299	3	localObject1	Object
    //   329	9	3	localObject2	Object
    //   25	190	4	bool	boolean
    //   50	262	5	locall	l
    //   56	269	6	localObject3	Object
    //   62	126	7	str1	String
    //   68	122	8	str2	String
    //   74	118	9	l1	long
    //   77	117	11	l2	long
    //   86	19	13	localBundle	Bundle
    //   89	184	14	localObject4	Object
    //   183	13	15	l3	long
    //   299	21	17	l4	long
    // Exception table:
    //   from	to	target	type
    //   119	123	329	finally
    //   123	127	329	finally
    //   128	132	329	finally
    //   134	139	329	finally
    //   144	149	329	finally
    //   154	157	329	finally
    //   159	163	329	finally
    //   165	170	329	finally
    //   178	183	329	finally
    //   195	200	329	finally
    //   200	204	329	finally
    //   206	211	329	finally
    //   214	219	329	finally
    //   225	228	329	finally
    //   230	234	329	finally
    //   235	239	329	finally
    //   243	247	329	finally
    //   247	251	329	finally
    //   252	256	329	finally
    //   256	260	329	finally
    //   261	265	329	finally
    //   274	279	329	finally
    //   295	299	329	finally
    //   304	309	329	finally
    //   311	316	329	finally
    //   319	324	329	finally
  }
  
  void a(EventParcel paramEventParcel, String paramString)
  {
    Object localObject1 = l().b(paramString);
    Object localObject2;
    if (localObject1 != null)
    {
      localObject2 = g;
      boolean bool1 = TextUtils.isEmpty((CharSequence)localObject2);
      if (!bool1) {}
    }
    else
    {
      localObject1 = f().s();
      localObject2 = "No app data available; dropping event";
      ((t.a)localObject1).a((String)localObject2, paramString);
    }
    for (;;)
    {
      return;
      localObject2 = new com/google/android/gms/measurement/internal/AppMetadata;
      String str1 = c;
      String str2 = g;
      String str3 = h;
      long l1 = i;
      long l2 = j;
      boolean bool2 = k;
      ((AppMetadata)localObject2).<init>(paramString, str1, str2, str3, l1, l2, null, bool2);
      a(paramEventParcel, (AppMetadata)localObject2);
    }
  }
  
  void a(UserAttributeParcel paramUserAttributeParcel, AppMetadata paramAppMetadata)
  {
    u();
    a();
    Object localObject1 = c;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool) {}
    for (;;)
    {
      return;
      localObject1 = k();
      Object localObject3 = b;
      ((f)localObject1).b((String)localObject3);
      localObject1 = k();
      localObject3 = b;
      Object localObject4 = paramUserAttributeParcel.a();
      Object localObject5 = ((f)localObject1).c((String)localObject3, localObject4);
      if (localObject5 == null) {
        continue;
      }
      localObject3 = new com/google/android/gms/measurement/internal/d;
      localObject4 = b;
      String str = b;
      long l1 = c;
      ((d)localObject3).<init>((String)localObject4, str, l1, localObject5);
      localObject1 = f().s();
      localObject4 = "Setting user attribute";
      str = b;
      ((t.a)localObject1).a((String)localObject4, str, localObject5);
      localObject1 = l();
      ((i)localObject1).b();
      try
      {
        c(paramAppMetadata);
        localObject1 = l();
        ((i)localObject1).a((d)localObject3);
        localObject1 = l();
        ((i)localObject1).o();
        localObject1 = f();
        localObject1 = ((t)localObject1).s();
        localObject4 = "User attribute set";
        str = b;
        localObject3 = d;
        ((t.a)localObject1).a((String)localObject4, str, localObject3);
        localObject1 = l();
        ((i)localObject1).p();
      }
      finally
      {
        l().p();
      }
    }
  }
  
  void a(ab paramab)
  {
    int i1 = v + 1;
    v = i1;
  }
  
  public void a(boolean paramBoolean)
  {
    B();
  }
  
  public void b(AppMetadata paramAppMetadata)
  {
    long l1 = 3600000L;
    long l2 = 1L;
    u();
    a();
    p.a(paramAppMetadata);
    p.a(b);
    Object localObject1 = c;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool) {}
    for (;;)
    {
      return;
      c(paramAppMetadata);
      localObject1 = l();
      String str1 = b;
      String str2 = "_f";
      localObject1 = ((i)localObject1).a(str1, str2);
      if (localObject1 == null)
      {
        long l3 = o().a();
        long l4 = l3 / l1 + l2;
        l1 *= l4;
        localObject1 = new com/google/android/gms/measurement/internal/UserAttributeParcel;
        Object localObject2 = Long.valueOf(l1);
        ((UserAttributeParcel)localObject1).<init>("_fot", l3, localObject2, "auto");
        a((UserAttributeParcel)localObject1, paramAppMetadata);
        localObject1 = new android/os/Bundle;
        ((Bundle)localObject1).<init>();
        str1 = "_c";
        ((Bundle)localObject1).putLong(str1, l2);
        localObject2 = new com/google/android/gms/measurement/internal/EventParcel;
        String str3 = "_f";
        EventParams localEventParams = new com/google/android/gms/measurement/internal/EventParams;
        localEventParams.<init>((Bundle)localObject1);
        String str4 = "auto";
        ((EventParcel)localObject2).<init>(str3, localEventParams, str4, l3);
        a((EventParcel)localObject2, paramAppMetadata);
      }
    }
  }
  
  void b(UserAttributeParcel paramUserAttributeParcel, AppMetadata paramAppMetadata)
  {
    u();
    a();
    Object localObject1 = c;
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool) {}
    for (;;)
    {
      return;
      localObject1 = f().s();
      String str1 = "Removing user attribute";
      String str2 = b;
      ((t.a)localObject1).a(str1, str2);
      localObject1 = l();
      ((i)localObject1).b();
      try
      {
        c(paramAppMetadata);
        localObject1 = l();
        str1 = b;
        str2 = b;
        ((i)localObject1).b(str1, str2);
        localObject1 = l();
        ((i)localObject1).o();
        localObject1 = f();
        localObject1 = ((t)localObject1).s();
        str1 = "User attribute removed";
        str2 = b;
        ((t.a)localObject1).a(str1, str2);
        localObject1 = l();
        ((i)localObject1).p();
      }
      finally
      {
        l().p();
      }
    }
  }
  
  protected boolean b()
  {
    boolean bool1 = true;
    a();
    u();
    Object localObject = t;
    boolean bool2;
    if (localObject == null)
    {
      localObject = k();
      String str = "android.permission.INTERNET";
      bool2 = ((f)localObject).d(str);
      if (!bool2) {
        break label176;
      }
      localObject = k();
      str = "android.permission.ACCESS_NETWORK_STATE";
      bool2 = ((f)localObject).d(str);
      if (!bool2) {
        break label176;
      }
      localObject = n();
      bool2 = AppMeasurementReceiver.a((Context)localObject);
      if (!bool2) {
        break label176;
      }
      localObject = n();
      bool2 = AppMeasurementService.a((Context)localObject);
      if (!bool2) {
        break label176;
      }
      bool2 = bool1;
      localObject = Boolean.valueOf(bool2);
      t = ((Boolean)localObject);
      localObject = t;
      bool2 = ((Boolean)localObject).booleanValue();
      if (bool2)
      {
        localObject = d();
        bool2 = ((h)localObject).C();
        if (!bool2)
        {
          localObject = r().b();
          bool2 = TextUtils.isEmpty((CharSequence)localObject);
          if (bool2) {
            break label184;
          }
        }
      }
    }
    for (;;)
    {
      localObject = Boolean.valueOf(bool1);
      t = ((Boolean)localObject);
      return t.booleanValue();
      label176:
      bool2 = false;
      localObject = null;
      break;
      label184:
      bool1 = false;
    }
  }
  
  protected void c()
  {
    u();
    f().r().a("App measurement is starting up");
    Object localObject = f().s();
    String str = "Debug logging enabled";
    ((t.a)localObject).a(str);
    boolean bool = v();
    if (bool)
    {
      localObject = g;
      bool = ((x)localObject).w();
      if (bool)
      {
        localObject = g;
        bool = ((x)localObject).x();
        if (!bool) {}
      }
      else
      {
        localObject = f().b();
        str = "Scheduler shutting down before Scion.start() called";
        ((t.a)localObject).a(str);
        return;
      }
    }
    localObject = l();
    ((i)localObject).s();
    bool = b();
    if (!bool)
    {
      localObject = k();
      str = "android.permission.INTERNET";
      bool = ((f)localObject).d(str);
      if (!bool)
      {
        localObject = f().b();
        str = "App is missing INTERNET permission";
        ((t.a)localObject).a(str);
      }
      localObject = k();
      str = "android.permission.ACCESS_NETWORK_STATE";
      bool = ((f)localObject).d(str);
      if (!bool)
      {
        localObject = f().b();
        str = "App is missing ACCESS_NETWORK_STATE permission";
        ((t.a)localObject).a(str);
      }
      localObject = n();
      bool = AppMeasurementReceiver.a((Context)localObject);
      if (!bool)
      {
        localObject = f().b();
        str = "AppMeasurementReceiver not registered/enabled";
        ((t.a)localObject).a(str);
      }
      localObject = n();
      bool = AppMeasurementService.a((Context)localObject);
      if (!bool)
      {
        localObject = f().b();
        str = "AppMeasurementService not registered/enabled";
        ((t.a)localObject).a(str);
      }
      localObject = f().b();
      str = "Uploading is not possible. App measurement disabled";
      ((t.a)localObject).a(str);
    }
    for (;;)
    {
      B();
      break;
      localObject = d();
      bool = ((h)localObject).C();
      if (!bool)
      {
        bool = v();
        if (!bool)
        {
          localObject = r().b();
          bool = TextUtils.isEmpty((CharSequence)localObject);
          if (!bool)
          {
            localObject = i();
            ((ad)localObject).o();
          }
        }
      }
    }
  }
  
  public h d()
  {
    return d;
  }
  
  public w e()
  {
    w localw = e;
    a(localw);
    return e;
  }
  
  public t f()
  {
    t localt = f;
    b(localt);
    return f;
  }
  
  public x g()
  {
    x localx = g;
    b(localx);
    return g;
  }
  
  x h()
  {
    return g;
  }
  
  public ad i()
  {
    ad localad = o;
    b(localad);
    return o;
  }
  
  public com.google.android.gms.measurement.a j()
  {
    return h;
  }
  
  public f k()
  {
    f localf = i;
    a(localf);
    return i;
  }
  
  public i l()
  {
    i locali = j;
    b(locali);
    return j;
  }
  
  public u m()
  {
    u localu = k;
    b(localu);
    return k;
  }
  
  public Context n()
  {
    return c;
  }
  
  public e o()
  {
    return l;
  }
  
  public ae p()
  {
    ae localae = m;
    b(localae);
    return m;
  }
  
  public k q()
  {
    k localk = n;
    b(localk);
    return n;
  }
  
  public r r()
  {
    r localr = p;
    b(localr);
    return p;
  }
  
  public v s()
  {
    Object localObject = q;
    if (localObject == null)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Network broadcast receiver not created");
      throw ((Throwable)localObject);
    }
    return q;
  }
  
  public c t()
  {
    c localc = r;
    b(localc);
    return r;
  }
  
  public void u()
  {
    g().e();
  }
  
  protected boolean v()
  {
    return false;
  }
  
  void w()
  {
    Object localObject = d();
    boolean bool = ((h)localObject).C();
    if (bool)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Unexpected call on package side");
      throw ((Throwable)localObject);
    }
  }
  
  public void x()
  {
    int i1 = 0;
    Object localObject1 = null;
    u();
    a();
    Object localObject2 = d();
    boolean bool1 = ((h)localObject2).C();
    if (!bool1)
    {
      localObject2 = e().q();
      if (localObject2 == null)
      {
        localObject2 = f().o();
        localObject3 = "Upload data called on the client side before use of service was decided";
        ((t.a)localObject2).a((String)localObject3);
      }
    }
    long l2;
    Object localObject4;
    Object localObject6;
    long l3;
    do
    {
      boolean bool4;
      do
      {
        for (;;)
        {
          return;
          bool1 = ((Boolean)localObject2).booleanValue();
          if (bool1)
          {
            localObject2 = f().b();
            localObject3 = "Upload called in the client side when service should be used";
            ((t.a)localObject2).a((String)localObject3);
          }
          else
          {
            bool1 = z();
            if (bool1)
            {
              localObject2 = f().o();
              localObject3 = "Uploading requested multiple times";
              ((t.a)localObject2).a((String)localObject3);
            }
            else
            {
              localObject2 = m();
              bool1 = ((u)localObject2).b();
              if (bool1) {
                break;
              }
              localObject2 = f().o();
              localObject3 = "Network not connected, ignoring upload request";
              ((t.a)localObject2).a((String)localObject3);
              B();
            }
          }
        }
        localObject2 = ec;
        long l1 = ((w.a)localObject2).a();
        l2 = 0L;
        boolean bool3 = l1 < l2;
        if (bool3)
        {
          localObject4 = f().s();
          localObject5 = "Uploading events. Elapsed time since last upload attempt (ms)";
          localObject6 = o();
          l3 = ((e)localObject6).a();
          l1 = Math.abs(l3 - l1);
          localObject2 = Long.valueOf(l1);
          ((t.a)localObject4).a((String)localObject5, localObject2);
        }
        localObject2 = l().r();
        bool4 = TextUtils.isEmpty((CharSequence)localObject2);
      } while (bool4);
      localObject3 = d();
      i5 = ((h)localObject3).I();
      int i4 = d().J();
      localObject5 = l();
      localObject4 = ((i)localObject5).a((String)localObject2, i5, i4);
      bool1 = ((List)localObject4).isEmpty();
    } while (bool1);
    int i5 = 0;
    Object localObject3 = null;
    Object localObject5 = ((List)localObject4).iterator();
    boolean bool5;
    do
    {
      bool1 = ((Iterator)localObject5).hasNext();
      if (!bool1) {
        break;
      }
      localObject2 = (j.d)nextfirst;
      localObject6 = s;
      bool5 = TextUtils.isEmpty((CharSequence)localObject6);
    } while (bool5);
    localObject2 = s;
    for (localObject5 = localObject2;; localObject5 = null)
    {
      if (localObject5 != null)
      {
        i5 = 0;
        localObject3 = null;
        int i2 = ((List)localObject4).size();
        if (i5 < i2)
        {
          localObject2 = (j.d)getfirst;
          localObject6 = s;
          bool5 = TextUtils.isEmpty((CharSequence)localObject6);
          if (bool5) {}
          boolean bool2;
          do
          {
            i2 = i5 + 1;
            i5 = i2;
            break;
            localObject2 = s;
            bool2 = ((String)localObject2).equals(localObject5);
          } while (bool2);
          localObject2 = ((List)localObject4).subList(0, i5);
        }
      }
      for (localObject3 = localObject2;; localObject3 = localObject4)
      {
        localObject4 = new com/google/android/gms/internal/j$c;
        ((j.c)localObject4).<init>();
        localObject2 = new j.d[((List)localObject3).size()];
        a = ((j.d[])localObject2);
        localObject5 = new java/util/ArrayList;
        int i3 = ((List)localObject3).size();
        ((ArrayList)localObject5).<init>(i3);
        localObject2 = o();
        l3 = ((e)localObject2).a();
        for (;;)
        {
          localObject2 = a;
          i3 = localObject2.length;
          if (i1 >= i3) {
            break;
          }
          localObject6 = a;
          localObject2 = (j.d)getfirst;
          localObject6[i1] = localObject2;
          localObject2 = getsecond;
          ((List)localObject5).add(localObject2);
          localObject2 = a[i1];
          long l4 = d().B();
          localObject6 = Long.valueOf(l4);
          r = ((Long)localObject6);
          localObject2 = a[i1];
          localObject6 = Long.valueOf(l3);
          d = ((Long)localObject6);
          localObject2 = a[i1];
          bool5 = d().C();
          localObject6 = Boolean.valueOf(bool5);
          z = ((Boolean)localObject6);
          i1 += 1;
        }
        localObject2 = k().a((j.c)localObject4);
        localObject3 = d().K();
        try
        {
          localObject1 = new java/net/URL;
          ((URL)localObject1).<init>((String)localObject3);
          a((List)localObject5);
          localObject4 = e();
          localObject4 = d;
          localObject5 = o();
          l2 = ((e)localObject5).a();
          ((w.a)localObject4).a(l2);
          localObject4 = m();
          localObject5 = new com/google/android/gms/measurement/internal/y$2;
          ((y.2)localObject5).<init>(this);
          ((u)localObject4).a((URL)localObject1, (byte[])localObject2, (u.a)localObject5);
        }
        catch (MalformedURLException localMalformedURLException)
        {
          t.a locala = f().b();
          localObject1 = "Failed to parse upload URL. Not uploading";
          locala.a((String)localObject1, localObject3);
        }
        break;
      }
    }
  }
  
  void y()
  {
    int i1 = w + 1;
    w = i1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/y.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */