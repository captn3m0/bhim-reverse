package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Pair;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.ads.identifier.AdvertisingIdClient.Info;
import com.google.android.gms.internal.e;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Locale;
import java.util.UUID;

class w
  extends ab
{
  static final Pair a;
  public final w.b b;
  public final w.a c;
  public final w.a d;
  public final w.a e;
  public final w.a f;
  private SharedPreferences h;
  private String i;
  private boolean j;
  private long k;
  private final SecureRandom l;
  
  static
  {
    Pair localPair = new android/util/Pair;
    Long localLong = Long.valueOf(0L);
    localPair.<init>("", localLong);
    a = localPair;
  }
  
  w(y paramy)
  {
    super(paramy);
    w.b localb = new com/google/android/gms/measurement/internal/w$b;
    long l2 = n().H();
    localb.<init>(this, "health_monitor", l2, null);
    b = localb;
    Object localObject = new com/google/android/gms/measurement/internal/w$a;
    ((w.a)localObject).<init>(this, "last_upload", l1);
    c = ((w.a)localObject);
    localObject = new com/google/android/gms/measurement/internal/w$a;
    ((w.a)localObject).<init>(this, "last_upload_attempt", l1);
    d = ((w.a)localObject);
    localObject = new com/google/android/gms/measurement/internal/w$a;
    ((w.a)localObject).<init>(this, "backoff", l1);
    e = ((w.a)localObject);
    localObject = new com/google/android/gms/measurement/internal/w$a;
    ((w.a)localObject).<init>(this, "last_delete_stale", l1);
    f = ((w.a)localObject);
    localObject = new java/security/SecureRandom;
    ((SecureRandom)localObject).<init>();
    l = ((SecureRandom)localObject);
  }
  
  static MessageDigest a(String paramString)
  {
    int m = 0;
    MessageDigest localMessageDigest = null;
    int n = 0;
    m = 2;
    if (n < m) {}
    for (;;)
    {
      try
      {
        localMessageDigest = MessageDigest.getInstance(paramString);
        if (localMessageDigest != null) {
          return localMessageDigest;
        }
      }
      catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
      {
        m = n + 1;
        n = m;
      }
      break;
      m = 0;
      Object localObject = null;
    }
  }
  
  private SharedPreferences s()
  {
    e();
    y();
    return h;
  }
  
  protected void a()
  {
    SharedPreferences localSharedPreferences = i().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
    h = localSharedPreferences;
  }
  
  void a(boolean paramBoolean)
  {
    e();
    Object localObject = l().t();
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    ((t.a)localObject).a("Setting useService", localBoolean);
    localObject = s().edit();
    ((SharedPreferences.Editor)localObject).putBoolean("use_service", paramBoolean);
    ((SharedPreferences.Editor)localObject).apply();
  }
  
  Pair b()
  {
    e();
    Object localObject1 = h();
    long l1 = ((e)localObject1).b();
    localObject3 = i;
    long l2;
    boolean bool1;
    if (localObject3 != null)
    {
      l2 = k;
      bool1 = l1 < l2;
      if (bool1)
      {
        localObject1 = new android/util/Pair;
        localObject4 = i;
        bool1 = j;
        localObject3 = Boolean.valueOf(bool1);
        ((Pair)localObject1).<init>(localObject4, localObject3);
      }
    }
    for (;;)
    {
      return (Pair)localObject1;
      localObject3 = n();
      l2 = ((h)localObject3).x();
      l1 += l2;
      k = l1;
      boolean bool2 = true;
      AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(bool2);
      try
      {
        localObject1 = i();
        localObject1 = AdvertisingIdClient.getAdvertisingIdInfo((Context)localObject1);
        localObject4 = ((AdvertisingIdClient.Info)localObject1).getId();
        i = ((String)localObject4);
        bool2 = ((AdvertisingIdClient.Info)localObject1).isLimitAdTrackingEnabled();
        j = bool2;
      }
      finally
      {
        for (;;)
        {
          localObject4 = l().s();
          localObject3 = "Unable to get advertising id";
          ((t.a)localObject4).a((String)localObject3, localObject2);
          String str = "";
          i = str;
        }
      }
      bool2 = false;
      AdvertisingIdClient.setShouldSkipGmsCoreVersionCheck(false);
      localObject1 = new android/util/Pair;
      localObject4 = i;
      bool1 = j;
      localObject3 = Boolean.valueOf(bool1);
      ((Pair)localObject1).<init>(localObject4, localObject3);
    }
  }
  
  void b(boolean paramBoolean)
  {
    e();
    Object localObject = l().t();
    Boolean localBoolean = Boolean.valueOf(paramBoolean);
    ((t.a)localObject).a("Setting measurementEnabled", localBoolean);
    localObject = s().edit();
    ((SharedPreferences.Editor)localObject).putBoolean("measurement_enabled", paramBoolean);
    ((SharedPreferences.Editor)localObject).apply();
  }
  
  String o()
  {
    int m = 1;
    Object localObject = (String)bfirst;
    MessageDigest localMessageDigest = a("MD5");
    if (localMessageDigest == null) {}
    Locale localLocale;
    String str;
    Object[] arrayOfObject;
    for (localObject = null;; localObject = String.format(localLocale, str, arrayOfObject))
    {
      return (String)localObject;
      localLocale = Locale.US;
      str = "%032X";
      arrayOfObject = new Object[m];
      BigInteger localBigInteger = new java/math/BigInteger;
      localObject = ((String)localObject).getBytes();
      localObject = localMessageDigest.digest((byte[])localObject);
      localBigInteger.<init>(m, (byte[])localObject);
      arrayOfObject[0] = localBigInteger;
    }
  }
  
  String p()
  {
    return UUID.randomUUID().toString().replaceAll("-", "");
  }
  
  Boolean q()
  {
    e();
    Object localObject = s();
    String str = "use_service";
    boolean bool = ((SharedPreferences)localObject).contains(str);
    if (!bool) {
      bool = false;
    }
    for (localObject = null;; localObject = Boolean.valueOf(bool))
    {
      return (Boolean)localObject;
      localObject = s();
      str = "use_service";
      bool = ((SharedPreferences)localObject).getBoolean(str, false);
    }
  }
  
  boolean r()
  {
    e();
    return s().getBoolean("measurement_enabled", true);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/w.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */