package com.google.android.gms.measurement.internal;

abstract class ab
  extends aa
{
  private boolean a;
  private boolean b;
  private boolean c;
  
  ab(y paramy)
  {
    super(paramy);
    g.a(this);
  }
  
  protected abstract void a();
  
  boolean w()
  {
    boolean bool = a;
    if (bool)
    {
      bool = b;
      if (bool) {}
    }
    for (bool = true;; bool = false) {
      return bool;
    }
  }
  
  boolean x()
  {
    return c;
  }
  
  protected void y()
  {
    boolean bool = w();
    if (!bool)
    {
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Not initialized");
      throw localIllegalStateException;
    }
  }
  
  public final void z()
  {
    boolean bool = a;
    if (bool)
    {
      IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
      localIllegalStateException.<init>("Can't initialize twice");
      throw localIllegalStateException;
    }
    a();
    g.y();
    a = true;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/ab.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */