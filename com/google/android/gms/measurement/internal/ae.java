package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.e;
import com.google.android.gms.measurement.AppMeasurementService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ae
  extends ab
{
  private final ae.a a;
  private q b;
  private Boolean c;
  private final j d;
  private final b e;
  private final List f;
  private final j h;
  
  protected ae(y paramy)
  {
    super(paramy);
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    f = ((List)localObject);
    localObject = new com/google/android/gms/measurement/internal/b;
    e locale = paramy.o();
    ((b)localObject).<init>(locale);
    e = ((b)localObject);
    localObject = new com/google/android/gms/measurement/internal/ae$a;
    ((ae.a)localObject).<init>(this);
    a = ((ae.a)localObject);
    localObject = new com/google/android/gms/measurement/internal/ae$1;
    ((ae.1)localObject).<init>(this, paramy);
    d = ((j)localObject);
    localObject = new com/google/android/gms/measurement/internal/ae$2;
    ((ae.2)localObject).<init>(this, paramy);
    h = ((j)localObject);
  }
  
  private void A()
  {
    e();
    s();
  }
  
  private void B()
  {
    e();
    Object localObject1 = l().t();
    int i = f.size();
    Object localObject2 = Integer.valueOf(i);
    ((t.a)localObject1).a("Processing queued up service tasks", localObject2);
    localObject1 = f;
    Iterator localIterator = ((List)localObject1).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject1 = (Runnable)localIterator.next();
      localObject2 = k();
      ((x)localObject2).a((Runnable)localObject1);
    }
    f.clear();
    h.c();
  }
  
  private void a(ComponentName paramComponentName)
  {
    e();
    Object localObject = b;
    if (localObject != null)
    {
      b = null;
      localObject = l().t();
      String str = "Disconnected from device MeasurementService";
      ((t.a)localObject).a(str, paramComponentName);
      A();
    }
  }
  
  private void a(q paramq)
  {
    e();
    p.a(paramq);
    b = paramq;
    r();
    B();
  }
  
  private void a(Runnable paramRunnable)
  {
    e();
    boolean bool = b();
    if (bool) {
      paramRunnable.run();
    }
    for (;;)
    {
      return;
      Object localObject = f;
      long l1 = ((List)localObject).size();
      h localh = n();
      long l2 = localh.G();
      bool = l1 < l2;
      if (!bool)
      {
        localObject = l().b();
        String str = "Discarding data. Max runnable queue size reached";
        ((t.a)localObject).a(str);
      }
      else
      {
        f.add(paramRunnable);
        localObject = g;
        bool = ((y)localObject).v();
        if (!bool)
        {
          localObject = h;
          l2 = 60000L;
          ((j)localObject).a(l2);
        }
        s();
      }
    }
  }
  
  private void r()
  {
    e();
    e.a();
    Object localObject = g;
    boolean bool = ((y)localObject).v();
    if (!bool)
    {
      localObject = d;
      h localh = n();
      long l = localh.y();
      ((j)localObject).a(l);
    }
  }
  
  private void s()
  {
    e();
    y();
    boolean bool1 = b();
    if (bool1) {}
    for (;;)
    {
      return;
      Object localObject1 = c;
      Object localObject2;
      if (localObject1 == null)
      {
        localObject1 = m().q();
        c = ((Boolean)localObject1);
        localObject1 = c;
        if (localObject1 == null)
        {
          l().t().a("State of service unknown");
          bool1 = u();
          localObject1 = Boolean.valueOf(bool1);
          c = ((Boolean)localObject1);
          localObject1 = m();
          localObject2 = c;
          boolean bool2 = ((Boolean)localObject2).booleanValue();
          ((w)localObject1).a(bool2);
        }
      }
      localObject1 = c;
      bool1 = ((Boolean)localObject1).booleanValue();
      if (bool1)
      {
        localObject1 = l().t();
        localObject2 = "Using measurement service";
        ((t.a)localObject1).a((String)localObject2);
        localObject1 = a;
        ((ae.a)localObject1).a();
      }
      else
      {
        bool1 = t();
        if (bool1)
        {
          localObject1 = g;
          bool1 = ((y)localObject1).v();
          if (!bool1)
          {
            l().t().a("Using local app measurement service");
            localObject1 = new android/content/Intent;
            ((Intent)localObject1).<init>("com.google.android.gms.measurement.START");
            localObject2 = new android/content/ComponentName;
            Context localContext = i();
            Class localClass = AppMeasurementService.class;
            ((ComponentName)localObject2).<init>(localContext, localClass);
            ((Intent)localObject1).setComponent((ComponentName)localObject2);
            localObject2 = a;
            ((ae.a)localObject2).a((Intent)localObject1);
            continue;
          }
        }
        localObject1 = n();
        bool1 = ((h)localObject1).D();
        if (bool1)
        {
          l().t().a("Using direct local measurement implementation");
          localObject1 = new com/google/android/gms/measurement/internal/z;
          localObject2 = g;
          boolean bool3 = true;
          ((z)localObject1).<init>((y)localObject2, bool3);
          a((q)localObject1);
        }
        else
        {
          localObject1 = l().b();
          localObject2 = "Not in main process. Unable to use local measurement implementation. Please register the AppMeasurementService service in the app manifest";
          ((t.a)localObject1).a((String)localObject2);
        }
      }
    }
  }
  
  private boolean t()
  {
    Object localObject = i().getPackageManager();
    Intent localIntent = new android/content/Intent;
    Context localContext = i();
    Class localClass = AppMeasurementService.class;
    localIntent.<init>(localContext, localClass);
    int i = 65536;
    localObject = ((PackageManager)localObject).queryIntentServices(localIntent, i);
    int j;
    if (localObject != null)
    {
      j = ((List)localObject).size();
      if (j > 0) {
        j = 1;
      }
    }
    for (;;)
    {
      return j;
      int k = 0;
      localObject = null;
    }
  }
  
  private boolean u()
  {
    boolean bool1 = true;
    t.a locala = null;
    e();
    y();
    Object localObject1 = n();
    boolean bool2 = ((h)localObject1).C();
    if (bool2) {}
    for (;;)
    {
      return bool1;
      localObject1 = new android/content/Intent;
      ((Intent)localObject1).<init>("com.google.android.gms.measurement.START");
      Object localObject2 = new android/content/ComponentName;
      ((ComponentName)localObject2).<init>("com.google.android.gms", "com.google.android.gms.measurement.service.MeasurementBrokerService");
      ((Intent)localObject1).setComponent((ComponentName)localObject2);
      localObject2 = com.google.android.gms.common.stats.b.a();
      l().t().a("Checking service availability");
      Context localContext = i();
      ae.7 local7 = new com/google/android/gms/measurement/internal/ae$7;
      local7.<init>(this);
      bool2 = ((com.google.android.gms.common.stats.b)localObject2).a(localContext, (Intent)localObject1, local7, 0);
      if (bool2)
      {
        locala = l().t();
        localObject1 = "Service available";
        locala.a((String)localObject1);
      }
      else
      {
        bool1 = false;
      }
    }
  }
  
  private void v()
  {
    e();
    boolean bool = b();
    if (!bool) {}
    for (;;)
    {
      return;
      t.a locala = l().t();
      String str = "Inactivity, disconnecting from AppMeasurementService";
      locala.a(str);
      q();
    }
  }
  
  protected void a() {}
  
  protected void a(EventParcel paramEventParcel, String paramString)
  {
    p.a(paramEventParcel);
    e();
    y();
    ae.4 local4 = new com/google/android/gms/measurement/internal/ae$4;
    local4.<init>(this, paramString, paramEventParcel);
    a(local4);
  }
  
  protected void a(UserAttributeParcel paramUserAttributeParcel)
  {
    e();
    y();
    ae.5 local5 = new com/google/android/gms/measurement/internal/ae$5;
    local5.<init>(this, paramUserAttributeParcel);
    a(local5);
  }
  
  public boolean b()
  {
    e();
    y();
    q localq = b;
    boolean bool;
    if (localq != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localq = null;
    }
  }
  
  protected void o()
  {
    e();
    y();
    ae.3 local3 = new com/google/android/gms/measurement/internal/ae$3;
    local3.<init>(this);
    a(local3);
  }
  
  protected void p()
  {
    e();
    y();
    ae.6 local6 = new com/google/android/gms/measurement/internal/ae$6;
    local6.<init>(this);
    a(local6);
  }
  
  public void q()
  {
    e();
    y();
    try
    {
      com.google.android.gms.common.stats.b localb = com.google.android.gms.common.stats.b.a();
      Context localContext = i();
      ae.a locala = a;
      localb.a(localContext, locala);
    }
    catch (IllegalStateException localIllegalStateException)
    {
      for (;;) {}
    }
    catch (IllegalArgumentException localIllegalArgumentException)
    {
      for (;;) {}
    }
    b = null;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/ae.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */