package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class e
  implements Parcelable.Creator
{
  static void a(UserAttributeParcel paramUserAttributeParcel, Parcel paramParcel, int paramInt)
  {
    int i = a.a(paramParcel);
    int j = a;
    a.a(paramParcel, 1, j);
    Object localObject = b;
    a.a(paramParcel, 2, (String)localObject, false);
    long l = c;
    a.a(paramParcel, 3, l);
    localObject = d;
    a.a(paramParcel, 4, (Long)localObject, false);
    localObject = e;
    a.a(paramParcel, 5, (Float)localObject, false);
    localObject = f;
    a.a(paramParcel, 6, (String)localObject, false);
    localObject = g;
    a.a(paramParcel, 7, (String)localObject, false);
    a.a(paramParcel, i);
  }
  
  public UserAttributeParcel a(Parcel paramParcel)
  {
    String str1 = null;
    int i = zza.b(paramParcel);
    int j = 0;
    StringBuilder localStringBuilder = null;
    long l = 0L;
    String str2 = null;
    Float localFloat = null;
    Long localLong = null;
    String str3 = null;
    for (;;)
    {
      k = paramParcel.dataPosition();
      if (k >= i) {
        break;
      }
      k = zza.a(paramParcel);
      int m = zza.a(k);
      switch (m)
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        j = zza.d(paramParcel, k);
        break;
      case 2: 
        str3 = zza.h(paramParcel, k);
        break;
      case 3: 
        l = zza.e(paramParcel, k);
        break;
      case 4: 
        localLong = zza.f(paramParcel, k);
        break;
      case 5: 
        localFloat = zza.g(paramParcel, k);
        break;
      case 6: 
        str2 = zza.h(paramParcel, k);
        break;
      case 7: 
        str1 = zza.h(paramParcel, k);
      }
    }
    int k = paramParcel.dataPosition();
    if (k != i)
    {
      localObject = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      String str4 = "Overread allowed size end=" + i;
      ((zza.zza)localObject).<init>(str4, paramParcel);
      throw ((Throwable)localObject);
    }
    Object localObject = new com/google/android/gms/measurement/internal/UserAttributeParcel;
    ((UserAttributeParcel)localObject).<init>(j, str3, l, localLong, localFloat, str2, str1);
    return (UserAttributeParcel)localObject;
  }
  
  public UserAttributeParcel[] a(int paramInt)
  {
    return new UserAttributeParcel[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/e.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */