package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.p;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.Semaphore;

public class x
  extends ab
{
  private x.c a;
  private x.c b;
  private final BlockingQueue c;
  private final BlockingQueue d;
  private final Thread.UncaughtExceptionHandler e;
  private final Thread.UncaughtExceptionHandler f;
  private final Object h;
  private final Semaphore i;
  private volatile boolean j;
  
  x(y paramy)
  {
    super(paramy);
    Object localObject = new java/lang/Object;
    localObject.<init>();
    h = localObject;
    localObject = new java/util/concurrent/Semaphore;
    ((Semaphore)localObject).<init>(2);
    i = ((Semaphore)localObject);
    localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>();
    c = ((BlockingQueue)localObject);
    localObject = new java/util/concurrent/LinkedBlockingQueue;
    ((LinkedBlockingQueue)localObject).<init>();
    d = ((BlockingQueue)localObject);
    localObject = new com/google/android/gms/measurement/internal/x$b;
    ((x.b)localObject).<init>(this, "Thread death: Uncaught exception on worker thread");
    e = ((Thread.UncaughtExceptionHandler)localObject);
    localObject = new com/google/android/gms/measurement/internal/x$b;
    ((x.b)localObject).<init>(this, "Thread death: Uncaught exception on network thread");
    f = ((Thread.UncaughtExceptionHandler)localObject);
  }
  
  private void a(FutureTask paramFutureTask)
  {
    synchronized (h)
    {
      Object localObject2 = c;
      ((BlockingQueue)localObject2).add(paramFutureTask);
      localObject2 = a;
      if (localObject2 == null)
      {
        localObject2 = new com/google/android/gms/measurement/internal/x$c;
        Object localObject4 = "Measurement Worker";
        BlockingQueue localBlockingQueue = c;
        ((x.c)localObject2).<init>(this, (String)localObject4, localBlockingQueue);
        a = ((x.c)localObject2);
        localObject2 = a;
        localObject4 = e;
        ((x.c)localObject2).setUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)localObject4);
        localObject2 = a;
        ((x.c)localObject2).start();
        return;
      }
      localObject2 = a;
      ((x.c)localObject2).a();
    }
  }
  
  private void b(FutureTask paramFutureTask)
  {
    synchronized (h)
    {
      Object localObject2 = d;
      ((BlockingQueue)localObject2).add(paramFutureTask);
      localObject2 = b;
      if (localObject2 == null)
      {
        localObject2 = new com/google/android/gms/measurement/internal/x$c;
        Object localObject4 = "Measurement Network";
        BlockingQueue localBlockingQueue = d;
        ((x.c)localObject2).<init>(this, (String)localObject4, localBlockingQueue);
        b = ((x.c)localObject2);
        localObject2 = b;
        localObject4 = f;
        ((x.c)localObject2).setUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)localObject4);
        localObject2 = b;
        ((x.c)localObject2).start();
        return;
      }
      localObject2 = b;
      ((x.c)localObject2).a();
    }
  }
  
  protected void a() {}
  
  public void a(Runnable paramRunnable)
  {
    y();
    p.a(paramRunnable);
    x.a locala = new com/google/android/gms/measurement/internal/x$a;
    locala.<init>(this, paramRunnable, "Task exception on worker thread");
    a(locala);
  }
  
  public void b(Runnable paramRunnable)
  {
    y();
    p.a(paramRunnable);
    x.a locala = new com/google/android/gms/measurement/internal/x$a;
    locala.<init>(this, paramRunnable, "Task exception on network thread");
    b(locala);
  }
  
  public void d()
  {
    Object localObject = Thread.currentThread();
    x.c localc = b;
    if (localObject != localc)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Call expected from network thread");
      throw ((Throwable)localObject);
    }
  }
  
  public void e()
  {
    Object localObject = Thread.currentThread();
    x.c localc = a;
    if (localObject != localc)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Call expected from worker thread");
      throw ((Throwable)localObject);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/x.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */