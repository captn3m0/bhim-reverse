package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import com.google.android.gms.common.internal.p;

class a
{
  final String a;
  final String b;
  final String c;
  final String d;
  final long e;
  final long f;
  final String g;
  final String h;
  final long i;
  final long j;
  final boolean k;
  
  a(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong1, long paramLong2, String paramString5, String paramString6, long paramLong3, long paramLong4, boolean paramBoolean)
  {
    p.a(paramString1);
    long l = 0L;
    boolean bool = paramLong1 < l;
    if (!bool) {}
    for (bool = true;; bool = false)
    {
      p.b(bool);
      a = paramString1;
      b = paramString2;
      bool = TextUtils.isEmpty(paramString3);
      if (bool) {
        paramString3 = null;
      }
      c = paramString3;
      d = paramString4;
      e = paramLong1;
      f = paramLong2;
      g = paramString5;
      h = paramString6;
      i = paramLong3;
      j = paramLong4;
      k = paramBoolean;
      return;
    }
  }
  
  public a a(long paramLong)
  {
    a locala = new com/google/android/gms/measurement/internal/a;
    String str1 = a;
    String str2 = b;
    String str3 = c;
    String str4 = d;
    long l1 = e;
    long l2 = f;
    String str5 = g;
    String str6 = h;
    long l3 = i;
    boolean bool = k;
    locala.<init>(str1, str2, str3, str4, l1, l2, str5, str6, l3, paramLong, bool);
    return locala;
  }
  
  public a a(t paramt, long paramLong)
  {
    p.a(paramt);
    long l1 = e;
    long l2 = 1L;
    long l3 = l1 + l2;
    l1 = 2147483647L;
    boolean bool1 = l3 < l1;
    if (bool1)
    {
      t.a locala = paramt.o();
      localObject = "Bundle index overflow";
      locala.a((String)localObject);
      l3 = 0L;
    }
    Object localObject = new com/google/android/gms/measurement/internal/a;
    String str1 = a;
    String str2 = b;
    String str3 = c;
    String str4 = d;
    String str5 = g;
    String str6 = h;
    long l4 = i;
    long l5 = j;
    boolean bool2 = k;
    ((a)localObject).<init>(str1, str2, str3, str4, l3, paramLong, str5, str6, l4, l5, bool2);
    return (a)localObject;
  }
  
  public a a(String paramString, long paramLong)
  {
    a locala = new com/google/android/gms/measurement/internal/a;
    String str1 = a;
    String str2 = b;
    String str3 = d;
    long l1 = e;
    long l2 = f;
    String str4 = g;
    String str5 = h;
    long l3 = j;
    boolean bool = k;
    locala.<init>(str1, str2, paramString, str3, l1, l2, str4, str5, paramLong, l3, bool);
    return locala;
  }
  
  public a a(String paramString1, String paramString2)
  {
    a locala = new com/google/android/gms/measurement/internal/a;
    String str1 = a;
    String str2 = c;
    long l1 = e;
    long l2 = f;
    String str3 = g;
    String str4 = h;
    long l3 = i;
    long l4 = j;
    boolean bool = k;
    locala.<init>(str1, paramString1, str2, paramString2, l1, l2, str3, str4, l3, l4, bool);
    return locala;
  }
  
  public a a(boolean paramBoolean)
  {
    a locala = new com/google/android/gms/measurement/internal/a;
    String str1 = a;
    String str2 = b;
    String str3 = c;
    String str4 = d;
    long l1 = e;
    long l2 = f;
    String str5 = g;
    String str6 = h;
    long l3 = i;
    long l4 = j;
    locala.<init>(str1, str2, str3, str4, l1, l2, str5, str6, l3, l4, paramBoolean);
    return locala;
  }
  
  public a b(String paramString1, String paramString2)
  {
    a locala = new com/google/android/gms/measurement/internal/a;
    String str1 = a;
    String str2 = b;
    String str3 = c;
    String str4 = d;
    long l1 = e;
    long l2 = f;
    long l3 = i;
    long l4 = j;
    boolean bool = k;
    locala.<init>(str1, str2, str3, str4, l1, l2, paramString1, paramString2, l3, l4, bool);
    return locala;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */