package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Looper;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.c.b;
import com.google.android.gms.common.api.c.c;
import com.google.android.gms.common.internal.d;
import com.google.android.gms.common.internal.p;

public class ae$a
  implements ServiceConnection, c.b, c.c
{
  private volatile boolean b;
  private volatile s c;
  
  protected ae$a(ae paramae) {}
  
  public void a()
  {
    a.e();
    Object localObject1 = a;
    Object localObject4 = ((ae)localObject1).i();
    for (;;)
    {
      try
      {
        bool = b;
        if (bool)
        {
          localObject1 = a;
          localObject1 = ((ae)localObject1).l();
          localObject1 = ((t)localObject1).t();
          localObject4 = "Connection attempt already in progress";
          ((t.a)localObject1).a((String)localObject4);
          return;
        }
        localObject1 = c;
        if (localObject1 != null)
        {
          localObject1 = a;
          localObject1 = ((ae)localObject1).l();
          localObject1 = ((t)localObject1).t();
          localObject4 = "Already awaiting connection attempt";
          ((t.a)localObject1).a((String)localObject4);
          continue;
        }
        localObject3 = new com/google/android/gms/measurement/internal/s;
      }
      finally {}
      Looper localLooper = Looper.getMainLooper();
      d locald = d.a((Context)localObject4);
      ((s)localObject3).<init>((Context)localObject4, localLooper, locald, this, this);
      c = ((s)localObject3);
      Object localObject3 = a;
      localObject3 = ((ae)localObject3).l();
      localObject3 = ((t)localObject3).t();
      localObject4 = "Connecting to remote service";
      ((t.a)localObject3).a((String)localObject4);
      boolean bool = true;
      b = bool;
      localObject3 = c;
      ((s)localObject3).e();
    }
  }
  
  public void a(int paramInt)
  {
    p.b("MeasurementServiceConnection.onConnectionSuspended");
    a.l().s().a("Service connection suspended");
    x localx = a.k();
    ae.a.4 local4 = new com/google/android/gms/measurement/internal/ae$a$4;
    local4.<init>(this);
    localx.a(local4);
  }
  
  /* Error */
  public void a(android.content.Intent paramIntent)
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   4: invokevirtual 27	com/google/android/gms/measurement/internal/ae:e	()V
    //   7: aload_0
    //   8: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   11: invokevirtual 31	com/google/android/gms/measurement/internal/ae:i	()Landroid/content/Context;
    //   14: astore_2
    //   15: invokestatic 104	com/google/android/gms/common/stats/b:a	()Lcom/google/android/gms/common/stats/b;
    //   18: astore_3
    //   19: aload_0
    //   20: monitorenter
    //   21: aload_0
    //   22: getfield 33	com/google/android/gms/measurement/internal/ae$a:b	Z
    //   25: istore 4
    //   27: iload 4
    //   29: ifeq +29 -> 58
    //   32: aload_0
    //   33: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   36: astore_2
    //   37: aload_2
    //   38: invokevirtual 37	com/google/android/gms/measurement/internal/ae:l	()Lcom/google/android/gms/measurement/internal/t;
    //   41: astore_2
    //   42: aload_2
    //   43: invokevirtual 43	com/google/android/gms/measurement/internal/t:t	()Lcom/google/android/gms/measurement/internal/t$a;
    //   46: astore_2
    //   47: ldc 45
    //   49: astore_3
    //   50: aload_2
    //   51: aload_3
    //   52: invokevirtual 50	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   55: aload_0
    //   56: monitorexit
    //   57: return
    //   58: iconst_1
    //   59: istore 4
    //   61: aload_0
    //   62: iload 4
    //   64: putfield 33	com/google/android/gms/measurement/internal/ae$a:b	Z
    //   67: aload_0
    //   68: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   71: astore 5
    //   73: aload 5
    //   75: invokestatic 107	com/google/android/gms/measurement/internal/ae:a	(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/ae$a;
    //   78: astore 5
    //   80: sipush 129
    //   83: istore 6
    //   85: aload_3
    //   86: aload_2
    //   87: aload_1
    //   88: aload 5
    //   90: iload 6
    //   92: invokevirtual 111	com/google/android/gms/common/stats/b:a	(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    //   95: pop
    //   96: aload_0
    //   97: monitorexit
    //   98: goto -41 -> 57
    //   101: astore_2
    //   102: aload_0
    //   103: monitorexit
    //   104: aload_2
    //   105: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	106	0	this	a
    //   0	106	1	paramIntent	android.content.Intent
    //   14	73	2	localObject1	Object
    //   101	4	2	localObject2	Object
    //   18	68	3	localObject3	Object
    //   25	38	4	bool	boolean
    //   71	18	5	localObject4	Object
    //   83	8	6	i	int
    // Exception table:
    //   from	to	target	type
    //   21	25	101	finally
    //   32	36	101	finally
    //   37	41	101	finally
    //   42	46	101	finally
    //   51	55	101	finally
    //   55	57	101	finally
    //   62	67	101	finally
    //   67	71	101	finally
    //   73	78	101	finally
    //   90	96	101	finally
    //   96	98	101	finally
    //   102	104	101	finally
  }
  
  /* Error */
  public void a(android.os.Bundle paramBundle)
  {
    // Byte code:
    //   0: ldc 113
    //   2: invokestatic 80	com/google/android/gms/common/internal/p:b	(Ljava/lang/String;)V
    //   5: aload_0
    //   6: monitorenter
    //   7: aconst_null
    //   8: astore_2
    //   9: aload_0
    //   10: iconst_0
    //   11: putfield 33	com/google/android/gms/measurement/internal/ae$a:b	Z
    //   14: aload_0
    //   15: getfield 52	com/google/android/gms/measurement/internal/ae$a:c	Lcom/google/android/gms/measurement/internal/s;
    //   18: astore_2
    //   19: aload_2
    //   20: invokevirtual 116	com/google/android/gms/measurement/internal/s:l	()Landroid/os/IInterface;
    //   23: astore_2
    //   24: aload_2
    //   25: checkcast 118	com/google/android/gms/measurement/internal/q
    //   28: astore_2
    //   29: aconst_null
    //   30: astore_3
    //   31: aload_0
    //   32: aconst_null
    //   33: putfield 52	com/google/android/gms/measurement/internal/ae$a:c	Lcom/google/android/gms/measurement/internal/s;
    //   36: aload_0
    //   37: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   40: astore_3
    //   41: aload_3
    //   42: invokevirtual 89	com/google/android/gms/measurement/internal/ae:k	()Lcom/google/android/gms/measurement/internal/x;
    //   45: astore_3
    //   46: new 120	com/google/android/gms/measurement/internal/ae$a$3
    //   49: astore 4
    //   51: aload 4
    //   53: aload_0
    //   54: aload_2
    //   55: invokespecial 123	com/google/android/gms/measurement/internal/ae$a$3:<init>	(Lcom/google/android/gms/measurement/internal/ae$a;Lcom/google/android/gms/measurement/internal/q;)V
    //   58: aload_3
    //   59: aload 4
    //   61: invokevirtual 99	com/google/android/gms/measurement/internal/x:a	(Ljava/lang/Runnable;)V
    //   64: aload_0
    //   65: monitorexit
    //   66: return
    //   67: astore_2
    //   68: aconst_null
    //   69: astore_2
    //   70: aload_0
    //   71: aconst_null
    //   72: putfield 52	com/google/android/gms/measurement/internal/ae$a:c	Lcom/google/android/gms/measurement/internal/s;
    //   75: goto -11 -> 64
    //   78: astore_2
    //   79: aload_0
    //   80: monitorexit
    //   81: aload_2
    //   82: athrow
    //   83: astore_2
    //   84: goto -16 -> 68
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	87	0	this	a
    //   0	87	1	paramBundle	android.os.Bundle
    //   8	47	2	localObject1	Object
    //   67	1	2	localIllegalStateException	IllegalStateException
    //   69	1	2	localObject2	Object
    //   78	4	2	localObject3	Object
    //   83	1	2	localDeadObjectException	android.os.DeadObjectException
    //   30	29	3	localObject4	Object
    //   49	11	4	local3	ae.a.3
    // Exception table:
    //   from	to	target	type
    //   14	18	67	java/lang/IllegalStateException
    //   19	23	67	java/lang/IllegalStateException
    //   24	28	67	java/lang/IllegalStateException
    //   32	36	67	java/lang/IllegalStateException
    //   36	40	67	java/lang/IllegalStateException
    //   41	45	67	java/lang/IllegalStateException
    //   46	49	67	java/lang/IllegalStateException
    //   54	58	67	java/lang/IllegalStateException
    //   59	64	67	java/lang/IllegalStateException
    //   10	14	78	finally
    //   14	18	78	finally
    //   19	23	78	finally
    //   24	28	78	finally
    //   32	36	78	finally
    //   36	40	78	finally
    //   41	45	78	finally
    //   46	49	78	finally
    //   54	58	78	finally
    //   59	64	78	finally
    //   64	66	78	finally
    //   71	75	78	finally
    //   79	81	78	finally
    //   14	18	83	android/os/DeadObjectException
    //   19	23	83	android/os/DeadObjectException
    //   24	28	83	android/os/DeadObjectException
    //   32	36	83	android/os/DeadObjectException
    //   36	40	83	android/os/DeadObjectException
    //   41	45	83	android/os/DeadObjectException
    //   46	49	83	android/os/DeadObjectException
    //   54	58	83	android/os/DeadObjectException
    //   59	64	83	android/os/DeadObjectException
  }
  
  public void a(ConnectionResult paramConnectionResult)
  {
    p.b("MeasurementServiceConnection.onConnectionFailed");
    t.a locala = a.l().o();
    String str = "Service connection failed";
    locala.a(str, paramConnectionResult);
    locala = null;
    try
    {
      b = false;
      locala = null;
      c = null;
      return;
    }
    finally {}
  }
  
  /* Error */
  public void onServiceConnected(ComponentName paramComponentName, android.os.IBinder paramIBinder)
  {
    // Byte code:
    //   0: ldc -117
    //   2: invokestatic 80	com/google/android/gms/common/internal/p:b	(Ljava/lang/String;)V
    //   5: aload_0
    //   6: monitorenter
    //   7: aconst_null
    //   8: astore_3
    //   9: aload_0
    //   10: iconst_0
    //   11: putfield 33	com/google/android/gms/measurement/internal/ae$a:b	Z
    //   14: aload_2
    //   15: ifnonnull +31 -> 46
    //   18: aload_0
    //   19: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   22: astore_3
    //   23: aload_3
    //   24: invokevirtual 37	com/google/android/gms/measurement/internal/ae:l	()Lcom/google/android/gms/measurement/internal/t;
    //   27: astore_3
    //   28: aload_3
    //   29: invokevirtual 141	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   32: astore_3
    //   33: ldc -113
    //   35: astore 4
    //   37: aload_3
    //   38: aload 4
    //   40: invokevirtual 50	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   43: aload_0
    //   44: monitorexit
    //   45: return
    //   46: aconst_null
    //   47: astore_3
    //   48: aload_2
    //   49: invokeinterface 149 1 0
    //   54: astore 4
    //   56: ldc -105
    //   58: astore 5
    //   60: aload 5
    //   62: aload 4
    //   64: invokevirtual 157	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   67: istore 6
    //   69: iload 6
    //   71: ifeq +91 -> 162
    //   74: aload_2
    //   75: invokestatic 162	com/google/android/gms/measurement/internal/q$a:a	(Landroid/os/IBinder;)Lcom/google/android/gms/measurement/internal/q;
    //   78: astore_3
    //   79: aload_0
    //   80: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   83: astore 4
    //   85: aload 4
    //   87: invokevirtual 37	com/google/android/gms/measurement/internal/ae:l	()Lcom/google/android/gms/measurement/internal/t;
    //   90: astore 4
    //   92: aload 4
    //   94: invokevirtual 43	com/google/android/gms/measurement/internal/t:t	()Lcom/google/android/gms/measurement/internal/t$a;
    //   97: astore 4
    //   99: ldc -92
    //   101: astore 5
    //   103: aload 4
    //   105: aload 5
    //   107: invokevirtual 50	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   110: aload_3
    //   111: ifnonnull +123 -> 234
    //   114: invokestatic 104	com/google/android/gms/common/stats/b:a	()Lcom/google/android/gms/common/stats/b;
    //   117: astore_3
    //   118: aload_0
    //   119: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   122: astore 4
    //   124: aload 4
    //   126: invokevirtual 31	com/google/android/gms/measurement/internal/ae:i	()Landroid/content/Context;
    //   129: astore 4
    //   131: aload_0
    //   132: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   135: astore 5
    //   137: aload 5
    //   139: invokestatic 107	com/google/android/gms/measurement/internal/ae:a	(Lcom/google/android/gms/measurement/internal/ae;)Lcom/google/android/gms/measurement/internal/ae$a;
    //   142: astore 5
    //   144: aload_3
    //   145: aload 4
    //   147: aload 5
    //   149: invokevirtual 167	com/google/android/gms/common/stats/b:a	(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    //   152: aload_0
    //   153: monitorexit
    //   154: goto -109 -> 45
    //   157: astore_3
    //   158: aload_0
    //   159: monitorexit
    //   160: aload_3
    //   161: athrow
    //   162: aload_0
    //   163: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   166: astore 5
    //   168: aload 5
    //   170: invokevirtual 37	com/google/android/gms/measurement/internal/ae:l	()Lcom/google/android/gms/measurement/internal/t;
    //   173: astore 5
    //   175: aload 5
    //   177: invokevirtual 141	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   180: astore 5
    //   182: ldc -87
    //   184: astore 7
    //   186: aload 5
    //   188: aload 7
    //   190: aload 4
    //   192: invokevirtual 137	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   195: goto -85 -> 110
    //   198: astore 4
    //   200: aload_0
    //   201: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   204: astore 4
    //   206: aload 4
    //   208: invokevirtual 37	com/google/android/gms/measurement/internal/ae:l	()Lcom/google/android/gms/measurement/internal/t;
    //   211: astore 4
    //   213: aload 4
    //   215: invokevirtual 141	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   218: astore 4
    //   220: ldc -85
    //   222: astore 5
    //   224: aload 4
    //   226: aload 5
    //   228: invokevirtual 50	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;)V
    //   231: goto -121 -> 110
    //   234: aload_0
    //   235: getfield 18	com/google/android/gms/measurement/internal/ae$a:a	Lcom/google/android/gms/measurement/internal/ae;
    //   238: astore 4
    //   240: aload 4
    //   242: invokevirtual 89	com/google/android/gms/measurement/internal/ae:k	()Lcom/google/android/gms/measurement/internal/x;
    //   245: astore 4
    //   247: new 173	com/google/android/gms/measurement/internal/ae$a$1
    //   250: astore 5
    //   252: aload 5
    //   254: aload_0
    //   255: aload_3
    //   256: invokespecial 174	com/google/android/gms/measurement/internal/ae$a$1:<init>	(Lcom/google/android/gms/measurement/internal/ae$a;Lcom/google/android/gms/measurement/internal/q;)V
    //   259: aload 4
    //   261: aload 5
    //   263: invokevirtual 99	com/google/android/gms/measurement/internal/x:a	(Ljava/lang/Runnable;)V
    //   266: goto -114 -> 152
    //   269: astore_3
    //   270: goto -118 -> 152
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	273	0	this	a
    //   0	273	1	paramComponentName	ComponentName
    //   0	273	2	paramIBinder	android.os.IBinder
    //   8	137	3	localObject1	Object
    //   157	99	3	localq	q
    //   269	1	3	localIllegalArgumentException	IllegalArgumentException
    //   35	156	4	localObject2	Object
    //   198	1	4	localRemoteException	android.os.RemoteException
    //   204	56	4	localObject3	Object
    //   58	204	5	localObject4	Object
    //   67	3	6	bool	boolean
    //   184	5	7	str	String
    // Exception table:
    //   from	to	target	type
    //   10	14	157	finally
    //   18	22	157	finally
    //   23	27	157	finally
    //   28	32	157	finally
    //   38	43	157	finally
    //   43	45	157	finally
    //   48	54	157	finally
    //   62	67	157	finally
    //   74	78	157	finally
    //   79	83	157	finally
    //   85	90	157	finally
    //   92	97	157	finally
    //   105	110	157	finally
    //   114	117	157	finally
    //   118	122	157	finally
    //   124	129	157	finally
    //   131	135	157	finally
    //   137	142	157	finally
    //   147	152	157	finally
    //   152	154	157	finally
    //   158	160	157	finally
    //   162	166	157	finally
    //   168	173	157	finally
    //   175	180	157	finally
    //   190	195	157	finally
    //   200	204	157	finally
    //   206	211	157	finally
    //   213	218	157	finally
    //   226	231	157	finally
    //   234	238	157	finally
    //   240	245	157	finally
    //   247	250	157	finally
    //   255	259	157	finally
    //   261	266	157	finally
    //   48	54	198	android/os/RemoteException
    //   62	67	198	android/os/RemoteException
    //   74	78	198	android/os/RemoteException
    //   79	83	198	android/os/RemoteException
    //   85	90	198	android/os/RemoteException
    //   92	97	198	android/os/RemoteException
    //   105	110	198	android/os/RemoteException
    //   162	166	198	android/os/RemoteException
    //   168	173	198	android/os/RemoteException
    //   175	180	198	android/os/RemoteException
    //   190	195	198	android/os/RemoteException
    //   114	117	269	java/lang/IllegalArgumentException
    //   118	122	269	java/lang/IllegalArgumentException
    //   124	129	269	java/lang/IllegalArgumentException
    //   131	135	269	java/lang/IllegalArgumentException
    //   137	142	269	java/lang/IllegalArgumentException
    //   147	152	269	java/lang/IllegalArgumentException
  }
  
  public void onServiceDisconnected(ComponentName paramComponentName)
  {
    p.b("MeasurementServiceConnection.onServiceDisconnected");
    a.l().s().a("Service disconnected");
    x localx = a.k();
    ae.a.2 local2 = new com/google/android/gms/measurement/internal/ae$a$2;
    local2.<init>(this, paramComponentName);
    localx.a(local2);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/ae$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */