package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.p;
import java.util.Iterator;
import java.util.Set;

public class l
{
  final String a;
  final String b;
  final String c;
  final long d;
  final long e;
  final EventParams f;
  
  l(y paramy, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, Bundle paramBundle)
  {
    p.a(paramString2);
    p.a(paramString3);
    a = paramString2;
    b = paramString3;
    boolean bool = TextUtils.isEmpty(paramString1);
    if (bool) {
      paramString1 = null;
    }
    c = paramString1;
    d = paramLong1;
    e = paramLong2;
    long l1 = e;
    long l2 = 0L;
    bool = l1 < l2;
    if (bool)
    {
      l1 = e;
      l2 = d;
      bool = l1 < l2;
      if (bool)
      {
        localObject = paramy.f().o();
        String str = "Event created with reverse previous/current timestamps";
        ((t.a)localObject).a(str);
      }
    }
    Object localObject = a(paramy, paramBundle);
    f = ((EventParams)localObject);
  }
  
  private l(y paramy, String paramString1, String paramString2, String paramString3, long paramLong1, long paramLong2, EventParams paramEventParams)
  {
    p.a(paramString2);
    p.a(paramString3);
    p.a(paramEventParams);
    a = paramString2;
    b = paramString3;
    boolean bool = TextUtils.isEmpty(paramString1);
    if (bool) {
      paramString1 = null;
    }
    c = paramString1;
    d = paramLong1;
    e = paramLong2;
    long l1 = e;
    long l2 = 0L;
    bool = l1 < l2;
    if (bool)
    {
      l1 = e;
      l2 = d;
      bool = l1 < l2;
      if (bool)
      {
        t.a locala = paramy.f().o();
        String str = "Event created with reverse previous/current timestamps";
        locala.a(str);
      }
    }
    f = paramEventParams;
  }
  
  private EventParams a(y paramy, Bundle paramBundle)
  {
    Bundle localBundle;
    Object localObject1;
    if (paramBundle != null)
    {
      boolean bool = paramBundle.isEmpty();
      if (!bool)
      {
        localBundle = new android/os/Bundle;
        localBundle.<init>(paramBundle);
        localObject1 = localBundle.keySet();
        Iterator localIterator = ((Set)localObject1).iterator();
        for (;;)
        {
          bool = localIterator.hasNext();
          if (!bool) {
            break;
          }
          localObject1 = (String)localIterator.next();
          if (localObject1 == null)
          {
            localIterator.remove();
          }
          else
          {
            Object localObject2 = paramy.k();
            Object localObject3 = localBundle.get((String)localObject1);
            localObject2 = ((f)localObject2).a((String)localObject1, localObject3);
            if (localObject2 == null)
            {
              localIterator.remove();
            }
            else
            {
              localObject3 = paramy.k();
              ((f)localObject3).a(localBundle, (String)localObject1, localObject2);
            }
          }
        }
        localObject1 = new com/google/android/gms/measurement/internal/EventParams;
        ((EventParams)localObject1).<init>(localBundle);
      }
    }
    for (;;)
    {
      return (EventParams)localObject1;
      localObject1 = new com/google/android/gms/measurement/internal/EventParams;
      localBundle = new android/os/Bundle;
      localBundle.<init>();
      ((EventParams)localObject1).<init>(localBundle);
    }
  }
  
  l a(y paramy, long paramLong)
  {
    l locall = new com/google/android/gms/measurement/internal/l;
    String str1 = c;
    String str2 = a;
    String str3 = b;
    long l = d;
    EventParams localEventParams = f;
    locall.<init>(paramy, str1, str2, str3, l, paramLong, localEventParams);
    return locall;
  }
  
  public String toString()
  {
    char c1 = '\'';
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append("Event{appId='");
    Object localObject = a;
    localStringBuilder = localStringBuilder.append((String)localObject).append(c1).append(", name='");
    localObject = b;
    localStringBuilder = localStringBuilder.append((String)localObject).append(c1).append(", params=");
    localObject = f;
    return localObject + '}';
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/l.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */