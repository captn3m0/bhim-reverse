package com.google.android.gms.measurement.internal;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.e;
import com.google.android.gms.measurement.AppMeasurementReceiver;
import com.google.android.gms.measurement.AppMeasurementService;

public class c
  extends ab
{
  private boolean a;
  private final AlarmManager b;
  
  protected c(y paramy)
  {
    super(paramy);
    AlarmManager localAlarmManager = (AlarmManager)i().getSystemService("alarm");
    b = localAlarmManager;
  }
  
  private PendingIntent o()
  {
    Intent localIntent = new android/content/Intent;
    Context localContext = i();
    localIntent.<init>(localContext, AppMeasurementReceiver.class);
    localIntent.setAction("com.google.android.gms.measurement.UPLOAD");
    return PendingIntent.getBroadcast(i(), 0, localIntent, 0);
  }
  
  protected void a()
  {
    AlarmManager localAlarmManager = b;
    PendingIntent localPendingIntent = o();
    localAlarmManager.cancel(localPendingIntent);
  }
  
  public void a(long paramLong)
  {
    boolean bool1 = true;
    y();
    long l1 = 0L;
    boolean bool2 = paramLong < l1;
    if (bool2) {
      bool2 = bool1;
    }
    for (;;)
    {
      p.b(bool2);
      p.a(AppMeasurementReceiver.a(i()), "Receiver not registered/enabled");
      p.a(AppMeasurementService.a(i()), "Service not registered/enabled");
      b();
      l1 = h().b() + paramLong;
      a = bool1;
      AlarmManager localAlarmManager = b;
      long l2 = Math.max(n().N(), paramLong);
      PendingIntent localPendingIntent = o();
      localAlarmManager.setInexactRepeating(2, l1, l2, localPendingIntent);
      return;
      bool2 = false;
      localAlarmManager = null;
    }
  }
  
  public void b()
  {
    y();
    a = false;
    AlarmManager localAlarmManager = b;
    PendingIntent localPendingIntent = o();
    localAlarmManager.cancel(localPendingIntent);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/c.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */