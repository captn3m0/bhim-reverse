package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class g
  implements Parcelable.Creator
{
  static void a(AppMetadata paramAppMetadata, Parcel paramParcel, int paramInt)
  {
    int i = a.a(paramParcel);
    int j = a;
    a.a(paramParcel, 1, j);
    String str = b;
    a.a(paramParcel, 2, str, false);
    str = c;
    a.a(paramParcel, 3, str, false);
    str = d;
    a.a(paramParcel, 4, str, false);
    str = e;
    a.a(paramParcel, 5, str, false);
    long l = f;
    a.a(paramParcel, 6, l);
    l = g;
    a.a(paramParcel, 7, l);
    str = h;
    a.a(paramParcel, 8, str, false);
    boolean bool = paramAppMetadata.i;
    a.a(paramParcel, 9, bool);
    a.a(paramParcel, i);
  }
  
  public AppMetadata a(Parcel paramParcel)
  {
    long l1 = 0L;
    boolean bool = false;
    String str1 = null;
    int i = zza.b(paramParcel);
    long l2 = l1;
    String str2 = null;
    String str3 = null;
    String str4 = null;
    Object localObject1 = null;
    int j = 0;
    zza.zza localzza = null;
    for (;;)
    {
      k = paramParcel.dataPosition();
      if (k >= i) {
        break;
      }
      k = zza.a(paramParcel);
      int m = zza.a(k);
      switch (m)
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        j = zza.d(paramParcel, k);
        break;
      case 2: 
        localObject1 = zza.h(paramParcel, k);
        break;
      case 3: 
        str4 = zza.h(paramParcel, k);
        break;
      case 4: 
        str3 = zza.h(paramParcel, k);
        break;
      case 5: 
        str2 = zza.h(paramParcel, k);
        break;
      case 6: 
        l2 = zza.e(paramParcel, k);
        break;
      case 7: 
        l1 = zza.e(paramParcel, k);
        break;
      case 8: 
        str1 = zza.h(paramParcel, k);
        break;
      case 9: 
        bool = zza.c(paramParcel, k);
      }
    }
    int k = paramParcel.dataPosition();
    if (k != i)
    {
      localzza = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject2 = "Overread allowed size end=" + i;
      localzza.<init>((String)localObject2, paramParcel);
      throw localzza;
    }
    Object localObject2 = new com/google/android/gms/measurement/internal/AppMetadata;
    ((AppMetadata)localObject2).<init>(j, (String)localObject1, str4, str3, str2, l2, l1, str1, bool);
    return (AppMetadata)localObject2;
  }
  
  public AppMetadata[] a(int paramInt)
  {
    return new AppMetadata[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/g.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */