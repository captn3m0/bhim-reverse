package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;
import java.util.Iterator;

public class EventParams
  implements SafeParcelable, Iterable
{
  public static final n CREATOR;
  public final int a;
  private final Bundle b;
  
  static
  {
    n localn = new com/google/android/gms/measurement/internal/n;
    localn.<init>();
    CREATOR = localn;
  }
  
  EventParams(int paramInt, Bundle paramBundle)
  {
    a = paramInt;
    b = paramBundle;
  }
  
  EventParams(Bundle paramBundle)
  {
    p.a(paramBundle);
    b = paramBundle;
    a = 1;
  }
  
  public int a()
  {
    return b.size();
  }
  
  Object a(String paramString)
  {
    return b.get(paramString);
  }
  
  public Bundle b()
  {
    Bundle localBundle1 = new android/os/Bundle;
    Bundle localBundle2 = b;
    localBundle1.<init>(localBundle2);
    return localBundle1;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public Iterator iterator()
  {
    EventParams.1 local1 = new com/google/android/gms/measurement/internal/EventParams$1;
    local1.<init>(this);
    return local1;
  }
  
  public String toString()
  {
    return b.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    n.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/EventParams.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */