package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import com.google.android.gms.common.internal.p;

public final class w$a
{
  private final String b;
  private final long c;
  private boolean d;
  private long e;
  
  public w$a(w paramw, String paramString, long paramLong)
  {
    p.a(paramString);
    b = paramString;
    c = paramLong;
  }
  
  private void b()
  {
    boolean bool = d;
    if (bool) {}
    for (;;)
    {
      return;
      bool = true;
      d = bool;
      SharedPreferences localSharedPreferences = w.a(a);
      String str = b;
      long l1 = c;
      long l2 = localSharedPreferences.getLong(str, l1);
      e = l2;
    }
  }
  
  public long a()
  {
    b();
    return e;
  }
  
  public void a(long paramLong)
  {
    SharedPreferences.Editor localEditor = w.a(a).edit();
    String str = b;
    localEditor.putLong(str, paramLong);
    localEditor.apply();
    e = paramLong;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/w$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */