package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class o
  implements Parcelable.Creator
{
  static void a(EventParcel paramEventParcel, Parcel paramParcel, int paramInt)
  {
    int i = a.a(paramParcel);
    int j = a;
    a.a(paramParcel, 1, j);
    Object localObject = b;
    a.a(paramParcel, 2, (String)localObject, false);
    localObject = c;
    a.a(paramParcel, 3, (Parcelable)localObject, paramInt, false);
    localObject = d;
    a.a(paramParcel, 4, (String)localObject, false);
    long l = e;
    a.a(paramParcel, 5, l);
    a.a(paramParcel, i);
  }
  
  public EventParcel a(Parcel paramParcel)
  {
    String str1 = null;
    int i = zza.b(paramParcel);
    int j = 0;
    StringBuilder localStringBuilder = null;
    long l = 0L;
    Object localObject1 = null;
    String str2 = null;
    Object localObject2;
    for (;;)
    {
      k = paramParcel.dataPosition();
      if (k >= i) {
        break;
      }
      k = zza.a(paramParcel);
      int m = zza.a(k);
      switch (m)
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        j = zza.d(paramParcel, k);
        break;
      case 2: 
        str2 = zza.h(paramParcel, k);
        break;
      case 3: 
        localObject1 = EventParams.CREATOR;
        localObject2 = (EventParams)zza.a(paramParcel, k, (Parcelable.Creator)localObject1);
        localObject1 = localObject2;
        break;
      case 4: 
        str1 = zza.h(paramParcel, k);
        break;
      case 5: 
        l = zza.e(paramParcel, k);
      }
    }
    int k = paramParcel.dataPosition();
    if (k != i)
    {
      localObject2 = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localStringBuilder = new java/lang/StringBuilder;
      localStringBuilder.<init>();
      localObject3 = "Overread allowed size end=" + i;
      ((zza.zza)localObject2).<init>((String)localObject3, paramParcel);
      throw ((Throwable)localObject2);
    }
    Object localObject3 = new com/google/android/gms/measurement/internal/EventParcel;
    ((EventParcel)localObject3).<init>(j, str2, (EventParams)localObject1, str1, l);
    return (EventParcel)localObject3;
  }
  
  public EventParcel[] a(int paramInt)
  {
    return new EventParcel[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/o.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */