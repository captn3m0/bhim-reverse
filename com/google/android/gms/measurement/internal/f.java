package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.text.TextUtils;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.e;
import com.google.android.gms.internal.j.b;
import com.google.android.gms.internal.j.c;
import com.google.android.gms.internal.j.e;
import com.google.android.gms.internal.zztd;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class f
  extends aa
{
  f(y paramy)
  {
    super(paramy);
  }
  
  private Object a(int paramInt, Object paramObject, boolean paramBoolean)
  {
    int i = 0;
    float f = 0.0F;
    if (paramObject == null) {
      paramObject = null;
    }
    for (;;)
    {
      return paramObject;
      boolean bool2 = paramObject instanceof Long;
      if (!bool2)
      {
        bool2 = paramObject instanceof Float;
        if (!bool2)
        {
          bool2 = paramObject instanceof Integer;
          long l;
          if (bool2)
          {
            i = ((Integer)paramObject).intValue();
            l = i;
            paramObject = Long.valueOf(l);
          }
          else
          {
            bool2 = paramObject instanceof Byte;
            if (bool2)
            {
              i = ((Byte)paramObject).byteValue();
              l = i;
              paramObject = Long.valueOf(l);
            }
            else
            {
              bool2 = paramObject instanceof Short;
              if (bool2)
              {
                i = ((Short)paramObject).shortValue();
                l = i;
                paramObject = Long.valueOf(l);
              }
              else
              {
                bool2 = paramObject instanceof Boolean;
                boolean bool1;
                double d;
                if (bool2)
                {
                  paramObject = (Boolean)paramObject;
                  bool1 = ((Boolean)paramObject).booleanValue();
                  if (bool1) {
                    l = 1L;
                  }
                  for (d = Double.MIN_VALUE;; d = 0.0D)
                  {
                    paramObject = Long.valueOf(l);
                    break;
                    l = 0L;
                  }
                }
                bool2 = paramObject instanceof Double;
                if (bool2)
                {
                  d = ((Double)paramObject).doubleValue();
                  f = (float)d;
                  paramObject = Float.valueOf(f);
                }
                else
                {
                  bool2 = paramObject instanceof String;
                  if (!bool2)
                  {
                    bool2 = paramObject instanceof Character;
                    if (!bool2)
                    {
                      bool2 = paramObject instanceof CharSequence;
                      if (!bool2) {
                        break label300;
                      }
                    }
                  }
                  paramObject = String.valueOf(paramObject);
                  int j = ((String)paramObject).length();
                  if (j > paramInt) {
                    if (paramBoolean)
                    {
                      bool1 = false;
                      f = 0.0F;
                      paramObject = ((String)paramObject).substring(0, paramInt);
                    }
                    else
                    {
                      paramObject = null;
                      continue;
                      label300:
                      paramObject = null;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  private void a(String paramString1, String paramString2, int paramInt, Object paramObject)
  {
    Object localObject1;
    Object localObject2;
    Object localObject3;
    if (paramObject == null)
    {
      localObject1 = l().q();
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append(paramString1);
      localObject3 = " value can't be null. Ignoring ";
      localObject2 = (String)localObject3 + paramString1;
      ((t.a)localObject1).a((String)localObject2, paramString2);
    }
    for (;;)
    {
      return;
      boolean bool = paramObject instanceof Long;
      if (!bool)
      {
        bool = paramObject instanceof Float;
        if (!bool)
        {
          bool = paramObject instanceof Integer;
          if (!bool)
          {
            bool = paramObject instanceof Byte;
            if (!bool)
            {
              bool = paramObject instanceof Short;
              if (!bool)
              {
                bool = paramObject instanceof Boolean;
                if (!bool)
                {
                  bool = paramObject instanceof Double;
                  if (!bool)
                  {
                    bool = paramObject instanceof String;
                    if (!bool)
                    {
                      bool = paramObject instanceof Character;
                      if (!bool)
                      {
                        bool = paramObject instanceof CharSequence;
                        if (!bool) {
                          continue;
                        }
                      }
                    }
                    localObject1 = String.valueOf(paramObject);
                    int j = ((String)localObject1).length();
                    if (j > paramInt)
                    {
                      localObject2 = l().q();
                      localObject3 = new java/lang/StringBuilder;
                      ((StringBuilder)localObject3).<init>();
                      localObject3 = ((StringBuilder)localObject3).append("Ignoring ").append(paramString1);
                      String str = ". Value is too long. name, value length";
                      localObject3 = str;
                      int i = ((String)localObject1).length();
                      localObject1 = Integer.valueOf(i);
                      ((t.a)localObject2).a((String)localObject3, paramString2, localObject1);
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  public static boolean a(Context paramContext, Class paramClass)
  {
    try
    {
      Object localObject1 = paramContext.getPackageManager();
      ComponentName localComponentName = new android/content/ComponentName;
      localComponentName.<init>(paramContext, paramClass);
      int i = 4;
      localObject1 = ((PackageManager)localObject1).getServiceInfo(localComponentName, i);
      if (localObject1 == null) {
        break label48;
      }
      bool = enabled;
      if (!bool) {
        break label48;
      }
      bool = true;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        label48:
        boolean bool = false;
        Object localObject2 = null;
      }
    }
    return bool;
  }
  
  public static boolean a(Context paramContext, Class paramClass, boolean paramBoolean)
  {
    try
    {
      Object localObject1 = paramContext.getPackageManager();
      ComponentName localComponentName = new android/content/ComponentName;
      localComponentName.<init>(paramContext, paramClass);
      int i = 2;
      localObject1 = ((PackageManager)localObject1).getReceiverInfo(localComponentName, i);
      if (localObject1 == null) {
        break label66;
      }
      boolean bool1 = enabled;
      if (!bool1) {
        break label66;
      }
      if (paramBoolean)
      {
        bool2 = exported;
        if (!bool2) {
          break label66;
        }
      }
      bool2 = true;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        label66:
        boolean bool2 = false;
        Object localObject2 = null;
      }
    }
    return bool2;
  }
  
  private int e(String paramString)
  {
    Object localObject = "_ldl";
    boolean bool = ((String)localObject).equals(paramString);
    if (bool) {
      localObject = n();
    }
    for (int i = ((h)localObject).s();; i = ((h)localObject).r())
    {
      return i;
      localObject = n();
    }
  }
  
  public Object a(String paramString, Object paramObject)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    Object localObject;
    if (!bool)
    {
      localObject = "_";
      bool = paramString.startsWith((String)localObject);
      if (bool) {
        localObject = n();
      }
    }
    for (int i = ((h)localObject).q();; i = ((h)localObject).p())
    {
      return a(i, paramObject, false);
      localObject = n();
    }
  }
  
  public void a(Bundle paramBundle, String paramString, Object paramObject)
  {
    boolean bool = paramObject instanceof Long;
    if (bool)
    {
      paramObject = (Long)paramObject;
      long l = ((Long)paramObject).longValue();
      paramBundle.putLong(paramString, l);
    }
    for (;;)
    {
      return;
      bool = paramObject instanceof Float;
      if (bool)
      {
        paramObject = (Float)paramObject;
        float f = ((Float)paramObject).floatValue();
        paramBundle.putFloat(paramString, f);
      }
      else
      {
        bool = paramObject instanceof String;
        Object localObject;
        if (bool)
        {
          localObject = String.valueOf(paramObject);
          paramBundle.putString(paramString, (String)localObject);
        }
        else if (paramString != null)
        {
          localObject = l().q();
          String str1 = "Not putting event parameter. Invalid value type. name, type";
          String str2 = paramObject.getClass().getSimpleName();
          ((t.a)localObject).a(str1, paramString, str2);
        }
      }
    }
  }
  
  public void a(j.b paramb, Object paramObject)
  {
    t.a locala = null;
    p.a(paramObject);
    b = null;
    c = null;
    d = null;
    boolean bool = paramObject instanceof String;
    if (bool)
    {
      paramObject = (String)paramObject;
      b = ((String)paramObject);
    }
    for (;;)
    {
      return;
      bool = paramObject instanceof Long;
      if (bool)
      {
        paramObject = (Long)paramObject;
        c = ((Long)paramObject);
      }
      else
      {
        bool = paramObject instanceof Float;
        if (bool)
        {
          paramObject = (Float)paramObject;
          d = ((Float)paramObject);
        }
        else
        {
          locala = l().b();
          String str = "Ignoring invalid (type) event param value";
          locala.a(str, paramObject);
        }
      }
    }
  }
  
  public void a(j.e parame, Object paramObject)
  {
    t.a locala = null;
    p.a(paramObject);
    c = null;
    d = null;
    e = null;
    boolean bool = paramObject instanceof String;
    if (bool)
    {
      paramObject = (String)paramObject;
      c = ((String)paramObject);
    }
    for (;;)
    {
      return;
      bool = paramObject instanceof Long;
      if (bool)
      {
        paramObject = (Long)paramObject;
        d = ((Long)paramObject);
      }
      else
      {
        bool = paramObject instanceof Float;
        if (bool)
        {
          paramObject = (Float)paramObject;
          e = ((Float)paramObject);
        }
        else
        {
          locala = l().b();
          String str = "Ignoring invalid (type) user attribute value";
          locala.a(str, paramObject);
        }
      }
    }
  }
  
  public void a(String paramString)
  {
    int i = n().b();
    a("event", i, paramString);
  }
  
  void a(String paramString1, int paramInt, String paramString2)
  {
    int i = 95;
    Object localObject;
    if (paramString2 == null)
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = paramString1 + " name is required and can't be null";
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    }
    int j = paramString2.length();
    if (j == 0)
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = paramString1 + " name is required and can't be empty";
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    }
    IllegalArgumentException localIllegalArgumentException = null;
    j = paramString2.charAt(0);
    boolean bool1 = Character.isLetter(j);
    if ((!bool1) && (j != i))
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = paramString1 + " name must start with a letter or _";
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    }
    int k = 1;
    for (;;)
    {
      int m = paramString2.length();
      if (k >= m) {
        break;
      }
      m = paramString2.charAt(k);
      if (m != i)
      {
        boolean bool2 = Character.isLetterOrDigit(m);
        if (!bool2)
        {
          localIllegalArgumentException = new java/lang/IllegalArgumentException;
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          localObject = paramString1 + " name must consist of letters, digits or _ (underscores)";
          localIllegalArgumentException.<init>((String)localObject);
          throw localIllegalArgumentException;
        }
      }
      k += 1;
    }
    k = paramString2.length();
    if (k > paramInt)
    {
      localIllegalArgumentException = new java/lang/IllegalArgumentException;
      localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = paramString1 + " name is too long. The maximum supported length is " + paramInt;
      localIllegalArgumentException.<init>((String)localObject);
      throw localIllegalArgumentException;
    }
  }
  
  public boolean a(long paramLong1, long paramLong2)
  {
    long l = 0L;
    boolean bool1 = true;
    boolean bool2 = paramLong1 < l;
    if (bool2)
    {
      bool2 = paramLong2 < l;
      if (bool2) {
        break label31;
      }
    }
    for (;;)
    {
      return bool1;
      label31:
      e locale = h();
      l = Math.abs(locale.a() - paramLong1);
      bool2 = l < paramLong2;
      if (!bool2) {
        bool1 = false;
      }
    }
  }
  
  public byte[] a(j.c paramc)
  {
    try
    {
      i = paramc.e();
      arrayOfByte = new byte[i];
      localObject2 = zztd.a(arrayOfByte);
      paramc.a((zztd)localObject2);
      ((zztd)localObject2).b();
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        byte[] arrayOfByte;
        Object localObject2 = l().b();
        String str = "Data loss. Failed to serialize batch";
        ((t.a)localObject2).a(str, localIOException);
        int i = 0;
        Object localObject1 = null;
      }
    }
    return arrayOfByte;
  }
  
  public byte[] a(byte[] paramArrayOfByte)
  {
    try
    {
      ByteArrayOutputStream localByteArrayOutputStream = new java/io/ByteArrayOutputStream;
      localByteArrayOutputStream.<init>();
      GZIPOutputStream localGZIPOutputStream = new java/util/zip/GZIPOutputStream;
      localGZIPOutputStream.<init>(localByteArrayOutputStream);
      localGZIPOutputStream.write(paramArrayOfByte);
      localGZIPOutputStream.close();
      localByteArrayOutputStream.close();
      return localByteArrayOutputStream.toByteArray();
    }
    catch (IOException localIOException)
    {
      l().b().a("Failed to gzip content", localIOException);
      throw localIOException;
    }
  }
  
  public void b(String paramString)
  {
    int i = n().o();
    a("user attribute", i, paramString);
  }
  
  public void b(String paramString, Object paramObject)
  {
    String str = "_ldl";
    boolean bool = str.equals(paramString);
    int i;
    if (bool)
    {
      str = "user attribute referrer";
      i = e(paramString);
      a(str, paramString, i, paramObject);
    }
    for (;;)
    {
      return;
      str = "user attribute";
      i = e(paramString);
      a(str, paramString, i, paramObject);
    }
  }
  
  /* Error */
  public byte[] b(byte[] paramArrayOfByte)
  {
    // Byte code:
    //   0: new 359	java/io/ByteArrayInputStream
    //   3: astore_2
    //   4: aload_2
    //   5: aload_1
    //   6: invokespecial 361	java/io/ByteArrayInputStream:<init>	([B)V
    //   9: new 363	java/util/zip/GZIPInputStream
    //   12: astore_3
    //   13: aload_3
    //   14: aload_2
    //   15: invokespecial 366	java/util/zip/GZIPInputStream:<init>	(Ljava/io/InputStream;)V
    //   18: new 324	java/io/ByteArrayOutputStream
    //   21: astore 4
    //   23: aload 4
    //   25: invokespecial 325	java/io/ByteArrayOutputStream:<init>	()V
    //   28: sipush 1024
    //   31: istore 5
    //   33: iload 5
    //   35: newarray <illegal type>
    //   37: astore 6
    //   39: aload_3
    //   40: aload 6
    //   42: invokevirtual 371	java/util/zip/GZIPInputStream:read	([B)I
    //   45: istore 7
    //   47: iload 7
    //   49: ifgt +17 -> 66
    //   52: aload_3
    //   53: invokevirtual 372	java/util/zip/GZIPInputStream:close	()V
    //   56: aload_2
    //   57: invokevirtual 373	java/io/ByteArrayInputStream:close	()V
    //   60: aload 4
    //   62: invokevirtual 342	java/io/ByteArrayOutputStream:toByteArray	()[B
    //   65: areturn
    //   66: aload 4
    //   68: aload 6
    //   70: iconst_0
    //   71: iload 7
    //   73: invokevirtual 376	java/io/ByteArrayOutputStream:write	([BII)V
    //   76: goto -37 -> 39
    //   79: astore_2
    //   80: aload_0
    //   81: invokevirtual 71	com/google/android/gms/measurement/internal/f:l	()Lcom/google/android/gms/measurement/internal/t;
    //   84: invokevirtual 240	com/google/android/gms/measurement/internal/t:b	()Lcom/google/android/gms/measurement/internal/t$a;
    //   87: ldc_w 378
    //   90: aload_2
    //   91: invokevirtual 98	com/google/android/gms/measurement/internal/t$a:a	(Ljava/lang/String;Ljava/lang/Object;)V
    //   94: aload_2
    //   95: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	96	0	this	f
    //   0	96	1	paramArrayOfByte	byte[]
    //   3	54	2	localByteArrayInputStream	java.io.ByteArrayInputStream
    //   79	16	2	localIOException	IOException
    //   12	41	3	localGZIPInputStream	java.util.zip.GZIPInputStream
    //   21	46	4	localByteArrayOutputStream	ByteArrayOutputStream
    //   31	3	5	i	int
    //   37	32	6	arrayOfByte	byte[]
    //   45	27	7	j	int
    // Exception table:
    //   from	to	target	type
    //   0	3	79	java/io/IOException
    //   5	9	79	java/io/IOException
    //   9	12	79	java/io/IOException
    //   14	18	79	java/io/IOException
    //   18	21	79	java/io/IOException
    //   23	28	79	java/io/IOException
    //   33	37	79	java/io/IOException
    //   40	45	79	java/io/IOException
    //   52	56	79	java/io/IOException
    //   56	60	79	java/io/IOException
    //   60	65	79	java/io/IOException
    //   71	76	79	java/io/IOException
  }
  
  public Object c(String paramString, Object paramObject)
  {
    Object localObject = "_ldl";
    boolean bool1 = ((String)localObject).equals(paramString);
    int i;
    boolean bool2;
    if (bool1)
    {
      i = e(paramString);
      bool2 = true;
    }
    for (localObject = a(i, paramObject, bool2);; localObject = a(i, paramObject, false))
    {
      return localObject;
      i = e(paramString);
      bool2 = false;
    }
  }
  
  public void c(String paramString)
  {
    int i = n().o();
    a("event param", i, paramString);
  }
  
  public boolean d(String paramString)
  {
    e();
    Context localContext = i();
    int i = localContext.checkCallingOrSelfPermission(paramString);
    if (i == 0) {
      i = 1;
    }
    for (;;)
    {
      return i;
      int j = 0;
      localContext = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/f.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */