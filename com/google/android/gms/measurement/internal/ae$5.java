package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

class ae$5
  implements Runnable
{
  ae$5(ae paramae, UserAttributeParcel paramUserAttributeParcel) {}
  
  public void run()
  {
    Object localObject1 = ae.c(b);
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = b.l().b();
      localObject2 = "Discarding data. Failed to set user attribute";
      ((t.a)localObject1).a((String)localObject2);
    }
    for (;;)
    {
      return;
      try
      {
        localObject2 = a;
        localObject3 = b;
        localObject3 = ((ae)localObject3).f();
        Object localObject4 = b;
        localObject4 = ((ae)localObject4).l();
        localObject4 = ((t)localObject4).u();
        localObject3 = ((r)localObject3).a((String)localObject4);
        ((q)localObject1).a((UserAttributeParcel)localObject2, (AppMetadata)localObject3);
        localObject1 = b;
        ae.d((ae)localObject1);
      }
      catch (RemoteException localRemoteException)
      {
        localObject2 = b.l().b();
        Object localObject3 = "Failed to send attribute to AppMeasurementService";
        ((t.a)localObject2).a((String)localObject3, localRemoteException);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/ae$5.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */