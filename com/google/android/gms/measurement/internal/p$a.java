package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.c;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.b;

public final class p$a
{
  private final Object a;
  private final b b;
  private Object c;
  
  private p$a(b paramb, Object paramObject)
  {
    p.a(paramb);
    b = paramb;
    a = paramObject;
  }
  
  static a a(String paramString, int paramInt)
  {
    return a(paramString, paramInt, paramInt);
  }
  
  static a a(String paramString, int paramInt1, int paramInt2)
  {
    a locala = new com/google/android/gms/measurement/internal/p$a;
    Object localObject = Integer.valueOf(paramInt2);
    localObject = b.a(paramString, (Integer)localObject);
    Integer localInteger = Integer.valueOf(paramInt1);
    locala.<init>((b)localObject, localInteger);
    return locala;
  }
  
  static a a(String paramString, long paramLong)
  {
    return a(paramString, paramLong, paramLong);
  }
  
  static a a(String paramString, long paramLong1, long paramLong2)
  {
    a locala = new com/google/android/gms/measurement/internal/p$a;
    Object localObject = Long.valueOf(paramLong2);
    localObject = b.a(paramString, (Long)localObject);
    Long localLong = Long.valueOf(paramLong1);
    locala.<init>((b)localObject, localLong);
    return locala;
  }
  
  static a a(String paramString1, String paramString2)
  {
    return a(paramString1, paramString2, paramString2);
  }
  
  static a a(String paramString1, String paramString2, String paramString3)
  {
    a locala = new com/google/android/gms/measurement/internal/p$a;
    b localb = b.a(paramString1, paramString3);
    locala.<init>(localb, paramString2);
    return locala;
  }
  
  static a a(String paramString, boolean paramBoolean)
  {
    return a(paramString, paramBoolean, paramBoolean);
  }
  
  static a a(String paramString, boolean paramBoolean1, boolean paramBoolean2)
  {
    a locala = new com/google/android/gms/measurement/internal/p$a;
    b localb = b.a(paramString, paramBoolean2);
    Boolean localBoolean = Boolean.valueOf(paramBoolean1);
    locala.<init>(localb, localBoolean);
    return locala;
  }
  
  public Object a()
  {
    Object localObject = c;
    if (localObject != null) {
      localObject = c;
    }
    for (;;)
    {
      return localObject;
      boolean bool = c.a;
      if (bool)
      {
        bool = b.b();
        if (bool)
        {
          localObject = b.d();
          continue;
        }
      }
      localObject = a;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/p$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */