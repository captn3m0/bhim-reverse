package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.e;

abstract class j
{
  private static volatile Handler b;
  private final y a;
  private final Runnable c;
  private volatile long d;
  private boolean e;
  
  j(y paramy)
  {
    p.a(paramy);
    a = paramy;
    e = true;
    j.1 local1 = new com/google/android/gms/measurement/internal/j$1;
    local1.<init>(this);
    c = local1;
  }
  
  private Handler d()
  {
    Handler localHandler = b;
    if (localHandler != null) {
      localHandler = b;
    }
    for (;;)
    {
      return localHandler;
      synchronized (j.class)
      {
        localHandler = b;
        if (localHandler == null)
        {
          localHandler = new android/os/Handler;
          Object localObject2 = a;
          localObject2 = ((y)localObject2).n();
          localObject2 = ((Context)localObject2).getMainLooper();
          localHandler.<init>((Looper)localObject2);
          b = localHandler;
        }
        localHandler = b;
      }
    }
  }
  
  public abstract void a();
  
  public void a(long paramLong)
  {
    c();
    long l = 0L;
    boolean bool = paramLong < l;
    if (!bool)
    {
      l = a.o().a();
      d = l;
      Object localObject1 = d();
      Object localObject2 = c;
      bool = ((Handler)localObject1).postDelayed((Runnable)localObject2, paramLong);
      if (!bool)
      {
        localObject1 = a.f().b();
        localObject2 = "Failed to schedule delayed post. time";
        Long localLong = Long.valueOf(paramLong);
        ((t.a)localObject1).a((String)localObject2, localLong);
      }
    }
  }
  
  public boolean b()
  {
    long l1 = d;
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (bool) {}
    for (bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void c()
  {
    d = 0L;
    Handler localHandler = d();
    Runnable localRunnable = c;
    localHandler.removeCallbacks(localRunnable);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/j.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */