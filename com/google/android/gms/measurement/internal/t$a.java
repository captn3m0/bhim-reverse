package com.google.android.gms.measurement.internal;

public class t$a
{
  private final int b;
  private final boolean c;
  private final boolean d;
  
  t$a(t paramt, int paramInt, boolean paramBoolean1, boolean paramBoolean2)
  {
    b = paramInt;
    c = paramBoolean1;
    d = paramBoolean2;
  }
  
  public void a(String paramString)
  {
    t localt = a;
    int i = b;
    boolean bool1 = c;
    boolean bool2 = d;
    localt.a(i, bool1, bool2, paramString, null, null, null);
  }
  
  public void a(String paramString, Object paramObject)
  {
    t localt = a;
    int i = b;
    boolean bool1 = c;
    boolean bool2 = d;
    localt.a(i, bool1, bool2, paramString, paramObject, null, null);
  }
  
  public void a(String paramString, Object paramObject1, Object paramObject2)
  {
    t localt = a;
    int i = b;
    boolean bool1 = c;
    boolean bool2 = d;
    localt.a(i, bool1, bool2, paramString, paramObject1, paramObject2, null);
  }
  
  public void a(String paramString, Object paramObject1, Object paramObject2, Object paramObject3)
  {
    t localt = a;
    int i = b;
    boolean bool1 = c;
    boolean bool2 = d;
    localt.a(i, bool1, bool2, paramString, paramObject1, paramObject2, paramObject3);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/t$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */