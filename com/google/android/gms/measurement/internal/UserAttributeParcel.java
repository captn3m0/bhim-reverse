package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class UserAttributeParcel
  implements SafeParcelable
{
  public static final e CREATOR;
  public final int a;
  public final String b;
  public final long c;
  public final Long d;
  public final Float e;
  public final String f;
  public final String g;
  
  static
  {
    e locale = new com/google/android/gms/measurement/internal/e;
    locale.<init>();
    CREATOR = locale;
  }
  
  UserAttributeParcel(int paramInt, String paramString1, long paramLong, Long paramLong1, Float paramFloat, String paramString2, String paramString3)
  {
    a = paramInt;
    b = paramString1;
    c = paramLong;
    d = paramLong1;
    e = paramFloat;
    f = paramString2;
    g = paramString3;
  }
  
  UserAttributeParcel(String paramString1, long paramLong, Object paramObject, String paramString2)
  {
    p.a(paramString1);
    int i = 1;
    a = i;
    b = paramString1;
    c = paramLong;
    g = paramString2;
    if (paramObject == null)
    {
      d = null;
      e = null;
      f = null;
    }
    for (;;)
    {
      return;
      i = paramObject instanceof Long;
      if (i != 0)
      {
        paramObject = (Long)paramObject;
        d = ((Long)paramObject);
        e = null;
        f = null;
      }
      else
      {
        i = paramObject instanceof Float;
        if (i != 0)
        {
          d = null;
          paramObject = (Float)paramObject;
          e = ((Float)paramObject);
          f = null;
        }
        else
        {
          i = paramObject instanceof String;
          if (i == 0) {
            break;
          }
          d = null;
          e = null;
          paramObject = (String)paramObject;
          f = ((String)paramObject);
        }
      }
    }
    IllegalArgumentException localIllegalArgumentException = new java/lang/IllegalArgumentException;
    localIllegalArgumentException.<init>("User attribute given of un-supported type");
    throw localIllegalArgumentException;
  }
  
  public Object a()
  {
    Object localObject = d;
    if (localObject != null) {
      localObject = d;
    }
    for (;;)
    {
      return localObject;
      localObject = e;
      if (localObject != null)
      {
        localObject = e;
      }
      else
      {
        localObject = f;
        if (localObject != null) {
          localObject = f;
        } else {
          localObject = null;
        }
      }
    }
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    e.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/UserAttributeParcel.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */