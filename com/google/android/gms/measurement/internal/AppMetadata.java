package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.text.TextUtils;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.common.internal.safeparcel.SafeParcelable;

public class AppMetadata
  implements SafeParcelable
{
  public static final g CREATOR;
  public final int a;
  public final String b;
  public final String c;
  public final String d;
  public final String e;
  public final long f;
  public final long g;
  public final String h;
  public final boolean i;
  
  static
  {
    g localg = new com/google/android/gms/measurement/internal/g;
    localg.<init>();
    CREATOR = localg;
  }
  
  AppMetadata(int paramInt, String paramString1, String paramString2, String paramString3, String paramString4, long paramLong1, long paramLong2, String paramString5, boolean paramBoolean)
  {
    a = paramInt;
    b = paramString1;
    c = paramString2;
    d = paramString3;
    e = paramString4;
    f = paramLong1;
    g = paramLong2;
    h = paramString5;
    int j = 3;
    if (paramInt >= j) {}
    for (i = paramBoolean;; i = j)
    {
      return;
      j = 1;
    }
  }
  
  AppMetadata(String paramString1, String paramString2, String paramString3, String paramString4, long paramLong1, long paramLong2, String paramString5, boolean paramBoolean)
  {
    p.a(paramString1);
    a = 3;
    b = paramString1;
    boolean bool = TextUtils.isEmpty(paramString2);
    if (bool) {
      paramString2 = null;
    }
    c = paramString2;
    d = paramString3;
    e = paramString4;
    f = paramLong1;
    g = paramLong2;
    h = paramString5;
    i = paramBoolean;
  }
  
  public int describeContents()
  {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt)
  {
    g.a(this, paramParcel, paramInt);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/AppMetadata.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */