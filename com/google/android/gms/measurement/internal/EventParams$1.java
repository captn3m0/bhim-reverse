package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import java.util.Iterator;
import java.util.Set;

class EventParams$1
  implements Iterator
{
  Iterator a;
  
  EventParams$1(EventParams paramEventParams)
  {
    Iterator localIterator = EventParams.a(b).keySet().iterator();
    a = localIterator;
  }
  
  public String a()
  {
    return (String)a.next();
  }
  
  public boolean hasNext()
  {
    return a.hasNext();
  }
  
  public void remove()
  {
    UnsupportedOperationException localUnsupportedOperationException = new java/lang/UnsupportedOperationException;
    localUnsupportedOperationException.<init>("Remove not supported");
    throw localUnsupportedOperationException;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/EventParams$1.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */