package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable.Creator;
import com.google.android.gms.common.internal.safeparcel.a;
import com.google.android.gms.common.internal.safeparcel.zza;
import com.google.android.gms.common.internal.safeparcel.zza.zza;

public class n
  implements Parcelable.Creator
{
  static void a(EventParams paramEventParams, Parcel paramParcel, int paramInt)
  {
    int i = a.a(paramParcel);
    int j = a;
    a.a(paramParcel, 1, j);
    Bundle localBundle = paramEventParams.b();
    a.a(paramParcel, 2, localBundle, false);
    a.a(paramParcel, i);
  }
  
  public EventParams a(Parcel paramParcel)
  {
    int i = zza.b(paramParcel);
    int j = 0;
    Object localObject1 = null;
    Object localObject2 = null;
    for (;;)
    {
      k = paramParcel.dataPosition();
      if (k >= i) {
        break;
      }
      k = zza.a(paramParcel);
      int m = zza.a(k);
      switch (m)
      {
      default: 
        zza.b(paramParcel, k);
        break;
      case 1: 
        j = zza.d(paramParcel, k);
        break;
      case 2: 
        localObject2 = zza.j(paramParcel, k);
      }
    }
    int k = paramParcel.dataPosition();
    if (k != i)
    {
      localObject2 = new com/google/android/gms/common/internal/safeparcel/zza$zza;
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = "Overread allowed size end=" + i;
      ((zza.zza)localObject2).<init>((String)localObject1, paramParcel);
      throw ((Throwable)localObject2);
    }
    EventParams localEventParams = new com/google/android/gms/measurement/internal/EventParams;
    localEventParams.<init>(j, (Bundle)localObject2);
    return localEventParams;
  }
  
  public EventParams[] a(int paramInt)
  {
    return new EventParams[paramInt];
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/n.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */