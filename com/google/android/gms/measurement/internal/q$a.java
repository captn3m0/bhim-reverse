package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.os.IBinder;
import android.os.Parcel;

public abstract class q$a
  extends Binder
  implements q
{
  public q$a()
  {
    attachInterface(this, "com.google.android.gms.measurement.internal.IMeasurementService");
  }
  
  public static q a(IBinder paramIBinder)
  {
    Object localObject;
    if (paramIBinder == null) {
      localObject = null;
    }
    for (;;)
    {
      return (q)localObject;
      localObject = paramIBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
      if (localObject != null)
      {
        boolean bool = localObject instanceof q;
        if (bool)
        {
          localObject = (q)localObject;
          continue;
        }
      }
      localObject = new com/google/android/gms/measurement/internal/q$a$a;
      ((q.a.a)localObject).<init>(paramIBinder);
    }
  }
  
  public IBinder asBinder()
  {
    return this;
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2)
  {
    int i = 1;
    Object localObject1 = null;
    boolean bool;
    switch (paramInt1)
    {
    default: 
      bool = super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
    }
    for (;;)
    {
      return bool;
      Object localObject2 = "com.google.android.gms.measurement.internal.IMeasurementService";
      paramParcel2.writeString((String)localObject2);
      bool = i;
      continue;
      localObject2 = "com.google.android.gms.measurement.internal.IMeasurementService";
      paramParcel1.enforceInterface((String)localObject2);
      int j = paramParcel1.readInt();
      if (j != 0) {}
      int k;
      for (localObject2 = EventParcel.CREATOR.a(paramParcel1);; localObject2 = null)
      {
        k = paramParcel1.readInt();
        if (k != 0) {
          localObject1 = AppMetadata.CREATOR.a(paramParcel1);
        }
        a((EventParcel)localObject2, (AppMetadata)localObject1);
        paramParcel2.writeNoException();
        j = i;
        break;
        j = 0;
      }
      localObject2 = "com.google.android.gms.measurement.internal.IMeasurementService";
      paramParcel1.enforceInterface((String)localObject2);
      j = paramParcel1.readInt();
      if (j != 0) {}
      for (localObject2 = UserAttributeParcel.CREATOR.a(paramParcel1);; localObject2 = null)
      {
        k = paramParcel1.readInt();
        if (k != 0) {
          localObject1 = AppMetadata.CREATOR.a(paramParcel1);
        }
        a((UserAttributeParcel)localObject2, (AppMetadata)localObject1);
        paramParcel2.writeNoException();
        j = i;
        break;
        j = 0;
      }
      localObject2 = "com.google.android.gms.measurement.internal.IMeasurementService";
      paramParcel1.enforceInterface((String)localObject2);
      j = paramParcel1.readInt();
      if (j != 0)
      {
        localObject2 = AppMetadata.CREATOR;
        localObject1 = ((g)localObject2).a(paramParcel1);
      }
      a((AppMetadata)localObject1);
      paramParcel2.writeNoException();
      j = i;
      continue;
      localObject2 = "com.google.android.gms.measurement.internal.IMeasurementService";
      paramParcel1.enforceInterface((String)localObject2);
      j = paramParcel1.readInt();
      if (j != 0)
      {
        localObject2 = EventParcel.CREATOR;
        localObject1 = ((o)localObject2).a(paramParcel1);
      }
      localObject2 = paramParcel1.readString();
      String str = paramParcel1.readString();
      a((EventParcel)localObject1, (String)localObject2, str);
      paramParcel2.writeNoException();
      j = i;
      continue;
      localObject2 = "com.google.android.gms.measurement.internal.IMeasurementService";
      paramParcel1.enforceInterface((String)localObject2);
      j = paramParcel1.readInt();
      if (j != 0)
      {
        localObject2 = AppMetadata.CREATOR;
        localObject1 = ((g)localObject2).a(paramParcel1);
      }
      b((AppMetadata)localObject1);
      paramParcel2.writeNoException();
      j = i;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/q$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */