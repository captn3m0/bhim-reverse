package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.app.Application.ActivityLifecycleCallbacks;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;

class ad$a
  implements Application.ActivityLifecycleCallbacks
{
  private ad$a(ad paramad) {}
  
  private boolean a(String paramString)
  {
    boolean bool = TextUtils.isEmpty(paramString);
    ad localad;
    if (!bool)
    {
      localad = a;
      String str1 = "auto";
      String str2 = "_ldl";
      localad.a(str1, str2, paramString);
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localad = null;
    }
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle)
  {
    for (;;)
    {
      try
      {
        localObject1 = a;
        localObject1 = ((ad)localObject1).l();
        localObject1 = ((t)localObject1).t();
        localObject3 = "onActivityCreated";
        ((t.a)localObject1).a((String)localObject3);
        localObject1 = paramActivity.getIntent();
        if (localObject1 != null)
        {
          localObject1 = ((Intent)localObject1).getData();
          if (localObject1 != null)
          {
            bool = ((Uri)localObject1).isHierarchical();
            if (bool)
            {
              localObject3 = "referrer";
              localObject1 = ((Uri)localObject1).getQueryParameter((String)localObject3);
              bool = TextUtils.isEmpty((CharSequence)localObject1);
              if (!bool) {
                continue;
              }
            }
          }
        }
        return;
      }
      finally
      {
        Object localObject1;
        boolean bool;
        Object localObject3 = a.l().b();
        String str = "Throwable caught in onActivityCreated";
        ((t.a)localObject3).a(str, localObject2);
        continue;
        localObject3 = a;
        localObject3 = ((ad)localObject3).l();
        localObject3 = ((t)localObject3).s();
        str = "Activity created with referrer";
        ((t.a)localObject3).a(str, localObject2);
        a((String)localObject2);
        continue;
      }
      localObject3 = "gclid";
      bool = ((String)localObject1).contains((CharSequence)localObject3);
      if (bool) {
        continue;
      }
      localObject1 = a;
      localObject1 = ((ad)localObject1).l();
      localObject1 = ((t)localObject1).s();
      localObject3 = "Activity created with data 'referrer' param without gclid";
      ((t.a)localObject1).a((String)localObject3);
    }
  }
  
  public void onActivityDestroyed(Activity paramActivity) {}
  
  public void onActivityPaused(Activity paramActivity) {}
  
  public void onActivityResumed(Activity paramActivity) {}
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity) {}
  
  public void onActivityStopped(Activity paramActivity) {}
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/ad$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */