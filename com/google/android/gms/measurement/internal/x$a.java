package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.p;
import java.util.concurrent.FutureTask;

final class x$a
  extends FutureTask
{
  private final String b;
  
  x$a(x paramx, Runnable paramRunnable, String paramString)
  {
    super(paramRunnable, null);
    p.a(paramString);
    b = paramString;
  }
  
  protected void setException(Throwable paramThrowable)
  {
    t.a locala = a.l().b();
    String str = b;
    locala.a(str, paramThrowable);
    super.setException(paramThrowable);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/x$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */