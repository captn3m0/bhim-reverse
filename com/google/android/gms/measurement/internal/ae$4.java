package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;

class ae$4
  implements Runnable
{
  ae$4(ae paramae, String paramString, EventParcel paramEventParcel) {}
  
  public void run()
  {
    Object localObject1 = ae.c(c);
    Object localObject2;
    if (localObject1 == null)
    {
      localObject1 = c.l().b();
      localObject2 = "Discarding data. Failed to send event to service";
      ((t.a)localObject1).a((String)localObject2);
      return;
    }
    for (;;)
    {
      try
      {
        localObject2 = a;
        boolean bool = TextUtils.isEmpty((CharSequence)localObject2);
        if (!bool) {
          break label140;
        }
        localObject2 = b;
        localObject3 = c;
        localObject3 = ((ae)localObject3).f();
        localObject4 = c;
        localObject4 = ((ae)localObject4).l();
        localObject4 = ((t)localObject4).u();
        localObject3 = ((r)localObject3).a((String)localObject4);
        ((q)localObject1).a((EventParcel)localObject2, (AppMetadata)localObject3);
        localObject1 = c;
        ae.d((ae)localObject1);
      }
      catch (RemoteException localRemoteException)
      {
        localObject2 = c.l().b();
        localObject3 = "Failed to send event to AppMeasurementService";
        ((t.a)localObject2).a((String)localObject3, localRemoteException);
      }
      break;
      label140:
      localObject2 = b;
      Object localObject3 = a;
      Object localObject4 = c;
      localObject4 = ((ae)localObject4).l();
      localObject4 = ((t)localObject4).u();
      localRemoteException.a((EventParcel)localObject2, (String)localObject3, (String)localObject4);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/measurement/internal/ae$4.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */