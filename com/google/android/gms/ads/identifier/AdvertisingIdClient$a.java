package com.google.android.gms.ads.identifier;

import java.lang.ref.WeakReference;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

class AdvertisingIdClient$a
  extends Thread
{
  CountDownLatch a;
  boolean b;
  private WeakReference c;
  private long d;
  
  public AdvertisingIdClient$a(AdvertisingIdClient paramAdvertisingIdClient, long paramLong)
  {
    Object localObject = new java/lang/ref/WeakReference;
    ((WeakReference)localObject).<init>(paramAdvertisingIdClient);
    c = ((WeakReference)localObject);
    d = paramLong;
    localObject = new java/util/concurrent/CountDownLatch;
    ((CountDownLatch)localObject).<init>(1);
    a = ((CountDownLatch)localObject);
    b = false;
    start();
  }
  
  private void c()
  {
    AdvertisingIdClient localAdvertisingIdClient = (AdvertisingIdClient)c.get();
    if (localAdvertisingIdClient != null)
    {
      localAdvertisingIdClient.finish();
      boolean bool = true;
      b = bool;
    }
  }
  
  public void a()
  {
    a.countDown();
  }
  
  public boolean b()
  {
    return b;
  }
  
  public void run()
  {
    try
    {
      CountDownLatch localCountDownLatch = a;
      long l = d;
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      boolean bool = localCountDownLatch.await(l, localTimeUnit);
      if (!bool) {
        c();
      }
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        c();
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/ads/identifier/AdvertisingIdClient$a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */