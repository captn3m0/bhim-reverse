package com.google.android.gms.ads.identifier;

import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.d;
import com.google.android.gms.common.internal.p;
import com.google.android.gms.internal.a;
import com.google.android.gms.internal.a.a;
import java.io.IOException;

public class AdvertisingIdClient
{
  private static boolean zzoN = false;
  private final Context mContext;
  d zzoH;
  a zzoI;
  boolean zzoJ;
  Object zzoK;
  AdvertisingIdClient.a zzoL;
  final long zzoM;
  
  public AdvertisingIdClient(Context paramContext)
  {
    this(paramContext, 30000L);
  }
  
  public AdvertisingIdClient(Context paramContext, long paramLong)
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    zzoK = localObject;
    p.a(paramContext);
    mContext = paramContext;
    zzoJ = false;
    zzoM = paramLong;
  }
  
  public static AdvertisingIdClient.Info getAdvertisingIdInfo(Context paramContext)
  {
    AdvertisingIdClient localAdvertisingIdClient = new com/google/android/gms/ads/identifier/AdvertisingIdClient;
    long l = -1;
    localAdvertisingIdClient.<init>(paramContext, l);
    AdvertisingIdClient.Info localInfo = null;
    try
    {
      localAdvertisingIdClient.zzb(false);
      localInfo = localAdvertisingIdClient.getInfo();
      return localInfo;
    }
    finally
    {
      localAdvertisingIdClient.finish();
    }
  }
  
  public static void setShouldSkipGmsCoreVersionCheck(boolean paramBoolean)
  {
    zzoN = paramBoolean;
  }
  
  static a zza(Context paramContext, d paramd)
  {
    IOException localIOException2;
    try
    {
      IBinder localIBinder = paramd.a();
      return a.a.a(localIBinder);
    }
    catch (InterruptedException localInterruptedException)
    {
      IOException localIOException1 = new java/io/IOException;
      localIOException1.<init>("Interrupted exception");
      throw localIOException1;
    }
    finally
    {
      localIOException2 = new java/io/IOException;
      localIOException2.<init>(localThrowable);
    }
  }
  
  private void zzaL()
  {
    synchronized (zzoK)
    {
      AdvertisingIdClient.a locala = zzoL;
      if (locala != null)
      {
        locala = zzoL;
        locala.a();
      }
      try
      {
        locala = zzoL;
        locala.join();
      }
      catch (InterruptedException localInterruptedException)
      {
        long l1;
        long l2;
        boolean bool;
        for (;;) {}
      }
      l1 = zzoM;
      l2 = 0L;
      bool = l1 < l2;
      if (bool)
      {
        locala = new com/google/android/gms/ads/identifier/AdvertisingIdClient$a;
        l1 = zzoM;
        locala.<init>(this, l1);
        zzoL = locala;
      }
      return;
    }
  }
  
  static d zzp(Context paramContext)
  {
    Object localObject3;
    Object localObject4;
    Object localObject2;
    try
    {
      Object localObject1 = paramContext.getPackageManager();
      localObject3 = "com.android.vending";
      localObject4 = null;
      ((PackageManager)localObject1).getPackageInfo((String)localObject3, 0);
      boolean bool1 = zzoN;
      if (bool1)
      {
        localObject3 = "Skipping gmscore version check";
        Log.d("Ads", (String)localObject3);
        localObject1 = com.google.android.gms.common.b.a();
        int i = ((com.google.android.gms.common.b)localObject1).a(paramContext);
        switch (i)
        {
        default: 
          localObject1 = new java/io/IOException;
          ((IOException)localObject1).<init>("Google Play services not available");
          throw ((Throwable)localObject1);
        }
      }
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      localObject2 = new com/google/android/gms/common/GooglePlayServicesNotAvailableException;
      ((GooglePlayServicesNotAvailableException)localObject2).<init>(9);
      throw ((Throwable)localObject2);
    }
    try
    {
      GooglePlayServicesUtil.zzac(paramContext);
      localObject2 = new com/google/android/gms/common/d;
      ((d)localObject2).<init>();
      localObject3 = new android/content/Intent;
      ((Intent)localObject3).<init>("com.google.android.gms.ads.identifier.service.START");
      localObject4 = "com.google.android.gms";
      ((Intent)localObject3).setPackage((String)localObject4);
      int j;
      boolean bool2;
      localIOException = new java/io/IOException;
    }
    catch (GooglePlayServicesNotAvailableException localGooglePlayServicesNotAvailableException)
    {
      try
      {
        localObject4 = com.google.android.gms.common.stats.b.a();
        j = 1;
        bool2 = ((com.google.android.gms.common.stats.b)localObject4).a(paramContext, (Intent)localObject3, (ServiceConnection)localObject2, j);
        if (!bool2) {
          break label181;
        }
        return (d)localObject2;
      }
      finally
      {
        localObject3 = new java/io/IOException;
        ((IOException)localObject3).<init>(localThrowable);
      }
      localGooglePlayServicesNotAvailableException = localGooglePlayServicesNotAvailableException;
      localObject3 = new java/io/IOException;
      ((IOException)localObject3).<init>(localGooglePlayServicesNotAvailableException);
      throw ((Throwable)localObject3);
    }
    label181:
    IOException localIOException;
    localIOException.<init>("Connection failure");
    throw localIOException;
  }
  
  protected void finalize()
  {
    finish();
    super.finalize();
  }
  
  public void finish()
  {
    Object localObject1 = "Calling this from your main thread can lead to deadlock";
    p.c((String)localObject1);
    for (;;)
    {
      try
      {
        localObject1 = mContext;
        if (localObject1 != null)
        {
          localObject1 = zzoH;
          if (localObject1 != null) {}
        }
        else
        {
          return;
        }
      }
      finally {}
      try
      {
        bool = zzoJ;
        if (bool)
        {
          localObject1 = com.google.android.gms.common.stats.b.a();
          localObject3 = mContext;
          localObject4 = zzoH;
          ((com.google.android.gms.common.stats.b)localObject1).a((Context)localObject3, (ServiceConnection)localObject4);
        }
      }
      catch (IllegalArgumentException localIllegalArgumentException)
      {
        localObject3 = "AdvertisingIdClient";
        localObject4 = "AdvertisingIdClient unbindService failed.";
        Log.i((String)localObject3, (String)localObject4, localIllegalArgumentException);
        continue;
      }
      boolean bool = false;
      localObject1 = null;
      zzoJ = false;
      bool = false;
      localObject1 = null;
      zzoI = null;
      bool = false;
      localObject1 = null;
      zzoH = null;
    }
  }
  
  public AdvertisingIdClient.Info getInfo()
  {
    Object localObject1 = "Calling this from your main thread can lead to deadlock";
    p.c((String)localObject1);
    Object localObject6;
    try
    {
      bool1 = zzoJ;
      if (bool1) {
        break label126;
      }
      synchronized (zzoK)
      {
        localObject1 = zzoL;
        if (localObject1 != null)
        {
          localObject1 = zzoL;
          bool1 = ((AdvertisingIdClient.a)localObject1).b();
          if (bool1) {}
        }
        else
        {
          localObject1 = new java/io/IOException;
          localObject6 = "AdvertisingIdClient is not connected.";
          ((IOException)localObject1).<init>((String)localObject6);
          throw ((Throwable)localObject1);
        }
      }
    }
    finally {}
    boolean bool1 = false;
    IOException localIOException1 = null;
    try
    {
      zzb(false);
      bool1 = zzoJ;
      if (!bool1)
      {
        localIOException1 = new java/io/IOException;
        ??? = "AdvertisingIdClient cannot reconnect.";
        localIOException1.<init>((String)???);
        throw localIOException1;
      }
    }
    catch (Exception localException)
    {
      ??? = new java/io/IOException;
      localObject6 = "AdvertisingIdClient cannot reconnect.";
      ((IOException)???).<init>((String)localObject6, localException);
      throw ((Throwable)???);
    }
    label126:
    Object localObject4 = zzoH;
    p.a(localObject4);
    localObject4 = zzoI;
    p.a(localObject4);
    try
    {
      localObject4 = new com/google/android/gms/ads/identifier/AdvertisingIdClient$Info;
      ??? = zzoI;
      ??? = ((a)???).a();
      localObject6 = zzoI;
      boolean bool2 = true;
      boolean bool3 = ((a)localObject6).a(bool2);
      ((AdvertisingIdClient.Info)localObject4).<init>((String)???, bool3);
      zzaL();
      return (AdvertisingIdClient.Info)localObject4;
    }
    catch (RemoteException localRemoteException)
    {
      ??? = "AdvertisingIdClient";
      localObject6 = "GMS remote exception ";
      Log.i((String)???, (String)localObject6, localRemoteException);
      IOException localIOException2 = new java/io/IOException;
      ??? = "Remote exception";
      localIOException2.<init>((String)???);
      throw localIOException2;
    }
  }
  
  public void start()
  {
    zzb(true);
  }
  
  protected void zzb(boolean paramBoolean)
  {
    Object localObject1 = "Calling this from your main thread can lead to deadlock";
    p.c((String)localObject1);
    try
    {
      boolean bool = zzoJ;
      if (bool) {
        finish();
      }
      localObject1 = mContext;
      localObject1 = zzp((Context)localObject1);
      zzoH = ((d)localObject1);
      localObject1 = mContext;
      d locald = zzoH;
      localObject1 = zza((Context)localObject1, locald);
      zzoI = ((a)localObject1);
      bool = true;
      zzoJ = bool;
      if (paramBoolean) {
        zzaL();
      }
      return;
    }
    finally {}
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/ads/identifier/AdvertisingIdClient.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */