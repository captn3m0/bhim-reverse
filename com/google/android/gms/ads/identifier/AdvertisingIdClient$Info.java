package com.google.android.gms.ads.identifier;

public final class AdvertisingIdClient$Info
{
  private final String zzoS;
  private final boolean zzoT;
  
  public AdvertisingIdClient$Info(String paramString, boolean paramBoolean)
  {
    zzoS = paramString;
    zzoT = paramBoolean;
  }
  
  public String getId()
  {
    return zzoS;
  }
  
  public boolean isLimitAdTrackingEnabled()
  {
    return zzoT;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append("{");
    String str = zzoS;
    localStringBuilder = localStringBuilder.append(str).append("}");
    boolean bool = zzoT;
    return bool;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/ads/identifier/AdvertisingIdClient$Info.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */