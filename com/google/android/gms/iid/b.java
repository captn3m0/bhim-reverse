package com.google.android.gms.iid;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.gcm.GcmReceiver;
import com.google.android.gms.gcm.c;
import java.io.IOException;

public class b
  extends Service
{
  static String a = "action";
  private static String f = "google.com/iid";
  private static String g = "CMD";
  private static String h = "gcm.googleapis.com/refresh";
  MessengerCompat b;
  BroadcastReceiver c;
  int d;
  int e;
  
  public b()
  {
    Object localObject = new com/google/android/gms/iid/MessengerCompat;
    b.1 local1 = new com/google/android/gms/iid/b$1;
    Looper localLooper = Looper.getMainLooper();
    local1.<init>(this, localLooper);
    ((MessengerCompat)localObject).<init>(local1);
    b = ((MessengerCompat)localObject);
    localObject = new com/google/android/gms/iid/b$2;
    ((b.2)localObject).<init>(this);
    c = ((BroadcastReceiver)localObject);
  }
  
  static void a(Context paramContext)
  {
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.google.android.gms.iid.InstanceID");
    String str = paramContext.getPackageName();
    localIntent.setPackage(str);
    str = g;
    localIntent.putExtra(str, "SYNC");
    paramContext.startService(localIntent);
  }
  
  static void a(Context paramContext, f paramf)
  {
    paramf.b();
    Intent localIntent = new android/content/Intent;
    localIntent.<init>("com.google.android.gms.iid.InstanceID");
    String str = g;
    localIntent.putExtra(str, "RST");
    str = paramContext.getPackageName();
    localIntent.setPackage(str);
    paramContext.startService(localIntent);
  }
  
  private void a(Message paramMessage, int paramInt)
  {
    e.a(this);
    getPackageManager();
    int i = e.c;
    Object localObject1;
    if (paramInt != i)
    {
      i = e.b;
      if (paramInt != i)
      {
        localObject1 = "InstanceID";
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("Message from unexpected caller ").append(paramInt).append(" mine=");
        int j = e.b;
        localObject2 = ((StringBuilder)localObject2).append(j);
        String str = " appid=";
        localObject2 = ((StringBuilder)localObject2).append(str);
        j = e.c;
        localObject2 = j;
        Log.w((String)localObject1, (String)localObject2);
      }
    }
    for (;;)
    {
      return;
      localObject1 = (Intent)obj;
      a((Intent)localObject1);
    }
  }
  
  void a()
  {
    try
    {
      int i = d + -1;
      d = i;
      i = d;
      if (i == 0)
      {
        i = e;
        stopSelf(i);
      }
      String str1 = "InstanceID";
      int j = 3;
      boolean bool = Log.isLoggable(str1, j);
      if (bool)
      {
        str1 = "InstanceID";
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        String str2 = "Stop ";
        localObject2 = ((StringBuilder)localObject2).append(str2);
        int k = d;
        localObject2 = ((StringBuilder)localObject2).append(k);
        str2 = " ";
        localObject2 = ((StringBuilder)localObject2).append(str2);
        k = e;
        localObject2 = ((StringBuilder)localObject2).append(k);
        localObject2 = ((StringBuilder)localObject2).toString();
        Log.d(str1, (String)localObject2);
      }
      return;
    }
    finally {}
  }
  
  void a(int paramInt)
  {
    try
    {
      int i = d + 1;
      d = i;
      i = e;
      if (paramInt > i) {
        e = paramInt;
      }
      return;
    }
    finally {}
  }
  
  public void a(Intent paramIntent)
  {
    int i = 3;
    boolean bool1 = true;
    Bundle localBundle1 = null;
    Object localObject1 = "subtype";
    Object localObject2 = paramIntent.getStringExtra((String)localObject1);
    Object localObject3;
    Object localObject4;
    Object localObject5;
    if (localObject2 == null)
    {
      localObject1 = a.b(this);
      localObject3 = g;
      localObject3 = paramIntent.getStringExtra((String)localObject3);
      localObject4 = paramIntent.getStringExtra("error");
      if (localObject4 == null)
      {
        localObject4 = paramIntent.getStringExtra("registration_id");
        if (localObject4 == null) {
          break label181;
        }
      }
      localObject3 = "InstanceID";
      boolean bool2 = Log.isLoggable((String)localObject3, i);
      if (bool2)
      {
        localObject3 = "InstanceID";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "Register result in service ";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject2 = (String)localObject2;
        Log.d((String)localObject3, (String)localObject2);
      }
      localObject1 = ((a)localObject1).d();
      ((e)localObject1).d(paramIntent);
    }
    for (;;)
    {
      return;
      localObject1 = new android/os/Bundle;
      ((Bundle)localObject1).<init>();
      localObject3 = "subtype";
      ((Bundle)localObject1).putString((String)localObject3, (String)localObject2);
      localObject1 = a.a(this, (Bundle)localObject1);
      break;
      label181:
      localObject4 = "InstanceID";
      boolean bool3 = Log.isLoggable((String)localObject4, i);
      if (bool3)
      {
        localObject4 = "InstanceID";
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        localObject5 = ((StringBuilder)localObject5).append("Service command ").append((String)localObject2).append(" ").append((String)localObject3).append(" ");
        Bundle localBundle2 = paramIntent.getExtras();
        localObject5 = localBundle2;
        Log.d((String)localObject4, (String)localObject5);
      }
      localObject4 = paramIntent.getStringExtra("unregistered");
      if (localObject4 != null)
      {
        localObject3 = ((a)localObject1).c();
        if (localObject2 == null) {
          localObject2 = "";
        }
        ((f)localObject3).e((String)localObject2);
        localObject1 = ((a)localObject1).d();
        ((e)localObject1).d(paramIntent);
      }
      else
      {
        localObject4 = h;
        localObject5 = paramIntent.getStringExtra("from");
        bool3 = ((String)localObject4).equals(localObject5);
        if (bool3)
        {
          localObject1 = ((a)localObject1).c();
          ((f)localObject1).e((String)localObject2);
          a(false);
        }
        else
        {
          localObject4 = "RST";
          bool3 = ((String)localObject4).equals(localObject3);
          if (bool3)
          {
            ((a)localObject1).b();
            a(bool1);
          }
          else
          {
            localObject4 = "RST_FULL";
            bool3 = ((String)localObject4).equals(localObject3);
            if (bool3)
            {
              localObject2 = ((a)localObject1).c();
              boolean bool4 = ((f)localObject2).a();
              if (!bool4)
              {
                localObject1 = ((a)localObject1).c();
                ((f)localObject1).b();
                a(bool1);
              }
            }
            else
            {
              localObject4 = "SYNC";
              bool3 = ((String)localObject4).equals(localObject3);
              if (bool3)
              {
                localObject1 = ((a)localObject1).c();
                ((f)localObject1).e((String)localObject2);
                a(false);
              }
              else
              {
                localObject1 = "PING";
                boolean bool5 = ((String)localObject1).equals(localObject3);
                if (bool5) {
                  try
                  {
                    localObject2 = c.a(this);
                    localObject3 = f;
                    localObject4 = e.b();
                    long l = 0L;
                    localBundle1 = paramIntent.getExtras();
                    ((c)localObject2).a((String)localObject3, (String)localObject4, l, localBundle1);
                  }
                  catch (IOException localIOException)
                  {
                    String str = "InstanceID";
                    localObject2 = "Failed to send ping response";
                    Log.w(str, (String)localObject2);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  public void a(boolean paramBoolean)
  {
    b();
  }
  
  public void b() {}
  
  public IBinder onBind(Intent paramIntent)
  {
    boolean bool;
    if (paramIntent != null)
    {
      localObject = "com.google.android.gms.iid.InstanceID";
      String str = paramIntent.getAction();
      bool = ((String)localObject).equals(str);
      if (!bool) {}
    }
    for (Object localObject = b.a();; localObject = null)
    {
      return (IBinder)localObject;
      bool = false;
    }
  }
  
  public void onCreate()
  {
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("com.google.android.c2dm.intent.REGISTRATION");
    Object localObject = getPackageName();
    localIntentFilter.addCategory((String)localObject);
    localObject = c;
    registerReceiver((BroadcastReceiver)localObject, localIntentFilter, "com.google.android.c2dm.permission.RECEIVE", null);
  }
  
  public void onDestroy()
  {
    BroadcastReceiver localBroadcastReceiver = c;
    unregisterReceiver(localBroadcastReceiver);
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2)
  {
    int i = 2;
    a(paramInt2);
    int j;
    if (paramIntent == null)
    {
      a();
      j = i;
    }
    for (;;)
    {
      return j;
      try
      {
        Object localObject1 = paramIntent.getAction();
        String str = "com.google.android.gms.iid.InstanceID";
        boolean bool = str.equals(localObject1);
        if (bool)
        {
          k = Build.VERSION.SDK_INT;
          int m = 18;
          if (k <= m)
          {
            localObject1 = "GSF";
            localObject1 = paramIntent.getParcelableExtra((String)localObject1);
            localObject1 = (Intent)localObject1;
            if (localObject1 != null)
            {
              startService((Intent)localObject1);
              k = 1;
              a();
              continue;
            }
          }
          a(paramIntent);
        }
        a();
        localObject1 = paramIntent.getStringExtra("from");
        if (localObject1 != null) {
          GcmReceiver.a(paramIntent);
        }
        int k = i;
      }
      finally
      {
        a();
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/iid/b.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */