package com.google.android.gms.iid;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import java.io.IOException;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Map;

public class a
{
  static Map a;
  static String f;
  private static f g;
  private static e h;
  Context b;
  KeyPair c;
  String d = "";
  long e;
  
  static
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    a = localHashMap;
  }
  
  protected a(Context paramContext, String paramString, Bundle paramBundle)
  {
    Context localContext = paramContext.getApplicationContext();
    b = localContext;
    d = paramString;
  }
  
  static int a(Context paramContext)
  {
    int i = 0;
    try
    {
      Object localObject = paramContext.getPackageManager();
      str2 = paramContext.getPackageName();
      localStringBuilder = null;
      localObject = ((PackageManager)localObject).getPackageInfo(str2, 0);
      i = versionCode;
    }
    catch (PackageManager.NameNotFoundException localNameNotFoundException)
    {
      for (;;)
      {
        String str2 = "InstanceID";
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        String str3 = "Never happens: can't find own package ";
        localStringBuilder = localStringBuilder.append(str3);
        String str1 = localNameNotFoundException;
        Log.w(str2, str1);
      }
    }
    return i;
  }
  
  public static a a(Context paramContext, Bundle paramBundle)
  {
    Class localClass = a.class;
    Object localObject1;
    if (paramBundle == null)
    {
      localObject1 = "";
      if (localObject1 != null) {
        break label153;
      }
      localObject1 = "";
    }
    label153:
    for (Object localObject3 = localObject1;; localObject3 = localObject2)
    {
      try
      {
        Object localObject4 = paramContext.getApplicationContext();
        localObject1 = g;
        if (localObject1 == null)
        {
          localObject1 = new com/google/android/gms/iid/f;
          ((f)localObject1).<init>((Context)localObject4);
          g = (f)localObject1;
          localObject1 = new com/google/android/gms/iid/e;
          ((e)localObject1).<init>((Context)localObject4);
          h = (e)localObject1;
        }
        int i = a((Context)localObject4);
        localObject1 = Integer.toString(i);
        f = (String)localObject1;
        localObject1 = a;
        localObject1 = ((Map)localObject1).get(localObject3);
        localObject1 = (a)localObject1;
        if (localObject1 == null)
        {
          localObject1 = new com/google/android/gms/iid/a;
          ((a)localObject1).<init>((Context)localObject4, (String)localObject3, paramBundle);
          localObject4 = a;
          ((Map)localObject4).put(localObject3, localObject1);
        }
        return (a)localObject1;
      }
      finally {}
      localObject1 = "subtype";
      localObject1 = paramBundle.getString((String)localObject1);
      break;
    }
  }
  
  static String a(KeyPair paramKeyPair)
  {
    Object localObject1 = paramKeyPair.getPublic().getEncoded();
    localObject3 = "SHA1";
    try
    {
      localObject3 = MessageDigest.getInstance((String)localObject3);
      localObject1 = ((MessageDigest)localObject3).digest((byte[])localObject1);
      int i = 0;
      localObject3 = null;
      i = (localObject1[0] & 0xF) + 112;
      int j = 0;
      i = (byte)(i & 0xFF);
      localObject1[0] = i;
      i = 0;
      localObject3 = null;
      j = 8;
      int k = 11;
      localObject1 = Base64.encodeToString((byte[])localObject1, 0, j, k);
    }
    catch (NoSuchAlgorithmException localNoSuchAlgorithmException)
    {
      for (;;)
      {
        localObject3 = "Unexpected error, device missing required alghorithms";
        Log.w("InstanceID", (String)localObject3);
        Object localObject2 = null;
      }
    }
    return (String)localObject1;
  }
  
  static String a(byte[] paramArrayOfByte)
  {
    return Base64.encodeToString(paramArrayOfByte, 11);
  }
  
  public static a b(Context paramContext)
  {
    return a(paramContext, null);
  }
  
  public String a(String paramString1, String paramString2, Bundle paramBundle)
  {
    int i = 0;
    String str1 = null;
    Object localObject1 = Looper.getMainLooper();
    Object localObject2 = Looper.myLooper();
    if (localObject1 == localObject2)
    {
      localObject1 = new java/io/IOException;
      ((IOException)localObject1).<init>("MAIN_THREAD");
      throw ((Throwable)localObject1);
    }
    int j = 1;
    boolean bool = e();
    if (bool) {}
    for (String str2 = null; str2 != null; str2 = ((f)localObject2).a(str3, paramString1, paramString2))
    {
      return str2;
      localObject2 = g;
      str3 = d;
    }
    if (paramBundle == null)
    {
      paramBundle = new android/os/Bundle;
      paramBundle.<init>();
    }
    localObject2 = paramBundle.getString("ttl");
    if (localObject2 != null)
    {
      j = 0;
      localObject1 = null;
    }
    localObject2 = "jwt";
    String str3 = paramBundle.getString("type");
    bool = ((String)localObject2).equals(str3);
    if (bool) {}
    for (;;)
    {
      str2 = b(paramString1, paramString2, paramBundle);
      localObject1 = "InstanceID";
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      str3 = "token: ";
      localObject2 = str3 + str2;
      Log.w((String)localObject1, (String)localObject2);
      if ((str2 == null) || (i == 0)) {
        break;
      }
      localObject1 = g;
      str1 = d;
      String str4 = f;
      localObject2 = paramString1;
      str3 = paramString2;
      ((f)localObject1).a(str1, paramString1, paramString2, str2, str4);
      break;
      i = j;
    }
  }
  
  KeyPair a()
  {
    Object localObject = c;
    String str;
    if (localObject == null)
    {
      localObject = g;
      str = d;
      localObject = ((f)localObject).c(str);
      c = ((KeyPair)localObject);
    }
    localObject = c;
    if (localObject == null)
    {
      long l1 = System.currentTimeMillis();
      e = l1;
      localObject = g;
      str = d;
      long l2 = e;
      localObject = ((f)localObject).a(str, l2);
      c = ((KeyPair)localObject);
    }
    return c;
  }
  
  public String b(String paramString1, String paramString2, Bundle paramBundle)
  {
    if (paramString2 != null)
    {
      localObject1 = "scope";
      paramBundle.putString((String)localObject1, paramString2);
    }
    paramBundle.putString("sender", paramString1);
    Object localObject1 = "";
    Object localObject2 = d;
    boolean bool1 = ((String)localObject1).equals(localObject2);
    if (bool1) {}
    for (localObject1 = paramString1;; localObject1 = d)
    {
      localObject2 = "legacy.register";
      boolean bool2 = paramBundle.containsKey((String)localObject2);
      if (!bool2)
      {
        paramBundle.putString("subscription", paramString1);
        paramBundle.putString("subtype", (String)localObject1);
        paramBundle.putString("X-subscription", paramString1);
        localObject2 = "X-subtype";
        paramBundle.putString((String)localObject2, (String)localObject1);
      }
      localObject1 = h;
      localObject2 = a();
      localObject1 = ((e)localObject1).a(paramBundle, (KeyPair)localObject2);
      return h.b((Intent)localObject1);
    }
  }
  
  void b()
  {
    e = 0L;
    f localf = g;
    String str = d;
    localf.d(str);
    c = null;
  }
  
  f c()
  {
    return g;
  }
  
  e d()
  {
    return h;
  }
  
  boolean e()
  {
    boolean bool1 = true;
    Object localObject = g;
    String str = "appVersion";
    localObject = ((f)localObject).a(str);
    boolean bool2;
    if (localObject != null)
    {
      str = f;
      bool2 = ((String)localObject).equals(str);
      if (bool2) {
        break label38;
      }
    }
    for (;;)
    {
      return bool1;
      label38:
      localObject = g;
      str = "lastToken";
      localObject = ((f)localObject).a(str);
      if (localObject != null)
      {
        localObject = Long.valueOf(Long.parseLong((String)localObject));
        long l1 = System.currentTimeMillis() / 1000L;
        long l2 = ((Long)localObject).longValue();
        l1 -= l2;
        l2 = 604800L;
        bool2 = l1 < l2;
        if (!bool2) {
          bool1 = false;
        }
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/google/android/gms/iid/a.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */