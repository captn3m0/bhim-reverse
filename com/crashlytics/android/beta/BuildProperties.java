package com.crashlytics.android.beta;

import java.io.InputStream;
import java.util.Properties;

class BuildProperties
{
  private static final String BUILD_ID = "build_id";
  private static final String PACKAGE_NAME = "package_name";
  private static final String VERSION_CODE = "version_code";
  private static final String VERSION_NAME = "version_name";
  public final String buildId;
  public final String packageName;
  public final String versionCode;
  public final String versionName;
  
  BuildProperties(String paramString1, String paramString2, String paramString3, String paramString4)
  {
    versionCode = paramString1;
    versionName = paramString2;
    buildId = paramString3;
    packageName = paramString4;
  }
  
  public static BuildProperties fromProperties(Properties paramProperties)
  {
    String str1 = paramProperties.getProperty("version_code");
    String str2 = paramProperties.getProperty("version_name");
    String str3 = paramProperties.getProperty("build_id");
    String str4 = paramProperties.getProperty("package_name");
    BuildProperties localBuildProperties = new com/crashlytics/android/beta/BuildProperties;
    localBuildProperties.<init>(str1, str2, str3, str4);
    return localBuildProperties;
  }
  
  public static BuildProperties fromPropertiesStream(InputStream paramInputStream)
  {
    Properties localProperties = new java/util/Properties;
    localProperties.<init>();
    localProperties.load(paramInputStream);
    return fromProperties(localProperties);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/BuildProperties.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */