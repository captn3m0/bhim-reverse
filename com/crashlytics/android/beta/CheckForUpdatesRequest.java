package com.crashlytics.android.beta;

import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.b.a;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.c;
import io.fabric.sdk.android.services.network.d;
import java.util.HashMap;
import java.util.Map;

class CheckForUpdatesRequest
  extends a
{
  static final String BETA_SOURCE = "3";
  static final String BUILD_VERSION = "build_version";
  static final String DISPLAY_VERSION = "display_version";
  static final String HEADER_BETA_TOKEN = "X-CRASHLYTICS-BETA-TOKEN";
  static final String INSTANCE = "instance";
  static final String SDK_ANDROID_DIR_TOKEN_TYPE = "3";
  static final String SOURCE = "source";
  private final CheckForUpdatesResponseTransform responseTransform;
  
  public CheckForUpdatesRequest(h paramh, String paramString1, String paramString2, d paramd, CheckForUpdatesResponseTransform paramCheckForUpdatesResponseTransform)
  {
    super(paramh, paramString1, paramString2, paramd, localc);
    responseTransform = paramCheckForUpdatesResponseTransform;
  }
  
  private HttpRequest applyHeadersTo(HttpRequest paramHttpRequest, String paramString1, String paramString2)
  {
    HttpRequest localHttpRequest = paramHttpRequest.a("Accept", "application/json");
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = ((StringBuilder)localObject).append("Crashlytics Android SDK/");
    String str = kit.getVersion();
    localObject = str;
    localHttpRequest = localHttpRequest.a("User-Agent", (String)localObject).a("X-CRASHLYTICS-DEVELOPER-TOKEN", "470fa2b4ae81cd56ecbcda9735803434cec591fa").a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    localObject = kit.getVersion();
    localHttpRequest = localHttpRequest.a("X-CRASHLYTICS-API-CLIENT-VERSION", (String)localObject).a("X-CRASHLYTICS-API-KEY", paramString1);
    localObject = createBetaTokenHeaderValueFor(paramString2);
    return localHttpRequest.a("X-CRASHLYTICS-BETA-TOKEN", (String)localObject);
  }
  
  static String createBetaTokenHeaderValueFor(String paramString)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    return "3:" + paramString;
  }
  
  private Map getQueryParamsFor(BuildProperties paramBuildProperties)
  {
    HashMap localHashMap = new java/util/HashMap;
    localHashMap.<init>();
    String str = versionCode;
    localHashMap.put("build_version", str);
    str = versionName;
    localHashMap.put("display_version", str);
    str = buildId;
    localHashMap.put("instance", str);
    localHashMap.put("source", "3");
    return localHashMap;
  }
  
  /* Error */
  public CheckForUpdatesResponse invoke(String paramString1, String paramString2, BuildProperties paramBuildProperties)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: aload_3
    //   5: invokespecial 116	com/crashlytics/android/beta/CheckForUpdatesRequest:getQueryParamsFor	(Lcom/crashlytics/android/beta/BuildProperties;)Ljava/util/Map;
    //   8: astore 5
    //   10: aload_0
    //   11: aload 5
    //   13: invokevirtual 120	com/crashlytics/android/beta/CheckForUpdatesRequest:getHttpRequest	(Ljava/util/Map;)Lio/fabric/sdk/android/services/network/HttpRequest;
    //   16: astore 6
    //   18: aload_0
    //   19: aload 6
    //   21: aload_1
    //   22: aload_2
    //   23: invokespecial 124	com/crashlytics/android/beta/CheckForUpdatesRequest:applyHeadersTo	(Lio/fabric/sdk/android/services/network/HttpRequest;Ljava/lang/String;Ljava/lang/String;)Lio/fabric/sdk/android/services/network/HttpRequest;
    //   26: astore 6
    //   28: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   31: astore 7
    //   33: ldc -124
    //   35: astore 8
    //   37: new 52	java/lang/StringBuilder
    //   40: astore 9
    //   42: aload 9
    //   44: invokespecial 55	java/lang/StringBuilder:<init>	()V
    //   47: ldc -122
    //   49: astore 10
    //   51: aload 9
    //   53: aload 10
    //   55: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   58: astore 9
    //   60: aload_0
    //   61: invokevirtual 137	com/crashlytics/android/beta/CheckForUpdatesRequest:getUrl	()Ljava/lang/String;
    //   64: astore 10
    //   66: aload 9
    //   68: aload 10
    //   70: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: astore 9
    //   75: aload 9
    //   77: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   80: astore 9
    //   82: aload 7
    //   84: aload 8
    //   86: aload 9
    //   88: invokeinterface 142 3 0
    //   93: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   96: astore 7
    //   98: ldc -124
    //   100: astore 8
    //   102: new 52	java/lang/StringBuilder
    //   105: astore 9
    //   107: aload 9
    //   109: invokespecial 55	java/lang/StringBuilder:<init>	()V
    //   112: ldc -112
    //   114: astore 10
    //   116: aload 9
    //   118: aload 10
    //   120: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: astore 9
    //   125: aload 9
    //   127: aload 5
    //   129: invokevirtual 147	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   132: astore 5
    //   134: aload 5
    //   136: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   139: astore 5
    //   141: aload 7
    //   143: aload 8
    //   145: aload 5
    //   147: invokeinterface 142 3 0
    //   152: aload 6
    //   154: invokevirtual 151	io/fabric/sdk/android/services/network/HttpRequest:c	()Z
    //   157: istore 11
    //   159: iload 11
    //   161: ifeq +133 -> 294
    //   164: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   167: astore 5
    //   169: ldc -124
    //   171: astore 7
    //   173: ldc -103
    //   175: astore 8
    //   177: aload 5
    //   179: aload 7
    //   181: aload 8
    //   183: invokeinterface 142 3 0
    //   188: new 155	org/json/JSONObject
    //   191: astore 5
    //   193: aload 6
    //   195: invokevirtual 158	io/fabric/sdk/android/services/network/HttpRequest:e	()Ljava/lang/String;
    //   198: astore 7
    //   200: aload 5
    //   202: aload 7
    //   204: invokespecial 161	org/json/JSONObject:<init>	(Ljava/lang/String;)V
    //   207: aload_0
    //   208: getfield 39	com/crashlytics/android/beta/CheckForUpdatesRequest:responseTransform	Lcom/crashlytics/android/beta/CheckForUpdatesResponseTransform;
    //   211: astore 7
    //   213: aload 7
    //   215: aload 5
    //   217: invokevirtual 167	com/crashlytics/android/beta/CheckForUpdatesResponseTransform:fromJson	(Lorg/json/JSONObject;)Lcom/crashlytics/android/beta/CheckForUpdatesResponse;
    //   220: astore 4
    //   222: aload 6
    //   224: ifnull +67 -> 291
    //   227: aload 6
    //   229: ldc -87
    //   231: invokevirtual 172	io/fabric/sdk/android/services/network/HttpRequest:b	(Ljava/lang/String;)Ljava/lang/String;
    //   234: astore 5
    //   236: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   239: astore 6
    //   241: ldc -82
    //   243: astore 7
    //   245: new 52	java/lang/StringBuilder
    //   248: astore 8
    //   250: aload 8
    //   252: invokespecial 55	java/lang/StringBuilder:<init>	()V
    //   255: ldc -80
    //   257: astore 9
    //   259: aload 8
    //   261: aload 9
    //   263: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   266: astore 8
    //   268: aload 8
    //   270: aload 5
    //   272: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   275: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   278: astore 5
    //   280: aload 6
    //   282: aload 7
    //   284: aload 5
    //   286: invokeinterface 142 3 0
    //   291: aload 4
    //   293: areturn
    //   294: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   297: astore 5
    //   299: ldc -124
    //   301: astore 7
    //   303: new 52	java/lang/StringBuilder
    //   306: astore 8
    //   308: aload 8
    //   310: invokespecial 55	java/lang/StringBuilder:<init>	()V
    //   313: ldc -78
    //   315: astore 9
    //   317: aload 8
    //   319: aload 9
    //   321: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   324: astore 8
    //   326: aload 6
    //   328: invokevirtual 181	io/fabric/sdk/android/services/network/HttpRequest:b	()I
    //   331: istore 12
    //   333: aload 8
    //   335: iload 12
    //   337: invokevirtual 184	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   340: astore 8
    //   342: aload 8
    //   344: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   347: astore 8
    //   349: aload 5
    //   351: aload 7
    //   353: aload 8
    //   355: invokeinterface 186 3 0
    //   360: aload 6
    //   362: ifnull -71 -> 291
    //   365: aload 6
    //   367: ldc -87
    //   369: invokevirtual 172	io/fabric/sdk/android/services/network/HttpRequest:b	(Ljava/lang/String;)Ljava/lang/String;
    //   372: astore 5
    //   374: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   377: astore 6
    //   379: ldc -82
    //   381: astore 7
    //   383: new 52	java/lang/StringBuilder
    //   386: astore 8
    //   388: aload 8
    //   390: invokespecial 55	java/lang/StringBuilder:<init>	()V
    //   393: ldc -80
    //   395: astore 9
    //   397: aload 8
    //   399: aload 9
    //   401: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   404: astore 8
    //   406: aload 8
    //   408: aload 5
    //   410: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   413: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   416: astore 5
    //   418: aload 6
    //   420: aload 7
    //   422: aload 5
    //   424: invokeinterface 142 3 0
    //   429: goto -138 -> 291
    //   432: astore 5
    //   434: aconst_null
    //   435: astore 6
    //   437: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   440: astore 7
    //   442: ldc -124
    //   444: astore 8
    //   446: new 52	java/lang/StringBuilder
    //   449: astore 9
    //   451: aload 9
    //   453: invokespecial 55	java/lang/StringBuilder:<init>	()V
    //   456: ldc -68
    //   458: astore 10
    //   460: aload 9
    //   462: aload 10
    //   464: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   467: astore 9
    //   469: aload_0
    //   470: invokevirtual 137	com/crashlytics/android/beta/CheckForUpdatesRequest:getUrl	()Ljava/lang/String;
    //   473: astore 10
    //   475: aload 9
    //   477: aload 10
    //   479: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   482: astore 9
    //   484: aload 9
    //   486: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   489: astore 9
    //   491: aload 7
    //   493: aload 8
    //   495: aload 9
    //   497: aload 5
    //   499: invokeinterface 191 4 0
    //   504: aload 6
    //   506: ifnull -215 -> 291
    //   509: aload 6
    //   511: ldc -87
    //   513: invokevirtual 172	io/fabric/sdk/android/services/network/HttpRequest:b	(Ljava/lang/String;)Ljava/lang/String;
    //   516: astore 5
    //   518: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   521: astore 6
    //   523: ldc -82
    //   525: astore 7
    //   527: new 52	java/lang/StringBuilder
    //   530: astore 8
    //   532: aload 8
    //   534: invokespecial 55	java/lang/StringBuilder:<init>	()V
    //   537: ldc -80
    //   539: astore 9
    //   541: aload 8
    //   543: aload 9
    //   545: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   548: astore 8
    //   550: aload 8
    //   552: aload 5
    //   554: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   557: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   560: astore 5
    //   562: aload 6
    //   564: aload 7
    //   566: aload 5
    //   568: invokeinterface 142 3 0
    //   573: goto -282 -> 291
    //   576: astore 5
    //   578: aconst_null
    //   579: astore 6
    //   581: aload 5
    //   583: astore 4
    //   585: aload 6
    //   587: ifnull +67 -> 654
    //   590: aload 6
    //   592: ldc -87
    //   594: invokevirtual 172	io/fabric/sdk/android/services/network/HttpRequest:b	(Ljava/lang/String;)Ljava/lang/String;
    //   597: astore 5
    //   599: invokestatic 130	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   602: astore 6
    //   604: ldc -82
    //   606: astore 7
    //   608: new 52	java/lang/StringBuilder
    //   611: astore 8
    //   613: aload 8
    //   615: invokespecial 55	java/lang/StringBuilder:<init>	()V
    //   618: ldc -80
    //   620: astore 9
    //   622: aload 8
    //   624: aload 9
    //   626: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   629: astore 8
    //   631: aload 8
    //   633: aload 5
    //   635: invokevirtual 61	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   638: invokevirtual 74	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   641: astore 5
    //   643: aload 6
    //   645: aload 7
    //   647: aload 5
    //   649: invokeinterface 142 3 0
    //   654: aload 4
    //   656: athrow
    //   657: astore 4
    //   659: goto -74 -> 585
    //   662: astore 5
    //   664: goto -227 -> 437
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	667	0	this	CheckForUpdatesRequest
    //   0	667	1	paramString1	String
    //   0	667	2	paramString2	String
    //   0	667	3	paramBuildProperties	BuildProperties
    //   1	654	4	localObject1	Object
    //   657	1	4	localObject2	Object
    //   8	415	5	localObject3	Object
    //   432	66	5	localException1	Exception
    //   516	51	5	str1	String
    //   576	6	5	localObject4	Object
    //   597	51	5	str2	String
    //   662	1	5	localException2	Exception
    //   16	628	6	localObject5	Object
    //   31	615	7	localObject6	Object
    //   35	597	8	localObject7	Object
    //   40	585	9	localObject8	Object
    //   49	429	10	str3	String
    //   157	3	11	bool	boolean
    //   331	5	12	i	int
    // Exception table:
    //   from	to	target	type
    //   4	8	432	java/lang/Exception
    //   11	16	432	java/lang/Exception
    //   4	8	576	finally
    //   11	16	576	finally
    //   22	26	657	finally
    //   28	31	657	finally
    //   37	40	657	finally
    //   42	47	657	finally
    //   53	58	657	finally
    //   60	64	657	finally
    //   68	73	657	finally
    //   75	80	657	finally
    //   86	93	657	finally
    //   93	96	657	finally
    //   102	105	657	finally
    //   107	112	657	finally
    //   118	123	657	finally
    //   127	132	657	finally
    //   134	139	657	finally
    //   145	152	657	finally
    //   152	157	657	finally
    //   164	167	657	finally
    //   181	188	657	finally
    //   188	191	657	finally
    //   193	198	657	finally
    //   202	207	657	finally
    //   207	211	657	finally
    //   215	220	657	finally
    //   294	297	657	finally
    //   303	306	657	finally
    //   308	313	657	finally
    //   319	324	657	finally
    //   326	331	657	finally
    //   335	340	657	finally
    //   342	347	657	finally
    //   353	360	657	finally
    //   437	440	657	finally
    //   446	449	657	finally
    //   451	456	657	finally
    //   462	467	657	finally
    //   469	473	657	finally
    //   477	482	657	finally
    //   484	489	657	finally
    //   497	504	657	finally
    //   22	26	662	java/lang/Exception
    //   28	31	662	java/lang/Exception
    //   37	40	662	java/lang/Exception
    //   42	47	662	java/lang/Exception
    //   53	58	662	java/lang/Exception
    //   60	64	662	java/lang/Exception
    //   68	73	662	java/lang/Exception
    //   75	80	662	java/lang/Exception
    //   86	93	662	java/lang/Exception
    //   93	96	662	java/lang/Exception
    //   102	105	662	java/lang/Exception
    //   107	112	662	java/lang/Exception
    //   118	123	662	java/lang/Exception
    //   127	132	662	java/lang/Exception
    //   134	139	662	java/lang/Exception
    //   145	152	662	java/lang/Exception
    //   152	157	662	java/lang/Exception
    //   164	167	662	java/lang/Exception
    //   181	188	662	java/lang/Exception
    //   188	191	662	java/lang/Exception
    //   193	198	662	java/lang/Exception
    //   202	207	662	java/lang/Exception
    //   207	211	662	java/lang/Exception
    //   215	220	662	java/lang/Exception
    //   294	297	662	java/lang/Exception
    //   303	306	662	java/lang/Exception
    //   308	313	662	java/lang/Exception
    //   319	324	662	java/lang/Exception
    //   326	331	662	java/lang/Exception
    //   335	340	662	java/lang/Exception
    //   342	347	662	java/lang/Exception
    //   353	360	662	java/lang/Exception
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/CheckForUpdatesRequest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */