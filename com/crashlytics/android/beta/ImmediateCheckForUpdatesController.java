package com.crashlytics.android.beta;

class ImmediateCheckForUpdatesController
  extends AbstractCheckForUpdatesController
{
  public ImmediateCheckForUpdatesController()
  {
    super(true);
  }
  
  public boolean isActivityLifecycleTriggered()
  {
    return false;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/ImmediateCheckForUpdatesController.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */