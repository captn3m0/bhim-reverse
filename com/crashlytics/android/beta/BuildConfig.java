package com.crashlytics.android.beta;

public final class BuildConfig
{
  public static final String APPLICATION_ID = "com.crashlytics.android.beta";
  public static final String ARTIFACT_ID = "beta";
  public static final String BUILD_NUMBER = "142";
  public static final String BUILD_TYPE = "release";
  public static final boolean DEBUG = false;
  public static final String FLAVOR = "";
  public static final String GROUP = "com.crashlytics.sdk.android";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.2.2";
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/BuildConfig.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */