package com.crashlytics.android.beta;

import io.fabric.sdk.android.a;
import io.fabric.sdk.android.a.b;
import java.util.concurrent.ExecutorService;

class ActivityLifecycleCheckForUpdatesController
  extends AbstractCheckForUpdatesController
{
  private final a.b callbacks;
  private final ExecutorService executorService;
  
  public ActivityLifecycleCheckForUpdatesController(a parama, ExecutorService paramExecutorService)
  {
    Object localObject = new com/crashlytics/android/beta/ActivityLifecycleCheckForUpdatesController$1;
    ((ActivityLifecycleCheckForUpdatesController.1)localObject).<init>(this);
    callbacks = ((a.b)localObject);
    executorService = paramExecutorService;
    localObject = callbacks;
    parama.a((a.b)localObject);
  }
  
  public boolean isActivityLifecycleTriggered()
  {
    return true;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/ActivityLifecycleCheckForUpdatesController.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */