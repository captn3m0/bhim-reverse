package com.crashlytics.android.beta;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.services.b.g;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.b.o.a;
import io.fabric.sdk.android.services.e.f;
import io.fabric.sdk.android.services.network.d;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class AbstractCheckForUpdatesController
  implements UpdatesController
{
  static final long LAST_UPDATE_CHECK_DEFAULT = 0L;
  static final String LAST_UPDATE_CHECK_KEY = "last_update_check";
  private static final long MILLIS_PER_SECOND = 1000L;
  private Beta beta;
  private f betaSettings;
  private BuildProperties buildProps;
  private Context context;
  private io.fabric.sdk.android.services.b.k currentTimeProvider;
  private final AtomicBoolean externallyReady;
  private d httpRequestFactory;
  private o idManager;
  private final AtomicBoolean initialized;
  private long lastCheckTimeMillis;
  private io.fabric.sdk.android.services.d.c preferenceStore;
  
  public AbstractCheckForUpdatesController()
  {
    this(false);
  }
  
  public AbstractCheckForUpdatesController(boolean paramBoolean)
  {
    AtomicBoolean localAtomicBoolean = new java/util/concurrent/atomic/AtomicBoolean;
    localAtomicBoolean.<init>();
    initialized = localAtomicBoolean;
    lastCheckTimeMillis = 0L;
    localAtomicBoolean = new java/util/concurrent/atomic/AtomicBoolean;
    localAtomicBoolean.<init>(paramBoolean);
    externallyReady = localAtomicBoolean;
  }
  
  private void performUpdateCheck()
  {
    io.fabric.sdk.android.c.h().a("Beta", "Performing update check");
    Object localObject1 = new io/fabric/sdk/android/services/b/g;
    ((g)localObject1).<init>();
    Object localObject2 = context;
    String str1 = ((g)localObject1).a((Context)localObject2);
    localObject1 = idManager.i();
    localObject2 = o.a.c;
    localObject1 = ((Map)localObject1).get(localObject2);
    Object localObject3 = localObject1;
    localObject3 = (String)localObject1;
    localObject1 = new com/crashlytics/android/beta/CheckForUpdatesRequest;
    localObject2 = beta;
    String str2 = beta.getOverridenSpiEndpoint();
    String str3 = betaSettings.a;
    d locald = httpRequestFactory;
    CheckForUpdatesResponseTransform localCheckForUpdatesResponseTransform = new com/crashlytics/android/beta/CheckForUpdatesResponseTransform;
    localCheckForUpdatesResponseTransform.<init>();
    ((CheckForUpdatesRequest)localObject1).<init>((h)localObject2, str2, str3, locald, localCheckForUpdatesResponseTransform);
    localObject2 = buildProps;
    ((CheckForUpdatesRequest)localObject1).invoke(str1, (String)localObject3, (BuildProperties)localObject2);
  }
  
  protected void checkForUpdates()
  {
    for (;;)
    {
      long l1;
      synchronized (preferenceStore)
      {
        Object localObject2 = preferenceStore;
        localObject2 = ((io.fabric.sdk.android.services.d.c)localObject2).a();
        Object localObject4 = "last_update_check";
        boolean bool1 = ((SharedPreferences)localObject2).contains((String)localObject4);
        if (bool1)
        {
          localObject2 = preferenceStore;
          localObject4 = preferenceStore;
          localObject4 = ((io.fabric.sdk.android.services.d.c)localObject4).b();
          String str2 = "last_update_check";
          localObject4 = ((SharedPreferences.Editor)localObject4).remove(str2);
          ((io.fabric.sdk.android.services.d.c)localObject2).a((SharedPreferences.Editor)localObject4);
        }
        localObject2 = currentTimeProvider;
        l1 = ((io.fabric.sdk.android.services.b.k)localObject2).a();
        localObject4 = betaSettings;
        long l2 = b * 1000L;
        io.fabric.sdk.android.k localk2 = io.fabric.sdk.android.c.h();
        Object localObject6 = new java/lang/StringBuilder;
        ((StringBuilder)localObject6).<init>();
        localObject6 = "Check for updates delay: " + l2;
        localk2.a("Beta", (String)localObject6);
        localk2 = io.fabric.sdk.android.c.h();
        localObject6 = new java/lang/StringBuilder;
        ((StringBuilder)localObject6).<init>();
        localObject6 = ((StringBuilder)localObject6).append("Check for updates last check time: ");
        long l3 = getLastCheckTimeMillis();
        localObject6 = l3;
        localk2.a("Beta", (String)localObject6);
        long l4 = getLastCheckTimeMillis();
        l2 += l4;
        localk2 = io.fabric.sdk.android.c.h();
        String str3 = "Beta";
        localObject6 = new java/lang/StringBuilder;
        ((StringBuilder)localObject6).<init>();
        localObject6 = ((StringBuilder)localObject6).append("Check for updates current time: ").append(l1);
        String str4 = ", next check time: ";
        localObject6 = str4 + l2;
        localk2.a(str3, (String)localObject6);
        boolean bool2 = l1 < l2;
        if (bool2) {}
      }
      io.fabric.sdk.android.k localk1 = io.fabric.sdk.android.c.h();
      ??? = "Beta";
      String str1 = "Check for updates next check time was not passed";
      localk1.a((String)???, str1);
    }
  }
  
  long getLastCheckTimeMillis()
  {
    return lastCheckTimeMillis;
  }
  
  public void initialize(Context paramContext, Beta paramBeta, o paramo, f paramf, BuildProperties paramBuildProperties, io.fabric.sdk.android.services.d.c paramc, io.fabric.sdk.android.services.b.k paramk, d paramd)
  {
    context = paramContext;
    beta = paramBeta;
    idManager = paramo;
    betaSettings = paramf;
    buildProps = paramBuildProperties;
    preferenceStore = paramc;
    currentTimeProvider = paramk;
    httpRequestFactory = paramd;
    boolean bool = signalInitialized();
    if (bool) {
      checkForUpdates();
    }
  }
  
  void setLastCheckTimeMillis(long paramLong)
  {
    lastCheckTimeMillis = paramLong;
  }
  
  protected boolean signalExternallyReady()
  {
    externallyReady.set(true);
    return initialized.get();
  }
  
  boolean signalInitialized()
  {
    initialized.set(true);
    return externallyReady.get();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/AbstractCheckForUpdatesController.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */