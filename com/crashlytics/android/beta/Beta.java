package com.crashlytics.android.beta;

import android.app.Application;
import android.content.Context;
import android.os.Build.VERSION;
import android.text.TextUtils;
import io.fabric.sdk.android.a;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.m;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.b.o.a;
import io.fabric.sdk.android.services.b.s;
import io.fabric.sdk.android.services.e.f;
import io.fabric.sdk.android.services.e.q;
import io.fabric.sdk.android.services.e.t;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

public class Beta
  extends h
  implements m
{
  private static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
  private static final String CRASHLYTICS_BUILD_PROPERTIES = "crashlytics-build.properties";
  static final String NO_DEVICE_TOKEN = "";
  public static final String TAG = "Beta";
  private final io.fabric.sdk.android.services.a.b deviceTokenCache;
  private final DeviceTokenLoader deviceTokenLoader;
  private UpdatesController updatesController;
  
  public Beta()
  {
    Object localObject = new io/fabric/sdk/android/services/a/b;
    ((io.fabric.sdk.android.services.a.b)localObject).<init>();
    deviceTokenCache = ((io.fabric.sdk.android.services.a.b)localObject);
    localObject = new com/crashlytics/android/beta/DeviceTokenLoader;
    ((DeviceTokenLoader)localObject).<init>();
    deviceTokenLoader = ((DeviceTokenLoader)localObject);
  }
  
  private String getBetaDeviceToken(Context paramContext, String paramString)
  {
    bool1 = false;
    localObject1 = null;
    try
    {
      localObject2 = deviceTokenCache;
      localObject4 = deviceTokenLoader;
      localObject2 = ((io.fabric.sdk.android.services.a.b)localObject2).a(paramContext, (io.fabric.sdk.android.services.a.d)localObject4);
      localObject2 = (String)localObject2;
      localObject4 = "";
      boolean bool2 = ((String)localObject4).equals(localObject2);
      if (bool2) {
        localObject2 = null;
      }
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2;
        Object localObject4 = c.h();
        String str = "Beta";
        Object localObject5 = "Failed to load the Beta device token";
        ((k)localObject4).e(str, (String)localObject5, localException);
        Object localObject3 = null;
        continue;
        bool1 = false;
        localObject1 = null;
      }
    }
    localObject4 = c.h();
    str = "Beta";
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject5 = ((StringBuilder)localObject1).append("Beta device token present: ");
    bool1 = TextUtils.isEmpty((CharSequence)localObject2);
    if (!bool1)
    {
      bool1 = true;
      localObject1 = bool1;
      ((k)localObject4).a(str, (String)localObject1);
      return (String)localObject2;
    }
  }
  
  private f getBetaSettingsData()
  {
    Object localObject = q.a().b();
    if (localObject != null) {}
    for (localObject = f;; localObject = null) {
      return (f)localObject;
    }
  }
  
  public static Beta getInstance()
  {
    return (Beta)c.a(Beta.class);
  }
  
  /* Error */
  private BuildProperties loadBuildProperties(Context paramContext)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aload_1
    //   3: invokevirtual 116	android/content/Context:getAssets	()Landroid/content/res/AssetManager;
    //   6: astore_3
    //   7: ldc 14
    //   9: astore 4
    //   11: aload_3
    //   12: aload 4
    //   14: invokevirtual 122	android/content/res/AssetManager:open	(Ljava/lang/String;)Ljava/io/InputStream;
    //   17: astore 4
    //   19: aload 4
    //   21: ifnull +346 -> 367
    //   24: aload 4
    //   26: invokestatic 128	com/crashlytics/android/beta/BuildProperties:fromPropertiesStream	(Ljava/io/InputStream;)Lcom/crashlytics/android/beta/BuildProperties;
    //   29: astore_2
    //   30: invokestatic 56	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   33: astore_3
    //   34: ldc 20
    //   36: astore 5
    //   38: new 58	java/lang/StringBuilder
    //   41: astore 6
    //   43: aload 6
    //   45: invokespecial 59	java/lang/StringBuilder:<init>	()V
    //   48: aload_2
    //   49: getfield 131	com/crashlytics/android/beta/BuildProperties:packageName	Ljava/lang/String;
    //   52: astore 7
    //   54: aload 6
    //   56: aload 7
    //   58: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: astore 6
    //   63: ldc -123
    //   65: astore 7
    //   67: aload 6
    //   69: aload 7
    //   71: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   74: astore 6
    //   76: aload_2
    //   77: getfield 136	com/crashlytics/android/beta/BuildProperties:versionName	Ljava/lang/String;
    //   80: astore 7
    //   82: aload 6
    //   84: aload 7
    //   86: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   89: astore 6
    //   91: ldc -118
    //   93: astore 7
    //   95: aload 6
    //   97: aload 7
    //   99: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   102: astore 6
    //   104: aload_2
    //   105: getfield 141	com/crashlytics/android/beta/BuildProperties:versionCode	Ljava/lang/String;
    //   108: astore 7
    //   110: aload 6
    //   112: aload 7
    //   114: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   117: astore 6
    //   119: ldc -113
    //   121: astore 7
    //   123: aload 6
    //   125: aload 7
    //   127: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: astore 6
    //   132: ldc -111
    //   134: astore 7
    //   136: aload 6
    //   138: aload 7
    //   140: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   143: astore 6
    //   145: aload_2
    //   146: getfield 148	com/crashlytics/android/beta/BuildProperties:buildId	Ljava/lang/String;
    //   149: astore 7
    //   151: aload 6
    //   153: aload 7
    //   155: invokevirtual 65	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   158: astore 6
    //   160: aload 6
    //   162: invokevirtual 79	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   165: astore 6
    //   167: aload_3
    //   168: aload 5
    //   170: aload 6
    //   172: invokeinterface 84 3 0
    //   177: aload_2
    //   178: astore_3
    //   179: aload 4
    //   181: ifnull +8 -> 189
    //   184: aload 4
    //   186: invokevirtual 153	java/io/InputStream:close	()V
    //   189: aload_3
    //   190: areturn
    //   191: astore_2
    //   192: invokestatic 56	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   195: astore 4
    //   197: ldc 20
    //   199: astore 5
    //   201: ldc -101
    //   203: astore 6
    //   205: aload 4
    //   207: aload 5
    //   209: aload 6
    //   211: aload_2
    //   212: invokeinterface 90 4 0
    //   217: goto -28 -> 189
    //   220: astore_3
    //   221: aconst_null
    //   222: astore 4
    //   224: aconst_null
    //   225: astore 8
    //   227: aload_3
    //   228: astore_2
    //   229: aconst_null
    //   230: astore_3
    //   231: invokestatic 56	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   234: astore 5
    //   236: ldc 20
    //   238: astore 6
    //   240: ldc -99
    //   242: astore 7
    //   244: aload 5
    //   246: aload 6
    //   248: aload 7
    //   250: aload_2
    //   251: invokeinterface 90 4 0
    //   256: aload 4
    //   258: ifnull -69 -> 189
    //   261: aload 4
    //   263: invokevirtual 153	java/io/InputStream:close	()V
    //   266: goto -77 -> 189
    //   269: astore_2
    //   270: invokestatic 56	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   273: astore 4
    //   275: ldc 20
    //   277: astore 5
    //   279: ldc -101
    //   281: astore 6
    //   283: aload 4
    //   285: aload 5
    //   287: aload 6
    //   289: aload_2
    //   290: invokeinterface 90 4 0
    //   295: goto -106 -> 189
    //   298: astore_3
    //   299: aconst_null
    //   300: astore 4
    //   302: aload 4
    //   304: ifnull +8 -> 312
    //   307: aload 4
    //   309: invokevirtual 153	java/io/InputStream:close	()V
    //   312: aload_3
    //   313: athrow
    //   314: astore_2
    //   315: invokestatic 56	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   318: astore 4
    //   320: ldc 20
    //   322: astore 5
    //   324: ldc -101
    //   326: astore 6
    //   328: aload 4
    //   330: aload 5
    //   332: aload 6
    //   334: aload_2
    //   335: invokeinterface 90 4 0
    //   340: goto -28 -> 312
    //   343: astore_3
    //   344: goto -42 -> 302
    //   347: astore 8
    //   349: aconst_null
    //   350: astore_3
    //   351: aload 8
    //   353: astore_2
    //   354: goto -123 -> 231
    //   357: astore 8
    //   359: aload_2
    //   360: astore_3
    //   361: aload 8
    //   363: astore_2
    //   364: goto -133 -> 231
    //   367: aconst_null
    //   368: astore_3
    //   369: goto -190 -> 179
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	372	0	this	Beta
    //   0	372	1	paramContext	Context
    //   1	177	2	localBuildProperties	BuildProperties
    //   191	21	2	localIOException1	java.io.IOException
    //   228	23	2	localObject1	Object
    //   269	21	2	localIOException2	java.io.IOException
    //   314	21	2	localIOException3	java.io.IOException
    //   353	11	2	localObject2	Object
    //   6	184	3	localObject3	Object
    //   220	8	3	localException1	Exception
    //   230	1	3	localObject4	Object
    //   298	15	3	localObject5	Object
    //   343	1	3	localObject6	Object
    //   350	19	3	localObject7	Object
    //   9	320	4	localObject8	Object
    //   36	295	5	localObject9	Object
    //   41	292	6	localObject10	Object
    //   52	197	7	str	String
    //   225	1	8	localObject11	Object
    //   347	5	8	localException2	Exception
    //   357	5	8	localException3	Exception
    // Exception table:
    //   from	to	target	type
    //   184	189	191	java/io/IOException
    //   2	6	220	java/lang/Exception
    //   12	17	220	java/lang/Exception
    //   261	266	269	java/io/IOException
    //   2	6	298	finally
    //   12	17	298	finally
    //   307	312	314	java/io/IOException
    //   24	29	343	finally
    //   30	33	343	finally
    //   38	41	343	finally
    //   43	48	343	finally
    //   48	52	343	finally
    //   56	61	343	finally
    //   69	74	343	finally
    //   76	80	343	finally
    //   84	89	343	finally
    //   97	102	343	finally
    //   104	108	343	finally
    //   112	117	343	finally
    //   125	130	343	finally
    //   138	143	343	finally
    //   145	149	343	finally
    //   153	158	343	finally
    //   160	165	343	finally
    //   170	177	343	finally
    //   231	234	343	finally
    //   250	256	343	finally
    //   24	29	347	java/lang/Exception
    //   30	33	357	java/lang/Exception
    //   38	41	357	java/lang/Exception
    //   43	48	357	java/lang/Exception
    //   48	52	357	java/lang/Exception
    //   56	61	357	java/lang/Exception
    //   69	74	357	java/lang/Exception
    //   76	80	357	java/lang/Exception
    //   84	89	357	java/lang/Exception
    //   97	102	357	java/lang/Exception
    //   104	108	357	java/lang/Exception
    //   112	117	357	java/lang/Exception
    //   125	130	357	java/lang/Exception
    //   138	143	357	java/lang/Exception
    //   145	149	357	java/lang/Exception
    //   153	158	357	java/lang/Exception
    //   160	165	357	java/lang/Exception
    //   170	177	357	java/lang/Exception
  }
  
  boolean canCheckForUpdates(f paramf, BuildProperties paramBuildProperties)
  {
    String str;
    boolean bool;
    if (paramf != null)
    {
      str = a;
      bool = TextUtils.isEmpty(str);
      if ((!bool) && (paramBuildProperties != null)) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
  
  UpdatesController createUpdatesController(int paramInt, Application paramApplication)
  {
    int i = 14;
    Object localObject;
    if (paramInt >= i)
    {
      a locala = getFabric().e();
      ExecutorService localExecutorService = getFabric().f();
      localObject = new com/crashlytics/android/beta/ActivityLifecycleCheckForUpdatesController;
      ((ActivityLifecycleCheckForUpdatesController)localObject).<init>(locala, localExecutorService);
    }
    for (;;)
    {
      return (UpdatesController)localObject;
      localObject = new com/crashlytics/android/beta/ImmediateCheckForUpdatesController;
      ((ImmediateCheckForUpdatesController)localObject).<init>();
    }
  }
  
  protected Boolean doInBackground()
  {
    Object localObject1 = c.h();
    Object localObject2 = "Beta kit initializing...";
    ((k)localObject1).a("Beta", (String)localObject2);
    Object localObject3 = getContext();
    o localo = getIdManager();
    localObject1 = localo.j();
    localObject1 = getBetaDeviceToken((Context)localObject3, (String)localObject1);
    boolean bool = TextUtils.isEmpty((CharSequence)localObject1);
    if (bool)
    {
      localObject1 = c.h();
      localObject3 = "Beta";
      localObject2 = "A Beta device token was not found for this app";
      ((k)localObject1).a((String)localObject3, (String)localObject2);
      bool = false;
    }
    for (localObject1 = Boolean.valueOf(false);; localObject1 = Boolean.valueOf(bool))
    {
      return (Boolean)localObject1;
      localObject1 = c.h();
      localObject2 = "Beta";
      ((k)localObject1).a((String)localObject2, "Beta device token is present, checking for app updates.");
      f localf = getBetaSettingsData();
      BuildProperties localBuildProperties = loadBuildProperties((Context)localObject3);
      bool = canCheckForUpdates(localf, localBuildProperties);
      if (bool)
      {
        localObject1 = updatesController;
        io.fabric.sdk.android.services.d.d locald = new io/fabric/sdk/android/services/d/d;
        locald.<init>(this);
        s locals = new io/fabric/sdk/android/services/b/s;
        locals.<init>();
        io.fabric.sdk.android.services.network.b localb = new io/fabric/sdk/android/services/network/b;
        localObject2 = c.h();
        localb.<init>((k)localObject2);
        localObject2 = this;
        ((UpdatesController)localObject1).initialize((Context)localObject3, this, localo, localf, localBuildProperties, locald, locals, localb);
      }
      bool = true;
    }
  }
  
  public Map getDeviceIdentifiers()
  {
    String str = getIdManager().j();
    Object localObject = getContext();
    str = getBetaDeviceToken((Context)localObject, str);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>();
    boolean bool = TextUtils.isEmpty(str);
    if (!bool)
    {
      o.a locala = o.a.c;
      ((Map)localObject).put(locala, str);
    }
    return (Map)localObject;
  }
  
  public String getIdentifier()
  {
    return "com.crashlytics.sdk.android:beta";
  }
  
  String getOverridenSpiEndpoint()
  {
    return i.b(getContext(), "com.crashlytics.ApiEndpoint");
  }
  
  public String getVersion()
  {
    return "1.2.2.142";
  }
  
  protected boolean onPreExecute()
  {
    Object localObject = (Application)getContext().getApplicationContext();
    int i = Build.VERSION.SDK_INT;
    localObject = createUpdatesController(i, (Application)localObject);
    updatesController = ((UpdatesController)localObject);
    return true;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/Beta.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */