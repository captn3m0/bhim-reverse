package com.crashlytics.android.beta;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import io.fabric.sdk.android.services.a.d;
import java.io.FileInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class DeviceTokenLoader
  implements d
{
  private static final String BETA_APP_PACKAGE_NAME = "io.crash.air";
  private static final String DIRFACTOR_DEVICE_TOKEN_PREFIX = "assets/com.crashlytics.android.beta/dirfactor-device-token=";
  
  String determineDeviceToken(ZipInputStream paramZipInputStream)
  {
    Object localObject = paramZipInputStream.getNextEntry();
    int i;
    int j;
    if (localObject != null)
    {
      localObject = ((ZipEntry)localObject).getName();
      String str = "assets/com.crashlytics.android.beta/dirfactor-device-token=";
      boolean bool = ((String)localObject).startsWith(str);
      if (bool)
      {
        str = "assets/com.crashlytics.android.beta/dirfactor-device-token=";
        i = str.length();
        j = ((String)localObject).length() + -1;
      }
    }
    for (localObject = ((String)localObject).substring(i, j);; localObject = "") {
      return (String)localObject;
    }
  }
  
  ZipInputStream getZipInputStreamOfApkFrom(Context paramContext, String paramString)
  {
    Object localObject = paramContext.getPackageManager().getApplicationInfo(paramString, 0);
    ZipInputStream localZipInputStream = new java/util/zip/ZipInputStream;
    FileInputStream localFileInputStream = new java/io/FileInputStream;
    localObject = sourceDir;
    localFileInputStream.<init>((String)localObject);
    localZipInputStream.<init>(localFileInputStream);
    return localZipInputStream;
  }
  
  /* Error */
  public String load(Context paramContext)
  {
    // Byte code:
    //   0: invokestatic 81	java/lang/System:nanoTime	()J
    //   3: lstore_2
    //   4: ldc 46
    //   6: astore 4
    //   8: aconst_null
    //   9: astore 5
    //   11: ldc 11
    //   13: astore 6
    //   15: aload_0
    //   16: aload_1
    //   17: aload 6
    //   19: invokevirtual 85	com/crashlytics/android/beta/DeviceTokenLoader:getZipInputStreamOfApkFrom	(Landroid/content/Context;Ljava/lang/String;)Ljava/util/zip/ZipInputStream;
    //   22: astore 5
    //   24: aload_0
    //   25: aload 5
    //   27: invokevirtual 89	com/crashlytics/android/beta/DeviceTokenLoader:determineDeviceToken	(Ljava/util/zip/ZipInputStream;)Ljava/lang/String;
    //   30: astore 4
    //   32: aload 5
    //   34: ifnull +8 -> 42
    //   37: aload 5
    //   39: invokevirtual 92	java/util/zip/ZipInputStream:close	()V
    //   42: invokestatic 81	java/lang/System:nanoTime	()J
    //   45: lload_2
    //   46: lsub
    //   47: l2d
    //   48: ldc2_w 95
    //   51: ddiv
    //   52: dstore 7
    //   54: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   57: astore 5
    //   59: new 106	java/lang/StringBuilder
    //   62: astore 9
    //   64: aload 9
    //   66: invokespecial 107	java/lang/StringBuilder:<init>	()V
    //   69: aload 9
    //   71: ldc 109
    //   73: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: dload 7
    //   78: invokevirtual 116	java/lang/StringBuilder:append	(D)Ljava/lang/StringBuilder;
    //   81: ldc 118
    //   83: invokevirtual 113	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   86: invokevirtual 121	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   89: astore 6
    //   91: aload 5
    //   93: ldc 104
    //   95: aload 6
    //   97: invokeinterface 127 3 0
    //   102: aload 4
    //   104: areturn
    //   105: astore 5
    //   107: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   110: astore 6
    //   112: ldc 104
    //   114: astore 10
    //   116: ldc -127
    //   118: astore 11
    //   120: aload 6
    //   122: aload 10
    //   124: aload 11
    //   126: aload 5
    //   128: invokeinterface 133 4 0
    //   133: goto -91 -> 42
    //   136: astore 6
    //   138: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   141: astore 6
    //   143: ldc 104
    //   145: astore 10
    //   147: ldc -121
    //   149: astore 11
    //   151: aload 6
    //   153: aload 10
    //   155: aload 11
    //   157: invokeinterface 127 3 0
    //   162: aload 5
    //   164: ifnull -122 -> 42
    //   167: aload 5
    //   169: invokevirtual 92	java/util/zip/ZipInputStream:close	()V
    //   172: goto -130 -> 42
    //   175: astore 5
    //   177: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   180: astore 6
    //   182: ldc 104
    //   184: astore 10
    //   186: ldc -127
    //   188: astore 11
    //   190: aload 6
    //   192: aload 10
    //   194: aload 11
    //   196: aload 5
    //   198: invokeinterface 133 4 0
    //   203: goto -161 -> 42
    //   206: astore 12
    //   208: aconst_null
    //   209: astore 6
    //   211: aload 12
    //   213: astore 5
    //   215: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   218: astore 10
    //   220: ldc 104
    //   222: astore 11
    //   224: ldc -119
    //   226: astore 13
    //   228: aload 10
    //   230: aload 11
    //   232: aload 13
    //   234: aload 5
    //   236: invokeinterface 133 4 0
    //   241: aload 6
    //   243: ifnull -201 -> 42
    //   246: aload 6
    //   248: invokevirtual 92	java/util/zip/ZipInputStream:close	()V
    //   251: goto -209 -> 42
    //   254: astore 5
    //   256: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   259: astore 6
    //   261: ldc 104
    //   263: astore 10
    //   265: ldc -127
    //   267: astore 11
    //   269: aload 6
    //   271: aload 10
    //   273: aload 11
    //   275: aload 5
    //   277: invokeinterface 133 4 0
    //   282: goto -240 -> 42
    //   285: astore 12
    //   287: aconst_null
    //   288: astore 6
    //   290: aload 12
    //   292: astore 5
    //   294: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   297: astore 10
    //   299: ldc 104
    //   301: astore 11
    //   303: ldc -117
    //   305: astore 13
    //   307: aload 10
    //   309: aload 11
    //   311: aload 13
    //   313: aload 5
    //   315: invokeinterface 133 4 0
    //   320: aload 6
    //   322: ifnull -280 -> 42
    //   325: aload 6
    //   327: invokevirtual 92	java/util/zip/ZipInputStream:close	()V
    //   330: goto -288 -> 42
    //   333: astore 5
    //   335: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   338: astore 6
    //   340: ldc 104
    //   342: astore 10
    //   344: ldc -127
    //   346: astore 11
    //   348: aload 6
    //   350: aload 10
    //   352: aload 11
    //   354: aload 5
    //   356: invokeinterface 133 4 0
    //   361: goto -319 -> 42
    //   364: astore 4
    //   366: aconst_null
    //   367: astore 6
    //   369: aload 6
    //   371: ifnull +8 -> 379
    //   374: aload 6
    //   376: invokevirtual 92	java/util/zip/ZipInputStream:close	()V
    //   379: aload 4
    //   381: athrow
    //   382: astore 5
    //   384: invokestatic 102	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   387: astore 6
    //   389: ldc 104
    //   391: astore 10
    //   393: ldc -127
    //   395: astore 14
    //   397: aload 6
    //   399: aload 10
    //   401: aload 14
    //   403: aload 5
    //   405: invokeinterface 133 4 0
    //   410: goto -31 -> 379
    //   413: astore 4
    //   415: aload 5
    //   417: astore 6
    //   419: goto -50 -> 369
    //   422: astore 4
    //   424: goto -55 -> 369
    //   427: astore 12
    //   429: aload 5
    //   431: astore 6
    //   433: aload 12
    //   435: astore 5
    //   437: goto -143 -> 294
    //   440: astore 12
    //   442: aload 5
    //   444: astore 6
    //   446: aload 12
    //   448: astore 5
    //   450: goto -235 -> 215
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	453	0	this	DeviceTokenLoader
    //   0	453	1	paramContext	Context
    //   3	43	2	l	long
    //   6	97	4	str1	String
    //   364	16	4	localObject1	Object
    //   413	1	4	localObject2	Object
    //   422	1	4	localObject3	Object
    //   9	83	5	localObject4	Object
    //   105	63	5	localIOException1	java.io.IOException
    //   175	22	5	localIOException2	java.io.IOException
    //   213	22	5	localObject5	Object
    //   254	22	5	localIOException3	java.io.IOException
    //   292	22	5	localObject6	Object
    //   333	22	5	localIOException4	java.io.IOException
    //   382	48	5	localIOException5	java.io.IOException
    //   435	14	5	localObject7	Object
    //   13	108	6	localObject8	Object
    //   136	1	6	localNameNotFoundException	android.content.pm.PackageManager.NameNotFoundException
    //   141	304	6	localObject9	Object
    //   52	25	7	d	double
    //   62	8	9	localStringBuilder	StringBuilder
    //   114	286	10	localObject10	Object
    //   118	235	11	str2	String
    //   206	6	12	localFileNotFoundException1	java.io.FileNotFoundException
    //   285	6	12	localIOException6	java.io.IOException
    //   427	7	12	localIOException7	java.io.IOException
    //   440	7	12	localFileNotFoundException2	java.io.FileNotFoundException
    //   226	86	13	str3	String
    //   395	7	14	str4	String
    // Exception table:
    //   from	to	target	type
    //   37	42	105	java/io/IOException
    //   17	22	136	android/content/pm/PackageManager$NameNotFoundException
    //   25	30	136	android/content/pm/PackageManager$NameNotFoundException
    //   167	172	175	java/io/IOException
    //   17	22	206	java/io/FileNotFoundException
    //   246	251	254	java/io/IOException
    //   17	22	285	java/io/IOException
    //   325	330	333	java/io/IOException
    //   17	22	364	finally
    //   374	379	382	java/io/IOException
    //   25	30	413	finally
    //   138	141	413	finally
    //   155	162	413	finally
    //   215	218	422	finally
    //   234	241	422	finally
    //   294	297	422	finally
    //   313	320	422	finally
    //   25	30	427	java/io/IOException
    //   25	30	440	java/io/FileNotFoundException
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/DeviceTokenLoader.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */