package com.crashlytics.android.beta;

import android.content.Context;
import io.fabric.sdk.android.services.b.k;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.d.c;
import io.fabric.sdk.android.services.e.f;
import io.fabric.sdk.android.services.network.d;

abstract interface UpdatesController
{
  public abstract void initialize(Context paramContext, Beta paramBeta, o paramo, f paramf, BuildProperties paramBuildProperties, c paramc, k paramk, d paramd);
  
  public abstract boolean isActivityLifecycleTriggered();
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/beta/UpdatesController.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */