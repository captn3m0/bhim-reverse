package com.crashlytics.android;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.beta.Beta;
import com.crashlytics.android.core.CrashlyticsCore;
import com.crashlytics.android.core.CrashlyticsListener;
import com.crashlytics.android.core.PinningInfoProvider;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.i;
import io.fabric.sdk.android.k;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

public class Crashlytics
  extends h
  implements i
{
  public static final String TAG = "Crashlytics";
  public final Answers answers;
  public final Beta beta;
  public final CrashlyticsCore core;
  public final Collection kits;
  
  public Crashlytics()
  {
    this(localAnswers, localBeta, localCrashlyticsCore);
  }
  
  Crashlytics(Answers paramAnswers, Beta paramBeta, CrashlyticsCore paramCrashlyticsCore)
  {
    answers = paramAnswers;
    beta = paramBeta;
    core = paramCrashlyticsCore;
    Object localObject = new h[3];
    localObject[0] = paramAnswers;
    localObject[1] = paramBeta;
    localObject[2] = paramCrashlyticsCore;
    localObject = Collections.unmodifiableCollection(Arrays.asList((Object[])localObject));
    kits = ((Collection)localObject);
  }
  
  private static void checkInitialized()
  {
    Object localObject = getInstance();
    if (localObject == null)
    {
      localObject = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject).<init>("Crashlytics must be initialized by calling Fabric.with(Context) prior to calling Crashlytics.getInstance()");
      throw ((Throwable)localObject);
    }
  }
  
  public static Crashlytics getInstance()
  {
    return (Crashlytics)c.a(Crashlytics.class);
  }
  
  public static PinningInfoProvider getPinningInfoProvider()
  {
    checkInitialized();
    return getInstancecore.getPinningInfoProvider();
  }
  
  public static void log(int paramInt, String paramString1, String paramString2)
  {
    checkInitialized();
    getInstancecore.log(paramInt, paramString1, paramString2);
  }
  
  public static void log(String paramString)
  {
    checkInitialized();
    getInstancecore.log(paramString);
  }
  
  public static void logException(Throwable paramThrowable)
  {
    checkInitialized();
    getInstancecore.logException(paramThrowable);
  }
  
  public static void setBool(String paramString, boolean paramBoolean)
  {
    checkInitialized();
    getInstancecore.setBool(paramString, paramBoolean);
  }
  
  public static void setDouble(String paramString, double paramDouble)
  {
    checkInitialized();
    getInstancecore.setDouble(paramString, paramDouble);
  }
  
  public static void setFloat(String paramString, float paramFloat)
  {
    checkInitialized();
    getInstancecore.setFloat(paramString, paramFloat);
  }
  
  public static void setInt(String paramString, int paramInt)
  {
    checkInitialized();
    getInstancecore.setInt(paramString, paramInt);
  }
  
  public static void setLong(String paramString, long paramLong)
  {
    checkInitialized();
    getInstancecore.setLong(paramString, paramLong);
  }
  
  public static void setPinningInfoProvider(PinningInfoProvider paramPinningInfoProvider)
  {
    c.h().d("Crashlytics", "Use of Crashlytics.setPinningInfoProvider is deprecated");
  }
  
  public static void setString(String paramString1, String paramString2)
  {
    checkInitialized();
    getInstancecore.setString(paramString1, paramString2);
  }
  
  public static void setUserEmail(String paramString)
  {
    checkInitialized();
    getInstancecore.setUserEmail(paramString);
  }
  
  public static void setUserIdentifier(String paramString)
  {
    checkInitialized();
    getInstancecore.setUserIdentifier(paramString);
  }
  
  public static void setUserName(String paramString)
  {
    checkInitialized();
    getInstancecore.setUserName(paramString);
  }
  
  public void crash()
  {
    core.crash();
  }
  
  protected Void doInBackground()
  {
    return null;
  }
  
  public boolean getDebugMode()
  {
    c.h().d("Crashlytics", "Use of Crashlytics.getDebugMode is deprecated.");
    getFabric();
    return c.i();
  }
  
  public String getIdentifier()
  {
    return "com.crashlytics.sdk.android:crashlytics";
  }
  
  public Collection getKits()
  {
    return kits;
  }
  
  public String getVersion()
  {
    return "2.6.5.151";
  }
  
  public void setDebugMode(boolean paramBoolean)
  {
    c.h().d("Crashlytics", "Use of Crashlytics.setDebugMode is deprecated.");
  }
  
  public void setListener(CrashlyticsListener paramCrashlyticsListener)
  {
    try
    {
      CrashlyticsCore localCrashlyticsCore = core;
      localCrashlyticsCore.setListener(paramCrashlyticsListener);
      return;
    }
    finally {}
  }
  
  public boolean verifyPinning(URL paramURL)
  {
    return core.verifyPinning(paramURL);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/Crashlytics.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */