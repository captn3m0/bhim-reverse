package com.crashlytics.android.core;

import com.crashlytics.android.core.internal.models.ThreadData.FrameData;

final class NativeCrashWriter$FrameMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private static final int PROTOBUF_TAG = 3;
  private final long address;
  private final String file;
  private final int importance;
  private final long offset;
  private final String symbol;
  
  public NativeCrashWriter$FrameMessage(ThreadData.FrameData paramFrameData)
  {
    super(3, arrayOfProtobufMessage);
    long l = address;
    address = l;
    String str = symbol;
    symbol = str;
    str = file;
    file = str;
    l = offset;
    offset = l;
    int i = importance;
    importance = i;
  }
  
  public int getPropertiesSize()
  {
    long l = address;
    int i = CodedOutputStream.computeUInt64Size(1, l);
    ByteString localByteString = ByteString.copyFromUtf8(symbol);
    int j = CodedOutputStream.computeBytesSize(2, localByteString);
    i += j;
    localByteString = ByteString.copyFromUtf8(file);
    j = CodedOutputStream.computeBytesSize(3, localByteString);
    i += j;
    l = offset;
    j = CodedOutputStream.computeUInt64Size(4, l);
    i += j;
    int k = importance;
    j = CodedOutputStream.computeUInt32Size(5, k);
    return i + j;
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream)
  {
    long l = address;
    paramCodedOutputStream.writeUInt64(1, l);
    ByteString localByteString = ByteString.copyFromUtf8(symbol);
    paramCodedOutputStream.writeBytes(2, localByteString);
    localByteString = ByteString.copyFromUtf8(file);
    paramCodedOutputStream.writeBytes(3, localByteString);
    l = offset;
    paramCodedOutputStream.writeUInt64(4, l);
    int i = importance;
    paramCodedOutputStream.writeUInt32(5, i);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$FrameMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */