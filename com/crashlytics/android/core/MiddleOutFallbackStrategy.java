package com.crashlytics.android.core;

class MiddleOutFallbackStrategy
  implements StackTraceTrimmingStrategy
{
  private final int maximumStackSize;
  private final MiddleOutStrategy middleOutStrategy;
  private final StackTraceTrimmingStrategy[] trimmingStrategies;
  
  public MiddleOutFallbackStrategy(int paramInt, StackTraceTrimmingStrategy... paramVarArgs)
  {
    maximumStackSize = paramInt;
    trimmingStrategies = paramVarArgs;
    MiddleOutStrategy localMiddleOutStrategy = new com/crashlytics/android/core/MiddleOutStrategy;
    localMiddleOutStrategy.<init>(paramInt);
    middleOutStrategy = localMiddleOutStrategy;
  }
  
  public StackTraceElement[] getTrimmedStackTrace(StackTraceElement[] paramArrayOfStackTraceElement)
  {
    int i = paramArrayOfStackTraceElement.length;
    int j = maximumStackSize;
    if (i <= j) {
      return paramArrayOfStackTraceElement;
    }
    StackTraceTrimmingStrategy[] arrayOfStackTraceTrimmingStrategy = trimmingStrategies;
    int k = arrayOfStackTraceTrimmingStrategy.length;
    i = 0;
    j = 0;
    MiddleOutStrategy localMiddleOutStrategy = null;
    Object localObject2;
    for (Object localObject1 = paramArrayOfStackTraceElement;; localObject1 = localObject2)
    {
      if (j < k)
      {
        localObject2 = arrayOfStackTraceTrimmingStrategy[j];
        int m = localObject1.length;
        int n = maximumStackSize;
        if (m > n) {}
      }
      else
      {
        j = localObject1.length;
        int i1 = maximumStackSize;
        if (j > i1)
        {
          localMiddleOutStrategy = middleOutStrategy;
          localObject1 = localMiddleOutStrategy.getTrimmedStackTrace((StackTraceElement[])localObject1);
        }
        paramArrayOfStackTraceElement = (StackTraceElement[])localObject1;
        break;
      }
      localObject2 = ((StackTraceTrimmingStrategy)localObject2).getTrimmedStackTrace(paramArrayOfStackTraceElement);
      i = j + 1;
      j = i;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/MiddleOutFallbackStrategy.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */