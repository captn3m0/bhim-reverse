package com.crashlytics.android.core;

final class ByteString$CodedBuilder
{
  private final byte[] buffer;
  private final CodedOutputStream output;
  
  private ByteString$CodedBuilder(int paramInt)
  {
    Object localObject = new byte[paramInt];
    buffer = ((byte[])localObject);
    localObject = CodedOutputStream.newInstance(buffer);
    output = ((CodedOutputStream)localObject);
  }
  
  public ByteString build()
  {
    output.checkNoSpaceLeft();
    ByteString localByteString = new com/crashlytics/android/core/ByteString;
    byte[] arrayOfByte = buffer;
    localByteString.<init>(arrayOfByte, null);
    return localByteString;
  }
  
  public CodedOutputStream getCodedOutput()
  {
    return output;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/ByteString$CodedBuilder.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */