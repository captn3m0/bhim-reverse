package com.crashlytics.android.core;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

class CrashlyticsUncaughtExceptionHandler$6
  implements Callable
{
  CrashlyticsUncaughtExceptionHandler$6(CrashlyticsUncaughtExceptionHandler paramCrashlyticsUncaughtExceptionHandler, long paramLong, String paramString) {}
  
  public Void call()
  {
    Object localObject = CrashlyticsUncaughtExceptionHandler.access$200(this$0);
    boolean bool = ((AtomicBoolean)localObject).get();
    if (!bool)
    {
      localObject = CrashlyticsUncaughtExceptionHandler.access$300(this$0);
      long l = val$timestamp;
      String str = val$msg;
      ((LogFileManager)localObject).writeToLog(l, str);
    }
    return null;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$6.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */