package com.crashlytics.android.core;

final class NativeCrashWriter$RepeatedMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private final NativeCrashWriter.ProtobufMessage[] messages;
  
  public NativeCrashWriter$RepeatedMessage(NativeCrashWriter.ProtobufMessage... paramVarArgs)
  {
    super(0, arrayOfProtobufMessage);
    messages = paramVarArgs;
  }
  
  public int getSize()
  {
    int i = 0;
    NativeCrashWriter.ProtobufMessage[] arrayOfProtobufMessage = messages;
    int j = arrayOfProtobufMessage.length;
    int k = 0;
    while (i < j)
    {
      NativeCrashWriter.ProtobufMessage localProtobufMessage = arrayOfProtobufMessage[i];
      int m = localProtobufMessage.getSize();
      k += m;
      i += 1;
    }
    return k;
  }
  
  public void write(CodedOutputStream paramCodedOutputStream)
  {
    NativeCrashWriter.ProtobufMessage[] arrayOfProtobufMessage = messages;
    int i = arrayOfProtobufMessage.length;
    int j = 0;
    while (j < i)
    {
      NativeCrashWriter.ProtobufMessage localProtobufMessage = arrayOfProtobufMessage[j];
      localProtobufMessage.write(paramCodedOutputStream);
      j += 1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$RepeatedMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */