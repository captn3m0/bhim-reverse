package com.crashlytics.android.core;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

class CrashlyticsUncaughtExceptionHandler$7
  implements Runnable
{
  CrashlyticsUncaughtExceptionHandler$7(CrashlyticsUncaughtExceptionHandler paramCrashlyticsUncaughtExceptionHandler, Date paramDate, Thread paramThread, Throwable paramThrowable) {}
  
  public void run()
  {
    Object localObject = CrashlyticsUncaughtExceptionHandler.access$200(this$0);
    boolean bool = ((AtomicBoolean)localObject).get();
    if (!bool)
    {
      localObject = this$0;
      Date localDate = val$now;
      Thread localThread = val$thread;
      Throwable localThrowable = val$ex;
      CrashlyticsUncaughtExceptionHandler.access$400((CrashlyticsUncaughtExceptionHandler)localObject, localDate, localThread, localThrowable);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$7.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */