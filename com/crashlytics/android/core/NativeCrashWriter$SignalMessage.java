package com.crashlytics.android.core;

import com.crashlytics.android.core.internal.models.SignalData;

final class NativeCrashWriter$SignalMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private static final int PROTOBUF_TAG = 3;
  private final long sigAddr;
  private final String sigCode;
  private final String sigName;
  
  public NativeCrashWriter$SignalMessage(SignalData paramSignalData)
  {
    super(3, arrayOfProtobufMessage);
    String str = name;
    sigName = str;
    str = code;
    sigCode = str;
    long l = faultAddress;
    sigAddr = l;
  }
  
  public int getPropertiesSize()
  {
    ByteString localByteString1 = ByteString.copyFromUtf8(sigName);
    int i = CodedOutputStream.computeBytesSize(1, localByteString1);
    ByteString localByteString2 = ByteString.copyFromUtf8(sigCode);
    int j = CodedOutputStream.computeBytesSize(2, localByteString2);
    i += j;
    long l = sigAddr;
    j = CodedOutputStream.computeUInt64Size(3, l);
    return i + j;
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream)
  {
    ByteString localByteString = ByteString.copyFromUtf8(sigName);
    paramCodedOutputStream.writeBytes(1, localByteString);
    localByteString = ByteString.copyFromUtf8(sigCode);
    paramCodedOutputStream.writeBytes(2, localByteString);
    long l = sigAddr;
    paramCodedOutputStream.writeUInt64(3, l);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$SignalMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */