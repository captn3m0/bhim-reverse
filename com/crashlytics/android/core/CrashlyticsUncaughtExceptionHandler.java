package com.crashlytics.android.core;

import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Environment;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.l;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.d.a;
import io.fabric.sdk.android.services.e.p;
import java.io.Closeable;
import java.io.File;
import java.io.FilenameFilter;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CrashlyticsUncaughtExceptionHandler
  implements Thread.UncaughtExceptionHandler
{
  private static final int ANALYZER_VERSION = 1;
  static final FilenameFilter ANY_SESSION_FILENAME_FILTER;
  private static final String EVENT_TYPE_CRASH = "crash";
  private static final String EVENT_TYPE_LOGGED = "error";
  private static final String GENERATOR_FORMAT = "Crashlytics Android SDK/%s";
  private static final String[] INITIAL_SESSION_PART_TAGS;
  static final String INVALID_CLS_CACHE_DIR = "invalidClsFiles";
  static final Comparator LARGEST_FILE_NAME_FIRST;
  private static final int MAX_COMPLETE_SESSIONS_COUNT = 4;
  static final int MAX_INVALID_SESSIONS = 4;
  private static final int MAX_LOCAL_LOGGED_EXCEPTIONS = 64;
  static final int MAX_OPEN_SESSIONS = 8;
  static final int MAX_STACK_SIZE = 1024;
  static final int NUM_STACK_REPETITIONS_ALLOWED = 10;
  private static final Map SEND_AT_CRASHTIME_HEADER;
  static final String SESSION_APP_TAG = "SessionApp";
  static final String SESSION_BEGIN_TAG = "BeginSession";
  static final String SESSION_DEVICE_TAG = "SessionDevice";
  static final String SESSION_EVENT_MISSING_BINARY_IMGS_TAG = "SessionMissingBinaryImages";
  static final String SESSION_FATAL_TAG = "SessionCrash";
  static final FilenameFilter SESSION_FILE_FILTER;
  private static final Pattern SESSION_FILE_PATTERN;
  private static final int SESSION_ID_LENGTH = 35;
  static final String SESSION_NON_FATAL_TAG = "SessionEvent";
  static final String SESSION_OS_TAG = "SessionOS";
  static final String SESSION_USER_TAG = "SessionUser";
  static final Comparator SMALLEST_FILE_NAME_FIRST;
  private final CrashlyticsCore crashlyticsCore;
  private final Thread.UncaughtExceptionHandler defaultHandler;
  private final DevicePowerStateListener devicePowerStateListener;
  private final AtomicInteger eventCounter;
  private final CrashlyticsExecutorServiceWrapper executorServiceWrapper;
  private final a fileStore;
  private final o idManager;
  private final AtomicBoolean isHandlingException;
  private final LogFileManager logFileManager;
  private final StackTraceTrimmingStrategy stackTraceTrimmingStrategy;
  private final String unityVersion;
  
  static
  {
    Object localObject = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$1;
    ((CrashlyticsUncaughtExceptionHandler.1)localObject).<init>();
    SESSION_FILE_FILTER = (FilenameFilter)localObject;
    localObject = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$2;
    ((CrashlyticsUncaughtExceptionHandler.2)localObject).<init>();
    LARGEST_FILE_NAME_FIRST = (Comparator)localObject;
    localObject = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$3;
    ((CrashlyticsUncaughtExceptionHandler.3)localObject).<init>();
    SMALLEST_FILE_NAME_FIRST = (Comparator)localObject;
    localObject = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$4;
    ((CrashlyticsUncaughtExceptionHandler.4)localObject).<init>();
    ANY_SESSION_FILENAME_FILTER = (FilenameFilter)localObject;
    SESSION_FILE_PATTERN = Pattern.compile("([\\d|A-Z|a-z]{12}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{4}\\-[\\d|A-Z|a-z]{12}).+");
    SEND_AT_CRASHTIME_HEADER = Collections.singletonMap("X-CRASHLYTICS-SEND-FLAGS", "1");
    localObject = new String[4];
    localObject[0] = "SessionUser";
    localObject[1] = "SessionApp";
    localObject[2] = "SessionOS";
    localObject[3] = "SessionDevice";
    INITIAL_SESSION_PART_TAGS = (String[])localObject;
  }
  
  CrashlyticsUncaughtExceptionHandler(Thread.UncaughtExceptionHandler paramUncaughtExceptionHandler, CrashlyticsExecutorServiceWrapper paramCrashlyticsExecutorServiceWrapper, o paramo, UnityVersionProvider paramUnityVersionProvider, a parama, CrashlyticsCore paramCrashlyticsCore)
  {
    Object localObject1 = new java/util/concurrent/atomic/AtomicInteger;
    ((AtomicInteger)localObject1).<init>(0);
    eventCounter = ((AtomicInteger)localObject1);
    defaultHandler = paramUncaughtExceptionHandler;
    executorServiceWrapper = paramCrashlyticsExecutorServiceWrapper;
    idManager = paramo;
    crashlyticsCore = paramCrashlyticsCore;
    localObject1 = paramUnityVersionProvider.getUnityVersion();
    unityVersion = ((String)localObject1);
    fileStore = parama;
    localObject1 = new java/util/concurrent/atomic/AtomicBoolean;
    ((AtomicBoolean)localObject1).<init>(false);
    isHandlingException = ((AtomicBoolean)localObject1);
    localObject1 = paramCrashlyticsCore.getContext();
    Object localObject2 = new com/crashlytics/android/core/LogFileManager;
    ((LogFileManager)localObject2).<init>((Context)localObject1, parama);
    logFileManager = ((LogFileManager)localObject2);
    localObject2 = new com/crashlytics/android/core/DevicePowerStateListener;
    ((DevicePowerStateListener)localObject2).<init>((Context)localObject1);
    devicePowerStateListener = ((DevicePowerStateListener)localObject2);
    localObject1 = new com/crashlytics/android/core/MiddleOutFallbackStrategy;
    StackTraceTrimmingStrategy[] arrayOfStackTraceTrimmingStrategy = new StackTraceTrimmingStrategy[1];
    RemoveRepeatsStrategy localRemoveRepeatsStrategy = new com/crashlytics/android/core/RemoveRepeatsStrategy;
    localRemoveRepeatsStrategy.<init>(10);
    arrayOfStackTraceTrimmingStrategy[0] = localRemoveRepeatsStrategy;
    ((MiddleOutFallbackStrategy)localObject1).<init>(1024, arrayOfStackTraceTrimmingStrategy);
    stackTraceTrimmingStrategy = ((StackTraceTrimmingStrategy)localObject1);
  }
  
  private void closeOpenSessions(File[] paramArrayOfFile, int paramInt1, int paramInt2)
  {
    Object localObject1 = c.h();
    String str1 = "CrashlyticsCore";
    Object localObject2 = "Closing open sessions.";
    ((k)localObject1).a(str1, (String)localObject2);
    for (;;)
    {
      int i = paramArrayOfFile.length;
      if (paramInt1 >= i) {
        break;
      }
      localObject1 = paramArrayOfFile[paramInt1];
      str1 = getSessionIdFromSessionFile((File)localObject1);
      localObject2 = c.h();
      String str2 = "CrashlyticsCore";
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      String str3 = "Closing session: ";
      localObject3 = str3 + str1;
      ((k)localObject2).a(str2, (String)localObject3);
      writeSessionPartsToSessionFile((File)localObject1, str1, paramInt2);
      paramInt1 += 1;
    }
  }
  
  private void closeWithoutRenamingOrLog(ClsFileOutputStream paramClsFileOutputStream)
  {
    if (paramClsFileOutputStream == null) {}
    for (;;)
    {
      return;
      try
      {
        paramClsFileOutputStream.closeInProgressStream();
      }
      catch (IOException localIOException)
      {
        k localk = c.h();
        String str1 = "CrashlyticsCore";
        String str2 = "Error closing session file stream in the presence of an exception";
        localk.e(str1, str2, localIOException);
      }
    }
  }
  
  private static void copyToCodedOutputStream(InputStream paramInputStream, CodedOutputStream paramCodedOutputStream, int paramInt)
  {
    byte[] arrayOfByte = new byte[paramInt];
    int i = 0;
    for (;;)
    {
      int j = arrayOfByte.length;
      if (i >= j) {
        break;
      }
      j = arrayOfByte.length - i;
      j = paramInputStream.read(arrayOfByte, i, j);
      if (j < 0) {
        break;
      }
      i += j;
    }
    paramCodedOutputStream.writeRawBytes(arrayOfByte);
  }
  
  private void deleteSessionPartFilesFor(String paramString)
  {
    File[] arrayOfFile = listSessionPartFilesFor(paramString);
    int i = arrayOfFile.length;
    int j = 0;
    while (j < i)
    {
      File localFile = arrayOfFile[j];
      localFile.delete();
      j += 1;
    }
  }
  
  private void doCloseSessions(boolean paramBoolean)
  {
    int i;
    Object localObject1;
    int k;
    k localk;
    Object localObject2;
    if (paramBoolean)
    {
      i = 1;
      int j = i + 8;
      trimOpenSessions(j);
      localObject1 = listSortedSessionBeginFiles();
      k = localObject1.length;
      if (k > i) {
        break label67;
      }
      localk = c.h();
      localObject1 = "CrashlyticsCore";
      localObject2 = "No open sessions to be closed.";
      localk.a((String)localObject1, (String)localObject2);
    }
    for (;;)
    {
      return;
      i = 0;
      localk = null;
      break;
      label67:
      localObject2 = getSessionIdFromSessionFile(localObject1[i]);
      writeSessionUser((String)localObject2);
      localObject2 = crashlyticsCore;
      localObject2 = CrashlyticsCore.getSessionSettingsData();
      if (localObject2 == null)
      {
        localk = c.h();
        localObject1 = "CrashlyticsCore";
        localObject2 = "Unable to close session. Settings are not loaded.";
        localk.a((String)localObject1, (String)localObject2);
      }
      else
      {
        k = c;
        closeOpenSessions((File[])localObject1, i, k);
      }
    }
  }
  
  private void doOpenSession()
  {
    Date localDate = new java/util/Date;
    localDate.<init>();
    Object localObject1 = new com/crashlytics/android/core/CLSUUID;
    Object localObject2 = idManager;
    ((CLSUUID)localObject1).<init>((o)localObject2);
    localObject1 = ((CLSUUID)localObject1).toString();
    localObject2 = c.h();
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = "Opening an new session with ID " + (String)localObject1;
    ((k)localObject2).a("CrashlyticsCore", (String)localObject3);
    writeBeginSession((String)localObject1, localDate);
    writeSessionApp((String)localObject1);
    writeSessionOS((String)localObject1);
    writeSessionDevice((String)localObject1);
    logFileManager.setCurrentSession((String)localObject1);
  }
  
  /* Error */
  private void doWriteExternalCrashEvent(SessionEventData paramSessionEventData)
  {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: aconst_null
    //   5: astore 4
    //   7: aload_0
    //   8: invokespecial 370	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getPreviousSessionId	()Ljava/lang/String;
    //   11: astore 5
    //   13: aload 5
    //   15: ifnonnull +52 -> 67
    //   18: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   21: astore 6
    //   23: ldc -7
    //   25: astore_3
    //   26: ldc_w 372
    //   29: astore 5
    //   31: iconst_0
    //   32: istore 7
    //   34: aconst_null
    //   35: astore 8
    //   37: aload 6
    //   39: aload_3
    //   40: aload 5
    //   42: aconst_null
    //   43: invokeinterface 288 4 0
    //   48: aconst_null
    //   49: ldc_w 374
    //   52: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   55: ldc_w 381
    //   58: astore 6
    //   60: aconst_null
    //   61: aload 6
    //   63: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   66: return
    //   67: getstatic 390	java/util/Locale:US	Ljava/util/Locale;
    //   70: astore 8
    //   72: ldc_w 392
    //   75: astore 9
    //   77: iconst_2
    //   78: istore 10
    //   80: iload 10
    //   82: anewarray 4	java/lang/Object
    //   85: astore 11
    //   87: iconst_0
    //   88: istore 12
    //   90: aload_1
    //   91: getfield 398	com/crashlytics/android/core/internal/models/SessionEventData:signal	Lcom/crashlytics/android/core/internal/models/SignalData;
    //   94: astore 13
    //   96: aload 13
    //   98: getfield 403	com/crashlytics/android/core/internal/models/SignalData:code	Ljava/lang/String;
    //   101: astore 13
    //   103: aload 11
    //   105: iconst_0
    //   106: aload 13
    //   108: aastore
    //   109: iconst_1
    //   110: istore 12
    //   112: aload_1
    //   113: getfield 398	com/crashlytics/android/core/internal/models/SessionEventData:signal	Lcom/crashlytics/android/core/internal/models/SignalData;
    //   116: astore 13
    //   118: aload 13
    //   120: getfield 406	com/crashlytics/android/core/internal/models/SignalData:name	Ljava/lang/String;
    //   123: astore 13
    //   125: aload 11
    //   127: iload 12
    //   129: aload 13
    //   131: aastore
    //   132: aload 8
    //   134: aload 9
    //   136: aload 11
    //   138: invokestatic 410	java/lang/String:format	(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    //   141: astore 8
    //   143: aload 5
    //   145: aload 8
    //   147: invokestatic 413	com/crashlytics/android/core/CrashlyticsCore:recordFatalExceptionEvent	(Ljava/lang/String;Ljava/lang/String;)V
    //   150: aload_1
    //   151: getfield 417	com/crashlytics/android/core/internal/models/SessionEventData:binaryImages	[Lcom/crashlytics/android/core/internal/models/BinaryImageData;
    //   154: astore 8
    //   156: aload 8
    //   158: ifnull +180 -> 338
    //   161: aload_1
    //   162: getfield 417	com/crashlytics/android/core/internal/models/SessionEventData:binaryImages	[Lcom/crashlytics/android/core/internal/models/BinaryImageData;
    //   165: astore 8
    //   167: aload 8
    //   169: arraylength
    //   170: istore 7
    //   172: iload 7
    //   174: ifle +164 -> 338
    //   177: iload_2
    //   178: ifeq +168 -> 346
    //   181: ldc 57
    //   183: astore 6
    //   185: new 279	com/crashlytics/android/core/ClsFileOutputStream
    //   188: astore_3
    //   189: aload_0
    //   190: invokespecial 235	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getFilesDir	()Ljava/io/File;
    //   193: astore 8
    //   195: new 263	java/lang/StringBuilder
    //   198: astore 9
    //   200: aload 9
    //   202: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   205: aload 9
    //   207: aload 5
    //   209: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   212: astore 9
    //   214: aload 9
    //   216: aload 6
    //   218: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   221: astore 6
    //   223: aload 6
    //   225: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   228: astore 6
    //   230: aload_3
    //   231: aload 8
    //   233: aload 6
    //   235: invokespecial 420	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   238: aload_3
    //   239: invokestatic 424	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   242: astore 4
    //   244: new 426	com/crashlytics/android/core/MetaDataStore
    //   247: astore 6
    //   249: aload_0
    //   250: invokespecial 235	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getFilesDir	()Ljava/io/File;
    //   253: astore 8
    //   255: aload 6
    //   257: aload 8
    //   259: invokespecial 429	com/crashlytics/android/core/MetaDataStore:<init>	(Ljava/io/File;)V
    //   262: aload 6
    //   264: aload 5
    //   266: invokevirtual 433	com/crashlytics/android/core/MetaDataStore:readKeyData	(Ljava/lang/String;)Ljava/util/Map;
    //   269: astore 6
    //   271: new 187	com/crashlytics/android/core/LogFileManager
    //   274: astore 8
    //   276: aload_0
    //   277: getfield 162	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:crashlyticsCore	Lcom/crashlytics/android/core/CrashlyticsCore;
    //   280: astore 9
    //   282: aload 9
    //   284: invokevirtual 185	com/crashlytics/android/core/CrashlyticsCore:getContext	()Landroid/content/Context;
    //   287: astore 9
    //   289: aload_0
    //   290: getfield 172	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:fileStore	Lio/fabric/sdk/android/services/d/a;
    //   293: astore 11
    //   295: aload 8
    //   297: aload 9
    //   299: aload 11
    //   301: aload 5
    //   303: invokespecial 436	com/crashlytics/android/core/LogFileManager:<init>	(Landroid/content/Context;Lio/fabric/sdk/android/services/d/a;Ljava/lang/String;)V
    //   306: aload_1
    //   307: aload 8
    //   309: aload 6
    //   311: aload 4
    //   313: invokestatic 442	com/crashlytics/android/core/NativeCrashWriter:writeNativeCrash	(Lcom/crashlytics/android/core/internal/models/SessionEventData;Lcom/crashlytics/android/core/LogFileManager;Ljava/util/Map;Lcom/crashlytics/android/core/CodedOutputStream;)V
    //   316: aload 4
    //   318: ldc_w 374
    //   321: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   324: ldc_w 381
    //   327: astore 6
    //   329: aload_3
    //   330: aload 6
    //   332: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   335: goto -269 -> 66
    //   338: iconst_0
    //   339: istore_2
    //   340: aconst_null
    //   341: astore 6
    //   343: goto -166 -> 177
    //   346: ldc 54
    //   348: astore 6
    //   350: goto -165 -> 185
    //   353: astore 6
    //   355: aconst_null
    //   356: astore_3
    //   357: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   360: astore 5
    //   362: ldc -7
    //   364: astore 8
    //   366: ldc_w 444
    //   369: astore 9
    //   371: aload 5
    //   373: aload 8
    //   375: aload 9
    //   377: aload 6
    //   379: invokeinterface 288 4 0
    //   384: aload 4
    //   386: ldc_w 374
    //   389: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   392: ldc_w 381
    //   395: astore 6
    //   397: aload_3
    //   398: aload 6
    //   400: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   403: goto -337 -> 66
    //   406: astore 6
    //   408: aconst_null
    //   409: astore_3
    //   410: aload 4
    //   412: ldc_w 374
    //   415: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   418: aload_3
    //   419: ldc_w 381
    //   422: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   425: aload 6
    //   427: athrow
    //   428: astore 6
    //   430: goto -20 -> 410
    //   433: astore 6
    //   435: goto -78 -> 357
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	438	0	this	CrashlyticsUncaughtExceptionHandler
    //   0	438	1	paramSessionEventData	SessionEventData
    //   1	339	2	i	int
    //   3	416	3	localObject1	Object
    //   5	406	4	localCodedOutputStream	CodedOutputStream
    //   11	361	5	localObject2	Object
    //   21	328	6	localObject3	Object
    //   353	25	6	localException1	Exception
    //   395	4	6	str	String
    //   406	20	6	localObject4	Object
    //   428	1	6	localObject5	Object
    //   433	1	6	localException2	Exception
    //   32	141	7	j	int
    //   35	339	8	localObject6	Object
    //   75	301	9	localObject7	Object
    //   78	3	10	k	int
    //   85	215	11	localObject8	Object
    //   88	40	12	m	int
    //   94	36	13	localObject9	Object
    // Exception table:
    //   from	to	target	type
    //   7	11	353	java/lang/Exception
    //   18	21	353	java/lang/Exception
    //   42	48	353	java/lang/Exception
    //   67	70	353	java/lang/Exception
    //   80	85	353	java/lang/Exception
    //   90	94	353	java/lang/Exception
    //   96	101	353	java/lang/Exception
    //   106	109	353	java/lang/Exception
    //   112	116	353	java/lang/Exception
    //   118	123	353	java/lang/Exception
    //   129	132	353	java/lang/Exception
    //   136	141	353	java/lang/Exception
    //   145	150	353	java/lang/Exception
    //   150	154	353	java/lang/Exception
    //   161	165	353	java/lang/Exception
    //   167	170	353	java/lang/Exception
    //   185	188	353	java/lang/Exception
    //   189	193	353	java/lang/Exception
    //   195	198	353	java/lang/Exception
    //   200	205	353	java/lang/Exception
    //   207	212	353	java/lang/Exception
    //   216	221	353	java/lang/Exception
    //   223	228	353	java/lang/Exception
    //   233	238	353	java/lang/Exception
    //   7	11	406	finally
    //   18	21	406	finally
    //   42	48	406	finally
    //   67	70	406	finally
    //   80	85	406	finally
    //   90	94	406	finally
    //   96	101	406	finally
    //   106	109	406	finally
    //   112	116	406	finally
    //   118	123	406	finally
    //   129	132	406	finally
    //   136	141	406	finally
    //   145	150	406	finally
    //   150	154	406	finally
    //   161	165	406	finally
    //   167	170	406	finally
    //   185	188	406	finally
    //   189	193	406	finally
    //   195	198	406	finally
    //   200	205	406	finally
    //   207	212	406	finally
    //   216	221	406	finally
    //   223	228	406	finally
    //   233	238	406	finally
    //   238	242	428	finally
    //   244	247	428	finally
    //   249	253	428	finally
    //   257	262	428	finally
    //   264	269	428	finally
    //   271	274	428	finally
    //   276	280	428	finally
    //   282	287	428	finally
    //   289	293	428	finally
    //   301	306	428	finally
    //   311	316	428	finally
    //   357	360	428	finally
    //   377	384	428	finally
    //   238	242	433	java/lang/Exception
    //   244	247	433	java/lang/Exception
    //   249	253	433	java/lang/Exception
    //   257	262	433	java/lang/Exception
    //   264	269	433	java/lang/Exception
    //   271	274	433	java/lang/Exception
    //   276	280	433	java/lang/Exception
    //   282	287	433	java/lang/Exception
    //   289	293	433	java/lang/Exception
    //   301	306	433	java/lang/Exception
    //   311	316	433	java/lang/Exception
  }
  
  /* Error */
  private void doWriteNonFatal(Date paramDate, Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: invokespecial 231	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getCurrentSessionId	()Ljava/lang/String;
    //   7: astore 5
    //   9: aload 5
    //   11: ifnonnull +30 -> 41
    //   14: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   17: astore 6
    //   19: ldc -7
    //   21: astore 7
    //   23: ldc_w 448
    //   26: astore 8
    //   28: aload 6
    //   30: aload 7
    //   32: aload 8
    //   34: aconst_null
    //   35: invokeinterface 288 4 0
    //   40: return
    //   41: aload_3
    //   42: invokevirtual 452	java/lang/Object:getClass	()Ljava/lang/Class;
    //   45: invokevirtual 457	java/lang/Class:getName	()Ljava/lang/String;
    //   48: astore 6
    //   50: aload 5
    //   52: aload 6
    //   54: invokestatic 460	com/crashlytics/android/core/CrashlyticsCore:recordLoggedExceptionEvent	(Ljava/lang/String;Ljava/lang/String;)V
    //   57: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   60: astore 6
    //   62: ldc -7
    //   64: astore 7
    //   66: new 263	java/lang/StringBuilder
    //   69: astore 8
    //   71: aload 8
    //   73: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   76: ldc_w 462
    //   79: astore 9
    //   81: aload 8
    //   83: aload 9
    //   85: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: astore 8
    //   90: aload 8
    //   92: aload_3
    //   93: invokevirtual 465	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   96: astore 8
    //   98: ldc_w 467
    //   101: astore 9
    //   103: aload 8
    //   105: aload 9
    //   107: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: astore 8
    //   112: aload_2
    //   113: invokevirtual 470	java/lang/Thread:getName	()Ljava/lang/String;
    //   116: astore 9
    //   118: aload 8
    //   120: aload 9
    //   122: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   125: astore 8
    //   127: aload 8
    //   129: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   132: astore 8
    //   134: aload 6
    //   136: aload 7
    //   138: aload 8
    //   140: invokeinterface 257 3 0
    //   145: aload_0
    //   146: getfield 154	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:eventCounter	Ljava/util/concurrent/atomic/AtomicInteger;
    //   149: astore 6
    //   151: aload 6
    //   153: invokevirtual 474	java/util/concurrent/atomic/AtomicInteger:getAndIncrement	()I
    //   156: istore 10
    //   158: iload 10
    //   160: invokestatic 477	io/fabric/sdk/android/services/b/i:a	(I)Ljava/lang/String;
    //   163: astore 6
    //   165: new 263	java/lang/StringBuilder
    //   168: astore 7
    //   170: aload 7
    //   172: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   175: aload 7
    //   177: aload 5
    //   179: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   182: astore 7
    //   184: ldc 65
    //   186: astore 8
    //   188: aload 7
    //   190: aload 8
    //   192: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   195: astore 7
    //   197: aload 7
    //   199: aload 6
    //   201: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   204: astore 6
    //   206: aload 6
    //   208: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   211: astore 6
    //   213: new 279	com/crashlytics/android/core/ClsFileOutputStream
    //   216: astore 11
    //   218: aload_0
    //   219: invokespecial 235	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getFilesDir	()Ljava/io/File;
    //   222: astore 7
    //   224: aload 11
    //   226: aload 7
    //   228: aload 6
    //   230: invokespecial 420	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   233: aload 11
    //   235: invokestatic 424	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   238: astore 4
    //   240: ldc 19
    //   242: astore 12
    //   244: aload_0
    //   245: astore 6
    //   247: aload_1
    //   248: astore 7
    //   250: aload_2
    //   251: astore 8
    //   253: aload_3
    //   254: astore 9
    //   256: aload_0
    //   257: aload 4
    //   259: aload_1
    //   260: aload_2
    //   261: aload_3
    //   262: aload 12
    //   264: iconst_0
    //   265: invokespecial 481	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:writeSessionEvent	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;Z)V
    //   268: aload 4
    //   270: ldc_w 483
    //   273: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   276: ldc_w 485
    //   279: astore 6
    //   281: aload 11
    //   283: aload 6
    //   285: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   288: bipush 64
    //   290: istore 10
    //   292: aload_0
    //   293: aload 5
    //   295: iload 10
    //   297: invokespecial 490	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:trimSessionEventFiles	(Ljava/lang/String;I)V
    //   300: goto -260 -> 40
    //   303: astore 6
    //   305: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   308: astore 4
    //   310: ldc -7
    //   312: astore 7
    //   314: ldc_w 492
    //   317: astore 8
    //   319: aload 4
    //   321: aload 7
    //   323: aload 8
    //   325: aload 6
    //   327: invokeinterface 288 4 0
    //   332: goto -292 -> 40
    //   335: astore 6
    //   337: aconst_null
    //   338: astore 7
    //   340: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   343: astore 8
    //   345: ldc -7
    //   347: astore 9
    //   349: ldc_w 494
    //   352: astore 12
    //   354: aload 8
    //   356: aload 9
    //   358: aload 12
    //   360: aload 6
    //   362: invokeinterface 288 4 0
    //   367: aload 4
    //   369: ldc_w 483
    //   372: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   375: ldc_w 485
    //   378: astore 6
    //   380: aload 7
    //   382: aload 6
    //   384: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   387: goto -99 -> 288
    //   390: astore 6
    //   392: aconst_null
    //   393: astore 7
    //   395: aload 4
    //   397: ldc_w 483
    //   400: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   403: aload 7
    //   405: ldc_w 485
    //   408: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   411: aload 6
    //   413: athrow
    //   414: astore 6
    //   416: aload 11
    //   418: astore 7
    //   420: goto -25 -> 395
    //   423: astore 6
    //   425: goto -30 -> 395
    //   428: astore 6
    //   430: aload 11
    //   432: astore 7
    //   434: goto -94 -> 340
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	437	0	this	CrashlyticsUncaughtExceptionHandler
    //   0	437	1	paramDate	Date
    //   0	437	2	paramThread	Thread
    //   0	437	3	paramThrowable	Throwable
    //   1	395	4	localObject1	Object
    //   7	287	5	str1	String
    //   17	267	6	localObject2	Object
    //   303	23	6	localException1	Exception
    //   335	26	6	localException2	Exception
    //   378	5	6	str2	String
    //   390	22	6	localObject3	Object
    //   414	1	6	localObject4	Object
    //   423	1	6	localObject5	Object
    //   428	1	6	localException3	Exception
    //   21	412	7	localObject6	Object
    //   26	329	8	localObject7	Object
    //   79	278	9	localObject8	Object
    //   156	140	10	i	int
    //   216	215	11	localClsFileOutputStream	ClsFileOutputStream
    //   242	117	12	str3	String
    // Exception table:
    //   from	to	target	type
    //   295	300	303	java/lang/Exception
    //   57	60	335	java/lang/Exception
    //   66	69	335	java/lang/Exception
    //   71	76	335	java/lang/Exception
    //   83	88	335	java/lang/Exception
    //   92	96	335	java/lang/Exception
    //   105	110	335	java/lang/Exception
    //   112	116	335	java/lang/Exception
    //   120	125	335	java/lang/Exception
    //   127	132	335	java/lang/Exception
    //   138	145	335	java/lang/Exception
    //   145	149	335	java/lang/Exception
    //   151	156	335	java/lang/Exception
    //   158	163	335	java/lang/Exception
    //   165	168	335	java/lang/Exception
    //   170	175	335	java/lang/Exception
    //   177	182	335	java/lang/Exception
    //   190	195	335	java/lang/Exception
    //   199	204	335	java/lang/Exception
    //   206	211	335	java/lang/Exception
    //   213	216	335	java/lang/Exception
    //   218	222	335	java/lang/Exception
    //   228	233	335	java/lang/Exception
    //   57	60	390	finally
    //   66	69	390	finally
    //   71	76	390	finally
    //   83	88	390	finally
    //   92	96	390	finally
    //   105	110	390	finally
    //   112	116	390	finally
    //   120	125	390	finally
    //   127	132	390	finally
    //   138	145	390	finally
    //   145	149	390	finally
    //   151	156	390	finally
    //   158	163	390	finally
    //   165	168	390	finally
    //   170	175	390	finally
    //   177	182	390	finally
    //   190	195	390	finally
    //   199	204	390	finally
    //   206	211	390	finally
    //   213	216	390	finally
    //   218	222	390	finally
    //   228	233	390	finally
    //   233	238	414	finally
    //   264	268	414	finally
    //   340	343	423	finally
    //   360	367	423	finally
    //   233	238	428	java/lang/Exception
    //   264	268	428	java/lang/Exception
  }
  
  private File[] ensureFileArrayNotNull(File[] paramArrayOfFile)
  {
    if (paramArrayOfFile == null) {
      paramArrayOfFile = new File[0];
    }
    return paramArrayOfFile;
  }
  
  private String getCurrentSessionId()
  {
    Object localObject = listSortedSessionBeginFiles();
    int i = localObject.length;
    if (i > 0) {
      i = 0;
    }
    for (localObject = getSessionIdFromSessionFile(localObject[0]);; localObject = null) {
      return (String)localObject;
    }
  }
  
  private File getFilesDir()
  {
    return fileStore.a();
  }
  
  private String getPreviousSessionId()
  {
    int i = 1;
    Object localObject = listSortedSessionBeginFiles();
    int j = localObject.length;
    if (j > i) {}
    for (localObject = getSessionIdFromSessionFile(localObject[i]);; localObject = null) {
      return (String)localObject;
    }
  }
  
  static String getSessionIdFromSessionFile(File paramFile)
  {
    return paramFile.getName().substring(0, 35);
  }
  
  private File[] getTrimmedNonFatalFiles(String paramString, File[] paramArrayOfFile, int paramInt)
  {
    int i = paramArrayOfFile.length;
    if (i > paramInt)
    {
      Object localObject1 = c.h();
      Object localObject2 = Locale.US;
      String str = "Trimming down to %d logged exceptions.";
      int j = 1;
      Object[] arrayOfObject = new Object[j];
      Integer localInteger = Integer.valueOf(paramInt);
      arrayOfObject[0] = localInteger;
      localObject2 = String.format((Locale)localObject2, str, arrayOfObject);
      ((k)localObject1).a("CrashlyticsCore", (String)localObject2);
      trimSessionEventFiles(paramString, paramInt);
      localObject1 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter;
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append(paramString);
      localObject2 = "SessionEvent";
      localObject3 = (String)localObject2;
      ((CrashlyticsUncaughtExceptionHandler.FileNameContainsFilter)localObject1).<init>((String)localObject3);
      paramArrayOfFile = listFilesMatching((FilenameFilter)localObject1);
    }
    return paramArrayOfFile;
  }
  
  private UserMetaData getUserMetaData(String paramString)
  {
    boolean bool = isHandlingException();
    Object localObject1;
    Object localObject2;
    if (bool)
    {
      localObject1 = new com/crashlytics/android/core/UserMetaData;
      localObject2 = crashlyticsCore.getUserIdentifier();
      String str1 = crashlyticsCore.getUserName();
      String str2 = crashlyticsCore.getUserEmail();
      ((UserMetaData)localObject1).<init>((String)localObject2, str1, str2);
    }
    for (;;)
    {
      return (UserMetaData)localObject1;
      localObject1 = new com/crashlytics/android/core/MetaDataStore;
      localObject2 = getFilesDir();
      ((MetaDataStore)localObject1).<init>((File)localObject2);
      localObject1 = ((MetaDataStore)localObject1).readUserData(paramString);
    }
  }
  
  private void handleUncaughtException(Date paramDate, Thread paramThread, Throwable paramThrowable)
  {
    crashlyticsCore.createCrashMarker();
    writeFatal(paramDate, paramThread, paramThrowable);
    doCloseSessions();
    doOpenSession();
    trimSessionFiles();
    CrashlyticsCore localCrashlyticsCore = crashlyticsCore;
    boolean bool = localCrashlyticsCore.shouldPromptUserBeforeSendingCrashReports();
    if (!bool) {
      sendSessionReports();
    }
  }
  
  private File[] listCompleteSessionFiles()
  {
    FilenameFilter localFilenameFilter = SESSION_FILE_FILTER;
    return listFilesMatching(localFilenameFilter);
  }
  
  private File[] listFiles(File paramFile)
  {
    File[] arrayOfFile = paramFile.listFiles();
    return ensureFileArrayNotNull(arrayOfFile);
  }
  
  private File[] listFilesMatching(File paramFile, FilenameFilter paramFilenameFilter)
  {
    File[] arrayOfFile = paramFile.listFiles(paramFilenameFilter);
    return ensureFileArrayNotNull(arrayOfFile);
  }
  
  private File[] listFilesMatching(FilenameFilter paramFilenameFilter)
  {
    File localFile = getFilesDir();
    return listFilesMatching(localFile, paramFilenameFilter);
  }
  
  private File[] listSessionPartFilesFor(String paramString)
  {
    CrashlyticsUncaughtExceptionHandler.SessionPartFileFilter localSessionPartFileFilter = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$SessionPartFileFilter;
    localSessionPartFileFilter.<init>(paramString);
    return listFilesMatching(localSessionPartFileFilter);
  }
  
  private File[] listSortedSessionBeginFiles()
  {
    File[] arrayOfFile = listSessionBeginFiles();
    Comparator localComparator = LARGEST_FILE_NAME_FIRST;
    Arrays.sort(arrayOfFile, localComparator);
    return arrayOfFile;
  }
  
  private void retainSessions(File[] paramArrayOfFile, Set paramSet)
  {
    int i = paramArrayOfFile.length;
    int j = 0;
    k localk = null;
    for (;;)
    {
      File localFile;
      String str1;
      String str3;
      if (j < i)
      {
        localFile = paramArrayOfFile[j];
        str1 = localFile.getName();
        localObject = SESSION_FILE_PATTERN.matcher(str1);
        k = ((Matcher)localObject).matches();
        if (k == 0)
        {
          localk = c.h();
          String str2 = "CrashlyticsCore";
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          str3 = "Deleting unknown file: ";
          localObject = ((StringBuilder)localObject).append(str3);
          str1 = str1;
          localk.a(str2, str1);
          localFile.delete();
        }
      }
      else
      {
        return;
      }
      int k = 1;
      Object localObject = ((Matcher)localObject).group(k);
      boolean bool = paramSet.contains(localObject);
      if (!bool)
      {
        localObject = c.h();
        str3 = "CrashlyticsCore";
        StringBuilder localStringBuilder = new java/lang/StringBuilder;
        localStringBuilder.<init>();
        String str4 = "Trimming session file: ";
        localStringBuilder = localStringBuilder.append(str4);
        str1 = str1;
        ((k)localObject).a(str3, str1);
        localFile.delete();
      }
      j += 1;
    }
  }
  
  private void sendSessionReports()
  {
    File[] arrayOfFile = listCompleteSessionFiles();
    int i = arrayOfFile.length;
    int j = 0;
    while (j < i)
    {
      File localFile = arrayOfFile[j];
      CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
      CrashlyticsUncaughtExceptionHandler.SendSessionRunnable localSendSessionRunnable = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$SendSessionRunnable;
      CrashlyticsCore localCrashlyticsCore = crashlyticsCore;
      localSendSessionRunnable.<init>(localCrashlyticsCore, localFile);
      localCrashlyticsExecutorServiceWrapper.executeAsync(localSendSessionRunnable);
      j += 1;
    }
  }
  
  /* Error */
  private void synthesizeSessionFile(File paramFile1, String paramString, File[] paramArrayOfFile, File paramFile2)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 5
    //   3: iconst_1
    //   4: istore 6
    //   6: aload 4
    //   8: ifnull +226 -> 234
    //   11: new 279	com/crashlytics/android/core/ClsFileOutputStream
    //   14: astore 7
    //   16: aload_0
    //   17: invokespecial 235	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getFilesDir	()Ljava/io/File;
    //   20: astore 8
    //   22: aload 7
    //   24: aload 8
    //   26: aload_2
    //   27: invokespecial 420	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   30: aload 7
    //   32: invokestatic 424	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   35: astore 5
    //   37: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   40: astore 8
    //   42: ldc -7
    //   44: astore 9
    //   46: new 263	java/lang/StringBuilder
    //   49: astore 10
    //   51: aload 10
    //   53: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   56: ldc_w 615
    //   59: astore 11
    //   61: aload 10
    //   63: aload 11
    //   65: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   68: astore 10
    //   70: aload 10
    //   72: aload_2
    //   73: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: astore 10
    //   78: aload 10
    //   80: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   83: astore 10
    //   85: aload 8
    //   87: aload 9
    //   89: aload 10
    //   91: invokeinterface 257 3 0
    //   96: aload 5
    //   98: aload_1
    //   99: invokestatic 619	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:writeToCosFromFile	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/io/File;)V
    //   102: iconst_4
    //   103: istore 12
    //   105: new 342	java/util/Date
    //   108: astore 9
    //   110: aload 9
    //   112: invokespecial 343	java/util/Date:<init>	()V
    //   115: aload 9
    //   117: invokevirtual 623	java/util/Date:getTime	()J
    //   120: lstore 13
    //   122: ldc2_w 624
    //   125: lstore 15
    //   127: lload 13
    //   129: lload 15
    //   131: ldiv
    //   132: lstore 13
    //   134: aload 5
    //   136: iload 12
    //   138: lload 13
    //   140: invokevirtual 631	com/crashlytics/android/core/CodedOutputStream:writeUInt64	(IJ)V
    //   143: iconst_5
    //   144: istore 12
    //   146: aload 5
    //   148: iload 12
    //   150: iload 6
    //   152: invokevirtual 636	com/crashlytics/android/core/CodedOutputStream:writeBool	(IZ)V
    //   155: bipush 11
    //   157: istore 12
    //   159: iconst_1
    //   160: istore 17
    //   162: aload 5
    //   164: iload 12
    //   166: iload 17
    //   168: invokevirtual 641	com/crashlytics/android/core/CodedOutputStream:writeUInt32	(II)V
    //   171: bipush 12
    //   173: istore 12
    //   175: iconst_3
    //   176: istore 17
    //   178: aload 5
    //   180: iload 12
    //   182: iload 17
    //   184: invokevirtual 645	com/crashlytics/android/core/CodedOutputStream:writeEnum	(II)V
    //   187: aload_0
    //   188: aload 5
    //   190: aload_2
    //   191: invokespecial 649	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:writeInitialPartsTo	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/lang/String;)V
    //   194: aload 5
    //   196: aload_3
    //   197: aload_2
    //   198: invokestatic 653	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:writeNonFatalEventsTo	(Lcom/crashlytics/android/core/CodedOutputStream;[Ljava/io/File;Ljava/lang/String;)V
    //   201: iload 6
    //   203: ifeq +10 -> 213
    //   206: aload 5
    //   208: aload 4
    //   210: invokestatic 619	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:writeToCosFromFile	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/io/File;)V
    //   213: aload 5
    //   215: ldc_w 655
    //   218: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   221: ldc_w 657
    //   224: astore 18
    //   226: aload 7
    //   228: aload 18
    //   230: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   233: return
    //   234: iconst_0
    //   235: istore 6
    //   237: aconst_null
    //   238: astore 18
    //   240: goto -229 -> 11
    //   243: astore 18
    //   245: aconst_null
    //   246: astore 7
    //   248: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   251: astore 8
    //   253: ldc -7
    //   255: astore 9
    //   257: new 263	java/lang/StringBuilder
    //   260: astore 10
    //   262: aload 10
    //   264: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   267: ldc_w 659
    //   270: astore 11
    //   272: aload 10
    //   274: aload 11
    //   276: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   279: astore 10
    //   281: aload 10
    //   283: aload_2
    //   284: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   287: astore 10
    //   289: aload 10
    //   291: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   294: astore 10
    //   296: aload 8
    //   298: aload 9
    //   300: aload 10
    //   302: aload 18
    //   304: invokeinterface 288 4 0
    //   309: ldc_w 655
    //   312: astore 18
    //   314: aload 5
    //   316: aload 18
    //   318: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   321: aload_0
    //   322: aload 7
    //   324: invokespecial 663	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:closeWithoutRenamingOrLog	(Lcom/crashlytics/android/core/ClsFileOutputStream;)V
    //   327: goto -94 -> 233
    //   330: astore 18
    //   332: aconst_null
    //   333: astore 7
    //   335: aload 5
    //   337: ldc_w 655
    //   340: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   343: aload 7
    //   345: ldc_w 657
    //   348: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   351: aload 18
    //   353: athrow
    //   354: astore 18
    //   356: goto -21 -> 335
    //   359: astore 18
    //   361: goto -113 -> 248
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	364	0	this	CrashlyticsUncaughtExceptionHandler
    //   0	364	1	paramFile1	File
    //   0	364	2	paramString	String
    //   0	364	3	paramArrayOfFile	File[]
    //   0	364	4	paramFile2	File
    //   1	335	5	localCodedOutputStream	CodedOutputStream
    //   4	232	6	bool	boolean
    //   14	330	7	localClsFileOutputStream	ClsFileOutputStream
    //   20	277	8	localObject1	Object
    //   44	255	9	localObject2	Object
    //   49	252	10	localObject3	Object
    //   59	216	11	str1	String
    //   103	78	12	i	int
    //   120	19	13	l1	long
    //   125	5	15	l2	long
    //   160	23	17	j	int
    //   224	15	18	str2	String
    //   243	60	18	localException1	Exception
    //   312	5	18	str3	String
    //   330	22	18	localObject4	Object
    //   354	1	18	localObject5	Object
    //   359	1	18	localException2	Exception
    // Exception table:
    //   from	to	target	type
    //   11	14	243	java/lang/Exception
    //   16	20	243	java/lang/Exception
    //   26	30	243	java/lang/Exception
    //   11	14	330	finally
    //   16	20	330	finally
    //   26	30	330	finally
    //   30	35	354	finally
    //   37	40	354	finally
    //   46	49	354	finally
    //   51	56	354	finally
    //   63	68	354	finally
    //   72	76	354	finally
    //   78	83	354	finally
    //   89	96	354	finally
    //   98	102	354	finally
    //   105	108	354	finally
    //   110	115	354	finally
    //   115	120	354	finally
    //   129	132	354	finally
    //   138	143	354	finally
    //   150	155	354	finally
    //   166	171	354	finally
    //   182	187	354	finally
    //   190	194	354	finally
    //   197	201	354	finally
    //   208	213	354	finally
    //   248	251	354	finally
    //   257	260	354	finally
    //   262	267	354	finally
    //   274	279	354	finally
    //   283	287	354	finally
    //   289	294	354	finally
    //   302	309	354	finally
    //   30	35	359	java/lang/Exception
    //   37	40	359	java/lang/Exception
    //   46	49	359	java/lang/Exception
    //   51	56	359	java/lang/Exception
    //   63	68	359	java/lang/Exception
    //   72	76	359	java/lang/Exception
    //   78	83	359	java/lang/Exception
    //   89	96	359	java/lang/Exception
    //   98	102	359	java/lang/Exception
    //   105	108	359	java/lang/Exception
    //   110	115	359	java/lang/Exception
    //   115	120	359	java/lang/Exception
    //   129	132	359	java/lang/Exception
    //   138	143	359	java/lang/Exception
    //   150	155	359	java/lang/Exception
    //   166	171	359	java/lang/Exception
    //   182	187	359	java/lang/Exception
    //   190	194	359	java/lang/Exception
    //   197	201	359	java/lang/Exception
    //   208	213	359	java/lang/Exception
  }
  
  private void trimInvalidSessionFiles()
  {
    File localFile = getInvalidFilesDir();
    int i = localFile.exists();
    if (i == 0) {}
    for (;;)
    {
      return;
      Object localObject = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$InvalidPartFileFilter;
      ((CrashlyticsUncaughtExceptionHandler.InvalidPartFileFilter)localObject).<init>();
      File[] arrayOfFile = listFilesMatching(localFile, (FilenameFilter)localObject);
      localObject = Collections.reverseOrder();
      Arrays.sort(arrayOfFile, (Comparator)localObject);
      HashSet localHashSet = new java/util/HashSet;
      localHashSet.<init>();
      i = 0;
      localObject = null;
      for (;;)
      {
        int k = arrayOfFile.length;
        if (i >= k) {
          break;
        }
        int m = localHashSet.size();
        int n = 4;
        if (m >= n) {
          break;
        }
        String str = getSessionIdFromSessionFile(arrayOfFile[i]);
        localHashSet.add(str);
        int j;
        i += 1;
      }
      localObject = listFiles(localFile);
      retainSessions((File[])localObject, localHashSet);
    }
  }
  
  private void trimOpenSessions(int paramInt)
  {
    HashSet localHashSet = new java/util/HashSet;
    localHashSet.<init>();
    File[] arrayOfFile = listSortedSessionBeginFiles();
    int i = arrayOfFile.length;
    int j = Math.min(paramInt, i);
    i = 0;
    Object localObject = null;
    while (i < j)
    {
      String str = getSessionIdFromSessionFile(arrayOfFile[i]);
      localHashSet.add(str);
      i += 1;
    }
    logFileManager.discardOldLogFiles(localHashSet);
    localObject = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$AnySessionPartFileFilter;
    ((CrashlyticsUncaughtExceptionHandler.AnySessionPartFileFilter)localObject).<init>(null);
    localObject = listFilesMatching((FilenameFilter)localObject);
    retainSessions((File[])localObject, localHashSet);
  }
  
  private void trimSessionEventFiles(String paramString, int paramInt)
  {
    File localFile = getFilesDir();
    CrashlyticsUncaughtExceptionHandler.FileNameContainsFilter localFileNameContainsFilter = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter;
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = paramString + "SessionEvent";
    localFileNameContainsFilter.<init>((String)localObject);
    localObject = SMALLEST_FILE_NAME_FIRST;
    Utils.capFileCount(localFile, localFileNameContainsFilter, paramInt, (Comparator)localObject);
  }
  
  private void writeBeginSession(String paramString, Date paramDate)
  {
    CodedOutputStream localCodedOutputStream = null;
    try
    {
      localClsFileOutputStream = new com/crashlytics/android/core/ClsFileOutputStream;
      Object localObject1 = getFilesDir();
      Object localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject4 = ((StringBuilder)localObject4).append(paramString);
      Object localObject5 = "BeginSession";
      localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
      localObject4 = ((StringBuilder)localObject4).toString();
      localClsFileOutputStream.<init>((File)localObject1, (String)localObject4);
      int i;
      Object localObject6;
      long l1;
      long l2;
      i.a(localCodedOutputStream, "Failed to flush to session begin file.");
    }
    finally
    {
      try
      {
        localCodedOutputStream = CodedOutputStream.newInstance(localClsFileOutputStream);
        localObject1 = Locale.US;
        localObject4 = "Crashlytics Android SDK/%s";
        i = 1;
        localObject5 = new Object[i];
        localObject6 = crashlyticsCore;
        localObject6 = ((CrashlyticsCore)localObject6).getVersion();
        localObject5[0] = localObject6;
        localObject1 = String.format((Locale)localObject1, (String)localObject4, (Object[])localObject5);
        l1 = paramDate.getTime();
        l2 = 1000L;
        l1 /= l2;
        SessionProtobufHelper.writeBeginSession(localCodedOutputStream, paramString, (String)localObject1, l1);
        i.a(localCodedOutputStream, "Failed to flush to session begin file.");
        i.a(localClsFileOutputStream, "Failed to close begin session file.");
        return;
      }
      finally
      {
        ClsFileOutputStream localClsFileOutputStream;
        for (;;) {}
      }
      localObject2 = finally;
      localClsFileOutputStream = null;
    }
    i.a(localClsFileOutputStream, "Failed to close begin session file.");
    throw ((Throwable)localObject2);
  }
  
  /* Error */
  private void writeFatal(Date paramDate, Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore 4
    //   3: aload_0
    //   4: invokespecial 231	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getCurrentSessionId	()Ljava/lang/String;
    //   7: astore 5
    //   9: aload 5
    //   11: ifnonnull +51 -> 62
    //   14: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   17: astore 5
    //   19: ldc -7
    //   21: astore 6
    //   23: ldc_w 725
    //   26: astore 7
    //   28: aconst_null
    //   29: astore 8
    //   31: aload 5
    //   33: aload 6
    //   35: aload 7
    //   37: aconst_null
    //   38: invokeinterface 288 4 0
    //   43: aconst_null
    //   44: ldc_w 374
    //   47: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   50: ldc_w 381
    //   53: astore 5
    //   55: aconst_null
    //   56: aload 5
    //   58: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   61: return
    //   62: aload_3
    //   63: invokevirtual 452	java/lang/Object:getClass	()Ljava/lang/Class;
    //   66: astore 6
    //   68: aload 6
    //   70: invokevirtual 457	java/lang/Class:getName	()Ljava/lang/String;
    //   73: astore 6
    //   75: aload 5
    //   77: aload 6
    //   79: invokestatic 413	com/crashlytics/android/core/CrashlyticsCore:recordFatalExceptionEvent	(Ljava/lang/String;Ljava/lang/String;)V
    //   82: new 279	com/crashlytics/android/core/ClsFileOutputStream
    //   85: astore 9
    //   87: aload_0
    //   88: invokespecial 235	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getFilesDir	()Ljava/io/File;
    //   91: astore 6
    //   93: new 263	java/lang/StringBuilder
    //   96: astore 7
    //   98: aload 7
    //   100: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   103: aload 7
    //   105: aload 5
    //   107: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   110: astore 5
    //   112: ldc 57
    //   114: astore 7
    //   116: aload 5
    //   118: aload 7
    //   120: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: astore 5
    //   125: aload 5
    //   127: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   130: astore 5
    //   132: aload 9
    //   134: aload 6
    //   136: aload 5
    //   138: invokespecial 420	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   141: aload 9
    //   143: invokestatic 424	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   146: astore 4
    //   148: ldc 16
    //   150: astore 10
    //   152: iconst_1
    //   153: istore 11
    //   155: aload_0
    //   156: astore 5
    //   158: aload_1
    //   159: astore 6
    //   161: aload_2
    //   162: astore 7
    //   164: aload_3
    //   165: astore 8
    //   167: aload_0
    //   168: aload 4
    //   170: aload_1
    //   171: aload_2
    //   172: aload_3
    //   173: aload 10
    //   175: iload 11
    //   177: invokespecial 481	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:writeSessionEvent	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;Ljava/lang/String;Z)V
    //   180: aload 4
    //   182: ldc_w 374
    //   185: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   188: ldc_w 381
    //   191: astore 5
    //   193: aload 9
    //   195: aload 5
    //   197: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   200: goto -139 -> 61
    //   203: astore 5
    //   205: aconst_null
    //   206: astore 6
    //   208: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   211: astore 7
    //   213: ldc -7
    //   215: astore 8
    //   217: ldc_w 727
    //   220: astore 10
    //   222: aload 7
    //   224: aload 8
    //   226: aload 10
    //   228: aload 5
    //   230: invokeinterface 288 4 0
    //   235: aload 4
    //   237: ldc_w 374
    //   240: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   243: ldc_w 381
    //   246: astore 5
    //   248: aload 6
    //   250: aload 5
    //   252: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   255: goto -194 -> 61
    //   258: astore 5
    //   260: aconst_null
    //   261: astore 6
    //   263: aload 4
    //   265: ldc_w 374
    //   268: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   271: aload 6
    //   273: ldc_w 381
    //   276: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   279: aload 5
    //   281: athrow
    //   282: astore 5
    //   284: aload 9
    //   286: astore 6
    //   288: goto -25 -> 263
    //   291: astore 5
    //   293: goto -30 -> 263
    //   296: astore 5
    //   298: aload 9
    //   300: astore 6
    //   302: goto -94 -> 208
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	305	0	this	CrashlyticsUncaughtExceptionHandler
    //   0	305	1	paramDate	Date
    //   0	305	2	paramThread	Thread
    //   0	305	3	paramThrowable	Throwable
    //   1	263	4	localCodedOutputStream	CodedOutputStream
    //   7	189	5	localObject1	Object
    //   203	26	5	localException1	Exception
    //   246	5	5	str1	String
    //   258	22	5	localObject2	Object
    //   282	1	5	localObject3	Object
    //   291	1	5	localObject4	Object
    //   296	1	5	localException2	Exception
    //   21	280	6	localObject5	Object
    //   26	197	7	localObject6	Object
    //   29	196	8	localObject7	Object
    //   85	214	9	localClsFileOutputStream	ClsFileOutputStream
    //   150	77	10	str2	String
    //   153	23	11	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   3	7	203	java/lang/Exception
    //   14	17	203	java/lang/Exception
    //   37	43	203	java/lang/Exception
    //   62	66	203	java/lang/Exception
    //   68	73	203	java/lang/Exception
    //   77	82	203	java/lang/Exception
    //   82	85	203	java/lang/Exception
    //   87	91	203	java/lang/Exception
    //   93	96	203	java/lang/Exception
    //   98	103	203	java/lang/Exception
    //   105	110	203	java/lang/Exception
    //   118	123	203	java/lang/Exception
    //   125	130	203	java/lang/Exception
    //   136	141	203	java/lang/Exception
    //   3	7	258	finally
    //   14	17	258	finally
    //   37	43	258	finally
    //   62	66	258	finally
    //   68	73	258	finally
    //   77	82	258	finally
    //   82	85	258	finally
    //   87	91	258	finally
    //   93	96	258	finally
    //   98	103	258	finally
    //   105	110	258	finally
    //   118	123	258	finally
    //   125	130	258	finally
    //   136	141	258	finally
    //   141	146	282	finally
    //   175	180	282	finally
    //   208	211	291	finally
    //   228	235	291	finally
    //   141	146	296	java/lang/Exception
    //   175	180	296	java/lang/Exception
  }
  
  private void writeInitialPartsTo(CodedOutputStream paramCodedOutputStream, String paramString)
  {
    String[] arrayOfString = INITIAL_SESSION_PART_TAGS;
    int i = arrayOfString.length;
    int j = 0;
    if (j < i)
    {
      Object localObject1 = arrayOfString[j];
      Object localObject2 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter;
      Object localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = paramString + (String)localObject1;
      ((CrashlyticsUncaughtExceptionHandler.FileNameContainsFilter)localObject2).<init>((String)localObject3);
      localObject2 = listFilesMatching((FilenameFilter)localObject2);
      int k = localObject2.length;
      Object localObject4;
      Object localObject5;
      if (k == 0)
      {
        localObject2 = c.h();
        localObject3 = "CrashlyticsCore";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "Can't find ";
        localObject1 = (String)localObject5 + (String)localObject1 + " data for session ID " + paramString;
        localObject4 = null;
        ((k)localObject2).e((String)localObject3, (String)localObject1, null);
      }
      for (;;)
      {
        j += 1;
        break;
        localObject3 = c.h();
        localObject4 = "CrashlyticsCore";
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        String str = "Collecting ";
        localObject1 = ((StringBuilder)localObject5).append(str).append((String)localObject1);
        localObject5 = " data for session ID ";
        localObject1 = (String)localObject5 + paramString;
        ((k)localObject3).a((String)localObject4, (String)localObject1);
        localObject1 = localObject2[0];
        writeToCosFromFile(paramCodedOutputStream, (File)localObject1);
      }
    }
  }
  
  private static void writeNonFatalEventsTo(CodedOutputStream paramCodedOutputStream, File[] paramArrayOfFile, String paramString)
  {
    int i = 0;
    File localFile = null;
    Comparator localComparator = i.a;
    Arrays.sort(paramArrayOfFile, localComparator);
    int j = paramArrayOfFile.length;
    int k = 0;
    localComparator = null;
    for (;;)
    {
      if (k >= j) {
        return;
      }
      localFile = paramArrayOfFile[k];
      try
      {
        localk = c.h();
        str1 = "CrashlyticsCore";
        localObject = Locale.US;
        String str2 = "Found Non Fatal for session ID %s in %s ";
        int m = 2;
        Object[] arrayOfObject = new Object[m];
        int n = 0;
        arrayOfObject[0] = paramString;
        n = 1;
        String str3 = localFile.getName();
        arrayOfObject[n] = str3;
        localObject = String.format((Locale)localObject, str2, arrayOfObject);
        localk.a(str1, (String)localObject);
        writeToCosFromFile(paramCodedOutputStream, localFile);
      }
      catch (Exception localException)
      {
        for (;;)
        {
          k localk = c.h();
          String str1 = "CrashlyticsCore";
          Object localObject = "Error writting non-fatal to session.";
          localk.e(str1, (String)localObject, localException);
        }
      }
      i = k + 1;
      k = i;
    }
  }
  
  private void writeSessionApp(String paramString)
  {
    Object localObject1 = null;
    try
    {
      localClsFileOutputStream = new com/crashlytics/android/core/ClsFileOutputStream;
      Object localObject4 = getFilesDir();
      localObject8 = new java/lang/StringBuilder;
      ((StringBuilder)localObject8).<init>();
      localObject8 = ((StringBuilder)localObject8).append(paramString);
      Object localObject9 = "SessionApp";
      localObject8 = ((StringBuilder)localObject8).append((String)localObject9);
      localObject8 = ((StringBuilder)localObject8).toString();
      localClsFileOutputStream.<init>((File)localObject4, (String)localObject8);
      Object localObject10;
      Object localObject11;
      Object localObject12;
      int i;
      String str;
      i.a((Flushable)localObject1, "Failed to flush to session app file.");
    }
    finally
    {
      try
      {
        localObject4 = CodedOutputStream.newInstance(localClsFileOutputStream);
      }
      finally
      {
        localObject8 = localClsFileOutputStream;
      }
      try
      {
        localObject1 = idManager;
        localObject1 = ((o)localObject1).c();
        localObject8 = crashlyticsCore;
        localObject8 = ((CrashlyticsCore)localObject8).getApiKey();
        localObject9 = crashlyticsCore;
        localObject9 = ((CrashlyticsCore)localObject9).getVersionCode();
        localObject10 = crashlyticsCore;
        localObject10 = ((CrashlyticsCore)localObject10).getVersionName();
        localObject11 = idManager;
        localObject11 = ((o)localObject11).b();
        localObject12 = crashlyticsCore;
        localObject12 = ((CrashlyticsCore)localObject12).getInstallerPackageName();
        localObject12 = l.a((String)localObject12);
        i = ((l)localObject12).a();
        str = unityVersion;
        SessionProtobufHelper.writeSessionApp((CodedOutputStream)localObject4, (String)localObject1, (String)localObject8, (String)localObject9, (String)localObject10, (String)localObject11, i, str);
        i.a((Flushable)localObject4, "Failed to flush to session app file.");
        i.a(localClsFileOutputStream, "Failed to close session app file.");
        return;
      }
      finally
      {
        localObject8 = localClsFileOutputStream;
        Object localObject13 = localObject6;
        Object localObject7 = localObject2;
        Object localObject3 = localObject13;
        break label198;
      }
      localObject5 = finally;
      localObject8 = null;
    }
    label198:
    i.a((Closeable)localObject8, "Failed to close session app file.");
    throw ((Throwable)localObject5);
  }
  
  /* Error */
  private void writeSessionDevice(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: aconst_null
    //   3: astore_3
    //   4: new 279	com/crashlytics/android/core/ClsFileOutputStream
    //   7: astore 4
    //   9: aload_0
    //   10: invokespecial 235	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getFilesDir	()Ljava/io/File;
    //   13: astore 5
    //   15: new 263	java/lang/StringBuilder
    //   18: astore 6
    //   20: aload 6
    //   22: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   25: aload 6
    //   27: aload_1
    //   28: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   31: astore 6
    //   33: ldc 51
    //   35: astore 7
    //   37: aload 6
    //   39: aload 7
    //   41: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: astore 6
    //   46: aload 6
    //   48: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   51: astore 6
    //   53: aload 4
    //   55: aload 5
    //   57: aload 6
    //   59: invokespecial 420	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   62: aload 4
    //   64: invokestatic 424	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   67: astore_3
    //   68: aload_0
    //   69: getfield 162	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:crashlyticsCore	Lcom/crashlytics/android/core/CrashlyticsCore;
    //   72: astore 5
    //   74: aload 5
    //   76: invokevirtual 185	com/crashlytics/android/core/CrashlyticsCore:getContext	()Landroid/content/Context;
    //   79: astore 5
    //   81: new 774	android/os/StatFs
    //   84: astore 7
    //   86: invokestatic 779	android/os/Environment:getDataDirectory	()Ljava/io/File;
    //   89: astore_2
    //   90: aload_2
    //   91: invokevirtual 782	java/io/File:getPath	()Ljava/lang/String;
    //   94: astore_2
    //   95: aload 7
    //   97: aload_2
    //   98: invokespecial 783	android/os/StatFs:<init>	(Ljava/lang/String;)V
    //   101: aload_0
    //   102: getfield 160	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:idManager	Lio/fabric/sdk/android/services/b/o;
    //   105: astore_2
    //   106: aload_2
    //   107: invokevirtual 785	io/fabric/sdk/android/services/b/o:h	()Ljava/lang/String;
    //   110: astore_2
    //   111: invokestatic 786	io/fabric/sdk/android/services/b/i:a	()I
    //   114: istore 8
    //   116: invokestatic 792	java/lang/Runtime:getRuntime	()Ljava/lang/Runtime;
    //   119: astore 9
    //   121: aload 9
    //   123: invokevirtual 795	java/lang/Runtime:availableProcessors	()I
    //   126: istore 10
    //   128: invokestatic 797	io/fabric/sdk/android/services/b/i:b	()J
    //   131: lstore 11
    //   133: aload 7
    //   135: invokevirtual 800	android/os/StatFs:getBlockCount	()I
    //   138: istore 13
    //   140: iload 13
    //   142: i2l
    //   143: lstore 14
    //   145: aload 7
    //   147: invokevirtual 803	android/os/StatFs:getBlockSize	()I
    //   150: istore 16
    //   152: iload 16
    //   154: i2l
    //   155: lstore 17
    //   157: lload 14
    //   159: lload 17
    //   161: lmul
    //   162: lstore 14
    //   164: aload 5
    //   166: invokestatic 807	io/fabric/sdk/android/services/b/i:f	(Landroid/content/Context;)Z
    //   169: istore 19
    //   171: aload_0
    //   172: getfield 160	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:idManager	Lio/fabric/sdk/android/services/b/o;
    //   175: astore 7
    //   177: aload 7
    //   179: invokevirtual 811	io/fabric/sdk/android/services/b/o:i	()Ljava/util/Map;
    //   182: astore 20
    //   184: aload 5
    //   186: invokestatic 814	io/fabric/sdk/android/services/b/i:h	(Landroid/content/Context;)I
    //   189: istore 21
    //   191: getstatic 819	android/os/Build:MODEL	Ljava/lang/String;
    //   194: astore 7
    //   196: getstatic 822	android/os/Build:MANUFACTURER	Ljava/lang/String;
    //   199: astore 22
    //   201: getstatic 825	android/os/Build:PRODUCT	Ljava/lang/String;
    //   204: astore 23
    //   206: aload_3
    //   207: aload_2
    //   208: iload 8
    //   210: aload 7
    //   212: iload 10
    //   214: lload 11
    //   216: lload 14
    //   218: iload 19
    //   220: aload 20
    //   222: iload 21
    //   224: aload 22
    //   226: aload 23
    //   228: invokestatic 828	com/crashlytics/android/core/SessionProtobufHelper:writeSessionDevice	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/lang/String;ILjava/lang/String;IJJZLjava/util/Map;ILjava/lang/String;Ljava/lang/String;)V
    //   231: aload_3
    //   232: ldc_w 830
    //   235: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   238: aload 4
    //   240: ldc_w 832
    //   243: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   246: return
    //   247: astore 5
    //   249: aload_3
    //   250: ldc_w 830
    //   253: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   256: aload_2
    //   257: ldc_w 832
    //   260: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   263: aload 5
    //   265: athrow
    //   266: astore 5
    //   268: aload 4
    //   270: astore_2
    //   271: goto -22 -> 249
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	274	0	this	CrashlyticsUncaughtExceptionHandler
    //   0	274	1	paramString	String
    //   1	270	2	localObject1	Object
    //   3	247	3	localCodedOutputStream	CodedOutputStream
    //   7	262	4	localClsFileOutputStream	ClsFileOutputStream
    //   13	172	5	localObject2	Object
    //   247	17	5	localObject3	Object
    //   266	1	5	localObject4	Object
    //   18	40	6	localObject5	Object
    //   35	176	7	localObject6	Object
    //   114	95	8	i	int
    //   119	3	9	localRuntime	Runtime
    //   126	87	10	j	int
    //   131	84	11	l1	long
    //   138	3	13	k	int
    //   143	74	14	l2	long
    //   150	3	16	m	int
    //   155	5	17	l3	long
    //   169	50	19	bool	boolean
    //   182	39	20	localMap	Map
    //   189	34	21	n	int
    //   199	26	22	str1	String
    //   204	23	23	str2	String
    // Exception table:
    //   from	to	target	type
    //   4	7	247	finally
    //   9	13	247	finally
    //   15	18	247	finally
    //   20	25	247	finally
    //   27	31	247	finally
    //   39	44	247	finally
    //   46	51	247	finally
    //   57	62	247	finally
    //   62	67	266	finally
    //   68	72	266	finally
    //   74	79	266	finally
    //   81	84	266	finally
    //   86	89	266	finally
    //   90	94	266	finally
    //   97	101	266	finally
    //   101	105	266	finally
    //   106	110	266	finally
    //   111	114	266	finally
    //   116	119	266	finally
    //   121	126	266	finally
    //   128	131	266	finally
    //   133	138	266	finally
    //   145	150	266	finally
    //   164	169	266	finally
    //   171	175	266	finally
    //   177	182	266	finally
    //   184	189	266	finally
    //   191	194	266	finally
    //   196	199	266	finally
    //   201	204	266	finally
    //   226	231	266	finally
  }
  
  private void writeSessionEvent(CodedOutputStream paramCodedOutputStream, Date paramDate, Thread paramThread, Throwable paramThrowable, String paramString, boolean paramBoolean)
  {
    TrimmedThrowableData localTrimmedThrowableData = new com/crashlytics/android/core/TrimmedThrowableData;
    Object localObject1 = stackTraceTrimmingStrategy;
    localTrimmedThrowableData.<init>(paramThrowable, (StackTraceTrimmingStrategy)localObject1);
    Object localObject2 = crashlyticsCore.getContext();
    long l1 = paramDate.getTime();
    long l2 = 1000L;
    long l3 = l1 / l2;
    Float localFloat = i.c((Context)localObject2);
    boolean bool1 = devicePowerStateListener.isPowerConnected();
    int j = i.a((Context)localObject2, bool1);
    boolean bool3 = i.d((Context)localObject2);
    int k = getResourcesgetConfigurationorientation;
    long l4 = i.b();
    long l5 = i.b((Context)localObject2);
    long l6 = l4 - l5;
    long l7 = i.b(Environment.getDataDirectory().getPath());
    ActivityManager.RunningAppProcessInfo localRunningAppProcessInfo = i.a(((Context)localObject2).getPackageName(), (Context)localObject2);
    LinkedList localLinkedList = new java/util/LinkedList;
    localLinkedList.<init>();
    StackTraceElement[] arrayOfStackTraceElement = stacktrace;
    String str1 = crashlyticsCore.getBuildId();
    localObject1 = idManager;
    String str2 = ((o)localObject1).c();
    Object localObject3;
    Object localObject4;
    int m;
    if (paramBoolean)
    {
      localObject3 = Thread.getAllStackTraces();
      arrayOfThread = new Thread[((Map)localObject3).size()];
      bool1 = false;
      localObject1 = null;
      localObject3 = ((Map)localObject3).entrySet();
      localObject4 = ((Set)localObject3).iterator();
      for (m = 0;; m = i)
      {
        bool1 = ((Iterator)localObject4).hasNext();
        if (!bool1) {
          break;
        }
        localObject1 = (Map.Entry)((Iterator)localObject4).next();
        localObject3 = (Thread)((Map.Entry)localObject1).getKey();
        arrayOfThread[m] = localObject3;
        localObject3 = stackTraceTrimmingStrategy;
        localObject1 = (StackTraceElement[])((Map.Entry)localObject1).getValue();
        localObject1 = ((StackTraceTrimmingStrategy)localObject3).getTrimmedStackTrace((StackTraceElement[])localObject1);
        localLinkedList.add(localObject1);
        i = m + 1;
      }
    }
    int i = 0;
    localObject1 = null;
    Thread[] arrayOfThread = new Thread[0];
    localObject1 = "com.crashlytics.CollectCustomKeys";
    boolean bool4 = true;
    boolean bool2 = i.a((Context)localObject2, (String)localObject1, bool4);
    if (!bool2)
    {
      localObject2 = new java/util/TreeMap;
      ((TreeMap)localObject2).<init>();
    }
    for (;;)
    {
      localObject4 = logFileManager;
      localObject1 = paramCodedOutputStream;
      localObject3 = paramString;
      SessionProtobufHelper.writeSessionEvent(paramCodedOutputStream, l3, paramString, localTrimmedThrowableData, paramThread, arrayOfStackTraceElement, arrayOfThread, localLinkedList, (Map)localObject2, (LogFileManager)localObject4, localRunningAppProcessInfo, k, str2, str1, localFloat, j, bool3, l6, l7);
      return;
      localObject1 = crashlyticsCore.getAttributes();
      if (localObject1 != null)
      {
        int n = ((Map)localObject1).size();
        m = 1;
        if (n > m)
        {
          localObject2 = new java/util/TreeMap;
          ((TreeMap)localObject2).<init>((Map)localObject1);
          continue;
        }
      }
      localObject2 = localObject1;
    }
  }
  
  private void writeSessionOS(String paramString)
  {
    CodedOutputStream localCodedOutputStream = null;
    try
    {
      localClsFileOutputStream = new com/crashlytics/android/core/ClsFileOutputStream;
      Object localObject1 = getFilesDir();
      Object localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject4 = ((StringBuilder)localObject4).append(paramString);
      String str = "SessionOS";
      localObject4 = ((StringBuilder)localObject4).append(str);
      localObject4 = ((StringBuilder)localObject4).toString();
      localClsFileOutputStream.<init>((File)localObject1, (String)localObject4);
      boolean bool;
      i.a(localCodedOutputStream, "Failed to flush to session OS file.");
    }
    finally
    {
      try
      {
        localCodedOutputStream = CodedOutputStream.newInstance(localClsFileOutputStream);
        localObject1 = crashlyticsCore;
        localObject1 = ((CrashlyticsCore)localObject1).getContext();
        bool = i.g((Context)localObject1);
        SessionProtobufHelper.writeSessionOS(localCodedOutputStream, bool);
        i.a(localCodedOutputStream, "Failed to flush to session OS file.");
        i.a(localClsFileOutputStream, "Failed to close session OS file.");
        return;
      }
      finally
      {
        ClsFileOutputStream localClsFileOutputStream;
        for (;;) {}
      }
      localObject2 = finally;
      localClsFileOutputStream = null;
    }
    i.a(localClsFileOutputStream, "Failed to close session OS file.");
    throw ((Throwable)localObject2);
  }
  
  private void writeSessionPartsToSessionFile(File paramFile, String paramString, int paramInt)
  {
    int i = 2;
    int j = 1;
    Object localObject1 = null;
    Object localObject2 = c.h();
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    Object localObject4 = "Collecting session parts for ID ";
    localObject3 = (String)localObject4 + paramString;
    ((k)localObject2).a("CrashlyticsCore", (String)localObject3);
    localObject2 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter;
    Object localObject5 = new java/lang/StringBuilder;
    ((StringBuilder)localObject5).<init>();
    localObject5 = paramString + "SessionCrash";
    ((CrashlyticsUncaughtExceptionHandler.FileNameContainsFilter)localObject2).<init>((String)localObject5);
    localObject3 = listFilesMatching((FilenameFilter)localObject2);
    label257:
    Object localObject9;
    if (localObject3 != null)
    {
      int k = localObject3.length;
      if (k > 0)
      {
        k = j;
        localObject5 = c.h();
        Object localObject6 = Locale.US;
        String str = "Session %s has fatal exception: %s";
        Object localObject7 = new Object[i];
        localObject7[0] = paramString;
        Object localObject8 = Boolean.valueOf(k);
        localObject7[j] = localObject8;
        localObject6 = String.format((Locale)localObject6, str, (Object[])localObject7);
        ((k)localObject5).a("CrashlyticsCore", (String)localObject6);
        localObject5 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter;
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject4 = ((StringBuilder)localObject4).append(paramString);
        localObject6 = "SessionEvent";
        localObject4 = (String)localObject6;
        ((CrashlyticsUncaughtExceptionHandler.FileNameContainsFilter)localObject5).<init>((String)localObject4);
        localObject4 = listFilesMatching((FilenameFilter)localObject5);
        if (localObject4 == null) {
          break label423;
        }
        int n = localObject4.length;
        if (n <= 0) {
          break label423;
        }
        n = j;
        localObject6 = c.h();
        str = "CrashlyticsCore";
        localObject7 = Locale.US;
        localObject8 = "Session %s has non-fatal exceptions: %s";
        Object[] arrayOfObject = new Object[i];
        arrayOfObject[0] = paramString;
        Boolean localBoolean = Boolean.valueOf(n);
        arrayOfObject[j] = localBoolean;
        localObject9 = String.format((Locale)localObject7, (String)localObject8, arrayOfObject);
        ((k)localObject6).a(str, (String)localObject9);
        if ((k == 0) && (n == 0)) {
          break label441;
        }
        localObject9 = getTrimmedNonFatalFiles(paramString, (File[])localObject4, paramInt);
        if (k == 0) {
          break label432;
        }
        localObject2 = localObject3[0];
        label355:
        synthesizeSessionFile(paramFile, paramString, (File[])localObject9, (File)localObject2);
      }
    }
    for (;;)
    {
      localObject2 = c.h();
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = "Removing session part files for ID " + paramString;
      ((k)localObject2).a("CrashlyticsCore", (String)localObject1);
      deleteSessionPartFilesFor(paramString);
      return;
      int m = 0;
      localObject2 = null;
      break;
      label423:
      int i1 = 0;
      localObject5 = null;
      break label257;
      label432:
      m = 0;
      localObject2 = null;
      break label355;
      label441:
      localObject2 = c.h();
      localObject9 = "CrashlyticsCore";
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject5 = "No events present for session ID ";
      localObject1 = (String)localObject5 + paramString;
      ((k)localObject2).a((String)localObject9, (String)localObject1);
    }
  }
  
  /* Error */
  private void writeSessionUser(String paramString)
  {
    // Byte code:
    //   0: aconst_null
    //   1: astore_2
    //   2: new 279	com/crashlytics/android/core/ClsFileOutputStream
    //   5: astore_3
    //   6: aload_0
    //   7: invokespecial 235	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getFilesDir	()Ljava/io/File;
    //   10: astore 4
    //   12: new 263	java/lang/StringBuilder
    //   15: astore 5
    //   17: aload 5
    //   19: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   22: aload 5
    //   24: aload_1
    //   25: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   28: astore 5
    //   30: ldc 71
    //   32: astore 6
    //   34: aload 5
    //   36: aload 6
    //   38: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: astore 5
    //   43: aload 5
    //   45: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   48: astore 5
    //   50: aload_3
    //   51: aload 4
    //   53: aload 5
    //   55: invokespecial 420	com/crashlytics/android/core/ClsFileOutputStream:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   58: aload_3
    //   59: invokestatic 424	com/crashlytics/android/core/CodedOutputStream:newInstance	(Ljava/io/OutputStream;)Lcom/crashlytics/android/core/CodedOutputStream;
    //   62: astore_2
    //   63: aload_0
    //   64: aload_1
    //   65: invokespecial 983	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:getUserMetaData	(Ljava/lang/String;)Lcom/crashlytics/android/core/UserMetaData;
    //   68: astore 4
    //   70: aload 4
    //   72: invokevirtual 986	com/crashlytics/android/core/UserMetaData:isEmpty	()Z
    //   75: istore 7
    //   77: iload 7
    //   79: ifeq +22 -> 101
    //   82: aload_2
    //   83: ldc_w 988
    //   86: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   89: ldc_w 990
    //   92: astore 4
    //   94: aload_3
    //   95: aload 4
    //   97: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   100: return
    //   101: aload 4
    //   103: getfield 993	com/crashlytics/android/core/UserMetaData:id	Ljava/lang/String;
    //   106: astore 5
    //   108: aload 4
    //   110: getfield 994	com/crashlytics/android/core/UserMetaData:name	Ljava/lang/String;
    //   113: astore 6
    //   115: aload 4
    //   117: getfield 997	com/crashlytics/android/core/UserMetaData:email	Ljava/lang/String;
    //   120: astore 4
    //   122: aload_2
    //   123: aload 5
    //   125: aload 6
    //   127: aload 4
    //   129: invokestatic 1000	com/crashlytics/android/core/SessionProtobufHelper:writeSessionUser	(Lcom/crashlytics/android/core/CodedOutputStream;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    //   132: aload_2
    //   133: ldc_w 988
    //   136: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   139: ldc_w 990
    //   142: astore 4
    //   144: aload_3
    //   145: aload 4
    //   147: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   150: goto -50 -> 100
    //   153: astore 4
    //   155: aconst_null
    //   156: astore_3
    //   157: aload_2
    //   158: ldc_w 988
    //   161: invokestatic 379	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Flushable;Ljava/lang/String;)V
    //   164: aload_3
    //   165: ldc_w 990
    //   168: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   171: aload 4
    //   173: athrow
    //   174: astore 4
    //   176: goto -19 -> 157
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	179	0	this	CrashlyticsUncaughtExceptionHandler
    //   0	179	1	paramString	String
    //   1	157	2	localCodedOutputStream	CodedOutputStream
    //   5	160	3	localClsFileOutputStream	ClsFileOutputStream
    //   10	136	4	localObject1	Object
    //   153	19	4	localObject2	Object
    //   174	1	4	localObject3	Object
    //   15	109	5	localObject4	Object
    //   32	94	6	str	String
    //   75	3	7	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   2	5	153	finally
    //   6	10	153	finally
    //   12	15	153	finally
    //   17	22	153	finally
    //   24	28	153	finally
    //   36	41	153	finally
    //   43	48	153	finally
    //   53	58	153	finally
    //   58	62	174	finally
    //   64	68	174	finally
    //   70	75	174	finally
    //   101	106	174	finally
    //   108	113	174	finally
    //   115	120	174	finally
    //   127	132	174	finally
  }
  
  /* Error */
  private static void writeToCosFromFile(CodedOutputStream paramCodedOutputStream, File paramFile)
  {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual 669	java/io/File:exists	()Z
    //   4: istore_2
    //   5: iload_2
    //   6: ifne +61 -> 67
    //   9: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   12: astore_3
    //   13: ldc -7
    //   15: astore 4
    //   17: new 263	java/lang/StringBuilder
    //   20: astore 5
    //   22: aload 5
    //   24: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   27: aload 5
    //   29: ldc_w 1002
    //   32: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   35: astore 5
    //   37: aload_1
    //   38: invokevirtual 499	java/io/File:getName	()Ljava/lang/String;
    //   41: astore 6
    //   43: aload 5
    //   45: aload 6
    //   47: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   53: astore 5
    //   55: aload_3
    //   56: aload 4
    //   58: aload 5
    //   60: aconst_null
    //   61: invokeinterface 288 4 0
    //   66: return
    //   67: new 1004	java/io/FileInputStream
    //   70: astore 4
    //   72: aload 4
    //   74: aload_1
    //   75: invokespecial 1005	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   78: aload_1
    //   79: invokevirtual 1008	java/io/File:length	()J
    //   82: lstore 7
    //   84: lload 7
    //   86: l2i
    //   87: istore_2
    //   88: aload 4
    //   90: aload_0
    //   91: iload_2
    //   92: invokestatic 1012	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:copyToCodedOutputStream	(Ljava/io/InputStream;Lcom/crashlytics/android/core/CodedOutputStream;I)V
    //   95: ldc_w 1014
    //   98: astore_3
    //   99: aload 4
    //   101: aload_3
    //   102: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   105: goto -39 -> 66
    //   108: astore_3
    //   109: aconst_null
    //   110: astore 4
    //   112: aload 4
    //   114: ldc_w 1014
    //   117: invokestatic 384	io/fabric/sdk/android/services/b/i:a	(Ljava/io/Closeable;Ljava/lang/String;)V
    //   120: aload_3
    //   121: athrow
    //   122: astore_3
    //   123: goto -11 -> 112
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	126	0	paramCodedOutputStream	CodedOutputStream
    //   0	126	1	paramFile	File
    //   4	2	2	bool	boolean
    //   87	5	2	i	int
    //   12	90	3	localObject1	Object
    //   108	13	3	localObject2	Object
    //   122	1	3	localObject3	Object
    //   15	98	4	localObject4	Object
    //   20	39	5	localObject5	Object
    //   41	5	6	str	String
    //   82	3	7	l	long
    // Exception table:
    //   from	to	target	type
    //   67	70	108	finally
    //   74	78	108	finally
    //   78	82	122	finally
    //   91	95	122	finally
  }
  
  void cacheKeyData(Map paramMap)
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsUncaughtExceptionHandler.9 local9 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$9;
    local9.<init>(this, paramMap);
    localCrashlyticsExecutorServiceWrapper.executeAsync(local9);
  }
  
  void cacheUserData(String paramString1, String paramString2, String paramString3)
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsUncaughtExceptionHandler.8 local8 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$8;
    local8.<init>(this, paramString1, paramString2, paramString3);
    localCrashlyticsExecutorServiceWrapper.executeAsync(local8);
  }
  
  void cleanInvalidTempFiles()
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsUncaughtExceptionHandler.12 local12 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$12;
    local12.<init>(this);
    localCrashlyticsExecutorServiceWrapper.executeAsync(local12);
  }
  
  void doCleanInvalidTempFiles(File[] paramArrayOfFile)
  {
    int i = 0;
    Object localObject1 = new java/util/HashSet;
    ((HashSet)localObject1).<init>();
    int j = paramArrayOfFile.length;
    int m = 0;
    File localFile = null;
    Object localObject2;
    Object localObject3;
    String str1;
    Object localObject4;
    String str2;
    while (m < j)
    {
      localObject2 = paramArrayOfFile[m];
      localObject3 = c.h();
      str1 = "CrashlyticsCore";
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      str2 = "Found invalid session part file: ";
      localObject4 = str2 + localObject2;
      ((k)localObject3).a(str1, (String)localObject4);
      localObject2 = getSessionIdFromSessionFile((File)localObject2);
      ((Set)localObject1).add(localObject2);
      m += 1;
    }
    boolean bool2 = ((Set)localObject1).isEmpty();
    if (bool2) {}
    for (;;)
    {
      return;
      localFile = getInvalidFilesDir();
      boolean bool1 = localFile.exists();
      if (!bool1) {
        localFile.mkdir();
      }
      CrashlyticsUncaughtExceptionHandler.13 local13 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$13;
      local13.<init>(this, (Set)localObject1);
      localObject1 = listFilesMatching(local13);
      int k = localObject1.length;
      while (i < k)
      {
        localObject2 = localObject1[i];
        localObject3 = c.h();
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        str2 = "Moving session file: ";
        localObject4 = str2 + localObject2;
        ((k)localObject3).a("CrashlyticsCore", (String)localObject4);
        localObject3 = new java/io/File;
        str1 = ((File)localObject2).getName();
        ((File)localObject3).<init>(localFile, str1);
        boolean bool3 = ((File)localObject2).renameTo((File)localObject3);
        if (!bool3)
        {
          localObject3 = c.h();
          str1 = "CrashlyticsCore";
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          str2 = "Could not move session file. Deleting ";
          localObject4 = str2 + localObject2;
          ((k)localObject3).a(str1, (String)localObject4);
          ((File)localObject2).delete();
        }
        i += 1;
      }
      trimInvalidSessionFiles();
    }
  }
  
  void doCloseSessions()
  {
    doCloseSessions(false);
  }
  
  boolean finalizeSessions()
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsUncaughtExceptionHandler.11 local11 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$11;
    local11.<init>(this);
    return ((Boolean)localCrashlyticsExecutorServiceWrapper.executeSyncLoggingException(local11)).booleanValue();
  }
  
  File getInvalidFilesDir()
  {
    File localFile1 = new java/io/File;
    File localFile2 = getFilesDir();
    localFile1.<init>(localFile2, "invalidClsFiles");
    return localFile1;
  }
  
  boolean hasOpenSession()
  {
    File[] arrayOfFile = listSessionBeginFiles();
    int i = arrayOfFile.length;
    if (i > 0) {
      i = 1;
    }
    for (;;)
    {
      return i;
      int j = 0;
      arrayOfFile = null;
    }
  }
  
  boolean isHandlingException()
  {
    return isHandlingException.get();
  }
  
  File[] listSessionBeginFiles()
  {
    CrashlyticsUncaughtExceptionHandler.FileNameContainsFilter localFileNameContainsFilter = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter;
    localFileNameContainsFilter.<init>("BeginSession");
    return listFilesMatching(localFileNameContainsFilter);
  }
  
  void openSession()
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsUncaughtExceptionHandler.10 local10 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$10;
    local10.<init>(this);
    localCrashlyticsExecutorServiceWrapper.executeAsync(local10);
  }
  
  void trimSessionFiles()
  {
    File localFile = getFilesDir();
    FilenameFilter localFilenameFilter = SESSION_FILE_FILTER;
    Comparator localComparator = SMALLEST_FILE_NAME_FIRST;
    Utils.capFileCount(localFile, localFilenameFilter, 4, localComparator);
  }
  
  /* Error */
  public void uncaughtException(Thread paramThread, Throwable paramThrowable)
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 179	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:isHandlingException	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   6: astore_3
    //   7: iconst_1
    //   8: istore 4
    //   10: aload_3
    //   11: iload 4
    //   13: invokevirtual 1074	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   16: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   19: astore_3
    //   20: ldc -7
    //   22: astore 5
    //   24: new 263	java/lang/StringBuilder
    //   27: astore 6
    //   29: aload 6
    //   31: invokespecial 264	java/lang/StringBuilder:<init>	()V
    //   34: ldc_w 1076
    //   37: astore 7
    //   39: aload 6
    //   41: aload 7
    //   43: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: astore 6
    //   48: aload 6
    //   50: aload_2
    //   51: invokevirtual 465	java/lang/StringBuilder:append	(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   54: astore 6
    //   56: ldc_w 467
    //   59: astore 7
    //   61: aload 6
    //   63: aload 7
    //   65: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   68: astore 6
    //   70: aload_1
    //   71: invokevirtual 470	java/lang/Thread:getName	()Ljava/lang/String;
    //   74: astore 7
    //   76: aload 6
    //   78: aload 7
    //   80: invokevirtual 270	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   83: astore 6
    //   85: aload 6
    //   87: invokevirtual 273	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   90: astore 6
    //   92: aload_3
    //   93: aload 5
    //   95: aload 6
    //   97: invokeinterface 257 3 0
    //   102: aload_0
    //   103: getfield 199	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:devicePowerStateListener	Lcom/crashlytics/android/core/DevicePowerStateListener;
    //   106: astore_3
    //   107: aload_3
    //   108: invokevirtual 1079	com/crashlytics/android/core/DevicePowerStateListener:dispose	()V
    //   111: new 342	java/util/Date
    //   114: astore_3
    //   115: aload_3
    //   116: invokespecial 343	java/util/Date:<init>	()V
    //   119: aload_0
    //   120: getfield 158	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:executorServiceWrapper	Lcom/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
    //   123: astore 5
    //   125: new 1081	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$5
    //   128: astore 6
    //   130: aload 6
    //   132: aload_0
    //   133: aload_3
    //   134: aload_1
    //   135: aload_2
    //   136: invokespecial 1084	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$5:<init>	(Lcom/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;Ljava/util/Date;Ljava/lang/Thread;Ljava/lang/Throwable;)V
    //   139: aload 5
    //   141: aload 6
    //   143: invokevirtual 1062	com/crashlytics/android/core/CrashlyticsExecutorServiceWrapper:executeSyncLoggingException	(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    //   146: pop
    //   147: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   150: astore_3
    //   151: ldc -7
    //   153: astore 5
    //   155: ldc_w 1086
    //   158: astore 6
    //   160: aload_3
    //   161: aload 5
    //   163: aload 6
    //   165: invokeinterface 257 3 0
    //   170: aload_0
    //   171: getfield 156	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:defaultHandler	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   174: astore_3
    //   175: aload_3
    //   176: aload_1
    //   177: aload_2
    //   178: invokeinterface 1090 3 0
    //   183: aload_0
    //   184: getfield 179	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:isHandlingException	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   187: astore_3
    //   188: iconst_0
    //   189: istore 4
    //   191: aconst_null
    //   192: astore 5
    //   194: aload_3
    //   195: iconst_0
    //   196: invokevirtual 1074	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   199: aload_0
    //   200: monitorexit
    //   201: return
    //   202: astore_3
    //   203: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   206: astore 5
    //   208: ldc -7
    //   210: astore 6
    //   212: ldc_w 1092
    //   215: astore 7
    //   217: aload 5
    //   219: aload 6
    //   221: aload 7
    //   223: aload_3
    //   224: invokeinterface 288 4 0
    //   229: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   232: astore_3
    //   233: ldc -7
    //   235: astore 5
    //   237: ldc_w 1086
    //   240: astore 6
    //   242: aload_3
    //   243: aload 5
    //   245: aload 6
    //   247: invokeinterface 257 3 0
    //   252: aload_0
    //   253: getfield 156	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:defaultHandler	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   256: astore_3
    //   257: aload_3
    //   258: aload_1
    //   259: aload_2
    //   260: invokeinterface 1090 3 0
    //   265: aload_0
    //   266: getfield 179	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:isHandlingException	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   269: astore_3
    //   270: iconst_0
    //   271: istore 4
    //   273: aconst_null
    //   274: astore 5
    //   276: aload_3
    //   277: iconst_0
    //   278: invokevirtual 1074	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   281: goto -82 -> 199
    //   284: astore_3
    //   285: aload_0
    //   286: monitorexit
    //   287: aload_3
    //   288: athrow
    //   289: astore_3
    //   290: invokestatic 247	io/fabric/sdk/android/c:h	()Lio/fabric/sdk/android/k;
    //   293: astore 5
    //   295: ldc -7
    //   297: astore 6
    //   299: ldc_w 1086
    //   302: astore 7
    //   304: aload 5
    //   306: aload 6
    //   308: aload 7
    //   310: invokeinterface 257 3 0
    //   315: aload_0
    //   316: getfield 156	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:defaultHandler	Ljava/lang/Thread$UncaughtExceptionHandler;
    //   319: astore 5
    //   321: aload 5
    //   323: aload_1
    //   324: aload_2
    //   325: invokeinterface 1090 3 0
    //   330: aload_0
    //   331: getfield 179	com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler:isHandlingException	Ljava/util/concurrent/atomic/AtomicBoolean;
    //   334: astore 5
    //   336: aconst_null
    //   337: astore 6
    //   339: aload 5
    //   341: iconst_0
    //   342: invokevirtual 1074	java/util/concurrent/atomic/AtomicBoolean:set	(Z)V
    //   345: aload_3
    //   346: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	347	0	this	CrashlyticsUncaughtExceptionHandler
    //   0	347	1	paramThread	Thread
    //   0	347	2	paramThrowable	Throwable
    //   6	189	3	localObject1	Object
    //   202	22	3	localException	Exception
    //   232	45	3	localObject2	Object
    //   284	4	3	localObject3	Object
    //   289	57	3	localObject4	Object
    //   8	264	4	bool	boolean
    //   22	318	5	localObject5	Object
    //   27	311	6	localObject6	Object
    //   37	272	7	str	String
    // Exception table:
    //   from	to	target	type
    //   16	19	202	java/lang/Exception
    //   24	27	202	java/lang/Exception
    //   29	34	202	java/lang/Exception
    //   41	46	202	java/lang/Exception
    //   50	54	202	java/lang/Exception
    //   63	68	202	java/lang/Exception
    //   70	74	202	java/lang/Exception
    //   78	83	202	java/lang/Exception
    //   85	90	202	java/lang/Exception
    //   95	102	202	java/lang/Exception
    //   102	106	202	java/lang/Exception
    //   107	111	202	java/lang/Exception
    //   111	114	202	java/lang/Exception
    //   115	119	202	java/lang/Exception
    //   119	123	202	java/lang/Exception
    //   125	128	202	java/lang/Exception
    //   135	139	202	java/lang/Exception
    //   141	147	202	java/lang/Exception
    //   2	6	284	finally
    //   11	16	284	finally
    //   147	150	284	finally
    //   163	170	284	finally
    //   170	174	284	finally
    //   177	183	284	finally
    //   183	187	284	finally
    //   195	199	284	finally
    //   229	232	284	finally
    //   245	252	284	finally
    //   252	256	284	finally
    //   259	265	284	finally
    //   265	269	284	finally
    //   277	281	284	finally
    //   290	293	284	finally
    //   308	315	284	finally
    //   315	319	284	finally
    //   324	330	284	finally
    //   330	334	284	finally
    //   341	345	284	finally
    //   345	347	284	finally
    //   16	19	289	finally
    //   24	27	289	finally
    //   29	34	289	finally
    //   41	46	289	finally
    //   50	54	289	finally
    //   63	68	289	finally
    //   70	74	289	finally
    //   78	83	289	finally
    //   85	90	289	finally
    //   95	102	289	finally
    //   102	106	289	finally
    //   107	111	289	finally
    //   111	114	289	finally
    //   115	119	289	finally
    //   119	123	289	finally
    //   125	128	289	finally
    //   135	139	289	finally
    //   141	147	289	finally
    //   203	206	289	finally
    //   223	229	289	finally
  }
  
  void writeExternalCrashEvent(SessionEventData paramSessionEventData)
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsUncaughtExceptionHandler.14 local14 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$14;
    local14.<init>(this, paramSessionEventData);
    localCrashlyticsExecutorServiceWrapper.executeAsync(local14);
  }
  
  void writeNonFatalException(Thread paramThread, Throwable paramThrowable)
  {
    Date localDate = new java/util/Date;
    localDate.<init>();
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsUncaughtExceptionHandler.7 local7 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$7;
    local7.<init>(this, localDate, paramThread, paramThrowable);
    localCrashlyticsExecutorServiceWrapper.executeAsync(local7);
  }
  
  void writeToLog(long paramLong, String paramString)
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsUncaughtExceptionHandler.6 local6 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$6;
    local6.<init>(this, paramLong, paramString);
    localCrashlyticsExecutorServiceWrapper.executeAsync(local6);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */