package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.concurrent.Callable;

final class CrashlyticsCore$CrashMarkerCheck
  implements Callable
{
  private final CrashlyticsFileMarker crashMarker;
  
  public CrashlyticsCore$CrashMarkerCheck(CrashlyticsFileMarker paramCrashlyticsFileMarker)
  {
    crashMarker = paramCrashlyticsFileMarker;
  }
  
  public Boolean call()
  {
    Object localObject = crashMarker;
    boolean bool = ((CrashlyticsFileMarker)localObject).isPresent();
    if (!bool) {}
    for (localObject = Boolean.FALSE;; localObject = Boolean.TRUE)
    {
      return (Boolean)localObject;
      localObject = c.h();
      String str1 = "CrashlyticsCore";
      String str2 = "Found previous crash marker.";
      ((k)localObject).a(str1, str2);
      crashMarker.remove();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsCore$CrashMarkerCheck.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */