package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class SessionReport
  implements Report
{
  private final Map customHeaders;
  private final File file;
  private final File[] files;
  
  public SessionReport(File paramFile)
  {
    this(paramFile, localMap);
  }
  
  public SessionReport(File paramFile, Map paramMap)
  {
    file = paramFile;
    Object localObject = new File[1];
    Map localMap = null;
    localObject[0] = paramFile;
    files = ((File[])localObject);
    localObject = new java/util/HashMap;
    ((HashMap)localObject).<init>(paramMap);
    customHeaders = ((Map)localObject);
    localObject = file;
    long l1 = ((File)localObject).length();
    long l2 = 0L;
    boolean bool = l1 < l2;
    if (!bool)
    {
      localObject = customHeaders;
      localMap = ReportUploader.HEADER_INVALID_CLS_FILE;
      ((Map)localObject).putAll(localMap);
    }
  }
  
  public Map getCustomHeaders()
  {
    return Collections.unmodifiableMap(customHeaders);
  }
  
  public File getFile()
  {
    return file;
  }
  
  public String getFileName()
  {
    return getFile().getName();
  }
  
  public File[] getFiles()
  {
    return files;
  }
  
  public String getIdentifier()
  {
    String str = getFileName();
    int i = str.lastIndexOf('.');
    return str.substring(0, i);
  }
  
  public void remove()
  {
    k localk = c.h();
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = ((StringBuilder)localObject).append("Removing report at ");
    String str = file.getPath();
    localObject = str;
    localk.a("CrashlyticsCore", (String)localObject);
    file.delete();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/SessionReport.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */