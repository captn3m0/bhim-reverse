package com.crashlytics.android.core;

import java.util.concurrent.CountDownLatch;

class CrashPromptDialog$OptInLatch
{
  private final CountDownLatch latch;
  private boolean send = false;
  
  private CrashPromptDialog$OptInLatch()
  {
    CountDownLatch localCountDownLatch = new java/util/concurrent/CountDownLatch;
    localCountDownLatch.<init>(1);
    latch = localCountDownLatch;
  }
  
  void await()
  {
    try
    {
      CountDownLatch localCountDownLatch = latch;
      localCountDownLatch.await();
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;) {}
    }
  }
  
  boolean getOptIn()
  {
    return send;
  }
  
  void setOptIn(boolean paramBoolean)
  {
    send = paramBoolean;
    latch.countDown();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashPromptDialog$OptInLatch.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */