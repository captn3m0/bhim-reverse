package com.crashlytics.android.core;

final class NativeCrashWriter$LogMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private static final int PROTOBUF_TAG = 6;
  ByteString logBytes;
  
  public NativeCrashWriter$LogMessage(ByteString paramByteString)
  {
    super(6, arrayOfProtobufMessage);
    logBytes = paramByteString;
  }
  
  public int getPropertiesSize()
  {
    ByteString localByteString = logBytes;
    return CodedOutputStream.computeBytesSize(1, localByteString);
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream)
  {
    ByteString localByteString = logBytes;
    paramCodedOutputStream.writeBytes(1, localByteString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$LogMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */