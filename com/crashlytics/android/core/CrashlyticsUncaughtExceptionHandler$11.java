package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

class CrashlyticsUncaughtExceptionHandler$11
  implements Callable
{
  CrashlyticsUncaughtExceptionHandler$11(CrashlyticsUncaughtExceptionHandler paramCrashlyticsUncaughtExceptionHandler) {}
  
  public Boolean call()
  {
    Object localObject = CrashlyticsUncaughtExceptionHandler.access$200(this$0);
    boolean bool1 = ((AtomicBoolean)localObject).get();
    String str1;
    String str2;
    if (bool1)
    {
      localObject = c.h();
      str1 = "CrashlyticsCore";
      str2 = "Skipping session finalization because a crash has already occurred.";
      ((k)localObject).a(str1, str2);
    }
    for (localObject = Boolean.FALSE;; localObject = Boolean.TRUE)
    {
      return (Boolean)localObject;
      c.h().a("CrashlyticsCore", "Finalizing previously open sessions.");
      localObject = this$0;
      boolean bool2 = true;
      CrashlyticsUncaughtExceptionHandler.access$800((CrashlyticsUncaughtExceptionHandler)localObject, bool2);
      localObject = c.h();
      str1 = "CrashlyticsCore";
      str2 = "Closed all previously open sessions";
      ((k)localObject).a(str1, str2);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$11.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */