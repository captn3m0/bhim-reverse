package com.crashlytics.android.core;

import java.io.IOException;

class CodedOutputStream$OutOfSpaceException
  extends IOException
{
  private static final long serialVersionUID = -6947486886997889499L;
  
  CodedOutputStream$OutOfSpaceException()
  {
    super("CodedOutputStream was writing to a flat byte array and ran out of space.");
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CodedOutputStream$OutOfSpaceException.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */