package com.crashlytics.android.core;

import com.crashlytics.android.core.internal.models.BinaryImageData;
import com.crashlytics.android.core.internal.models.CustomAttributeData;
import com.crashlytics.android.core.internal.models.DeviceData;
import com.crashlytics.android.core.internal.models.SessionEventData;
import com.crashlytics.android.core.internal.models.SignalData;
import com.crashlytics.android.core.internal.models.ThreadData;
import com.crashlytics.android.core.internal.models.ThreadData.FrameData;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

class NativeCrashWriter
{
  private static final SignalData DEFAULT_SIGNAL;
  private static final NativeCrashWriter.BinaryImageMessage[] EMPTY_BINARY_IMAGE_MESSAGES = new NativeCrashWriter.BinaryImageMessage[0];
  private static final NativeCrashWriter.ProtobufMessage[] EMPTY_CHILDREN;
  private static final NativeCrashWriter.CustomAttributeMessage[] EMPTY_CUSTOM_ATTRIBUTE_MESSAGES = new NativeCrashWriter.CustomAttributeMessage[0];
  private static final NativeCrashWriter.FrameMessage[] EMPTY_FRAME_MESSAGES;
  private static final NativeCrashWriter.ThreadMessage[] EMPTY_THREAD_MESSAGES;
  static final String NDK_CRASH_TYPE = "ndk-crash";
  
  static
  {
    SignalData localSignalData = new com/crashlytics/android/core/internal/models/SignalData;
    localSignalData.<init>("", "", 0L);
    DEFAULT_SIGNAL = localSignalData;
    EMPTY_CHILDREN = new NativeCrashWriter.ProtobufMessage[0];
    EMPTY_THREAD_MESSAGES = new NativeCrashWriter.ThreadMessage[0];
    EMPTY_FRAME_MESSAGES = new NativeCrashWriter.FrameMessage[0];
  }
  
  private static NativeCrashWriter.RepeatedMessage createBinaryImagesMessage(BinaryImageData[] paramArrayOfBinaryImageData)
  {
    int i;
    if (paramArrayOfBinaryImageData != null) {
      i = paramArrayOfBinaryImageData.length;
    }
    for (NativeCrashWriter.BinaryImageMessage[] arrayOfBinaryImageMessage = new NativeCrashWriter.BinaryImageMessage[i];; arrayOfBinaryImageMessage = EMPTY_BINARY_IMAGE_MESSAGES)
    {
      int j = 0;
      localRepeatedMessage = null;
      for (;;)
      {
        int k = arrayOfBinaryImageMessage.length;
        if (j >= k) {
          break;
        }
        NativeCrashWriter.BinaryImageMessage localBinaryImageMessage = new com/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage;
        BinaryImageData localBinaryImageData = paramArrayOfBinaryImageData[j];
        localBinaryImageMessage.<init>(localBinaryImageData);
        arrayOfBinaryImageMessage[j] = localBinaryImageMessage;
        j += 1;
      }
    }
    NativeCrashWriter.RepeatedMessage localRepeatedMessage = new com/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
    localRepeatedMessage.<init>(arrayOfBinaryImageMessage);
    return localRepeatedMessage;
  }
  
  private static NativeCrashWriter.RepeatedMessage createCustomAttributesMessage(CustomAttributeData[] paramArrayOfCustomAttributeData)
  {
    int i;
    if (paramArrayOfCustomAttributeData != null) {
      i = paramArrayOfCustomAttributeData.length;
    }
    for (NativeCrashWriter.CustomAttributeMessage[] arrayOfCustomAttributeMessage = new NativeCrashWriter.CustomAttributeMessage[i];; arrayOfCustomAttributeMessage = EMPTY_CUSTOM_ATTRIBUTE_MESSAGES)
    {
      int j = 0;
      localRepeatedMessage = null;
      for (;;)
      {
        int k = arrayOfCustomAttributeMessage.length;
        if (j >= k) {
          break;
        }
        NativeCrashWriter.CustomAttributeMessage localCustomAttributeMessage = new com/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage;
        CustomAttributeData localCustomAttributeData = paramArrayOfCustomAttributeData[j];
        localCustomAttributeMessage.<init>(localCustomAttributeData);
        arrayOfCustomAttributeMessage[j] = localCustomAttributeMessage;
        j += 1;
      }
    }
    NativeCrashWriter.RepeatedMessage localRepeatedMessage = new com/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
    localRepeatedMessage.<init>(arrayOfCustomAttributeMessage);
    return localRepeatedMessage;
  }
  
  private static NativeCrashWriter.ProtobufMessage createDeviceMessage(DeviceData paramDeviceData)
  {
    Object localObject;
    if (paramDeviceData == null)
    {
      localObject = new com/crashlytics/android/core/NativeCrashWriter$NullMessage;
      ((NativeCrashWriter.NullMessage)localObject).<init>();
    }
    for (;;)
    {
      return (NativeCrashWriter.ProtobufMessage)localObject;
      localObject = new com/crashlytics/android/core/NativeCrashWriter$DeviceMessage;
      int i = batteryCapacity;
      float f1 = i;
      float f2 = f1 / 100.0F;
      int j = batteryVelocity;
      boolean bool = proximity;
      int k = orientation;
      long l1 = totalPhysicalMemory;
      long l2 = availablePhysicalMemory;
      l1 -= l2;
      l2 = totalInternalStorage;
      long l3 = availableInternalStorage;
      l2 -= l3;
      ((NativeCrashWriter.DeviceMessage)localObject).<init>(f2, j, bool, k, l1, l2);
    }
  }
  
  private static NativeCrashWriter.EventMessage createEventMessage(SessionEventData paramSessionEventData, LogFileManager paramLogFileManager, Map paramMap)
  {
    Object localObject1 = signal;
    Object localObject2;
    Object localObject3;
    Object localObject4;
    if (localObject1 != null)
    {
      localObject1 = signal;
      localObject2 = new com/crashlytics/android/core/NativeCrashWriter$SignalMessage;
      ((NativeCrashWriter.SignalMessage)localObject2).<init>((SignalData)localObject1);
      localObject1 = createThreadsMessage(threads);
      localObject3 = createBinaryImagesMessage(binaryImages);
      localObject4 = new com/crashlytics/android/core/NativeCrashWriter$ExecutionMessage;
      ((NativeCrashWriter.ExecutionMessage)localObject4).<init>((NativeCrashWriter.SignalMessage)localObject2, (NativeCrashWriter.RepeatedMessage)localObject1, (NativeCrashWriter.RepeatedMessage)localObject3);
      localObject1 = createCustomAttributesMessage(mergeCustomAttributes(customAttributes, paramMap));
      localObject2 = new com/crashlytics/android/core/NativeCrashWriter$ApplicationMessage;
      ((NativeCrashWriter.ApplicationMessage)localObject2).<init>((NativeCrashWriter.ExecutionMessage)localObject4, (NativeCrashWriter.RepeatedMessage)localObject1);
      localObject1 = deviceData;
      localObject3 = createDeviceMessage((DeviceData)localObject1);
      localObject4 = paramLogFileManager.getByteStringForLog();
      if (localObject4 == null)
      {
        localObject1 = c.h();
        String str1 = "CrashlyticsCore";
        String str2 = "No log data to include with this event.";
        ((k)localObject1).a(str1, str2);
      }
      paramLogFileManager.clearLog();
      if (localObject4 == null) {
        break label200;
      }
      localObject1 = new com/crashlytics/android/core/NativeCrashWriter$LogMessage;
      ((NativeCrashWriter.LogMessage)localObject1).<init>((ByteString)localObject4);
    }
    for (;;)
    {
      localObject4 = new com/crashlytics/android/core/NativeCrashWriter$EventMessage;
      long l = timestamp;
      NativeCrashWriter.ProtobufMessage[] arrayOfProtobufMessage = new NativeCrashWriter.ProtobufMessage[3];
      arrayOfProtobufMessage[0] = localObject2;
      arrayOfProtobufMessage[1] = localObject3;
      arrayOfProtobufMessage[2] = localObject1;
      ((NativeCrashWriter.EventMessage)localObject4).<init>(l, "ndk-crash", arrayOfProtobufMessage);
      return (NativeCrashWriter.EventMessage)localObject4;
      localObject1 = DEFAULT_SIGNAL;
      break;
      label200:
      localObject1 = new com/crashlytics/android/core/NativeCrashWriter$NullMessage;
      ((NativeCrashWriter.NullMessage)localObject1).<init>();
    }
  }
  
  private static NativeCrashWriter.RepeatedMessage createFramesMessage(ThreadData.FrameData[] paramArrayOfFrameData)
  {
    int i;
    if (paramArrayOfFrameData != null) {
      i = paramArrayOfFrameData.length;
    }
    for (NativeCrashWriter.FrameMessage[] arrayOfFrameMessage = new NativeCrashWriter.FrameMessage[i];; arrayOfFrameMessage = EMPTY_FRAME_MESSAGES)
    {
      int j = 0;
      localRepeatedMessage = null;
      for (;;)
      {
        int k = arrayOfFrameMessage.length;
        if (j >= k) {
          break;
        }
        NativeCrashWriter.FrameMessage localFrameMessage = new com/crashlytics/android/core/NativeCrashWriter$FrameMessage;
        ThreadData.FrameData localFrameData = paramArrayOfFrameData[j];
        localFrameMessage.<init>(localFrameData);
        arrayOfFrameMessage[j] = localFrameMessage;
        j += 1;
      }
    }
    NativeCrashWriter.RepeatedMessage localRepeatedMessage = new com/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
    localRepeatedMessage.<init>(arrayOfFrameMessage);
    return localRepeatedMessage;
  }
  
  private static NativeCrashWriter.RepeatedMessage createThreadsMessage(ThreadData[] paramArrayOfThreadData)
  {
    int i;
    if (paramArrayOfThreadData != null) {
      i = paramArrayOfThreadData.length;
    }
    for (NativeCrashWriter.ThreadMessage[] arrayOfThreadMessage = new NativeCrashWriter.ThreadMessage[i];; arrayOfThreadMessage = EMPTY_THREAD_MESSAGES)
    {
      int j = 0;
      localRepeatedMessage1 = null;
      for (;;)
      {
        int k = arrayOfThreadMessage.length;
        if (j >= k) {
          break;
        }
        ThreadData localThreadData = paramArrayOfThreadData[j];
        NativeCrashWriter.ThreadMessage localThreadMessage = new com/crashlytics/android/core/NativeCrashWriter$ThreadMessage;
        NativeCrashWriter.RepeatedMessage localRepeatedMessage2 = createFramesMessage(frames);
        localThreadMessage.<init>(localThreadData, localRepeatedMessage2);
        arrayOfThreadMessage[j] = localThreadMessage;
        j += 1;
      }
    }
    NativeCrashWriter.RepeatedMessage localRepeatedMessage1 = new com/crashlytics/android/core/NativeCrashWriter$RepeatedMessage;
    localRepeatedMessage1.<init>(arrayOfThreadMessage);
    return localRepeatedMessage1;
  }
  
  private static CustomAttributeData[] mergeCustomAttributes(CustomAttributeData[] paramArrayOfCustomAttributeData, Map paramMap)
  {
    int i = 0;
    String str = null;
    Object localObject1 = new java/util/TreeMap;
    ((TreeMap)localObject1).<init>(paramMap);
    Object localObject4;
    if (paramArrayOfCustomAttributeData != null)
    {
      j = paramArrayOfCustomAttributeData.length;
      int k = 0;
      localObject2 = null;
      while (k < j)
      {
        localObject3 = paramArrayOfCustomAttributeData[k];
        localObject4 = key;
        localObject3 = value;
        ((Map)localObject1).put(localObject4, localObject3);
        k += 1;
      }
    }
    Object localObject2 = ((Map)localObject1).entrySet();
    localObject1 = new Map.Entry[((Map)localObject1).size()];
    localObject2 = (Map.Entry[])((Set)localObject2).toArray((Object[])localObject1);
    int m = localObject2.length;
    Object localObject3 = new CustomAttributeData[m];
    for (int j = 0;; j = i)
    {
      i = localObject3.length;
      if (j >= i) {
        break;
      }
      localObject4 = new com/crashlytics/android/core/internal/models/CustomAttributeData;
      str = (String)localObject2[j].getKey();
      localObject1 = (String)localObject2[j].getValue();
      ((CustomAttributeData)localObject4).<init>(str, (String)localObject1);
      localObject3[j] = localObject4;
      i = j + 1;
    }
    return (CustomAttributeData[])localObject3;
  }
  
  public static void writeNativeCrash(SessionEventData paramSessionEventData, LogFileManager paramLogFileManager, Map paramMap, CodedOutputStream paramCodedOutputStream)
  {
    createEventMessage(paramSessionEventData, paramLogFileManager, paramMap).write(paramCodedOutputStream);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */