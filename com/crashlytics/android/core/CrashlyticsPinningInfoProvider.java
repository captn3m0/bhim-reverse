package com.crashlytics.android.core;

import io.fabric.sdk.android.services.network.f;
import java.io.InputStream;

class CrashlyticsPinningInfoProvider
  implements f
{
  private final PinningInfoProvider pinningInfo;
  
  public CrashlyticsPinningInfoProvider(PinningInfoProvider paramPinningInfoProvider)
  {
    pinningInfo = paramPinningInfoProvider;
  }
  
  public String getKeyStorePassword()
  {
    return pinningInfo.getKeyStorePassword();
  }
  
  public InputStream getKeyStoreStream()
  {
    return pinningInfo.getKeyStoreStream();
  }
  
  public long getPinCreationTimeInMillis()
  {
    return -1;
  }
  
  public String[] getPins()
  {
    return pinningInfo.getPins();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsPinningInfoProvider.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */