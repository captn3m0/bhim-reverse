package com.crashlytics.android.core;

import android.os.Looper;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

class CrashlyticsExecutorServiceWrapper
{
  private final ExecutorService executorService;
  
  public CrashlyticsExecutorServiceWrapper(ExecutorService paramExecutorService)
  {
    executorService = paramExecutorService;
  }
  
  Future executeAsync(Runnable paramRunnable)
  {
    try
    {
      localObject1 = executorService;
      localObject2 = new com/crashlytics/android/core/CrashlyticsExecutorServiceWrapper$1;
      ((CrashlyticsExecutorServiceWrapper.1)localObject2).<init>(this, paramRunnable);
      localObject1 = ((ExecutorService)localObject1).submit((Runnable)localObject2);
    }
    catch (RejectedExecutionException localRejectedExecutionException)
    {
      for (;;)
      {
        Object localObject1;
        k localk = c.h();
        Object localObject2 = "CrashlyticsCore";
        String str = "Executor is shut down because we're handling a fatal crash.";
        localk.a((String)localObject2, str);
        localk = null;
      }
    }
    return (Future)localObject1;
  }
  
  Future executeAsync(Callable paramCallable)
  {
    try
    {
      localObject1 = executorService;
      localObject2 = new com/crashlytics/android/core/CrashlyticsExecutorServiceWrapper$2;
      ((CrashlyticsExecutorServiceWrapper.2)localObject2).<init>(this, paramCallable);
      localObject1 = ((ExecutorService)localObject1).submit((Callable)localObject2);
    }
    catch (RejectedExecutionException localRejectedExecutionException)
    {
      for (;;)
      {
        Object localObject1;
        k localk = c.h();
        Object localObject2 = "CrashlyticsCore";
        String str = "Executor is shut down because we're handling a fatal crash.";
        localk.a((String)localObject2, str);
        localk = null;
      }
    }
    return (Future)localObject1;
  }
  
  Object executeSyncLoggingException(Callable paramCallable)
  {
    for (Object localObject1 = null;; localObject1 = ((Future)localObject2).get())
    {
      try
      {
        localObject2 = Looper.getMainLooper();
        localObject3 = Looper.myLooper();
        if (localObject2 != localObject3) {
          break label52;
        }
        localObject2 = executorService;
        localObject2 = ((ExecutorService)localObject2).submit(paramCallable);
        long l = 4;
        localObject4 = TimeUnit.SECONDS;
        localObject1 = ((Future)localObject2).get(l, (TimeUnit)localObject4);
      }
      catch (RejectedExecutionException localRejectedExecutionException)
      {
        for (;;)
        {
          Object localObject2;
          k localk = c.h();
          localObject3 = "CrashlyticsCore";
          str = "Executor is shut down because we're handling a fatal crash.";
          localk.a((String)localObject3, str);
        }
      }
      catch (Exception localException)
      {
        for (;;)
        {
          label52:
          Object localObject3 = c.h();
          String str = "CrashlyticsCore";
          Object localObject4 = "Failed to execute task.";
          ((k)localObject3).e(str, (String)localObject4, localException);
        }
      }
      return localObject1;
      localObject2 = executorService;
      localObject2 = ((ExecutorService)localObject2).submit(paramCallable);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsExecutorServiceWrapper.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */