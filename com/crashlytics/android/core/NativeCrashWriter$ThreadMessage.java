package com.crashlytics.android.core;

import com.crashlytics.android.core.internal.models.ThreadData;

final class NativeCrashWriter$ThreadMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private static final int PROTOBUF_TAG = 1;
  private final int importance;
  private final String name;
  
  public NativeCrashWriter$ThreadMessage(ThreadData paramThreadData, NativeCrashWriter.RepeatedMessage paramRepeatedMessage)
  {
    super(i, (NativeCrashWriter.ProtobufMessage[])localObject);
    localObject = name;
    name = ((String)localObject);
    int j = importance;
    importance = j;
  }
  
  private boolean hasName()
  {
    String str = name;
    int i;
    if (str != null)
    {
      str = name;
      i = str.length();
      if (i > 0) {
        i = 1;
      }
    }
    for (;;)
    {
      return i;
      int j = 0;
      str = null;
    }
  }
  
  public int getPropertiesSize()
  {
    boolean bool = hasName();
    ByteString localByteString;
    if (bool) {
      localByteString = ByteString.copyFromUtf8(name);
    }
    for (int i = CodedOutputStream.computeBytesSize(1, localByteString);; i = 0)
    {
      int j = importance;
      int k = CodedOutputStream.computeUInt32Size(2, j);
      return i + k;
    }
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream)
  {
    int i = hasName();
    if (i != 0)
    {
      i = 1;
      ByteString localByteString = ByteString.copyFromUtf8(name);
      paramCodedOutputStream.writeBytes(i, localByteString);
    }
    int j = importance;
    paramCodedOutputStream.writeUInt32(2, j);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$ThreadMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */