package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.q;
import io.fabric.sdk.android.services.b.q.c;
import java.io.File;
import java.io.IOException;
import java.util.Locale;

class QueueFileLogStore
  implements FileLogStore
{
  private q logFile;
  private final int maxLogSize;
  private final File workingFile;
  
  public QueueFileLogStore(File paramFile, int paramInt)
  {
    workingFile = paramFile;
    maxLogSize = paramInt;
  }
  
  private void doWriteToLog(long paramLong, String paramString)
  {
    Object localObject1 = logFile;
    if (localObject1 == null) {
      return;
    }
    if (paramString == null) {}
    String str;
    for (localObject1 = "null";; str = paramString) {
      try
      {
        int i = maxLogSize;
        i /= 4;
        int j = ((String)localObject1).length();
        if (j > i)
        {
          localObject2 = new java/lang/StringBuilder;
          ((StringBuilder)localObject2).<init>();
          localObject3 = "...";
          localObject2 = ((StringBuilder)localObject2).append((String)localObject3);
          k = ((String)localObject1).length();
          i = k - i;
          localObject1 = ((String)localObject1).substring(i);
          localObject1 = ((StringBuilder)localObject2).append((String)localObject1);
          localObject1 = ((StringBuilder)localObject1).toString();
        }
        localObject4 = "\r";
        localObject2 = " ";
        localObject1 = ((String)localObject1).replaceAll((String)localObject4, (String)localObject2);
        localObject4 = "\n";
        localObject2 = " ";
        localObject1 = ((String)localObject1).replaceAll((String)localObject4, (String)localObject2);
        localObject4 = Locale.US;
        localObject2 = "%d %s%n";
        int k = 2;
        localObject3 = new Object[k];
        int m = 0;
        Long localLong = Long.valueOf(paramLong);
        localObject3[0] = localLong;
        m = 1;
        localObject3[m] = localObject1;
        localObject1 = String.format((Locale)localObject4, (String)localObject2, (Object[])localObject3);
        localObject4 = "UTF-8";
        localObject1 = ((String)localObject1).getBytes((String)localObject4);
        localObject4 = logFile;
        ((q)localObject4).a((byte[])localObject1);
        for (;;)
        {
          localObject1 = logFile;
          boolean bool = ((q)localObject1).b();
          if (bool) {
            break;
          }
          localObject1 = logFile;
          int n = ((q)localObject1).a();
          i = maxLogSize;
          if (n <= i) {
            break;
          }
          localObject1 = logFile;
          ((q)localObject1).c();
        }
      }
      catch (IOException localIOException)
      {
        Object localObject4 = c.h();
        Object localObject2 = "CrashlyticsCore";
        Object localObject3 = "There was a problem writing to the Crashlytics log.";
        ((k)localObject4).e((String)localObject2, (String)localObject3, localIOException);
      }
    }
  }
  
  private void openLogFile()
  {
    q localq = logFile;
    if (localq == null) {}
    try
    {
      localq = new io/fabric/sdk/android/services/b/q;
      localObject1 = workingFile;
      localq.<init>((File)localObject1);
      logFile = localq;
      return;
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        Object localObject1 = c.h();
        String str = "CrashlyticsCore";
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("Could not open log file: ");
        File localFile = workingFile;
        localObject2 = localFile;
        ((k)localObject1).e(str, (String)localObject2, localIOException);
      }
    }
  }
  
  public void closeLogFile()
  {
    i.a(logFile, "There was a problem closing the Crashlytics log file.");
    logFile = null;
  }
  
  public void deleteLogFile()
  {
    closeLogFile();
    workingFile.delete();
  }
  
  public ByteString getLogAsByteString()
  {
    int i = 0;
    Object localObject1 = null;
    Object localObject2 = workingFile;
    boolean bool = ((File)localObject2).exists();
    if (!bool) {}
    for (;;)
    {
      return (ByteString)localObject1;
      openLogFile();
      localObject2 = logFile;
      byte[] arrayOfByte;
      if (localObject2 != null)
      {
        localObject2 = new int[1];
        localObject2[0] = 0;
        localObject1 = logFile;
        i = ((q)localObject1).a();
        arrayOfByte = new byte[i];
      }
      try
      {
        localObject1 = logFile;
        localObject3 = new com/crashlytics/android/core/QueueFileLogStore$1;
        ((QueueFileLogStore.1)localObject3).<init>(this, arrayOfByte, (int[])localObject2);
        ((q)localObject1).a((q.c)localObject3);
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          Object localObject3 = c.h();
          String str1 = "CrashlyticsCore";
          String str2 = "A problem occurred while reading the Crashlytics log file.";
          ((k)localObject3).e(str1, str2, localIOException);
        }
      }
      i = localObject2[0];
      localObject1 = ByteString.copyFrom(arrayOfByte, 0, i);
    }
  }
  
  public void writeToLog(long paramLong, String paramString)
  {
    openLogFile();
    doWriteToLog(paramLong, paramString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/QueueFileLogStore.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */