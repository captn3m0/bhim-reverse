package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

class ReportUploader
{
  private static final String CLS_FILE_EXT = ".cls";
  static final Map HEADER_INVALID_CLS_FILE;
  private static final short[] RETRY_INTERVALS;
  private static final FilenameFilter crashFileFilter;
  private final String apiKey;
  private final CreateReportSpiCall createReportCall;
  private final Object fileAccessLock;
  private Thread uploadThread;
  
  static
  {
    Object localObject = new com/crashlytics/android/core/ReportUploader$1;
    ((ReportUploader.1)localObject).<init>();
    crashFileFilter = (FilenameFilter)localObject;
    HEADER_INVALID_CLS_FILE = Collections.singletonMap("X-CRASHLYTICS-INVALID-SESSION", "1");
    localObject = new short[6];
    localObject[0] = 10;
    localObject[1] = 20;
    localObject[2] = 30;
    localObject[3] = 60;
    localObject[4] = 120;
    localObject[5] = 'Ĭ';
    RETRY_INTERVALS = (short[])localObject;
  }
  
  public ReportUploader(String paramString, CreateReportSpiCall paramCreateReportSpiCall)
  {
    Object localObject = new java/lang/Object;
    localObject.<init>();
    fileAccessLock = localObject;
    if (paramCreateReportSpiCall == null)
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("createReportCall must not be null.");
      throw ((Throwable)localObject);
    }
    createReportCall = paramCreateReportSpiCall;
    apiKey = paramString;
  }
  
  List findReports()
  {
    int i = 0;
    Object localObject1 = null;
    c.h().a("CrashlyticsCore", "Checking for crash reports...");
    Object localObject4 = CrashlyticsCore.getInstance();
    Object localObject5 = ((CrashlyticsCore)localObject4).getHandler();
    int j;
    int k;
    Object localObject8;
    Object localObject9;
    synchronized (fileAccessLock)
    {
      localObject4 = ((CrashlyticsCore)localObject4).getSdkDirectory();
      localObject7 = crashFileFilter;
      localObject7 = ((File)localObject4).listFiles((FilenameFilter)localObject7);
      localObject4 = ((CrashlyticsUncaughtExceptionHandler)localObject5).getInvalidFilesDir();
      localObject5 = ((File)localObject4).listFiles();
      ??? = new java/util/LinkedList;
      ((LinkedList)???).<init>();
      j = localObject7.length;
      k = 0;
      localObject4 = null;
      if (k < j)
      {
        localObject8 = localObject7[k];
        localObject9 = c.h();
        String str1 = "CrashlyticsCore";
        Object localObject10 = new java/lang/StringBuilder;
        ((StringBuilder)localObject10).<init>();
        localObject10 = ((StringBuilder)localObject10).append("Found crash report ");
        String str2 = ((File)localObject8).getPath();
        localObject10 = str2;
        ((k)localObject9).a(str1, (String)localObject10);
        localObject9 = new com/crashlytics/android/core/SessionReport;
        ((SessionReport)localObject9).<init>((File)localObject8);
        ((List)???).add(localObject9);
        k += 1;
      }
    }
    Object localObject7 = new java/util/HashMap;
    ((HashMap)localObject7).<init>();
    if (localObject5 != null)
    {
      j = localObject5.length;
      k = 0;
      localObject4 = null;
      while (k < j)
      {
        localObject8 = localObject5[k];
        localObject3 = CrashlyticsUncaughtExceptionHandler.getSessionIdFromSessionFile((File)localObject8);
        boolean bool2 = ((Map)localObject7).containsKey(localObject3);
        if (!bool2)
        {
          localObject9 = new java/util/LinkedList;
          ((LinkedList)localObject9).<init>();
          ((Map)localObject7).put(localObject3, localObject9);
        }
        localObject3 = (List)((Map)localObject7).get(localObject3);
        ((List)localObject3).add(localObject8);
        i = k + 1;
        k = i;
      }
    }
    Object localObject3 = ((Map)localObject7).keySet();
    localObject5 = ((Set)localObject3).iterator();
    for (;;)
    {
      bool1 = ((Iterator)localObject5).hasNext();
      if (!bool1) {
        break;
      }
      localObject3 = ((Iterator)localObject5).next();
      localObject4 = localObject3;
      localObject4 = (String)localObject3;
      localObject3 = c.h();
      localObject8 = new java/lang/StringBuilder;
      ((StringBuilder)localObject8).<init>();
      localObject9 = "Found invalid session: ";
      localObject8 = (String)localObject9 + (String)localObject4;
      ((k)localObject3).a("CrashlyticsCore", (String)localObject8);
      localObject3 = (List)((Map)localObject7).get(localObject4);
      InvalidSessionReport localInvalidSessionReport = new com/crashlytics/android/core/InvalidSessionReport;
      int m = ((List)localObject3).size();
      localObject8 = new File[m];
      localObject3 = (File[])((List)localObject3).toArray((Object[])localObject8);
      localInvalidSessionReport.<init>((String)localObject4, (File[])localObject3);
      ((List)???).add(localInvalidSessionReport);
    }
    boolean bool1 = ((List)???).isEmpty();
    if (bool1)
    {
      localObject3 = c.h();
      localObject4 = "CrashlyticsCore";
      localObject5 = "No reports found.";
      ((k)localObject3).a((String)localObject4, (String)localObject5);
    }
    return (List)???;
  }
  
  boolean forceUpload(Report paramReport)
  {
    boolean bool1 = false;
    Object localObject1 = null;
    synchronized (fileAccessLock)
    {
      try
      {
        localObject4 = new com/crashlytics/android/core/CreateReportRequest;
        localObject5 = apiKey;
        ((CreateReportRequest)localObject4).<init>((String)localObject5, paramReport);
        localObject5 = createReportCall;
        boolean bool2 = ((CreateReportSpiCall)localObject5).invoke((CreateReportRequest)localObject4);
        localObject6 = c.h();
        localObject7 = "CrashlyticsCore";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject8 = "Crashlytics report upload ";
        localObject8 = ((StringBuilder)localObject4).append((String)localObject8);
        if (!bool2) {
          break label152;
        }
        localObject4 = "complete: ";
        localObject4 = ((StringBuilder)localObject8).append((String)localObject4);
        localObject8 = paramReport.getIdentifier();
        localObject4 = ((StringBuilder)localObject4).append((String)localObject8);
        localObject4 = ((StringBuilder)localObject4).toString();
        ((k)localObject6).c((String)localObject7, (String)localObject4);
        if (bool2)
        {
          paramReport.remove();
          bool1 = true;
        }
      }
      catch (Exception localException)
      {
        for (;;)
        {
          Object localObject4;
          label152:
          Object localObject5 = c.h();
          Object localObject6 = "CrashlyticsCore";
          Object localObject7 = new java/lang/StringBuilder;
          ((StringBuilder)localObject7).<init>();
          Object localObject8 = "Error occurred sending report ";
          localObject7 = ((StringBuilder)localObject7).append((String)localObject8);
          localObject7 = ((StringBuilder)localObject7).append(paramReport);
          localObject7 = ((StringBuilder)localObject7).toString();
          ((k)localObject5).e((String)localObject6, (String)localObject7, localException);
        }
      }
      return bool1;
      localObject4 = "FAILED: ";
    }
  }
  
  boolean isUploading()
  {
    Thread localThread = uploadThread;
    boolean bool;
    if (localThread != null) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localThread = null;
    }
  }
  
  public void uploadReports()
  {
    uploadReports(0.0F);
  }
  
  public void uploadReports(float paramFloat)
  {
    try
    {
      Thread localThread = uploadThread;
      if (localThread == null)
      {
        localThread = new java/lang/Thread;
        ReportUploader.Worker localWorker = new com/crashlytics/android/core/ReportUploader$Worker;
        localWorker.<init>(this, paramFloat);
        String str = "Crashlytics Report Uploader";
        localThread.<init>(localWorker, str);
        uploadThread = localThread;
        localThread = uploadThread;
        localThread.start();
      }
      return;
    }
    finally {}
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/ReportUploader.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */