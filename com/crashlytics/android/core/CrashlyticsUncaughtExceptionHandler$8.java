package com.crashlytics.android.core;

import java.io.File;
import java.util.concurrent.Callable;

class CrashlyticsUncaughtExceptionHandler$8
  implements Callable
{
  CrashlyticsUncaughtExceptionHandler$8(CrashlyticsUncaughtExceptionHandler paramCrashlyticsUncaughtExceptionHandler, String paramString1, String paramString2, String paramString3) {}
  
  public Void call()
  {
    String str1 = CrashlyticsUncaughtExceptionHandler.access$500(this$0);
    MetaDataStore localMetaDataStore = new com/crashlytics/android/core/MetaDataStore;
    Object localObject = CrashlyticsUncaughtExceptionHandler.access$600(this$0);
    localMetaDataStore.<init>((File)localObject);
    localObject = new com/crashlytics/android/core/UserMetaData;
    String str2 = val$userId;
    String str3 = val$userName;
    String str4 = val$userEmail;
    ((UserMetaData)localObject).<init>(str2, str3, str4);
    localMetaDataStore.writeUserData(str1, (UserMetaData)localObject);
    return null;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$8.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */