package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.concurrent.Callable;

class CrashlyticsExecutorServiceWrapper$2
  implements Callable
{
  CrashlyticsExecutorServiceWrapper$2(CrashlyticsExecutorServiceWrapper paramCrashlyticsExecutorServiceWrapper, Callable paramCallable) {}
  
  public Object call()
  {
    try
    {
      localObject1 = val$callable;
      localObject1 = ((Callable)localObject1).call();
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject1;
        k localk = c.h();
        String str1 = "CrashlyticsCore";
        String str2 = "Failed to execute task.";
        localk.e(str1, str2, localException);
        Object localObject2 = null;
      }
    }
    return localObject1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsExecutorServiceWrapper$2.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */