package com.crashlytics.android.core;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.e.q;
import io.fabric.sdk.android.services.e.t;
import java.io.File;
import java.util.Map;

final class CrashlyticsUncaughtExceptionHandler$SendSessionRunnable
  implements Runnable
{
  private final CrashlyticsCore crashlyticsCore;
  private final File fileToSend;
  
  public CrashlyticsUncaughtExceptionHandler$SendSessionRunnable(CrashlyticsCore paramCrashlyticsCore, File paramFile)
  {
    crashlyticsCore = paramCrashlyticsCore;
    fileToSend = paramFile;
  }
  
  public void run()
  {
    Object localObject1 = crashlyticsCore.getContext();
    boolean bool = i.n((Context)localObject1);
    if (!bool) {}
    for (;;)
    {
      return;
      localObject1 = c.h();
      Object localObject2 = "Attempting to send crash report at time of crash...";
      ((k)localObject1).a("CrashlyticsCore", (String)localObject2);
      localObject1 = q.a().b();
      Object localObject3 = crashlyticsCore;
      localObject1 = ((CrashlyticsCore)localObject3).getCreateReportSpiCall((t)localObject1);
      if (localObject1 != null)
      {
        localObject3 = new com/crashlytics/android/core/ReportUploader;
        localObject2 = crashlyticsCore.getApiKey();
        ((ReportUploader)localObject3).<init>((String)localObject2, (CreateReportSpiCall)localObject1);
        localObject1 = new com/crashlytics/android/core/SessionReport;
        localObject2 = fileToSend;
        Map localMap = CrashlyticsUncaughtExceptionHandler.access$1200();
        ((SessionReport)localObject1).<init>((File)localObject2, localMap);
        ((ReportUploader)localObject3).forceUpload((Report)localObject1);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$SendSessionRunnable.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */