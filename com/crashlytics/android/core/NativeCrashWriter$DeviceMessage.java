package com.crashlytics.android.core;

final class NativeCrashWriter$DeviceMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private static final int PROTOBUF_TAG = 5;
  private final float batteryLevel;
  private final int batteryVelocity;
  private final long diskUsed;
  private final int orientation;
  private final boolean proximityOn;
  private final long ramUsed;
  
  public NativeCrashWriter$DeviceMessage(float paramFloat, int paramInt1, boolean paramBoolean, int paramInt2, long paramLong1, long paramLong2)
  {
    super(5, arrayOfProtobufMessage);
    batteryLevel = paramFloat;
    batteryVelocity = paramInt1;
    proximityOn = paramBoolean;
    orientation = paramInt2;
    ramUsed = paramLong1;
    diskUsed = paramLong2;
  }
  
  public int getPropertiesSize()
  {
    float f = batteryLevel;
    int i = CodedOutputStream.computeFloatSize(1, f);
    int j = 0 + i;
    int k = batteryVelocity;
    i = CodedOutputStream.computeSInt32Size(2, k);
    j += i;
    boolean bool = proximityOn;
    i = CodedOutputStream.computeBoolSize(3, bool);
    j += i;
    int m = orientation;
    i = CodedOutputStream.computeUInt32Size(4, m);
    j += i;
    long l = ramUsed;
    i = CodedOutputStream.computeUInt64Size(5, l);
    j += i;
    l = diskUsed;
    i = CodedOutputStream.computeUInt64Size(6, l);
    return j + i;
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream)
  {
    float f = batteryLevel;
    paramCodedOutputStream.writeFloat(1, f);
    int i = batteryVelocity;
    paramCodedOutputStream.writeSInt32(2, i);
    boolean bool = proximityOn;
    paramCodedOutputStream.writeBool(3, bool);
    int j = orientation;
    paramCodedOutputStream.writeUInt32(4, j);
    long l = ramUsed;
    paramCodedOutputStream.writeUInt64(5, l);
    l = diskUsed;
    paramCodedOutputStream.writeUInt64(6, l);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$DeviceMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */