package com.crashlytics.android.core;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.concurrent.atomic.AtomicBoolean;

class DevicePowerStateListener
{
  private static final IntentFilter FILTER_BATTERY_CHANGED;
  private static final IntentFilter FILTER_POWER_CONNECTED;
  private static final IntentFilter FILTER_POWER_DISCONNECTED;
  private final Context context;
  private boolean isPowerConnected;
  private final BroadcastReceiver powerConnectedReceiver;
  private final BroadcastReceiver powerDisconnectedReceiver;
  private final AtomicBoolean receiversRegistered;
  
  static
  {
    IntentFilter localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("android.intent.action.BATTERY_CHANGED");
    FILTER_BATTERY_CHANGED = localIntentFilter;
    localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("android.intent.action.ACTION_POWER_CONNECTED");
    FILTER_POWER_CONNECTED = localIntentFilter;
    localIntentFilter = new android/content/IntentFilter;
    localIntentFilter.<init>("android.intent.action.ACTION_POWER_DISCONNECTED");
    FILTER_POWER_DISCONNECTED = localIntentFilter;
  }
  
  public DevicePowerStateListener(Context paramContext)
  {
    context = paramContext;
    int k = 0;
    Object localObject1 = FILTER_BATTERY_CHANGED;
    Object localObject2 = paramContext.registerReceiver(null, (IntentFilter)localObject1);
    if (localObject2 != null)
    {
      localObject1 = "status";
      j = ((Intent)localObject2).getIntExtra((String)localObject1, j);
    }
    k = 2;
    if (j != k)
    {
      k = 5;
      if (j != k) {}
    }
    else
    {
      j = i;
    }
    for (;;)
    {
      isPowerConnected = j;
      Object localObject3 = new com/crashlytics/android/core/DevicePowerStateListener$1;
      ((DevicePowerStateListener.1)localObject3).<init>(this);
      powerConnectedReceiver = ((BroadcastReceiver)localObject3);
      localObject3 = new com/crashlytics/android/core/DevicePowerStateListener$2;
      ((DevicePowerStateListener.2)localObject3).<init>(this);
      powerDisconnectedReceiver = ((BroadcastReceiver)localObject3);
      localObject3 = powerConnectedReceiver;
      localObject2 = FILTER_POWER_CONNECTED;
      paramContext.registerReceiver((BroadcastReceiver)localObject3, (IntentFilter)localObject2);
      localObject3 = powerDisconnectedReceiver;
      localObject2 = FILTER_POWER_DISCONNECTED;
      paramContext.registerReceiver((BroadcastReceiver)localObject3, (IntentFilter)localObject2);
      localObject3 = new java/util/concurrent/atomic/AtomicBoolean;
      ((AtomicBoolean)localObject3).<init>(i);
      receiversRegistered = ((AtomicBoolean)localObject3);
      return;
      j = 0;
      localObject3 = null;
    }
  }
  
  public void dispose()
  {
    Object localObject = receiversRegistered;
    BroadcastReceiver localBroadcastReceiver = null;
    boolean bool = ((AtomicBoolean)localObject).getAndSet(false);
    if (!bool) {}
    for (;;)
    {
      return;
      localObject = context;
      localBroadcastReceiver = powerConnectedReceiver;
      ((Context)localObject).unregisterReceiver(localBroadcastReceiver);
      localObject = context;
      localBroadcastReceiver = powerDisconnectedReceiver;
      ((Context)localObject).unregisterReceiver(localBroadcastReceiver);
    }
  }
  
  public boolean isPowerConnected()
  {
    return isPowerConnected;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/DevicePowerStateListener.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */