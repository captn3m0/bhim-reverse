package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

class InvalidSessionReport
  implements Report
{
  private final Map customHeaders;
  private final File[] files;
  private final String identifier;
  
  public InvalidSessionReport(String paramString, File[] paramArrayOfFile)
  {
    files = paramArrayOfFile;
    HashMap localHashMap = new java/util/HashMap;
    Map localMap = ReportUploader.HEADER_INVALID_CLS_FILE;
    localHashMap.<init>(localMap);
    customHeaders = localHashMap;
    identifier = paramString;
  }
  
  public Map getCustomHeaders()
  {
    return Collections.unmodifiableMap(customHeaders);
  }
  
  public File getFile()
  {
    return files[0];
  }
  
  public String getFileName()
  {
    return files[0].getName();
  }
  
  public File[] getFiles()
  {
    return files;
  }
  
  public String getIdentifier()
  {
    return identifier;
  }
  
  public void remove()
  {
    File[] arrayOfFile = files;
    int i = arrayOfFile.length;
    int j = 0;
    while (j < i)
    {
      File localFile = arrayOfFile[j];
      k localk = c.h();
      String str1 = "CrashlyticsCore";
      Object localObject = new java/lang/StringBuilder;
      ((StringBuilder)localObject).<init>();
      localObject = ((StringBuilder)localObject).append("Removing invalid report file at ");
      String str2 = localFile.getPath();
      localObject = str2;
      localk.a(str1, (String)localObject);
      localFile.delete();
      j += 1;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/InvalidSessionReport.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */