package com.crashlytics.android.core;

import com.crashlytics.android.core.internal.models.CustomAttributeData;

final class NativeCrashWriter$CustomAttributeMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private static final int PROTOBUF_TAG = 2;
  private final String key;
  private final String value;
  
  public NativeCrashWriter$CustomAttributeMessage(CustomAttributeData paramCustomAttributeData)
  {
    super(2, arrayOfProtobufMessage);
    String str = key;
    key = str;
    str = value;
    value = str;
  }
  
  public int getPropertiesSize()
  {
    int i = 1;
    ByteString localByteString = ByteString.copyFromUtf8(key);
    int j = CodedOutputStream.computeBytesSize(i, localByteString);
    int k = 2;
    Object localObject = value;
    if (localObject == null) {}
    for (localObject = "";; localObject = value)
    {
      localObject = ByteString.copyFromUtf8((String)localObject);
      return CodedOutputStream.computeBytesSize(k, (ByteString)localObject) + j;
    }
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream)
  {
    int i = 1;
    ByteString localByteString = ByteString.copyFromUtf8(key);
    paramCodedOutputStream.writeBytes(i, localByteString);
    int j = 2;
    Object localObject = value;
    if (localObject == null) {}
    for (localObject = "";; localObject = value)
    {
      localObject = ByteString.copyFromUtf8((String)localObject);
      paramCodedOutputStream.writeBytes(j, (ByteString)localObject);
      return;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$CustomAttributeMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */