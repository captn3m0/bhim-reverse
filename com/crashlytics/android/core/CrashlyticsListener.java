package com.crashlytics.android.core;

public abstract interface CrashlyticsListener
{
  public abstract void crashlyticsDidDetectCrashDuringPreviousExecution();
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsListener.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */