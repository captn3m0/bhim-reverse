package com.crashlytics.android.core;

import java.io.File;
import java.util.Comparator;

final class CrashlyticsUncaughtExceptionHandler$2
  implements Comparator
{
  public int compare(File paramFile1, File paramFile2)
  {
    String str1 = paramFile2.getName();
    String str2 = paramFile1.getName();
    return str1.compareTo(str2);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$2.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */