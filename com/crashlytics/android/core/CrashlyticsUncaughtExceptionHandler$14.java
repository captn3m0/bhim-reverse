package com.crashlytics.android.core;

import com.crashlytics.android.core.internal.models.SessionEventData;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicBoolean;

class CrashlyticsUncaughtExceptionHandler$14
  implements Callable
{
  CrashlyticsUncaughtExceptionHandler$14(CrashlyticsUncaughtExceptionHandler paramCrashlyticsUncaughtExceptionHandler, SessionEventData paramSessionEventData) {}
  
  public Void call()
  {
    Object localObject = CrashlyticsUncaughtExceptionHandler.access$200(this$0);
    boolean bool = ((AtomicBoolean)localObject).get();
    if (!bool)
    {
      localObject = this$0;
      SessionEventData localSessionEventData = val$crashEventData;
      CrashlyticsUncaughtExceptionHandler.access$1100((CrashlyticsUncaughtExceptionHandler)localObject, localSessionEventData);
    }
    return null;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$14.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */