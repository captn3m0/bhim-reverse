package com.crashlytics.android.core;

import java.io.File;
import java.io.FilenameFilter;

class CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter
  implements FilenameFilter
{
  private final String string;
  
  public CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter(String paramString)
  {
    string = paramString;
  }
  
  public boolean accept(File paramFile, String paramString)
  {
    String str = string;
    boolean bool = paramString.contains(str);
    if (bool)
    {
      str = ".cls_temp";
      bool = paramString.endsWith(str);
      if (!bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      str = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$FileNameContainsFilter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */