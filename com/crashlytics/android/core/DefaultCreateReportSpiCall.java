package com.crashlytics.android.core;

import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.a;
import io.fabric.sdk.android.services.b.r;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.d;
import java.io.File;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class DefaultCreateReportSpiCall
  extends a
  implements CreateReportSpiCall
{
  static final String FILE_CONTENT_TYPE = "application/octet-stream";
  static final String FILE_PARAM = "report[file]";
  static final String IDENTIFIER_PARAM = "report[identifier]";
  static final String MULTI_FILE_PARAM = "report[file";
  
  public DefaultCreateReportSpiCall(h paramh, String paramString1, String paramString2, d paramd)
  {
    super(paramh, paramString1, paramString2, paramd, localc);
  }
  
  DefaultCreateReportSpiCall(h paramh, String paramString1, String paramString2, d paramd, io.fabric.sdk.android.services.network.c paramc)
  {
    super(paramh, paramString1, paramString2, paramd, paramc);
  }
  
  private HttpRequest applyHeadersTo(HttpRequest paramHttpRequest, CreateReportRequest paramCreateReportRequest)
  {
    Object localObject1 = apiKey;
    Object localObject2 = paramHttpRequest.a("X-CRASHLYTICS-API-KEY", (String)localObject1).a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    Object localObject3 = kit.getVersion();
    localObject2 = ((HttpRequest)localObject2).a("X-CRASHLYTICS-API-CLIENT-VERSION", (String)localObject3);
    localObject3 = report.getCustomHeaders().entrySet().iterator();
    for (localObject1 = localObject2;; localObject1 = localObject2)
    {
      boolean bool = ((Iterator)localObject3).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (Map.Entry)((Iterator)localObject3).next();
      localObject2 = ((HttpRequest)localObject1).a((Map.Entry)localObject2);
    }
    return (HttpRequest)localObject1;
  }
  
  private HttpRequest applyMultipartDataTo(HttpRequest paramHttpRequest, Report paramReport)
  {
    int i = 0;
    Object localObject1 = null;
    Object localObject2 = paramReport.getIdentifier();
    paramHttpRequest.e("report[identifier]", (String)localObject2);
    Object localObject3 = paramReport.getFiles();
    int j = localObject3.length;
    int k = 1;
    if (j == k)
    {
      localObject1 = io.fabric.sdk.android.c.h();
      localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = ((StringBuilder)localObject2).append("Adding single file ");
      Object localObject4 = paramReport.getFileName();
      localObject2 = ((StringBuilder)localObject2).append((String)localObject4).append(" to report ");
      localObject4 = paramReport.getIdentifier();
      localObject2 = (String)localObject4;
      ((k)localObject1).a("CrashlyticsCore", (String)localObject2);
      localObject1 = "report[file]";
      localObject3 = paramReport.getFileName();
      localObject2 = "application/octet-stream";
      localObject4 = paramReport.getFile();
      paramHttpRequest = paramHttpRequest.a((String)localObject1, (String)localObject3, (String)localObject2, (File)localObject4);
    }
    for (;;)
    {
      return paramHttpRequest;
      localObject2 = paramReport.getFiles();
      int m = localObject2.length;
      j = 0;
      localObject3 = null;
      while (i < m)
      {
        File localFile = localObject2[i];
        Object localObject5 = io.fabric.sdk.android.c.h();
        Object localObject6 = new java/lang/StringBuilder;
        ((StringBuilder)localObject6).<init>();
        localObject6 = ((StringBuilder)localObject6).append("Adding file ");
        String str1 = localFile.getName();
        localObject6 = ((StringBuilder)localObject6).append(str1).append(" to report ");
        str1 = paramReport.getIdentifier();
        localObject6 = str1;
        ((k)localObject5).a("CrashlyticsCore", (String)localObject6);
        localObject5 = new java/lang/StringBuilder;
        ((StringBuilder)localObject5).<init>();
        localObject5 = "report[file" + j + "]";
        String str2 = localFile.getName();
        localObject6 = "application/octet-stream";
        paramHttpRequest.a((String)localObject5, str2, (String)localObject6, localFile);
        j += 1;
        i += 1;
      }
    }
  }
  
  public boolean invoke(CreateReportRequest paramCreateReportRequest)
  {
    Object localObject1 = getHttpRequest();
    localObject1 = applyHeadersTo((HttpRequest)localObject1, paramCreateReportRequest);
    Object localObject2 = report;
    localObject1 = applyMultipartDataTo((HttpRequest)localObject1, (Report)localObject2);
    localObject2 = io.fabric.sdk.android.c.h();
    Object localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = ((StringBuilder)localObject3).append("Sending report to: ");
    Object localObject4 = getUrl();
    localObject3 = (String)localObject4;
    ((k)localObject2).a("CrashlyticsCore", (String)localObject3);
    int i = ((HttpRequest)localObject1).b();
    Object localObject5 = io.fabric.sdk.android.c.h();
    localObject4 = new java/lang/StringBuilder;
    ((StringBuilder)localObject4).<init>();
    localObject4 = ((StringBuilder)localObject4).append("Create report request ID: ");
    String str = "X-REQUEST-ID";
    localObject1 = ((HttpRequest)localObject1).b(str);
    localObject1 = (String)localObject1;
    ((k)localObject5).a("CrashlyticsCore", (String)localObject1);
    localObject1 = io.fabric.sdk.android.c.h();
    localObject5 = "CrashlyticsCore";
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject4 = "Result was: ";
    localObject3 = (String)localObject4 + i;
    ((k)localObject1).a((String)localObject5, (String)localObject3);
    int j = r.a(i);
    if (j == 0) {
      j = 1;
    }
    for (;;)
    {
      return j;
      int k = 0;
      localObject1 = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/DefaultCreateReportSpiCall.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */