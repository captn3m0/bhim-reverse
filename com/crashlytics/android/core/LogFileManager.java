package com.crashlytics.android.core;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.d.a;
import java.io.File;
import java.util.Set;

class LogFileManager
{
  private static final String DIRECTORY_NAME = "log-files";
  private static final String LOGFILE_EXT = ".temp";
  private static final String LOGFILE_PREFIX = "crashlytics-userlog-";
  static final int MAX_LOG_SIZE = 65536;
  private static final LogFileManager.NoopLogStore NOOP_LOG_STORE;
  private final Context context;
  private FileLogStore currentLog;
  private final a fileStore;
  
  static
  {
    LogFileManager.NoopLogStore localNoopLogStore = new com/crashlytics/android/core/LogFileManager$NoopLogStore;
    localNoopLogStore.<init>(null);
    NOOP_LOG_STORE = localNoopLogStore;
  }
  
  public LogFileManager(Context paramContext, a parama)
  {
    this(paramContext, parama, null);
  }
  
  public LogFileManager(Context paramContext, a parama, String paramString)
  {
    context = paramContext;
    fileStore = parama;
    LogFileManager.NoopLogStore localNoopLogStore = NOOP_LOG_STORE;
    currentLog = localNoopLogStore;
    setCurrentSession(paramString);
  }
  
  private File getLogFileDir()
  {
    File localFile1 = new java/io/File;
    File localFile2 = fileStore.a();
    String str = "log-files";
    localFile1.<init>(localFile2, str);
    boolean bool = localFile1.exists();
    if (!bool) {
      localFile1.mkdirs();
    }
    return localFile1;
  }
  
  private String getSessionIdForFile(File paramFile)
  {
    String str1 = paramFile.getName();
    String str2 = ".temp";
    int i = str1.lastIndexOf(str2);
    int j = -1;
    if (i == j) {}
    for (;;)
    {
      return str1;
      String str3 = "crashlytics-userlog-";
      j = str3.length();
      str1 = str1.substring(j, i);
    }
  }
  
  private File getWorkingFileForSession(String paramString)
  {
    Object localObject = new java/lang/StringBuilder;
    ((StringBuilder)localObject).<init>();
    localObject = "crashlytics-userlog-" + paramString + ".temp";
    File localFile1 = new java/io/File;
    File localFile2 = getLogFileDir();
    localFile1.<init>(localFile2, (String)localObject);
    return localFile1;
  }
  
  private boolean isLoggingEnabled()
  {
    return i.a(context, "com.crashlytics.CollectCustomLogs", true);
  }
  
  public void clearLog()
  {
    currentLog.deleteLogFile();
  }
  
  public void discardOldLogFiles(Set paramSet)
  {
    File localFile1 = getLogFileDir();
    File[] arrayOfFile = localFile1.listFiles();
    if (arrayOfFile != null)
    {
      int i = arrayOfFile.length;
      int j = 0;
      localFile1 = null;
      while (j < i)
      {
        File localFile2 = arrayOfFile[j];
        String str = getSessionIdForFile(localFile2);
        boolean bool = paramSet.contains(str);
        if (!bool) {
          localFile2.delete();
        }
        j += 1;
      }
    }
  }
  
  public ByteString getByteStringForLog()
  {
    return currentLog.getLogAsByteString();
  }
  
  public final void setCurrentSession(String paramString)
  {
    currentLog.closeLogFile();
    Object localObject = NOOP_LOG_STORE;
    currentLog = ((FileLogStore)localObject);
    if (paramString == null) {}
    for (;;)
    {
      return;
      boolean bool = isLoggingEnabled();
      if (!bool)
      {
        localObject = c.h();
        String str1 = "CrashlyticsCore";
        String str2 = "Preferences requested no custom logs. Aborting log file creation.";
        ((k)localObject).a(str1, str2);
      }
      else
      {
        localObject = getWorkingFileForSession(paramString);
        int i = 65536;
        setLogFile((File)localObject, i);
      }
    }
  }
  
  void setLogFile(File paramFile, int paramInt)
  {
    QueueFileLogStore localQueueFileLogStore = new com/crashlytics/android/core/QueueFileLogStore;
    localQueueFileLogStore.<init>(paramFile, paramInt);
    currentLog = localQueueFileLogStore;
  }
  
  public void writeToLog(long paramLong, String paramString)
  {
    currentLog.writeToLog(paramLong, paramString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/LogFileManager.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */