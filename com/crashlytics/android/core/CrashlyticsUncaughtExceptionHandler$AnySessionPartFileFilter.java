package com.crashlytics.android.core;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class CrashlyticsUncaughtExceptionHandler$AnySessionPartFileFilter
  implements FilenameFilter
{
  public boolean accept(File paramFile, String paramString)
  {
    Object localObject = CrashlyticsUncaughtExceptionHandler.SESSION_FILE_FILTER;
    boolean bool = ((FilenameFilter)localObject).accept(paramFile, paramString);
    if (!bool)
    {
      localObject = CrashlyticsUncaughtExceptionHandler.access$000().matcher(paramString);
      bool = ((Matcher)localObject).matches();
      if (bool) {
        bool = true;
      }
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject = null;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$AnySessionPartFileFilter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */