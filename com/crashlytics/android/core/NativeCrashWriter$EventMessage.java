package com.crashlytics.android.core;

final class NativeCrashWriter$EventMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private static final int PROTOBUF_TAG = 10;
  private final String crashType;
  private final long time;
  
  public NativeCrashWriter$EventMessage(long paramLong, String paramString, NativeCrashWriter.ProtobufMessage... paramVarArgs)
  {
    super(10, paramVarArgs);
    time = paramLong;
    crashType = paramString;
  }
  
  public int getPropertiesSize()
  {
    long l = time;
    int i = CodedOutputStream.computeUInt64Size(1, l);
    ByteString localByteString = ByteString.copyFromUtf8(crashType);
    int j = CodedOutputStream.computeBytesSize(2, localByteString);
    return i + j;
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream)
  {
    long l = time;
    paramCodedOutputStream.writeUInt64(1, l);
    ByteString localByteString = ByteString.copyFromUtf8(crashType);
    paramCodedOutputStream.writeBytes(2, localByteString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$EventMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */