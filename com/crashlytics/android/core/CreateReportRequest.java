package com.crashlytics.android.core;

class CreateReportRequest
{
  public final String apiKey;
  public final Report report;
  
  public CreateReportRequest(String paramString, Report paramReport)
  {
    apiKey = paramString;
    report = paramReport;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CreateReportRequest.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */