package com.crashlytics.android.core;

class MiddleOutStrategy
  implements StackTraceTrimmingStrategy
{
  private final int trimmedSize;
  
  public MiddleOutStrategy(int paramInt)
  {
    trimmedSize = paramInt;
  }
  
  public StackTraceElement[] getTrimmedStackTrace(StackTraceElement[] paramArrayOfStackTraceElement)
  {
    int i = 0;
    int j = paramArrayOfStackTraceElement.length;
    int k = trimmedSize;
    if (j <= k) {}
    for (;;)
    {
      return paramArrayOfStackTraceElement;
      k = trimmedSize / 2;
      int m = trimmedSize - k;
      j = trimmedSize;
      StackTraceElement[] arrayOfStackTraceElement = new StackTraceElement[j];
      System.arraycopy(paramArrayOfStackTraceElement, 0, arrayOfStackTraceElement, 0, m);
      i = paramArrayOfStackTraceElement.length - k;
      System.arraycopy(paramArrayOfStackTraceElement, i, arrayOfStackTraceElement, m, k);
      paramArrayOfStackTraceElement = arrayOfStackTraceElement;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/MiddleOutStrategy.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */