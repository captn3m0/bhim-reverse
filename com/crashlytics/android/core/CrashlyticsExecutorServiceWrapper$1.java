package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;

class CrashlyticsExecutorServiceWrapper$1
  implements Runnable
{
  CrashlyticsExecutorServiceWrapper$1(CrashlyticsExecutorServiceWrapper paramCrashlyticsExecutorServiceWrapper, Runnable paramRunnable) {}
  
  public void run()
  {
    try
    {
      Runnable localRunnable = val$runnable;
      localRunnable.run();
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        k localk = c.h();
        String str1 = "CrashlyticsCore";
        String str2 = "Failed to execute task.";
        localk.e(str1, str2, localException);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsExecutorServiceWrapper$1.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */