package com.crashlytics.android.core.internal.models;

public class DeviceData
{
  public final long availableInternalStorage;
  public final long availablePhysicalMemory;
  public final int batteryCapacity;
  public final int batteryVelocity;
  public final int orientation;
  public final boolean proximity;
  public final long totalInternalStorage;
  public final long totalPhysicalMemory;
  
  public DeviceData(int paramInt1, long paramLong1, long paramLong2, long paramLong3, long paramLong4, int paramInt2, int paramInt3, boolean paramBoolean)
  {
    orientation = paramInt1;
    totalPhysicalMemory = paramLong1;
    totalInternalStorage = paramLong2;
    availablePhysicalMemory = paramLong3;
    availableInternalStorage = paramLong4;
    batteryCapacity = paramInt2;
    batteryVelocity = paramInt3;
    proximity = paramBoolean;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/internal/models/DeviceData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */