package com.crashlytics.android.core.internal.models;

public class SessionEventData
{
  public final BinaryImageData[] binaryImages;
  public final CustomAttributeData[] customAttributes;
  public final DeviceData deviceData;
  public final SignalData signal;
  public final ThreadData[] threads;
  public final long timestamp;
  
  public SessionEventData(long paramLong, SignalData paramSignalData, ThreadData[] paramArrayOfThreadData, BinaryImageData[] paramArrayOfBinaryImageData, CustomAttributeData[] paramArrayOfCustomAttributeData, DeviceData paramDeviceData)
  {
    timestamp = paramLong;
    signal = paramSignalData;
    threads = paramArrayOfThreadData;
    binaryImages = paramArrayOfBinaryImageData;
    customAttributes = paramArrayOfCustomAttributeData;
    deviceData = paramDeviceData;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/internal/models/SessionEventData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */