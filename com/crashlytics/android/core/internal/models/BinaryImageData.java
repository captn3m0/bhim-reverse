package com.crashlytics.android.core.internal.models;

public class BinaryImageData
{
  public final long baseAddress;
  public final String id;
  public final String path;
  public final long size;
  
  public BinaryImageData(long paramLong1, long paramLong2, String paramString1, String paramString2)
  {
    baseAddress = paramLong1;
    size = paramLong2;
    path = paramString1;
    id = paramString2;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/internal/models/BinaryImageData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */