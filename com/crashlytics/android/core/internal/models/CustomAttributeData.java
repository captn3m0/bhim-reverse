package com.crashlytics.android.core.internal.models;

public class CustomAttributeData
{
  public final String key;
  public final String value;
  
  public CustomAttributeData(String paramString1, String paramString2)
  {
    key = paramString1;
    value = paramString2;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/internal/models/CustomAttributeData.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */