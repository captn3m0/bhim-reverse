package com.crashlytics.android.core;

abstract class NativeCrashWriter$ProtobufMessage
{
  private final ProtobufMessage[] children;
  private final int tag;
  
  public NativeCrashWriter$ProtobufMessage(int paramInt, ProtobufMessage... paramVarArgs)
  {
    tag = paramInt;
    if (paramVarArgs != null) {}
    for (;;)
    {
      children = paramVarArgs;
      return;
      paramVarArgs = NativeCrashWriter.access$000();
    }
  }
  
  public int getPropertiesSize()
  {
    return 0;
  }
  
  public int getSize()
  {
    int i = getSizeNoTag();
    int j = CodedOutputStream.computeRawVarint32Size(i);
    i += j;
    j = CodedOutputStream.computeTagSize(tag);
    return i + j;
  }
  
  public int getSizeNoTag()
  {
    int i = getPropertiesSize();
    ProtobufMessage[] arrayOfProtobufMessage = children;
    int j = arrayOfProtobufMessage.length;
    int k = 0;
    while (k < j)
    {
      ProtobufMessage localProtobufMessage = arrayOfProtobufMessage[k];
      int m = localProtobufMessage.getSize();
      i += m;
      k += 1;
    }
    return i;
  }
  
  public void write(CodedOutputStream paramCodedOutputStream)
  {
    int i = tag;
    int j = 2;
    paramCodedOutputStream.writeTag(i, j);
    i = getSizeNoTag();
    paramCodedOutputStream.writeRawVarint32(i);
    writeProperties(paramCodedOutputStream);
    ProtobufMessage[] arrayOfProtobufMessage = children;
    int k = arrayOfProtobufMessage.length;
    i = 0;
    while (i < k)
    {
      ProtobufMessage localProtobufMessage = arrayOfProtobufMessage[i];
      localProtobufMessage.write(paramCodedOutputStream);
      i += 1;
    }
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream) {}
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$ProtobufMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */