package com.crashlytics.android.core;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.internal.CrashEventDataProvider;
import com.crashlytics.android.core.internal.models.SessionEventData;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.j.a;
import io.fabric.sdk.android.services.b.j.b;
import io.fabric.sdk.android.services.concurrency.UnmetDependencyException;
import io.fabric.sdk.android.services.concurrency.l;
import io.fabric.sdk.android.services.d.a;
import io.fabric.sdk.android.services.e.e;
import io.fabric.sdk.android.services.e.m;
import io.fabric.sdk.android.services.e.p;
import io.fabric.sdk.android.services.e.q;
import io.fabric.sdk.android.services.e.t;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.f;
import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.net.ssl.HttpsURLConnection;

public class CrashlyticsCore
  extends h
{
  static final float CLS_DEFAULT_PROCESS_DELAY = 1.0F;
  static final String COLLECT_CUSTOM_KEYS = "com.crashlytics.CollectCustomKeys";
  static final String COLLECT_CUSTOM_LOGS = "com.crashlytics.CollectCustomLogs";
  static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
  static final String CRASHLYTICS_REQUIRE_BUILD_ID = "com.crashlytics.RequireBuildId";
  static final boolean CRASHLYTICS_REQUIRE_BUILD_ID_DEFAULT = true;
  static final String CRASH_MARKER_FILE_NAME = "crash_marker";
  static final int DEFAULT_MAIN_HANDLER_TIMEOUT_SEC = 4;
  private static final String INITIALIZATION_MARKER_FILE_NAME = "initialization_marker";
  static final int MAX_ATTRIBUTES = 64;
  static final int MAX_ATTRIBUTE_SIZE = 1024;
  private static final String MISSING_BUILD_ID_MSG = "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.";
  private static final String PREF_ALWAYS_SEND_REPORTS_KEY = "always_send_reports_opt_in";
  private static final boolean SHOULD_PROMPT_BEFORE_SENDING_REPORTS_DEFAULT = false;
  public static final String TAG = "CrashlyticsCore";
  private String apiKey;
  private final ConcurrentHashMap attributes;
  private String buildId;
  private CrashlyticsFileMarker crashMarker;
  private float delay;
  private boolean disabled;
  private CrashlyticsExecutorServiceWrapper executorServiceWrapper;
  private CrashEventDataProvider externalCrashEventDataProvider;
  private a fileStore;
  private CrashlyticsUncaughtExceptionHandler handler;
  private io.fabric.sdk.android.services.network.d httpRequestFactory;
  private CrashlyticsFileMarker initializationMarker;
  private String installerPackageName;
  private CrashlyticsListener listener;
  private String packageName;
  private final PinningInfoProvider pinningInfo;
  private File sdkDir;
  private final long startTime;
  private String userEmail = null;
  private String userId = null;
  private String userName = null;
  private String versionCode;
  private String versionName;
  
  public CrashlyticsCore()
  {
    this(1.0F, null, null, false);
  }
  
  CrashlyticsCore(float paramFloat, CrashlyticsListener paramCrashlyticsListener, PinningInfoProvider paramPinningInfoProvider, boolean paramBoolean)
  {
    this(paramFloat, paramCrashlyticsListener, paramPinningInfoProvider, paramBoolean, localExecutorService);
  }
  
  CrashlyticsCore(float paramFloat, CrashlyticsListener paramCrashlyticsListener, PinningInfoProvider paramPinningInfoProvider, boolean paramBoolean, ExecutorService paramExecutorService)
  {
    delay = paramFloat;
    if (paramCrashlyticsListener != null) {}
    for (;;)
    {
      listener = paramCrashlyticsListener;
      pinningInfo = paramPinningInfoProvider;
      disabled = paramBoolean;
      localObject = new com/crashlytics/android/core/CrashlyticsExecutorServiceWrapper;
      ((CrashlyticsExecutorServiceWrapper)localObject).<init>(paramExecutorService);
      executorServiceWrapper = ((CrashlyticsExecutorServiceWrapper)localObject);
      localObject = new java/util/concurrent/ConcurrentHashMap;
      ((ConcurrentHashMap)localObject).<init>();
      attributes = ((ConcurrentHashMap)localObject);
      long l = System.currentTimeMillis();
      startTime = l;
      return;
      paramCrashlyticsListener = new com/crashlytics/android/core/CrashlyticsCore$NoOpListener;
      paramCrashlyticsListener.<init>(null);
    }
  }
  
  private void checkForPreviousCrash()
  {
    Object localObject1 = executorServiceWrapper;
    Object localObject2 = new com/crashlytics/android/core/CrashlyticsCore$CrashMarkerCheck;
    Object localObject3 = crashMarker;
    ((CrashlyticsCore.CrashMarkerCheck)localObject2).<init>((CrashlyticsFileMarker)localObject3);
    localObject1 = (Boolean)((CrashlyticsExecutorServiceWrapper)localObject1).executeSyncLoggingException((Callable)localObject2);
    localObject2 = Boolean.TRUE;
    boolean bool = ((Boolean)localObject2).equals(localObject1);
    if (!bool) {}
    for (;;)
    {
      return;
      try
      {
        localObject1 = listener;
        ((CrashlyticsListener)localObject1).crashlyticsDidDetectCrashDuringPreviousExecution();
      }
      catch (Exception localException)
      {
        localObject2 = io.fabric.sdk.android.c.h();
        localObject3 = "CrashlyticsCore";
        String str = "Exception thrown by CrashlyticsListener while notifying of previous crash.";
        ((k)localObject2).e((String)localObject3, str, localException);
      }
    }
  }
  
  private void doLog(int paramInt, String paramString1, String paramString2)
  {
    boolean bool = disabled;
    if (bool) {}
    for (;;)
    {
      return;
      String str1 = "prior to logging messages.";
      bool = ensureFabricWithCalled(str1);
      if (bool)
      {
        long l1 = System.currentTimeMillis();
        long l2 = startTime;
        l1 -= l2;
        CrashlyticsUncaughtExceptionHandler localCrashlyticsUncaughtExceptionHandler = handler;
        String str2 = formatLogMessage(paramInt, paramString1, paramString2);
        localCrashlyticsUncaughtExceptionHandler.writeToLog(l1, str2);
      }
    }
  }
  
  private static boolean ensureFabricWithCalled(String paramString)
  {
    Object localObject1 = getInstance();
    boolean bool;
    if (localObject1 != null)
    {
      localObject1 = handler;
      if (localObject1 != null) {}
    }
    else
    {
      localObject1 = io.fabric.sdk.android.c.h();
      String str = "CrashlyticsCore";
      Object localObject2 = new java/lang/StringBuilder;
      ((StringBuilder)localObject2).<init>();
      localObject2 = "Crashlytics must be initialized by calling Fabric.with(Context) " + paramString;
      ((k)localObject1).e(str, (String)localObject2, null);
      bool = false;
      localObject1 = null;
    }
    for (;;)
    {
      return bool;
      bool = true;
    }
  }
  
  private void finishInitSynchronously()
  {
    Object localObject1 = new com/crashlytics/android/core/CrashlyticsCore$1;
    ((CrashlyticsCore.1)localObject1).<init>(this);
    Object localObject2 = getDependencies();
    Object localObject3 = ((Collection)localObject2).iterator();
    for (;;)
    {
      boolean bool = ((Iterator)localObject3).hasNext();
      if (!bool) {
        break;
      }
      localObject2 = (l)((Iterator)localObject3).next();
      ((io.fabric.sdk.android.services.concurrency.g)localObject1).addDependency((l)localObject2);
    }
    localObject2 = getFabric().f().submit((Callable)localObject1);
    localObject1 = io.fabric.sdk.android.c.h();
    localObject3 = "CrashlyticsCore";
    String str = "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously.";
    ((k)localObject1).a((String)localObject3, str);
    long l = 4;
    try
    {
      localObject1 = TimeUnit.SECONDS;
      ((Future)localObject2).get(l, (TimeUnit)localObject1);
      return;
    }
    catch (InterruptedException localInterruptedException)
    {
      for (;;)
      {
        localObject1 = io.fabric.sdk.android.c.h();
        localObject3 = "CrashlyticsCore";
        str = "Crashlytics was interrupted during initialization.";
        ((k)localObject1).e((String)localObject3, str, localInterruptedException);
      }
    }
    catch (ExecutionException localExecutionException)
    {
      for (;;)
      {
        localObject1 = io.fabric.sdk.android.c.h();
        localObject3 = "CrashlyticsCore";
        str = "Problem encountered during Crashlytics initialization.";
        ((k)localObject1).e((String)localObject3, str, localExecutionException);
      }
    }
    catch (TimeoutException localTimeoutException)
    {
      for (;;)
      {
        localObject1 = io.fabric.sdk.android.c.h();
        localObject3 = "CrashlyticsCore";
        str = "Crashlytics timed out during initialization.";
        ((k)localObject1).e((String)localObject3, str, localTimeoutException);
      }
    }
  }
  
  private static String formatLogMessage(int paramInt, String paramString1, String paramString2)
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    String str = i.b(paramInt);
    return str + "/" + paramString1 + " " + paramString2;
  }
  
  public static CrashlyticsCore getInstance()
  {
    return (CrashlyticsCore)io.fabric.sdk.android.c.a(CrashlyticsCore.class);
  }
  
  private boolean getSendDecisionFromUser(Activity paramActivity, io.fabric.sdk.android.services.e.o paramo)
  {
    Object localObject = new com/crashlytics/android/core/CrashlyticsCore$7;
    ((CrashlyticsCore.7)localObject).<init>(this);
    localObject = CrashPromptDialog.create(paramActivity, paramo, (CrashPromptDialog.AlwaysSendCallback)localObject);
    CrashlyticsCore.8 local8 = new com/crashlytics/android/core/CrashlyticsCore$8;
    local8.<init>(this, (CrashPromptDialog)localObject);
    paramActivity.runOnUiThread(local8);
    io.fabric.sdk.android.c.h().a("CrashlyticsCore", "Waiting for user opt-in.");
    ((CrashPromptDialog)localObject).await();
    return ((CrashPromptDialog)localObject).getOptIn();
  }
  
  static p getSessionSettingsData()
  {
    Object localObject = q.a().b();
    if (localObject == null) {}
    for (localObject = null;; localObject = b) {
      return (p)localObject;
    }
  }
  
  private boolean installExceptionHandler(UnityVersionProvider paramUnityVersionProvider)
  {
    try
    {
      Object localObject1 = io.fabric.sdk.android.c.h();
      localObject3 = "CrashlyticsCore";
      localObject4 = "Installing exception handler...";
      ((k)localObject1).a((String)localObject3, (String)localObject4);
      localObject1 = new com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler;
      localObject3 = Thread.getDefaultUncaughtExceptionHandler();
      localObject4 = executorServiceWrapper;
      localObject5 = getIdManager();
      a locala = fileStore;
      ((CrashlyticsUncaughtExceptionHandler)localObject1).<init>((Thread.UncaughtExceptionHandler)localObject3, (CrashlyticsExecutorServiceWrapper)localObject4, (io.fabric.sdk.android.services.b.o)localObject5, paramUnityVersionProvider, locala, this);
      handler = ((CrashlyticsUncaughtExceptionHandler)localObject1);
      localObject1 = handler;
      ((CrashlyticsUncaughtExceptionHandler)localObject1).openSession();
      localObject1 = handler;
      Thread.setDefaultUncaughtExceptionHandler((Thread.UncaughtExceptionHandler)localObject1);
      localObject1 = io.fabric.sdk.android.c.h();
      localObject3 = "CrashlyticsCore";
      localObject4 = "Successfully installed exception handler.";
      ((k)localObject1).a((String)localObject3, (String)localObject4);
      bool = true;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject3 = io.fabric.sdk.android.c.h();
        Object localObject4 = "CrashlyticsCore";
        Object localObject5 = "There was a problem installing the exception handler.";
        ((k)localObject3).e((String)localObject4, (String)localObject5, localException);
        handler = null;
        boolean bool = false;
        Object localObject2 = null;
      }
    }
    return bool;
  }
  
  static boolean isBuildIdValid(String paramString, boolean paramBoolean)
  {
    boolean bool1 = true;
    Object localObject;
    if (!paramBoolean)
    {
      localObject = io.fabric.sdk.android.c.h();
      String str1 = "CrashlyticsCore";
      String str2 = "Configured not to require a build ID.";
      ((k)localObject).a(str1, str2);
    }
    for (;;)
    {
      return bool1;
      boolean bool2 = i.c(paramString);
      if (bool2)
      {
        Log.e("CrashlyticsCore", ".");
        Log.e("CrashlyticsCore", ".     |  | ");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".   \\ |  | /");
        Log.e("CrashlyticsCore", ".    \\    /");
        Log.e("CrashlyticsCore", ".     \\  /");
        Log.e("CrashlyticsCore", ".      \\/");
        Log.e("CrashlyticsCore", ".");
        Log.e("CrashlyticsCore", "This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.");
        Log.e("CrashlyticsCore", ".");
        Log.e("CrashlyticsCore", ".      /\\");
        Log.e("CrashlyticsCore", ".     /  \\");
        Log.e("CrashlyticsCore", ".    /    \\");
        Log.e("CrashlyticsCore", ".   / |  | \\");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".     |  |");
        Log.e("CrashlyticsCore", ".     |  |");
        localObject = ".";
        Log.e("CrashlyticsCore", (String)localObject);
        bool1 = false;
      }
    }
  }
  
  static void recordFatalExceptionEvent(String paramString1, String paramString2)
  {
    Answers localAnswers = (Answers)io.fabric.sdk.android.c.a(Answers.class);
    if (localAnswers != null)
    {
      j.a locala = new io/fabric/sdk/android/services/b/j$a;
      locala.<init>(paramString1, paramString2);
      localAnswers.onException(locala);
    }
  }
  
  static void recordLoggedExceptionEvent(String paramString1, String paramString2)
  {
    Answers localAnswers = (Answers)io.fabric.sdk.android.c.a(Answers.class);
    if (localAnswers != null)
    {
      j.b localb = new io/fabric/sdk/android/services/b/j$b;
      localb.<init>(paramString1, paramString2);
      localAnswers.onException(localb);
    }
  }
  
  private static String sanitizeAttribute(String paramString)
  {
    int i = 1024;
    if (paramString != null)
    {
      paramString = paramString.trim();
      int j = paramString.length();
      if (j > i)
      {
        j = 0;
        paramString = paramString.substring(0, i);
      }
    }
    return paramString;
  }
  
  private void setAndValidateKitProperties(Context paramContext)
  {
    Object localObject1 = pinningInfo;
    if (localObject1 != null)
    {
      localObject1 = new com/crashlytics/android/core/CrashlyticsPinningInfoProvider;
      Object localObject2 = pinningInfo;
      ((CrashlyticsPinningInfoProvider)localObject1).<init>((PinningInfoProvider)localObject2);
      localObject2 = new io/fabric/sdk/android/services/network/b;
      Object localObject3 = io.fabric.sdk.android.c.h();
      ((io.fabric.sdk.android.services.network.b)localObject2).<init>((k)localObject3);
      httpRequestFactory = ((io.fabric.sdk.android.services.network.d)localObject2);
      httpRequestFactory.a((f)localObject1);
      localObject1 = paramContext.getPackageName();
      packageName = ((String)localObject1);
      localObject1 = getIdManager().j();
      installerPackageName = ((String)localObject1);
      localObject1 = io.fabric.sdk.android.c.h();
      localObject3 = new java/lang/StringBuilder;
      ((StringBuilder)localObject3).<init>();
      localObject3 = ((StringBuilder)localObject3).append("Installer package name is: ");
      String str = installerPackageName;
      localObject3 = str;
      ((k)localObject1).a("CrashlyticsCore", (String)localObject3);
      localObject1 = paramContext.getPackageManager();
      localObject2 = packageName;
      localObject3 = null;
      localObject1 = ((PackageManager)localObject1).getPackageInfo((String)localObject2, 0);
      int i = versionCode;
      localObject2 = Integer.toString(i);
      versionCode = ((String)localObject2);
      localObject2 = versionName;
      if (localObject2 != null) {
        break label189;
      }
    }
    label189:
    for (localObject1 = "0.0";; localObject1 = versionName)
    {
      versionName = ((String)localObject1);
      return;
      localObject1 = null;
      break;
    }
  }
  
  boolean canSendWithUserApproval()
  {
    q localq = q.a();
    CrashlyticsCore.6 local6 = new com/crashlytics/android/core/CrashlyticsCore$6;
    local6.<init>(this);
    Boolean localBoolean = Boolean.valueOf(true);
    return ((Boolean)localq.a(local6, localBoolean)).booleanValue();
  }
  
  public void crash()
  {
    CrashTest localCrashTest = new com/crashlytics/android/core/CrashTest;
    localCrashTest.<init>();
    localCrashTest.indexOutOfBounds();
  }
  
  void createCrashMarker()
  {
    crashMarker.create();
  }
  
  boolean didPreviousInitializationFail()
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsCore.4 local4 = new com/crashlytics/android/core/CrashlyticsCore$4;
    local4.<init>(this);
    return ((Boolean)localCrashlyticsExecutorServiceWrapper.executeSyncLoggingException(local4)).booleanValue();
  }
  
  protected Void doInBackground()
  {
    markInitializationStarted();
    Object localObject1 = getExternalCrashEventData();
    if (localObject1 != null)
    {
      localObject3 = handler;
      ((CrashlyticsUncaughtExceptionHandler)localObject3).writeExternalCrashEvent((SessionEventData)localObject1);
    }
    localObject1 = handler;
    ((CrashlyticsUncaughtExceptionHandler)localObject1).cleanInvalidTempFiles();
    for (;;)
    {
      try
      {
        localObject1 = q.a();
        localObject1 = ((q)localObject1).b();
        if (localObject1 != null) {
          continue;
        }
        localObject1 = io.fabric.sdk.android.c.h();
        localObject3 = "CrashlyticsCore";
        str1 = "Received null settings, skipping initialization!";
        ((k)localObject1).d((String)localObject3, str1);
      }
      catch (Exception localException)
      {
        boolean bool;
        float f;
        localObject3 = io.fabric.sdk.android.c.h();
        String str1 = "CrashlyticsCore";
        String str2 = "Crashlytics encountered a problem during asynchronous initialization.";
        ((k)localObject3).e(str1, str2, localException);
        markInitializationComplete();
        continue;
      }
      finally
      {
        markInitializationComplete();
      }
      return null;
      localObject3 = d;
      bool = c;
      if (!bool)
      {
        localObject1 = io.fabric.sdk.android.c.h();
        localObject3 = "CrashlyticsCore";
        str1 = "Collection of crash reports disabled in Crashlytics settings.";
        ((k)localObject1).a((String)localObject3, str1);
        markInitializationComplete();
      }
      else
      {
        localObject3 = handler;
        ((CrashlyticsUncaughtExceptionHandler)localObject3).finalizeSessions();
        localObject1 = getCreateReportSpiCall((t)localObject1);
        if (localObject1 == null)
        {
          localObject1 = io.fabric.sdk.android.c.h();
          localObject3 = "CrashlyticsCore";
          str1 = "Unable to create a call to upload reports.";
          ((k)localObject1).d((String)localObject3, str1);
          markInitializationComplete();
        }
        else
        {
          localObject3 = new com/crashlytics/android/core/ReportUploader;
          str1 = apiKey;
          ((ReportUploader)localObject3).<init>(str1, (CreateReportSpiCall)localObject1);
          f = delay;
          ((ReportUploader)localObject3).uploadReports(f);
          markInitializationComplete();
        }
      }
    }
  }
  
  String getApiKey()
  {
    return apiKey;
  }
  
  Map getAttributes()
  {
    return Collections.unmodifiableMap(attributes);
  }
  
  String getBuildId()
  {
    return buildId;
  }
  
  CreateReportSpiCall getCreateReportSpiCall(t paramt)
  {
    DefaultCreateReportSpiCall localDefaultCreateReportSpiCall;
    if (paramt != null)
    {
      localDefaultCreateReportSpiCall = new com/crashlytics/android/core/DefaultCreateReportSpiCall;
      String str1 = getOverridenSpiEndpoint();
      String str2 = a.d;
      io.fabric.sdk.android.services.network.d locald = httpRequestFactory;
      localDefaultCreateReportSpiCall.<init>(this, str1, str2, locald);
    }
    for (;;)
    {
      return localDefaultCreateReportSpiCall;
      localDefaultCreateReportSpiCall = null;
    }
  }
  
  SessionEventData getExternalCrashEventData()
  {
    SessionEventData localSessionEventData = null;
    CrashEventDataProvider localCrashEventDataProvider = externalCrashEventDataProvider;
    if (localCrashEventDataProvider != null) {
      localSessionEventData = externalCrashEventDataProvider.getCrashEventData();
    }
    return localSessionEventData;
  }
  
  CrashlyticsUncaughtExceptionHandler getHandler()
  {
    return handler;
  }
  
  public String getIdentifier()
  {
    return "com.crashlytics.sdk.android.crashlytics-core";
  }
  
  String getInstallerPackageName()
  {
    return installerPackageName;
  }
  
  String getOverridenSpiEndpoint()
  {
    return i.b(getContext(), "com.crashlytics.ApiEndpoint");
  }
  
  String getPackageName()
  {
    return packageName;
  }
  
  public PinningInfoProvider getPinningInfoProvider()
  {
    boolean bool = disabled;
    if (!bool) {}
    for (PinningInfoProvider localPinningInfoProvider = pinningInfo;; localPinningInfoProvider = null)
    {
      return localPinningInfoProvider;
      bool = false;
    }
  }
  
  File getSdkDirectory()
  {
    Object localObject = sdkDir;
    if (localObject == null)
    {
      localObject = new io/fabric/sdk/android/services/d/b;
      ((io.fabric.sdk.android.services.d.b)localObject).<init>(this);
      localObject = ((io.fabric.sdk.android.services.d.b)localObject).a();
      sdkDir = ((File)localObject);
    }
    return sdkDir;
  }
  
  String getUserEmail()
  {
    Object localObject = getIdManager();
    boolean bool = ((io.fabric.sdk.android.services.b.o)localObject).a();
    if (bool) {}
    for (localObject = userEmail;; localObject = null)
    {
      return (String)localObject;
      bool = false;
    }
  }
  
  String getUserIdentifier()
  {
    Object localObject = getIdManager();
    boolean bool = ((io.fabric.sdk.android.services.b.o)localObject).a();
    if (bool) {}
    for (localObject = userId;; localObject = null)
    {
      return (String)localObject;
      bool = false;
    }
  }
  
  String getUserName()
  {
    Object localObject = getIdManager();
    boolean bool = ((io.fabric.sdk.android.services.b.o)localObject).a();
    if (bool) {}
    for (localObject = userName;; localObject = null)
    {
      return (String)localObject;
      bool = false;
    }
  }
  
  public String getVersion()
  {
    return "2.3.14.151";
  }
  
  String getVersionCode()
  {
    return versionCode;
  }
  
  String getVersionName()
  {
    return versionName;
  }
  
  boolean internalVerifyPinning(URL paramURL)
  {
    Object localObject1 = getPinningInfoProvider();
    boolean bool;
    if (localObject1 != null)
    {
      localObject1 = httpRequestFactory;
      Object localObject2 = io.fabric.sdk.android.services.network.c.a;
      String str = paramURL.toString();
      localObject2 = ((io.fabric.sdk.android.services.network.d)localObject1).a((io.fabric.sdk.android.services.network.c)localObject2, str);
      localObject1 = (HttpsURLConnection)((HttpRequest)localObject2).a();
      ((HttpsURLConnection)localObject1).setInstanceFollowRedirects(false);
      ((HttpRequest)localObject2).b();
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localObject1 = null;
    }
  }
  
  public void log(int paramInt, String paramString1, String paramString2)
  {
    doLog(paramInt, paramString1, paramString2);
    k localk = io.fabric.sdk.android.c.h();
    Object localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject1 = "" + paramString1;
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = "" + paramString2;
    localk.a(paramInt, (String)localObject1, (String)localObject2, true);
  }
  
  public void log(String paramString)
  {
    doLog(3, "CrashlyticsCore", paramString);
  }
  
  public void logException(Throwable paramThrowable)
  {
    boolean bool = disabled;
    if (bool) {}
    for (;;)
    {
      return;
      Object localObject = "prior to logging exceptions.";
      bool = ensureFabricWithCalled((String)localObject);
      if (bool) {
        if (paramThrowable == null)
        {
          localObject = io.fabric.sdk.android.c.h();
          int i = 5;
          String str1 = "CrashlyticsCore";
          String str2 = "Crashlytics is ignoring a request to log a null exception.";
          ((k)localObject).a(i, str1, str2);
        }
        else
        {
          localObject = handler;
          Thread localThread = Thread.currentThread();
          ((CrashlyticsUncaughtExceptionHandler)localObject).writeNonFatalException(localThread, paramThrowable);
        }
      }
    }
  }
  
  void markInitializationComplete()
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsCore.3 local3 = new com/crashlytics/android/core/CrashlyticsCore$3;
    local3.<init>(this);
    localCrashlyticsExecutorServiceWrapper.executeAsync(local3);
  }
  
  void markInitializationStarted()
  {
    CrashlyticsExecutorServiceWrapper localCrashlyticsExecutorServiceWrapper = executorServiceWrapper;
    CrashlyticsCore.2 local2 = new com/crashlytics/android/core/CrashlyticsCore$2;
    local2.<init>(this);
    localCrashlyticsExecutorServiceWrapper.executeSyncLoggingException(local2);
  }
  
  protected boolean onPreExecute()
  {
    Context localContext = super.getContext();
    return onPreExecute(localContext);
  }
  
  boolean onPreExecute(Context paramContext)
  {
    boolean bool1 = true;
    boolean bool2 = false;
    UnmetDependencyException localUnmetDependencyException = null;
    boolean bool3 = disabled;
    if (bool3) {}
    for (;;)
    {
      return bool2;
      Object localObject1 = new io/fabric/sdk/android/services/b/g;
      ((io.fabric.sdk.android.services.b.g)localObject1).<init>();
      localObject1 = ((io.fabric.sdk.android.services.b.g)localObject1).a(paramContext);
      apiKey = ((String)localObject1);
      localObject1 = apiKey;
      if (localObject1 != null)
      {
        localObject1 = i.m(paramContext);
        buildId = ((String)localObject1);
        localObject1 = "com.crashlytics.RequireBuildId";
        bool3 = i.a(paramContext, (String)localObject1, bool1);
        String str1 = buildId;
        bool3 = isBuildIdValid(str1, bool3);
        if (!bool3)
        {
          localUnmetDependencyException = new io/fabric/sdk/android/services/concurrency/UnmetDependencyException;
          localUnmetDependencyException.<init>("This app relies on Crashlytics. Please sign up for access at https://fabric.io/sign_up,\ninstall an Android build tool and ask a team member to invite you to this app's organization.");
          throw localUnmetDependencyException;
        }
        localObject1 = io.fabric.sdk.android.c.h();
        Object localObject2 = new java/lang/StringBuilder;
        ((StringBuilder)localObject2).<init>();
        localObject2 = ((StringBuilder)localObject2).append("Initializing Crashlytics ");
        String str2 = getVersion();
        localObject2 = str2;
        ((k)localObject1).c("CrashlyticsCore", (String)localObject2);
        localObject1 = new io/fabric/sdk/android/services/d/b;
        ((io.fabric.sdk.android.services.d.b)localObject1).<init>(this);
        fileStore = ((a)localObject1);
        localObject1 = new com/crashlytics/android/core/CrashlyticsFileMarker;
        localObject2 = fileStore;
        ((CrashlyticsFileMarker)localObject1).<init>("crash_marker", (a)localObject2);
        crashMarker = ((CrashlyticsFileMarker)localObject1);
        localObject1 = new com/crashlytics/android/core/CrashlyticsFileMarker;
        str1 = "initialization_marker";
        localObject2 = fileStore;
        ((CrashlyticsFileMarker)localObject1).<init>(str1, (a)localObject2);
        initializationMarker = ((CrashlyticsFileMarker)localObject1);
        try
        {
          setAndValidateKitProperties(paramContext);
          localObject1 = new com/crashlytics/android/core/ManifestUnityVersionProvider;
          str1 = getPackageName();
          ((ManifestUnityVersionProvider)localObject1).<init>(paramContext, str1);
          boolean bool4 = didPreviousInitializationFail();
          checkForPreviousCrash();
          bool3 = installExceptionHandler((UnityVersionProvider)localObject1);
          if (!bool3) {
            continue;
          }
          if (!bool4) {
            break label344;
          }
          bool3 = i.n(paramContext);
          if (!bool3) {
            break label344;
          }
          finishInitSynchronously();
        }
        catch (Exception localException)
        {
          localObject1 = io.fabric.sdk.android.c.h();
          str1 = "CrashlyticsCore";
          localObject2 = "Crashlytics was not started due to an exception during initialization";
          ((k)localObject1).e(str1, (String)localObject2, localException);
        }
        continue;
        label344:
        bool2 = bool1;
      }
    }
  }
  
  public void setBool(String paramString, boolean paramBoolean)
  {
    String str = Boolean.toString(paramBoolean);
    setString(paramString, str);
  }
  
  public void setDouble(String paramString, double paramDouble)
  {
    String str = Double.toString(paramDouble);
    setString(paramString, str);
  }
  
  void setExternalCrashEventDataProvider(CrashEventDataProvider paramCrashEventDataProvider)
  {
    externalCrashEventDataProvider = paramCrashEventDataProvider;
  }
  
  public void setFloat(String paramString, float paramFloat)
  {
    String str = Float.toString(paramFloat);
    setString(paramString, str);
  }
  
  public void setInt(String paramString, int paramInt)
  {
    String str = Integer.toString(paramInt);
    setString(paramString, str);
  }
  
  public void setListener(CrashlyticsListener paramCrashlyticsListener)
  {
    try
    {
      Object localObject1 = io.fabric.sdk.android.c.h();
      String str1 = "CrashlyticsCore";
      String str2 = "Use of setListener is deprecated.";
      ((k)localObject1).d(str1, str2);
      if (paramCrashlyticsListener == null)
      {
        localObject1 = new java/lang/IllegalArgumentException;
        str1 = "listener must not be null.";
        ((IllegalArgumentException)localObject1).<init>(str1);
        throw ((Throwable)localObject1);
      }
    }
    finally {}
    listener = paramCrashlyticsListener;
  }
  
  public void setLong(String paramString, long paramLong)
  {
    String str = Long.toString(paramLong);
    setString(paramString, str);
  }
  
  void setShouldSendUserReportsWithoutPrompting(boolean paramBoolean)
  {
    io.fabric.sdk.android.services.d.d locald = new io/fabric/sdk/android/services/d/d;
    locald.<init>(this);
    SharedPreferences.Editor localEditor = locald.b().putBoolean("always_send_reports_opt_in", paramBoolean);
    locald.a(localEditor);
  }
  
  public void setString(String paramString1, String paramString2)
  {
    boolean bool1 = disabled;
    if (bool1) {}
    Object localObject2;
    Object localObject3;
    for (;;)
    {
      return;
      localObject1 = "prior to setting keys.";
      bool1 = ensureFabricWithCalled((String)localObject1);
      if (bool1) {
        if (paramString1 == null)
        {
          localObject1 = getContext();
          if (localObject1 != null)
          {
            bool1 = i.i((Context)localObject1);
            if (bool1)
            {
              localObject1 = new java/lang/IllegalArgumentException;
              ((IllegalArgumentException)localObject1).<init>("Custom attribute key must not be null.");
              throw ((Throwable)localObject1);
            }
          }
          localObject1 = io.fabric.sdk.android.c.h();
          localObject2 = "CrashlyticsCore";
          localObject3 = "Attempting to set custom attribute with null key, ignoring.";
          ((k)localObject1).e((String)localObject2, (String)localObject3, null);
        }
        else
        {
          localObject2 = sanitizeAttribute(paramString1);
          localObject1 = attributes;
          int i = ((ConcurrentHashMap)localObject1).size();
          int j = 64;
          if (i < j) {
            break;
          }
          localObject1 = attributes;
          boolean bool2 = ((ConcurrentHashMap)localObject1).containsKey(localObject2);
          if (bool2) {
            break;
          }
          localObject1 = io.fabric.sdk.android.c.h();
          localObject2 = "CrashlyticsCore";
          localObject3 = "Exceeded maximum number of custom attributes (64)";
          ((k)localObject1).a((String)localObject2, (String)localObject3);
        }
      }
    }
    if (paramString2 == null) {}
    for (Object localObject1 = "";; localObject1 = sanitizeAttribute(paramString2))
    {
      localObject3 = attributes;
      ((ConcurrentHashMap)localObject3).put(localObject2, localObject1);
      localObject1 = handler;
      localObject2 = attributes;
      ((CrashlyticsUncaughtExceptionHandler)localObject1).cacheKeyData((Map)localObject2);
      break;
    }
  }
  
  public void setUserEmail(String paramString)
  {
    boolean bool = disabled;
    if (bool) {}
    for (;;)
    {
      return;
      Object localObject = "prior to setting user data.";
      bool = ensureFabricWithCalled((String)localObject);
      if (bool)
      {
        localObject = sanitizeAttribute(paramString);
        userEmail = ((String)localObject);
        localObject = handler;
        String str1 = userId;
        String str2 = userName;
        String str3 = userEmail;
        ((CrashlyticsUncaughtExceptionHandler)localObject).cacheUserData(str1, str2, str3);
      }
    }
  }
  
  public void setUserIdentifier(String paramString)
  {
    boolean bool = disabled;
    if (bool) {}
    for (;;)
    {
      return;
      Object localObject = "prior to setting user data.";
      bool = ensureFabricWithCalled((String)localObject);
      if (bool)
      {
        localObject = sanitizeAttribute(paramString);
        userId = ((String)localObject);
        localObject = handler;
        String str1 = userId;
        String str2 = userName;
        String str3 = userEmail;
        ((CrashlyticsUncaughtExceptionHandler)localObject).cacheUserData(str1, str2, str3);
      }
    }
  }
  
  public void setUserName(String paramString)
  {
    boolean bool = disabled;
    if (bool) {}
    for (;;)
    {
      return;
      Object localObject = "prior to setting user data.";
      bool = ensureFabricWithCalled((String)localObject);
      if (bool)
      {
        localObject = sanitizeAttribute(paramString);
        userName = ((String)localObject);
        localObject = handler;
        String str1 = userId;
        String str2 = userName;
        String str3 = userEmail;
        ((CrashlyticsUncaughtExceptionHandler)localObject).cacheUserData(str1, str2, str3);
      }
    }
  }
  
  boolean shouldPromptUserBeforeSendingCrashReports()
  {
    q localq = q.a();
    CrashlyticsCore.5 local5 = new com/crashlytics/android/core/CrashlyticsCore$5;
    local5.<init>(this);
    Boolean localBoolean = Boolean.valueOf(false);
    return ((Boolean)localq.a(local5, localBoolean)).booleanValue();
  }
  
  boolean shouldSendReportsWithoutPrompting()
  {
    io.fabric.sdk.android.services.d.d locald = new io/fabric/sdk/android/services/d/d;
    locald.<init>(this);
    return locald.a().getBoolean("always_send_reports_opt_in", false);
  }
  
  public boolean verifyPinning(URL paramURL)
  {
    try
    {
      bool = internalVerifyPinning(paramURL);
    }
    catch (Exception localException)
    {
      for (;;)
      {
        k localk = io.fabric.sdk.android.c.h();
        String str1 = "CrashlyticsCore";
        String str2 = "Could not verify SSL pinning";
        localk.e(str1, str2, localException);
        boolean bool = false;
        Object localObject = null;
      }
    }
    return bool;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsCore.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */