package com.crashlytics.android.core;

import android.app.Activity;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.services.e.o;
import io.fabric.sdk.android.services.e.q.b;
import io.fabric.sdk.android.services.e.t;

class CrashlyticsCore$6
  implements q.b
{
  CrashlyticsCore$6(CrashlyticsCore paramCrashlyticsCore) {}
  
  public Boolean usingSettings(t paramt)
  {
    boolean bool1 = true;
    Activity localActivity = this$0.getFabric().b();
    if (localActivity != null)
    {
      boolean bool2 = localActivity.isFinishing();
      if (!bool2)
      {
        Object localObject = this$0;
        bool2 = ((CrashlyticsCore)localObject).shouldPromptUserBeforeSendingCrashReports();
        if (bool2)
        {
          CrashlyticsCore localCrashlyticsCore = this$0;
          localObject = c;
          bool1 = CrashlyticsCore.access$200(localCrashlyticsCore, localActivity, (o)localObject);
        }
      }
    }
    return Boolean.valueOf(bool1);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsCore$6.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */