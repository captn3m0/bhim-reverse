package com.crashlytics.android.core;

import com.crashlytics.android.core.internal.models.BinaryImageData;

final class NativeCrashWriter$BinaryImageMessage
  extends NativeCrashWriter.ProtobufMessage
{
  private static final int PROTOBUF_TAG = 4;
  private final long baseAddr;
  private final String filePath;
  private final long imageSize;
  private final String uuid;
  
  public NativeCrashWriter$BinaryImageMessage(BinaryImageData paramBinaryImageData)
  {
    super(4, arrayOfProtobufMessage);
    long l = baseAddress;
    baseAddr = l;
    l = size;
    imageSize = l;
    String str = path;
    filePath = str;
    str = id;
    uuid = str;
  }
  
  public int getPropertiesSize()
  {
    long l = baseAddr;
    int i = CodedOutputStream.computeUInt64Size(1, l);
    l = imageSize;
    int j = CodedOutputStream.computeUInt64Size(2, l);
    ByteString localByteString1 = ByteString.copyFromUtf8(filePath);
    int k = CodedOutputStream.computeBytesSize(3, localByteString1);
    ByteString localByteString2 = ByteString.copyFromUtf8(uuid);
    int m = CodedOutputStream.computeBytesSize(4, localByteString2);
    return i + k + j + m;
  }
  
  public void writeProperties(CodedOutputStream paramCodedOutputStream)
  {
    long l = baseAddr;
    paramCodedOutputStream.writeUInt64(1, l);
    l = imageSize;
    paramCodedOutputStream.writeUInt64(2, l);
    ByteString localByteString = ByteString.copyFromUtf8(filePath);
    paramCodedOutputStream.writeBytes(3, localByteString);
    localByteString = ByteString.copyFromUtf8(uuid);
    paramCodedOutputStream.writeBytes(4, localByteString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/NativeCrashWriter$BinaryImageMessage.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */