package com.crashlytics.android.core;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class CrashlyticsUncaughtExceptionHandler$4
  implements FilenameFilter
{
  public boolean accept(File paramFile, String paramString)
  {
    return CrashlyticsUncaughtExceptionHandler.access$000().matcher(paramString).matches();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsUncaughtExceptionHandler$4.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */