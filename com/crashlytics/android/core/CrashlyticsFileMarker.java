package com.crashlytics.android.core;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.d.a;
import java.io.File;
import java.io.IOException;

class CrashlyticsFileMarker
{
  private final a fileStore;
  private final String markerName;
  
  public CrashlyticsFileMarker(String paramString, a parama)
  {
    markerName = paramString;
    fileStore = parama;
  }
  
  private File getMarkerFile()
  {
    File localFile1 = new java/io/File;
    File localFile2 = fileStore.a();
    String str = markerName;
    localFile1.<init>(localFile2, str);
    return localFile1;
  }
  
  public boolean create()
  {
    boolean bool = false;
    try
    {
      File localFile = getMarkerFile();
      bool = localFile.createNewFile();
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        k localk = c.h();
        String str1 = "CrashlyticsCore";
        Object localObject = new java/lang/StringBuilder;
        ((StringBuilder)localObject).<init>();
        localObject = ((StringBuilder)localObject).append("Error creating marker: ");
        String str2 = markerName;
        localObject = str2;
        localk.e(str1, (String)localObject, localIOException);
      }
    }
    return bool;
  }
  
  public boolean isPresent()
  {
    return getMarkerFile().exists();
  }
  
  public boolean remove()
  {
    return getMarkerFile().delete();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CrashlyticsFileMarker.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */