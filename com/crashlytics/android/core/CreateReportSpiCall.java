package com.crashlytics.android.core;

abstract interface CreateReportSpiCall
{
  public abstract boolean invoke(CreateReportRequest paramCreateReportRequest);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/core/CreateReportSpiCall.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */