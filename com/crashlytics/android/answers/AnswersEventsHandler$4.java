package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import java.util.concurrent.ScheduledExecutorService;

class AnswersEventsHandler$4
  implements Runnable
{
  AnswersEventsHandler$4(AnswersEventsHandler paramAnswersEventsHandler) {}
  
  public void run()
  {
    try
    {
      Object localObject1 = this$0;
      localObject1 = AnswersEventsHandler.access$000((AnswersEventsHandler)localObject1);
      SessionEventMetadata localSessionEventMetadata = ((SessionMetadataCollector)localObject1).getMetadata();
      localObject1 = this$0;
      localObject1 = AnswersEventsHandler.access$100((AnswersEventsHandler)localObject1);
      SessionAnalyticsFilesManager localSessionAnalyticsFilesManager = ((AnswersFilesManagerProvider)localObject1).getAnalyticsFilesManager();
      localObject1 = this$0;
      localSessionAnalyticsFilesManager.registerRollOverListener((io.fabric.sdk.android.services.c.d)localObject1);
      AnswersEventsHandler localAnswersEventsHandler = this$0;
      localObject1 = new com/crashlytics/android/answers/EnabledSessionAnalyticsManagerStrategy;
      localObject2 = this$0;
      localObject2 = AnswersEventsHandler.access$200((AnswersEventsHandler)localObject2);
      localObject3 = this$0;
      localObject3 = AnswersEventsHandler.access$300((AnswersEventsHandler)localObject3);
      localObject4 = this$0;
      localObject4 = executor;
      Object localObject5 = this$0;
      localObject5 = AnswersEventsHandler.access$400((AnswersEventsHandler)localObject5);
      ((EnabledSessionAnalyticsManagerStrategy)localObject1).<init>((h)localObject2, (Context)localObject3, (ScheduledExecutorService)localObject4, localSessionAnalyticsFilesManager, (io.fabric.sdk.android.services.network.d)localObject5, localSessionEventMetadata);
      strategy = ((SessionAnalyticsManagerStrategy)localObject1);
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = c.h();
        Object localObject3 = "Answers";
        Object localObject4 = "Failed to enable events";
        ((k)localObject2).e((String)localObject3, (String)localObject4, localException);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/AnswersEventsHandler$4.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */