package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.services.b.k;
import io.fabric.sdk.android.services.c.c;
import java.util.UUID;

class SessionAnalyticsFilesManager
  extends io.fabric.sdk.android.services.c.b
{
  private static final String SESSION_ANALYTICS_TO_SEND_FILE_EXTENSION = ".tap";
  private static final String SESSION_ANALYTICS_TO_SEND_FILE_PREFIX = "sa";
  private io.fabric.sdk.android.services.e.b analyticsSettingsData;
  
  SessionAnalyticsFilesManager(Context paramContext, SessionEventTransform paramSessionEventTransform, k paramk, c paramc)
  {
    super(paramContext, paramSessionEventTransform, paramk, paramc, 100);
  }
  
  protected String generateUniqueRollOverFileName()
  {
    Object localObject = UUID.randomUUID();
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append("sa").append("_");
    localObject = ((UUID)localObject).toString();
    localObject = localStringBuilder.append((String)localObject).append("_");
    long l = currentTimeProvider.a();
    return l + ".tap";
  }
  
  protected int getMaxByteSizePerFile()
  {
    io.fabric.sdk.android.services.e.b localb = analyticsSettingsData;
    if (localb == null) {}
    for (int i = super.getMaxByteSizePerFile();; i = c)
    {
      return i;
      localb = analyticsSettingsData;
    }
  }
  
  protected int getMaxFilesToKeep()
  {
    io.fabric.sdk.android.services.e.b localb = analyticsSettingsData;
    if (localb == null) {}
    for (int i = super.getMaxFilesToKeep();; i = e)
    {
      return i;
      localb = analyticsSettingsData;
    }
  }
  
  void setAnalyticsSettingsData(io.fabric.sdk.android.services.e.b paramb)
  {
    analyticsSettingsData = paramb;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/SessionAnalyticsFilesManager.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */