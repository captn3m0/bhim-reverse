package com.crashlytics.android.answers;

import android.app.Activity;
import android.os.Bundle;
import io.fabric.sdk.android.a.b;

class AnswersLifecycleCallbacks
  extends a.b
{
  private final SessionAnalyticsManager analyticsManager;
  private final BackgroundManager backgroundManager;
  
  public AnswersLifecycleCallbacks(SessionAnalyticsManager paramSessionAnalyticsManager, BackgroundManager paramBackgroundManager)
  {
    analyticsManager = paramSessionAnalyticsManager;
    backgroundManager = paramBackgroundManager;
  }
  
  public void onActivityCreated(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityDestroyed(Activity paramActivity) {}
  
  public void onActivityPaused(Activity paramActivity)
  {
    SessionAnalyticsManager localSessionAnalyticsManager = analyticsManager;
    SessionEvent.Type localType = SessionEvent.Type.PAUSE;
    localSessionAnalyticsManager.onLifecycle(paramActivity, localType);
    backgroundManager.onActivityPaused();
  }
  
  public void onActivityResumed(Activity paramActivity)
  {
    SessionAnalyticsManager localSessionAnalyticsManager = analyticsManager;
    SessionEvent.Type localType = SessionEvent.Type.RESUME;
    localSessionAnalyticsManager.onLifecycle(paramActivity, localType);
    backgroundManager.onActivityResumed();
  }
  
  public void onActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {}
  
  public void onActivityStarted(Activity paramActivity)
  {
    SessionAnalyticsManager localSessionAnalyticsManager = analyticsManager;
    SessionEvent.Type localType = SessionEvent.Type.START;
    localSessionAnalyticsManager.onLifecycle(paramActivity, localType);
  }
  
  public void onActivityStopped(Activity paramActivity)
  {
    SessionAnalyticsManager localSessionAnalyticsManager = analyticsManager;
    SessionEvent.Type localType = SessionEvent.Type.STOP;
    localSessionAnalyticsManager.onLifecycle(paramActivity, localType);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/AnswersLifecycleCallbacks.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */