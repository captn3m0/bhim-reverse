package com.crashlytics.android.answers;

import io.fabric.sdk.android.services.concurrency.a.a;
import java.util.Random;

class RandomBackoff
  implements a
{
  final a backoff;
  final double jitterPercent;
  final Random random;
  
  public RandomBackoff(a parama, double paramDouble)
  {
    this(parama, paramDouble, localRandom);
  }
  
  public RandomBackoff(a parama, double paramDouble, Random paramRandom)
  {
    double d = 0.0D;
    boolean bool = paramDouble < d;
    Object localObject;
    if (!bool)
    {
      d = 1.0D;
      bool = paramDouble < d;
      if (!bool) {}
    }
    else
    {
      localObject = new java/lang/IllegalArgumentException;
      ((IllegalArgumentException)localObject).<init>("jitterPercent must be between 0.0 and 1.0");
      throw ((Throwable)localObject);
    }
    if (parama == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("backoff must not be null");
      throw ((Throwable)localObject);
    }
    if (paramRandom == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("random must not be null");
      throw ((Throwable)localObject);
    }
    backoff = parama;
    jitterPercent = paramDouble;
    random = paramRandom;
  }
  
  public long getDelayMillis(int paramInt)
  {
    double d1 = randomJitter();
    double d2 = backoff.getDelayMillis(paramInt);
    return (d1 * d2);
  }
  
  double randomJitter()
  {
    double d1 = 1.0D;
    double d2 = jitterPercent;
    d2 = d1 - d2;
    double d3 = jitterPercent + d1;
    d1 = random.nextDouble();
    d3 = (d3 - d2) * d1;
    return d2 + d3;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/RandomBackoff.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */