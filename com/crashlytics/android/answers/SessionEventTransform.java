package com.crashlytics.android.answers;

import android.os.Build.VERSION;
import io.fabric.sdk.android.services.c.a;
import java.io.IOException;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class SessionEventTransform
  implements a
{
  static final String ADVERTISING_ID_KEY = "advertisingId";
  static final String ANDROID_ID_KEY = "androidId";
  static final String APP_BUNDLE_ID_KEY = "appBundleId";
  static final String APP_VERSION_CODE_KEY = "appVersionCode";
  static final String APP_VERSION_NAME_KEY = "appVersionName";
  static final String BETA_DEVICE_TOKEN_KEY = "betaDeviceToken";
  static final String BUILD_ID_KEY = "buildId";
  static final String CUSTOM_ATTRIBUTES = "customAttributes";
  static final String CUSTOM_TYPE = "customType";
  static final String DETAILS_KEY = "details";
  static final String DEVICE_MODEL_KEY = "deviceModel";
  static final String EXECUTION_ID_KEY = "executionId";
  static final String INSTALLATION_ID_KEY = "installationId";
  static final String LIMIT_AD_TRACKING_ENABLED_KEY = "limitAdTrackingEnabled";
  static final String OS_VERSION_KEY = "osVersion";
  static final String PREDEFINED_ATTRIBUTES = "predefinedAttributes";
  static final String PREDEFINED_TYPE = "predefinedType";
  static final String TIMESTAMP_KEY = "timestamp";
  static final String TYPE_KEY = "type";
  
  public JSONObject buildJsonForEvent(SessionEvent paramSessionEvent)
  {
    try
    {
      JSONObject localJSONObject = new org/json/JSONObject;
      localJSONObject.<init>();
      localObject1 = sessionEventMetadata;
      localObject2 = "appBundleId";
      Object localObject3 = appBundleId;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "executionId";
      localObject3 = executionId;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "installationId";
      localObject3 = installationId;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "androidId";
      localObject3 = androidId;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "advertisingId";
      localObject3 = advertisingId;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "limitAdTrackingEnabled";
      localObject3 = limitAdTrackingEnabled;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "betaDeviceToken";
      localObject3 = betaDeviceToken;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "buildId";
      localObject3 = buildId;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "osVersion";
      localObject3 = osVersion;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "deviceModel";
      localObject3 = deviceModel;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "appVersionCode";
      localObject3 = appVersionCode;
      localJSONObject.put((String)localObject2, localObject3);
      localObject2 = "appVersionName";
      localObject1 = appVersionName;
      localJSONObject.put((String)localObject2, localObject1);
      localObject1 = "timestamp";
      long l = timestamp;
      localJSONObject.put((String)localObject1, l);
      localObject1 = "type";
      localObject2 = type;
      localObject2 = ((SessionEvent.Type)localObject2).toString();
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = details;
      if (localObject1 != null)
      {
        localObject1 = "details";
        localObject2 = new org/json/JSONObject;
        localObject3 = details;
        ((JSONObject)localObject2).<init>((Map)localObject3);
        localJSONObject.put((String)localObject1, localObject2);
      }
      localObject1 = "customType";
      localObject2 = customType;
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = customAttributes;
      if (localObject1 != null)
      {
        localObject1 = "customAttributes";
        localObject2 = new org/json/JSONObject;
        localObject3 = customAttributes;
        ((JSONObject)localObject2).<init>((Map)localObject3);
        localJSONObject.put((String)localObject1, localObject2);
      }
      localObject1 = "predefinedType";
      localObject2 = predefinedType;
      localJSONObject.put((String)localObject1, localObject2);
      localObject1 = predefinedAttributes;
      if (localObject1 != null)
      {
        localObject1 = "predefinedAttributes";
        localObject2 = new org/json/JSONObject;
        localObject3 = predefinedAttributes;
        ((JSONObject)localObject2).<init>((Map)localObject3);
        localJSONObject.put((String)localObject1, localObject2);
      }
      return localJSONObject;
    }
    catch (JSONException localJSONException)
    {
      Object localObject2;
      int i = Build.VERSION.SDK_INT;
      int j = 9;
      if (i >= j)
      {
        localObject1 = new java/io/IOException;
        localObject2 = localJSONException.getMessage();
        ((IOException)localObject1).<init>((String)localObject2, localJSONException);
        throw ((Throwable)localObject1);
      }
      Object localObject1 = new java/io/IOException;
      String str = localJSONException.getMessage();
      ((IOException)localObject1).<init>(str);
      throw ((Throwable)localObject1);
    }
  }
  
  public byte[] toBytes(SessionEvent paramSessionEvent)
  {
    return buildJsonForEvent(paramSessionEvent).toString().getBytes("UTF-8");
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/SessionEventTransform.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */