package com.crashlytics.android.answers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class AnswersAttributes
{
  final Map attributes;
  final AnswersEventValidator validator;
  
  public AnswersAttributes(AnswersEventValidator paramAnswersEventValidator)
  {
    ConcurrentHashMap localConcurrentHashMap = new java/util/concurrent/ConcurrentHashMap;
    localConcurrentHashMap.<init>();
    attributes = localConcurrentHashMap;
    validator = paramAnswersEventValidator;
  }
  
  void put(String paramString, Number paramNumber)
  {
    Object localObject = validator;
    String str = "key";
    boolean bool = ((AnswersEventValidator)localObject).isNull(paramString, str);
    if (!bool)
    {
      localObject = validator;
      str = "value";
      bool = ((AnswersEventValidator)localObject).isNull(paramNumber, str);
      if (!bool) {
        break label47;
      }
    }
    for (;;)
    {
      return;
      label47:
      localObject = validator.limitStringLength(paramString);
      putAttribute((String)localObject, paramNumber);
    }
  }
  
  void put(String paramString1, String paramString2)
  {
    Object localObject = validator;
    String str = "key";
    boolean bool = ((AnswersEventValidator)localObject).isNull(paramString1, str);
    if (!bool)
    {
      localObject = validator;
      str = "value";
      bool = ((AnswersEventValidator)localObject).isNull(paramString2, str);
      if (!bool) {
        break label47;
      }
    }
    for (;;)
    {
      return;
      label47:
      localObject = validator.limitStringLength(paramString1);
      str = validator.limitStringLength(paramString2);
      putAttribute((String)localObject, str);
    }
  }
  
  void putAttribute(String paramString, Object paramObject)
  {
    Object localObject = validator;
    Map localMap = attributes;
    boolean bool = ((AnswersEventValidator)localObject).isFullMap(localMap, paramString);
    if (!bool)
    {
      localObject = attributes;
      ((Map)localObject).put(paramString, paramObject);
    }
  }
  
  public String toString()
  {
    JSONObject localJSONObject = new org/json/JSONObject;
    Map localMap = attributes;
    localJSONObject.<init>(localMap);
    return localJSONObject.toString();
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/AnswersAttributes.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */