package com.crashlytics.android.answers;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;

class AnswersEventsHandler$6
  implements Runnable
{
  AnswersEventsHandler$6(AnswersEventsHandler paramAnswersEventsHandler, SessionEvent.Builder paramBuilder, boolean paramBoolean) {}
  
  public void run()
  {
    try
    {
      Object localObject1 = this$0;
      localObject1 = strategy;
      localObject2 = val$eventBuilder;
      ((SessionAnalyticsManagerStrategy)localObject1).processEvent((SessionEvent.Builder)localObject2);
      boolean bool = val$flush;
      if (bool)
      {
        localObject1 = this$0;
        localObject1 = strategy;
        ((SessionAnalyticsManagerStrategy)localObject1).rollFileOver();
      }
      return;
    }
    catch (Exception localException)
    {
      for (;;)
      {
        Object localObject2 = c.h();
        String str1 = "Answers";
        String str2 = "Failed to process event";
        ((k)localObject2).e(str1, str2, localException);
      }
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/AnswersEventsHandler$6.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */