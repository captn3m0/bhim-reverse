package com.crashlytics.android.answers;

public class CustomEvent
  extends AnswersEvent
{
  private final String eventName;
  
  public CustomEvent(String paramString)
  {
    if (paramString == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("eventName must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = validator.limitStringLength(paramString);
    eventName = ((String)localObject);
  }
  
  String getCustomType()
  {
    return eventName;
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append("{eventName:\"");
    Object localObject = eventName;
    localStringBuilder = localStringBuilder.append((String)localObject).append('"').append(", customAttributes:");
    localObject = customAttributes;
    return localObject + "}";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/CustomEvent.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */