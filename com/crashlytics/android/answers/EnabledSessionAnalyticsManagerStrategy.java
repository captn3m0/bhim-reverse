package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.g;
import io.fabric.sdk.android.services.c.f;
import io.fabric.sdk.android.services.e.b;
import io.fabric.sdk.android.services.network.d;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

class EnabledSessionAnalyticsManagerStrategy
  implements SessionAnalyticsManagerStrategy
{
  static final int UNDEFINED_ROLLOVER_INTERVAL_SECONDS = 255;
  g apiKey;
  private final Context context;
  boolean customEventsEnabled;
  EventFilter eventFilter;
  private final ScheduledExecutorService executorService;
  private final SessionAnalyticsFilesManager filesManager;
  f filesSender;
  private final d httpRequestFactory;
  private final h kit;
  final SessionEventMetadata metadata;
  boolean predefinedEventsEnabled;
  private final AtomicReference rolloverFutureRef;
  volatile int rolloverIntervalSeconds;
  
  public EnabledSessionAnalyticsManagerStrategy(h paramh, Context paramContext, ScheduledExecutorService paramScheduledExecutorService, SessionAnalyticsFilesManager paramSessionAnalyticsFilesManager, d paramd, SessionEventMetadata paramSessionEventMetadata)
  {
    Object localObject = new java/util/concurrent/atomic/AtomicReference;
    ((AtomicReference)localObject).<init>();
    rolloverFutureRef = ((AtomicReference)localObject);
    localObject = new io/fabric/sdk/android/services/b/g;
    ((g)localObject).<init>();
    apiKey = ((g)localObject);
    localObject = new com/crashlytics/android/answers/KeepAllEventFilter;
    ((KeepAllEventFilter)localObject).<init>();
    eventFilter = ((EventFilter)localObject);
    customEventsEnabled = bool;
    predefinedEventsEnabled = bool;
    rolloverIntervalSeconds = -1;
    kit = paramh;
    context = paramContext;
    executorService = paramScheduledExecutorService;
    filesManager = paramSessionAnalyticsFilesManager;
    httpRequestFactory = paramd;
    metadata = paramSessionEventMetadata;
  }
  
  public void cancelTimeBasedFileRollOver()
  {
    Object localObject = rolloverFutureRef.get();
    if (localObject != null)
    {
      io.fabric.sdk.android.services.b.i.a(context, "Cancelling time-based rollover because no events are currently being generated.");
      ((ScheduledFuture)rolloverFutureRef.get()).cancel(false);
      localObject = rolloverFutureRef;
      ((AtomicReference)localObject).set(null);
    }
  }
  
  public void deleteAllEvents()
  {
    filesManager.deleteAllEventsFiles();
  }
  
  public void processEvent(SessionEvent.Builder paramBuilder)
  {
    Object localObject1 = metadata;
    localObject2 = paramBuilder.build((SessionEventMetadata)localObject1);
    boolean bool = customEventsEnabled;
    if (!bool)
    {
      localObject1 = SessionEvent.Type.CUSTOM;
      localObject3 = type;
      bool = ((SessionEvent.Type)localObject1).equals(localObject3);
      if (bool)
      {
        localObject1 = c.h();
        localObject3 = "Answers";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "Custom events tracking disabled - skipping event: ";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject2 = localObject2;
        ((k)localObject1).a((String)localObject3, (String)localObject2);
      }
    }
    for (;;)
    {
      return;
      bool = predefinedEventsEnabled;
      if (!bool)
      {
        localObject1 = SessionEvent.Type.PREDEFINED;
        localObject3 = type;
        bool = ((SessionEvent.Type)localObject1).equals(localObject3);
        if (bool)
        {
          localObject1 = c.h();
          localObject3 = "Answers";
          localObject4 = new java/lang/StringBuilder;
          ((StringBuilder)localObject4).<init>();
          localObject5 = "Predefined events tracking disabled - skipping event: ";
          localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
          localObject2 = localObject2;
          ((k)localObject1).a((String)localObject3, (String)localObject2);
          continue;
        }
      }
      localObject1 = eventFilter;
      bool = ((EventFilter)localObject1).skipEvent((SessionEvent)localObject2);
      if (bool)
      {
        localObject1 = c.h();
        localObject3 = "Answers";
        localObject4 = new java/lang/StringBuilder;
        ((StringBuilder)localObject4).<init>();
        localObject5 = "Skipping filtered event: ";
        localObject4 = ((StringBuilder)localObject4).append((String)localObject5);
        localObject2 = localObject2;
        ((k)localObject1).a((String)localObject3, (String)localObject2);
      }
      try
      {
        localObject1 = filesManager;
        ((SessionAnalyticsFilesManager)localObject1).writeEvent(localObject2);
      }
      catch (IOException localIOException)
      {
        for (;;)
        {
          localObject3 = c.h();
          localObject4 = "Answers";
          localObject5 = new java/lang/StringBuilder;
          ((StringBuilder)localObject5).<init>();
          String str = "Failed to write event: ";
          localObject5 = ((StringBuilder)localObject5).append(str);
          localObject2 = localObject2;
          ((k)localObject3).e((String)localObject4, (String)localObject2, localIOException);
        }
      }
      scheduleTimeBasedRollOverIfNeeded();
    }
  }
  
  public boolean rollFileOver()
  {
    try
    {
      SessionAnalyticsFilesManager localSessionAnalyticsFilesManager = filesManager;
      bool = localSessionAnalyticsFilesManager.rollFileOver();
    }
    catch (IOException localIOException)
    {
      for (;;)
      {
        Context localContext = context;
        String str = "Failed to roll file over.";
        io.fabric.sdk.android.services.b.i.a(localContext, str, localIOException);
        boolean bool = false;
        Object localObject = null;
      }
    }
    return bool;
  }
  
  void scheduleTimeBasedFileRollOver(long paramLong1, long paramLong2)
  {
    Object localObject1 = rolloverFutureRef.get();
    int i;
    if (localObject1 == null) {
      i = 1;
    }
    for (;;)
    {
      if (i != 0)
      {
        localObject2 = new io/fabric/sdk/android/services/c/i;
        localObject1 = context;
        ((io.fabric.sdk.android.services.c.i)localObject2).<init>((Context)localObject1, this);
        localObject1 = context;
        localObject3 = new java/lang/StringBuilder;
        ((StringBuilder)localObject3).<init>();
        localObject3 = ((StringBuilder)localObject3).append("Scheduling time based file roll over every ").append(paramLong2);
        String str = " seconds";
        localObject3 = str;
        io.fabric.sdk.android.services.b.i.a((Context)localObject1, (String)localObject3);
      }
      try
      {
        AtomicReference localAtomicReference = rolloverFutureRef;
        localObject1 = executorService;
        TimeUnit localTimeUnit = TimeUnit.SECONDS;
        localObject1 = ((ScheduledExecutorService)localObject1).scheduleAtFixedRate((Runnable)localObject2, paramLong1, paramLong2, localTimeUnit);
        localAtomicReference.set(localObject1);
        return;
      }
      catch (RejectedExecutionException localRejectedExecutionException)
      {
        for (;;)
        {
          localObject2 = context;
          localObject3 = "Failed to schedule time based file roll over";
          io.fabric.sdk.android.services.b.i.a((Context)localObject2, (String)localObject3, localRejectedExecutionException);
        }
      }
      i = 0;
      localObject1 = null;
    }
  }
  
  public void scheduleTimeBasedRollOverIfNeeded()
  {
    int i = rolloverIntervalSeconds;
    int j = -1;
    if (i != j) {}
    for (i = 1;; i = 0)
    {
      if (i != 0)
      {
        i = rolloverIntervalSeconds;
        long l1 = i;
        int k = rolloverIntervalSeconds;
        long l2 = k;
        scheduleTimeBasedFileRollOver(l1, l2);
      }
      return;
    }
  }
  
  public void sendEvents()
  {
    int i = 0;
    Object localObject1 = null;
    Object localObject2 = filesSender;
    if (localObject2 == null)
    {
      localObject2 = context;
      localObject1 = "skipping files send because we don't yet know the target endpoint";
      io.fabric.sdk.android.services.b.i.a((Context)localObject2, (String)localObject1);
      return;
    }
    io.fabric.sdk.android.services.b.i.a(context, "Sending all files");
    Object localObject3 = filesManager.getBatchOfFilesToSend();
    int j = 0;
    localObject2 = null;
    for (;;)
    {
      try
      {
        i = ((List)localObject3).size();
        if (i > 0)
        {
          localObject1 = context;
          localObject4 = Locale.US;
          str = "attempt to send batch of %d files";
          int k = 1;
          Object[] arrayOfObject = new Object[k];
          int m = ((List)localObject3).size();
          Integer localInteger = Integer.valueOf(m);
          arrayOfObject[0] = localInteger;
          localObject4 = String.format((Locale)localObject4, str, arrayOfObject);
          io.fabric.sdk.android.services.b.i.a((Context)localObject1, (String)localObject4);
          localObject1 = filesSender;
          bool = ((f)localObject1).send((List)localObject3);
          if (bool) {
            i = ((List)localObject3).size() + j;
          }
        }
      }
      catch (Exception localException1)
      {
        try
        {
          boolean bool;
          localObject2 = filesManager;
          ((SessionAnalyticsFilesManager)localObject2).deleteSentFiles((List)localObject3);
          j = i;
          if (!bool)
          {
            if (j != 0) {
              break;
            }
            localObject2 = filesManager;
            ((SessionAnalyticsFilesManager)localObject2).deleteOldestInRollOverIfOverMax();
            break;
          }
          localObject1 = filesManager;
          localObject1 = ((SessionAnalyticsFilesManager)localObject1).getBatchOfFilesToSend();
          localObject3 = localObject1;
        }
        catch (Exception localException2)
        {
          Object localObject4;
          String str;
          for (;;) {}
        }
        localException1 = localException1;
        i = j;
        localObject2 = localException1;
      }
      localObject3 = context;
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject4 = ((StringBuilder)localObject4).append("Failed to send batch of analytics files to server: ");
      str = ((Exception)localObject2).getMessage();
      localObject4 = str;
      io.fabric.sdk.android.services.b.i.a((Context)localObject3, (String)localObject4, (Throwable)localObject2);
      j = i;
    }
  }
  
  public void setAnalyticsSettingsData(b paramb, String paramString)
  {
    Object localObject1 = new com/crashlytics/android/answers/SessionAnalyticsFilesSender;
    Object localObject2 = kit;
    Object localObject3 = a;
    d locald = httpRequestFactory;
    Object localObject4 = apiKey;
    Object localObject5 = context;
    localObject5 = ((g)localObject4).a((Context)localObject5);
    localObject4 = paramString;
    ((SessionAnalyticsFilesSender)localObject1).<init>((h)localObject2, paramString, (String)localObject3, locald, (String)localObject5);
    localObject1 = AnswersRetryFilesSender.build((SessionAnalyticsFilesSender)localObject1);
    filesSender = ((f)localObject1);
    filesManager.setAnalyticsSettingsData(paramb);
    boolean bool = f;
    customEventsEnabled = bool;
    localObject2 = c.h();
    localObject4 = "Answers";
    localObject1 = new java/lang/StringBuilder;
    ((StringBuilder)localObject1).<init>();
    localObject3 = ((StringBuilder)localObject1).append("Custom event tracking ");
    bool = customEventsEnabled;
    if (bool)
    {
      localObject1 = "enabled";
      localObject1 = (String)localObject1;
      ((k)localObject2).a((String)localObject4, (String)localObject1);
      bool = g;
      predefinedEventsEnabled = bool;
      localObject2 = c.h();
      localObject4 = "Answers";
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject3 = ((StringBuilder)localObject1).append("Predefined event tracking ");
      bool = predefinedEventsEnabled;
      if (!bool) {
        break label317;
      }
    }
    label317:
    for (localObject1 = "enabled";; localObject1 = "disabled")
    {
      localObject1 = (String)localObject1;
      ((k)localObject2).a((String)localObject4, (String)localObject1);
      int i = i;
      int j = 1;
      if (i > j)
      {
        localObject1 = c.h();
        localObject2 = "Answers";
        localObject4 = "Event sampling enabled";
        ((k)localObject1).a((String)localObject2, (String)localObject4);
        localObject1 = new com/crashlytics/android/answers/SamplingEventFilter;
        j = i;
        ((SamplingEventFilter)localObject1).<init>(j);
        eventFilter = ((EventFilter)localObject1);
      }
      i = b;
      rolloverIntervalSeconds = i;
      long l = rolloverIntervalSeconds;
      scheduleTimeBasedFileRollOver(0L, l);
      return;
      localObject1 = "disabled";
      break;
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/EnabledSessionAnalyticsManagerStrategy.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */