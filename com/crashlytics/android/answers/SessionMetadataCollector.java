package com.crashlytics.android.answers;

import android.content.Context;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.b.o.a;
import java.util.Map;
import java.util.UUID;

class SessionMetadataCollector
{
  private final Context context;
  private final o idManager;
  private final String versionCode;
  private final String versionName;
  
  public SessionMetadataCollector(Context paramContext, o paramo, String paramString1, String paramString2)
  {
    context = paramContext;
    idManager = paramo;
    versionCode = paramString1;
    versionName = paramString2;
  }
  
  public SessionEventMetadata getMetadata()
  {
    Object localObject1 = idManager.i();
    String str1 = idManager.c();
    String str2 = idManager.b();
    Object localObject2 = o.a.d;
    String str3 = (String)((Map)localObject1).get(localObject2);
    localObject2 = o.a.g;
    String str4 = (String)((Map)localObject1).get(localObject2);
    Boolean localBoolean = idManager.l();
    localObject2 = o.a.c;
    String str5 = (String)((Map)localObject1).get(localObject2);
    String str6 = i.m(context);
    String str7 = idManager.d();
    String str8 = idManager.g();
    localObject2 = UUID.randomUUID().toString();
    localObject1 = new com/crashlytics/android/answers/SessionEventMetadata;
    String str9 = versionCode;
    String str10 = versionName;
    ((SessionEventMetadata)localObject1).<init>(str1, (String)localObject2, str2, str3, str4, localBoolean, str5, str6, str7, str8, str9, str10);
    return (SessionEventMetadata)localObject1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/SessionMetadataCollector.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */