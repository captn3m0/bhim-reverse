package com.crashlytics.android.answers;

import java.util.Map;

class SessionEvent$Builder
{
  Map customAttributes;
  String customType;
  Map details;
  Map predefinedAttributes;
  String predefinedType;
  final long timestamp;
  final SessionEvent.Type type;
  
  public SessionEvent$Builder(SessionEvent.Type paramType)
  {
    type = paramType;
    long l = System.currentTimeMillis();
    timestamp = l;
    details = null;
    customType = null;
    customAttributes = null;
    predefinedType = null;
    predefinedAttributes = null;
  }
  
  public SessionEvent build(SessionEventMetadata paramSessionEventMetadata)
  {
    SessionEvent localSessionEvent = new com/crashlytics/android/answers/SessionEvent;
    long l = timestamp;
    SessionEvent.Type localType = type;
    Map localMap1 = details;
    String str1 = customType;
    Map localMap2 = customAttributes;
    String str2 = predefinedType;
    Map localMap3 = predefinedAttributes;
    localSessionEvent.<init>(paramSessionEventMetadata, l, localType, localMap1, str1, localMap2, str2, localMap3, null);
    return localSessionEvent;
  }
  
  public Builder customAttributes(Map paramMap)
  {
    customAttributes = paramMap;
    return this;
  }
  
  public Builder customType(String paramString)
  {
    customType = paramString;
    return this;
  }
  
  public Builder details(Map paramMap)
  {
    details = paramMap;
    return this;
  }
  
  public Builder predefinedAttributes(Map paramMap)
  {
    predefinedAttributes = paramMap;
    return this;
  }
  
  public Builder predefinedType(String paramString)
  {
    predefinedType = paramString;
    return this;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/SessionEvent$Builder.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */