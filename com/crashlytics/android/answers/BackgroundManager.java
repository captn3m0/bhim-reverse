package com.crashlytics.android.answers;

import io.fabric.sdk.android.c;
import io.fabric.sdk.android.k;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

class BackgroundManager
{
  private static final int BACKGROUND_DELAY = 5000;
  final AtomicReference backgroundFutureRef;
  private final ScheduledExecutorService executorService;
  private volatile boolean flushOnBackground;
  boolean inBackground;
  private final List listeners;
  
  public BackgroundManager(ScheduledExecutorService paramScheduledExecutorService)
  {
    Object localObject = new java/util/ArrayList;
    ((ArrayList)localObject).<init>();
    listeners = ((List)localObject);
    flushOnBackground = bool;
    localObject = new java/util/concurrent/atomic/AtomicReference;
    ((AtomicReference)localObject).<init>();
    backgroundFutureRef = ((AtomicReference)localObject);
    inBackground = bool;
    executorService = paramScheduledExecutorService;
  }
  
  private void notifyBackground()
  {
    Object localObject = listeners;
    Iterator localIterator = ((List)localObject).iterator();
    for (;;)
    {
      boolean bool = localIterator.hasNext();
      if (!bool) {
        break;
      }
      localObject = (BackgroundManager.Listener)localIterator.next();
      ((BackgroundManager.Listener)localObject).onBackground();
    }
  }
  
  public void onActivityPaused()
  {
    boolean bool = flushOnBackground;
    if (bool)
    {
      bool = inBackground;
      if (!bool)
      {
        bool = true;
        inBackground = bool;
      }
    }
    try
    {
      AtomicReference localAtomicReference = backgroundFutureRef;
      localk = null;
      localObject1 = executorService;
      localObject2 = new com/crashlytics/android/answers/BackgroundManager$1;
      ((BackgroundManager.1)localObject2).<init>(this);
      long l = 5000L;
      TimeUnit localTimeUnit = TimeUnit.MILLISECONDS;
      localObject1 = ((ScheduledExecutorService)localObject1).schedule((Runnable)localObject2, l, localTimeUnit);
      localAtomicReference.compareAndSet(null, localObject1);
      return;
    }
    catch (RejectedExecutionException localRejectedExecutionException)
    {
      for (;;)
      {
        k localk = c.h();
        Object localObject1 = "Answers";
        Object localObject2 = "Failed to schedule background detector";
        localk.a((String)localObject1, (String)localObject2, localRejectedExecutionException);
      }
    }
  }
  
  public void onActivityResumed()
  {
    inBackground = false;
    ScheduledFuture localScheduledFuture = (ScheduledFuture)backgroundFutureRef.getAndSet(null);
    if (localScheduledFuture != null) {
      localScheduledFuture.cancel(false);
    }
  }
  
  public void registerListener(BackgroundManager.Listener paramListener)
  {
    listeners.add(paramListener);
  }
  
  public void setFlushOnBackground(boolean paramBoolean)
  {
    flushOnBackground = paramBoolean;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/BackgroundManager.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */