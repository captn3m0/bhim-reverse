package com.crashlytics.android.answers;

public class RatingEvent
  extends PredefinedEvent
{
  static final String CONTENT_ID_ATTRIBUTE = "contentId";
  static final String CONTENT_NAME_ATTRIBUTE = "contentName";
  static final String CONTENT_TYPE_ATTRIBUTE = "contentType";
  static final String RATING_ATTRIBUTE = "rating";
  static final String TYPE = "rating";
  
  String getPredefinedType()
  {
    return "rating";
  }
  
  public RatingEvent putContentId(String paramString)
  {
    predefinedAttributes.put("contentId", paramString);
    return this;
  }
  
  public RatingEvent putContentName(String paramString)
  {
    predefinedAttributes.put("contentName", paramString);
    return this;
  }
  
  public RatingEvent putContentType(String paramString)
  {
    predefinedAttributes.put("contentType", paramString);
    return this;
  }
  
  public RatingEvent putRating(int paramInt)
  {
    AnswersAttributes localAnswersAttributes = predefinedAttributes;
    Integer localInteger = Integer.valueOf(paramInt);
    localAnswersAttributes.put("rating", localInteger);
    return this;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/RatingEvent.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */