package com.crashlytics.android.answers;

import android.app.Activity;
import android.content.Context;
import android.os.Looper;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.n;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.network.d;
import java.util.concurrent.ScheduledExecutorService;

class SessionAnalyticsManager
  implements BackgroundManager.Listener
{
  static final String EXECUTOR_SERVICE = "Answers Events Handler";
  static final String ON_CRASH_ERROR_MSG = "onCrash called from main thread!!!";
  final BackgroundManager backgroundManager;
  final AnswersEventsHandler eventsHandler;
  private final long installedAt;
  final io.fabric.sdk.android.a lifecycleManager;
  final AnswersPreferenceManager preferenceManager;
  
  SessionAnalyticsManager(AnswersEventsHandler paramAnswersEventsHandler, io.fabric.sdk.android.a parama, BackgroundManager paramBackgroundManager, AnswersPreferenceManager paramAnswersPreferenceManager, long paramLong)
  {
    eventsHandler = paramAnswersEventsHandler;
    lifecycleManager = parama;
    backgroundManager = paramBackgroundManager;
    preferenceManager = paramAnswersPreferenceManager;
    installedAt = paramLong;
  }
  
  public static SessionAnalyticsManager build(h paramh, Context paramContext, o paramo, String paramString1, String paramString2, long paramLong)
  {
    Object localObject1 = new com/crashlytics/android/answers/SessionMetadataCollector;
    ((SessionMetadataCollector)localObject1).<init>(paramContext, paramo, paramString1, paramString2);
    Object localObject2 = new com/crashlytics/android/answers/AnswersFilesManagerProvider;
    Object localObject3 = new io/fabric/sdk/android/services/d/b;
    ((io.fabric.sdk.android.services.d.b)localObject3).<init>(paramh);
    ((AnswersFilesManagerProvider)localObject2).<init>(paramContext, (io.fabric.sdk.android.services.d.a)localObject3);
    Object localObject4 = new io/fabric/sdk/android/services/network/b;
    localObject3 = c.h();
    ((io.fabric.sdk.android.services.network.b)localObject4).<init>((k)localObject3);
    io.fabric.sdk.android.a locala = new io/fabric/sdk/android/a;
    locala.<init>(paramContext);
    ScheduledExecutorService localScheduledExecutorService = n.b("Answers Events Handler");
    BackgroundManager localBackgroundManager = new com/crashlytics/android/answers/BackgroundManager;
    localBackgroundManager.<init>(localScheduledExecutorService);
    localObject3 = new com/crashlytics/android/answers/AnswersEventsHandler;
    Object localObject5 = paramh;
    ((AnswersEventsHandler)localObject3).<init>(paramh, paramContext, (AnswersFilesManagerProvider)localObject2, (SessionMetadataCollector)localObject1, (d)localObject4, localScheduledExecutorService);
    localObject4 = AnswersPreferenceManager.build(paramContext);
    localObject5 = new com/crashlytics/android/answers/SessionAnalyticsManager;
    localObject2 = locala;
    localObject1 = localBackgroundManager;
    ((SessionAnalyticsManager)localObject5).<init>((AnswersEventsHandler)localObject3, locala, localBackgroundManager, (AnswersPreferenceManager)localObject4, paramLong);
    return (SessionAnalyticsManager)localObject5;
  }
  
  public void disable()
  {
    lifecycleManager.a();
    eventsHandler.disable();
  }
  
  public void enable()
  {
    eventsHandler.enable();
    Object localObject = lifecycleManager;
    AnswersLifecycleCallbacks localAnswersLifecycleCallbacks = new com/crashlytics/android/answers/AnswersLifecycleCallbacks;
    BackgroundManager localBackgroundManager = backgroundManager;
    localAnswersLifecycleCallbacks.<init>(this, localBackgroundManager);
    ((io.fabric.sdk.android.a)localObject).a(localAnswersLifecycleCallbacks);
    localObject = backgroundManager;
    ((BackgroundManager)localObject).registerListener(this);
    boolean bool = isFirstLaunch();
    if (bool)
    {
      long l = installedAt;
      onInstall(l);
      localObject = preferenceManager;
      ((AnswersPreferenceManager)localObject).setAnalyticsLaunched();
    }
  }
  
  boolean isFirstLaunch()
  {
    AnswersPreferenceManager localAnswersPreferenceManager = preferenceManager;
    boolean bool = localAnswersPreferenceManager.hasAnalyticsLaunched();
    if (!bool) {
      bool = true;
    }
    for (;;)
    {
      return bool;
      bool = false;
      localAnswersPreferenceManager = null;
    }
  }
  
  public void onBackground()
  {
    c.h().a("Answers", "Flush events when app is backgrounded");
    eventsHandler.flushEvents();
  }
  
  public void onCrash(String paramString1, String paramString2)
  {
    Object localObject1 = Looper.myLooper();
    Object localObject2 = Looper.getMainLooper();
    if (localObject1 == localObject2)
    {
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("onCrash called from main thread!!!");
      throw ((Throwable)localObject1);
    }
    c.h().a("Answers", "Logged crash");
    localObject1 = eventsHandler;
    localObject2 = SessionEvent.crashEventBuilder(paramString1, paramString2);
    ((AnswersEventsHandler)localObject1).processEventSync((SessionEvent.Builder)localObject2);
  }
  
  public void onCustom(CustomEvent paramCustomEvent)
  {
    Object localObject1 = c.h();
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = "Logged custom event: " + paramCustomEvent;
    ((k)localObject1).a("Answers", (String)localObject2);
    localObject1 = eventsHandler;
    SessionEvent.Builder localBuilder = SessionEvent.customEventBuilder(paramCustomEvent);
    ((AnswersEventsHandler)localObject1).processEventAsync(localBuilder);
  }
  
  public void onError(String paramString) {}
  
  public void onInstall(long paramLong)
  {
    c.h().a("Answers", "Logged install");
    AnswersEventsHandler localAnswersEventsHandler = eventsHandler;
    SessionEvent.Builder localBuilder = SessionEvent.installEventBuilder(paramLong);
    localAnswersEventsHandler.processEventAsyncAndFlush(localBuilder);
  }
  
  public void onLifecycle(Activity paramActivity, SessionEvent.Type paramType)
  {
    Object localObject1 = c.h();
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = ((StringBuilder)localObject2).append("Logged lifecycle event: ");
    String str = paramType.name();
    localObject2 = str;
    ((k)localObject1).a("Answers", (String)localObject2);
    localObject1 = eventsHandler;
    SessionEvent.Builder localBuilder = SessionEvent.lifecycleEventBuilder(paramType, paramActivity);
    ((AnswersEventsHandler)localObject1).processEventAsync(localBuilder);
  }
  
  public void onPredefined(PredefinedEvent paramPredefinedEvent)
  {
    Object localObject1 = c.h();
    Object localObject2 = new java/lang/StringBuilder;
    ((StringBuilder)localObject2).<init>();
    localObject2 = "Logged predefined event: " + paramPredefinedEvent;
    ((k)localObject1).a("Answers", (String)localObject2);
    localObject1 = eventsHandler;
    SessionEvent.Builder localBuilder = SessionEvent.predefinedEventBuilder(paramPredefinedEvent);
    ((AnswersEventsHandler)localObject1).processEventAsync(localBuilder);
  }
  
  public void setAnalyticsSettingsData(io.fabric.sdk.android.services.e.b paramb, String paramString)
  {
    BackgroundManager localBackgroundManager = backgroundManager;
    boolean bool = h;
    localBackgroundManager.setFlushOnBackground(bool);
    eventsHandler.setAnalyticsSettingsData(paramb, paramString);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/SessionAnalyticsManager.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */