package com.crashlytics.android.answers;

import io.fabric.sdk.android.services.c.f;
import io.fabric.sdk.android.services.concurrency.a.a;
import io.fabric.sdk.android.services.concurrency.a.b;
import io.fabric.sdk.android.services.concurrency.a.c;
import io.fabric.sdk.android.services.concurrency.a.d;
import io.fabric.sdk.android.services.concurrency.a.e;
import java.util.List;

class AnswersRetryFilesSender
  implements f
{
  private static final int BACKOFF_MS = 1000;
  private static final int BACKOFF_POWER = 8;
  private static final double JITTER_PERCENT = 0.1D;
  private static final int MAX_RETRIES = 5;
  private final SessionAnalyticsFilesSender filesSender;
  private final RetryManager retryManager;
  
  AnswersRetryFilesSender(SessionAnalyticsFilesSender paramSessionAnalyticsFilesSender, RetryManager paramRetryManager)
  {
    filesSender = paramSessionAnalyticsFilesSender;
    retryManager = paramRetryManager;
  }
  
  public static AnswersRetryFilesSender build(SessionAnalyticsFilesSender paramSessionAnalyticsFilesSender)
  {
    Object localObject1 = new com/crashlytics/android/answers/RandomBackoff;
    Object localObject2 = new io/fabric/sdk/android/services/concurrency/a/c;
    ((c)localObject2).<init>(1000L, 8);
    ((RandomBackoff)localObject1).<init>((a)localObject2, 0.1D);
    localObject2 = new io/fabric/sdk/android/services/concurrency/a/b;
    ((b)localObject2).<init>(5);
    e locale = new io/fabric/sdk/android/services/concurrency/a/e;
    locale.<init>((a)localObject1, (d)localObject2);
    localObject1 = new com/crashlytics/android/answers/RetryManager;
    ((RetryManager)localObject1).<init>(locale);
    localObject2 = new com/crashlytics/android/answers/AnswersRetryFilesSender;
    ((AnswersRetryFilesSender)localObject2).<init>(paramSessionAnalyticsFilesSender, (RetryManager)localObject1);
    return (AnswersRetryFilesSender)localObject2;
  }
  
  public boolean send(List paramList)
  {
    boolean bool1 = false;
    RetryManager localRetryManager = null;
    long l = System.nanoTime();
    Object localObject = retryManager;
    boolean bool2 = ((RetryManager)localObject).canRetry(l);
    if (bool2)
    {
      localObject = filesSender;
      bool2 = ((SessionAnalyticsFilesSender)localObject).send(paramList);
      if (!bool2) {
        break label61;
      }
      localRetryManager = retryManager;
      localRetryManager.reset();
      bool1 = true;
    }
    for (;;)
    {
      return bool1;
      label61:
      localObject = retryManager;
      ((RetryManager)localObject).recordRetry(l);
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/AnswersRetryFilesSender.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */