package com.crashlytics.android.answers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import io.fabric.sdk.android.services.d.c;
import io.fabric.sdk.android.services.d.d;

class AnswersPreferenceManager
{
  static final String PREFKEY_ANALYTICS_LAUNCHED = "analytics_launched";
  static final String PREF_STORE_NAME = "settings";
  private final c prefStore;
  
  AnswersPreferenceManager(c paramc)
  {
    prefStore = paramc;
  }
  
  public static AnswersPreferenceManager build(Context paramContext)
  {
    d locald = new io/fabric/sdk/android/services/d/d;
    locald.<init>(paramContext, "settings");
    AnswersPreferenceManager localAnswersPreferenceManager = new com/crashlytics/android/answers/AnswersPreferenceManager;
    localAnswersPreferenceManager.<init>(locald);
    return localAnswersPreferenceManager;
  }
  
  public boolean hasAnalyticsLaunched()
  {
    return prefStore.a().getBoolean("analytics_launched", false);
  }
  
  public void setAnalyticsLaunched()
  {
    c localc = prefStore;
    SharedPreferences.Editor localEditor = prefStore.b().putBoolean("analytics_launched", true);
    localc.a(localEditor);
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/AnswersPreferenceManager.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */