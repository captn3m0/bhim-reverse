package com.crashlytics.android.answers;

import io.fabric.sdk.android.c;
import java.util.Map;

public abstract class AnswersEvent
{
  public static final int MAX_NUM_ATTRIBUTES = 20;
  public static final int MAX_STRING_LENGTH = 100;
  final AnswersAttributes customAttributes;
  final AnswersEventValidator validator;
  
  public AnswersEvent()
  {
    Object localObject = new com/crashlytics/android/answers/AnswersEventValidator;
    boolean bool = c.i();
    ((AnswersEventValidator)localObject).<init>(20, 100, bool);
    validator = ((AnswersEventValidator)localObject);
    localObject = new com/crashlytics/android/answers/AnswersAttributes;
    AnswersEventValidator localAnswersEventValidator = validator;
    ((AnswersAttributes)localObject).<init>(localAnswersEventValidator);
    customAttributes = ((AnswersAttributes)localObject);
  }
  
  Map getCustomAttributes()
  {
    return customAttributes.attributes;
  }
  
  public AnswersEvent putCustomAttribute(String paramString, Number paramNumber)
  {
    customAttributes.put(paramString, paramNumber);
    return this;
  }
  
  public AnswersEvent putCustomAttribute(String paramString1, String paramString2)
  {
    customAttributes.put(paramString1, paramString2);
    return this;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/AnswersEvent.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */