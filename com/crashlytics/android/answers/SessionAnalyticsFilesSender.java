package com.crashlytics.android.answers;

import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.a;
import io.fabric.sdk.android.services.b.r;
import io.fabric.sdk.android.services.c.f;
import io.fabric.sdk.android.services.network.HttpRequest;
import io.fabric.sdk.android.services.network.d;
import java.io.File;
import java.util.Iterator;
import java.util.List;

class SessionAnalyticsFilesSender
  extends a
  implements f
{
  static final String FILE_CONTENT_TYPE = "application/vnd.crashlytics.android.events";
  static final String FILE_PARAM_NAME = "session_analytics_file_";
  private final String apiKey;
  
  public SessionAnalyticsFilesSender(h paramh, String paramString1, String paramString2, d paramd, String paramString3)
  {
    super(paramh, paramString1, paramString2, paramd, localc);
    apiKey = paramString3;
  }
  
  public boolean send(List paramList)
  {
    boolean bool1 = false;
    Object localObject1 = getHttpRequest().a("X-CRASHLYTICS-API-CLIENT-TYPE", "android");
    Object localObject2 = kit.getVersion();
    localObject1 = ((HttpRequest)localObject1).a("X-CRASHLYTICS-API-CLIENT-VERSION", (String)localObject2);
    localObject2 = apiKey;
    localObject2 = ((HttpRequest)localObject1).a("X-CRASHLYTICS-API-KEY", (String)localObject2);
    Object localObject3 = paramList.iterator();
    int i = 0;
    k localk = null;
    for (;;)
    {
      boolean bool2 = ((Iterator)localObject3).hasNext();
      if (!bool2) {
        break;
      }
      localObject1 = (File)((Iterator)localObject3).next();
      localObject4 = new java/lang/StringBuilder;
      ((StringBuilder)localObject4).<init>();
      localObject4 = "session_analytics_file_" + i;
      String str1 = ((File)localObject1).getName();
      String str2 = "application/vnd.crashlytics.android.events";
      ((HttpRequest)localObject2).a((String)localObject4, str1, str2, (File)localObject1);
      j = i + 1;
      i = j;
    }
    localObject1 = io.fabric.sdk.android.c.h();
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject3 = ((StringBuilder)localObject3).append("Sending ");
    int k = paramList.size();
    localObject3 = ((StringBuilder)localObject3).append(k).append(" analytics files to ");
    Object localObject4 = getUrl();
    localObject3 = (String)localObject4;
    ((k)localObject1).a("Answers", (String)localObject3);
    int j = ((HttpRequest)localObject2).b();
    localk = io.fabric.sdk.android.c.h();
    localObject2 = "Answers";
    localObject3 = new java/lang/StringBuilder;
    ((StringBuilder)localObject3).<init>();
    localObject4 = "Response code for analytics file send is ";
    localObject3 = (String)localObject4 + j;
    localk.a((String)localObject2, (String)localObject3);
    j = r.a(j);
    if (j == 0) {
      bool1 = true;
    }
    return bool1;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/SessionAnalyticsFilesSender.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */