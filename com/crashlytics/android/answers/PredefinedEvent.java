package com.crashlytics.android.answers;

import java.util.Map;

public abstract class PredefinedEvent
  extends AnswersEvent
{
  final AnswersAttributes predefinedAttributes;
  
  public PredefinedEvent()
  {
    AnswersAttributes localAnswersAttributes = new com/crashlytics/android/answers/AnswersAttributes;
    AnswersEventValidator localAnswersEventValidator = validator;
    localAnswersAttributes.<init>(localAnswersEventValidator);
    predefinedAttributes = localAnswersAttributes;
  }
  
  Map getPredefinedAttributes()
  {
    return predefinedAttributes.attributes;
  }
  
  abstract String getPredefinedType();
  
  public String toString()
  {
    StringBuilder localStringBuilder = new java/lang/StringBuilder;
    localStringBuilder.<init>();
    localStringBuilder = localStringBuilder.append("{type:\"");
    Object localObject = getPredefinedType();
    localStringBuilder = localStringBuilder.append((String)localObject).append('"').append(", predefinedAttributes:");
    localObject = predefinedAttributes;
    localStringBuilder = localStringBuilder.append(localObject).append(", customAttributes:");
    localObject = customAttributes;
    return localObject + "}";
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/PredefinedEvent.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */