package com.crashlytics.android.answers;

abstract interface EventFilter
{
  public abstract boolean skipEvent(SessionEvent paramSessionEvent);
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/EventFilter.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */