package com.crashlytics.android.answers;

import io.fabric.sdk.android.services.concurrency.a.e;

class RetryManager
{
  private static final long NANOSECONDS_IN_MS = 1000000L;
  long lastRetry;
  private e retryState;
  
  public RetryManager(e parame)
  {
    if (parame == null)
    {
      NullPointerException localNullPointerException = new java/lang/NullPointerException;
      localNullPointerException.<init>("retryState must not be null");
      throw localNullPointerException;
    }
    retryState = parame;
  }
  
  public boolean canRetry(long paramLong)
  {
    e locale = retryState;
    long l1 = locale.a();
    long l2 = 1000000L * l1;
    l1 = lastRetry;
    l1 = paramLong - l1;
    boolean bool = l1 < l2;
    if (!bool) {}
    for (bool = true;; bool = false) {
      return bool;
    }
  }
  
  public void recordRetry(long paramLong)
  {
    lastRetry = paramLong;
    e locale = retryState.b();
    retryState = locale;
  }
  
  public void reset()
  {
    lastRetry = 0L;
    e locale = retryState.c();
    retryState = locale;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/RetryManager.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */