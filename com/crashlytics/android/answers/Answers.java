package com.crashlytics.android.answers;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import io.fabric.sdk.android.c;
import io.fabric.sdk.android.h;
import io.fabric.sdk.android.k;
import io.fabric.sdk.android.services.b.i;
import io.fabric.sdk.android.services.b.j.a;
import io.fabric.sdk.android.services.b.j.b;
import io.fabric.sdk.android.services.b.o;
import io.fabric.sdk.android.services.e.b;
import io.fabric.sdk.android.services.e.m;
import io.fabric.sdk.android.services.e.q;
import io.fabric.sdk.android.services.e.t;
import java.io.File;

public class Answers
  extends h
{
  static final String CRASHLYTICS_API_ENDPOINT = "com.crashlytics.ApiEndpoint";
  public static final String TAG = "Answers";
  SessionAnalyticsManager analyticsManager;
  
  public static Answers getInstance()
  {
    return (Answers)c.a(Answers.class);
  }
  
  protected Boolean doInBackground()
  {
    for (;;)
    {
      try
      {
        localObject1 = q.a();
        localObject1 = ((q)localObject1).b();
        if (localObject1 != null) {
          continue;
        }
        localObject1 = c.h();
        localObject2 = "Answers";
        str1 = "Failed to retrieve settings";
        ((k)localObject1).e((String)localObject2, str1);
        bool1 = false;
        localObject1 = null;
        localObject1 = Boolean.valueOf(false);
      }
      catch (Exception localException)
      {
        Object localObject1;
        boolean bool1;
        boolean bool2;
        Object localObject2 = c.h();
        String str1 = "Answers";
        String str2 = "Error dealing with settings";
        ((k)localObject2).e(str1, str2, localException);
        Boolean localBoolean = Boolean.valueOf(false);
        continue;
      }
      return (Boolean)localObject1;
      localObject2 = d;
      bool2 = d;
      if (bool2)
      {
        localObject2 = c.h();
        str1 = "Answers";
        str2 = "Analytics collection enabled";
        ((k)localObject2).a(str1, str2);
        localObject2 = analyticsManager;
        localObject1 = e;
        str1 = getOverridenSpiEndpoint();
        ((SessionAnalyticsManager)localObject2).setAnalyticsSettingsData((b)localObject1, str1);
        bool1 = true;
        localObject1 = Boolean.valueOf(bool1);
      }
      else
      {
        localObject1 = c.h();
        localObject2 = "Answers";
        str1 = "Analytics collection disabled";
        ((k)localObject1).a((String)localObject2, str1);
        localObject1 = analyticsManager;
        ((SessionAnalyticsManager)localObject1).disable();
        bool1 = false;
        localObject1 = null;
        localObject1 = Boolean.valueOf(false);
      }
    }
  }
  
  public String getIdentifier()
  {
    return "com.crashlytics.sdk.android:answers";
  }
  
  String getOverridenSpiEndpoint()
  {
    return i.b(getContext(), "com.crashlytics.ApiEndpoint");
  }
  
  public String getVersion()
  {
    return "1.3.10.143";
  }
  
  public void logAddToCart(AddToCartEvent paramAddToCartEvent)
  {
    if (paramAddToCartEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramAddToCartEvent);
    }
  }
  
  public void logContentView(ContentViewEvent paramContentViewEvent)
  {
    if (paramContentViewEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramContentViewEvent);
    }
  }
  
  public void logCustom(CustomEvent paramCustomEvent)
  {
    if (paramCustomEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onCustom(paramCustomEvent);
    }
  }
  
  public void logInvite(InviteEvent paramInviteEvent)
  {
    if (paramInviteEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramInviteEvent);
    }
  }
  
  public void logLevelEnd(LevelEndEvent paramLevelEndEvent)
  {
    if (paramLevelEndEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramLevelEndEvent);
    }
  }
  
  public void logLevelStart(LevelStartEvent paramLevelStartEvent)
  {
    if (paramLevelStartEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramLevelStartEvent);
    }
  }
  
  public void logLogin(LoginEvent paramLoginEvent)
  {
    if (paramLoginEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramLoginEvent);
    }
  }
  
  public void logPurchase(PurchaseEvent paramPurchaseEvent)
  {
    if (paramPurchaseEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramPurchaseEvent);
    }
  }
  
  public void logRating(RatingEvent paramRatingEvent)
  {
    if (paramRatingEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramRatingEvent);
    }
  }
  
  public void logSearch(SearchEvent paramSearchEvent)
  {
    if (paramSearchEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramSearchEvent);
    }
  }
  
  public void logShare(ShareEvent paramShareEvent)
  {
    if (paramShareEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramShareEvent);
    }
  }
  
  public void logSignUp(SignUpEvent paramSignUpEvent)
  {
    if (paramSignUpEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramSignUpEvent);
    }
  }
  
  public void logStartCheckout(StartCheckoutEvent paramStartCheckoutEvent)
  {
    if (paramStartCheckoutEvent == null)
    {
      localObject = new java/lang/NullPointerException;
      ((NullPointerException)localObject).<init>("event must not be null");
      throw ((Throwable)localObject);
    }
    Object localObject = analyticsManager;
    if (localObject != null)
    {
      localObject = analyticsManager;
      ((SessionAnalyticsManager)localObject).onPredefined(paramStartCheckoutEvent);
    }
  }
  
  public void onException(j.a parama)
  {
    SessionAnalyticsManager localSessionAnalyticsManager = analyticsManager;
    if (localSessionAnalyticsManager != null)
    {
      localSessionAnalyticsManager = analyticsManager;
      String str1 = parama.a();
      String str2 = parama.b();
      localSessionAnalyticsManager.onCrash(str1, str2);
    }
  }
  
  public void onException(j.b paramb)
  {
    SessionAnalyticsManager localSessionAnalyticsManager = analyticsManager;
    if (localSessionAnalyticsManager != null)
    {
      localSessionAnalyticsManager = analyticsManager;
      String str = paramb.a();
      localSessionAnalyticsManager.onError(str);
    }
  }
  
  protected boolean onPreExecute()
  {
    boolean bool = false;
    for (;;)
    {
      try
      {
        localObject1 = getContext();
        localObject2 = ((Context)localObject1).getPackageManager();
        localObject3 = ((Context)localObject1).getPackageName();
        int i = 0;
        str1 = null;
        localPackageInfo = ((PackageManager)localObject2).getPackageInfo((String)localObject3, 0);
        i = versionCode;
        str1 = Integer.toString(i);
        str2 = versionName;
        if (str2 != null) {
          continue;
        }
        str2 = "0.0";
        int j = Build.VERSION.SDK_INT;
        int k = 9;
        if (j < k) {
          continue;
        }
        l = firstInstallTime;
        localObject3 = getIdManager();
        localObject2 = this;
        localObject2 = SessionAnalyticsManager.build(this, (Context)localObject1, (o)localObject3, str1, str2, l);
        analyticsManager = ((SessionAnalyticsManager)localObject2);
        localObject2 = analyticsManager;
        ((SessionAnalyticsManager)localObject2).enable();
        bool = true;
      }
      catch (Exception localException)
      {
        Object localObject2;
        PackageInfo localPackageInfo;
        String str2;
        long l;
        Object localObject1 = c.h();
        Object localObject3 = "Answers";
        String str1 = "Error retrieving app properties";
        ((k)localObject1).e((String)localObject3, str1, localException);
        continue;
      }
      return bool;
      str2 = versionName;
      continue;
      localPackageInfo = null;
      localObject2 = ((PackageManager)localObject2).getApplicationInfo((String)localObject3, 0);
      localObject3 = new java/io/File;
      localObject2 = sourceDir;
      ((File)localObject3).<init>((String)localObject2);
      l = ((File)localObject3).lastModified();
    }
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/Answers.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */