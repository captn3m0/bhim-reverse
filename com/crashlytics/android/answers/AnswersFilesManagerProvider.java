package com.crashlytics.android.answers;

import android.content.Context;
import android.os.Looper;
import io.fabric.sdk.android.services.b.k;
import io.fabric.sdk.android.services.b.s;
import io.fabric.sdk.android.services.c.g;
import io.fabric.sdk.android.services.d.a;
import java.io.File;

class AnswersFilesManagerProvider
{
  static final String SESSION_ANALYTICS_FILE_NAME = "session_analytics.tap";
  static final String SESSION_ANALYTICS_TO_SEND_DIR = "session_analytics_to_send";
  final Context context;
  final a fileStore;
  
  public AnswersFilesManagerProvider(Context paramContext, a parama)
  {
    context = paramContext;
    fileStore = parama;
  }
  
  public SessionAnalyticsFilesManager getAnalyticsFilesManager()
  {
    Object localObject1 = Looper.myLooper();
    Object localObject2 = Looper.getMainLooper();
    if (localObject1 == localObject2)
    {
      localObject1 = new java/lang/IllegalStateException;
      ((IllegalStateException)localObject1).<init>("AnswersFilesManagerProvider cannot be called on the main thread");
      throw ((Throwable)localObject1);
    }
    localObject1 = new com/crashlytics/android/answers/SessionEventTransform;
    ((SessionEventTransform)localObject1).<init>();
    localObject2 = new io/fabric/sdk/android/services/b/s;
    ((s)localObject2).<init>();
    Object localObject3 = fileStore.a();
    g localg = new io/fabric/sdk/android/services/c/g;
    Context localContext = context;
    localg.<init>(localContext, (File)localObject3, "session_analytics.tap", "session_analytics_to_send");
    localObject3 = new com/crashlytics/android/answers/SessionAnalyticsFilesManager;
    localContext = context;
    ((SessionAnalyticsFilesManager)localObject3).<init>(localContext, (SessionEventTransform)localObject1, (k)localObject2, localg);
    return (SessionAnalyticsFilesManager)localObject3;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/AnswersFilesManagerProvider.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */