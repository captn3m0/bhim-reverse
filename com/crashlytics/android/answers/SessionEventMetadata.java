package com.crashlytics.android.answers;

final class SessionEventMetadata
{
  public final String advertisingId;
  public final String androidId;
  public final String appBundleId;
  public final String appVersionCode;
  public final String appVersionName;
  public final String betaDeviceToken;
  public final String buildId;
  public final String deviceModel;
  public final String executionId;
  public final String installationId;
  public final Boolean limitAdTrackingEnabled;
  public final String osVersion;
  private String stringRepresentation;
  
  public SessionEventMetadata(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5, Boolean paramBoolean, String paramString6, String paramString7, String paramString8, String paramString9, String paramString10, String paramString11)
  {
    appBundleId = paramString1;
    executionId = paramString2;
    installationId = paramString3;
    androidId = paramString4;
    advertisingId = paramString5;
    limitAdTrackingEnabled = paramBoolean;
    betaDeviceToken = paramString6;
    buildId = paramString7;
    osVersion = paramString8;
    deviceModel = paramString9;
    appVersionCode = paramString10;
    appVersionName = paramString11;
  }
  
  public String toString()
  {
    Object localObject1 = stringRepresentation;
    if (localObject1 == null)
    {
      localObject1 = new java/lang/StringBuilder;
      ((StringBuilder)localObject1).<init>();
      localObject1 = ((StringBuilder)localObject1).append("appBundleId=");
      Object localObject2 = appBundleId;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", executionId=");
      localObject2 = executionId;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", installationId=");
      localObject2 = installationId;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", androidId=");
      localObject2 = androidId;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", advertisingId=");
      localObject2 = advertisingId;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", limitAdTrackingEnabled=");
      localObject2 = limitAdTrackingEnabled;
      localObject1 = ((StringBuilder)localObject1).append(localObject2).append(", betaDeviceToken=");
      localObject2 = betaDeviceToken;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", buildId=");
      localObject2 = buildId;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", osVersion=");
      localObject2 = osVersion;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", deviceModel=");
      localObject2 = deviceModel;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", appVersionCode=");
      localObject2 = appVersionCode;
      localObject1 = ((StringBuilder)localObject1).append((String)localObject2).append(", appVersionName=");
      localObject2 = appVersionName;
      localObject1 = (String)localObject2;
      stringRepresentation = ((String)localObject1);
    }
    return stringRepresentation;
  }
}


/* Location:              /home/nemo/projects/personal/bhim-reverse/BHIM-enjarify.jar!/com/crashlytics/android/answers/SessionEventMetadata.class
 * Java compiler version: 5 (49.0)
 * JD-Core Version:       0.7.1
 */